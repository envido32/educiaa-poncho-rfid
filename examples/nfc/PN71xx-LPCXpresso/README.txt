About
=====

This package contains the LPCXpresso projects allowing to demonstrate NFC support.


Details
===========

 * Support for all NCI based NXP's NFC Controller (e.g PN7120/PN7150)
 * Support LPC82X, LPC11Uxx and LPC11U6x NXP's Microcontoller Units
 * Demonstrates:
    o NFC Forum tags reading (read NDEF message)
    o P2P (NDEF message exchange through NFC Forum SNEP protocol) 
    o Type 4 Tag Emulation
    o NFC Forum T2T and T4T tag writing (write NDEF message)
    o Raw tag (non-NDEF) access (MIFARE, ISO14443-4, ISO14443-3A and ISO15693)
    o Multiple tag support (up to 2 of the same technology or multi-protocol card)


Installation
============

Follow instructions from AN11658 NXP-NCI LPCXpresso example:
 http://www.nxp.com/documents/application_note/AN11658.pdf


History
=======

 * v2.2: Fixed issue with multiple Tag 3 type tag support
	 Fixed issue with Multiple ISO15693 tag support
         Raising ATS and ATTRIB card parameters to the interface
	 Prevent potential RF settings EEPROM memory corruption
 * v2.1: Added support for LPC11U6X
         Added T4T tag NDEF message writing support
	 Update MCU compiler optimization level to 'optimize for size'
	 Discontinued support for LPC11xx and LPC12xx 
	 Fixed compilation warnings
	 Fixed incompliancy of T4T NDEF check procedure
	 Fixed wrong parsing of connection handover NDEF record
	 Fixed interroperability issues with some NFC phones
 * v2.0: Rework of the application structure
	 Fixed latencies in execution
         Fixed P2P timing issue between 2 devices
	 Added card RAW access example
	 Added more NFC settings example
         Added multiple tag support (up to 2 of the same technology)
	 Added presence_check for all card types
 * v1.6: Added NXP SLDA
 * v1.5: Added ISO15693 reader mode support
 * v1.4: Added Configure Settings capabilities
 * v1.3: Fixed latency issue in Debug mode
         Added multi-protocols card support
         Added VCard NDEF record parsing
 * v1.2: Added support for LPC11Uxx
 * v1.1: Added support for LPC82X
 * v1.0: First release
