About
=====

This package contains MCUXpresso projects allowing to demonstrate NFC support.


Details
===========

 * Support for all NCI based NXP's NFC Controller (e.g PN7120, PN7150)
 * Support LPC82X, LPC11Uxx and LPC11U6x NXP's Microcontoller Units from LPC family
 * Support K64F NXP's Microcontoller Unit from Kinetis family based on KSDK 2.2
 * Demonstrates:
    o NFC Forum tags reading (read NDEF message)
    o P2P (NDEF message exchange through NFC Forum SNEP protocol) 
    o Type 4 Tag Emulation
    o NFC Forum T2T and T4T tag writing (write NDEF message)
    o Raw tag (non-NDEF) access (MIFARE, ISO14443-4, ISO14443-3A and ISO15693)
    o Multiple tag support (up to 2 of the same technology or multi-protocol card)

Installation
============

Follow instructions from AN11990 NXP-NCI MCUXpresso example:
 http://www.nxp.com/documents/application_note/AN11990.pdf


History
=======

 * v1.2: Add mechanism to prevent applying settings if no change
         Add API to allow configuring settings and retrieving FW version
         Add possibility to send successive NDEF messages in P2P mode
	 Fix issue in T2T reader mode in case NDEF message size is null
	 Fix issue in T4T reader mode in case card makes use of data chaining
 * v1.1: Fixed issue with multiple Tag 3 type tag support
	 Fixed issue with Multiple ISO15693 tag support
         Raising ATS and ATTRIB card parameters to the interface
	 Prevent potential RF settings EEPROM memory corruption
 * v1.0: First release
