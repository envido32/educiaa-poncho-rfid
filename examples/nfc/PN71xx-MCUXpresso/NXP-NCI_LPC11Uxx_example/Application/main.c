/*
*         Copyright (c), NXP Semiconductors Caen / France
*
*                     (C)NXP Semiconductors
*       All rights are reserved. Reproduction in whole or in part is
*      prohibited without the written consent of the copyright owner.
*  NXP reserves the right to make changes without notice at any time.
* NXP makes no warranty, expressed, implied or statutory, including but
* not limited to any implied warranty of merchantability or fitness for any
*particular purpose, or that the use will not infringe any third party patent,
* copyright or trademark. NXP must not be liable for any loss or damage
*                          arising from its use.
*/

#include <stdio.h>
#include <nfc_task.h>
#include <board.h>
#include <i2c.h>
#include <gpio.h>

#define I2C_FASTPLUS_BIT     0

static void Init_I2C_PinMux(void)
{
	Board_SystemInit();

	Chip_SYSCTL_PeriphReset(RESET_I2C0);
	Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 4, IOCON_FUNC1 | I2C_FASTPLUS_BIT);
	Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 5, IOCON_FUNC1 | I2C_FASTPLUS_BIT);
}

//-----------------------------------------------------------------------
// Main Function
//-----------------------------------------------------------------------
int main(void)
{
	Board_Init();
	Init_I2C_PinMux();

	i2c_Init();
	gpio_Init();

    printf("\nRunning the NXP-NCI project.\n");

	task_nfc();

	return 0;
}

////////////////////////////////////////////////////////////////////////////////
// EOF
////////////////////////////////////////////////////////////////////////////////
