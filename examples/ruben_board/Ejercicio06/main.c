/* FreeRTOS.org includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

/* Demo includes. */
#include "basic_io.h"

void vTareaEstadoBoton (void *pvParameters);
void vTareaRed (void *pvParameters);
void vTareaGreen (void *pvParameters);
void vTareaBlue (void *pvParameters);
char boton = 0;

/*-----------------------------------------------------------*/

#define SALIDA	1
#define ENTRADA 0

xSemaphoreHandle xRed;
xSemaphoreHandle xGreen;
xSemaphoreHandle xBlue;

int main( void )
{
	/* Init the semi-hosting. */
	printf( "\n" );

	vSemaphoreCreateBinary( xRed );
	vSemaphoreCreateBinary( xGreen );
	vSemaphoreCreateBinary( xBlue );

	xTaskCreate(
			vTareaEstadoBoton,			/* Pointer to the function that implements the task. */
			"BOTON",					/* Text name for the task.  This is to facilitate debugging only. */
			configMINIMAL_STACK_SIZE,	/* Stack depth in words. */
			(void*) 0,					/* gTestParamas[0] */
			1,							/* Prioridad */
			NULL );						/* Task handle. */

	xTaskCreate(
				vTareaRed,					/* Pointer to the function that implements the task. */
				"Red",						/* Text name for the task.  This is to facilitate debugging only. */
				configMINIMAL_STACK_SIZE,	/* Stack depth in words. */
				(void*) 0,					/* gTestParamas[0] */
				1,							/* Prioridad */
				NULL );						/* Task handle. */

	xTaskCreate(
				vTareaGreen,					/* Pointer to the function that implements the task. */
				"Green",						/* Text name for the task.  This is to facilitate debugging only. */
				configMINIMAL_STACK_SIZE,	/* Stack depth in words. */
				(void*) 0,					/* gTestParamas[0] */
				1,							/* Prioridad */
				NULL );						/* Task handle. */

	xTaskCreate(
				vTareaBlue,					/* Pointer to the function that implements the task. */
				"Blue",						/* Text name for the task.  This is to facilitate debugging only. */
				configMINIMAL_STACK_SIZE,	/* Stack depth in words. */
				(void*) 0,					/* gTestParamas[0] */
				1,							/* Prioridad */
				NULL );						/* Task handle. */

	/* Start the scheduler so our tasks start executing. */
	vTaskStartScheduler();

	/* If all is well we will never reach here as the scheduler will now be
	running.  If we do reach here then it is likely that there was insufficient
	heap available for the idle task to be created. */
	for( ;; );
	return 0;
}
/*-----------------------------------------------------------*/

#define RED_PORT	2
#define RED_BIT		(1UL << 3)
#define GREEN_PORT	2
#define GREEN_BIT	(1UL << 4)
#define BLUE_PORT	2
#define BLUE_BIT	(1UL << 2)

#define PASIVO 1
#define ACTIVO 0

#define ON	1
#define OFF	0

#define PB_PORT 	0
#define PB_BIT 		(1UL << 18)

#define PULSADOR	(!((GPIO_ReadValue(PB_PORT)) & PB_BIT))
#define TIEMPO	25
#define DELAY	MS(1000)


// Tarea vTareaOndaCuadrada: Cada 500ms chequea si está presionado
// el boton WAKEUP SW_4 de la base board. Si lo encuentra pulsado
// Genera un pulso de 500ms en el led del stick.
// No hace debounce del pulsador.
void vTareaEstadoBoton (void *pvParameters)
{
	char aux = OFF;
	GPIO_SetDir(PB_PORT, PB_BIT, ENTRADA);
	for ( ; ; ) {
		if (PULSADOR){
			vTaskDelay(MS(TIEMPO));
			if (PULSADOR){
				aux = boton;
				boton = ON;
			}
		}
		else {
			vTaskDelay(MS(TIEMPO));
			if (!PULSADOR){
				aux = boton;
				boton = OFF;
			}
		}
		if ((aux == ON) && (boton == OFF)){
			xSemaphoreGive (xRed);
			xSemaphoreGive (xGreen);
			xSemaphoreGive (xBlue);
			aux = OFF;
		}
	}
}

void vTareaRed (void *pvParameters)
{
	GPIO_SetDir(RED_PORT, RED_BIT, SALIDA);
	for ( ; ; )
	{
		xSemaphoreTake(xRed, portMAX_DELAY);
		GPIO_SetValue(RED_PORT, RED_BIT);
		vTaskDelay(DELAY);
		GPIO_ClearValue(RED_PORT, RED_BIT);
	}
}

void vTareaGreen (void *pvParameters)
{
	GPIO_SetDir(GREEN_PORT, GREEN_BIT, SALIDA);
	for ( ; ; )
	{
		xSemaphoreTake(xGreen, portMAX_DELAY);
		GPIO_SetValue(GREEN_PORT, GREEN_BIT);
		vTaskDelay(DELAY);
		GPIO_ClearValue(GREEN_PORT, GREEN_BIT);
	}
}

void vTareaBlue (void *pvParameters)
{
	GPIO_SetDir(BLUE_PORT, BLUE_BIT, SALIDA);
	for ( ; ; )
	{
		xSemaphoreTake(xBlue, portMAX_DELAY);
		GPIO_SetValue(BLUE_PORT, BLUE_BIT);
		vTaskDelay(DELAY);
		GPIO_ClearValue(BLUE_PORT, BLUE_BIT);
	}
}
