/* FreeRTOS.org includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "LPC17xx.h"
#include "lpc17xx_adc.h"

/* Demo includes. */
#include "basic_io.h"

void vADC (void *pvParameters);
void vLEDs (void *pvParameters);

uint16_t estadoADC = 0;

/*-----------------------------------------------------------*/

int main( void )
{
	/* Init the semi-hosting. */
	printf( "\n" );

	xTaskCreate(
			vADC,						/* Pointer to the function that implements the task. */
			"ADC",					/* Text name for the task.  This is to facilitate debugging only. */
			configMINIMAL_STACK_SIZE,	/* Stack depth in words. */
			(void*) 0,					/* gTestParamas[0] */
			1,							/* Prioridad */
			NULL );						/* Task handle. */

	xTaskCreate(
				vLEDs,						/* Pointer to the function that implements the task. */
				"LEDs",					/* Text name for the task.  This is to facilitate debugging only. */
				configMINIMAL_STACK_SIZE,	/* Stack depth in words. */
				(void*) 0,					/* gTestParamas[0] */
				1,							/* Prioridad */
				NULL );						/* Task handle. */

	/* Start the scheduler so our tasks start executing. */
	vTaskStartScheduler();

	/* If all is well we will never reach here as the scheduler will now be
	running.  If we do reach here then it is likely that there was insufficient
	heap available for the idle task to be created. */
	for( ;; );
	return 0;
}
/*-----------------------------------------------------------*/
#define SALIDA		1
#define ENTRADA 	0

#define PASIVO 		1
#define ACTIVO 		0

#define PB_PORT 	1
#define PB_BIT 		(1UL << 31)

#define PULSADOR 	(!((GPIO_ReadValue(PB_PORT)) & PB_BIT))
#define TIEMPO		250
#define PERIODO		1000
#define	CLK			200000
#define FULL		4096
#define NROLEDS		8
#define CHANNEL 	5
#define PORT		0
#define BIT			1


const char LED[NROLEDS][NROLEDS] = {{ 0, 0, 2, 2, 2, 0, 2, 2},
									{17,22, 2, 3, 4, 4,11,12}};

void vADC (void *pvParameters)
{
	ADC_Init(LPC_ADC, CLK);
	ADC_ChannelCmd(LPC_ADC, CHANNEL, ENABLE);
	ADC_BurstCmd(LPC_ADC, ENABLE);
	for ( ; ; ) {
		estadoADC = ADC_ChannelGetData (LPC_ADC, CHANNEL);
	}
}

// Funcion Pulso: Genera un pulso de tiempo_ms en el led del stick.
void vLEDs (void *pvParameters)
{
	char i =0;
	for (i = 0; i<NROLEDS; i++){
		GPIO_SetDir(LED[i][PORT], LED[i][BIT], SALIDA);
		GPIO_SetValue(LED[i][PORT], LED[i][BIT]);
	}
	for ( ; ; ){
		for (i = 0; i<NROLEDS; i++){
			if (i < (estadoADC / (FULL / NROLEDS)))
				GPIO_ClearValue(LED[i][PORT], LED[i][BIT]);
			else
				GPIO_SetValue(LED[i][PORT], LED[i][BIT]);
		}
	}
}
