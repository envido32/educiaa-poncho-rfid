/* FreeRTOS.org includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
//#include "lpc17xx_gpio.c"


/* Demo includes. */
#include "basic_io.h"

#define SALIDA	1
#define ENTRADA 0

#define LED1_PORT	0
#define LED1_BIT	(1UL << 17)
#define LED2_PORT	0
#define LED2_BIT	(1UL << 22)

#define PASIVO 1
#define ACTIVO 0

#define RISING	0
#define FALLING	1

#define ON	1
#define OFF	0

#define TRUE	1
#define FALSE	0

#define PB_PORT 	0
#define PB_BIT 		(1UL << 18)

#define PULSADOR (!((GPIO_ReadValue(PB_PORT)) & PB_BIT))
#define TIEMPO 25

void vTareaEstadoBoton (void *pvParameters);
void vTareaLED1 (void *pvParameters);
void vTareaLED2 (void *pvParameters);
void vTareaTiempo (void *pvParameters);

char boton = 0;
portTickType xBotonInit = 0;
portTickType xBotonFin = 0;
xSemaphoreHandle xBotonUp;
xSemaphoreHandle xBotonDw;
xSemaphoreHandle xLed;

/*-----------------------------------------------------------*/

int main( void )
{
	/* Init the semi-hosting. */
	printf( "\n" );
//	GPIO_SetValue (LED1_PORT, LED1_BIT);
//	GPIO_SetValue (LED2_PORT, LED2_BIT);

	vSemaphoreCreateBinary( xBotonUp );
	vSemaphoreCreateBinary( xBotonDw );
	vSemaphoreCreateBinary( xLed );

	xTaskCreate(
			vTareaEstadoBoton,			/* Pointer to the function that implements the task. */
			"ANTIREB",					/* Text name for the task.  This is to facilitate debugging only. */
			configMINIMAL_STACK_SIZE,	/* Stack depth in words. */
			(void*) 0,					/* gTestParamas[0] */
			1,							/* Prioridad */
			NULL );						/* Task handle. */

	xTaskCreate(
			vTareaLED1,					/* Pointer to the function that implements the task. */
			"LED1",						/* Text name for the task.  This is to facilitate debugging only. */
			configMINIMAL_STACK_SIZE,	/* Stack depth in words. */
			(void*) 0,					/* gTestParamas[0] */
			1,							/* Prioridad */
			NULL );						/* Task handle. */

	xTaskCreate(
			vTareaLED2,					/* Pointer to the function that implements the task. */
			"LED2",						/* Text name for the task.  This is to facilitate debugging only. */
			configMINIMAL_STACK_SIZE,	/* Stack depth in words. */
			(void*) 0,					/* gTestParamas[0] */
			1,							/* Prioridad */
			NULL );						/* Task handle. */

	xTaskCreate(
			vTareaTiempo,					/* Pointer to the function that implements the task. */
			"Tiempo",						/* Text name for the task.  This is to facilitate debugging only. */
			configMINIMAL_STACK_SIZE,	/* Stack depth in words. */
			(void*) 0,					/* gTestParamas[0] */
			1,							/* Prioridad */
			NULL );						/* Task handle. */

	/* Start the scheduler so our tasks start executing. */
	vTaskStartScheduler();

	/* If all is well we will never reach here as the scheduler will now be
	running.  If we do reach here then it is likely that there was insufficient
	heap available for the idle task to be created. */
	for( ;; );
	return 0;
}
/*-----------------------------------------------------------*/

void vTareaEstadoBoton (void *pvParameters)
{
	char aux = OFF;
	GPIO_SetDir(PB_PORT, PB_BIT, ENTRADA);
//	GPIO_IntCmd(PB_PORT, PB_BIT, RISING);
	for ( ; ; ) {
		if (PULSADOR){
			vTaskDelay(MS(TIEMPO));
			if (PULSADOR && !boton){
				boton = ON;
				xSemaphoreGive (xBotonUp);
			}
		}
		else {
			vTaskDelay(MS(TIEMPO));
			if (!PULSADOR && boton){
				boton = OFF;
				xSemaphoreGive (xBotonDw);
			}
		}
	}
}

void vTareaTiempo (void *pvParameters)
{
	for ( ; ; ) {
		xSemaphoreTake(xBotonUp, portMAX_DELAY);
		xBotonInit = xTaskGetTickCount();
		xSemaphoreTake(xBotonDw, portMAX_DELAY);
		xBotonFin = xTaskGetTickCount();
		xSemaphoreGive (xLed);
	}
}

void vTareaLED1 (void *pvParameters)
{
	GPIO_SetDir(LED1_PORT, LED1_BIT, SALIDA);
	GPIO_SetValue(LED1_PORT, LED1_BIT);
	for ( ; ; ) {
		if (boton) 	GPIO_ClearValue(LED1_PORT, LED1_BIT);
		else 		GPIO_SetValue(LED1_PORT, LED1_BIT);
	}
}

void vTareaLED2 (void *pvParameters)
{
	portTickType xLastWakeTime;
	GPIO_SetDir(LED2_PORT, LED2_BIT, SALIDA);
	GPIO_SetValue (LED2_PORT, LED2_BIT);
	for ( ; ; ) {
		xSemaphoreTake(xLed, portMAX_DELAY);
		xLastWakeTime = xTaskGetTickCount();
		GPIO_ClearValue (LED2_PORT, LED2_BIT);
		vTaskDelayUntil(&xLastWakeTime, xBotonFin - xBotonInit);
		GPIO_SetValue (LED2_PORT, LED2_BIT);
	}
}
