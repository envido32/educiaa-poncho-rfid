/* FreeRTOS.org includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

/* Demo includes. */
#include "basic_io.h"

#define SALIDA	1
#define ENTRADA 0

#define LED1_PORT	0
#define LED1_BIT	(1UL << 17)
#define LED2_PORT	0
#define LED2_BIT	(1UL << 22)

#define PASIVO 1
#define ACTIVO 0

#define ON	1
#define OFF	0

#define TRUE	1
#define FALSE	0

#define PB_PORT 	0
#define PB_BIT 		(1UL << 18)

#define PULSADOR (!((GPIO_ReadValue(PB_PORT)) & PB_BIT))
#define TIEMPO 25

void vTareaEstadoBoton (void *pvParameters);
void vTareaLED1 (void *pvParameters);
void vTareaLED2 (void *pvParameters);
// void vTareaTiempo (void *pvParameters);

xQueueHandle xQueue;

char boton = 0;
portTickType xBotonInit;
portTickType xBotonFin;

/*-----------------------------------------------------------*/

int main( void )
{
	/* Init the semi-hosting. */
	printf( "\n" );

	xQueue = xQueueCreate(16, sizeof(portTickType));

	xTaskCreate(
			vTareaEstadoBoton,			/* Pointer to the function that implements the task. */
			"ANTIREB",					/* Text name for the task.  This is to facilitate debugging only. */
			configMINIMAL_STACK_SIZE,	/* Stack depth in words. */
			(void*) 0,					/* gTestParamas[0] */
			1,							/* Prioridad */
			NULL );						/* Task handle. */

	xTaskCreate(
			vTareaLED1,					/* Pointer to the function that implements the task. */
			"LED1",						/* Text name for the task.  This is to facilitate debugging only. */
			configMINIMAL_STACK_SIZE,	/* Stack depth in words. */
			(void*) 0,					/* gTestParamas[0] */
			1,							/* Prioridad */
			NULL );						/* Task handle. */

	xTaskCreate(
			vTareaLED2,					/* Pointer to the function that implements the task. */
			"LED2",						/* Text name for the task.  This is to facilitate debugging only. */
			configMINIMAL_STACK_SIZE,	/* Stack depth in words. */
			(void*) 0,					/* gTestParamas[0] */
			1,							/* Prioridad */
			NULL );						/* Task handle. */

//	xTaskCreate(
//			vTareaTiempo,					/* Pointer to the function that implements the task. */
//			"TIEMPO",						/* Text name for the task.  This is to facilitate debugging only. */
//			configMINIMAL_STACK_SIZE,	/* Stack depth in words. */
//			(void*) 0,					/* gTestParamas[0] */
//			1,							/* Prioridad */
//			NULL );						/* Task handle. */

	/* Start the scheduler so our tasks start executing. */
	vTaskStartScheduler();

	/* If all is well we will never reach here as the scheduler will now be
	running.  If we do reach here then it is likely that there was insufficient
	heap available for the idle task to be created. */
	for( ;; );
	return 0;
}
/*-----------------------------------------------------------*/

void vTareaEstadoBoton (void *pvParameters)
{
	char aux = OFF;
	portTickType xLastWakeTime;
	GPIO_SetDir(PB_PORT, PB_BIT, ENTRADA);
	for ( ; ; ) {
		if (PULSADOR){
			vTaskDelay(MS(TIEMPO));
			if (PULSADOR){
				aux = boton;
				boton = ON;
			}
		}
		else {
			vTaskDelay(MS(TIEMPO));
			if (!PULSADOR){
				aux = boton;
				boton = OFF;
			}
		}
		if (aux != boton){
			xLastWakeTime = xTaskGetTickCount();
			xQueueSendToBack(xQueue, &xLastWakeTime, 0);
			aux = boton;
		}
	}
}

void vTareaLED1 (void *pvParameters)
{
	GPIO_SetDir(LED1_PORT, LED1_BIT, SALIDA);
	GPIO_SetValue(LED1_PORT, LED1_BIT);
	for ( ; ; ) {
		if (boton) 	GPIO_ClearValue(LED1_PORT, LED1_BIT);
		else 		GPIO_SetValue(LED1_PORT, LED1_BIT);
	}
}

void vTareaLED2 (void *pvParameters)
{
	portTickType xLastWakeTime;
	portTickType xBotonFin;
	portTickType xBotonInit;
	GPIO_SetDir(LED2_PORT, LED2_BIT, SALIDA);
	GPIO_SetValue (LED2_PORT, LED2_BIT);
	for ( ; ; ) {
		xQueueReceive(xQueue, &xBotonInit, portMAX_DELAY);
		xQueueReceive(xQueue, &xBotonFin, portMAX_DELAY);
		xLastWakeTime = xTaskGetTickCount();
		GPIO_ClearValue (LED2_PORT, LED2_BIT);
		vTaskDelayUntil(&xLastWakeTime, xBotonFin - xBotonInit);
		GPIO_SetValue (LED2_PORT, LED2_BIT);
		}
}
