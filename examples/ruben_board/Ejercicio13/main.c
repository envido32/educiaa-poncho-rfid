/* FreeRTOS.org includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

/* Demo includes. */
#include "basic_io.h"

#define SALIDA	1
#define ENTRADA 0

#define LED1_PORT	0
#define LED1_BIT	(1UL << 17)
//#define LED2_PORT	0
//#define LED2_BIT	(1UL << 22)
#define LED2_PORT	0
#define LED2_BIT	(1UL << 17)


#define PASIVO 1
#define ACTIVO 0

#define ON	1
#define OFF	0

#define TRUE	1
#define FALSE	0

#define PB_PORT 	0
#define PB_BIT 		(1UL << 18)

#define PULSADOR 	(!((GPIO_ReadValue(PB_PORT)) & PB_BIT))
#define TIEMPO 		MS(25)
#define PERIODO 	MS(1000)

void vTareaEstadoBoton (void *pvParameters);
void vTareaLED (void *pvParameters);
void vTareaOndaCuadrada (void *pvParameters);
void Pulso (long tiempo_ms);

xQueueHandle xQueue;
xSemaphoreHandle mutex;

char boton = 0;
portTickType xBotonInit = 0;
portTickType xBotonFin = 0;

/*-----------------------------------------------------------*/

int main( void )
{
	/* Init the semi-hosting. */
	printf( "\n" );

	mutex = xSemaphoreCreateMutex();
	xQueue = xQueueCreate(16, sizeof(portTickType));

	xTaskCreate(
			vTareaEstadoBoton,			/* Pointer to the function that implements the task. */
			"ANTIREB",					/* Text name for the task.  This is to facilitate debugging only. */
			configMINIMAL_STACK_SIZE,	/* Stack depth in words. */
			(void*) 0,					/* gTestParamas[0] */
			1,							/* Prioridad */
			NULL );						/* Task handle. */

	xTaskCreate(
			vTareaOndaCuadrada,			/* Pointer to the function that implements the task. */
			"ONDA",						/* Text name for the task.  This is to facilitate debugging only. */
			configMINIMAL_STACK_SIZE,	/* Stack depth in words. */
			(void*) 0,					/* gTestParamas[0] */
			1,							/* Prioridad */
			NULL );						/* Task handle. */

	xTaskCreate(
			vTareaLED,					/* Pointer to the function that implements the task. */
			"LED",						/* Text name for the task.  This is to facilitate debugging only. */
			configMINIMAL_STACK_SIZE,	/* Stack depth in words. */
			(void*) 0,					/* gTestParamas[0] */
			1,							/* Prioridad */
			NULL );						/* Task handle. */


	/* Start the scheduler so our tasks start executing. */
	vTaskStartScheduler();

	/* If all is well we will never reach here as the scheduler will now be
	running.  If we do reach here then it is likely that there was insufficient
	heap available for the idle task to be created. */
	for( ;; );
	return 0;
}
/*-----------------------------------------------------------*/

void vTareaEstadoBoton (void *pvParameters)
{
	char aux = OFF;
	GPIO_SetDir(PB_PORT, PB_BIT, ENTRADA);
	for ( ; ; ) {
		if (PULSADOR){
			vTaskDelay(TIEMPO);
			if (PULSADOR){
				aux = boton;
				boton = ON;
			}
		}
		else {
			vTaskDelay(MS(TIEMPO));
			if (!PULSADOR){
				aux = boton;
				boton = OFF;
			}
		}
		if (aux != boton){
			xQueueSendToBack(xQueue, xTaskGetTickCount(), 0);
			aux = boton;
		}
	}
}

void vTareaOndaCuadrada (void *pvParameters)
{
	portTickType xLastWakeTime;
	GPIO_SetDir(LED1_PORT, LED1_BIT, SALIDA);
	xSemaphoreGive(mutex);
		for ( ; ; ) {
			xSemaphoreTake(mutex, portMAX_DELAY);
			xLastWakeTime = xTaskGetTickCount();
			Pulso(PERIODO/2);
			vTaskDelayUntil(&xLastWakeTime, PERIODO);
			xSemaphoreGive(mutex);
		}
}

void Pulso (long tiempo_ms)
{
	portTickType xLastWakeTime;
	xLastWakeTime = xTaskGetTickCount();
	GPIO_SetValue(LED1_PORT, LED1_BIT);
	vTaskDelayUntil(&xLastWakeTime, tiempo_ms);
	GPIO_ClearValue(LED1_PORT, LED1_BIT);
}

void vTareaLED (void *pvParameters)
{
	portTickType xLastWakeTime;
	portTickType xBotonFin;
	portTickType xBotonInit;
	portTickType xTemp;
	GPIO_SetDir(LED2_PORT, LED2_BIT, SALIDA);
	for ( ; ; ) {
		xQueueReceive(xQueue, &xBotonInit, portMAX_DELAY);
		xQueueReceive(xQueue, &xBotonFin, portMAX_DELAY);
		xSemaphoreTake(mutex, portMAX_DELAY);
		xLastWakeTime = xTaskGetTickCount();
		GPIO_ClearValue (LED2_PORT, LED2_BIT);
		xTemp = xBotonFin - xBotonInit;
		vTaskDelayUntil(&xLastWakeTime, xTemp);
		GPIO_SetValue (LED2_PORT, LED2_BIT);
		xSemaphoreGive(mutex);
	}
}
