EESchema Schematic File Version 4
LIBS:poncho_grande-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 3
Title "Poncho RFID para la EDU-CIAA"
Date "2018-11-19"
Rev "1.0"
Comp "Proyecto CIAA - COMPUTADORA INDUSTRIAL ABIERTA ARGENTINA"
Comment1 "https://gitlab.com/envido32/educiaa-poncho-rfid"
Comment2 "Autores y Licencia del template (Manuel Cajide Maidana - UNLaM)"
Comment3 "Autor del poncho (Manuel Cajide Maidana - UTN FRA)."
Comment4 "CÓDIGO PONCHO: Prototipo"
$EndDescr
$Comp
L power:+3.3V #PWR?
U 1 1 560A0C4B
P 4850 1100
AR Path="/560A0C4B" Ref="#PWR?"  Part="1" 
AR Path="/560A0C15/560A0C4B" Ref="#PWR025"  Part="1" 
F 0 "#PWR025" H 4850 1060 30  0001 C CNN
F 1 "+3.3V" H 4850 1210 30  0000 C CNN
F 2 "" H 4850 1100 60  0000 C CNN
F 3 "" H 4850 1100 60  0000 C CNN
	1    4850 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5100 1200 4850 1200
Wire Wire Line
	4850 1200 4850 1100
$Comp
L power:GND #PWR?
U 1 1 560A0C7E
P 6750 3250
AR Path="/560A0C7E" Ref="#PWR?"  Part="1" 
AR Path="/560A0C15/560A0C7E" Ref="#PWR026"  Part="1" 
F 0 "#PWR026" H 6750 3250 30  0001 C CNN
F 1 "GND" H 6750 3180 30  0001 C CNN
F 2 "" H 6750 3250 60  0000 C CNN
F 3 "" H 6750 3250 60  0000 C CNN
	1    6750 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	6750 1050 6750 1300
Wire Wire Line
	6750 1300 6750 2100
Wire Wire Line
	6750 2100 6750 2200
Wire Wire Line
	6750 2200 6750 2300
Wire Wire Line
	6750 2300 6750 2400
Wire Wire Line
	6750 2400 6750 2500
Wire Wire Line
	6750 2500 6750 2600
Wire Wire Line
	6750 2600 6750 2700
Wire Wire Line
	6750 2700 6750 3000
Wire Wire Line
	6750 3000 6750 3100
Wire Wire Line
	6750 3100 6750 3250
Wire Wire Line
	6750 1300 6300 1300
Wire Wire Line
	6300 2100 6750 2100
Connection ~ 6750 2100
Wire Wire Line
	6300 2200 6750 2200
Connection ~ 6750 2200
Wire Wire Line
	6300 2300 6750 2300
Connection ~ 6750 2300
Wire Wire Line
	6300 2400 6750 2400
Connection ~ 6750 2400
Wire Wire Line
	6300 2500 6750 2500
Connection ~ 6750 2500
Wire Wire Line
	6300 2600 6750 2600
Connection ~ 6750 2600
Wire Wire Line
	6300 2700 6750 2700
Connection ~ 6750 2700
Wire Wire Line
	6300 3000 6750 3000
Connection ~ 6750 3000
Wire Wire Line
	6300 3100 6750 3100
Connection ~ 6750 3100
$Comp
L power:GNDA #PWR?
U 1 1 560A0D89
P 6600 1800
AR Path="/560A0D89" Ref="#PWR?"  Part="1" 
AR Path="/560A0C15/560A0D89" Ref="#PWR027"  Part="1" 
F 0 "#PWR027" H 6600 1800 40  0001 C CNN
F 1 "GNDA" H 6600 1730 40  0000 C CNN
F 2 "" H 6600 1800 60  0000 C CNN
F 3 "" H 6600 1800 60  0000 C CNN
	1    6600 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	6450 2000 6300 2000
Wire Wire Line
	6450 1900 6300 1900
Wire Wire Line
	6450 1800 6300 1800
Connection ~ 6450 1900
Connection ~ 6450 1800
Wire Wire Line
	6300 1500 6450 1500
Wire Wire Line
	6450 1500 6450 1600
Wire Wire Line
	6450 1600 6450 1650
Wire Wire Line
	6450 1650 6450 1700
Wire Wire Line
	6450 1700 6450 1750
Wire Wire Line
	6450 1750 6450 1800
Wire Wire Line
	6450 1800 6450 1900
Wire Wire Line
	6450 1900 6450 2000
Wire Wire Line
	6300 1700 6450 1700
Connection ~ 6450 1700
Wire Wire Line
	6300 1600 6450 1600
Connection ~ 6450 1600
Wire Wire Line
	6600 1800 6600 1750
Wire Wire Line
	6600 1750 6450 1750
Connection ~ 6450 1750
$Comp
L power:GND #PWR?
U 1 1 560A117F
P 4750 6000
AR Path="/560A117F" Ref="#PWR?"  Part="1" 
AR Path="/560A0C15/560A117F" Ref="#PWR028"  Part="1" 
F 0 "#PWR028" H 4750 6000 30  0001 C CNN
F 1 "GND" H 4750 5930 30  0001 C CNN
F 2 "" H 4750 6000 60  0000 C CNN
F 3 "" H 4750 6000 60  0000 C CNN
	1    4750 6000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 5850 5100 5850
Wire Wire Line
	4750 4050 4750 4150
Wire Wire Line
	4750 4150 4750 4250
Wire Wire Line
	4750 4250 4750 4450
Wire Wire Line
	4750 4450 4750 4550
Wire Wire Line
	4750 4550 4750 4750
Wire Wire Line
	4750 4750 4750 4850
Wire Wire Line
	4750 4850 4750 5150
Wire Wire Line
	4750 5150 4750 5250
Wire Wire Line
	4750 5250 4750 5750
Wire Wire Line
	4750 5750 4750 5850
Wire Wire Line
	4750 5850 4750 6000
Wire Wire Line
	4750 4050 5100 4050
Connection ~ 4750 5850
Wire Wire Line
	5100 4150 4750 4150
Connection ~ 4750 4150
Wire Wire Line
	5100 4250 4750 4250
Connection ~ 4750 4250
Wire Wire Line
	5100 4450 4750 4450
Connection ~ 4750 4450
Wire Wire Line
	5100 4550 4750 4550
Connection ~ 4750 4550
Wire Wire Line
	5100 4750 4750 4750
Connection ~ 4750 4750
Wire Wire Line
	5100 4850 4750 4850
Connection ~ 4750 4850
Wire Wire Line
	5100 5150 4750 5150
Connection ~ 4750 5150
Wire Wire Line
	5100 5250 4750 5250
Connection ~ 4750 5250
Wire Wire Line
	5100 5750 4750 5750
Connection ~ 4750 5750
$Comp
L power:+3.3V #PWR?
U 1 1 560A13F3
P 4750 3900
AR Path="/560A13F3" Ref="#PWR?"  Part="1" 
AR Path="/560A0C15/560A13F3" Ref="#PWR029"  Part="1" 
F 0 "#PWR029" H 4800 3930 20  0001 C CNN
F 1 "+3.3V" H 4750 3990 30  0000 C CNN
F 2 "" H 4750 3900 60  0000 C CNN
F 3 "" H 4750 3900 60  0000 C CNN
	1    4750 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5100 3950 4750 3950
Wire Wire Line
	4750 3950 4750 3900
Wire Wire Line
	5100 2100 3650 2100
Wire Wire Line
	5100 2200 3650 2200
$Comp
L power:GNDA #PWR?
U 1 1 560A1A14
P 4150 1600
AR Path="/560A1A14" Ref="#PWR?"  Part="1" 
AR Path="/560A0C15/560A1A14" Ref="#PWR030"  Part="1" 
F 0 "#PWR030" H 4150 1600 40  0001 C CNN
F 1 "GNDA" H 4150 1450 40  0000 C CNN
F 2 "" H 4150 1600 60  0000 C CNN
F 3 "" H 4150 1600 60  0000 C CNN
	1    4150 1600
	1    0    0    -1  
$EndComp
Text HLabel 3650 2100 0    60   BiDi ~ 0
I2C_SDA
Text HLabel 3650 2200 0    60   BiDi ~ 0
I2C_SCL
Text HLabel 4150 2600 0    60   BiDi ~ 0
CAN_TD
Text HLabel 3650 2700 0    60   BiDi ~ 0
KB_C1
Text HLabel 3650 2800 0    60   BiDi ~ 0
KB_F0
Text HLabel 3650 2900 0    60   BiDi ~ 0
KB_F3
Text HLabel 3650 3000 0    60   BiDi ~ 0
KB_F2
Text HLabel 3650 3100 0    60   BiDi ~ 0
KB_C0
Text HLabel 2400 3400 0    60   BiDi ~ 0
KB_C2
Text HLabel 2400 3500 0    60   BiDi ~ 0
KB_F1
Wire Wire Line
	7250 2800 6300 2800
Wire Wire Line
	6300 2900 7250 2900
Wire Wire Line
	6300 4050 6850 4050
Wire Wire Line
	6300 4150 6850 4150
Wire Wire Line
	6300 4250 6850 4250
Wire Wire Line
	6300 4350 6850 4350
Wire Wire Line
	6300 4450 6850 4450
Text HLabel 4650 4950 0    60   BiDi ~ 0
SPI_MOSI
Text HLabel 4050 5350 0    60   BiDi ~ 0
GPIO_0
Text HLabel 4050 5450 0    60   BiDi ~ 0
GPIO_2
Text HLabel 4050 5550 0    60   BiDi ~ 0
GPIO_4
Text HLabel 4050 5650 0    60   BiDi ~ 0
GPIO_6
Text HLabel 7500 4750 2    60   BiDi ~ 0
SPI_MISO
Text HLabel 7500 4850 2    60   BiDi ~ 0
SPI_SCK
Text HLabel 6850 4050 2    60   BiDi ~ 0
RXD1
Text HLabel 6850 4150 2    60   BiDi ~ 0
TXEN
Text HLabel 6850 4250 2    60   BiDi ~ 0
MDC
Text HLabel 6850 4350 2    60   BiDi ~ 0
CRS
Text HLabel 6850 4450 2    60   BiDi ~ 0
MDIO
Text HLabel 7500 5450 2    60   BiDi ~ 0
GPIO_1
Text HLabel 7500 5550 2    60   BiDi ~ 0
GPIO_3
Text HLabel 7500 5650 2    60   BiDi ~ 0
GPIO_5
Text HLabel 7500 5750 2    60   BiDi ~ 0
GPIO_7
Text HLabel 7500 5850 2    60   BiDi ~ 0
GPIO_8
$Comp
L power:PWR_FLAG #FLG?
U 1 1 560A61E2
P 6750 1050
AR Path="/560A61E2" Ref="#FLG?"  Part="1" 
AR Path="/560A0C15/560A61E2" Ref="#FLG032"  Part="1" 
F 0 "#FLG032" H 6750 1145 30  0001 C CNN
F 1 "PWR_FLAG" H 6750 1230 30  0000 C CNN
F 2 "" H 6750 1050 60  0000 C CNN
F 3 "" H 6750 1050 60  0000 C CNN
	1    6750 1050
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG?
U 1 1 560A61F6
P 6600 1650
AR Path="/560A61F6" Ref="#FLG?"  Part="1" 
AR Path="/560A0C15/560A61F6" Ref="#FLG033"  Part="1" 
F 0 "#FLG033" H 6600 1745 30  0001 C CNN
F 1 "PWR_FLAG" H 6600 1830 30  0000 C CNN
F 2 "" H 6600 1650 60  0000 C CNN
F 3 "" H 6600 1650 60  0000 C CNN
	1    6600 1650
	1    0    0    -1  
$EndComp
Connection ~ 6750 1300
Connection ~ 6450 1650
Wire Wire Line
	6450 1050 6450 1200
Wire Wire Line
	6450 1200 6300 1200
$Comp
L power:+5V #PWR?
U 1 1 560C551F
P 6450 1050
AR Path="/560C551F" Ref="#PWR?"  Part="1" 
AR Path="/560A0C15/560C551F" Ref="#PWR034"  Part="1" 
F 0 "#PWR034" H 6450 1140 20  0001 C CNN
F 1 "+5V" H 6450 1140 30  0000 C CNN
F 2 "" H 6450 1050 60  0000 C CNN
F 3 "" H 6450 1050 60  0000 C CNN
	1    6450 1050
	1    0    0    -1  
$EndComp
Wire Wire Line
	6550 3800 6550 3950
Wire Wire Line
	6550 3950 6300 3950
$Comp
L power:+5V #PWR?
U 1 1 560CBF57
P 6550 3800
AR Path="/560CBF57" Ref="#PWR?"  Part="1" 
AR Path="/560A0C15/560CBF57" Ref="#PWR035"  Part="1" 
F 0 "#PWR035" H 6550 3890 20  0001 C CNN
F 1 "+5V" H 6550 3890 30  0000 C CNN
F 2 "" H 6550 3800 60  0000 C CNN
F 3 "" H 6550 3800 60  0000 C CNN
	1    6550 3800
	1    0    0    -1  
$EndComp
$Comp
L Poncho_Esqueleto:Conn_Poncho2P_2x_20x2 XA?
U 2 1 560C5732
P 5350 4250
AR Path="/560C5732" Ref="XA?"  Part="2" 
AR Path="/560A0C15/560C5732" Ref="XA1"  Part="2" 
F 0 "XA1" H 5650 4650 60  0000 C CNN
F 1 "Conn_Poncho2P_2x_20x2" H 5700 2550 60  0000 C CNN
F 2 "Poncho_Esqueleto:Conn_Poncho_SinBorde" H 5600 2450 60  0000 C CNN
F 3 "" H 5350 4250 60  0000 C CNN
	2    5350 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	6450 1650 6600 1650
Text HLabel 4050 5050 0    60   BiDi ~ 0
LCD_EN
Wire Wire Line
	6300 5850 7500 5850
Wire Wire Line
	6300 5750 7500 5750
Wire Wire Line
	6300 5650 7500 5650
Wire Wire Line
	6300 5550 7500 5550
Wire Wire Line
	6300 5450 7500 5450
$Comp
L Poncho_Esqueleto:Conn_Poncho2P_2x_20x2 XA?
U 1 1 560C57B9
P 5350 1500
AR Path="/560C57B9" Ref="XA?"  Part="1" 
AR Path="/560A0C15/560C57B9" Ref="XA1"  Part="1" 
F 0 "XA1" H 5650 1900 60  0000 C CNN
F 1 "Conn_Poncho2P_2x_20x2" H 5700 -200 60  0000 C CNN
F 2 "Poncho_Esqueleto:Conn_Poncho_SinBorde" H 5650 -300 60  0000 C CNN
F 3 "" H 5350 1500 60  0000 C CNN
	1    5350 1500
	1    0    0    -1  
$EndComp
Text HLabel 4150 2500 0    60   BiDi ~ 0
CAN_RD
Wire Wire Line
	5100 2700 3650 2700
Wire Wire Line
	5100 2800 3650 2800
Wire Wire Line
	5100 2900 3650 2900
Wire Wire Line
	5100 3000 3650 3000
Wire Wire Line
	5100 3100 3650 3100
Wire Wire Line
	5100 2600 4150 2600
Wire Wire Line
	5100 2500 4150 2500
Wire Wire Line
	4050 5350 5100 5350
Wire Wire Line
	4050 5450 5100 5450
Wire Wire Line
	4050 5550 5100 5550
Wire Wire Line
	4050 5650 5100 5650
Wire Wire Line
	4650 4950 5100 4950
Wire Wire Line
	6300 4750 7500 4750
Wire Wire Line
	6300 4850 7500 4850
Text HLabel 4950 1300 0    60   BiDi ~ 0
RESET
Text HLabel 4950 1400 0    60   BiDi ~ 0
ISP
Text HLabel 4950 2300 0    60   BiDi ~ 0
RS_RXD
Text HLabel 4950 2400 0    60   BiDi ~ 0
RS_TXD
Text HLabel 7250 1400 2    60   BiDi ~ 0
WAKEUP
Wire Wire Line
	7250 1400 6300 1400
Wire Wire Line
	5100 1300 4950 1300
Wire Wire Line
	4950 1400 5100 1400
Wire Wire Line
	5100 2300 4950 2300
Text HLabel 2400 1300 0    60   BiDi ~ 0
RESET
Text HLabel 2400 1400 0    60   BiDi ~ 0
ISP
Text HLabel 2400 1500 0    60   BiDi ~ 0
WAKEUP
Text HLabel 2400 3200 0    60   BiDi ~ 0
RS_RXD
$Comp
L power:+3.3VADC #PWR?
U 1 1 560A1ACD
P 4150 1950
AR Path="/560A1ACD" Ref="#PWR?"  Part="1" 
AR Path="/560A0C15/560A1ACD" Ref="#PWR031"  Part="1" 
F 0 "#PWR031" H 4150 2070 20  0001 C CNN
F 1 "+3.3VADC" H 4150 2040 30  0000 C CNN
F 2 "" H 4150 1950 60  0000 C CNN
F 3 "" H 4150 1950 60  0000 C CNN
	1    4150 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5100 1500 4150 1500
Wire Wire Line
	4150 1500 4150 1600
Wire Wire Line
	5100 2000 4150 2000
Wire Wire Line
	4150 2000 4150 1950
Text HLabel 4950 1600 0    60   BiDi ~ 0
ADC3
Text HLabel 4950 1700 0    60   BiDi ~ 0
ADC2
Text HLabel 4950 1800 0    60   BiDi ~ 0
ADC1
Text HLabel 4950 1900 0    60   BiDi ~ 0
DAC
Wire Wire Line
	5100 1600 4950 1600
Wire Wire Line
	4950 1700 5100 1700
Wire Wire Line
	4950 1800 5100 1800
Wire Wire Line
	4950 1900 5100 1900
Text HLabel 2400 1700 0    60   BiDi ~ 0
ADC3
Text HLabel 2400 1800 0    60   BiDi ~ 0
ADC2
Text HLabel 2400 1900 0    60   BiDi ~ 0
ADC1
Text HLabel 2400 2000 0    60   BiDi ~ 0
DAC
Wire Wire Line
	2400 1300 2550 1300
NoConn ~ 2550 1300
Wire Wire Line
	2400 1400 2550 1400
NoConn ~ 2550 1400
Wire Wire Line
	2400 1500 2550 1500
NoConn ~ 2550 1500
Wire Wire Line
	2400 3400 2550 3400
NoConn ~ 2550 3400
Wire Wire Line
	2400 1700 2550 1700
NoConn ~ 2550 1700
Wire Wire Line
	2400 1800 2550 1800
NoConn ~ 2550 1800
Wire Wire Line
	2400 1900 2550 1900
NoConn ~ 2550 1900
Wire Wire Line
	2400 2000 2550 2000
NoConn ~ 2550 2000
Wire Wire Line
	2400 2900 2550 2900
NoConn ~ 2550 2900
Wire Wire Line
	2400 3000 2550 3000
NoConn ~ 2550 3000
Text HLabel 4050 4650 0    60   BiDi ~ 0
RCLK
Text HLabel 4050 4350 0    60   BiDi ~ 0
RXD0
Wire Wire Line
	4050 4350 5100 4350
Wire Wire Line
	5100 4650 4050 4650
Wire Wire Line
	6300 4550 6850 4550
Wire Wire Line
	6300 4650 6850 4650
Text HLabel 6850 4550 2    60   BiDi ~ 0
TXD0
Text HLabel 6850 4650 2    60   BiDi ~ 0
TXD1
Wire Wire Line
	6300 4950 6850 4950
Wire Wire Line
	6300 5050 6850 5050
Wire Wire Line
	6300 5150 6850 5150
Wire Wire Line
	6300 5250 6850 5250
Wire Wire Line
	6300 5350 6850 5350
Text HLabel 6850 4950 2    60   BiDi ~ 0
LCD_D4
Text HLabel 6850 5050 2    60   BiDi ~ 0
LCD_RS
Text HLabel 6850 5150 2    60   BiDi ~ 0
LCD_D3
Text HLabel 6850 5250 2    60   BiDi ~ 0
LCD_D2
Text HLabel 6850 5350 2    60   BiDi ~ 0
LCD_D1
Text HLabel 2400 2900 0    60   BiDi ~ 0
RXD0
Text HLabel 2400 3000 0    60   BiDi ~ 0
RCLK
Text HLabel 2400 2200 0    60   BiDi ~ 0
RXD1
Text HLabel 2400 2300 0    60   BiDi ~ 0
TXEN
Text HLabel 2400 2400 0    60   BiDi ~ 0
MDC
Text HLabel 2400 2500 0    60   BiDi ~ 0
CRS
Text HLabel 2400 2600 0    60   BiDi ~ 0
MDIO
Text HLabel 2400 2700 0    60   BiDi ~ 0
TXD0
Text HLabel 2400 2800 0    60   BiDi ~ 0
TXD1
Wire Wire Line
	2400 2200 2550 2200
NoConn ~ 2550 2200
Wire Wire Line
	2400 2300 2550 2300
NoConn ~ 2550 2300
Wire Wire Line
	2400 2400 2550 2400
NoConn ~ 2550 2400
Wire Wire Line
	2400 2500 2550 2500
NoConn ~ 2550 2500
Wire Wire Line
	2400 2600 2550 2600
NoConn ~ 2550 2600
Wire Wire Line
	2400 2700 2550 2700
NoConn ~ 2550 2700
Wire Wire Line
	2400 2800 2550 2800
NoConn ~ 2550 2800
Wire Wire Line
	4050 5050 5100 5050
Wire Wire Line
	4950 2400 5100 2400
Wire Wire Line
	2400 3500 2550 3500
NoConn ~ 2550 3500
Wire Wire Line
	2400 3200 2550 3200
NoConn ~ 2550 3200
Text HLabel 2400 3700 0    60   BiDi ~ 0
LCD_EN
Text HLabel 2400 3800 0    60   BiDi ~ 0
LCD_RS
Wire Wire Line
	2400 3700 2550 3700
NoConn ~ 2550 3700
Wire Wire Line
	2400 3800 2550 3800
NoConn ~ 2550 3800
Text HLabel 7250 2800 2    60   BiDi ~ 0
KB_C2
Text HLabel 7250 2900 2    60   BiDi ~ 0
KB_F1
$EndSCHEMATC
