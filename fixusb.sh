#!/bin/sh
echo "Fixing USB... "
openocd -f /firmware_v2/modules/tools/openocd/cfg/cortexM4/lpc43xx/lpc4337/ciaa-nxp.cfg
sudo adduser $USER dialout
sudo usermod -aG plugdev $USER
sudo udevadm control --reload-rules
sudo service udev restart
echo "USB fixed"