#include "sapi_delay.h"    // <= su propio archivo de cabecera (opcional)

void delayInaccurateUs(tick_t delay_us){
   volatile tick_t i;
   volatile tick_t delay;

   delay = (INACCURATE_TO_US_x10 * delay_us) / 10;

   for( i=delay; i>0; i-- );
}

void delayInaccurate(tick_t delay_ms){
   volatile tick_t i;
   volatile tick_t delay;

   delay = INACCURATE_TO_MS * delay_ms;

   for( i=delay; i>0; i-- );
}
