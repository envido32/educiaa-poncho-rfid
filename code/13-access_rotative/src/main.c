// Includes de FreeRTOS
#include "FreeRTOS.h"
#include "FreeRTOSConfig.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"
#include "sapi.h"
#include "string.h"

// Definiciones
#define MS(t)		((portTickType) ( (t) / portTICK_RATE_MS))
#define Stack(s)	( configMINIMAL_STACK_SIZE * s )
#define Priority(p)	( tskIDLE_PRIORITY + p )

// Config Display
#define LCD_Hor		( 16 )
#define LCD_Ver		( 2 )
#define LCD_pixH	( 5 )
#define LCD_pixV	( 8 )

// Config Keypad
#define MAX_FIL		( 4 )
#define MAX_COL		( 3 )
#define BTN_BUFF	( 32 )

#define BUZZER		( GPIO8 )
#define MENU_OPTS	( 4 )		// Cantidad de opciones disponibles en el menu de administrador
#define MAX_PASS	( 4 )
#define DEFAULT_PASS	"1337"
#define FIRM_VER	" Firmware v0.9b "

// Variables globales
xQueueHandle xBoton = NULL;		// Boton apretado

// Prototipo de funciones de las tareas
void vMenu( void* taskParmPtr );
void vKeypad( void* taskParmPtr );
void vBtn( char btn );

// Implementaciones de funciones de las tareas

/* MENU PRINCIPAL
 * - Inicializa y controla el display
 * - Guarda la contrasena y configura su default
 * - TODO: Carga la contrasena de la SD o EEPROM
 * MODOS: Usuario y Administrador
 */
void vMenu( void* taskParmPtr ) {
	// Init. Config
	unsigned int i = 0;
	char opc = NULL;
	bool_t admin = FALSE;
	char pass[MAX_PASS+1] = {DEFAULT_PASS};
	char input[MAX_PASS+1] = {0};
	char menu_options[MENU_OPTS][LCD_Hor+1] = {	"Exit",
							"Show Pass",
							"Change Pass",
							"Save"};

	gpioConfig( BUZZER, GPIO_OUTPUT );
	lcdInit( LCD_Hor, LCD_Ver, LCD_pixH, LCD_pixV );

	// Imprecion inicial en Display
	lcdClear(); 		// Borrar la pantalla
	lcdGoToXY( 1, 1 ); 	// Poner cursor en 1, 1
	lcdSendStringRaw( "CONTROL DE ACESO" );
	lcdGoToXY( 1, 2 ); 	// Poner cursor en 1, 2
	lcdSendStringRaw( FIRM_VER );
	vTaskDelay( MS(500) );
	vPortYield();		// Me aseguro que cargue otros modulos antes de iniciar

	// Si preciono la tecla durante el inicio entra al modo administrador
	if ( !gpioRead( TEC1 ))		// TODO DEBUG: Descomentar en release
		admin = TRUE;

	// Endless loop
	while(TRUE) {

		/* MODO ADMINISTRADOR: Se accede mantetiendo precionado el boton durante el inicio
		 * Opciones disponibles:
		 * - Mostrar contrasena: Imprime en el display la contrasena actual
		 * - Cambiar contrasena: El usuario puede cambiar la contrasena
		 * - Guardar contrasena: Guarda la contrasena actual en al SD o EEPROM
		 */
		if (admin) {
			xQueueReceive(xBoton, &opc, 0); // Descarto el primero por HOLD
			i = 0;
			do {
				// Cada 2 segundos rota las opciones disponisbles en pantalla
				lcdClear(); 		// Borrar la pantalla
				lcdGoToXY( 1, 1 ); 	// Poner cursor en 1, 1
				lcdSendStringRaw( "   ADMIN MENU   " );
				lcdGoToXY( 1, 2 ); 	// Poner cursor en 1, 2
				lcdData(i+'0');		// int a ASCII
				lcdSendStringRaw( ". " );
				lcdSendStringRaw( menu_options[i] );
				lcdGoToXY( LCD_Hor+1, 2 ); 	// Esconder cursor
				i++;
				i%=MENU_OPTS;		// Marquecina
			} while  (	!xQueueReceive(xBoton, &opc, MS(2000)) );

			// Sale al precionar un boton, imprime:
			// "> OPCION SELECT <"
			lcdClear(); 		// Borrar la pantalla
			lcdGoToXY( 1, 1 ); 	// Poner cursor en 1, 1
			lcdSendStringRaw( "> " );
			lcdSendStringRaw( menu_options[opc-'0'] );		// ASCII a int
			lcdGoToXY( LCD_Hor, 1 ); 	// Poner cursor en ultima posicion
			lcdSendStringRaw( "<" );
			lcdGoToXY( 1, 2 ); 	// Poner cursor en 1, 2

			switch (opc){
				case ('1'): {	// Show password
					// lcdSendStringRaw( pass );
					// (16-4)x2%2 = [    * * * *     ]
					lcdGoToXY( 1+(( LCD_Hor / 2 ) - MAX_PASS), 2 );
					for ( i = 0; pass[i] ; i++){
						lcdData( pass[i] );
						lcdData(' ');
					}
					xQueueReceive(xBoton, &opc, portMAX_DELAY);	// Any key to exit
				} break;
				case ('2'): {	// Change password - TODO: Pedir pass anterior
					// (16-4)x2%2 = [    * * * *     ]
					lcdGoToXY( 1+(( LCD_Hor / 2 ) - MAX_PASS), 2 );
					for ( i = 0; i < MAX_PASS; i++) {
						xQueueReceive(xBoton, &opc, portMAX_DELAY);
						pass[i] = opc;
						//lcdData(opc);	// TODO DEBUG: Showing pass
						lcdData('*');
						lcdData(' ');
					}
				} break;
				case ('3'): {	// TODO: Save to SD card or EEPROM
					opc = NULL;
				} break;
				case ('0'): {	// Exit admin mode.
					admin = FALSE;
				} break;
				// Botor invalido, vuelve al loop
				default: opc = NULL;
			}
		}

		/* MODO USUARIO:
		 * - Espera que el usuario ingrese por teclado matricial la contrasena
		 * - Verifica que la contrasena sea la correcta
		 * - En caso de ser correcta habilita
		 * - En caso de ser incorrecta bloquea
		 * - Dos segundos despues vuelve a esperar la proxima constasena
		 */
		else {
			lcdClear(); 		// Borrar la pantalla
			lcdGoToXY( 1, 1 ); 	// Poner cursor en 1, 1
			lcdSendStringRaw( "Input Password: " );
			// (16-4)x2%2 = [    * * * *     ]
			lcdGoToXY( 1+(( LCD_Hor / 2 ) - MAX_PASS), 2 );
			for ( i = 0; i < MAX_PASS; i++) {
				xQueueReceive(xBoton, &opc, portMAX_DELAY);
				input[i] = opc;
				//lcdData(opc);	// TODO DEBUG: Show pressed
				lcdData('*');
				lcdData(' ');
			}
			input[i+1] = NULL;

			lcdGoToXY( 1, 1 ); 	// Poner cursor en 1, 1
			gpioWrite( LEDB, OFF );
			if ( strcmp(pass, input) == 0 ) {
				lcdSendStringRaw( " ACCESS GRANTED " );
				gpioWrite( LEDG, ON );
			}
			else {
				lcdSendStringRaw( " ACCESS DENIED! " );
				gpioWrite( LEDR, ON );
				gpioWrite( BUZZER, ON );
			}
			vTaskDelay( MS(2000) );
			gpioWrite( LEDG, OFF );
			gpioWrite( LEDR, OFF );
			gpioWrite( BUZZER, OFF );
			gpioWrite( LEDB, ON );
		}
		vPortYield();
	}
}

/* CONTROLADOR DE TECLAS
 * Detector de flanco decendente
 * Envia los flancos luego de 20ms a la cola
 * Tiene un parametro de entrada: la tecla a iniciar.
 * De esta forma una misma tarea se llama una vez por cada tecla a iniciar.
 */
void vBtn( char btn ) {
	// Init. Config
	char btn_status = 0;	// 0 = Hi, 1 = Timer, 2 = Lo

	// Endless loop
	while(TRUE) {
		// Maquina de estados: detecta flanco de subida
		switch (btn_status) {
			case (0): { // Hi
				if (!gpioRead( btn ))
					btn_status = 1;
			}break;
			case (1): { // Timer
			// Espera que estabilize
				vTaskDelay( MS(25) );
				if ( !gpioRead( btn )) {
					btn -= TEC1;
					btn += 'W';
					// Q=TEC1, R=TEC2, S=TEC3, T=TEC4
					xQueueSend(xBoton, &btn, FALSE);
					btn -= 'W';
					btn += TEC1;
					btn_status = 2;
				}
				else
					btn_status = 0;
			}break;
			case (2): { // Lo
				if ( gpioRead( btn ))
					btn_status = 0;
			}break;
			default: btn_status = 0;
		}
		vPortYield();
	}
}

/* CONTROLADOR DE TECLADO MATRICIAL
 * Por detector de flanco.
 * Envia el char correspondiente luego de 20ms a la cola.
 * Verifica que sea la misma tecla.
 */
void vKeypad( void* taskParmPtr ) {
	// Init. Config
	keypad_t keypad;
	char btn_status = 0;	// 0 = Hi, 1 = Timer, 2 = Lo
	char matrix[ ( MAX_FIL * MAX_FIL )] = {	'1', '2', '3',
						'4', '5', '6',
						'7', '8', '9',
						'*', '0', '#'};
	
	// Filas --> Salidas
	uint8_t keypadRowPins1[MAX_FIL] = {
	T_FIL0,		// Row 0
	T_FIL1,		// Row 1
	T_FIL2,		// Row 2
	T_FIL3		// Row 3
	};

	// Columnas --> Entradas con pull-up (MODO = GPIO_INPUT_PULLUP)
	uint8_t keypadColPins1[MAX_COL] = {
	T_COL0,		// Column 0
	T_COL1,		// Column 1
	T_COL2,		// Column 2
	};

	keypadConfig( &keypad, keypadRowPins1, MAX_FIL, keypadColPins1, MAX_COL );

	uint16_t prev = FALSE;
	uint16_t key = FALSE;
	// Endless loop
	while(TRUE)
	{
		// Maquina de estados: detecta flanco de bajada
		switch (btn_status) {
			case (0): { // Hi
				if ( keypadRead ( &keypad, &prev )) {
					btn_status = 1;
				}
			}break;
			case (1): { // Timer
				// Espera que estabilize
				vTaskDelay( MS(25) );
				if ( keypadRead ( &keypad, &key )) {
					if ( key == prev ){		// Descarta si son teclas diferentes
						xQueueSend(xBoton, &matrix[key], 0);
						btn_status = 2;
					}
					else
						btn_status = 0;
				}
				else
					btn_status = 0;
			}break;
			case (2): { // Lo
				if ( !keypadRead ( &keypad, &prev ))
					btn_status = 0;
			}break;
			default: btn_status = 0;
		}
		vPortYield();
	}
}

// FUNCION PRINCIPAL
int main(void) {
	// Init. hardware
	boardConfig();

	// Led para dar señal de vida
	gpioWrite( LEDB, ON );

	// Inicializa colas
	xBoton = xQueueCreate(BTN_BUFF, sizeof(char));

	// Crear tareas freeRTOS
	xTaskCreate(
		vKeypad,					// pvTaskCode -Funcion de la tarea a ejecutar
		(const char *)"Keypad",		// pcName - Nombre de la tarea para debbug
		Stack(2),					// usStackDeph - Tamaño del stack de la tarea
		(void*)0,					// pvParameters - Parametros de tarea
		Priority(1),				// uxPriority - Prioridad de la tarea
		NULL						// pxCreatedTask - Puntero a la tarea creada en el sistema
	);
	xTaskCreate(
		vBtn,						// pvTaskCode -Funcion de la tarea a ejecutar
		(const char *)"BotonA",		// pcName - Nombre de la tarea para debbug
		Stack(2),					// usStackDeph - Tamaño del stack de la tarea
		(void * const)TEC1,						// pvParameters - Parametros de tarea
		Priority(1),				// uxPriority - Prioridad de la tarea
		NULL						// pxCreatedTask - Puntero a la tarea creada en el sistema
	);
	xTaskCreate(
		vBtn,						// pvTaskCode -Funcion de la tarea a ejecutar
		(const char *)"BotonB",		// pcName - Nombre de la tarea para debbug
		Stack(2),					// usStackDeph - Tamaño del stack de la tarea
		(void * const)TEC2,						// pvParameters - Parametros de tarea
		Priority(1),				// uxPriority - Prioridad de la tarea
		NULL						// pxCreatedTask - Puntero a la tarea creada en el sistema
	);
	xTaskCreate(
		vBtn,						// pvTaskCode -Funcion de la tarea a ejecutar
		(const char *)"BotonC",		// pcName - Nombre de la tarea para debbug
		Stack(2),					// usStackDeph - Tamaño del stack de la tarea
		(void * const)TEC3,						// pvParameters - Parametros de tarea
		Priority(1),				// uxPriority - Prioridad de la tarea
		NULL						// pxCreatedTask - Puntero a la tarea creada en el sistema
	);
	xTaskCreate(
		vBtn,						// pvTaskCode -Funcion de la tarea a ejecutar
		(const char *)"BotonD",		// pcName - Nombre de la tarea para debbug
		Stack(2),					// usStackDeph - Tamaño del stack de la tarea
		(void * const)TEC4,						// pvParameters - Parametros de tarea
		Priority(1),				// uxPriority - Prioridad de la tarea
		NULL						// pxCreatedTask - Puntero a la tarea creada en el sistema
	);
	xTaskCreate(
		vMenu,						// pvTaskCode -Funcion de la tarea a ejecutar
		(const char *)"Menu",		// pcName - Nombre de la tarea para debbug
		Stack(2),					// usStackDeph - Tamaño del stack de la tarea
		(void*)0,					// pvParameters - Parametros de tarea
		Priority(2),				// uxPriority - Prioridad de la tarea
		NULL						// pxCreatedTask - Puntero a la tarea creada en el sistema
	);

	// Iniciar scheduler
	vTaskStartScheduler();

	// Endless loop
	while(TRUE) {}	// ERROR: Si cae en este while 1 significa que no pudo iniciar el scheduler
	return 0;
}

void vApplicationTickHook ( void ){}

void vApplicationIdleHook ( void ){}
