# Código 

## Ejercicios de practica varios.

- [*freeRTOS_01_blinky*](freeRTOS_01_blinky/src/freeRTOS_blinky.c): Desarrollado por [Eric Pernia](https://github.com/epernia) como [“Hola Mundo”](https://es.wikipedia.org/wiki/Hola_mundo) básico de [FreeRTOS](http://www.freertos.org/) usando la [sAPI](https://github.com/epernia/sAPI/releases) para eduCIAA. El resto de los ejercicios usan este código de base.

- [*template*](template/src/main.c): Plantilla básica de desarrollo para los ejercicios. Blinky anterior con algunas modificaciones propias para hacerlo más claro o cómodo.

- [*testing*](testing/src/main.c): Código en desarrollo o fase de pruebas. Evitar su uso, esta en Git para seguimiento y versionado.

- [*01-frec_led*](01-frec_led/src/main.c): LED titilando, al presionar uno de los cuatro pulsadores de la placa cambia la velocidad de frecuencia en la que el LED titila.

- [*02-toggle_led*](02-toggle_led/src/main.c): LED titilando, al presionar uno de los cuatro pulsadores de la placa cambia cual de los LEDs titilan y apaga el resto.

- [*03-count_led*](03-count_led/src/main.c): Cuenta la cantidad de veces que se presiona la tecla 1, luego al presionar la tecla 2 titila un LED la cantidad de veces que se presiono la tecla 1.

- [*04-sem_count*](04-sem_count/src/main.c): Idem. 03 pero el disparo en lugar de hacerse con variable global se hace con un semáforo.

- [*05-sem_n_count*](05-sem_n_count/src/main.c): Idem. 04 pero el contador en lugar de hacerse con una variable global se hace con un semáforo de conteo.

- [*06-lcd*](06-lcd/src/main.c): "Hola, Mundo!" del display LCD.

- [*07-lcd_menu*](07-lcd_menu/src/main.c): Menu basico en LCD detectando las teclas precionadas.

- [*08-keypad*](08-keypad/src/main.c): Menu basico en LCD detectando un Keypad 4x3.

- [*09-access_basic*](09-access_basic/src/main.c): Control de aceso basico con contraseña fija por codigo.

- [*10-keypad_rising*](10-keypad_rising/src/main.c): Deteccion por flanco acendente de un Keypad.

- [*11-access_admin*](11-access_admin/src/main.c): Control de acceso con modo admin donde poder cambiar la password.

- [*12-access_keypad*](12-access_keypad/src/main.c): Similar al anterior, pero con un menu rotativo diseñado para usar desde keypad.
