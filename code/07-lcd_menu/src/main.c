// Includes de FreeRTOS
#include "FreeRTOS.h"
#include "FreeRTOSConfig.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"
#include "sapi.h"

// Definiciones
#define MS(t)					((portTickType) ( (t) / portTICK_RATE_MS))
#define Stack(s)				( configMINIMAL_STACK_SIZE * s )
#define Priority(p)				( tskIDLE_PRIORITY + p )
#define LCD_Hor					16
#define LCD_Ver					2
#define LCD_pixH				5
#define LCD_pixV				8
#define BTN_BUFF				32

// Variables globales
xQueueHandle xBoton = NULL;		// Boton apretado

// Prototipo de funciones de las tareas
void vMyTask( void* taskParmPtr );
void vBtn( int btn );

// FUNCION PRINCIPAL
// Menu basico
int main(void)
{
	// Init. hardware
	boardConfig();

	// Led para dar señal de vida
	gpioWrite( LED3, ON );

	xBoton = xQueueCreate(BTN_BUFF, sizeof(char));
	// Crear tareas freeRTOS
	xTaskCreate(
		vMyTask,					// pvTaskCode -Funcion de la tarea a ejecutar
		(const char *)"MyTask",		// pcName - Nombre de la tarea para debbug
		Stack(2),					// usStackDeph - Tamaño del stack de la tarea
		(void*)0,					// pvParameters - Parametros de tarea
		Priority(2),				// uxPriority - Prioridad de la tarea
		NULL						// pxCreatedTask - Puntero a la tarea creada en el sistema
	);
	xTaskCreate(
		vBtn,						// pvTaskCode -Funcion de la tarea a ejecutar
		(const char *)"Boton",		// pcName - Nombre de la tarea para debbug
		Stack(2),					// usStackDeph - Tamaño del stack de la tarea
		TEC1,						// pvParameters - Parametros de tarea
		Priority(1),				// uxPriority - Prioridad de la tarea
		NULL						// pxCreatedTask - Puntero a la tarea creada en el sistema
	);
	xTaskCreate(
		vBtn,						// pvTaskCode -Funcion de la tarea a ejecutar
		(const char *)"Boton",		// pcName - Nombre de la tarea para debbug
		Stack(2),					// usStackDeph - Tamaño del stack de la tarea
		TEC2,						// pvParameters - Parametros de tarea
		Priority(1),				// uxPriority - Prioridad de la tarea
		NULL						// pxCreatedTask - Puntero a la tarea creada en el sistema
	);
	xTaskCreate(
		vBtn,						// pvTaskCode -Funcion de la tarea a ejecutar
		(const char *)"Boton",		// pcName - Nombre de la tarea para debbug
		Stack(2),					// usStackDeph - Tamaño del stack de la tarea
		TEC3,						// pvParameters - Parametros de tarea
		Priority(1),				// uxPriority - Prioridad de la tarea
		NULL						// pxCreatedTask - Puntero a la tarea creada en el sistema
	);
	xTaskCreate(
		vBtn,						// pvTaskCode -Funcion de la tarea a ejecutar
		(const char *)"Boton",		// pcName - Nombre de la tarea para debbug
		Stack(2),					// usStackDeph - Tamaño del stack de la tarea
		TEC4,						// pvParameters - Parametros de tarea
		Priority(1),				// uxPriority - Prioridad de la tarea
		NULL						// pxCreatedTask - Puntero a la tarea creada en el sistema
	);

	// Iniciar scheduler
	vTaskStartScheduler();

	// Endless loop
	while(TRUE)
	{
	// ERROR: Si cae en este while 1 significa que no pudo iniciar el scheduler
	}
	return 0;
}

// Implementaciones de funciones de las tareas
void vMyTask( void* taskParmPtr )
{
	// Init. Config
	lcdInit( LCD_Hor, LCD_Ver, LCD_pixH, LCD_pixV );
	int opc = FALSE;

	// Endless loop
	while(TRUE)
	{
		xQueueReceive(xBoton, &opc, portMAX_DELAY);
		lcdClear(); 		// Borrar la pantalla
		lcdGoToXY( 1, 1 ); 	// Poner cursor en 1, 1
		switch (opc) {
			case (TEC1):
				lcdSendStringRaw( "Boton A" ); break;
			case (TEC2):
				lcdSendStringRaw( "Boton B" ); break;
			case (TEC3):
				lcdSendStringRaw( "Boton C" ); break;
			case (TEC4):
				lcdSendStringRaw( "Boton D" ); break;
			default:
				lcdSendStringRaw( "WTF?!" );
		}
		vPortYield();
	}
}

void vBtn( int btn )
{
	// Init. Config
	gpioConfig( GPIO0, GPIO_INPUT );

	// Endless loop
	while(TRUE)
	{
		if ( !gpioRead( btn ) )
			xQueueSend(xBoton, &btn, FALSE);
		vPortYield();
	}
}
