// Includes de FreeRTOS
#include "FreeRTOS.h"
#include "FreeRTOSConfig.h"
#include "task.h"
#include "sapi.h"        // <= sAPI header

// Definiciones

// Variables globales
int cont = 0;       // Contador
int btn_status = 0;	// 0 = Lo, 1 = Timer, 2 = Hi
int run = FALSE;    // Disparador

// Prototipo de funciones de las tareas
void vBlinky( void* taskParmPtr );
void vButtonCnt( void* taskParmPtr );
void vButtonRun( void* taskParmPtr );

// FUNCION PRINCIPAL
// Cuenta la cantidad de veces que se presiona la tecla 1, luego al presionar la tecla 2 titila un LED la cantidad de veces que se presiono la tecla 1.
int main(void)
{
	// Init. hardware
	boardConfig();
	gpioWrite( LED3, ON );

	// Crear tareas freeRTOS

	xTaskCreate(
		vButtonCnt,			// pvTaskCode -Funcion de la tarea a ejecutar
		(const char *)"Counter",	// pcName - Nombre de la tarea para debbug
		configMINIMAL_STACK_SIZE*2,	// usStackDeph - Tamaño del stack de la tarea
		(void*)0,			// pvParameters - Parametros de tarea
		tskIDLE_PRIORITY+1,		// uxPriority - Prioridad de la tarea
		NULL				// pxCreatedTask - Puntero a la tarea creada en el sistema
	);

	xTaskCreate(
		vButtonRun,			// pvTaskCode -Funcion de la tarea a ejecutar
		(const char *)"Run",		// pcName - Nombre de la tarea para debbug
		configMINIMAL_STACK_SIZE*2,	// usStackDeph - Tamaño del stack de la tarea
		(void*)0,			// pvParameters - Parametros de tarea
		tskIDLE_PRIORITY+1,		// uxPriority - Prioridad de la tarea
		NULL				// pxCreatedTask - Puntero a la tarea creada en el sistema
	);

	xTaskCreate(
		vBlinky,			// pvTaskCode -Funcion de la tarea a ejecutar
		(const char *)"Blinky",		// pcName - Nombre de la tarea para debbug
		configMINIMAL_STACK_SIZE*2,	// usStackDeph - Tamaño del stack de la tarea
		(void*)0,			// pvParameters - Parametros de tarea
		tskIDLE_PRIORITY+2,		// uxPriority - Prioridad de la tarea
		NULL				// pxCreatedTask - Puntero a la tarea creada en el sistema
	);

	// Iniciar scheduler
	vTaskStartScheduler();

	// Endless loop
	while( TRUE )
	{
	// ERROR: Si cae en este while 1 significa que no pudo iniciar el scheduler
	}
	return 0;
}

// Implementaciones de funciones de las tareas
void vBlinky( void* taskParmPtr )
{
	// Init. Config

	// Endless loop
	while(TRUE)
	{
		if (run) {
			if ( cont > 0 ) {
				// Intercambia el estado del LED
				gpioWrite( LED2 , ON );
				vTaskDelay( 500 / portTICK_RATE_MS );
				gpioWrite( LED2 , OFF );
				cont--;
			}
			else {
				run = FALSE;
			}
		}
		// Envia la tarea al estado bloqueado durante 500ms
		vTaskDelay( 500 / portTICK_RATE_MS );
	}
}

void vButtonCnt( void* taskParmPtr )
{
	// Init. Config
	gpioConfig( GPIO0, GPIO_INPUT );

	// Endless loop
	while(TRUE)
	{
	    // Maquina de estados: detecta flanco de subida
		switch (btn_status) {
			case (0): // Lo
			{
				if (!gpioRead( TEC1 ))
					btn_status = 1;
			}break;
			case (1): // Timer
			{
			    // Espera que estabilize
				vTaskDelay( 25 / portTICK_RATE_MS );
				if ( !gpioRead( TEC1 ))
				{
					cont++;
					btn_status = 2;
				}
				else
					btn_status = 0;
			}break;
			case (2): // Hi
			{
				if ( gpioRead( TEC1 ))
					btn_status = 0;
			}break;
			default:
				btn_status = 0;
		}
	}
}

void vButtonRun( void* taskParmPtr )
{
	// Init. Config
	gpioConfig( GPIO0, GPIO_INPUT );

	// Endless loop
	while(TRUE)
	{
		if ( !gpioRead( TEC2 ) ){
			run = TRUE;
		}
	}
}
