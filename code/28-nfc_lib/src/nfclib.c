#include "nfclib.h"

// Variables globales
char msg_read[NFC_MESSAGE_MAX];
char msg_write[NFC_MESSAGE_MAX];

/* Discovery loop configuration according to the targeted modes of operation */
const unsigned char DiscoveryTechnologies[] = {
    MODE_POLL | TECH_PASSIVE_NFCA,
    MODE_POLL | TECH_PASSIVE_NFCF,
    MODE_POLL | TECH_PASSIVE_NFCB,
    MODE_POLL | TECH_PASSIVE_15693,
};

/*==================[Functions]==============================================*/

int init_nfc(void){

    /* Register callback for reception of NDEF message from remote cards */
    RW_NDEF_RegisterPullCallback(*NdefPull_Cb);

    /* Open connection to NXPNCI device */
    if (NxpNci_Connect() == NFC_ERROR) {
        //printf("Error: cannot connect to NXPNCI device\n");
        return NFC_ERROR;
    }
    if (NxpNci_ConfigureSettings() == NFC_ERROR) {
        //printf("Error: cannot configure NXPNCI settings\n");
    	return NFC_ERROR;
    }

    if (NxpNci_ConfigureMode(0 | NXPNCI_MODE_RW) == NFC_ERROR) {
        //printf("Error: cannot configure NXPNCI\n");
    	return NFC_ERROR;
    }
    // Start Discovery //
    if (NxpNci_StartDiscovery(DiscoveryTechnologies,sizeof(DiscoveryTechnologies)) != NFC_SUCCESS) {
        //printf("Error: cannot start discovery\n");
        return NFC_ERROR;
    }

    return NFC_SUCCESS;
}

int nfc_reader(NxpNci_RfIntf_t RfIntf, char message[]) {

    int retVal;
    /* What's the detected card type ? */
    if (RfIntf.Protocol == PROT_T1T ||
        RfIntf.Protocol == PROT_T2T ||
        RfIntf.Protocol == PROT_T3T ||
        RfIntf.Protocol == PROT_ISODEP ) {
        /* Process NDEF message read */
        NxpNci_ProcessReaderMode(RfIntf, READ_NDEF);
        strcpy(message, msg_read);
        
        retVal = NFC_SUCCESS;
    }
    else{
        message[0] = NULL;
        retVal = NFC_ERROR;
    }

    /* Wait for card removal */
    NxpNci_ProcessReaderMode(RfIntf, PRESENCE_CHECK);

    /* Restart discovery loop */
    NxpNci_StopDiscovery();
    if (NxpNci_StartDiscovery(DiscoveryTechnologies,sizeof(DiscoveryTechnologies)) != NFC_SUCCESS) {
        retVal = NFC_ERROR;
    }

    return retVal;
}

int nfc_writer(NxpNci_RfIntf_t RfIntf, char message[]) {

    int retVal;

	ndef_pack_t NDEF_MESSAGE;
	NDEF_MESSAGE.structure = 0xD1;
	NDEF_MESSAGE.type_length = 0x01;
	NDEF_MESSAGE.type = 'T';
	NDEF_MESSAGE.status = 0x02;
	strcpy(NDEF_MESSAGE.lang, "en");

    strcpy(NDEF_MESSAGE.message, message);
	NDEF_MESSAGE.payload_length = strlen(NDEF_MESSAGE.message) + 3;

	/* What's the detected card type ? */
	if (RfIntf.Protocol == PROT_T1T ||
		RfIntf.Protocol == PROT_T2T ||
		RfIntf.Protocol == PROT_T3T ||
		RfIntf.Protocol == PROT_ISODEP ) {
		/* Process NDEF message read */
		NxpNci_ProcessReaderMode(RfIntf, READ_NDEF);
		RW_NDEF_SetMessage ((unsigned char *) &NDEF_MESSAGE, NDEF_MESSAGE.payload_length + 4, *NdefPush_Cb);
		/* Process NDEF message write */
		NxpNci_ReaderReActivate(&RfIntf);
		NxpNci_ProcessReaderMode(RfIntf, WRITE_NDEF);

        retVal = NFC_SUCCESS;
	}    
    else{
        retVal = NFC_ERROR;
    }

    /* Wait for card removal */
    NxpNci_ProcessReaderMode(RfIntf, PRESENCE_CHECK);
    
    /* Restart discovery loop */
    NxpNci_StopDiscovery();
    if (NxpNci_StartDiscovery(DiscoveryTechnologies,sizeof(DiscoveryTechnologies)) != NFC_SUCCESS) {
        retVal = NFC_ERROR;
    }

    return retVal;
}

void NdefPull_Cb(unsigned char *pNdefMessage, unsigned short NdefMessageSize){
    unsigned char *pNdefRecord = pNdefMessage;
    NdefRecord_t NdefRecord;
    unsigned char save;
    msg_read[0] = NULL;

    if (pNdefMessage == NULL){
        // Provisioned buffer size too small or NDEF message empty
        return;
    }

    while (pNdefRecord != NULL){
        // NDEF record received
        NdefRecord = DetectNdefRecordType(pNdefRecord);

        if(NdefRecord.recordType == WELL_KNOWN_SIMPLE_TEXT){
        	save = NdefRecord.recordPayload[NdefRecord.recordPayloadSize];
        	NdefRecord.recordPayload[NdefRecord.recordPayloadSize] = '\0';
			strcpy(msg_read, &NdefRecord.recordPayload[NdefRecord.recordPayload[0]+1]);
			NdefRecord.recordPayload[NdefRecord.recordPayloadSize] = save;
        }
        pNdefRecord = GetNextRecord(pNdefRecord);
    }
}

void NdefPush_Cb(unsigned char *pNdefRecord, unsigned short NdefRecordSize) {
    // NDEF Record sent
}
