//#define RW_SUPPORT

// Includes de FreeRTOS
#include "FreeRTOS.h"
#include "FreeRTOSConfig.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"
#include "sapi.h"
#include <tool.h>
#include <Nfc.h>
#include <ndef_helper.h>
#include "nfclib.h"

// Definiciones
#define Stack(s)	( configMINIMAL_STACK_SIZE + s )
#define Priority(p)	( tskIDLE_PRIORITY + p )
#define NFC_ADDR	( NXPNCI_I2C_ADDR )	// PN7150. Dato del fabricante
#define NFC_MESSAGE_MAX     ( 32 )

#define TASK_NFC_STACK_SIZE		1024
#define TASK_NFC_STACK_PRIO		(configMAX_PRIORITIES - 1)

#define BTN_BUFF				( 8 )

// Config Display
#define LCD_Hor		( 16 )
#define LCD_Ver		( 2 )
#define LCD_pixH	( 5 )
#define LCD_pixV	( 8 )

#define xMutexTake( xSemaphore, xBlockTime )		xSemaphoreTake( xSemaphore, xBlockTime )
#define xMutexFree( xSemaphore )					xSemaphoreGive( xSemaphore )

// Variables globales
xQueueHandle xNFCmode = NULL;		// Tarjeta NFC acercada
xSemaphoreHandle xLCD = NULL; 		// Mutex del Display

// Prototipo de tareas
void vReadNFC(void);
void vBtn( int btn );

// FUNCION PRINCIPAL
// Cuenta la cantidad de veces que se presiona la tecla 1, luego al presionar la tecla 2 titila un LED la cantidad de veces que se presiono la tecla 1.
int main(void)
{
	// Init. Config

	// Init. hardware
	boardConfig();
	// Inicializar LCD de 16x2 (caracteres x lineas) con cada caracter de 5x2 pixeles
	//lcdInit( 16, 2, 5, 8 );

	//lcdClear(); 		// Borrar la pantalla
	//lcdGoToXY( 1, 1 ); 	// Poner cursor en 1, 1
	//lcdSendStringRaw( "Hola, Mundo!" );

	// Inicializa colas
	xNFCmode = xQueueCreate(BTN_BUFF, sizeof(char));
	xLCD = xSemaphoreCreateMutex();

	// Crear tareas freeRTOS
	xTaskCreate(
		vReadNFC,					// pvTaskCode - Funcion de la tarea a ejecutar
		(const char*) "Read_NFC",	// pcName - Nombre de la tarea para debbug
		TASK_NFC_STACK_SIZE,		// usStackDeph - Tamaño del stack de la tarea
		NULL,						// pvParameters - Parametros de tarea
		TASK_NFC_STACK_PRIO,		// uxPriority - Prioridad de la tarea
		NULL						// pxCreatedTask - Puntero a la tarea creada en el sistema
		);
	xTaskCreate(
		vBtn,						// pvTaskCode - Funcion de la tarea a ejecutar
		(const char *)"BotonA",		// pcName - Nombre de la tarea para debbug
		Stack(2),					// usStackDeph - Tamaño del stack de la tarea
		(void * const)TEC1,			// pvParameters - Parametros de tarea
		Priority(1),				// uxPriority - Prioridad de la tarea
		NULL						// pxCreatedTask - Puntero a la tarea creada en el sistema
	);
	xTaskCreate(
		vBtn,						// pvTaskCode - Funcion de la tarea a ejecutar
		(const char *)"BotonB",		// pcName - Nombre de la tarea para debbug
		Stack(2),					// usStackDeph - Tamaño del stack de la tarea
		(void * const)TEC2,			// pvParameters - Parametros de tarea
		Priority(1),				// uxPriority - Prioridad de la tarea
		NULL						// pxCreatedTask - Puntero a la tarea creada en el sistema
	);
	xTaskCreate(
		vBtn,						// pvTaskCode - Funcion de la tarea a ejecutar
		(const char *)"BotonC",		// pcName - Nombre de la tarea para debbug
		Stack(2),					// usStackDeph - Tamaño del stack de la tarea
		(void * const)TEC3,			// pvParameters - Parametros de tarea
		Priority(1),				// uxPriority - Prioridad de la tarea
		NULL						// pxCreatedTask - Puntero a la tarea creada en el sistema
	);
	xTaskCreate(
		vBtn,						// pvTaskCode - Funcion de la tarea a ejecutar
		(const char *)"BotonD",		// pcName - Nombre de la tarea para debbug
		Stack(2),					// usStackDeph - Tamaño del stack de la tarea
		(void * const)TEC4,			// pvParameters - Parametros de tarea
		Priority(1),				// uxPriority - Prioridad de la tarea
		NULL						// pxCreatedTask - Puntero a la tarea creada en el sistema
	);
	// Iniciar scheduler
	vTaskStartScheduler();

	// Endless loop
	while( TRUE )
	{
	// ERROR: Si cae en este while 1 significa que no pudo iniciar el scheduler
	}
	return 0;
}

/* CONTROLADOR DE TECLAS
 * Detector de flanco decendente
 * Envia los flancos luego de 20ms a la cola
 * Tiene un parametro de entrada: la tecla a iniciar.
 * De esta forma una misma tarea se llama una vez por cada tecla a iniciar.
 */
void vBtn( int btn ) {
	// Init. Config
	char btn_status = 0;	// 0 = Hi, 1 = Timer, 2 = Lo

	// Endless loop
	while(TRUE) {
		// Maquina de estados: detecta flanco de subida
		switch (btn_status) {
			case (0): { // Hi
				if (!gpioRead( btn ))
					btn_status = 1;
			}break;
			case (1): { // Timer
			// Espera que estabilize
				vTaskDelay( MS(25) );
				if ( !gpioRead( btn )) {
					btn -= TEC1;
					btn += 'W';
					// W=TEC1, X=TEC2, Y=TEC3, Z=TEC4
					xQueueSend(xNFCmode, &btn, FALSE);	// DEBUG: Uso boton mientras no tengo NFC
					btn -= 'W';
					btn += TEC1;
					btn_status = 2;
				}
				else
					btn_status = 0;
			}break;
			case (2): { // Lo
				if ( gpioRead( btn ))
					btn_status = 0;
			}break;
			default: btn_status = 0;
		}
		vPortYield();
	}
}

void vReadNFC(void){
    NxpNci_RfIntf_t RfInterface;
    bool_t write = FALSE;
    int opc = NULL;
    char message[NFC_MESSAGE_MAX];

	lcdInit( LCD_Hor, LCD_Ver, LCD_pixH, LCD_pixV );

    if(init_nfc() == NFC_SUCCESS){
		xMutexTake(xLCD, portMAX_DELAY);
		lcdClear(); 		// Borrar la pantalla
		lcdGoToXY( 1, 1 ); 	// Poner cursor en 1, 1
		lcdSendStringRaw( " NFC started " );
		xMutexFree(xLCD);
        //printf("Error: cannot start NFC\n");
    }
    else{
    	xMutexTake(xLCD, portMAX_DELAY);
		lcdClear(); 		// Borrar la pantalla
		lcdGoToXY( 1, 1 ); 	// Poner cursor en 1, 1
		lcdSendStringRaw( " NFC failed " );
		xMutexFree(xLCD);
    }

    while (xQueueReceive(xNFCmode, &opc, FALSE));	// Descarta buffer previo del teclado

    while(TRUE) {

        //printf("\nWAITING FOR DEVICE DISCOVERY\n");
        /* Wait until a peer is discovered */
        while(NxpNci_WaitForDiscoveryNotification(&RfInterface) != NFC_SUCCESS);

        xQueueReceive(xNFCmode, &opc, FALSE);
		if (opc == 'W')
			write = TRUE;
		else
			write = FALSE;

		if ((RfInterface.ModeTech & MODE_MASK) == MODE_POLL) {
        	if(write){
        		strcpy(message, "el coso!");
        		opc = nfc_writer(RfInterface, message);
        	}
        	else{
				opc = nfc_reader(RfInterface, message);
        	}
			xMutexTake(xLCD, portMAX_DELAY);
			lcdClear(); 		// Borrar la pantalla
			lcdGoToXY( 1, 1 ); 	// Poner cursor en 1, 1
        	if (opc == NFC_SUCCESS){
				if(write)
					lcdSendStringRaw( " NFC writen: " );
				else
					lcdSendStringRaw( " NFC readed: " );
				lcdGoToXY( 1, 2 ); 	// Poner cursor en 1, 1
				lcdSendStringRaw( message );
        	}
        	else{
        		if(write)
					lcdSendStringRaw( " NFC write " );
				else
					lcdSendStringRaw( " NFC read " );
				lcdGoToXY( 1, 2 ); 	// Poner cursor en 1, 1
				lcdSendStringRaw( "     FAILED     " );
        	}
			xMutexFree(xLCD);
        }
        else {
            //printf("WRONG DISCOVERY\n");
        }
    }
}

/*==================[Functions]==============================================*/

void vApplicationTickHook ( void ){}

void vApplicationIdleHook ( void ){}
