//#define CARDEMU_SUPPORT
//#define P2P_SUPPORT
#define RW_SUPPORT

#define RW_NDEF_WRITING
//#define RW_RAW_EXCHANGE
//#define CARDEMU_RAW_EXCHANGE
//#define NO_NDEF_SUPPORT

// Includes de FreeRTOS
#include "FreeRTOS.h"
#include "FreeRTOSConfig.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"
#include "sapi.h"
#include <nfc_task.h>

// Definiciones
#define Stack(s)	( configMINIMAL_STACK_SIZE + s )
#define Priority(p)	( tskIDLE_PRIORITY + p )
//#define NFC_ADDR	( PN532_I2C_ADDRESS )	// PN532. Dato del fabricante
#define NFC_ADDR	( NXPNCI_I2C_ADDR )	// PN7150. Dato del fabricante
#define NFC_BUFF1	( 32 )
#define NFC_BUFF2	( 32 )
#define TASK_NFC_STACK_SIZE		1024
#define TASK_NFC_STACK_PRIO		(configMAX_PRIORITIES - 1)
// FUNCION PRINCIPAL
// Cuenta la cantidad de veces que se presiona la tecla 1, luego al presionar la tecla 2 titila un LED la cantidad de veces que se presiono la tecla 1.
int main(void)
{
	// Init. Config

	// Init. hardware
	boardConfig();
	// Inicializar LCD de 16x2 (caracteres x lineas) con cada caracter de 5x2 pixeles
	//lcdInit( 16, 2, 5, 8 );

	//lcdClear(); 		// Borrar la pantalla
	//lcdGoToXY( 1, 1 ); 	// Poner cursor en 1, 1
	//lcdSendStringRaw( "Hola, Mundo!" );

	// Crear tareas freeRTOS
	xTaskCreate((TaskFunction_t) task_nfc,
	    				(const char*) "NFC_task",
						TASK_NFC_STACK_SIZE,
						NULL,
						TASK_NFC_STACK_PRIO,
						NULL);

	// Iniciar scheduler
	vTaskStartScheduler();

	// Endless loop
	while( TRUE )
	{
	// ERROR: Si cae en este while 1 significa que no pudo iniciar el scheduler
	}
	return 0;
}


