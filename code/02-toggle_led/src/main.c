// Includes de FreeRTOS
#include "FreeRTOS.h"
#include "FreeRTOSConfig.h"
#include "task.h"
#include "sapi.h"        // <= sAPI header

// Definiciones

// Variables globales
int led_work = LEDB;        // Selecciona el LED de trabajo

// Prototipo de funciones de las tareas
void vBlinky( void* taskParmPtr );
void vButtons( void* taskParmPtr );

// FUNCION PRINCIPAL
// LED titilando, al presionar uno de los cuatro pulsadores de la placa cambia cual de los LEDs titilan y apaga el resto.
int main(void)
{
	// Init. hardware
	boardConfig();

	// Crear tareas freeRTOS

	xTaskCreate(
		vButtons,			// pvTaskCode -Funcion de la tarea a ejecutar
		(const char *)"Buttons",	// pcName - Nombre de la tarea para debbug
		configMINIMAL_STACK_SIZE*2,	// usStackDeph - Tamaño del stack de la tarea
		(void*)0,			// pvParameters - Parametros de tarea
		tskIDLE_PRIORITY+1,		// uxPriority - Prioridad de la tarea
		NULL				// pxCreatedTask - Puntero a la tarea creada en el sistema
	);

	xTaskCreate(
		vBlinky,			// pvTaskCode -Funcion de la tarea a ejecutar
		(const char *)"Blinky",		// pcName - Nombre de la tarea para debbug
		configMINIMAL_STACK_SIZE*2,	// usStackDeph - Tamaño del stack de la tarea
		(void*)0,			// pvParameters - Parametros de tarea
		tskIDLE_PRIORITY+2,		// uxPriority - Prioridad de la tarea
		NULL				// pxCreatedTask - Puntero a la tarea creada en el sistema
	);

	// Iniciar scheduler
	vTaskStartScheduler();

	// Endless loop
	while( TRUE )
	{
	// ERROR: Si cae en este while 1 significa que no pudo iniciar el scheduler
   }
   return 0;
}

// Implementaciones de funciones de las tareas
void vBlinky( void* taskParmPtr )
{
	// Init. Config

	// Endless loop
	while(TRUE)
	{
		// Intercambia el estado del LED
		gpioToggle( led_work );

		// Envia la tarea al estado bloqueado durante 500ms
		vTaskDelay( 500 / portTICK_RATE_MS );
	}
}

void vButtons( void* taskParmPtr )
{
	// Init. Config
	gpioConfig( GPIO0, GPIO_INPUT );

	// Endless loop
	while(TRUE)
	{
	    // Detecta que se preciona una tecla
		if ( !gpioRead( TEC1 ) ){
		    // Selecciona el LED de trabajo
			led_work = LEDB;
			// Apaga el resto
			gpioWrite( LED1 , OFF );
			gpioWrite( LED2 , OFF );
			gpioWrite( LED3 , OFF );
		}
		if ( !gpioRead( TEC2 ) ){
			led_work = LED1;
			gpioWrite( LEDB , OFF );
			gpioWrite( LED2 , OFF );
			gpioWrite( LED3 , OFF );
		}
		if ( !gpioRead( TEC3 ) ){
			led_work = LED2;
			gpioWrite( LEDB , OFF );
			gpioWrite( LED1 , OFF );
			gpioWrite( LED3 , OFF );
		}
		if ( !gpioRead( TEC4 ) ){
			led_work = LED3;
			gpioWrite( LEDB , OFF );
			gpioWrite( LED1 , OFF );
			gpioWrite( LED2 , OFF );
		}
	}
}
