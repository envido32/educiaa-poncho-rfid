// Includes de FreeRTOS
#include "FreeRTOS.h"
#include "FreeRTOSConfig.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"
#include "sapi.h"

// Definiciones
#define MS(t)		((portTickType) ( (t) / portTICK_RATE_MS))
#define Stack(s)	( configMINIMAL_STACK_SIZE + s )
#define Priority(p)	( tskIDLE_PRIORITY + p )
#define BUZZER		( GPIO8 )
#define BTN_BUFF	( 32 )

// Variables globales
xQueueHandle xBoton = NULL;			// Boton apretado

// Prototipo de funciones de las tareas
void vBuzzer( void* taskParmPtr );
void vBtn( char btn );

// FUNCION PRINCIPAL
// Cuenta la cantidad de veces que se presiona la tecla 1, luego al presionar la tecla 2 titila un LED la cantidad de veces que se presiono la tecla 1.
int main(void)
{
	// Init. hardware
	boardConfig();
	xBoton = xQueueCreate(BTN_BUFF, sizeof(char));

	// Crear tareas freeRTOS

	xTaskCreate(
		vBtn,						// pvTaskCode -Funcion de la tarea a ejecutar
		(const char *)"BotonA",		// pcName - Nombre de la tarea para debbug
		Stack(2),					// usStackDeph - Tamaño del stack de la tarea
		(void * const)TEC1,						// pvParameters - Parametros de tarea
		Priority(1),				// uxPriority - Prioridad de la tarea
		NULL						// pxCreatedTask - Puntero a la tarea creada en el sistema
	);
	xTaskCreate(
		vBtn,						// pvTaskCode -Funcion de la tarea a ejecutar
		(const char *)"BotonB",		// pcName - Nombre de la tarea para debbug
		Stack(2),					// usStackDeph - Tamaño del stack de la tarea
		(void * const)TEC2,						// pvParameters - Parametros de tarea
		Priority(1),				// uxPriority - Prioridad de la tarea
		NULL						// pxCreatedTask - Puntero a la tarea creada en el sistema
	);
	xTaskCreate(
		vBtn,						// pvTaskCode -Funcion de la tarea a ejecutar
		(const char *)"BotonC",		// pcName - Nombre de la tarea para debbug
		Stack(2),					// usStackDeph - Tamaño del stack de la tarea
		(void * const)TEC3,						// pvParameters - Parametros de tarea
		Priority(1),				// uxPriority - Prioridad de la tarea
		NULL						// pxCreatedTask - Puntero a la tarea creada en el sistema
	);
	xTaskCreate(
		vBtn,						// pvTaskCode -Funcion de la tarea a ejecutar
		(const char *)"BotonD",		// pcName - Nombre de la tarea para debbug
		Stack(2),					// usStackDeph - Tamaño del stack de la tarea
		(void * const)TEC4,						// pvParameters - Parametros de tarea
		Priority(1),				// uxPriority - Prioridad de la tarea
		NULL						// pxCreatedTask - Puntero a la tarea creada en el sistema
	);
	xTaskCreate(
		vBuzzer,					// pvTaskCode -Funcion de la tarea a ejecutar
		(const char *)"Buzzer",		// pcName - Nombre de la tarea para debbug
		Stack(2),					// usStackDeph - Tamaño del stack de la tarea
		(void*)0,					// pvParameters - Parametros de tarea
		Priority(1),				// uxPriority - Prioridad de la tarea
		NULL						// pxCreatedTask - Puntero a la tarea creada en el sistema
	);
	// Iniciar scheduler
	vTaskStartScheduler();

	// Endless loop
	while( TRUE )
	{
	// ERROR: Si cae en este while 1 significa que no pudo iniciar el scheduler
	}
	return 0;
}

// Implementaciones de funciones de las tareas
void vBuzzer( void* taskParmPtr )
{
	// Init. Config
	int i = 0;
	int timer = 2000;
	char opc = FALSE;
	gpioConfig( BUZZER, GPIO_OUTPUT );

	// Endless loop
	while(TRUE)
	{	    
	    // Suspende la tarea hasta recibir el semaforo
		xQueueReceive(xBoton, &opc, portMAX_DELAY);
			switch(opc){
			case(TEC1):{
				timer = 10;
				gpioWrite( LEDB , ON );
			}break;
			case(TEC2):{
				timer = 20;
				gpioWrite( LED1 , ON );

			}break;
			case(TEC3):{
				timer = 40;
				gpioWrite( LED2 , ON );

			}break;
			case(TEC4):{
				timer = 80;
				gpioWrite( LED3 , ON );

			}break;
			default:{
				timer = 2000;
			}
		}

		for (i = 0 ; i < (2000 / timer); i++){
			// Intercambia el estado del LED
			gpioWrite( BUZZER , ON );
			vTaskDelay( MS(timer) );
			gpioWrite( BUZZER , OFF );
			vTaskDelay( MS(timer) );
		}
		gpioWrite( LEDB , OFF );
		gpioWrite( LED1 , OFF );
		gpioWrite( LED2 , OFF );
		gpioWrite( LED3 , OFF );
		vPortYield();
	}
}


/* CONTROLADOR DE TECLAS
 * Detector de flanco decendente
 * Envia los flancos luego de 20ms a la cola
 * Tiene un parametro de entrada: la tecla a iniciar.
 * De esta forma una misma tarea se llama una vez por cada tecla a iniciar.
 */
void vBtn( char btn ) {
	// Init. Config
	char btn_status = 0;	// 0 = Hi, 1 = Timer, 2 = Lo

	// Endless loop
	while(TRUE) {
		// Maquina de estados: detecta flanco de subida
		switch (btn_status) {
			case (0): { // Hi
				if (!gpioRead( btn ))
					btn_status = 1;
			}break;
			case (1): { // Timer
			// Espera que estabilize
				vTaskDelay( MS(25) );
				if ( !gpioRead( btn )) {
					xQueueSend(xBoton, &btn, FALSE);
					btn_status = 2;
				}
				else
					btn_status = 0;
			}break;
			case (2): { // Lo
				if ( gpioRead( btn ))
					btn_status = 0;
			}break;
			default: btn_status = 0;
		}
		vPortYield();
	}
}
