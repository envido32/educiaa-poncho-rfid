// Includes de FreeRTOS
#include "FreeRTOS.h"
#include "FreeRTOSConfig.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"
#include "sapi.h"

// Definiciones
#define MS(t)		((portTickType) ( (t) / portTICK_RATE_MS))
#define Stack(s)	( configMINIMAL_STACK_SIZE * s )
#define Priority(p)	( tskIDLE_PRIORITY + p )

// Variables globales
xSemaphoreHandle xRun = NULL;	// Semaforo disparador

// Prototipo de funciones de las tareas
void vBlinky( void* taskParmPtr );

// FUNCION PRINCIPAL
// LED titilando, al presionar uno de los cuatro pulsadores de la placa cambia cual de los LEDs titilan y apaga el resto.
int main(void)
{
	// Init. hardware
	boardConfig();

	// Test Init IRQs
	gpioPinInt( 0, TEC1);
	gpioPinInt( 1, TEC2);
	gpioPinInt( 2, TEC3);
	gpioPinInt( 3, TEC4);

	vSemaphoreCreateBinary(xRun);
	// Crear tareas freeRTOS

	xTaskCreate(
		vBlinky,					// pvTaskCode -Funcion de la tarea a ejecutar
		(const char *)"Blinky",		// pcName - Nombre de la tarea para debbug
		Stack(2),					// usStackDeph - Tamaño del stack de la tarea
		(void*)0,					// pvParameters - Parametros de tarea
		Priority(2),				// uxPriority - Prioridad de la tarea
		NULL						// pxCreatedTask - Puntero a la tarea creada en el sistema
	);

	// Iniciar scheduler
	vTaskStartScheduler();

	// Endless loop
	while( TRUE )
	{
	// ERROR: Si cae en este while 1 significa que no pudo iniciar el scheduler
	}
    return 0;
}

// Implementaciones de funciones de las tareas
void vBlinky( void* taskParmPtr )
{
	// Init. Config
	int led_work = LED1;

	// Endless loop
	while(TRUE)
	{
		// Intercambia el estado del LED
		gpioToggle( led_work );

		// Envia la tarea al estado bloqueado durante 500ms
		vTaskDelay( MS(500) );
	}
}


void GPIO0_IRQHandler(void)
{
	volatile int i = 0;
	i++;
	if (gpioGetInt(0))
	{
	    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
	    gpioClearInt(0);
		xSemaphoreGiveFromISR(xRun, &xHigherPriorityTaskWoken);
		portEND_SWITCHING_ISR(xHigherPriorityTaskWoken);
	}
}

void GPIO1_IRQHandler(void)
{
	volatile int i = 0;
	i++;
	if (gpioGetInt(1))
	{
	    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
	    gpioClearInt(1);
		xSemaphoreGiveFromISR(xRun, &xHigherPriorityTaskWoken);
		portEND_SWITCHING_ISR(xHigherPriorityTaskWoken);
	}
}

void GPIO2_IRQHandler(void)
{
	volatile int i = 0;
	i++;
	if (gpioGetInt(2))
	{
	    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
	    gpioClearInt(2);
		xSemaphoreGiveFromISR(xRun, &xHigherPriorityTaskWoken);
		portEND_SWITCHING_ISR(xHigherPriorityTaskWoken);
	}
}

void GPIO3_IRQHandler(void)
{
	volatile int i = 0;
	i++;
	if (gpioGetInt(3))
	{
	    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
	    gpioClearInt(3);
		xSemaphoreGiveFromISR(xRun, &xHigherPriorityTaskWoken);
		portEND_SWITCHING_ISR(xHigherPriorityTaskWoken);
	}
}
