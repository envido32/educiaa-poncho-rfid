/* Copyright 2018, Manuel Cajide Maidana.
 * All rights reserved.
 *
 * This file is part sAPI library for microcontrollers.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _USERSLIB_H_
#define _USERSLIB_H_

/*==================[defines]================================================*/

#define MAX_PASS	( 4 )
#define MAX_DB		( 32 )

/*==================[inclusions]=============================================*/

#include "sapi_datatypes.h"

/*==================[typedef]================================================*/

typedef struct{
   unsigned int id;
   char pass[MAX_PASS+1];
   bool_t admin;
} user_t;

typedef union{
	user_t data;
	char eeprom[MAX_PASS+6];
} user_eeprom_t;

/*==================[external functions declaration]=========================*/

void userInit( user_t* users_db );
int userSearch( user_t* users_db, unsigned int user_id );
int userAdd( user_t* users_db, user_t new_user );
int userDel( user_t* users_db, unsigned int user_id );
void userSort( user_t* users_db );

/*==================[end of file]============================================*/
#endif /* #ifndef _USERSLIB_H_ */

