#define RW_SUPPORT

// Includes de FreeRTOS
#include "FreeRTOS.h"
#include "FreeRTOSConfig.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"
#include "sapi.h"
#include <tool.h>
#include <Nfc.h>
#include <ndef_helper.h>

// Definiciones
#define Stack(s)				( configMINIMAL_STACK_SIZE + s )
#define Priority(p)				( tskIDLE_PRIORITY + p )
#define NFC_ADDR				( NXPNCI_I2C_ADDR )	// PN7150. Dato del fabricante
#define NFC_BUFF1				( 32 )
#define NFC_BUFF2				( 32 )
#define BTN_BUFF				( 32 )

#define TASK_NFC_STACK_SIZE		320 //1024
#define TASK_NFC_STACK_PRIO		(configMAX_PRIORITIES - 1)

// Config Display
#define LCD_Hor		( 16 )
#define LCD_Ver		( 2 )
#define LCD_pixH	( 5 )
#define LCD_pixV	( 8 )

#define xMutexTake( xSemaphore, xBlockTime )		xSemaphoreTake( xSemaphore, xBlockTime )
#define xMutexFree( xSemaphore )					xSemaphoreGive( xSemaphore )

// Variables globales
xQueueHandle xNFCmode = NULL;		// Tarjeta NFC acercada
xSemaphoreHandle xLCD = NULL; 		// Mutex del Display

/* Discovery loop configuration according to the targeted modes of operation */
unsigned char DiscoveryTechnologies[] = {
    MODE_POLL | TECH_PASSIVE_NFCA,
    MODE_POLL | TECH_PASSIVE_NFCF,
    MODE_POLL | TECH_PASSIVE_NFCB,
    MODE_POLL | TECH_PASSIVE_15693,
};

// Prototipo de funciones
void NdefPull_Cb(unsigned char *pNdefMessage, unsigned short NdefMessageSize);
void NdefPush_Cb(unsigned char *pNdefRecord, unsigned short NdefRecordSize);
int init_nfc(void);
void nfc_reader(NxpNci_RfIntf_t RfIntf, bool_t);

// Prototipo de tareas
void vReadNFC(void);
void vBtn( int btn );

// FUNCION PRINCIPAL
// Cuenta la cantidad de veces que se presiona la tecla 1, luego al presionar la tecla 2 titila un LED la cantidad de veces que se presiono la tecla 1.
int main(void)
{
	// Init. Config

	// Init. hardware
	boardConfig();
	// Inicializar LCD de 16x2 (caracteres x lineas) con cada caracter de 5x2 pixeles
	//lcdInit( 16, 2, 5, 8 );

	//lcdClear(); 		// Borrar la pantalla
	//lcdGoToXY( 1, 1 ); 	// Poner cursor en 1, 1
	//lcdSendStringRaw( "Hola, Mundo!" );

	// Inicializa colas
	xNFCmode = xQueueCreate(BTN_BUFF, sizeof(char));

	xLCD = xSemaphoreCreateMutex();

	// Crear tareas freeRTOS

	xTaskCreate(
		vBtn,						// pvTaskCode - Funcion de la tarea a ejecutar
		(const char *)"BotonA",		// pcName - Nombre de la tarea para debbug
		Stack(2),					// usStackDeph - Tamaño del stack de la tarea
		(void * const)TEC1,			// pvParameters - Parametros de tarea
		Priority(1),				// uxPriority - Prioridad de la tarea
		NULL						// pxCreatedTask - Puntero a la tarea creada en el sistema
	);
	xTaskCreate(
		vBtn,						// pvTaskCode - Funcion de la tarea a ejecutar
		(const char *)"BotonB",		// pcName - Nombre de la tarea para debbug
		Stack(2),					// usStackDeph - Tamaño del stack de la tarea
		(void * const)TEC2,			// pvParameters - Parametros de tarea
		Priority(1),				// uxPriority - Prioridad de la tarea
		NULL						// pxCreatedTask - Puntero a la tarea creada en el sistema
	);
	xTaskCreate(
		vBtn,						// pvTaskCode - Funcion de la tarea a ejecutar
		(const char *)"BotonC",		// pcName - Nombre de la tarea para debbug
		Stack(2),					// usStackDeph - Tamaño del stack de la tarea
		(void * const)TEC3,			// pvParameters - Parametros de tarea
		Priority(1),				// uxPriority - Prioridad de la tarea
		NULL						// pxCreatedTask - Puntero a la tarea creada en el sistema
	);
	xTaskCreate(
		vBtn,						// pvTaskCode - Funcion de la tarea a ejecutar
		(const char *)"BotonD",		// pcName - Nombre de la tarea para debbug
		Stack(2),					// usStackDeph - Tamaño del stack de la tarea
		(void * const)TEC4,			// pvParameters - Parametros de tarea
		Priority(1),				// uxPriority - Prioridad de la tarea
		NULL						// pxCreatedTask - Puntero a la tarea creada en el sistema
	);
	xTaskCreate(
		vReadNFC,					// pvTaskCode - Funcion de la tarea a ejecutar
		(const char*) "Read_NFC",	// pcName - Nombre de la tarea para debbug
		TASK_NFC_STACK_SIZE,		// usStackDeph - Tamaño del stack de la tarea
		NULL,						// pvParameters - Parametros de tarea
		TASK_NFC_STACK_PRIO,		// uxPriority - Prioridad de la tarea
		NULL						// pxCreatedTask - Puntero a la tarea creada en el sistema
		);

	// Iniciar scheduler
	vTaskStartScheduler();

	// Endless loop
	while( TRUE )
	{
	// ERROR: Si cae en este while 1 significa que no pudo iniciar el scheduler
	}
	return 0;
}

void vReadNFC(void){

	lcdInit( LCD_Hor, LCD_Ver, LCD_pixH, LCD_pixV );

    NxpNci_RfIntf_t RfInterface;
    int write = FALSE;
    int aux;

    if(init_nfc()){
        //printf("Error: cannot start NFC\n");
    }

	xMutexTake(xLCD, portMAX_DELAY);
	lcdClear(); 		// Borrar la pantalla
	lcdGoToXY( 1, 1 ); 	// Poner cursor en 1, 1
	lcdSendStringRaw( " NFC iniciada " );
	xMutexFree(xLCD);

	while (xQueueReceive(xNFCmode, &write, FALSE));	// Descarta buffer previo del teclado

    while(TRUE) {

		xQueueReceive(xNFCmode, &write, portMAX_DELAY);
		if (write == 'W')
			write = TRUE;
		else
			write = FALSE;

	    /* Start Discovery */
		//NxpNci_StopDiscovery();
	    //aux = NxpNci_StartDiscovery(DiscoveryTechnologies,sizeof(DiscoveryTechnologies));

		//printf("\nWAITING FOR DEVICE DISCOVERY\n");
		/* Wait until a peer is discovered */
		while(NxpNci_WaitForDiscoveryNotification(&RfInterface) != NFC_SUCCESS);

		if ((RfInterface.ModeTech & MODE_MASK) == MODE_POLL) {
			nfc_reader(RfInterface, write);
		}
		//NxpNci_StopDiscovery();
    }
}

/* CONTROLADOR DE TECLAS
 * Detector de flanco decendente
 * Envia los flancos luego de 20ms a la cola
 * Tiene un parametro de entrada: la tecla a iniciar.
 * De esta forma una misma tarea se llama una vez por cada tecla a iniciar.
 */
void vBtn( int btn ) {
	// Init. Config
	char btn_status = 0;	// 0 = Hi, 1 = Timer, 2 = Lo

	// Endless loop
	while(TRUE) {
		// Maquina de estados: detecta flanco de subida
		switch (btn_status) {
			case (0): { // Hi
				if (!gpioRead( btn ))
					btn_status = 1;
			}break;
			case (1): { // Timer
			// Espera que estabilize
				vTaskDelay( MS(25) );
				if ( !gpioRead( btn )) {
					btn -= TEC1;
					btn += 'W';
					// W=TEC1, X=TEC2, Y=TEC3, Z=TEC4
					xQueueSend(xNFCmode, &btn, FALSE);	// DEBUG: Uso boton mientras no tengo NFC
					btn -= 'W';
					btn += TEC1;
					btn_status = 2;
				}
				else
					btn_status = 0;
			}break;
			case (2): { // Lo
				if ( gpioRead( btn ))
					btn_status = 0;
			}break;
			default: btn_status = 0;
		}
		vPortYield();
	}
}

/*==================[Functions]==============================================*/

int init_nfc(void){

    /* Mode configuration according to the targeted modes of operation */
    unsigned mode = 0 | NXPNCI_MODE_RW;

    /* Register callback for reception of NDEF message from remote cards */
    RW_NDEF_RegisterPullCallback(*NdefPull_Cb);

    /* Open connection to NXPNCI device */
    if (NxpNci_Connect() == NFC_ERROR) {
        //printf("Error: cannot connect to NXPNCI device\n");
        return NFC_ERROR;
    }
    if (NxpNci_ConfigureSettings() == NFC_ERROR) {
        //printf("Error: cannot configure NXPNCI settings\n");
    	return NFC_ERROR;
    }
    if (NxpNci_ConfigureMode(mode) == NFC_ERROR) {
        //printf("Error: cannot configure NXPNCI\n");
    	return NFC_ERROR;
    }
    /* Start Discovery */
    if (NxpNci_StartDiscovery(DiscoveryTechnologies,sizeof(DiscoveryTechnologies)) != NFC_SUCCESS) {
        //printf("Error: cannot start discovery\n");
        return NFC_ERROR;
    }
//    NxpNci_StopDiscovery();
    return NFC_SUCCESS;
}

void nfc_reader(NxpNci_RfIntf_t RfIntf, bool_t write) {
    /* For each discovered cards */

	ndef_pack_t NDEF_MESSAGE;
	NDEF_MESSAGE.structure = 0xD1;
	NDEF_MESSAGE.type_length = 0x01;
	NDEF_MESSAGE.type = 'T';
	NDEF_MESSAGE.status = 0x02;
	strcpy(NDEF_MESSAGE.lang, "en");

	strcpy(NDEF_MESSAGE.message, "qnkte!");
	NDEF_MESSAGE.payload_length = strlen(NDEF_MESSAGE.message) + 3;

	/* What's the detected card type ? */
	if (RfIntf.Protocol == PROT_T1T ||
		RfIntf.Protocol == PROT_T2T ||
		RfIntf.Protocol == PROT_T3T ||
		RfIntf.Protocol == PROT_ISODEP ) {
		/* Process NDEF message read */
		NxpNci_ProcessReaderMode(RfIntf, READ_NDEF);
		if (write){
			RW_NDEF_SetMessage ((unsigned char *) &NDEF_MESSAGE, NDEF_MESSAGE.payload_length + 4, *NdefPush_Cb);
			/* Process NDEF message write */
			NxpNci_ReaderReActivate(&RfIntf);
			NxpNci_ProcessReaderMode(RfIntf, WRITE_NDEF);
		}
	}

    /* Wait for card removal */
    NxpNci_ProcessReaderMode(RfIntf, PRESENCE_CHECK);

    //printf("CARD REMOVED\n");

    /* Restart discovery loop */
    NxpNci_StopDiscovery();
    NxpNci_StartDiscovery(DiscoveryTechnologies,sizeof(DiscoveryTechnologies));
}

void NdefPull_Cb(unsigned char *pNdefMessage, unsigned short NdefMessageSize){
    unsigned char *pNdefRecord = pNdefMessage;
    NdefRecord_t NdefRecord;
    unsigned char save;
    char message[NFC_MESSAGE_MAX];

    if (pNdefMessage == NULL){
        //printf("--- Provisioned buffer size too small or NDEF message empty \n");
        return;
    }

    while (pNdefRecord != NULL){
        //printf("--- NDEF record received:\n");
        NdefRecord = DetectNdefRecordType(pNdefRecord);

        if(NdefRecord.recordType == WELL_KNOWN_SIMPLE_TEXT){
        	save = NdefRecord.recordPayload[NdefRecord.recordPayloadSize];
        	NdefRecord.recordPayload[NdefRecord.recordPayloadSize] = '\0';
			//printf("   Text record: %s\n", &NdefRecord.recordPayload[NdefRecord.recordPayload[0]+1]);
			strcpy(message, &NdefRecord.recordPayload[NdefRecord.recordPayload[0]+1]);
			NdefRecord.recordPayload[NdefRecord.recordPayloadSize] = save;
        }
        pNdefRecord = GetNextRecord(pNdefRecord);
    }

	xMutexTake(xLCD, portMAX_DELAY);
	lcdClear(); 		// Borrar la pantalla
	lcdGoToXY( 1, 1 ); 	// Poner cursor en 1, 1
	lcdSendStringRaw( " NFC leida " );
	lcdGoToXY( 1, 2 ); 	// Poner cursor en 1, 1

	lcdSendStringRaw( message );
	xMutexFree(xLCD);
}

void NdefPush_Cb(unsigned char *pNdefRecord, unsigned short NdefRecordSize) {
    //printf("--- NDEF Record sent\n\n");
}

void vApplicationTickHook ( void ){}

void vApplicationIdleHook ( void ){}
