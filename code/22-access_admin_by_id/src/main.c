// Includes de FreeRTOS
#include "FreeRTOS.h"
#include "FreeRTOSConfig.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"
#include "sapi.h"
#include "string.h"

// Definiciones
#define Stack(s)	( configMINIMAL_STACK_SIZE * s )
#define Priority(p)	( tskIDLE_PRIORITY + p )

// Config Display
#define LCD_Hor		( 16 )
#define LCD_Ver		( 2 )
#define LCD_pixH	( 5 )
#define LCD_pixV	( 8 )

// Config Keypad
#define MAX_FIL		( 4 )
#define MAX_COL		( 3 )
#define BTN_BUFF	( 32 )
#define NFC_BUFF	( 5 )

#define BUZZER		( GPIO8 )
#define MENU_OPTS	( 6 )		// Cantidad de opciones disponibles en el menu de administrador
#define MAX_PASS	( 4 )
#define PASS_ADDR	( 0 )		// Max 16kb = 16*1024 = 16384
#define DEFAULT_PASS	"1337"
#define FIRM_VER	" Firmware v1.02 "

#define xMutexTake( xSemaphore, xBlockTime )		xSemaphoreTake( xSemaphore, xBlockTime )
#define xMutexFree( xSemaphore )					xSemaphoreGive( xSemaphore )

// Variables globales
xQueueHandle xBoton = NULL;			// Boton apretado
xQueueHandle xUserID = NULL;			// Tarjeta NFC acercada
xSemaphoreHandle semAdmin = NULL;	// Semaforo disparador
xSemaphoreHandle xLCD = NULL; 		// Mutex del Display

// Prototipo de funciones de las tareas
void vAdminMode( void* taskParmPtr );
void vUserMode( void* taskParmPtr );
void vModeSelector( void* taskParmPtr );
void vKeypad( void* taskParmPtr );
void vBtn( char btn );

// FUNCION PRINCIPAL
int main(void) {
	// Init. hardware
	boardConfig();

	// Led para dar señal de vida
	gpioWrite( LEDB, ON );
	Board_EEPROM_init();

	// Inicializa colas
	xBoton = xQueueCreate(BTN_BUFF, sizeof(char));
	xUserID = xQueueCreate(NFC_BUFF, sizeof(char));
	xLCD = xSemaphoreCreateMutex();
	vSemaphoreCreateBinary(semAdmin);
	//xSemaphoreTake(semAdmin, FALSE);		// Descarto el primera vez

	// Crear tareas freeRTOS
	xTaskCreate(
		vKeypad,					// pvTaskCode -Funcion de la tarea a ejecutar
		(const char *)"Keypad",		// pcName - Nombre de la tarea para debbug
		Stack(2),					// usStackDeph - Tamaño del stack de la tarea
		(void*)0,					// pvParameters - Parametros de tarea
		Priority(1),				// uxPriority - Prioridad de la tarea
		NULL						// pxCreatedTask - Puntero a la tarea creada en el sistema
	);
	xTaskCreate(
		vBtn,						// pvTaskCode -Funcion de la tarea a ejecutar
		(const char *)"BotonA",		// pcName - Nombre de la tarea para debbug
		Stack(2),					// usStackDeph - Tamaño del stack de la tarea
		(void * const)TEC1,			// pvParameters - Parametros de tarea
		Priority(1),				// uxPriority - Prioridad de la tarea
		NULL						// pxCreatedTask - Puntero a la tarea creada en el sistema
	);
	xTaskCreate(
		vBtn,						// pvTaskCode -Funcion de la tarea a ejecutar
		(const char *)"BotonB",		// pcName - Nombre de la tarea para debbug
		Stack(2),					// usStackDeph - Tamaño del stack de la tarea
		(void * const)TEC2,			// pvParameters - Parametros de tarea
		Priority(1),				// uxPriority - Prioridad de la tarea
		NULL						// pxCreatedTask - Puntero a la tarea creada en el sistema
	);
	xTaskCreate(
		vBtn,						// pvTaskCode -Funcion de la tarea a ejecutar
		(const char *)"BotonC",		// pcName - Nombre de la tarea para debbug
		Stack(2),					// usStackDeph - Tamaño del stack de la tarea
		(void * const)TEC3,			// pvParameters - Parametros de tarea
		Priority(1),				// uxPriority - Prioridad de la tarea
		NULL						// pxCreatedTask - Puntero a la tarea creada en el sistema
	);
	xTaskCreate(
		vBtn,						// pvTaskCode -Funcion de la tarea a ejecutar
		(const char *)"BotonD",		// pcName - Nombre de la tarea para debbug
		Stack(2),					// usStackDeph - Tamaño del stack de la tarea
		(void * const)TEC4,			// pvParameters - Parametros de tarea
		Priority(1),				// uxPriority - Prioridad de la tarea
		NULL						// pxCreatedTask - Puntero a la tarea creada en el sistema
	);
	xTaskCreate(
		vModeSelector,				// pvTaskCode -Funcion de la tarea a ejecutar
		(const char *)"Selector",	// pcName - Nombre de la tarea para debbug
		Stack(2),					// usStackDeph - Tamaño del stack de la tarea
		(void*)0,					// pvParameters - Parametros de tarea
		Priority(3),				// uxPriority - Prioridad de la tarea
		NULL						// pxCreatedTask - Puntero a la tarea creada en el sistema
	);
	// Iniciar scheduler
	vTaskStartScheduler();

	// Endless loop
	while(TRUE) {}	// ERROR: Si cae en este while 1 significa que no pudo iniciar el scheduler
	return 0;
}

// Implementaciones de funciones de las tareas


/* CONTROLADOR DEL MODO
 * Administra la tarea a ejecutar entre Admin o User Mode
 */
void vModeSelector( void* taskParmPtr ) {
	// Init. Config
	TaskHandle_t xHandleUser = NULL;
	TaskHandle_t xHandleAdmin = NULL;

	xMutexTake(xLCD, portMAX_DELAY);
	lcdInit( LCD_Hor, LCD_Ver, LCD_pixH, LCD_pixV );

	// Imprecion inicial en Display
	lcdClear(); 		// Borrar la pantalla
	lcdGoToXY( 1, 1 ); 	// Poner cursor en 1, 1
	lcdSendStringRaw( " ACCESS CONTROL " );
	lcdGoToXY( 1, 2 ); 	// Poner cursor en 1, 2
	lcdSendStringRaw( FIRM_VER );
	xMutexFree(xLCD);
	vTaskDelay( MS(500) );

	// Endless loop
	while(TRUE) {

		xSemaphoreTake(semAdmin, portMAX_DELAY);		// Abilita User Mode

		if(xHandleAdmin != NULL)				// Mata Admin Mode
			vTaskDelete(xHandleAdmin);
		xHandleAdmin = NULL;

		xTaskCreate(
			vUserMode,					// pvTaskCode -Funcion de la tarea a ejecutar
			(const char *)"User",		// pcName - Nombre de la tarea para debbug
			Stack(2),					// usStackDeph - Tamaño del stack de la tarea
			(void*)0,					// pvParameters - Parametros de tarea
			Priority(2),				// uxPriority - Prioridad de la tarea
			&xHandleUser				// pxCreatedTask - Puntero a la tarea creada en el sistema
		);

		vPortYield();
		xSemaphoreTake(semAdmin, portMAX_DELAY);		// Abilita Admin Mode

		if(xHandleUser != NULL)				// Mata User mode
			vTaskDelete(xHandleUser);
		xHandleUser = NULL;

		xTaskCreate(
			vAdminMode,					// pvTaskCode -Funcion de la tarea a ejecutar
			(const char *)"Admin",		// pcName - Nombre de la tarea para debbug
			Stack(2),					// usStackDeph - Tamaño del stack de la tarea
			(void*)0,					// pvParameters - Parametros de tarea
			Priority(2),				// uxPriority - Prioridad de la tarea
			&xHandleAdmin				// pxCreatedTask - Puntero a la tarea creada en el sistema
		);
		vPortYield();
	}
}

/* MODO USUARIO:
 * - Espera que el usuario ingrese por teclado matricial la contrasena
 * - Verifica que la contrasena sea la correcta
 * - En caso de ser correcta habilita
 * - En caso de ser incorrecta bloquea
 * - Dos segundos despues vuelve a esperar la proxima constasena
 */
void vUserMode( void* taskParmPtr ) {
	// Init. Config
	unsigned int i = 0;
	char opc = NULL;
	int aux = 0;
	char pass[MAX_PASS+1] = {DEFAULT_PASS};
	char input[MAX_PASS+1] = {0};
	char user_id = NULL;

	gpioConfig( BUZZER, GPIO_OUTPUT );

	for (i = 0; i < MAX_PASS; i++){
		aux = Board_EEPROM_readByte(PASS_ADDR+i);
		if (aux != -1){
			pass[i] = aux;
		}
		else {
			// Si la carga falla, cargar contrasena predeterminada
			strcpy(pass, DEFAULT_PASS);
		}
	}

	while (TRUE) {

		xMutexTake(xLCD, portMAX_DELAY);
		lcdClear(); 		// Borrar la pantalla
		lcdGoToXY( 1, 1 ); 	// Poner cursor en 1, 1
		lcdSendStringRaw( "Waiting for Card" );
		lcdGoToXY( 1, 2 ); 	// Poner cursor en 1, 2
		lcdSendStringRaw( "      [  ]      " );
		xMutexFree(xLCD);

		i=0;
		do {
			xMutexTake(xLCD, portMAX_DELAY);
			lcdGoToXY( (LCD_Hor / 2) - 3 - i, 2 ); 	// Poner cursor en 1, 2
			lcdData(' ');
			lcdGoToXY( (LCD_Hor / 2) + 4 + i, 2 ); 	// Poner cursor en 1, 2
			lcdData(' ');
			i++;
			i%=3;
			lcdGoToXY( (LCD_Hor / 2) - 3 - i, 2 ); 	// Poner cursor en 1, 2
			lcdData(')');
			lcdGoToXY( (LCD_Hor / 2) + 4 + i, 2 ); 	// Poner cursor en 1, 2
			lcdData('(');
			lcdGoToXY( LCD_Hor + 1, 2 ); 	// Poner cursor fuera
			xMutexFree(xLCD);
		} while ( !xQueueReceive(xUserID, &user_id, MS(500) ));	// Espera tarjeta NFC

		while (xQueueReceive(xBoton, &opc, FALSE));	// Descarta buffer previo del teclado

		xMutexTake(xLCD, portMAX_DELAY);
		lcdClear(); 		// Borrar la pantalla
		lcdGoToXY( 1, 1 ); 	// Poner cursor en 1, 1
		lcdSendStringRaw( "Input Password: " );
		// (16-4)x2%2 = [    * * * *     ]
		lcdGoToXY( 1+(( LCD_Hor / 2 ) - MAX_PASS), 2 );
		xMutexFree(xLCD);
		for ( i = 0; i < MAX_PASS; i++) {
			xQueueReceive(xBoton, &opc, portMAX_DELAY);
			input[i] = opc;
			xMutexTake(xLCD, portMAX_DELAY);
			//lcdData(opc);	// DEBUG: Show pressed
			lcdData('*');
			lcdData(' ');
			xMutexFree(xLCD);
		}
		input[i+1] = NULL;

		lcdGoToXY( 1, 1 ); 	// Poner cursor en 1, 1
		gpioWrite( LEDB, OFF );
		if ( strcmp(pass, input) == 0 ) {
			xMutexTake(xLCD, portMAX_DELAY);
			lcdSendStringRaw( " ACCESS GRANTED " );
			xMutexFree(xLCD);
			gpioWrite( LEDG, ON );
			if (user_id == 'Z') {
				xSemaphoreGive(semAdmin);	// Pasa a Admin Mode
				}
		}
		else {
			xMutexTake(xLCD, portMAX_DELAY);
			lcdSendStringRaw( " ACCESS DENIED! " );
			xMutexFree(xLCD);
			gpioWrite( LEDR, ON );
			//gpioWrite( BUZZER, ON );
		}
		vTaskDelay( MS(2000) );
		gpioWrite( LEDG, OFF );
		gpioWrite( LEDR, OFF );
		gpioWrite( BUZZER, OFF );
		gpioWrite( LEDB, ON );
		vPortYield();
	}
}

/* MODO ADMINISTRADOR
 * - Inicializa y controla el display
 * - Guarda la contrasena y configura su default
 * - Carga la contrasena de la SD o EEPROM
 * Opciones disponibles:
 * - Mostrar contrasena: Imprime en el display la contrasena actual
 * - Cambiar contrasena: El usuario puede cambiar la contrasena
 * - Guardar contrasena: Guarda la contrasena actual en al SD o EEPROM
 */
void vAdminMode( void* taskParmPtr ) {
	// Init. Config
	unsigned int i = 0;
	char opc = NULL;
	int aux = 0;
	char pass[MAX_PASS+1] = {DEFAULT_PASS};
	char menu_options[MENU_OPTS][LCD_Hor+1] = {	"Exit",
							"Show Pass",
							"Change Pass",
							"Save Pass",
							"Load from SD",
							"Store in SD"};

	for (i = 0; i < MAX_PASS; i++){
		aux = Board_EEPROM_readByte(PASS_ADDR+i);
		if (aux != -1){
			pass[i] = aux;
		}
		else{
			// Si la carga falla cargar contrasena predeterminada
			strcpy(pass, DEFAULT_PASS);
		}
	}

	// Endless loop
	while(TRUE) {
		i = 0;
		do {
			// Cada 2 segundos rota las opciones disponisbles en pantalla
			xMutexTake(xLCD, portMAX_DELAY);
			lcdClear(); 		// Borrar la pantalla
			lcdGoToXY( 1, 1 ); 	// Poner cursor en 1, 1
			lcdSendStringRaw( "   ADMIN MENU   " );
			lcdGoToXY( 1, 2 ); 	// Poner cursor en 1, 2
			lcdData(i+'0');		// int a ASCII
			lcdSendStringRaw( ". " );
			lcdSendStringRaw( menu_options[i] );
			lcdGoToXY( LCD_Hor+1, 2 ); 	// Esconder cursor
			xMutexFree(xLCD);
			i++;
			i%=MENU_OPTS;		// Marquecina
		} while  (	!xQueueReceive(xBoton, &opc, MS(2000)) );

		// Sale al precionar un boton, imprime:
		// "> OPCION SELECT <"
		xMutexTake(xLCD, portMAX_DELAY);
		lcdClear(); 		// Borrar la pantalla
		lcdGoToXY( 1, 1 ); 	// Poner cursor en 1, 1
		lcdSendStringRaw( "> " );
		lcdSendStringRaw( menu_options[opc-'0'] );		// ASCII to int
		lcdGoToXY( LCD_Hor, 1 ); 	// Poner cursor en ultima posicion
		lcdSendStringRaw( "<" );
		lcdGoToXY( 1, 2 ); 	// Poner cursor en 1, 2
		xMutexFree(xLCD);

		switch (opc){
			case ('1'): {	// Show password
				xMutexTake(xLCD, portMAX_DELAY);
				// lcdSendStringRaw( pass );
				// (16-4)x2%2 = [    * * * *     ]
				lcdGoToXY( 1+(( LCD_Hor / 2 ) - MAX_PASS), 2 );
				for ( i = 0; pass[i] ; i++){
					lcdData( pass[i] );
					lcdData(' ');
				}
				xMutexFree(xLCD);
				xQueueReceive(xBoton, &opc, portMAX_DELAY);	// Any key to exit
			} break;
			case ('2'): {	// Change password - TODO: Pedir pass anterior
				xMutexTake(xLCD, portMAX_DELAY);
				// (16-4)x2%2 = [    * * * *     ]
				lcdGoToXY( 1+(( LCD_Hor / 2 ) - MAX_PASS), 2 );
				for ( i = 0; i < MAX_PASS; i++) {
					xQueueReceive(xBoton, &opc, portMAX_DELAY);
					pass[i] = opc;
					//lcdData(opc);	// DEBUG: Showing pass
					lcdData('*');
					lcdData(' ');
				}
				xMutexFree(xLCD);
			} break;
			case ('3'): {	// Save to EEPROM
				xMutexTake(xLCD, portMAX_DELAY);
				for (i = 0; i < MAX_PASS; i++){
					lcdGoToXY( 1, 2 ); 	// Poner cursor en 1, 2
					aux = Board_EEPROM_writeByte(PASS_ADDR+i, pass[i]);
					if (aux != -1){
						lcdSendStringRaw( " Saved to EEPROM " );
						vTaskDelay( MS(500) );
					}
					else{
						lcdSendStringRaw( " SAVING ERROR! " );
						xQueueReceive(xBoton, &opc, portMAX_DELAY);
					}
				}
				xMutexFree(xLCD);
			} break;
			case ('4'): {	// TODO: Load from SD card
				xMutexTake(xLCD, portMAX_DELAY);
				lcdSendStringRaw( " Coming soon... " );
				xMutexFree(xLCD);
				xQueueReceive(xBoton, &opc, portMAX_DELAY);
			} break;
			case ('5'): {	// TODO: Store to SD card
				xMutexTake(xLCD, portMAX_DELAY);
				lcdSendStringRaw( " Coming soon... " );
				xMutexFree(xLCD);
				xQueueReceive(xBoton, &opc, portMAX_DELAY);
			} break;
			case ('0'): {	// Exit admin mode.
				xSemaphoreGive(semAdmin);	// Libera el semaforo
			} break;
			// Botor invalido, vuelve al loop
			//default: opc = NULL;
		}
		vPortYield();
	}
}

/* CONTROLADOR DE TECLAS
 * Detector de flanco decendente
 * Envia los flancos luego de 20ms a la cola
 * Tiene un parametro de entrada: la tecla a iniciar.
 * De esta forma una misma tarea se llama una vez por cada tecla a iniciar.
 */
void vBtn( char btn ) {
	// Init. Config
	char btn_status = 0;	// 0 = Hi, 1 = Timer, 2 = Lo


	// Endless loop
	while(TRUE) {
		// Maquina de estados: detecta flanco de subida
		switch (btn_status) {
			case (0): { // Hi
				if (!gpioRead( btn ))
					btn_status = 1;
			}break;
			case (1): { // Timer
			// Espera que estabilize
				vTaskDelay( MS(25) );
				if ( !gpioRead( btn )) {
					btn -= TEC1;
					btn += 'W';
					// W=TEC1, X=TEC2, Y=TEC3, Z=TEC4
					xQueueSend(xUserID, &btn, FALSE);	// DEBUG: Uso boton mientras no tengo NFC
//					xSemaphoreGive(semAdmin);	// Libera el semaforo
					btn -= 'W';
					btn += TEC1;
					btn_status = 2;
				}
				else
					btn_status = 0;
			}break;
			case (2): { // Lo
				if ( gpioRead( btn ))
					btn_status = 0;
			}break;
			default: btn_status = 0;
		}
		vPortYield();
	}
}

/* CONTROLADOR DE TECLADO MATRICIAL
 * Por detector de flanco.
 * Envia el char correspondiente luego de 20ms a la cola.
 * Verifica que sea la misma tecla.
 */
void vKeypad( void* taskParmPtr ) {
	// Init. Config
	keypad_t keypad;
	char btn_status = 0;	// 0 = Hi, 1 = Timer, 2 = Lo
	char matrix[ ( MAX_FIL * MAX_FIL )] = {	'1', '2', '3',
						'4', '5', '6',
						'7', '8', '9',
						'*', '0', '#'};
	
	// Filas --> Salidas
	uint8_t keypadRowPins1[MAX_FIL] = {
	T_FIL0,		// Row 0
	T_FIL1,		// Row 1
	T_FIL2,		// Row 2
	T_FIL3		// Row 3
	};

	// Columnas --> Entradas con pull-up (MODO = GPIO_INPUT_PULLUP)
	uint8_t keypadColPins1[MAX_COL] = {
	T_COL0,		// Column 0
	T_COL1,		// Column 1
	T_COL2,		// Column 2
	};

	keypadConfig( &keypad, keypadRowPins1, MAX_FIL, keypadColPins1, MAX_COL );

	uint16_t prev = FALSE;
	uint16_t key = FALSE;
	// Endless loop
	while(TRUE)
	{
		// Maquina de estados: detecta flanco de bajada
		switch (btn_status) {
			case (0): { // Hi
				if ( keypadRead ( &keypad, &prev )) {
					btn_status = 1;
				}
			}break;
			case (1): { // Timer
				// Espera que estabilize
				vTaskDelay( MS(25) );
				if ( keypadRead ( &keypad, &key )) {
					if ( key == prev ){		// Descarta si son teclas diferentes
						xQueueSend(xBoton, &matrix[key], 0);
						btn_status = 2;
					}
					else
						btn_status = 0;
				}
				else
					btn_status = 0;
			}break;
			case (2): { // Lo
				if ( !keypadRead ( &keypad, &prev ))
					btn_status = 0;
			}break;
			default: btn_status = 0;
		}
		vPortYield();
	}
}

void vApplicationTickHook ( void ){}

void vApplicationIdleHook ( void ){}
