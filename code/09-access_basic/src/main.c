// Includes de FreeRTOS
#include "FreeRTOS.h"
#include "FreeRTOSConfig.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"
#include "sapi.h"

// Definiciones
#define MS(t)					((portTickType) ( (t) / portTICK_RATE_MS))
#define Stack(s)				( configMINIMAL_STACK_SIZE * s )
#define Priority(p)				( tskIDLE_PRIORITY + p )
#define LCD_Hor					16
#define LCD_Ver					2
#define LCD_pixH				5
#define LCD_pixV				8
#define BTN_BUFF				32
#define MAX_FIL					4
#define MAX_COL					3
#define MAX_PASS				4

// Variables globales
xQueueHandle xBoton = NULL;		// Boton apretado

// Prototipo de funciones de las tareas
void vMenu( void* taskParmPtr );
void vKeypad( void* taskParmPtr );
void vBtn( int btn );

// FUNCION PRINCIPAL
// Menu basico con Keypad
int main(void)
{
	// Init. hardware
	boardConfig();

	// Led para dar señal de vida
	gpioWrite( LEDB, ON );

	xBoton = xQueueCreate(BTN_BUFF, sizeof(char));
	// Crear tareas freeRTOS
	xTaskCreate(
		vMenu,						// pvTaskCode -Funcion de la tarea a ejecutar
		(const char *)"Menu",		// pcName - Nombre de la tarea para debbug
		Stack(2),					// usStackDeph - Tamaño del stack de la tarea
		(void*)0,					// pvParameters - Parametros de tarea
		Priority(2),				// uxPriority - Prioridad de la tarea
		NULL						// pxCreatedTask - Puntero a la tarea creada en el sistema
	);
	xTaskCreate(
		vKeypad,					// pvTaskCode -Funcion de la tarea a ejecutar
		(const char *)"Keypad",		// pcName - Nombre de la tarea para debbug
		Stack(2),					// usStackDeph - Tamaño del stack de la tarea
		(void*)0,					// pvParameters - Parametros de tarea
		Priority(1),				// uxPriority - Prioridad de la tarea
		NULL						// pxCreatedTask - Puntero a la tarea creada en el sistema
	);
	xTaskCreate(
		vBtn,						// pvTaskCode -Funcion de la tarea a ejecutar
		(const char *)"BotonA",		// pcName - Nombre de la tarea para debbug
		Stack(2),					// usStackDeph - Tamaño del stack de la tarea
		(void * const)TEC1,			// pvParameters - Parametros de tarea
		Priority(1),				// uxPriority - Prioridad de la tarea
		NULL						// pxCreatedTask - Puntero a la tarea creada en el sistema
	);
	xTaskCreate(
		vBtn,						// pvTaskCode -Funcion de la tarea a ejecutar
		(const char *)"BotonB",		// pcName - Nombre de la tarea para debbug
		Stack(2),					// usStackDeph - Tamaño del stack de la tarea
		(void * const)TEC2,						// pvParameters - Parametros de tarea
		Priority(1),				// uxPriority - Prioridad de la tarea
		NULL						// pxCreatedTask - Puntero a la tarea creada en el sistema
	);
	xTaskCreate(
		vBtn,						// pvTaskCode -Funcion de la tarea a ejecutar
		(const char *)"BotonC",		// pcName - Nombre de la tarea para debbug
		Stack(2),					// usStackDeph - Tamaño del stack de la tarea
		(void * const)TEC3,						// pvParameters - Parametros de tarea
		Priority(1),				// uxPriority - Prioridad de la tarea
		NULL						// pxCreatedTask - Puntero a la tarea creada en el sistema
	);
	xTaskCreate(
		vBtn,						// pvTaskCode -Funcion de la tarea a ejecutar
		(const char *)"BotonD",		// pcName - Nombre de la tarea para debbug
		Stack(2),					// usStackDeph - Tamaño del stack de la tarea
		(void * const)TEC4,						// pvParameters - Parametros de tarea
		Priority(1),				// uxPriority - Prioridad de la tarea
		NULL						// pxCreatedTask - Puntero a la tarea creada en el sistema
	);

	// Iniciar scheduler
	vTaskStartScheduler();

	// Endless loop
	while(TRUE)
	{
	// ERROR: Si cae en este while 1 significa que no pudo iniciar el scheduler
	}
	return 0;
}

// Implementaciones de funciones de las tareas
void vMenu( void* taskParmPtr )
{
	// Init. Config
	lcdInit( LCD_Hor, LCD_Ver, LCD_pixH, LCD_pixV );
	int opc = FALSE;
	int i = 0;
	char pass[MAX_PASS] = {'A','C','D','C'};
	char input[MAX_PASS] = {0,0,0,0};
	int access = FALSE;
	// Endless loop
	while(TRUE)
	{
		lcdClear(); 		// Borrar la pantalla
		lcdGoToXY( 1, 1 ); 	// Poner cursor en 1, 1
		lcdSendStringRaw( "Input Password:" );
		lcdGoToXY( 1, 2 ); 	// Poner cursor en 1, 2
		for (i = 0; i < (( LCD_Hor / 2 ) - MAX_PASS); i++) { 	// (16-4)x2%2
			lcdData(' ');
		}
		for ( i = 0; i < MAX_PASS; i++) {
			xQueueReceive(xBoton, &opc, portMAX_DELAY);
			switch (opc){
				case (TEC1): opc = 'A';break;
				case (TEC2): opc = 'B';break;
				case (TEC3): opc = 'C';break;
				case (TEC4): opc = 'D';break;
				default: opc += '0';
			}
			input[i] = opc;	// [    * * * *     ]
			//lcdData(opc);	// DEBUG
			lcdData('*');
			lcdData(' ');
		}
		// String compare
		access = TRUE;
		for ( i = 0; i < MAX_PASS; i++) {
			if (pass[i] != input[i])
				access = FALSE;
		}
		lcdGoToXY( 1, 1 ); 	// Poner cursor en 1, 1
		if (access) {
			lcdSendStringRaw( " ACCESS GRANTED " );
			gpioWrite( LEDG, ON );
		}
		else {
			lcdSendStringRaw( " ACCESS DENIED! " );
			gpioWrite( LEDR, ON );
		}
		vTaskDelay( MS(2000) );
		gpioWrite( LEDR, OFF );
		gpioWrite( LEDG, OFF );
		vPortYield();
	}
}

void vBtn( int btn )
{
	// Init. Config
	int btn_status = 0;	// 0 = Hi, 1 = Timer, 2 = Lo
	gpioConfig( GPIO0, GPIO_INPUT );

	// Endless loop
	while(TRUE)
	{
		// Maquina de estados: detecta flanco de subida
		switch (btn_status) {
			case (0): // Hi
			{
				if (!gpioRead( btn ))
					btn_status = 1;
			}break;
			case (1): // Timer
			{
			// Espera que estabilize
				vTaskDelay( MS(25) );
				if ( !gpioRead( btn ))
				{
					xQueueSend(xBoton, &btn, FALSE);
					btn_status = 2;
				}
				else
					btn_status = 0;
			}break;
			case (2): // Lo
			{
				if ( gpioRead( btn ))
					btn_status = 0;
			}break;
			default:
			{
				btn_status = 0;
			}
		}
		vPortYield();
	}
}

void vKeypad( void* taskParmPtr )
{
	// Init. Config
	   keypad_t keypad;

	   // Filas --> Salidas
	   uint8_t keypadRowPins1[MAX_FIL] = {
	      T_FIL0,	 // Row 0
	      T_FIL1,    // Row 1
		  T_FIL2,    // Row 2
		  T_FIL3     // Row 3
	   };

	   // Columnas --> Entradas con pull-up (MODO = GPIO_INPUT_PULLUP)
	   uint8_t keypadColPins1[MAX_COL] = {
	      T_COL0,    // Column 0
	      T_COL1,    // Column 1
	      T_COL2,    // Column 2
	   };

	   keypadConfig( &keypad, keypadRowPins1, MAX_FIL, keypadColPins1, MAX_COL );

	   uint16_t key = FALSE;
	// Endless loop
	while(TRUE)
	{
		if ( keypadRead ( &keypad, &key ))
			xQueueSend(xBoton, &key, FALSE);
			vPortYield();
	}
}
