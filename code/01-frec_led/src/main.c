// Includes de FreeRTOS
#include "FreeRTOS.h"
#include "FreeRTOSConfig.h"
#include "task.h"
#include "sapi.h"        // <= sAPI header

// Definiciones

// Variables globales
int frec_mult = 1;  // Multiplicador de frecuencia

// Prototipo de funciones de las tareas
void vBlinky( void* taskParmPtr );
void vButtons( void* taskParmPtr );

// FUNCION PRINCIPAL
// LED titilando, al presionar uno de los cuatro pulsadores de la placa cambia la velocidad de frecuencia en la que el LED titila
int main(void)
{
	// Init. hardware
	boardConfig();
	gpioWrite( LED3, ON );

	// Crear tareas freeRTOS

	xTaskCreate(
		vButtons,			// pvTaskCode -Funcion de la tarea a ejecutar
		(const char *)"Buttons",	// pcName - Nombre de la tarea para debbug
		configMINIMAL_STACK_SIZE*2,	// usStackDeph - Tamaño del stack de la tarea
		(void*)0,			// pvParameters - Parametros de tarea
		tskIDLE_PRIORITY+1,		// uxPriority - Prioridad de la tarea
		NULL				// pxCreatedTask - Puntero a la tarea creada en el sistema
	);

	xTaskCreate(
		vBlinky,			// pvTaskCode -Funcion de la tarea a ejecutar
		(const char *)"Blinky",		// pcName - Nombre de la tarea para debbug
		configMINIMAL_STACK_SIZE*2,	// usStackDeph - Tamaño del stack de la tarea
		(void*)0,			// pvParameters - Parametros de tarea
		tskIDLE_PRIORITY+2,		// uxPriority - Prioridad de la tarea
		NULL				// pxCreatedTask - Puntero a la tarea creada en el sistema
	);

	// Iniciar scheduler
	vTaskStartScheduler();

	// Endless loop
	while( TRUE )
	{
	// ERROR: Si cae en este while 1 significa que no pudo iniciar el scheduler
   }
   return 0;
}

// Implementaciones de funciones de las tareas
void vBlinky( void* taskParmPtr )
{
	// Init. Config

	// Endless loop
	while(TRUE)
	{
		// Intercambia el estado del LED
		gpioToggle( LED2 );

		// Envia la tarea al estado bloqueado durante 500ms
		vTaskDelay( 500 / (frec_mult * portTICK_RATE_MS ));
	}
}

void vButtons( void* taskParmPtr )
{
	// Init. Config
	gpioConfig( GPIO0, GPIO_INPUT );

	// Endless loop
	while(TRUE)     
	{
	    // Si se preciona una tecla cambia el multiplicador de frecuencia
		if ( !gpioRead( TEC1 ) ){
			frec_mult = 1 ;
		}
		if ( !gpioRead( TEC2 ) ){
			frec_mult = 2 ;
		}
		if ( !gpioRead( TEC3 ) ){
			frec_mult = 3 ;
		}
		if ( !gpioRead( TEC4 ) ){
			frec_mult = 4 ;
		}
	}
}
