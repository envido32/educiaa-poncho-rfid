/*
*         Copyright (c), NXP Semiconductors Caen / France
*
*                     (C)NXP Semiconductors
*       All rights are reserved. Reproduction in whole or in part is
*      prohibited without the written consent of the copyright owner.
*  NXP reserves the right to make changes without notice at any time.
* NXP makes no warranty, expressed, implied or statutory, including but
* not limited to any implied warranty of merchantability or fitness for any
*particular purpose, or that the use will not infringe any third party patent,
* copyright or trademark. NXP must not be liable for any loss or damage
*                          arising from its use.
*/

#include <stdint.h>
#include "board.h"
#include "FreeRTOS.h"
#include "FreeRTOSConfig.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"
#include "sapi_i2c.h"
#include "sapi_board.h"
#include "sapi_gpio.h"
#include <tool.h>

#define xMutexTake( xSemaphore, xBlockTime )		xSemaphoreTake( xSemaphore, xBlockTime )
#define xMutexFree( xSemaphore )					xSemaphoreGive( xSemaphore )

static SemaphoreHandle_t I2Cmutex = NULL; 		// Mutex I2C
static SemaphoreHandle_t IrqSem = NULL;

void GPIO0_IRQHandler(void) {
	if (gpioGetInt(0)) {
	    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
	    gpioClearInt(0);
	    xSemaphoreGiveFromISR(IrqSem, &xHigherPriorityTaskWoken);
		portEND_SWITCHING_ISR(xHigherPriorityTaskWoken);
	}
}

static Status tml_Init(void) {

    gpioConfig(NXPNCI_IRQ_GPIO, GPIO_INPUT);
    gpioPinInt(0, NXPNCI_IRQ_GPIO);
    gpioConfig(NXPNCI_VEN_GPIO, GPIO_OUTPUT);
	i2cConfig(NXPNCI_I2C_INSTANCE, NXPNCI_I2C_BAUDRATE);

    I2Cmutex = xSemaphoreCreateMutex();
    IrqSem = xSemaphoreCreateBinary();
    xSemaphoreTake(IrqSem, FALSE);

    return SUCCESS;
}

static Status tml_DeInit(void) {
	vSemaphoreDelete(IrqSem);
	vSemaphoreDelete(I2Cmutex);
	gpioWrite(NXPNCI_VEN_GPIO, FALSE);
    return SUCCESS;
}

static Status tml_Reset(void) {
	gpioWrite(NXPNCI_VEN_GPIO, TRUE);
	Sleep(10);
	gpioWrite(NXPNCI_VEN_GPIO, FALSE);
	Sleep(10);
	gpioWrite(NXPNCI_VEN_GPIO, TRUE);
	Sleep(10);
	return SUCCESS;
}

static Status tml_Tx(uint8_t *pBuff, uint16_t buffLen) {

	if (i2cWrite(NXPNCI_I2C_INSTANCE, NXPNCI_I2C_ADDR, pBuff, buffLen, FALSE) != SUCCESS) {
    	Sleep(10);
    	if (i2cWrite(NXPNCI_I2C_INSTANCE, NXPNCI_I2C_ADDR, pBuff, buffLen, FALSE) != SUCCESS) {
    		return ERROR;
    	}
    }

	return SUCCESS;
}

static Status tml_Rx(uint8_t *pBuff, uint16_t buffLen, uint16_t *pBytesRead) {
    if(i2cRead(NXPNCI_I2C_INSTANCE, NXPNCI_I2C_ADDR, NULL, NULL, FALSE, pBuff, 3, FALSE) == SUCCESS) {
    	if ((pBuff[2] + 3) <= buffLen) {
			if (pBuff[2] > 0) {
				if(i2cRead(NXPNCI_I2C_INSTANCE, NXPNCI_I2C_ADDR, NULL, NULL, FALSE, &pBuff[3], pBuff[2], FALSE) == SUCCESS) {
					*pBytesRead = pBuff[2] + 3;
				}
				else {
					return ERROR;
				}
			} else {
				*pBytesRead = 3;
			}
    	}
		else {
			return ERROR;
		}
   }
    else {
    	return ERROR;
    }

	return SUCCESS;
}

static Status tml_WaitForRx(uint32_t timeout) {
	if (xSemaphoreTake(IrqSem, (timeout==0)?(portMAX_DELAY):(portTICK_PERIOD_MS*timeout)) != pdTRUE) {
		return ERROR;
	}
	return SUCCESS;
}

void tml_Connect(void) {
	tml_Init();
	tml_Reset();
}

void tml_Disconnect(void) {
	tml_DeInit();
}

void tml_Send(uint8_t *pBuffer, uint16_t BufferLen, uint16_t *pBytesSent) {
	xMutexTake(I2Cmutex, portMAX_DELAY);
	if(tml_Tx(pBuffer, BufferLen) == ERROR) {
		*pBytesSent = 0;
	}
	else {
		*pBytesSent = BufferLen;
	}
	xMutexFree(I2Cmutex);
}

void tml_Receive(uint8_t *pBuffer, uint16_t BufferLen, uint16_t *pBytes, uint16_t timeout) {
	xMutexTake(I2Cmutex, portMAX_DELAY);
	if (tml_WaitForRx(timeout) == ERROR) {
		*pBytes = 0;
	}
	else {
		tml_Rx(pBuffer, BufferLen, pBytes);
	}
	xMutexFree(I2Cmutex);
}
