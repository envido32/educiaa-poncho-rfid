#ifndef _NFC_LIB_H_
#define _NFC_LIB_H_

// Includes de FreeRTOS
#include "FreeRTOS.h"
#include "FreeRTOSConfig.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"
#include "sapi.h"
#include <tool.h>
#include <Nfc.h>
#include <ndef_helper.h>

// Definiciones
#define NFC_ADDR	( NXPNCI_I2C_ADDR )	// PN7150. Dato del fabricante
#define NFC_MESSAGE_MAX     ( 32 )

#define TASK_NFC_STACK_SIZE		1024
#define TASK_NFC_STACK_PRIO		(configMAX_PRIORITIES - 1)

typedef struct{
    char structure;        // MB/ME/CF/1/IL/TNF
    char type_length;
    char payload_length;
    char type;
    char status;
    char lang[2];
    char message[NFC_MESSAGE_MAX];
} ndef_pack_t;

// Prototipo de funciones
int init_nfc(void);
int nfc_reader(NxpNci_RfIntf_t RfIntf, char message[]);
int nfc_writer(NxpNci_RfIntf_t RfIntf, char message[]);
void NdefPull_Cb(unsigned char *pNdefMessage, unsigned short NdefMessageSize);
void NdefPush_Cb(unsigned char *pNdefRecord, unsigned short NdefRecordSize);

#endif /* end of include guard: _NFC_LIB_H_ */
