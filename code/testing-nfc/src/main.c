// Includes de FreeRTOS
#include "FreeRTOS.h"
#include "FreeRTOSConfig.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"
#include "sapi.h"
#include <nfc_task.h>

// Definiciones
#define Stack(s)	( configMINIMAL_STACK_SIZE + s )
#define Priority(p)	( tskIDLE_PRIORITY + p )
//#define NFC_ADDR	( PN532_I2C_ADDRESS )	// PN532. Dato del fabricante
#define NFC_ADDR	( NXPNCI_I2C_ADDR )	// PN7150. Dato del fabricante
#define NFC_BUFF1	( 32 )
#define NFC_BUFF2	( 32 )
#define TASK_NFC_STACK_SIZE		1024
#define TASK_NFC_STACK_PRIO		(configMAX_PRIORITIES - 1)


void vBlinky( void* taskParmPtr );

// FUNCION PRINCIPAL
// Cuenta la cantidad de veces que se presiona la tecla 1, luego al presionar la tecla 2 titila un LED la cantidad de veces que se presiono la tecla 1.
int main(void)
{
	// Init. Config

	// Init. hardware
	boardConfig();

	// Crear tareas freeRTOS
	xTaskCreate(
		vBlinky,					// pvTaskCode -Funcion de la tarea a ejecutar
		(const char *)"Blinky",		// pcName - Nombre de la tarea para debbug
		Stack(2),					// usStackDeph - Tamaño del stack de la tarea
		(void*)0,					// pvParameters - Parametros de tarea
		Priority(2),				// uxPriority - Prioridad de la tarea
		NULL						// pxCreatedTask - Puntero a la tarea creada en el sistema
	);

	xTaskCreate((TaskFunction_t) task_nfc,
	    				(const char*) "NFC_task",
						TASK_NFC_STACK_SIZE,
						NULL,
						TASK_NFC_STACK_PRIO,
						NULL);

	// Iniciar scheduler
	vTaskStartScheduler();

	// Endless loop
	while( TRUE )
	{
	// ERROR: Si cae en este while 1 significa que no pudo iniciar el scheduler
	}
	return 0;
}

// Implementaciones de funciones de las tareas
void vBlinky( void* taskParmPtr )
{
	// Init. Config
	int led_work = LED1;


	// Init. Hardware
	lcdInit( 16, 2, 5, 8 );

	lcdClear(); 		// Borrar la pantalla
	lcdGoToXY( 1, 1 ); 	// Poner cursor en 1, 1
	lcdSendStringRaw( "Hola, Mundo!" );

	// Endless loop
	while(TRUE)
	{
		// Intercambia el estado del LED
		gpioToggle( led_work );

		// Envia la tarea al estado bloqueado durante 500ms
		vTaskDelay( MS(500) );
	}
}

