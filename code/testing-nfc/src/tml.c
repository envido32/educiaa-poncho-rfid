/*
*         Copyright (c), NXP Semiconductors Caen / France
*
*                     (C)NXP Semiconductors
*       All rights are reserved. Reproduction in whole or in part is
*      prohibited without the written consent of the copyright owner.
*  NXP reserves the right to make changes without notice at any time.
* NXP makes no warranty, expressed, implied or statutory, including but
* not limited to any implied warranty of merchantability or fitness for any
*particular purpose, or that the use will not infringe any third party patent,
* copyright or trademark. NXP must not be liable for any loss or damage
*                          arising from its use.
*/

#include <stdint.h>
#include "board.h"
#include "FreeRTOS.h"
#include "FreeRTOSConfig.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"
#include "sapi_i2c.h"
#include "sapi_board.h"
#include "sapi_gpio.h"
#include <tool.h>

//#define RW_NDEF_WRITING
//#define RW_RAW_EXCHANGE
//#define CARDEMU_RAW_EXCHANGE
//#define NO_NDEF_SUPPORT

//#define RW_SUPPORT
//#define P2P_SUPPORT
//#define CARDEMU_SUPPORT


SemaphoreHandle_t IrqSem = NULL;
//i2c_master_transfer_t masterXfer;

//typedef enum {ERROR = 0, SUCCESS = !ERROR} Status;

void GPIO0_IRQHandler(void)
{
	//if (GPIO_ReadPinInput(NXPNCI_IRQ_GPIO, NXPNCI_IRQ_PIN) == 1)
	if (gpioGetInt(0))
	{
	    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
		//GPIO_ClearPinsInterruptFlags(NXPNCI_IRQ_GPIO, 1U << NXPNCI_IRQ_PIN);
	    gpioClearInt(0);
	    xSemaphoreGiveFromISR(IrqSem, &xHigherPriorityTaskWoken);
		portEND_SWITCHING_ISR(xHigherPriorityTaskWoken);
	}
}

static Status tml_Init(void) {
    //i2c_master_config_t masterConfig;
    //uint32_t sourceClock;

    //gpio_pin_config_t irq_config = {kGPIO_DigitalInput, 0,};
    //gpio_pin_config_t ven_config = {kGPIO_DigitalOutput, 0,};

    //GPIO_PinInit(NXPNCI_IRQ_GPIO, NXPNCI_IRQ_PIN, &irq_config);
    //GPIO_PinInit(NXPNCI_VEN_GPIO, NXPNCI_VEN_PIN, &ven_config);

    gpioConfig(NXPNCI_IRQ_GPIO, GPIO_INPUT);
    gpioPinInt(0, NXPNCI_IRQ_GPIO);

    gpioConfig(NXPNCI_VEN_GPIO, GPIO_OUTPUT);

    /*For FSL
    I2C_MasterGetDefaultConfig(&masterConfig);
    masterConfig.baudRate_Bps = NXPNCI_I2C_BAUDRATE;
    sourceClock = CLOCK_GetFreq(I2C0_CLK_SRC);
    masterXfer.slaveAddress = NXPNCI_I2C_ADDR;
    masterXfer.subaddress = 0;
    masterXfer.subaddressSize = 0;
    masterXfer.flags = kI2C_TransferDefaultFlag;
    I2C_MasterInit(NXPNCI_I2C_INSTANCE, &masterConfig, sourceClock);
     */

	i2cConfig(NXPNCI_I2C_INSTANCE, NXPNCI_I2C_BAUDRATE);

    IrqSem = xSemaphoreCreateBinary();
    xSemaphoreTake(IrqSem, FALSE);

    return SUCCESS;
}

static Status tml_DeInit(void) {
	vSemaphoreDelete(IrqSem);
	//GPIO_ClearPinsOutput(NXPNCI_VEN_GPIO, 1U << NXPNCI_VEN_PIN);
	gpioWrite(NXPNCI_VEN_GPIO, FALSE);
    return SUCCESS;
}

static Status tml_Reset(void) {
	//GPIO_SetPinsOutput(NXPNCI_VEN_GPIO, 1U << NXPNCI_VEN_PIN);
	gpioWrite(NXPNCI_VEN_GPIO, TRUE);
	Sleep(10);
	//GPIO_ClearPinsOutput(NXPNCI_VEN_GPIO, 1U << NXPNCI_VEN_PIN);
	gpioWrite(NXPNCI_VEN_GPIO, FALSE);
	Sleep(10);
	//GPIO_SetPinsOutput(NXPNCI_VEN_GPIO, 1U << NXPNCI_VEN_PIN);
	gpioWrite(NXPNCI_VEN_GPIO, TRUE);
	Sleep(10);
	return SUCCESS;
}

static Status tml_Tx(uint8_t *pBuff, uint16_t buffLen) {

	//if (I2C_WRITE(pBuff, buffLen) != kStatus_Success)
	if (i2cWrite(NXPNCI_I2C_INSTANCE, NXPNCI_I2C_ADDR, pBuff, buffLen, FALSE) != SUCCESS)
    {
    	Sleep(10);
    	//if(I2C_WRITE(pBuff, buffLen) != kStatus_Success)
    	if (i2cWrite(NXPNCI_I2C_INSTANCE, NXPNCI_I2C_ADDR, pBuff, buffLen, FALSE) != SUCCESS)
    	{
    		return ERROR;
    	}
    }

	return SUCCESS;
}

static Status tml_Rx(uint8_t *pBuff, uint16_t buffLen, uint16_t *pBytesRead) {
    //if(I2C_READ(pBuff, 3) == kStatus_Success)
    if(i2cRead(NXPNCI_I2C_INSTANCE, NXPNCI_I2C_ADDR, NULL, NULL, FALSE, pBuff, 3, FALSE) == SUCCESS)
    //if(i2cRead(NXPNCI_I2C_INSTANCE, NXPNCI_I2C_ADDR, FALSE, pBuff, 3, NULL, NULL, FALSE) == SUCCESS)
    /*
     * bool_t i2cRead( i2cMap_t  i2cNumber,
                uint8_t  i2cSlaveAddress,
                uint8_t* dataToReadBuffer,
                uint16_t dataToReadBufferSize,
                bool_t   sendWriteStop,
                uint8_t* receiveDataBuffer,
                uint16_t receiveDataBufferSize,
                bool_t   sendReadStop ){
     */
    {
    	if ((pBuff[2] + 3) <= buffLen)
    	{
			if (pBuff[2] > 0)
			{
				//if(I2C_READ(&pBuff[3], pBuff[2]) == kStatus_Success)
				if(i2cRead(NXPNCI_I2C_INSTANCE, NXPNCI_I2C_ADDR, NULL, NULL, FALSE, &pBuff[3], pBuff[2], FALSE) == SUCCESS)
				//if(i2cRead(NXPNCI_I2C_INSTANCE, NXPNCI_I2C_ADDR, FALSE, &pBuff[3], pBuff[2], NULL, NULL, FALSE) == SUCCESS)
				{
					*pBytesRead = pBuff[2] + 3;
				}
				else return ERROR;
			} else
			{
				*pBytesRead = 3;
			}
    	}
		else return ERROR;
   }
    else return ERROR;

	return SUCCESS;
}

static Status tml_WaitForRx(uint32_t timeout) {
	if (xSemaphoreTake(IrqSem, (timeout==0)?(portMAX_DELAY):(portTICK_PERIOD_MS*timeout)) != pdTRUE) return ERROR;
	return SUCCESS;
}

void tml_Connect(void) {
	tml_Init();
	tml_Reset();
}

void tml_Disconnect(void) {
	tml_DeInit();
}

void tml_Send(uint8_t *pBuffer, uint16_t BufferLen, uint16_t *pBytesSent) {
	if(tml_Tx(pBuffer, BufferLen) == ERROR) *pBytesSent = 0;
	else *pBytesSent = BufferLen;
}

void tml_Receive(uint8_t *pBuffer, uint16_t BufferLen, uint16_t *pBytes, uint16_t timeout) {
	if (tml_WaitForRx(timeout) == ERROR) *pBytes = 0;
	else tml_Rx(pBuffer, BufferLen, pBytes);
}


