// Includes de FreeRTOS
#include "FreeRTOS.h"
#include "FreeRTOSConfig.h"
//#include "FreeRTOS-openocd.c"
#include "task.h"
#include "semphr.h"
#include "queue.h"
#include "sapi.h"
#include "string.h"
#include "userslib.h"
#include "nfclib.h"

// Definiciones
#define Stack(s)	( configMINIMAL_STACK_SIZE * s )
#define Priority(p)	( tskIDLE_PRIORITY + p )

// Config Display
#define LCD_Hor		( 16 )
#define LCD_Ver		( 2 )
#define LCD_pixH	( 5 )
#define LCD_pixV	( 8 )

// Config Keypad
#define MAX_FIL		( 4 )
#define MAX_COL		( 3 )
#define BTN_BUFF	( 32 )
#define NFC_BUFF	( 5 )

#define BUZZER		( GPIO8 )
#define MENU_OPTS	( 6 )		// Cantidad de opciones disponibles en el menu de administrador
#define PASS_ADDR	( 0 )		// Max 16kb = 16*1024 = 16384
#define DEFAULT_PASS	"1337"
#define FIRM_VER	" Firmware v1.02 "

#define xMutexTake( xSemaphore, xBlockTime )		xSemaphoreTake( xSemaphore, xBlockTime )
#define xMutexFree( xSemaphore )					xSemaphoreGive( xSemaphore )

// Variables globales
QueueHandle_t xBoton = NULL;			// Boton apretado
QueueHandle_t xUserID = NULL;		// Tarjeta NFC acercada
SemaphoreHandle_t xLCD = NULL; 		// Mutex del Display

user_t users_db[MAX_DB];

// Prototipo de funciones de las tareas
void vAdminMode( void* taskParmPtr );
void vUserMode( void* taskParmPtr );
void vKeypad( void* taskParmPtr );
void vBtn( int btn );
void vWaitNFCmov( void* taskParmPtr );

unsigned int WaitForNFC(void);
void AskForPass(char* pass);
void AddUser(user_t* users_db);
void DelUser(user_t* users_db);

/*==================[Main]===================================================*/
int main(void) {
	// Init. hardware
	boardConfig();
	Board_EEPROM_init();

	// Led para dar señal de vida
	gpioWrite( LEDB, ON );

	// Inicializa colas
	xBoton = xQueueCreate(BTN_BUFF, sizeof(char));
	xUserID = xQueueCreate(NFC_BUFF, sizeof(char));
	xLCD = xSemaphoreCreateMutex();

	// DEBUG: crear usuario admin forzado
	if ( !gpioRead( TEC4 )){
		user_t new_user;
		new_user.id = 'Z';
		strcpy(new_user.pass, DEFAULT_PASS);
		new_user.admin = TRUE;
		userAdd( users_db, new_user );
	}

	// Crear tareas freeRTOS
	xTaskCreate(
		vKeypad,					// pvTaskCode - Funcion de la tarea a ejecutar
		(const char *)"Keypad",		// pcName - Nombre de la tarea para debbug
		Stack(1),					// usStackDeph - Tamaño del stack de la tarea
		(void*)0,					// pvParameters - Parametros de tarea
		Priority(1),				// uxPriority - Prioridad de la tarea
		NULL						// pxCreatedTask - Puntero a la tarea creada en el sistema
	);
	xTaskCreate(
		vBtn,						// pvTaskCode - Funcion de la tarea a ejecutar
		(const char *)"BotonA",		// pcName - Nombre de la tarea para debbug
		Stack(1),					// usStackDeph - Tamaño del stack de la tarea
		(void * const)TEC1,			// pvParameters - Parametros de tarea
		Priority(1),				// uxPriority - Prioridad de la tarea
		NULL						// pxCreatedTask - Puntero a la tarea creada en el sistema
	);
	xTaskCreate(
		vBtn,						// pvTaskCode - Funcion de la tarea a ejecutar
		(const char *)"BotonB",		// pcName - Nombre de la tarea para debbug
		Stack(1),					// usStackDeph - Tamaño del stack de la tarea
		(void * const)TEC2,			// pvParameters - Parametros de tarea
		Priority(1),				// uxPriority - Prioridad de la tarea
		NULL						// pxCreatedTask - Puntero a la tarea creada en el sistema
	);
	/*
	xTaskCreate(
		vBtn,						// pvTaskCode - Funcion de la tarea a ejecutar
		(const char *)"BotonC",		// pcName - Nombre de la tarea para debbug
		Stack(1),					// usStackDeph - Tamaño del stack de la tarea
		(void * const)TEC3,			// pvParameters - Parametros de tarea
		Priority(1),				// uxPriority - Prioridad de la tarea
		NULL						// pxCreatedTask - Puntero a la tarea creada en el sistema
	);
	xTaskCreate(
		vBtn,						// pvTaskCode - Funcion de la tarea a ejecutar
		(const char *)"BotonD",		// pcName - Nombre de la tarea para debbug
		Stack(1),					// usStackDeph - Tamaño del stack de la tarea
		(void * const)TEC4,			// pvParameters - Parametros de tarea
		Priority(1),				// uxPriority - Prioridad de la tarea
		NULL						// pxCreatedTask - Puntero a la tarea creada en el sistema
	);
	*/
	xTaskCreate(
		vUserMode,					// pvTaskCode - Funcion de la tarea a ejecutar
		(const char *)"User",		// pcName - Nombre de la tarea para debbug
		Stack(4),					// usStackDeph - Tamaño del stack de la tarea
		(void*)0,					// pvParameters - Parametros de tarea
		Priority(2),				// uxPriority - Prioridad de la tarea
		NULL						// pxCreatedTask - Puntero a la tarea creada en el sistema
	);

	// Iniciar scheduler
	vTaskStartScheduler();

	// Endless loop
	while(TRUE) {}	// ERROR: Si cae en este while 1 significa que no pudo iniciar el scheduler
	return 0;
}

/*==================[Tasks]==================================================*/


/* MODO USUARIO:
 * - Espera que el usuario ingrese por teclado matricial la contrasena
 * - Verifica que la contrasena sea la correcta
 * - En caso de ser correcta habilita
 * - En caso de ser incorrecta bloquea
 * - Dos segundos despues vuelve a esperar la proxima constasena
 */
void vUserMode( void* taskParmPtr ) {
	// Init. Config
	unsigned int i = 0;
	int opc = NULL;
	int aux = 0;
	char pass[MAX_PASS+1] = {DEFAULT_PASS};
	char input[MAX_PASS+1] = {0};
	unsigned int user_id = NULL;
    NxpNci_RfIntf_t RfInterface;

	gpioConfig( BUZZER, GPIO_OUTPUT );

	xMutexTake(xLCD, portMAX_DELAY);
	lcdInit( LCD_Hor, LCD_Ver, LCD_pixH, LCD_pixV );

	// Imprecion inicial en Display
	lcdClear(); 		// Borrar la pantalla
	lcdGoToXY( 1, 1 ); 	// Poner cursor en 1, 1
	lcdSendStringRaw( " ACCESS CONTROL " );
	lcdGoToXY( 1, 2 ); 	// Poner cursor en 1, 2
	lcdSendStringRaw( FIRM_VER );
	xMutexFree(xLCD);

	for (i = 0; i < MAX_PASS; i++){
		aux = Board_EEPROM_readByte(PASS_ADDR+i);
		if (aux != -1){
			pass[i] = aux;
		}
		else {
			// Si la carga falla, cargar contrasena predeterminada
			strcpy(pass, DEFAULT_PASS);
		}
	}

    if(init_nfc() != NFC_SUCCESS){
		xMutexTake(xLCD, portMAX_DELAY);
		lcdClear(); 		// Borrar la pantalla
		lcdGoToXY( 1, 1 ); 	// Poner cursor en 1, 1
		lcdSendStringRaw( " NFC failed! " );
		xMutexFree(xLCD);
		vTaskDelay( MS(500) );
	}

	vTaskDelay( MS(500) );

	while (TRUE) {

		user_id = WaitForNFC();

		opc = userSearch( users_db, user_id );

		if (opc == -1){
			xMutexTake(xLCD, portMAX_DELAY);
			lcdClear(); 		// Borrar la pantalla
			lcdGoToXY( 1, 1 ); 	// Poner cursor en 1, 1
			lcdSendStringRaw( "   > ERROR! <   " );
			lcdGoToXY( 1, 2 ); 	// Poner cursor en 1, 2
			lcdSendStringRaw( " User not found " );
			xMutexFree(xLCD);
			vTaskDelay( MS(2000) );
		}
		else{
			AskForPass(input);

			gpioWrite( LEDB, OFF );
			if ( strcmp(users_db[opc].pass, input) == 0 ) {
				if (users_db[opc].admin) {
					xTaskCreate(
						vAdminMode,					// pvTaskCode - Funcion de la tarea a ejecutar
						(const char *)"Admin",		// pcName - Nombre de la tarea para debbug
						Stack(4),					// usStackDeph - Tamaño del stack de la tarea
						(void*)0,					// pvParameters - Parametros de tarea
						Priority(2),				// uxPriority - Prioridad de la tarea
						NULL						// pxCreatedTask - Puntero a la tarea creada en el sistema
					);
					vPortYield();
					vTaskDelete(NULL);				// Kilss User Mode Task
				}
				else{
					xMutexTake(xLCD, portMAX_DELAY);
					lcdGoToXY( 1, 1 ); 	// Poner cursor en 1, 1
					lcdSendStringRaw( " ACCESS GRANTED " );
					xMutexFree(xLCD);
					gpioWrite( LEDG, ON );
				}
			}
			else {
				xMutexTake(xLCD, portMAX_DELAY);
				lcdGoToXY( 1, 1 ); 	// Poner cursor en 1, 1
				lcdSendStringRaw( " ACCESS DENIED! " );
				xMutexFree(xLCD);
				gpioWrite( LEDR, ON );
				//gpioWrite( BUZZER, ON );
			}
			vTaskDelay( MS(2000) );
			gpioWrite( LEDG, OFF );
			gpioWrite( LEDR, OFF );
			gpioWrite( BUZZER, OFF );
			gpioWrite( LEDB, ON );
		}
		vPortYield();
	}
}

/* MODO ADMINISTRADOR
 * - Inicializa y controla el display
 * - Guarda la contrasena y configura su default
 * - Carga la contrasena de la SD o EEPROM
 * Opciones disponibles:
 * - Mostrar contrasena: Imprime en el display la contrasena actual
 * - Cambiar contrasena: El usuario puede cambiar la contrasena
 * - Guardar contrasena: Guarda la contrasena actual en al SD o EEPROM
 */
void vAdminMode( void* taskParmPtr ) {
	// Init. Config
	unsigned int i = 0;
	int opc = NULL;
	int aux = 0;
	char pass[MAX_PASS+1] = {DEFAULT_PASS};
	//user_t users_db[MAX_DB];
	char menu_options[MENU_OPTS][LCD_Hor+1] = {	"Exit",
							"Add User",
							"Delete User",
							"Save Data",
							"Load from SD",
							"Store in SD"};

	for (i = 0; i < MAX_PASS; i++){
		aux = Board_EEPROM_readByte(PASS_ADDR+i);
		if (aux != -1){
			pass[i] = aux;
		}
		else{
			// Si la carga falla cargar contrasena predeterminada
			strcpy(pass, DEFAULT_PASS);
		}
	}

	// Endless loop
	while(TRUE) {
		i = 0;
		do {
			// Cada 2 segundos rota las opciones disponisbles en pantalla
			xMutexTake(xLCD, portMAX_DELAY);
			lcdClear(); 		// Borrar la pantalla
			lcdGoToXY( 1, 1 ); 	// Poner cursor en 1, 1
			lcdSendStringRaw( "   ADMIN MENU   " );
			lcdGoToXY( 1, 2 ); 	// Poner cursor en 1, 2
			lcdData(i+'0');		// int a ASCII
			lcdSendStringRaw( ". " );
			lcdSendStringRaw( menu_options[i] );
			lcdGoToXY( LCD_Hor+1, 2 ); 	// Esconder cursor
			xMutexFree(xLCD);
			i++;
			i%=MENU_OPTS;		// Marquecina
		} while  (	!xQueueReceive(xBoton, &opc, MS(2000)) );

		// Sale al precionar un boton, imprime:
		// "> OPCION SELECT <"
		xMutexTake(xLCD, portMAX_DELAY);
		lcdClear(); 		// Borrar la pantalla
		lcdGoToXY( 1, 1 ); 	// Poner cursor en 1, 1
		lcdSendStringRaw( "> " );
		lcdSendStringRaw( menu_options[opc-'0'] );		// ASCII to int
		lcdGoToXY( LCD_Hor, 1 ); 	// Poner cursor en ultima posicion
		lcdSendStringRaw( "<" );
		lcdGoToXY( 1, 2 ); 	// Poner cursor en 1, 2
		xMutexFree(xLCD);

		switch (opc){
			case ('1'): {	// Add user
				AddUser(users_db);
			} break;
			case ('2'): {	// Delete user
				DelUser(users_db);
			} break;
			case ('3'): {	// Save to EEPROM
				xMutexTake(xLCD, portMAX_DELAY);
				for (i = 0; i < MAX_PASS; i++){
					lcdGoToXY( 1, 2 ); 	// Poner cursor en 1, 2
					aux = Board_EEPROM_writeByte(PASS_ADDR+i, pass[i]);
					if (aux != -1){
						lcdSendStringRaw( " Saved to EEPROM " );
						vTaskDelay( MS(500) );
					}
					else{
						lcdSendStringRaw( " SAVING ERROR! " );
						vTaskDelay( MS(2000) );
					}
				}
				xMutexFree(xLCD);
			} break;
			case ('4'): {	// TODO: Load from SD card
				xMutexTake(xLCD, portMAX_DELAY);
				lcdSendStringRaw( " Coming soon... " );
				xMutexFree(xLCD);
				xQueueReceive(xBoton, &opc, portMAX_DELAY);
			} break;
			case ('5'): {	// TODO: Store to SD card
				xMutexTake(xLCD, portMAX_DELAY);
				lcdSendStringRaw( " Coming soon... " );
				xMutexFree(xLCD);
				xQueueReceive(xBoton, &opc, portMAX_DELAY);
			} break;
			case ('0'): {	// Exit admin mode
				xTaskCreate(
					vUserMode,					// pvTaskCode - Funcion de la tarea a ejecutar
					(const char *)"User",		// pcName - Nombre de la tarea para debbug
					Stack(4),					// usStackDeph - Tamaño del stack de la tarea
					(void*)0,					// pvParameters - Parametros de tarea
					Priority(2),				// uxPriority - Prioridad de la tarea
					NULL						// pxCreatedTask - Puntero a la tarea creada en el sistema
				);

				vPortYield();
				vTaskDelete(NULL);		// Kills Admin Mode Task
			} break;
			// Botor invalido, vuelve al loop
			//default: opc = NULL;
		}
		vPortYield();
	}
}

/* CONTROLADOR DE TECLAS
 * Detector de flanco decendente
 * Envia los flancos luego de 20ms a la cola
 * Tiene un parametro de entrada: la tecla a iniciar.
 * De esta forma una misma tarea se llama una vez por cada tecla a iniciar.
 */
void vBtn( int btn ) {
	// Init. Config
	char btn_status = 0;	// 0 = Hi, 1 = Timer, 2 = Lo


	// Endless loop
	while(TRUE) {
		// Maquina de estados: detecta flanco de subida
		switch (btn_status) {
			case (0): { // Hi
				if (!gpioRead( btn ))
					btn_status = 1;
			}break;
			case (1): { // Timer
			// Espera que estabilize
				vTaskDelay( MS(25) );
				if ( !gpioRead( btn )) {
					btn -= TEC1;
					btn += 'W';
					// W=TEC1, X=TEC2, Y=TEC3, Z=TEC4
					xQueueSend(xUserID, &btn, FALSE);	// DEBUG: Uso boton mientras no tengo NFC
					btn -= 'W';
					btn += TEC1;
					btn_status = 2;
				}
				else
					btn_status = 0;
			}break;
			case (2): { // Lo
				if ( gpioRead( btn ))
					btn_status = 0;
			}break;
			default: btn_status = 0;
		}
		vPortYield();
	}
}

/* CONTROLADOR DE TECLADO MATRICIAL
 * Por detector de flanco.
 * Envia el char correspondiente luego de 20ms a la cola.
 * Verifica que sea la misma tecla.
 */
void vKeypad( void* taskParmPtr ) {
	// Init. Config
	keypad_t keypad;
	char btn_status = 0;	// 0 = Hi, 1 = Timer, 2 = Lo
	char matrix[ ( MAX_FIL * MAX_FIL )] = {	'1', '2', '3',
						'4', '5', '6',
						'7', '8', '9',
						'*', '0', '#'};
	
	// Filas --> Salidas
	uint8_t keypadRowPins1[MAX_FIL] = {
	T_FIL0,		// Row 0
	T_FIL1,		// Row 1
	T_FIL2,		// Row 2
	T_FIL3		// Row 3
	};

	// Columnas --> Entradas con pull-up (MODO = GPIO_INPUT_PULLUP)
	uint8_t keypadColPins1[MAX_COL] = {
	T_COL0,		// Column 0
	T_COL1,		// Column 1
	T_COL2,		// Column 2
	};

	keypadConfig( &keypad, keypadRowPins1, MAX_FIL, keypadColPins1, MAX_COL );

	uint16_t prev = FALSE;
	uint16_t key = FALSE;
	// Endless loop
	while(TRUE)
	{
		// Maquina de estados: detecta flanco de bajada
		switch (btn_status) {
			case (0): { // Hi
				if ( keypadRead ( &keypad, &prev )) {
					btn_status = 1;
				}
			}break;
			case (1): { // Timer
				// Espera que estabilize
				vTaskDelay( MS(25) );
				if ( keypadRead ( &keypad, &key )) {
					if ( key == prev ){		// Descarta si son teclas diferentes
						xQueueSend(xBoton, &matrix[key], 0);
						btn_status = 2;
					}
					else
						btn_status = 0;
				}
				else
					btn_status = 0;
			}break;
			case (2): { // Lo
				if ( !keypadRead ( &keypad, &prev ))
					btn_status = 0;
			}break;
			default: btn_status = 0;
		}
		vPortYield();
	}
}

void vWaitNFCmov( void* taskParmPtr ) {
	int i = 0;

	xMutexTake(xLCD, portMAX_DELAY);
	lcdClear(); 		// Borrar la pantalla
	lcdGoToXY( 1, 1 ); 	// Poner cursor en 1, 1
	lcdSendStringRaw( "Waiting for Card" );
	lcdGoToXY( 1, 2 ); 	// Poner cursor en 1, 2
	lcdSendStringRaw( "  ))) [  ] (((  " );
	xMutexFree(xLCD);
	// Cute animation

	while(TRUE){
		xMutexTake(xLCD, portMAX_DELAY);
		lcdGoToXY( (LCD_Hor / 2) - 3 - i, 2 ); 	// Poner cursor en 1, 2
		lcdData(' ');
		lcdGoToXY( (LCD_Hor / 2) + 4 + i, 2 ); 	// Poner cursor en 1, 2
		lcdData(' ');
		i++;
		i%=3;
		lcdGoToXY( (LCD_Hor / 2) - 3 - i, 2 ); 	// Poner cursor en 1, 2
		lcdData(')');
		lcdGoToXY( (LCD_Hor / 2) + 4 + i, 2 ); 	// Poner cursor en 1, 2
		lcdData('(');
		lcdGoToXY( LCD_Hor + 1, 2 ); 	// Poner cursor fuera
		xMutexFree(xLCD);
		vTaskDelay( MS(500) );
	}
}

/*==================[Functions]==============================================*/

unsigned int WaitForNFC(void){
	int i;
	unsigned int retVal;
    char message[NFC_MESSAGE_MAX];
    NxpNci_RfIntf_t RfInterface;
    TaskHandle_t taskWaitNFC = NULL;

	xTaskCreate(
		vWaitNFCmov,				// pvTaskCode - Funcion de la tarea a ejecutar
		(const char *)"NFCmov",		// pcName - Nombre de la tarea para debbug
		Stack(1),					// usStackDeph - Tamaño del stack de la tarea
		(void*)0,					// pvParameters - Parametros de tarea
		Priority(1),				// uxPriority - Prioridad de la tarea
		&taskWaitNFC				// pxCreatedTask - Puntero a la tarea creada en el sistema
	);

	while(NxpNci_WaitForDiscoveryNotification(&RfInterface) != NFC_SUCCESS);
	//xQueueReceive(xUserID, &retVal, portMAX_DELAY);	// Espera tarjeta NFC
	vTaskDelete(taskWaitNFC);				// Kills NFC animation Task

	if ((RfInterface.ModeTech & MODE_MASK) == MODE_POLL) {
		if( nfc_reader(RfInterface, message) == NFC_SUCCESS){
			retVal = atoi(message);
		}
	}
	return retVal;
}

void AskForPass(char pass[MAX_PASS+1]){
	int i;
	char opc;

	while (xQueueReceive(xBoton, &opc, FALSE));	// Descarta buffer previo del teclado

	xMutexTake(xLCD, portMAX_DELAY);
	lcdClear(); 		// Borrar la pantalla
	lcdGoToXY( 1, 1 ); 	// Poner cursor en 1, 1
	lcdSendStringRaw( "Input Password: " );
	// (16-4)x2%2 = [    * * * *     ]
	lcdGoToXY( 1+(( LCD_Hor / 2 ) - MAX_PASS), 2 );
	xMutexFree(xLCD);
	for ( i = 0; i < MAX_PASS; i++) {
		xQueueReceive(xBoton, &opc, portMAX_DELAY);
		pass[i] = opc;
		xMutexTake(xLCD, portMAX_DELAY);
		//lcdData(opc);	// DEBUG: Showing pass
		lcdData('*');
		lcdData(' ');
		xMutexFree(xLCD);
	}
	pass[MAX_PASS] = NULL;
	return;
}

void AddUser(user_t* users_db){
	char opc;
	user_t new_user;

	new_user.id = WaitForNFC();
	AskForPass(new_user.pass);

	xMutexTake(xLCD, portMAX_DELAY);
	lcdClear(); 		// Borrar la pantalla
	lcdGoToXY( 1, 1 ); 	// Poner cursor en 1, 1
	lcdSendStringRaw( "  Grant admin?  " );
	lcdGoToXY( 1, 2 ); 	// Poner cursor en 1, 2
	lcdSendStringRaw( " 1. Yes - 2. No " );
	xMutexFree(xLCD);

	xQueueReceive(xBoton, &opc, portMAX_DELAY);

	if (opc == '1')
		new_user.admin = TRUE;
	else
		new_user.admin = FALSE;

	opc = userAdd( users_db, new_user );

	xMutexTake(xLCD, portMAX_DELAY);
	lcdClear(); 		// Borrar la pantalla
	lcdGoToXY( 1, 1 ); 	// Poner cursor en 1, 1
	switch(opc) {
		case(-1): {
			lcdSendStringRaw( "   > ERROR! <   " );
			lcdGoToXY( 1, 2 ); 	// Poner cursor en 1, 2
			lcdSendStringRaw( "  User exists   " );
		}break;
		case(-2): {
			lcdSendStringRaw( "   > ERROR! <   " );
			lcdGoToXY( 1, 2 ); 	// Poner cursor en 1, 2
			lcdSendStringRaw( " No free space  " );

		}break;
		default: {
			lcdSendStringRaw( "  > SUCCESS <   " );
			lcdGoToXY( 1, 2 ); 	// Poner cursor en 1, 2
			lcdSendStringRaw( "   User added   " );
		}
	}
	xMutexFree(xLCD);
	vTaskDelay( MS(2000) );
}

void DelUser(user_t* users_db){
	char opc;
	unsigned int user_id;

	user_id = WaitForNFC();

	opc = userDel( users_db, user_id );

	xMutexTake(xLCD, portMAX_DELAY);
	lcdClear(); 		// Borrar la pantalla
	lcdGoToXY( 1, 1 ); 	// Poner cursor en 1, 1
	switch(opc) {
		case(-1): {
			lcdSendStringRaw( "   > ERROR! <   " );
			lcdGoToXY( 1, 2 ); 	// Poner cursor en 1, 2
			lcdSendStringRaw( " User not found " );
		}break;
		default: {
			lcdSendStringRaw( "  > SUCCESS <   " );
			lcdGoToXY( 1, 2 ); 	// Poner cursor en 1, 2
			lcdSendStringRaw( "  User deleted  " );
		}
	}
	xMutexFree(xLCD);
	vTaskDelay( MS(2000) );
}

void vApplicationTickHook ( void ){}

void vApplicationIdleHook ( void ){}
