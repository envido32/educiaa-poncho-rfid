// Includes de FreeRTOS
#include "FreeRTOS.h"
#include "FreeRTOSConfig.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"
#include "sapi.h"

// Definiciones
#define MS(t)					((portTickType) ( (t) / portTICK_RATE_MS))
#define Stack(s)				( configMINIMAL_STACK_SIZE * s )
#define Priority(p)				( tskIDLE_PRIORITY + p )

// Variables globales

// Prototipo de funciones de las tareas
void vMyTask( void* taskParmPtr );

// FUNCION PRINCIPAL
// "Hola Mundo!" en LCD
int main(void)
{
	// Init. hardware
	boardConfig();

	// Led para dar señal de vida
	gpioWrite( LED3, ON );

	// Crear tareas freeRTOS
	xTaskCreate(
		vMyTask,			// pvTaskCode -Funcion de la tarea a ejecutar
		(const char *)"MyTask",		// pcName - Nombre de la tarea para debbug
		Stack(2),			// usStackDeph - Tamaño del stack de la tarea
		(void*)0,			// pvParameters - Parametros de tarea
		Priority(1),			// uxPriority - Prioridad de la tarea
		NULL				// pxCreatedTask - Puntero a la tarea creada en el sistema
	);

	// Iniciar scheduler
	vTaskStartScheduler();

	// Endless loop
	while(TRUE)
	{
	// ERROR: Si cae en este while 1 significa que no pudo iniciar el scheduler
	}
	return 0;
}

// Implementaciones de funciones de las tareas
void vMyTask( void* taskParmPtr )
{
	// Init. Config
	// Inicializar LCD de 16x2 (caracteres x lineas) con cada caracter de 5x2 pixeles
	lcdInit( 16, 2, 5, 8 );

	// Endless loop
	while(TRUE)
	{
		lcdClear(); 		// Borrar la pantalla
		lcdGoToXY( 1, 1 ); 	// Poner cursor en 1, 1
		lcdSendStringRaw( "Hola, Mundo!" );
		vTaskDelay( MS(3000) );
	}
}
