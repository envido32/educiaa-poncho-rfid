/* Copyright 2018, Manuel Cajide Maidana.
 * All rights reserved.
 *
 * This file is part sAPI library for microcontrollers.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/

#include "userslib.h"

/*==================[external functions declaration]=========================*/


// Inicialize in NULL a new users_db[MAX_DB]
void userInit( user_t* users_db ){
	int i,j;
	for( i = 0; i < MAX_DB; i++ ){
		users_db[i].id = NULL;
		users_db[i].admin = FALSE;
		for( j = 0; j < MAX_PASS + 1; j++ )
			users_db[i].pass[j] = NULL;
	}
}

/* Searchs in users_db[MAX_DB] if user_id exist
 * RETURN: Position in users_db.
 *		-1 is FAILED. User not found.
 */
int userSearch( user_t* users_db, unsigned int user_id ){
	int i;
	for( i = 0; i < MAX_DB; i++ ){
		if( users_db[i].id == user_id )
			return i;	// Found! Return position in DB array
	}
	return -1;			// Not found!
}

// Sorts users_db from higer user_id to lower (NULL to the end)
void userSort( user_t* users_db ){
	int i,j;
	user_t aux;
	for ( i = 0 ; i < MAX_DB ; i++ ){
		for ( j = 0; j < MAX_DB-i ; j++ ){ 							// Es menor y no menor igual
			if ( users_db[j].id < users_db[j+1].id ){		// Ordenar de Mayor a Menor (ceros al final)
				aux=users_db[j];
				users_db[j]=users_db[j+1];
				users_db[j+1]=aux;
			}
		}
	}
	return;
}

/* Adds a user_t to the users_db in first avaliable position
 * RETURN: Position where new_user was added.
 * 		-1 is FAILED. User already exists.
 * 		-2 is FAILED. DB is full
 */
int userAdd( user_t* users_db, user_t new_user ){
	int retVal;

	retVal = userSearch(users_db, new_user.id);
	if ( retVal != -1 ){
		return -1; 	// Cannot add, user already exists.
	}

	retVal = userSearch(users_db, NULL);	// Search first empty position
	if ( retVal == -1 ){
		return -2; 	// Cannot add, DB is full.
	}

	users_db[retVal] = new_user;
	return retVal;
}

/* Deletes a user by id in the users_db
 * RETURN: Position where new_user was deleted.
 * 		-1 is FAILED. User does not exists.
 */
int userDel( user_t* users_db , unsigned int user_id ){
	int retVal;
	int i;

	retVal = userSearch(users_db, user_id);
	if ( retVal == -1 ){
		return -1; 	// Cannot delete, user does not exists.
	}

	users_db[retVal].id = NULL;
	users_db[retVal].admin = FALSE;
	for( i = 0; i < MAX_PASS ; i++ )
		users_db[retVal].pass[i] = NULL;

	return retVal;
}

