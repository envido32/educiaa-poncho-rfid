// Includes de FreeRTOS
#include "FreeRTOS.h"
#include "FreeRTOSConfig.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"
#include "sapi.h"
#include "string.h"

// Definiciones
#define MS(t)			((portTickType) ( (t) / portTICK_RATE_MS))
#define Stack(s)		( configMINIMAL_STACK_SIZE * s )
#define Priority(p)		( tskIDLE_PRIORITY + p )
#define BTN_BUFF		( 32 )

// Variables globales
xQueueHandle xBoton = NULL;		// Boton apretado

// Prototipo de funciones de las tareas
void vSaveInEEPROM( void* taskParmPtr );
void vBtn( char btn );

// Implementaciones de funciones de las tareas
void vSaveInEEPROM( void* taskParmPtr )
{
	// Init. Config
	char tecla = 0;
	int addr = 0;	// Max 16*1024  = 16kb
	Board_EEPROM_init();

	while (TRUE) {

		tecla = Board_EEPROM_readByte(addr);
		gpioWrite( LEDB, OFF );
		switch (tecla){
			case(TEC1):{
				gpioWrite( LEDG, ON );
				vTaskDelay( MS (500));
				gpioWrite( LEDG, OFF );
				vTaskDelay( MS (500));
				gpioWrite( LEDG, ON );
				vTaskDelay( MS (500));
				gpioWrite( LEDG, OFF );
			}break;
			case(TEC2):{
				gpioWrite( LED1, ON );
				vTaskDelay( MS (500));
				gpioWrite( LED1, OFF );
				vTaskDelay( MS (500));
				gpioWrite( LED1, ON );
				vTaskDelay( MS (500));
				gpioWrite( LED1, OFF );
			}break;
			case(TEC3):{
				gpioWrite( LED2, ON );
				vTaskDelay( MS (500));
				gpioWrite( LED2, OFF );
				vTaskDelay( MS (500));
				gpioWrite( LED2, ON );
				vTaskDelay( MS (500));
				gpioWrite( LED2, OFF );
			}break;
			case(TEC4):{
				gpioWrite( LED3, ON );
				vTaskDelay( MS (500));
				gpioWrite( LED3, OFF );
				vTaskDelay( MS (500));
				gpioWrite( LED3, ON );
				vTaskDelay( MS (500));
				gpioWrite( LED3, OFF );
			}break;
			case(-1):{
				gpioWrite( LEDR, ON );
				vTaskDelay( MS (500));
				gpioWrite( LEDR, OFF );
				vTaskDelay( MS (500));
				gpioWrite( LEDR, ON );
				vTaskDelay( MS (500));
				gpioWrite( LEDR, OFF );
			}break;
			default: {
				vTaskDelay( MS (500));
				gpioWrite( LEDB, ON );
				vTaskDelay( MS (500));
				gpioWrite( LEDB, OFF );
				vTaskDelay( MS (500));
			}
		}
		gpioWrite( LEDB, ON );
		xQueueReceive(xBoton, &tecla, portMAX_DELAY);   // Toma el semaforo

		if ( Board_EEPROM_writeByte(addr, tecla) != -1 ){
			gpioWrite( LEDG, ON );
			vTaskDelay( MS (250));
			gpioWrite( LEDG, OFF );
			vTaskDelay( MS (250));
			gpioWrite( LEDG, ON );
			vTaskDelay( MS (250));
			gpioWrite( LEDG, OFF );
		}
		else{
			gpioWrite( LEDR, ON );
			vTaskDelay( MS (250));
			gpioWrite( LEDR, OFF );
			vTaskDelay( MS (250));
			gpioWrite( LEDR, ON );
			vTaskDelay( MS (250));
			gpioWrite( LEDR, OFF );
		}
		vPortYield();
	}
}

void vBtn( char btn ) {
	// Init. Config
	char btn_status = 0;	// 0 = Hi, 1 = Timer, 2 = Lo

	// Endless loop
	while(TRUE) {
		// Maquina de estados: detecta flanco de subida
		switch (btn_status) {
			case (0): { // Hi
				if (!gpioRead( btn ))
					btn_status = 1;
			}break;
			case (1): { // Timer
			// Espera que estabilize
				vTaskDelay( MS(25) );
				if ( !gpioRead( btn )) {
					xQueueSend(xBoton, &btn, FALSE);
					btn_status = 2;
				}
				else
					btn_status = 0;
			}break;
			case (2): { // Lo
				if ( gpioRead( btn ))
					btn_status = 0;
			}break;
			default: btn_status = 0;
		}
		vPortYield();
	}
}

// FUNCION PRINCIPAL
// Prueba de grabado entarjeta SD (protocolo SPI)
int main(void) {
	// Init. hardware
	boardConfig();
	xBoton = xQueueCreate(BTN_BUFF, sizeof(char));

	// Led para dar señal de vida
	gpioWrite( LEDB, ON );

	// Crear tareas freeRTOS
	xTaskCreate(
		vSaveInEEPROM,			// pvTaskCode -Funcion de la tarea a ejecutar
		(const char *)"SaveInEEPROM",	// pcName - Nombre de la tarea para debbug
		Stack(2),			// usStackDeph - Tamaño del stack de la tarea
		(void * const)0,		// pvParameters - Parametros de tarea
		Priority(1),			// uxPriority - Prioridad de la tarea
		NULL				// pxCreatedTask - Puntero a la tarea creada en el sistema
	);


	xTaskCreate(
		vBtn,						// pvTaskCode -Funcion de la tarea a ejecutar
		(const char *)"BotonA",		// pcName - Nombre de la tarea para debbug
		Stack(2),					// usStackDeph - Tamaño del stack de la tarea
		(void * const)TEC1,						// pvParameters - Parametros de tarea
		Priority(1),				// uxPriority - Prioridad de la tarea
		NULL						// pxCreatedTask - Puntero a la tarea creada en el sistema
	);
	xTaskCreate(
		vBtn,						// pvTaskCode -Funcion de la tarea a ejecutar
		(const char *)"BotonB",		// pcName - Nombre de la tarea para debbug
		Stack(2),					// usStackDeph - Tamaño del stack de la tarea
		(void * const)TEC2,						// pvParameters - Parametros de tarea
		Priority(1),				// uxPriority - Prioridad de la tarea
		NULL						// pxCreatedTask - Puntero a la tarea creada en el sistema
	);
	xTaskCreate(
		vBtn,						// pvTaskCode -Funcion de la tarea a ejecutar
		(const char *)"BotonC",		// pcName - Nombre de la tarea para debbug
		Stack(2),					// usStackDeph - Tamaño del stack de la tarea
		(void * const)TEC3,						// pvParameters - Parametros de tarea
		Priority(1),				// uxPriority - Prioridad de la tarea
		NULL						// pxCreatedTask - Puntero a la tarea creada en el sistema
	);
	xTaskCreate(
		vBtn,						// pvTaskCode -Funcion de la tarea a ejecutar
		(const char *)"BotonD",		// pcName - Nombre de la tarea para debbug
		Stack(2),					// usStackDeph - Tamaño del stack de la tarea
		(void * const)TEC4,						// pvParameters - Parametros de tarea
		Priority(1),				// uxPriority - Prioridad de la tarea
		NULL						// pxCreatedTask - Puntero a la tarea creada en el sistema
	);

	// Iniciar scheduler
	vTaskStartScheduler();

	// Endless loop
	while(TRUE) {}	// ERROR: Si cae en este while 1 significa que no pudo iniciar el scheduler
	return 0;
}

void vApplicationTickHook ( void ){}

void vApplicationIdleHook ( void ){}
