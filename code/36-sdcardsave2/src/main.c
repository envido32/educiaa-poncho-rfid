// Includes de FreeRTOS
#include "FreeRTOS.h"
#include "FreeRTOSConfig.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"
#include "sapi.h"
#include "ff.h"			// Biblioteca FAT FS
#include "string.h"
#include "userslib.h"

// Definiciones
#define MS(t)			((portTickType) ( (t) / portTICK_RATE_MS))
#define Stack(s)		( configMINIMAL_STACK_SIZE * s )
#define Priority(p)		( tskIDLE_PRIORITY + p )

// Variables globales
static FATFS fs;		// FatFs work area needed for each volume
static FIL fp;			// File object needed for each open file
xSemaphoreHandle xRun = NULL;   // Semaforo disparador

// Prototipo de funciones de las tareas
void vSaveInSD( void* taskParmPtr );
void vBtn( char btn );
void diskTask( void *taskParmPtr );

// Implementaciones de funciones de las tareas
void vSaveInSD( void* taskParmPtr )
{
	// Init. Config
	unsigned int nbytes;
	int i = 0;
	//char FILENAME[] = "newfile2.txt";
	char TEXT[] = "Hola, Mundo?!";

	char FILENAME[] = "users.nfc";
	int fstate;
	user_t data;
	data.id = 0x7FFFDDAA;
	strcpy(data.pass,"5678");
	data.admin = TRUE;

	spiConfig( SPI0 );

	// Endless loop
	while(TRUE) {
		xSemaphoreTake(xRun, portMAX_DELAY);
		// Monta la tarjeta SD
		fstate = f_mount( &fs, "", 0 );
		if( fstate == FR_OK ){
			// Exito: LED verde largo
			gpioWrite( LEDG, ON );
			vTaskDelay( MS(500) );
			gpioWrite( LEDG, OFF );
		}
		else {
			// Falla: LED rojo largo
			for (i = 0; i < fstate ; i++){
				gpioWrite( LEDR, ON );
				vTaskDelay( MS(250) );
				gpioWrite( LEDR, OFF );
				vTaskDelay( MS(250) );
			}
		}

		// Abre el archivo FILENAME
		fstate = f_open( &fp, FILENAME, FA_WRITE | FA_CREATE_ALWAYS );
		if( fstate == FR_OK ){
			// Escrible el string TEXT
			fstate = sizeof(data);
			f_write( &fp, &data, fstate, &nbytes );
			// Cierra (guarda) el archivo.
			f_close(&fp);

			// Verifica que lo guardado es lo correcto
			if( nbytes == sizeof(data) ){
				// Exito: LED verde corto
				gpioWrite( LEDG, ON );
				vTaskDelay( MS(250) );
				gpioWrite( LEDG, OFF );
			}
			else {
				// Falla: LED rojo corto
				for (i = 0 ; i < fstate ; i++){
					gpioWrite( LEDR, ON );
					vTaskDelay( MS(250) );
					gpioWrite( LEDR, OFF );
					vTaskDelay( MS(250) );
				}
			}
		}
		else {
		// Turn ON LEDR if the write operation was fail
			for (i = 0 ; i < fstate ; i++){
				gpioWrite( LEDR, ON );
				vTaskDelay( MS(250) );
				gpioWrite( LEDR, OFF );
				vTaskDelay( MS(250) );
			}
		}
		vPortYield();
	}
}

void vBtn( char btn ) {
	// Init. Config
	char btn_status = 0;	// 0 = Hi, 1 = Timer, 2 = Lo
	gpioConfig( btn, GPIO_INPUT );

	// Endless loop
	while(TRUE) {
		// Maquina de estados: detecta flanco de subida
		switch (btn_status) {
			case (0): { // Hi
				if (!gpioRead( btn ))
					btn_status = 1;
			}break;
			case (1): { // Timer
			// Espera que estabilize
				vTaskDelay( MS(25) );
				if ( !gpioRead( btn )) {
					xSemaphoreGive(xRun);   // Libera el semaforo
					btn_status = 2;
				}
				else
					btn_status = 0;
			}break;
			case (2): { // Lo
				if ( gpioRead( btn ))
					btn_status = 0;
			}break;
			default: btn_status = 0;
		}
		vPortYield();
	}
}

void diskTask( void *taskParmPtr ) {
	while(TRUE) {
		disk_timerproc();	// Disk timer process
		vTaskDelay( MS(10) );
	}
}

// FUNCION PRINCIPAL
// Prueba de grabado entarjeta SD (protocolo SPI)
int main(void) {
	// Init. hardware
	boardConfig();
	vSemaphoreCreateBinary(xRun);
	xSemaphoreTake(xRun, FALSE);

	// Led para dar señal de vida
	gpioWrite( LED3, ON );

	// Crear tareas freeRTOS
	xTaskCreate(
		vSaveInSD,			// pvTaskCode -Funcion de la tarea a ejecutar
		(const char *)"SaveInSD",	// pcName - Nombre de la tarea para debbug
		Stack(2),			// usStackDeph - Tamaño del stack de la tarea
		(void * const)0,		// pvParameters - Parametros de tarea
		Priority(1),			// uxPriority - Prioridad de la tarea
		NULL				// pxCreatedTask - Puntero a la tarea creada en el sistema
	);

	xTaskCreate(
		vBtn,				// pvTaskCode -Funcion de la tarea a ejecutar
		(const char *)"BotonA",		// pcName - Nombre de la tarea para debbug
		Stack(2),			// usStackDeph - Tamaño del stack de la tarea
		(void * const)TEC1,		// pvParameters - Parametros de tarea
		Priority(1),			// uxPriority - Prioridad de la tarea
		NULL				// pxCreatedTask - Puntero a la tarea creada en el sistema
	);

	xTaskCreate( 
		diskTask,			// pvTaskCode -Funcion de la tarea a ejecutar
		(const char *)"diskTask",	// pcName - Nombre de la tarea para debbug
		Stack(2),			// usStackDeph - Tamaño del stack de la tarea
		(void * const)0,		// pvParameters - Parametros de tarea
		Priority(3),			// uxPriority - Prioridad de la tarea
		NULL				// pxCreatedTask - Puntero a la tarea creada en el sistema
	);

	// Iniciar scheduler
	vTaskStartScheduler();

	// Endless loop
	while(TRUE) {
	// ERROR: Si cae en este while 1 significa que no pudo iniciar el scheduler
	}
	return 0;
}

void vApplicationTickHook ( void ){}

void vApplicationIdleHook ( void ){}
