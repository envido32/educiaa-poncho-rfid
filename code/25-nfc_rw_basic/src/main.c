#define RW_SUPPORT
#define RW_NDEF_WRITING

// Includes de FreeRTOS
#include "FreeRTOS.h"
#include "FreeRTOSConfig.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"
#include "sapi.h"
#include <tool.h>
#include <Nfc.h>
#include <ndef_helper.h>

// Definiciones
#define Stack(s)	( configMINIMAL_STACK_SIZE + s )
#define Priority(p)	( tskIDLE_PRIORITY + p )
//#define NFC_ADDR	( PN532_I2C_ADDRESS )	// PN532. Dato del fabricante
#define NFC_ADDR	( NXPNCI_I2C_ADDR )	// PN7150. Dato del fabricante
#define NFC_BUFF1	( 32 )
#define NFC_BUFF2	( 32 )
#define TASK_NFC_STACK_SIZE		1024
#define TASK_NFC_STACK_PRIO		(configMAX_PRIORITIES - 1)

/* Discovery loop configuration according to the targeted modes of operation */
unsigned char DiscoveryTechnologies[] = {
    MODE_POLL | TECH_PASSIVE_NFCA,
    MODE_POLL | TECH_PASSIVE_NFCF,
    MODE_POLL | TECH_PASSIVE_NFCB,
    MODE_POLL | TECH_PASSIVE_15693,
};

/* Mode configuration according to the targeted modes of operation */
unsigned mode = 0 | NXPNCI_MODE_RW;

#ifdef RW_NDEF_WRITING
const char NDEF_MESSAGE[] = { 0xD1,   // MB/ME/CF/1/IL/TNF
        0x01,   // TYPE LENGTH
        0x07,   // PAYLOAD LENTGH
        'T',    // TYPE
        0x02,   // Status
        'e', 'n', // Language
        'T', 'e', 's', 't' };

void NdefPush_Cb(unsigned char *pNdefRecord, unsigned short NdefRecordSize) {
    //printf("--- NDEF Record sent\n\n");
}

#endif // RW_NDEF_WRITING

void NdefPull_Cb(unsigned char *pNdefMessage, unsigned short NdefMessageSize);
void read_nfc(void);
void task_nfc_reader(NxpNci_RfIntf_t RfIntf);



// FUNCION PRINCIPAL
// Cuenta la cantidad de veces que se presiona la tecla 1, luego al presionar la tecla 2 titila un LED la cantidad de veces que se presiono la tecla 1.
int main(void)
{
	// Init. Config

	// Init. hardware
	boardConfig();
	// Inicializar LCD de 16x2 (caracteres x lineas) con cada caracter de 5x2 pixeles
	//lcdInit( 16, 2, 5, 8 );

	//lcdClear(); 		// Borrar la pantalla
	//lcdGoToXY( 1, 1 ); 	// Poner cursor en 1, 1
	//lcdSendStringRaw( "Hola, Mundo!" );

	// Crear tareas freeRTOS
	xTaskCreate((TaskFunction_t) read_nfc,
	    				(const char*) "read_NFC",
						TASK_NFC_STACK_SIZE,
						NULL,
						TASK_NFC_STACK_PRIO,
						NULL);

	// Iniciar scheduler
	vTaskStartScheduler();

	// Endless loop
	while( TRUE )
	{
	// ERROR: Si cae en este while 1 significa que no pudo iniciar el scheduler
	}
	return 0;
}


void read_nfc(void){
    NxpNci_RfIntf_t RfInterface;

    /* Register callback for reception of NDEF message from remote cards */
    RW_NDEF_RegisterPullCallback(*NdefPull_Cb);

    /* Open connection to NXPNCI device */
    if (NxpNci_Connect() == NFC_ERROR) {
        //printf("Error: cannot connect to NXPNCI device\n");
        return;
    }
    if (NxpNci_ConfigureSettings() == NFC_ERROR) {
        //printf("Error: cannot configure NXPNCI settings\n");
        return;
    }
    if (NxpNci_ConfigureMode(mode) == NFC_ERROR) {
        //printf("Error: cannot configure NXPNCI\n");
        return;
    }
    /* Start Discovery */
    if (NxpNci_StartDiscovery(DiscoveryTechnologies,sizeof(DiscoveryTechnologies)) != NFC_SUCCESS) {
        //printf("Error: cannot start discovery\n");
        return;
    }

    while(true) {
        //printf("\nWAITING FOR DEVICE DISCOVERY\n");

        /* Wait until a peer is discovered */
        while(NxpNci_WaitForDiscoveryNotification(&RfInterface) != NFC_SUCCESS);

        if ((RfInterface.ModeTech & MODE_MASK) == MODE_POLL) {
            task_nfc_reader(RfInterface);
        }
        else {
            //printf("WRONG DISCOVERY\n");
        }
    }
}


void task_nfc_reader(NxpNci_RfIntf_t RfIntf) {
    /* For each discovered cards */
    while(true){
        /* Display detected card information */
        //displayCardInfo(RfIntf);

        /* What's the detected card type ? */
        switch(RfIntf.Protocol) {
            case PROT_T1T:
            case PROT_T2T:
            case PROT_T3T:
            case PROT_ISODEP: {
                /* Process NDEF message read */
                NxpNci_ProcessReaderMode(RfIntf, READ_NDEF);
                #ifdef RW_NDEF_WRITING
					RW_NDEF_SetMessage ((unsigned char *) NDEF_MESSAGE, sizeof(NDEF_MESSAGE), *NdefPush_Cb);
					/* Process NDEF message write */
					NxpNci_ReaderReActivate(&RfIntf);
					NxpNci_ProcessReaderMode(RfIntf, WRITE_NDEF);
				#endif // RW_NDEF_WRITING
            	}break;
            case PROT_ISO15693:
                break;
            case PROT_MIFARE:
                break;
            default:
                break;
        }

        /* If more cards (or multi-protocol card) were discovered (only same technology are supported) select next one */
        if(RfIntf.MoreTags) {
            if(NxpNci_ReaderActivateNext(&RfIntf) == NFC_ERROR) break;
        }
        /* Otherwise leave */
        else break;
    }

    /* Wait for card removal */
    NxpNci_ProcessReaderMode(RfIntf, PRESENCE_CHECK);

    printf("CARD REMOVED\n");

    /* Restart discovery loop */
    NxpNci_StopDiscovery();
    NxpNci_StartDiscovery(DiscoveryTechnologies,sizeof(DiscoveryTechnologies));
}


void NdefPull_Cb(unsigned char *pNdefMessage, unsigned short NdefMessageSize){
    unsigned char *pNdefRecord = pNdefMessage;
    NdefRecord_t NdefRecord;
    unsigned char save;
    unsigned char message[] = "ORIGINAL TEST MESSAGE";

    if (pNdefMessage == NULL){
        //printf("--- Provisioned buffer size too small or NDEF message empty \n");
        return;
    }

    while (pNdefRecord != NULL){
        //printf("--- NDEF record received:\n");
        NdefRecord = DetectNdefRecordType(pNdefRecord);

        switch(NdefRecord.recordType){
            case MEDIA_VCARD:
                break;
            case WELL_KNOWN_SIMPLE_TEXT:{
                    save = NdefRecord.recordPayload[NdefRecord.recordPayloadSize];
                    NdefRecord.recordPayload[NdefRecord.recordPayloadSize] = '\0';
                    //printf("   Text record: %s\n", &NdefRecord.recordPayload[NdefRecord.recordPayload[0]+1]);
                    strcpy(message, &NdefRecord.recordPayload[NdefRecord.recordPayload[0]+1]);
                    NdefRecord.recordPayload[NdefRecord.recordPayloadSize] = save;
                }break;
            case WELL_KNOWN_SIMPLE_URI:
                break;
            case MEDIA_HANDOVER_WIFI:
                break;
            case WELL_KNOWN_HANDOVER_SELECT:
                break;
            case WELL_KNOWN_HANDOVER_REQUEST:
                break;
            case MEDIA_HANDOVER_BT:
                break;
            case MEDIA_HANDOVER_BLE:
                break;
            case MEDIA_HANDOVER_BLE_SECURE:
                break;
            default:
                break;
        }
        pNdefRecord = GetNextRecord(pNdefRecord);
    }
    printf("\n");
}

