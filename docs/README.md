# BITACORA

## Semana 1 (2018 \#38 Septiembre 16)

### Instalación de CIAA-IDE:

Quise instalar el entorno de programación primero en mi PC que tiene Mint 14.04 LTS (quizas algún día lo actualice) usando el [script](http://www.proyecto-ciaa.com.ar/devwiki/lib/exe/fetch.php?media=docu:fw:bm:ide:ciaa_ide_setup.sh.tar.gz) pero falló. 

Luego intenete en otra PC con Xubuntu 18.04 y también falló. 

Así que pase a [instalarlo a mano](http://www.proyecto-ciaa.com.ar/devwiki/doku.php?id=docu:fw:bm:ide:install_linux) y falló menos... 

Pero después de gritarle al monitor unas horas salió andando. 

Dicen que la instalación en Windows esta mejor armada, pero si me dan a elegir entre usar Windows y un tiro en el pie, elijo el izquierdo.

**Conclución**: Hace falta armar un script automático de instalación en linux actualizado y probado. No debería ser dificil pero es engorroso y lleva tiempo. Algún día cuando tenga tiempo y entienda lo que estoy haciendo espero poder hacerlo así ustedes no sufren tanto. 

Les dejo uno que puede servir pero le falta debugging. 

- [ciaa_ide_setup.sh](../ciaa_ide_setup.sh)

### Uso de la IDE:

Esta basada en [*Eclipse*](https://www.eclipse.org/) una IDE muy completa y conocida pero bastante pesada en RAM. Intente usarla en una [netbook K vieja](https://es.wikipedia.org/wiki/Conectar_Igualdad) (mucho más cómoda de transportar) pero rápidamente me pidió que termine con su sufrimiento. Quizás con una IDE más liviana (por ejemplo [*CodeLite*](https://www.codelite.org/)) bajan los requerimietos sin tener que llegar a la locura de trabajar 100% por consola... aunque para gustos... 

Requiere de complementos y configuraciones para funcionar correctamente:

- [*Toolchain de Arm*](https://developer.arm.com/open-source/gnu-toolchain/gnu-rm/downloads): Utilizado para compiacion del codigo para micros Arm.
- [*OpenOCD*](http://openocd.org/): Utilizado para el debbuging paso-a-paso por USB.
- [*git*](https://git-scm.com/): Opcional pero recomendado para hacer versionado del código. Hay muchas formas de usarlo pero hoy en día recomiendo usar GitLab ya que es la más completa y fácil de usar en su version gratuita. Evitar GitHub ya que [lo compró Microsoft](https://news.microsoft.com/2018/06/04/microsoft-to-acquire-github-for-7-5-billion/) y como todes sabemos Microsoft sacrifica gatitos en honor a satanás.
- [*Eclipse Plugins*](http://www.proyecto-ciaa.com.ar/devwiki/doku.php?id=docu:fw:bm:ide:ide_primeros_pasos): Para funcionar correctamente hay que vincular esto con unos plugins que esta explicado en el [manual de instalacion](http://www.proyecto-ciaa.com.ar/devwiki/doku.php?id=docu:fw:bm:ide:install_linux).

### Primer código: 

Empece por lo básico: un LED titilando. Me base en el [CIAA Firmware Upa 1.0.0 LTS](https://github.com/ciaa/Firmware/releases) que decía ser estable y depurado.

Si llegaron a esta etapa sin considerar mudarse a Purmamarca a plantar tapioca: FELICIDADES! La parte más aburrida ya terminó y sólo se hace una vez. Yo me tope con varios inconvenientes de configuraciones, librerías desactualizadas o incompatibles, sin estar seguro que hacían muchos de los pasos o porque fallaban. Revisando la [Wiki de CIAA](http://www.proyecto-ciaa.com.ar/devwiki/doku.php?id=start), [Googleando](http://lmgtfy.com/?s=d&q=rickroll) el resto y con saliva y paciencia todo salió andando (también la netbook pero muy lenta, ya voy a probar con otra IDE menos exigente).

Además cuando este el script de instalación andando como corresponde debería tomar menos de 5 minutos hacer una instalación limpia y lista para hacer pruebas.

Leyendo el código del Firmware me di cuenta que faltaban varias de las capacidades que sabia que la EduCIAA tenia y rápidamente me tope con el [Firmware v2](https://github.com/ciaa/firmware_v2/releases) que si bien aún esta en desarrollo, es más completo y tiene más ejemplos implementando la librería [sAPI](https://github.com/epernia/sAPI/releases).

## Semana 2 (2018 \#39, Septiembre 23)

Esta fue una semana mas feliz. Empece por repasar los ejemplos con la grata sorpresa que tiene ejemplos para todos los gustos. Los mas fáciles de usar son los de la libre librería [sAPI](https://github.com/epernia/sAPI) incluyendo [I2C](https://www.i2c-bus.org/) y [SPI](https://learn.sparkfun.com/tutorials/serial-peripheral-interface-spi) que son los que necesito para conectarme con el modulo RFID que seguramente este basado en [PN532](https://www.nxp.com/products/identification-and-security/nfc/nfc-reader-ics/nfc-integrated-solution:PN5321A3HN) o similar.

sAPI esta muy bien documentada y es más fácil de usar que el [CMSIS](https://developer.arm.com/embedded/cmsis) directamente ya que fue realizada con la CIAA en mente. También incluye ejemplos con FreeRTOS y Display LCD, y ademas en la semana compre un [Poncho Educativo](http://www.proyecto-ciaa.com.ar/devwiki/doku.php?id=repo:ponchos:educativounsj) para realizar aun más pruebas de hardware. Vino listo para usar, solo tuve que soltar los pines hembra a la placa y conectar.

Estuve haciendo pruebas varias: cambiando LED de color, escribiendo cosas en el display, apretando botones, etc. Nada realmente útil pero me ayudo a familiarizarme con el entorno y las fallas, entrar en confianza y tener un pequeño pico de dopamina cada vez que [algo hacia lo que yo queria](https://youtu.be/xos2MnVxe-c).

## Semana 3 (2018 \#40, Septiembre 30)

Esta semana no pude dedicarle tanto tiempo como me hubiese gustado. Estuve puliendo algunos ejemplos pero aun no los subí ya que quiero emprolijarlos antes.

Ademas estuve haciendo pruebas para mejorar el script de instalación para Ubuntu, aun no logre encontrar automatizar la instalación de Eclipse por completo pero las librerías y herramientas están todas. 

También me puse a probar el CodeLite como IDE pero el Makefile no es compatible así que hay que hacerlo de nuevo y no se si vale la pena el tiempo para investigarlo... 

## Semana 4 (2018 \#41, Octubre 07)

Ahora en el directorio [/code](../code) fui subiendo diferentes pruebas que fui haciendo de acuerdo al avanece de la metería. A diferencia de lo que se encuentra en [/examples](../examples) esto lo fui desarrollando yo desde cero y esta compilado y probado con éxito. Deje comentarios de explicaciones simples de su funcionamiento.

## Semana 8 ( 2018 \#45, Noviembre 04 )

Si bien pase varias semanas sin volcar en la bitácora el avance fue sostenido. Yay!

### It verks!

Hice muchas pruebas de código y ya hay un control de acceso básico funcionando en [17-access_mutex](../code/17-access_mutex). En el tenemos funcionando: **Display 16x2, Keypad 4x4, EEPROM, Buzzer**. Hecho en FreeRTOS con colas, mutex semáforos y esas cosas lindas de comunicación interna. 

También hice pruebas para grabar cosas en la **SDcard** que usa **protocolo SPI** y una librería llamada [FatFs by ChaN](http://elm-chan.org/fsw/ff/00index_e.html) para el manejo del **FileSystem**. Amo que este tan bien documentado, el proyecto se encuentra activo y ademas de funcionar muy bien es muy completo

### NFC y la lucha
A diferencia de NFC que me esta volviendo loco: decidí comprar un [modulo básico para Arduino](https://www.amazon.com/HiLetgo-Communication-Arduino-Raspberry-Android/dp/B01I1J17LC), tiene comunicación por I2C, SPI o HSU. Es baratito y se consigue fácil, probarlo con los ejemplos de Arduino fue rapido... pero hasta ahí terminan las historias felices.

**Librerias**

Primero es necesario adaptar las [librerías de Arduino](https://github.com/Seeed-Studio/PN532) que están hechas en una especie de C++ simplificado así que nada funciona así nomas... PERO NO ME VA A GANAR. Y aunque documentación hay mucha esta desordenada, por un lado la info del integrado, por otra la de NFC y por otra la de I2C... aunque es todo parte de lo mismo aun estoy revisando bien la cosa, deberían aprender de Mr. ChaN <3.

**Integrados**

Luego me puse a investigar para diseñar mi propia placa y para eso necesito el integrado [PN532](https://www.nxp.com/products/identification-and-security/nfc/nfc-reader-ics/nfc-integrated-solution:PN5321A3HN) usado por el modulito este... el cual ya esta siendo retirado del mercado  para ser reemplazado por el [PN7150](https://www.nxp.com/products/identification-and-security/nfc/nfc-reader-ics/pn7150-high-performance-nfc-controller-with-integrated-firmware-for-home-automation-applications:PN7150). Primer limitacion: solo funciona por I2C, asi que al carajo SPI o HSU... obviamente la mayoria de los ejemplos que ya tengo estan en HSU, y el SPI ya lo tenia andando por la SDcard... volvemos al principio.

Ok, empiezo a mandar mails y recorriendo locales de electrónica para averiguar que integrado para manejo de NFC (o RFID al menos) hay disponibles en Baires Furry City, y todos me ofrecieron el mismo modulo de Arduino (algunos no entienden la diferencia entre Modulo e Integrado) o peor y me miraron como si estuviese pidiendo empanadas en un McDonals.

Así que frustrado hice lo que debería haber hecho hace mucho tiempo... pedí Samples directamente a NXP de PN532 y PN7150, envío internacional gratuito cinco de cada uno. Llegarían directamente si es que la Aduana no tiene ganas de complicarme la vida al pedo.

Lo bueno es que ambos tienen los mismos encapsulados [SOT618](https://www.nxp.com/docs/en/package-information/SOT618-1.pdf). La contra de estos integrados es que son de montaje superficial, miden apenas 6x6mm y tienen 40 pines... te la regalo soldarlos a mano, algo voy a tener que inventar...

**Próximo paso**

Adelantar el PCB lo máximo posible, lograr hacer funcionar el modulo NFC y empezar a jugar con eso, orar para que los Samples lleguen a casa en tiempo y forma.
