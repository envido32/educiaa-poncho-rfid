/*
 *         Copyright (c), NXP Semiconductors
 *
 *                       (C)NXP Electronics N.V.2004
 *         All rights are reserved. Reproduction in whole or in part is
 *        prohibited without the written consent of the copyright owner.
 *    NXP reserves the right to make changes without notice at any time.
 *   NXP makes no warranty, expressed, implied or statutory, including but
 *   not limited to any implied warranty of merchantability or fitness for any
 *  particular purpose, or that the use will not infringe any third party patent,
 *   copyright or trademark. NXP must not be liable for any loss or damage
 *                            arising from its use.
 *
 */

/*!
 * \file phJalTransceiveFunctions.h
 * \brief Initiator Transceive Functions
 *
 *
 * Project: NFC MW / HAL
 *
 * $Date: Thu Sep 27 09:38:41 2007 $ 
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Aliases: SDK_HAL_2.2,SDK_HAL_2.2_071008,NFC_FRI_SDK_1.0_071031 $
 *
 */

#ifndef PHJALTRANSCEIVEFUNCTIONS_H
#define PHJALTRANSCEIVEFUNCTIONS_H


/*! \ingroup grp_file_attributes
 *  \name JAL Joiner
 *  File: \ref phJalTransceiveFunctions.h
 */
/*@{*/
#define PHJALTRANSCEIVEFUNCTIONS_FILEREVISION "$Revision: 1.1 $"
#define PHJALTRANSCEIVEFUNCTIONS_FILEALIASES  "$Aliases: SDK_HAL_2.2,SDK_HAL_2.2_071008,NFC_FRI_SDK_1.0_071031 $"
/*@}*/

#include <phcsBflStatus.h>

#include <phNfcTypes.h>
#include <phNfcHalTypes.h>
#include <phJalJoinerAux.h>

#define PSL_REQUIRED_BUFFER_LENGTH  2
#define FSL_VALUE                   0

#define BRIT_VALUE_POS              0
#define BRTI_VALUE_POS              1


/**
 * \ingroup grp_jal_nfci_aux_functions
 * \brief Function handling the Initiator part of the DEP protocol.
 *
 * Before processing the actual DEP Requests/Responses responsible structures
 * will be initialised and configured for proper operation. Afterwards
 * \ref HandleDEPAndWTX to start a DEP Request and handle incoming 
 * data or WTX Requests.
 */
NFCSTATUS phJal_DataExchangeProtocolRequest(phHal_sHwReference_t            *psHwReference,  
                                            phHal_sRemoteDevInformation_t   *psRemoteDevInfo,  
                                            phHal_sDepAdditionalInfo_t      *psDepAdditionalInfo,  
                                            uint8_t                         *pSendBuf,  
                                            uint16_t                        SendLength,  
                                            uint8_t                         *pRecvBuf,  
                                            uint16_t                        *pRecvLength);

/**
 * \ingroup grp_jal_nfci_aux_functions
 * \brief Function executing the PPS command of the Initiator.
 *
 * In case of a successful execution the new communication speed will be set
 * in the corresponding \ref phHal_sRemoteDevInformation_t structure.
 */
 NFCSTATUS phJal_ParameterSelectionRequest(phHal_sHwReference_t              *psHwReference,  
                                          phHal_sRemoteDevInformation_t     *psRemoteDevInfo,
                                          uint8_t                           *pSendBuf,  
                                          uint16_t                          SendLength);

/**
 * \ingroup grp_jal_nfci_aux_functions
 * \brief Function executing the ATT command of the Initiator.
 */
NFCSTATUS phJal_AttentionRequest(phHal_sHwReference_t               *psHwReference,  
                                 phHal_sRemoteDevInformation_t      *psRemoteDevInfo);

/**
 * \ingroup grp_jal_nfci_aux_functions
 * \brief Function transmitting/receiving raw commands.
 *
 * This function transparently sends the buffer content to the Target without
 * any further processing.
 */
NFCSTATUS phJal_TransceiveRawCommand(phHal_sHwReference_t        *psHwReference,  
                                     uint8_t                     *pSendBuf,  
                                     uint16_t                    SendLength,  
                                     uint8_t                     *pRecvBuf,  
                                     uint16_t                    *pRecvLength);

/**
 * \ingroup grp_jal_nfci_aux_functions
 * \brief Function transmitting/receiving Mifare commands.
 *
 * The BFL Frame will remember wether a key for authentication is available or not
 * (the key will be stored during authentication).
 * Once authenticated successfully, the key will automatically be used for all succeeding
 * calls of this function.
 */
NFCSTATUS phHalNfc_TransceiveMifareCommand(phHal_sHwReference_t             *psHwReference,
                                           phHal_sRemoteDevInformation_t    *psRemoteDevInfo,
                                           phHal_uCmdList_t                  Cmd,
                                           uint8_t                          *pSendBuf,
                                           uint16_t                         SendLength,
                                           uint8_t                          *pRecvBuf,
                                           uint16_t                         *pRecvLength);

/**
 * \ingroup grp_jal_nfci_aux_functions
 * \brief Function transmitting/receiving felica commands.
 *
 */
NFCSTATUS phHalNfc_TransceiveFelicaCommand(phHal_sHwReference_t             *psHwReference,
                                           phHal_sRemoteDevInformation_t    *psRemoteDevInfo,
                                           phHal_uCmdList_t                  Cmd,
                                           uint8_t                          *pSendBuf,
                                           uint16_t                         SendLength,
                                           uint8_t                          *pRecvBuf,
                                           uint16_t                         *pRecvLength);


/**
 * \ingroup grp_jal_nfci_aux_functions
 * \brief Function transmitting/receiving ISO 14443-4 commands.
 */
NFCSTATUS phHalNfc_TransceiveIso144434Command(phHal_sHwReference_t          *psHwReference,
                                              phHal_sRemoteDevInformation_t *psRemoteDevInfo,
                                              phHal_sDepAdditionalInfo_t    *psDepAdditionalInfo,
                                              phHal_uCmdList_t               Cmd,
                                              uint8_t                       *pSendBuf,
                                              uint16_t                      SendLength,
                                              uint8_t                       *pRecvBuf,
                                              uint16_t                      *pRecvLength);
/**
 * \ingroup grp_jal_nfci_aux_functions
 * \brief Function setting the NFC Mode and the communication speed of a 
 * \ref phHal_sRemoteDevInformation_t structure.
 */
NFCSTATUS phHalNfc_SetNewNfcRemoteDeviceOpMode(phHal_sRemoteDevInformation_t *psRemoteDevInfo,
                                                NfcModes                      CurrentMode,
                                                NfcSpeeds                     NewSpeed);  

/**
 * \ingroup grp_jal_nfci_aux_functions
 * \brief Function authenticating to a block of a Mifare card.
 *
 * The block for authentication, the authentication command, as well as the key, 
 * are stored in \ref phJal_sBFLUpperFrame_t::AuthenticatedBlockAddress, 
 * \ref phJal_sBFLUpperFrame_t::MifareAuthentCommand and 
 * \ref phJal_sBFLUpperFrame_t::CurrentMifareKey, respectively.
 */
phcsBfl_Status_t phJal_AuthenticateMifare(phHal_sHwReference_t             *psHwReference,
                                          phHal_sRemoteDevInformation_t    *psRemoteDevInfo);

/**
 * \ingroup grp_jal_nfci_aux_functions
 * \brief Function implementing the Initiator part of the DEP protocol.
 *
 * It's main purpose is the handle the DEP and WTX requests/response from the Target.  
 */
phcsBfl_Status_t HandleDEPAndWTX(phHal_sHwReference_t                *psHwReference,
                                 phJal_NfcIProtocol_t                *psProtocol,
                                 phcsBflNfc_InitiatorDepReqParam_t   *psDepRequestParam,
                                 uint16_t                            *pRecvLength);


#endif