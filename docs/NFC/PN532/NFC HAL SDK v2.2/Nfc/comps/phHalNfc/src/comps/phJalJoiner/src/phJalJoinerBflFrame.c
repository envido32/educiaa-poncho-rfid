/*
 *         Copyright (c), NXP Semiconductors
 *
 *                       (C)NXP Electronics N.V.2004
 *         All rights are reserved. Reproduction in whole or in part is
 *        prohibited without the written consent of the copyright owner.
 *    NXP reserves the right to make changes without notice at any time.
 *   NXP makes no warranty, expressed, implied or statutory, including but
 *   not limited to any implied warranty of merchantability or fitness for any
 *  particular purpose, or that the use will not infringe any third party patent,
 *   copyright or trademark. NXP must not be liable for any loss or damage
 *                            arising from its use.
 *
 */


/*!
 * \file phJalJoinerBflFrame.c
 * \brief For function declarations see \ref phJalJoinerBflFrame.h
 *
 *
 * Project: NFC MW / HAL
 *
 * $Date: Thu Sep 27 09:38:43 2007 $ 
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Aliases: SDK_HAL_2.2,SDK_HAL_2.2_071008,NFC_FRI_SDK_1.0_071031 $
 *
 */

#ifdef PHHAL_VERSION_ACQUISITION
    #define PHJALJOINERBFLFRAME_SOURCE_FILEREVISION "$Revision: 1.1 $"
    #define PHJALJOINERBFLFRAME_SOURCE_FILEALIASES  "$Aliases: SDK_HAL_2.2,SDK_HAL_2.2_071008,NFC_FRI_SDK_1.0_071031 $"
#else /* PHHAL_VERSION_ACQUISITION */

#include <string.h>

#include <phcsBflOpCtl_Hw1Ordinals.h>
#include <phcsBflHw1Reg.h>

#include <phNfcStatus.h>
#include <phOsalNfc.h>
#include <phJalJoinerBflFrame.h>
#include <phJalJoinerAux.h>
#include <phJalDebugReportFunctions.h>


const phHal_eOpModes_t PHJAL_P106[] = {phHal_eOpModesNfcPassive106,
                                       phHal_eOpModesISO14443_4A,
                                       phHal_eOpModesMifare,
                                       phHal_eOpModesArrayTerminator};
const phHal_eOpModes_t PHJAL_P106B[] = {phHal_eOpModesISO14443_4B,
                                        phHal_eOpModesArrayTerminator};
const phHal_eOpModes_t PHJAL_P212_424[] =  {phHal_eOpModesNfcPassive212,
                                            phHal_eOpModesNfcPassive424,
                                            phHal_eOpModesFelica212,
                                            phHal_eOpModesFelica424,
                                            phHal_eOpModesArrayTerminator};
const phHal_eOpModes_t PHJAL_A[] =  {phHal_eOpModesNfcActive106,
                                    phHal_eOpModesNfcActive212,
                                    phHal_eOpModesNfcActive424,
                                    phHal_eOpModesArrayTerminator};

const phHal_eOpModes_t *PHJAL_OPMODE_MATRIX[]=
{
    PHJAL_P106,
    PHJAL_P106B,
    PHJAL_P212_424,
    PHJAL_A,
    NULL
};


void phJal_BuildBflUpperFrame(phJal_sBFLUpperFrame_t   *bfl_frame,
                              uint8_t                  initiator__not_target,
                              phcsBflRegCtl_t          *rc_reg_ctl,
                              uint8_t                   do_initialise_chip)
{
    uint8_t i;

    phJal_Trace(NULL, "phJal_BuildBflUpperFrame");

    bfl_frame->TRxBufferSize = PHJAL_TRXBL;
    bfl_frame->CurrentTargetBufferLength = 0;
    bfl_frame->RcRegCtl = rc_reg_ctl;

    bfl_frame->BrsValue = 0;
    bfl_frame->FslValue = 0;

    /* Rc-I/O: */
    phcsBflIo_Hw1Init(&(bfl_frame->RcIo),
                      &(bfl_frame->RcIoP),
                      bfl_frame->RcRegCtl,
                      initiator__not_target);

    /* Operation Control component */
    phcsBflOpCtl_Hw1Init(&bfl_frame->RcOpCtl, 
                         &bfl_frame->RcOpCtlParams, 
                         bfl_frame->RcRegCtl);

    /* ISO 14443-3: */
    phcsBflI3P3A_Hw1Initialise(&(bfl_frame->Iso14443_3A),
                               &(bfl_frame->Iso14443_3P),
                               bfl_frame->RcRegCtl,
                               initiator__not_target);

    /* Fel-Pol: */

    phcsBflPolAct_Hw1Init(&(bfl_frame->FeliCaPolling),
                          &(bfl_frame->FeliCaPollingParams),
                          bfl_frame->RcRegCtl,
                          initiator__not_target);

    /* MF-Rd: */
    phcsBflMfRd_Init(&(bfl_frame->MfRd),
                     &(bfl_frame->MfRdP),
                     &(bfl_frame->RcIo),
                     (uint8_t*)(bfl_frame->TRxBuffer));

    /* FEL-Rd: */
    phcsBflFelRd_Init(&(bfl_frame->FelRd),
                      &(bfl_frame->FelRdP),
                      &(bfl_frame->RcIo),
                      (uint8_t*)(bfl_frame->TRxBuffer));
    

    /* NFC-T: */
    /* Dummy Init: Will be re-initialised by the Receive() function because of user-buffer. */
    phcsBflNfc_TargetInit(&(bfl_frame->NfcT),
                          &(bfl_frame->NfcTCp),
                          PHCS_BFLNFCCOMMON_PREAM_OMIT,
                          PHCS_BFLNFCCOMMON_PREAM_OMIT,
                          NULL,
                          0,
                          (uint8_t*)(bfl_frame->TNfcIdI),
                          (uint8_t*)(bfl_frame->TNfcIdT),
                          1,
                          &(bfl_frame->NfcTargetEndpoint),
                          &(bfl_frame->RcIo));

    bfl_frame->NfcTargetDispatchParam.self = &bfl_frame->NfcT;
    bfl_frame->TargetTimeout = 0x2E;
    bfl_frame->STMDoesSoftReset = 1;

    phcsBflI3P4AAct_Init(&(bfl_frame->ISO14443_4A_Act),
                         &(bfl_frame->ISO14443_4A_Pp),
                         bfl_frame->TRxBuffer,
                         bfl_frame->TRxBufferSize,
                         &(bfl_frame->RcIo));
    
    for (i = 0; i < PHJAL_MAX_RDEV; i++)
    {
        /*T=CL Protocol: */
        phcsBflI3P4_Init(&(bfl_frame->ISO14443_4[i]),
                         &(bfl_frame->ISO14443_4Cp[i]),
                         &(bfl_frame->RcIo));


        /* NFC-I: */
        phcsBflNfc_InitiatorInit(&(bfl_frame->NfcI[i]),
                                 &(bfl_frame->NfcICp[i]),
                                 PHCS_BFLNFCCOMMON_PREAM_OMIT,
                                 PHCS_BFLNFCCOMMON_PREAM_OMIT,
                                 (uint8_t*)(bfl_frame->TRxBuffer),
                                 bfl_frame->TRxBufferSize,
                                 (uint8_t*)(bfl_frame->INfcIdT),
                                 &(bfl_frame->RcIo));
    }

    /* initialise handle entries */
    for(i=0; i < PHJAL_MAX_HANDLE_ENTRIES; ++i)
    {
        bfl_frame->LogHandleEntries[i].HandleAvailable = 1;
        bfl_frame->LogHandleEntries[i].Handle = i+1;
    }

    for(i=0; i < PHJAL_MAX_RDEV; ++i)
    {
        bfl_frame->CidEntries[i].Handle = i+1;
        bfl_frame->CidEntries[i].HandleAvailable = 1;

        bfl_frame->DidEntries[i].Handle = i+1;
        bfl_frame->DidEntries[i].HandleAvailable = 1;
    }

    for(i=0; i < PHJAL_MAX_RDEV; ++i)
    {
        bfl_frame->ISO14443_4Entries[i].ProtocolAvailable = 1;
        bfl_frame->ISO14443_4Entries[i].Protocol = &(bfl_frame->ISO14443_4[i]);
        bfl_frame->ISO14443_4Entries[i].LogicalHandle = 0;
        bfl_frame->ISO14443_4Entries[i].DeviceId = 0;

        bfl_frame->ExtendedNfcIProtocol[i].Protocol = &(bfl_frame->NfcI[i]);
        bfl_frame->ExtendedNfcIProtocol[i].ResponseWaitingTime = NFC_ACTIVE_ACTIVATION_DELAY;
        bfl_frame->NfcInitiator_Entries[i].ProtocolAvailable = 1;
        bfl_frame->NfcInitiator_Entries[i].Protocol = &(bfl_frame->ExtendedNfcIProtocol[i]);
        bfl_frame->NfcInitiator_Entries[i].LogicalHandle = 0;
        bfl_frame->NfcInitiator_Entries[i].DeviceId = 0;
    }
    
    /* initialise UID cache */
    for(i=0; i < PHJAL_MAX_UID_CACHE_ENTRIES; ++i)
    {
        bfl_frame->UIDCache.CacheEntries[i].EntryAvailable = 1;
    }

    bfl_frame->UIDCache.NumUIDs = 0;

    bfl_frame->RcRegCtlModifyParam.self = bfl_frame->RcRegCtl;
    bfl_frame->RcRegCtlSetRegParam.self = bfl_frame->RcRegCtl;
    bfl_frame->RcRegCtlGetRegParam.self = bfl_frame->RcRegCtl;  
    
    bfl_frame->RcOpCtlAttribParam.self = &bfl_frame->RcOpCtl;

    bfl_frame->InitialPollExecuted = 0;
    bfl_frame->CIDlessModeActivated = 0;

    bfl_frame->TargetLoopItemsCreated = 0;
    bfl_frame->WaitingForReception = 0;
    bfl_frame->IsStartTargetMode = 0;

    /* HANDLES need to be set to NULL because of 1st init in STM!! */
    bfl_frame->ReceptionInfoHandle_STM = NULL; // Change of WFSPE() function in StartTargetMode - Addition of Handle
    bfl_frame->ReceptionInfoHandle_RHT = NULL; // Change of WFSPE() function in StartTargetMode - Addition of Handle
    bfl_frame->ReceptionThreadHandle = NULL;
    bfl_frame->TargetInfoHandle = NULL;
    bfl_frame->TerminateReceptionHandle = NULL;
    bfl_frame->TargetModeRunningHandle = NULL;
    //bfl_frame->TargetModeFunctionReturnHandle = NULL; // Peer to Peer BugFix: Semaphore removed
    bfl_frame->UserTimeoutHandle = NULL;

    bfl_frame->ReceptionUserToEndpointHandle = NULL;
    bfl_frame->ReceptionEndpointToUserHandle = NULL;
    bfl_frame->TransmissionUserToEndpointHandle = NULL;
    bfl_frame->TransmissionEndpointToUserHandle = NULL;
    bfl_frame->ReceptionUserReadyForMetaChaining = NULL;


    /* Cannot yet init as no HW Ref available (not yet alloc.). */
    bfl_frame->ReceptionTimeoutParams.psHwReference = NULL;
    
    bfl_frame->ReceptionHandlingParams.MetaChainingActive = 0;

    bfl_frame->AuthenticatedBlockAddress = 0;
    bfl_frame->MifareKeyAvailable = 0;
    bfl_frame->MifareAuthentCommand = 0; 

    /* Default to start is NFC-I. */
    bfl_frame->CurrentInitiatorMode = NO_MODE;

    /* Default line to sense interrupts: */
    bfl_frame->InterruptLine = JAL_SENSE_RI;


    /* In this  function we need addional initialisation/reset of the chip.
       Reason: Switching between NFCI and NFCT!
       See Mantis #530 */
    if (do_initialise_chip)
    {
        reportDbg0("Clearing Peripheral.\n");
        phJal_ClearPeripheral(bfl_frame);
    }
}

const phHal_eOpModes_t *phJal_GetMetaMode(phHal_eOpModes_t mode)
{
    const phHal_eOpModes_t *meta_mode;
    uint8_t i= 0, j = 0, hit = 0;

    while ((meta_mode = PHJAL_OPMODE_MATRIX[i]) != NULL)
    {   
        j = 0;
        while (meta_mode[j] != phHal_eOpModesArrayTerminator)
        {
            if (mode == meta_mode[j])
            {
                hit = 1;
                break;
            }
            j++;
        }
        if (hit) break;
        i++;
    }
    
    return meta_mode;
}

uint8_t phJal_IsOpModeCompatibleWithCurrentMetaMode(phHal_sHwReference_t    *psHwReference,
                                                    phHal_eOpModes_t        OpMode)
{
    uint8_t op_mode_compatible = OP_MODE_NOT_COMPATIBLE;

    if (psHwReference != NULL)
    {
        phJal_sBFLUpperFrame_t *bfl_frame = 
            (phJal_sBFLUpperFrame_t*)psHwReference->pBoardDriver; 
    
        if (bfl_frame->CurrentMetaMode == NULL)
        {
            op_mode_compatible = OP_MODE_COMPATIBLE;
        }
        else if (bfl_frame->CurrentMetaMode == phJal_GetMetaMode(OpMode))
        {
            op_mode_compatible = OP_MODE_COMPATIBLE;
        }
        else
        {
            op_mode_compatible = OP_MODE_NOT_COMPATIBLE;
        }
    }
    else
    {
        op_mode_compatible = OP_MODE_NOT_COMPATIBLE;
    }
    return op_mode_compatible;
}

uint8_t phJal_AreSessionsOpened(phHal_sRemoteDevInformation_t   psRemoteDevInfoList[],
                                uint8_t                         NbrOfRemoteDev)
{
    uint8_t result, device_counter;
    if (psRemoteDevInfoList != NULL)
    {
        result = 0;
        for(device_counter = 0; device_counter < NbrOfRemoteDev; ++device_counter)
        {
            if (psRemoteDevInfoList[device_counter].SessionOpened)
            {
                result = OP_MODE_IN_LIST;
                break;
            }
        }
    }
    else
    {
        result = 0;
    }
    return result;
}

NFCSTATUS phJal_AreOpModesValid(phHal_eOpModes_t OpModes[])
{
    NFCSTATUS result;
    phHal_eOpModes_t op_mode;
    const phHal_eOpModes_t *meta_mode = NULL;
    uint8_t op_mode_counter = 0;

    if (OpModes != NULL)
    {
        result = NFCSTATUS_SUCCESS;
        op_mode = OpModes[0];

        while (op_mode != phHal_eOpModesArrayTerminator)
        {
            meta_mode = phJal_GetMetaMode(op_mode);

            if (meta_mode == NULL)
            {
                result = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER);
                break;
            }
            op_mode = OpModes[op_mode_counter++];
        }
        
    }
    else
    {
        result = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER);
    }
    return result;
}

uint8_t phJal_IsOpModeInOpModesList(phHal_eOpModes_t OpModesList[], phHal_eOpModes_t OpModeToSearch)
{
    phHal_eOpModes_t op_mode;
    uint8_t op_mode_counter = 0;
    uint8_t op_mode_found = 0;
                                            
    if (OpModesList != NULL)
    {
        op_mode = OpModesList[0];
        while (op_mode != phHal_eOpModesArrayTerminator)
        {
            if (op_mode == OpModeToSearch)
            {
                op_mode_found = 1;
                break;
            }
            op_mode = OpModesList[++op_mode_counter];
        }
    }
    return op_mode_found;
}

int16_t phJal_AquireHandle(phJal_HandleEntry_t psEntries[])
{
    uint8_t handle_index = 0;
    int16_t handle = NO_HANDLE_AVAILABLE;

    phJal_Trace(NULL, "phJal_AquireHandle");
    
    while (handle_index < PHJAL_MAX_HANDLE_ENTRIES)
    {
        if (psEntries[handle_index].HandleAvailable == 1)
        {
            handle = (int16_t)psEntries[handle_index].Handle;
            psEntries[handle_index].HandleAvailable = 0;
            break;
        }
        ++handle_index;
    }
    return handle;
}

void phJal_ReleaseHandle(phJal_HandleEntry_t psEntries[], uint8_t Handle)
{
    uint8_t handle_index = 0;

    phJal_Trace(NULL, "phJal_ReleaseHandle");

    while (handle_index < PHJAL_MAX_HANDLE_ENTRIES)
    {
        if (psEntries[handle_index].Handle == Handle)
        {
            psEntries[handle_index].HandleAvailable = 1;
            break;
        }
        ++handle_index;
    }
}

void* phJal_AquireProtocol(phJal_ProtocolEntry_t psEntries[])
{
    uint8_t handle_index = 0;
    void *handle = NULL;

    phJal_Trace(NULL, "phJal_AquireProtocol");

    while (handle_index < PHJAL_MAX_RDEV)
    {
        if (psEntries[handle_index].ProtocolAvailable == 1)
        {
            handle = psEntries[handle_index].Protocol;
            psEntries[handle_index].ProtocolAvailable = 0;
            psEntries[handle_index].LogicalHandle = 0;
            psEntries[handle_index].DeviceId = 0;
            break;
        }
        ++handle_index;
    }
    return handle;
}

void phJal_ReleaseProtocol(phJal_ProtocolEntry_t psEntries[], void *psProtocol)
{
    uint8_t handle_index = 0;

    phJal_Trace(NULL, "phJal_ReleaseProtocol");

    while (handle_index < PHJAL_MAX_RDEV)
    {
        if (psEntries[handle_index].Protocol == psProtocol)
        {
            psEntries[handle_index].ProtocolAvailable = 1;
            psEntries[handle_index].LogicalHandle = 0;
            psEntries[handle_index].DeviceId = 0;
            break;
        }
        ++handle_index;
    }
}

void phJal_RegisterLogicalHandleForProtocol(phJal_ProtocolEntry_t psEntries[], 
                                            void *psProtocol, 
                                            uint8_t LogicalHandle)
{
    uint8_t handle_index = 0;

    phJal_Trace(NULL, "phJal_RegisterLogicalHandleForProtocol");
    
    if ((psProtocol != NULL) || (psEntries != NULL))
    {   
        while (handle_index < PHJAL_MAX_RDEV)
        {
            if (psEntries[handle_index].Protocol == psProtocol)
            {
                if (psEntries[handle_index].ProtocolAvailable == 0)
                {
                    psEntries[handle_index].LogicalHandle = LogicalHandle;
                }
                break;
            }
            ++handle_index;
        }
    }
}

phJal_ProtocolEntry_t* phJal_GetProtocolEntryForLogicalHandle(phJal_ProtocolEntry_t  psEntries[], 
                                                              uint8_t                handle)
{
    uint8_t handle_index = 0;
    void *protocol = NULL;

    phJal_Trace(NULL, "phJal_GetProtocolEntryForLogicalHandle");

    if (psEntries == NULL)
    {
        return NULL;
    }

    while (handle_index < PHJAL_MAX_RDEV)
    {
        if (psEntries[handle_index].LogicalHandle == handle)
        {
            if (psEntries[handle_index].ProtocolAvailable == 0)
            {
                protocol = &psEntries[handle_index];
            }
            break;
        }
        ++handle_index;
    }
    return protocol;
}

void phJal_RegisterDeviceIdForProtocol(phJal_ProtocolEntry_t psEntries[], 
                                        void                 *psProtocol,
                                        uint8_t              DeviceId)
{
    uint8_t handle_index = 0;

    phJal_Trace(NULL, "phJal_RegisterDeviceIdForProtocol");

    if ((psProtocol != NULL) || (psEntries != NULL))
    {
        while (handle_index < PHJAL_MAX_RDEV)
        {
            if (psEntries[handle_index].Protocol == psProtocol)
            {
                if (psEntries[handle_index].ProtocolAvailable == 0)
                {
                    psEntries[handle_index].DeviceId = DeviceId;
                }
                break;
            }
            ++handle_index;
        }
    }
}

uint8_t phJal_AddUIDCacheEntry(phJal_sUIDCache_t    *Cache,
                               uint8_t              *Uid, 
                               uint8_t              UidLength)
{
    uint8_t cache_status;
    uint8_t cache_index = 0;
    phJal_sUIDCacheEntry_t *cache_entry = NULL;

    phJal_Trace(NULL, "phJal_AddUIDCacheEntry");

    if (Cache != NULL)
    {
        if (Cache->NumUIDs < PHJAL_MAX_UID_CACHE_ENTRIES)
        {
            while (cache_index < PHJAL_MAX_UID_CACHE_ENTRIES)
            {
                if (Cache->CacheEntries[cache_index].EntryAvailable == 1)
                {
                    cache_entry = &(Cache->CacheEntries[cache_index]);
                    break;
                }
                ++cache_index;
            }

            if (cache_entry != NULL)
            {
                memcpy(cache_entry->UID, Uid, UidLength);                
                cache_entry->UIDLength = UidLength;
                cache_entry->EntryAvailable = 0;
                cache_status = OP_SUCCEEDED;               
                Cache->NumUIDs++;
            }
            else
            {
                cache_status = UID_CACHE_FULL;
            }
        }
        else
        {
            cache_status = UID_CACHE_FULL;
        }
    }
    else
    {
        cache_status = INVALID_CACHE;
    }
    return cache_status;
}

phJal_sUIDCacheEntry_t* phJal_GetNextCacheEntry(phJal_sUIDCache_t  *Cache,
                                                uint8_t            *pCurrentPosition)
{
    phJal_sUIDCacheEntry_t    *cache_entry = NULL;
    uint8_t cache_index = 0;
    uint8_t num_cache_entries = 0;

    phJal_Trace(NULL, "phJal_GetNextCacheEntry");

    if ((pCurrentPosition != NULL) ||
       (Cache != NULL))
    {
        cache_index = *pCurrentPosition;
        num_cache_entries = Cache->NumUIDs;
        while (cache_index < num_cache_entries)
        {
            cache_entry = &(Cache->CacheEntries[cache_index]);
            if (cache_entry->EntryAvailable == 0)
            {
                cache_entry->EntryAvailable = 1;
                Cache->NumUIDs--;
                (*pCurrentPosition)++;
                break;
            }
            else
            {
                ++cache_index;
                cache_entry = NULL;
            }
        }
    }
    else
    {
        cache_entry = NULL;
    }

    return cache_entry;
}


/* TRACE Functions to see call sequence of HAL functions. */

#if (defined PHJALJOINER_TRACE)
    
    void   phJal_BFLFrameTrace(phHal_sHwReference_t *psHwReference, int8_t *msg)
    {
        double t;
        if (psHwReference != NULL)
        {
            t = phJal_GetClockdiff(((phJal_sBFLUpperFrame_t*)(psHwReference->pBoardDriver))->StartTime);
            fprintf(stderr, "### T+ %0.6fs: %s\n", t, msg);
        } else
        {
            fprintf(stderr, "### T+ ?.??????s: %s\n",msg);
        }
    }

    double phJal_GetClockdiff(clock_t start)
    {
        const double ref = CLOCKS_PER_SEC;
        double diff;

        diff = (double)(clock() - start);
        return (diff / ref);
    }
#endif /* */



#endif /* Guard */