/*
 *         Copyright (c), NXP Semiconductors
 *
 *                       (C)NXP Electronics N.V.2004
 *         All rights are reserved. Reproduction in whole or in part is
 *        prohibited without the written consent of the copyright owner.
 *    NXP reserves the right to make changes without notice at any time.
 *   NXP makes no warranty, expressed, implied or statutory, including but
 *   not limited to any implied warranty of merchantability or fitness for any
 *  particular purpose, or that the use will not infringe any third party patent,
 *   copyright or trademark. NXP must not be liable for any loss or damage
 *                            arising from its use.
 *
 */


/*!
 * \file phJalJoiner.c
 * \brief TAL Function implementation
 *
 *  This API allows the HAL layer to use the PN531 NFC chip with the same API
 *  than HAL itself. The implementation is dedicated to the PN531.
 *
 * Project: NFC MW / HAL
 *
 * $Date: Thu Sep 27 09:38:43 2007 $ 
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Aliases: SDK_HAL_2.2,SDK_HAL_2.2_071008,NFC_FRI_SDK_1.0_071031 $
 *
 */

#ifdef PHHAL_VERSION_ACQUISITION
    #define PHJALJOINER_SOURCE_FILEREVISION "$Revision: 1.1 $"
    #define PHJALJOINER_SOURCE_FILEALIASES  "$Aliases: SDK_HAL_2.2,SDK_HAL_2.2_071008,NFC_FRI_SDK_1.0_071031 $"
#else /* PHHAL_VERSION_ACQUISITION */

#include <string.h>

#include <phcsBflOpCtl_Hw1Ordinals.h>
#include <phcsBflHw1Reg.h>

#include <phOsalNfc.h>
#include <phNfcStatus.h>
#include <phDalNfc.h>
#include <phNfcIoctlCode.h>
#include <phcsBflAux.h>

#include <phJalDebugReportFunctions.h>
#include <phJalConnectionFunctions.h>
#include <phJalTransceiveFunctions.h>
#include <phJalTargetModeFunctions.h>
#include <phJalJoiner.h>
#include <phJalJoinerAux.h>
#include <phJalJoinerBflFrame.h>
#include <phJalTargetSendReceiveFunctions.h>
#include <phJalDebugReportFunctions.h>

extern NFCSTATUS phJalJoiner_Open(phHal_sHwReference_t *psHwReference)
{
    const uint8_t   JAL_USE_CB          = 0x02;  /* 00 = BFL polling, 01 = CB Polling, 02 = CB INTR. */
    NFCSTATUS       status              = phDalNfc_Open(psHwReference);
    uint8_t         ioctlbuf[]          = {JAL_USE_CB};

    if (NFCSTATUS_SUCCESS == status)
    {
        /* PRIMARY INITIALISATION: */
        
        /* Switch RF OFF at the beginning, ignoring the status: */
        phJal_SwitchOffRf(psHwReference);
        /* Set the CB for interrupt sensing: */
        phJalJoiner_Ioctl(psHwReference, PHHALNFC_IOCTL_PN511_ENINTR, ioctlbuf, 1, NULL, NULL);
        /* Set the standard TO-WT for ATR_RES: */
        ioctlbuf[0] = JAL_DEFAULT_WT;
        phJalJoiner_Ioctl(psHwReference, PHHALNFC_IOCTL_PN511_SETWT, ioctlbuf, 1, NULL, NULL);

        #if (defined PHJALJOINER_TRACE)
            ((phJal_sBFLUpperFrame_t*)(psHwReference->pBoardDriver))->StartTime = clock();
            phJal_Trace(psHwReference, "phJalJoiner_Open");
        #endif /* TRACE */
    } else
    {
        /* Do nothing but return. */
    }

    return status;
}
                                                                                             
extern NFCSTATUS phJalJoiner_GetDeviceCapabilities(phHal_sHwReference_t          *psHwReference,
                                                   phHal_sDeviceCapabilities_t   *psDevCapabilities)
{
    NFCSTATUS result = NFCSTATUS_SUCCESS;

    phJal_Trace(psHwReference, "phJalJoiner_GetDeviceCapabilities");

    psDevCapabilities->psHwReference                    = psHwReference;
    strcpy(psDevCapabilities->VendorName.Name, PHJAL_J_VD_NAME);
    psDevCapabilities->VendorName.Length                = (uint8_t)strlen(PHJAL_J_VD_NAME);
    psDevCapabilities->MTU                              = MTU_SIZE;
    psDevCapabilities->MaxNumberOfRemoteDev             = PHJAL_MAX_RDEV;
    psDevCapabilities->MultiPollSessionSup              = 1;
    psDevCapabilities->TargetCascadeLevel               = 1;
    
    psDevCapabilities->InitiatorSupProtocol.Felica		= 1;
	psDevCapabilities->InitiatorSupProtocol.ISO14443_4A = 1;
    psDevCapabilities->InitiatorSupProtocol.ISO14443_4B = 0;
	psDevCapabilities->InitiatorSupProtocol.MifareStd	= 1;
	psDevCapabilities->InitiatorSupProtocol.MifareUL	= 1;
	psDevCapabilities->InitiatorSupProtocol.NFC			= 1;
    psDevCapabilities->InitiatorSupProtocol.Jewel       = 0;

	psDevCapabilities->TargetSupProtocol.Felica		    = 0;
	psDevCapabilities->TargetSupProtocol.ISO14443_4A    = 0;
    psDevCapabilities->TargetSupProtocol.ISO14443_4B    = 0;
	psDevCapabilities->TargetSupProtocol.MifareStd	    = 0;
	psDevCapabilities->TargetSupProtocol.MifareUL	    = 0;
	psDevCapabilities->TargetSupProtocol.NFC		    = 1;
    psDevCapabilities->TargetSupProtocol.Jewel          = 0;

    return result;
}

extern NFCSTATUS phJalJoiner_Poll(phHal_sHwReference_t           *psHwReference,
                                  phHal_eOpModes_t                OpModes[],
                                  phHal_sRemoteDevInformation_t   psRemoteDevInfoList[],
                                  uint8_t                        *pNbrOfRemoteDev,
                                  phHal_sDevInputParam_t         *psDevInputParam)
{
    NFCSTATUS                  result = NFCSTATUS_SUCCESS;
    uint8_t                    device_counter = 0;
    uint8_t                    num_free_remote_devices = *pNbrOfRemoteDev;
    uint8_t                    use_repeated_polling;
    phJal_sBFLUpperFrame_t    *bfl_frame = (phJal_sBFLUpperFrame_t*)psHwReference->pBoardDriver;
    const uint8_t              do_init_chip_now = 1;
    phcsBflIo_SetWecParam_t    event_callback_param = {0};


    phJal_Trace(psHwReference, "phJalJoiner_Poll");
    
    if (*pNbrOfRemoteDev > 0)
    {
        if (psDevInputParam->ControlFlags.PollingAddsDevs == 0x01)
        {
            #if (MULTI_POLL_SUPPORTED == 1)
                use_repeated_polling = 1;
                result = NFCSTATUS_SUCCESS;
            #else
                result = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_MULTI_POLL_NOT_SUPPORTED);
            #endif 
        }
        else
        {
            use_repeated_polling = 0;
        }
    
        if (result == NFCSTATUS_SUCCESS)
        {
            result = phJal_AreOpModesValid(OpModes);
            if (result == NFCSTATUS_SUCCESS)
            {
                if (bfl_frame->TargetLoopItemsCreated != 0)
                {
                    /* If we have started the Target mode before and the thing is still waiting for reception
                    within the main Target thread, we need to abort the reception and wait for the thread
                    to exit. Then we can create a new one and start over. */

                    /* We must allow to abort locally to be able to POL immediately: */
                    psHwReference->Flags.AbortAllowed = 1;
                    if (bfl_frame->WaitingForReception != 0)
                    {
                        /* We are currently waiting to receive data. */
                        result = phDalNfc_Abort(psHwReference);
                        phOsalNfc_ConsumeSemaphore(bfl_frame->TargetModeRestartCleanupSync);
                        phOsalNfc_ProduceSemaphore(bfl_frame->TargetModeRestartCleanupSync);
                    } else
                    {
                        /* We are somewhere in the Target processing loop: */
                        bfl_frame->ReceptionHandlingParams.TerminationState = TERMINATE_RECEPTION;
                        phOsalNfc_ConsumeSemaphore(bfl_frame->TargetModeRestartCleanupSync);
                        phOsalNfc_ProduceSemaphore(bfl_frame->TargetModeRestartCleanupSync);
                    }
                    /* This is req'd in order to prevent memleaks: */
                    phJal_CloseTargetReceptionHandling(bfl_frame);
                    phJal_CloseTargetDepHandlingEvents(bfl_frame);
                    psHwReference->Flags.AbortAllowed = 0;
                } else
                {
                    /* We do nothing because no sync obj or threads need to be closed. */
                }

                if (use_repeated_polling == 0)
                {
                    for (device_counter = 0; device_counter < *pNbrOfRemoteDev; ++device_counter)
                    {
                        phJalJoiner_Disconnect(psHwReference, &(psRemoteDevInfoList[device_counter]));
                        psRemoteDevInfoList[device_counter].OpMode = phHal_eOpModesInvalid;
                    }
                }

                if (num_free_remote_devices == 0)
                {
                    result = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INSUFFICIENT_RESOURCES);
                }
                else
                {
                    /* Continue Init: */
                    phJal_BuildBflUpperFrame(bfl_frame, 1, bfl_frame->RcRegCtl, do_init_chip_now);
                    event_callback_param.wait_event_cb = bfl_frame->WaitEventCb;
                    event_callback_param.self = &bfl_frame->RcIo;
                    bfl_frame->ReceptionTimeoutParams.psHwReference = psHwReference;
                    event_callback_param.user_ref = &bfl_frame->ReceptionTimeoutParams;
                    bfl_frame->RcIo.SetWaitEventCb(&event_callback_param);

                    /* Clear IRQ-INV: */
                    bfl_frame->RcRegCtlModifyParam.address = PHCS_BFL_JREG_COMMIEN;
	                bfl_frame->RcRegCtlModifyParam.mask    = 0x80;
	                bfl_frame->RcRegCtlModifyParam.set     = 0x00;
                    bfl_frame->RcRegCtl->ModifyRegister(&bfl_frame->RcRegCtlModifyParam);

	                /* Enable IRQPushPull so that the PN511 actively drives the signal */
	                bfl_frame->RcRegCtlModifyParam.address = PHCS_BFL_JREG_DIVIEN;
	                bfl_frame->RcRegCtlModifyParam.mask    = 0x80;
	                bfl_frame->RcRegCtlModifyParam.set     = 0xFF;
	                bfl_frame->RcRegCtl->ModifyRegister(&bfl_frame->RcRegCtlModifyParam);

                    result = phJal_PollForDevices(psHwReference,
                                                  psRemoteDevInfoList,
                                                  psDevInputParam,
                                                  OpModes,
                                                  pNbrOfRemoteDev,
                                                  use_repeated_polling);

                    /* Adjust error code to value that is mandated by spec in this case: */
                    if (PHNFCSTVAL(CID_NFC_NONE, result) ==
                        PHNFCSTVAL(CID_NFC_NONE, NFCSTATUS_RF_TIMEOUT))
                    {
                        result = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_NO_DEVICE_FOUND);    
                    }
                } 
            }
        }
    }
    else
    {
        result = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER);
    }

    return result;
}

extern NFCSTATUS phJalJoiner_Connect(phHal_sHwReference_t          *psHwReference,
                                     phHal_eOpModes_t               OpMode,
                                     phHal_sRemoteDevInformation_t *psRemoteDevInfo,
                                     phHal_sDevInputParam_t        *psDevInputParam)
{
    NFCSTATUS                result = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_DEVICE_REQUEST);
    phJal_sBFLUpperFrame_t  *bfl_frame = NULL;
    uint8_t                  retry = 0;
    const uint8_t            num_retry = 3;
    uint16_t                 retry_time_interval = 50;

    phJal_Trace(psHwReference, "phJalJoiner_Connect");

    bfl_frame = (phJal_sBFLUpperFrame_t*)psHwReference->pBoardDriver;

    if (phJal_AreSessionsOpened(psRemoteDevInfo, 1)) 
    {
        result = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_ALREADY_CONNECTED);
    }
    else
    {
        if (psRemoteDevInfo->OpMode != OpMode)
        {
            reportDbg2("connect:\t OpMode of remote dev %02X, OpMode to connect %02X\n",
                psRemoteDevInfo->OpMode,OpMode); 
            psRemoteDevInfo->SessionOpened = 0; 
            result = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_DEVICE_REQUEST);
        }
        else
        {
            do
            {
                if (retry > 0)
                {
                    reportDbg0("connect:\t !!!!! RETRY REQUIRED !!!!!\n");
                    /* If this is because of the required setup-time on Target
                       side, we allow for it ... */
                    retry_time_interval = retry_time_interval * 2;
                    phOsalNfc_Suspend(retry_time_interval);
                }
                /*
                 * CONNECT-REACTIVATE: We do it in a "retry" loop in order to
                 * be able to be able to react to errors. 
                 */
                if (OpMode == phHal_eOpModesMifare)
                {
                    result = NFCSTATUS_SUCCESS;
                }
                else if (OpMode == phHal_eOpModesISO14443_4A)
                {
                    result = phJal_ConnectToIso144434Device(psHwReference, psRemoteDevInfo,
                                psDevInputParam);
                    bfl_frame->CurrentInitiatorMode = PASSIVE_MODE;
                }
                else if (OpMode == phHal_eOpModesNfcPassive106)
                {
                    reportDbg0("connect:\t connect to passive 106k device\n");
                    result = phJal_ConnectToNfcPassive106Device(psHwReference, psRemoteDevInfo, 
                        psDevInputParam);
                    bfl_frame->CurrentInitiatorMode = PASSIVE_MODE;
                }
                else if ((OpMode == phHal_eOpModesNfcPassive212) ||
                        (OpMode == phHal_eOpModesNfcPassive424))
                {
                    result = phJal_ConnectToNfcPassive212424Device(psHwReference, psRemoteDevInfo,
                                psDevInputParam);
                    bfl_frame->CurrentInitiatorMode = PASSIVE_MODE;
                }
                else if ((OpMode == phHal_eOpModesFelica212) ||
                        (OpMode == phHal_eOpModesFelica424))
                {
                    result = NFCSTATUS_SUCCESS;
                    bfl_frame->CurrentInitiatorMode = PASSIVE_MODE;
                }
                else if ((OpMode == phHal_eOpModesNfcActive106) ||
                        (OpMode == phHal_eOpModesNfcActive212) ||
                        (OpMode == phHal_eOpModesNfcActive424))
                {
                    result = phJal_ConnectToNfcActiveDevice(psHwReference, psRemoteDevInfo,
                                psDevInputParam);
                    bfl_frame->CurrentInitiatorMode = ACTIVE_MODE;
                }
                else
                {
                psRemoteDevInfo->SessionOpened = 0; 
                result = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_DEVICE_REQUEST);
                bfl_frame->CurrentInitiatorMode = NO_MODE;
                }
                retry++;
            } while ((retry < num_retry) && (result != NFCSTATUS_SUCCESS));

            if (result == NFCSTATUS_SUCCESS)
            {
                psRemoteDevInfo->SessionOpened = 0x01;                                
            }
            else
            {
                psRemoteDevInfo->SessionOpened = 0x00;
                bfl_frame->CurrentInitiatorMode = NO_MODE;
            }
        }
    }    
    return result;
}


extern NFCSTATUS phJalJoiner_Transceive(phHal_sHwReference_t           *psHwReference,
                                        phHal_sRemoteDevInformation_t  *psRemoteDevInfo,
                                        phHal_uCmdList_t                Cmd,
                                        phHal_sDepAdditionalInfo_t     *psDepAdditionalInfo,
                                        uint8_t                        *pSendBuf,
                                        uint16_t                        SendLength,
                                        uint8_t                        *pRecvBuf,
                                        uint16_t                       *pRecvLength)
{
    NFCSTATUS result = NFCSTATUS_SUCCESS;

    phJal_Trace(psHwReference, "phJalJoiner_Transceive");

    if (psRemoteDevInfo->SessionOpened == 0x01)
    {
        result = phJal_SetBitrateForOpMode(psHwReference, psRemoteDevInfo->OpMode);
        
        if (result == NFCSTATUS_SUCCESS)
        {
            switch (psRemoteDevInfo->OpMode)
            {
            case phHal_eOpModesMifare:
                result = phHalNfc_TransceiveMifareCommand(psHwReference,
                                                          psRemoteDevInfo,
                                                          Cmd,
                                                          pSendBuf,
                                                          SendLength,
                                                          pRecvBuf,pRecvLength);
                break;
            case phHal_eOpModesNfcPassive106:
            case phHal_eOpModesNfcActive106:
                result = phJal_ConfigureHardwareForNfc106kProtocol(psHwReference);
            case phHal_eOpModesNfcPassive212:
            case phHal_eOpModesNfcPassive424:            
            case phHal_eOpModesNfcActive212:
            case phHal_eOpModesNfcActive424:
                if (result == NFCSTATUS_SUCCESS)
                {
                    if (Cmd.NfcInCmd == phHal_eNfcCmdListNfcDEP)
                    {
                        result = phJal_DataExchangeProtocolRequest(psHwReference, psRemoteDevInfo,
                        psDepAdditionalInfo, pSendBuf, SendLength, pRecvBuf, pRecvLength);
                    }
                    else if (Cmd.NfcInCmd == phHal_eNfcCmdListNfcPSL)
                    {
                        result = phJal_ParameterSelectionRequest(psHwReference, psRemoteDevInfo,
                            pSendBuf, SendLength);
                    }
                    else if(Cmd.NfcInCmd == phHal_eNfcCmdListNfcATT)
                    {
                        result = phJal_AttentionRequest(psHwReference, psRemoteDevInfo);
                    }
                    else
                    {
                        result = (PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER));
                    }
                }
                break;
            case phHal_eOpModesFelica212:
            case phHal_eOpModesFelica424:
                if (Cmd.FelCmd == phHal_eFelicaCmdListFelicaCmd)
                {
                result = phHalNfc_TransceiveFelicaCommand(psHwReference,
                                                          psRemoteDevInfo,
                                                          Cmd,
                                                          pSendBuf,
                                                          SendLength,
                                                          pRecvBuf,
                                                          pRecvLength);
                } else
                {
                    result = (PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER));
                }
                break;
            case phHal_eOpModesISO14443_4A:
            case phHal_eOpModesISO14443_4B:
               result = phHalNfc_TransceiveIso144434Command(psHwReference, psRemoteDevInfo, 
                    psDepAdditionalInfo, Cmd, pSendBuf, SendLength, pRecvBuf, pRecvLength);
               break;
            default:
                result = (PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER));
                break;
            }
        }
    }
    else
    {
        result = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_DEVICE);
    }

    return result;
}


extern NFCSTATUS phJalJoiner_Disconnect(phHal_sHwReference_t          *psHwReference,
                                        phHal_sRemoteDevInformation_t *psRemoteDevInfo)
{
    NFCSTATUS               result = NFCSTATUS_SUCCESS;
    phcsBfl_Status_t        bfl_status = PH_ERR_BFL_SUCCESS;
    uint8_t                 disconnect_nfc_device = 0;
    phJal_sBFLUpperFrame_t  *bfl_frame = (phJal_sBFLUpperFrame_t*)psHwReference->pBoardDriver;

    phJal_Trace(psHwReference, "phJalJoiner_Disconnect");

    if (psRemoteDevInfo->SessionOpened == 0x01)
    {
        switch(psRemoteDevInfo->OpMode)
        {
            case phHal_eOpModesMifare:
                psRemoteDevInfo->SessionOpened = 0;
                disconnect_nfc_device = 0;
                bfl_frame->MifareKeyAvailable = 0;
                break;        
            case phHal_eOpModesISO14443_4A:
                result = 
                    phJal_DisconnectIso144434Device(psHwReference, psRemoteDevInfo);
                if (result == NFCSTATUS_SUCCESS)
                {
                    psRemoteDevInfo->SessionOpened = 0;
                }
                break;        
            case phHal_eOpModesNfcPassive106:
                result = phJal_ConfigureHardwareForNfc106kProtocol(psHwReference);
                if (result == NFCSTATUS_SUCCESS)
                {
                    bfl_status = phJal_SetBitrate(psHwReference, RF106K, PASSIVE_MODE);
                }
                disconnect_nfc_device = 1;
                break;
            case phHal_eOpModesNfcActive106:
                result = phJal_ConfigureHardwareForNfc106kProtocol(psHwReference);
                if (result == NFCSTATUS_SUCCESS)
                {
                    bfl_status = phJal_SetBitrate(psHwReference, RF106K, ACTIVE_MODE);
                }
                disconnect_nfc_device = 1;
                break;
            case phHal_eOpModesNfcPassive212: 
                bfl_status = phJal_SetBitrate(psHwReference, RF212K, PASSIVE_MODE);
                disconnect_nfc_device = 1;
                break;
            case phHal_eOpModesNfcPassive424:
                bfl_status = phJal_SetBitrate(psHwReference, RF424K, PASSIVE_MODE);
                disconnect_nfc_device = 1;
                break;
            case phHal_eOpModesNfcActive212: 
                bfl_status = phJal_SetBitrate(psHwReference, RF212K, ACTIVE_MODE);
                disconnect_nfc_device = 1;
                break;
            case phHal_eOpModesNfcActive424:
                bfl_status = phJal_SetBitrate(psHwReference, RF424K, ACTIVE_MODE);                
                disconnect_nfc_device = 1;
                break;
            case phHal_eOpModesFelica212:
            case phHal_eOpModesFelica424:
                psRemoteDevInfo->SessionOpened = 0;
                disconnect_nfc_device = 0;
                break;
            default:
                psRemoteDevInfo->SessionOpened = 0; 
                disconnect_nfc_device = 0;
                result = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_DEVICE_REQUEST);
        }

        if (bfl_status == PH_ERR_BFL_SUCCESS)
        {
            if (disconnect_nfc_device == 1)
            {
                result = phJal_DisconnectNfcDevice(psHwReference, psRemoteDevInfo);
            }
        }
        else
        {
            result = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_BOARD_COMMUNICATION_ERROR);
        }
    }
    else
    {
        result = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_NO_DEVICE_CONNECTED);
    }

    return result;
}




extern NFCSTATUS phJalJoiner_Ioctl(phHal_sHwReference_t     *psHwReference,
                                   uint16_t                  IoctlCode,
                                   uint8_t                  *pInBuf,
                                   uint16_t                  InLength,
                                   uint8_t                  *pOutBuf,
                                   uint16_t                 *pOutLength)
{
    NFCSTATUS                    result = NFCSTATUS_SUCCESS;
    phcsBfl_Status_t             bflstatus = PH_ERR_BFL_SUCCESS;
    phcsBflRegCtl_SetRegParam_t  srp;
    phcsBflRegCtl_GetRegParam_t  grp;
    phcsBflOpCtl_AttribParam_t   atp;
    phcsBflIo_TransceiveParam_t  iotrxp;
    phJal_sBFLUpperFrame_t      *bfl_frame = ((phJal_sBFLUpperFrame_t*)(psHwReference->pBoardDriver));
    phcsBflRegCtl_t             *regctl = bfl_frame->RcRegCtl;
    phcsBflOpCtl_t              *opctl  = &(bfl_frame->RcOpCtl);
    phcsBflIo_t                 *io = &(bfl_frame->RcIo);
    uint8_t                      sam_cmd;
    uint32_t                     old_user_timeout;
    uint8_t                      DummyConnectionRequestBuffer[64];
    uint8_t                      DummyConnectionRequestLength;
    phHal_eOpModes_t             DummyOpModes[] = {phHal_eOpModesNfcPassive106, phHal_eOpModesArrayTerminator};
    phHal_sTargetInfo_t          DummyTargetInfo = {0};
    uint8_t                      RfStatus;

    phJal_Trace(psHwReference, "phJalJoiner_Ioctl");

    if (psHwReference == NULL)
    {
        result = PHNFCSTVAL(CID_NFC_DAL, NFCSTATUS_INVALID_PARAMETER);
    } else
    {
        if (psHwReference->Flags.LinkValid == 0)
        {
            result = PHNFCSTVAL(CID_NFC_DAL, NFCSTATUS_INVALID_DEVICE);
        }
    }

    if (result == NFCSTATUS_SUCCESS)
    {
        switch (IoctlCode)
        {
            case PHHALNFC_IOCTL_PN511_SOFTRESET:
                result = phDalNfc_Ioctl(psHwReference, IoctlCode, pInBuf, InLength, pOutBuf, pOutLength);
                break;

            case PHHALNFC_IOCTL_RAW_CMD:
                /* Doing no param checks here as 1:1 handed over to BFL which should check
                   itself. */
                iotrxp.self             = io;
                iotrxp.rx_bits          = 0;
                iotrxp.rx_buffer        = pOutBuf;
                iotrxp.rx_buffer_size   = *pOutLength;
                iotrxp.tx_buffer        = pInBuf;
                iotrxp.tx_buffer_size   = InLength;
                bflstatus = io->Transceive(&iotrxp);
                *pOutLength = iotrxp.rx_buffer_size;
                switch (bflstatus & ~PH_ERR_BFL_COMP_MASK)
                {
                    case PH_ERR_BFL_IO_TIMEOUT:
                        result = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_RF_TIMEOUT);
                        break;
                    case PH_ERR_BFL_BUF_2_SMALL:
                        result = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_BUFFER_TOO_SMALL);
                        break;
                    case PH_ERR_BFL_INVALID_PARAMETER:
                        result = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER);
                        break;
                    case PH_ERR_BFL_INTERFACE_ERROR:
                        result = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_BOARD_COMMUNICATION_ERROR);
                        break;
                    default:
                        result = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_CMD_ABORTED);
                        break;
                }
                break;

            case PHHALNFC_IOCTL_PN511_SETREG:
                if ((pInBuf != NULL) && (InLength >= 2))
                {
                    srp.self     = regctl;
                    srp.address  = pInBuf[0];
                    srp.reg_data = pInBuf[1];
                    bflstatus    = regctl->SetRegister(&srp);
                } else
                {
                    result = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER);
                }
                break;

            case PHHALNFC_IOCTL_PN511_GETREG:
                if ((pInBuf != NULL)  &&
                    (InLength >= 1)   &&
                    (pOutBuf != NULL) &&
                    (*pOutLength >= 1))
                {
                    grp.self     = regctl;
                    grp.address  = pInBuf[0];
                    bflstatus    = regctl->GetRegister(&grp);
                    pOutBuf[0]   = grp.reg_data;
                } else
                {
                    result = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER);
                }
                break;

            case PHHALNFC_IOCTL_PN5XX_SET_USER_TIMEOUT:
                /* Shall never come here, as handled in main file. */
                result = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_DEVICE_REQUEST);
                break;

            case PHHALNFC_IOCTL_PN5XX_SAM_CONFIG:
                if ((pInBuf != NULL) && (InLength >= 1))
                {
                    phJal_GetRfStatus(psHwReference, &RfStatus);
                    sam_cmd                 = pInBuf[0];
                    atp.self                = opctl;
                    switch (sam_cmd)
                    {
                        case PHHALNFC_IOCTL_PN5XX_SAM_CONFIG_RESET:
                            atp.group_ordinal       = PHCS_BFLOPCTL_GROUP_SIGNAL;
                            atp.attrtype_ordinal    = PHCS_BFLOPCTL_ATTR_SIGINOUT;
                            atp.param_a             = (RfStatus == PHJAL_INTRFON) ? PHCS_BFLOPCTL_VAL_READER : PHCS_BFLOPCTL_VAL_CARD;
                            atp.param_b             = PHCS_BFLOPCTL_VAL_NOSIGINOUT;
                            bflstatus = opctl->SetAttrib(&atp);
                        break;

                        case PHHALNFC_IOCTL_PN5XX_SAM_CONFIG_VIRTUAL:
                            old_user_timeout            = psHwReference->UserTimeout;
                            psHwReference->UserTimeout  = 1;
                            DummyTargetInfo.Options.AutoConnectionResponse = 1;
                            /* We start the Target mode here for a very short time, just to
                               let the SW set up the chip. Then the MUX is set. */
                            phJalJoiner_StartTargetMode(psHwReference,
                                                        &DummyTargetInfo,
                                                        DummyOpModes,
                                                        DummyConnectionRequestBuffer,
                                                        &DummyConnectionRequestLength);
                            psHwReference->UserTimeout  = old_user_timeout;
                            atp.group_ordinal           = PHCS_BFLOPCTL_GROUP_SIGNAL;
                            atp.attrtype_ordinal        = PHCS_BFLOPCTL_ATTR_SIGINOUT;
                            atp.param_a                 = PHCS_BFLOPCTL_VAL_CARD;
                            atp.param_b                 = PHCS_BFLOPCTL_VAL_ONLYSIGINOUT;
                            bflstatus = opctl->SetAttrib(&atp);
                            break;

                        case PHHALNFC_IOCTL_PN5XX_SAM_CONFIG_WIRED:
                            /* We reset the interface & wait for min RF reset time: */
                            atp.group_ordinal       = PHCS_BFLOPCTL_GROUP_SIGNAL;
                            atp.attrtype_ordinal    = PHCS_BFLOPCTL_ATTR_SIGINOUT;
                            atp.param_a             = (RfStatus == PHJAL_INTRFON) ? PHCS_BFLOPCTL_VAL_READER : PHCS_BFLOPCTL_VAL_CARD;
                            atp.param_b             = PHCS_BFLOPCTL_VAL_NOSIGINOUT;
                            bflstatus = opctl->SetAttrib(&atp);
                            phOsalNfc_Suspend(5);
                            if (bflstatus == PH_ERR_BFL_SUCCESS)
                            {
                                /* This works in Passive Initiator/Reader Mode. */
                                atp.group_ordinal       = PHCS_BFLOPCTL_GROUP_SIGNAL;
                                atp.attrtype_ordinal    = PHCS_BFLOPCTL_ATTR_SIGINOUT;
                                atp.param_a             = PHCS_BFLOPCTL_VAL_READER;
                                atp.param_b             = PHCS_BFLOPCTL_VAL_SIGINOUT;
                                bflstatus = opctl->SetAttrib(&atp);
                            }
                            break;

                        case PHHALNFC_IOCTL_PN5XX_SAM_CONFIG_BOTH:
                             /* We start the Target mode here for a very short time, just to
                                let the SW set up the chip. Then the MUX is set. */
                            old_user_timeout            = psHwReference->UserTimeout;
                            psHwReference->UserTimeout  = 1;
                            DummyTargetInfo.Options.AutoConnectionResponse = 1;
                            phJalJoiner_StartTargetMode(psHwReference,
                                                        &DummyTargetInfo,
                                                        DummyOpModes,
                                                        DummyConnectionRequestBuffer,
                                                        &DummyConnectionRequestLength);
                            psHwReference->UserTimeout  = old_user_timeout;
                            /* The next call done by the user after this IOCTL must be STM:
                            *  When this is issued, normally
                            *  there is a SoftReset done as well. In this case it must not
                            *  be done. 
                            */
                            bfl_frame->STMDoesSoftReset = 0;
                            atp.group_ordinal           = PHCS_BFLOPCTL_GROUP_SIGNAL;
                            atp.attrtype_ordinal        = PHCS_BFLOPCTL_ATTR_SIGINOUT;
                            atp.param_a                 = PHCS_BFLOPCTL_VAL_CARD;
                            atp.param_b                 = PHCS_BFLOPCTL_VAL_SIGINOUT;
                            bflstatus = opctl->SetAttrib(&atp);
                            break;

                        default:
                            result = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER);
                            break;
                    }
                    if (bflstatus != PH_ERR_BFL_SUCCESS)
                    {
                        result = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_BOARD_COMMUNICATION_ERROR);
                    }
                } else
                {
                    /* Buffer too small: */
                    result = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER);
                }
                break;

            case PHHALNFC_IOCTL_PN511_ENINTR:
                result = phDalNfc_Ioctl(psHwReference, IoctlCode, pInBuf, InLength, pOutBuf, pOutLength);
                break;

            case PHHALNFC_IOCTL_PN511_SETWT:
                if (InLength > 0) 
                {
                    if (pInBuf[0] > JAL_MAX_WT)
                    {
                        result = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER);
                    } else
                    {
                        bfl_frame->CurrentWT = pInBuf[0];
                        bfl_frame->JalWaitingTime = (uint32_t)(WAITING_TIME_UNIT * (1 << bfl_frame->CurrentWT));
                    }
                } else
                {
                    result = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER);
                }
                break;

            default:
                result = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER);
                break;
        }
    }
    return result;
}


extern NFCSTATUS phJalJoiner_Close(phHal_sHwReference_t *psHwReference)
{
    phJal_sBFLUpperFrame_t      *bfl_frame = NULL;
    NFCSTATUS                   command_status = NFCSTATUS_SUCCESS;

    phJal_Trace(psHwReference, "phJalJoiner_Close");
    
    bfl_frame = (phJal_sBFLUpperFrame_t*)psHwReference->pBoardDriver;
    reportDbg0("close func:\t closing\n");
    if (bfl_frame != NULL)
    {
        if (bfl_frame->TargetModeRunningHandle != NULL)
        {
            phOsalNfc_ConsumeSemaphore(bfl_frame->TerminateReceptionHandle);
            bfl_frame->ReceptionHandlingParams.TerminationState = TERMINATE_RECEPTION;
			reportDbg0("close func:\t terminating reception...\n");
            command_status = phOsalNfc_ProduceSemaphore(bfl_frame->TerminateReceptionHandle);
            phOsalNfc_ConsumeSemaphore(bfl_frame->TargetModeRunningHandle);            
            reportDbg0("close func:\t closing Target loop handles\n");            
            
            /* Close reception events: */
            phJal_CloseTargetReceptionHandling(bfl_frame);
            
            reportDbg0("close func:\t closing DEP handles\n");            
            phJal_CloseTargetDepHandlingEvents(bfl_frame);
        }
        /* 
         * Switching RF OFF before DAL-Close (as HW ref is still needed),
         * ignoring the status:
         */
        phJal_SwitchOffRf(psHwReference);

        command_status = phDalNfc_Close(psHwReference);
		reportDbg0("close func:\t device closed\n");
    }              
    else
    {
        command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER);
    }

    return command_status;
}


extern NFCSTATUS phJalJoiner_StartTargetMode(phHal_sHwReference_t     *psHwReference,
                                             phHal_sTargetInfo_t      *pTgInfo,
                                             phHal_eOpModes_t         OpModes[],
                                             uint8_t                  *pConnectionReq,
                                             uint8_t                  *pConnectionReqBufLength)
{
    NFCSTATUS                           command_status = NFCSTATUS_SUCCESS;
    phJal_sBFLUpperFrame_t              *bfl_frame = NULL;
    phcsBflIo_SetWecParam_t             event_callback_param = {0};
    uint32_t                            data_state = NO_STATE;
    const uint8_t                       do_init_chip_now = 1;
    uint32_t                            timeout_value;

    phJal_Trace(psHwReference, "phJalJoiner_StartTargetMode");

    bfl_frame = (phJal_sBFLUpperFrame_t*)psHwReference->pBoardDriver;

    if (bfl_frame->TargetLoopItemsCreated != 0)
    {
        /* If we have started the Target mode before and the thing is still waiting for reception
        within the main Target thread, we need to abort the reception and wait for the thread
        to exit. Then we can create a new one and start over. */
        if (bfl_frame->WaitingForReception != 0)
        {
            /* We are currently waiting to receive data. */
            command_status = phDalNfc_Abort(psHwReference);
            phOsalNfc_ConsumeSemaphore(bfl_frame->TargetModeRestartCleanupSync);
            phOsalNfc_ProduceSemaphore(bfl_frame->TargetModeRestartCleanupSync);
        } else
        {
            /* We are somewhere in the Target processing loop: */
            bfl_frame->ReceptionHandlingParams.TerminationState = TERMINATE_RECEPTION;
            phOsalNfc_ConsumeSemaphore(bfl_frame->TargetModeRestartCleanupSync);
            phOsalNfc_ProduceSemaphore(bfl_frame->TargetModeRestartCleanupSync);
        }
    } else
    {
        /* Nothing to be done, as no items exist (yet). */
    }

    if (command_status == NFCSTATUS_SUCCESS)
    {
        /* Generally, a SR is issued; However for IOCTL (SAM CFG) we need STM as aux function to
        set up the RF frontend after switching the MUX. A SR would cancel the setting - this is why
        SR is not done in this case, controlled by the STMDoesSoftReset flag. */
        if (bfl_frame->STMDoesSoftReset != 0)
        {
            command_status = phJalJoiner_Ioctl(psHwReference, PHHALNFC_IOCTL_PN511_SOFTRESET, NULL, 0, NULL, NULL);
        } else
        {
            bfl_frame->STMDoesSoftReset = 1;
        }
    } else
    {
        /* We don't STM as there was an error! */
    }

    if (command_status != NFCSTATUS_SUCCESS)
    {
        /* ERROR: We do nothing. */
    } else
    {
        bfl_frame->ReceptionHandlingParams.OpModes = OpModes;
        bfl_frame->ReceptionHandlingParams.pTgInfo = pTgInfo;
        bfl_frame->ReceptionHandlingParams.psHwReference = psHwReference;
        bfl_frame->ReceptionHandlingParams.pConnectionReq = pConnectionReq;
        bfl_frame->ReceptionHandlingParams.pConnectionReqBufLength = pConnectionReqBufLength;
        bfl_frame->ReceptionHandlingParams.ReceptionInfoState = NO_STATE;
        bfl_frame->ReceptionHandlingParams.TerminationState = NO_STATE;
        bfl_frame->ReceptionHandlingParams.TargetConfiguration = TARGET_NOT_CONFIGURED;
        bfl_frame->ReceptionHandlingParams.TargetRxSpeed = PHCS_BFLOPCTL_VAL_RF106K;
        bfl_frame->ReceptionHandlingParams.TargetTxSpeed = PHCS_BFLOPCTL_VAL_RF106K;
        bfl_frame->ReceptionHandlingParams.WaitingTimeMultiplier = 1;
        bfl_frame->ReceptionHandlingParams.WTXRequested = 0;
        bfl_frame->NfcTargetEndpoint.TargetEndpointCallback = TargetEndpointCallback;
        bfl_frame->ReceptionHandlingParams.EnableCRCAfterNextReception = 0;
        bfl_frame->ReceptionHandlingParams.UserNotifiedAboutDeselectedState = 0;
        bfl_frame->ReceptionHandlingParams.EndpointDEPMode = 0;

        /* This is req'd in order to prevent memleaks: */
        if (bfl_frame->TargetLoopItemsCreated != 0)
        {
            phJal_CloseTargetReceptionHandling(bfl_frame);
            phJal_CloseTargetDepHandlingEvents(bfl_frame);
        }

        phJal_BuildBflUpperFrame(bfl_frame, 0, bfl_frame->RcRegCtl, do_init_chip_now);
        bfl_frame->IsStartTargetMode = 1;
        memcpy(bfl_frame->TNfcIdT, pTgInfo->NFCID3t, PHCS_BFLNFCCOMMON_ID_LEN);

        event_callback_param.wait_event_cb = bfl_frame->WaitEventCb;

        event_callback_param.self = &bfl_frame->RcIo;
        bfl_frame->ReceptionTimeoutParams.psHwReference = psHwReference;
        event_callback_param.user_ref = &bfl_frame->ReceptionTimeoutParams;
        bfl_frame->RcIo.SetWaitEventCb(&event_callback_param);

        bfl_frame->RcOpCtlAttribParam.group_ordinal = PHCS_BFLOPCTL_GROUP_MODE;
        bfl_frame->RcOpCtlAttribParam.attrtype_ordinal = PHCS_BFLOPCTL_ATTR_PASSIVE;
        bfl_frame->RcOpCtlAttribParam.param_a = PHCS_BFLOPCTL_VAL_RF106K;
        bfl_frame->RcOpCtlAttribParam.param_b = PHCS_BFLOPCTL_VAL_RF106K;
        bfl_frame->RcOpCtl.SetAttrib(&bfl_frame->RcOpCtlAttribParam);

        /* INV-IRQ OFF */
        bfl_frame->RcRegCtlModifyParam.address = PHCS_BFL_JREG_COMMIEN;
	    bfl_frame->RcRegCtlModifyParam.mask    = 0x80;
	    bfl_frame->RcRegCtlModifyParam.set     = 0x00;
        bfl_frame->RcRegCtl->ModifyRegister(&bfl_frame->RcRegCtlModifyParam);

	    /* Enable IRQPushPull so that the PN511 actively drives the signal */
	    bfl_frame->RcRegCtlModifyParam.address = PHCS_BFL_JREG_DIVIEN;
	    bfl_frame->RcRegCtlModifyParam.mask    = 0x80;
	    bfl_frame->RcRegCtlModifyParam.set     = 0xFF;
	    bfl_frame->RcRegCtl->ModifyRegister(&bfl_frame->RcRegCtlModifyParam);

        ConfigureTargetIds(&bfl_frame->ReceptionHandlingParams);

        command_status = phJal_SetupTargetDepHandlingEvents(bfl_frame);
        if (command_status == NFCSTATUS_SUCCESS)
        {
            command_status = phJal_SetupTargetReceptionHandling(bfl_frame); 
            if (command_status == NFCSTATUS_SUCCESS)
            {
                reportDbg0("start target:\t waiting for reception event\n");
                /* Double time to avoid race cond.: */
                timeout_value = bfl_frame->ReceptionTimeoutParams.psHwReference->UserTimeout * 2;

                /* 
                 *    Change of WaitForSemaphoreProtectedEvent() function in StartTargetMode:
                 *
                 *          Since the same handle (ReceptionInfoState) is used in two threads - 
                 *          the current thread (phJalJoiner_StartTargetMode) and the other thread 
                 *          namely, phJal_ReceptionHandling thread, this leads to some deadlock 
                 *          condition. So, two handles namely ReceptionInfoState_STM for the former
                 *          thread and ReceptionInfoState_RHT for the latter are introduced.
                 *
                 */

                //command_status = WaitForSemaphoreProtectedEvent(bfl_frame->ReceptionInfoHandle, 
                //    &bfl_frame->ReceptionHandlingParams.ReceptionInfoState,
                //    DATA_RECEIVED|RECEPTION_TIMEOUT|AUTOMATIC_ATR_RES_SENT|
                //    TRANSMISSION_ERROR|TERMINATE_RECEPTION|RECEPTION_ABORTED|INTERFACE_ERROR,
                //    &data_state, timeout_value);
			    
                phOsalNfc_ConsumeSemaphore(bfl_frame->ReceptionInfoHandle_STM);
                data_state = bfl_frame->ReceptionHandlingParams.ReceptionInfoState;
                bfl_frame->ReceptionHandlingParams.ReceptionInfoState = NO_STATE;

                reportDbg2("start target:\t got reception event %02X with status 0x%02X\n", data_state, command_status);

                if (command_status == NFCSTATUS_SUCCESS)
                {
                    switch (data_state)
                    {
                        case RECEPTION_ABORTED:
                            command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_CMD_ABORTED);
                            break;

                        case INTERFACE_ERROR:
                            command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_BOARD_COMMUNICATION_ERROR);
                            break;

                        case RECEPTION_TIMEOUT:
                        case TRANSMISSION_ERROR:
                        case TERMINATE_RECEPTION:
                            command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_RF_TIMEOUT);
                            break;

                        default:
                            /* No adjustment. */
                            break;
                    }
                } else
                {
                    command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_CMD_ABORTED);
                }

                /* Peer to Peer BugFix: The following Semaphore Handle created for the Mantis entry #586
                 *                      causes problems in the Peer to Peer communications as explained 
                 *                      in the mantis entry #941 as well as in the peer to peer communication
                 *                      under Linux. So, this semaphore is removed
                 */

                /* Resync function exit with the end of processing in reception loop: */
                //phOsalNfc_ConsumeSemaphore(bfl_frame->TargetModeFunctionReturnHandle);
                //phOsalNfc_ProduceSemaphore(bfl_frame->TargetModeFunctionReturnHandle);


            }
            else
            {
                reportDbg1("start target:\t error during reception handling setup (status: 0x%02X\n", 
                    command_status);
                phJal_CloseTargetDepHandlingEvents(bfl_frame);
                command_status = (PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INSUFFICIENT_RESOURCES));
            }
        }
        else
        {
            command_status = (PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INSUFFICIENT_RESOURCES));
        }
        
        bfl_frame->ReceptionHandlingParams.OpModes = NULL;
    }
    bfl_frame->IsStartTargetMode = 0;
    return command_status;
}



extern NFCSTATUS phJalJoiner_Receive(phHal_sHwReference_t        *psHwReference,
                                     phHal_sDepAdditionalInfo_t  *psDepAdditionalInfo,
                                     uint8_t                     *pRecvBuf,
                                     uint16_t                    *pRecvLength)
{
    NFCSTATUS               command_status = NFCSTATUS_SUCCESS;
    phJal_sBFLUpperFrame_t  *bfl_frame = NULL;
	uint8_t                 target_configuration = TARGET_NOT_CONFIGURED;
    uint8_t                 start_reception = 0;

    phJal_Trace(psHwReference, "phJalJoiner_Receive");

    bfl_frame = (phJal_sBFLUpperFrame_t*)psHwReference->pBoardDriver;

    /* Change of WFSPE() function in StartTargetMode - Addition of Handle: */
	if ((bfl_frame->ReceptionInfoHandle_STM != NULL) && 
        (bfl_frame->ReceptionInfoHandle_RHT != NULL) &&
		(bfl_frame->TargetInfoHandle != NULL))
    {
		command_status = phOsalNfc_ConsumeSemaphore(bfl_frame->TargetInfoHandle);
		reportDbg0("receive func:\t started\n");
        if (command_status == NFCSTATUS_SUCCESS)
        {
			target_configuration = bfl_frame->ReceptionHandlingParams.TargetConfiguration;
			command_status = phOsalNfc_ProduceSemaphore(bfl_frame->TargetInfoHandle);
			if (command_status == NFCSTATUS_SUCCESS)
			{                
				switch(target_configuration)
                {
                    case TARGET_DESELECTED:				
                        if (bfl_frame->ReceptionHandlingParams.UserNotifiedAboutDeselectedState == 0)
                        {
                            start_reception = 0;
                            command_status = (PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_DESELECTED));
                            /* At the next call the user is allowed to wait for new requests */
                            bfl_frame->ReceptionHandlingParams.UserNotifiedAboutDeselectedState = 1;
                        }
                        else
                        {
                            start_reception = 1;
                        }
                        break;
                    case TARGET_RELEASED:                
                        start_reception = 0;
                        command_status = (PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_RELEASED));
                        break;
                    case TARGET_NOT_CONFIGURED:                    
                        start_reception = 0;
                        command_status = (PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_DEVICE_REQUEST));
                        break;
                    case TARGET_CONFIGURATION_IN_PROGRESS:
                    case TARGET_DESELECT_IN_PROGRESS:
                    case TARGET_DESELECT_CONFIGURATION:
                    case TARGET_WAKED_UP:
                    case TARGET_READY:
                        start_reception = 1;
                        break;
                    default:
                        start_reception = 0;
                        command_status = (PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_DEVICE_REQUEST));
                        break;
                }

                if (start_reception == 1)
                {
                    command_status = phJal_ReceiveDataFromEndpoint(bfl_frame, 
                                                                   psDepAdditionalInfo,
                                                                   pRecvBuf,
                                                                   pRecvLength);
					reportDbg2("receive func:\t returning %d bytes to user (status: %02X)\n", 
                        *pRecvLength, command_status);
                }
                else
                {
                    *pRecvLength = 0;
                }
            }
            else
            {
                *pRecvLength = 0;
            }
        }
        else
        {
            *pRecvLength = 0;
        }
    }
    else
    {
        *pRecvLength = 0;
        command_status = (PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER));
    }

    return command_status;
}



extern NFCSTATUS phJalJoiner_Send(phHal_sHwReference_t          *psHwReference,
                                  phHal_sDepAdditionalInfo_t    *psDepAdditionalInfo,
                                  uint8_t                       *pSendBuf,
                                  uint16_t                      SendLength)
{
    NFCSTATUS				command_status = NFCSTATUS_SUCCESS;
    phJal_sBFLUpperFrame_t  *bfl_frame = NULL;
	uint8_t                 target_configuration = TARGET_NOT_CONFIGURED;

    phJal_Trace(psHwReference, "phJalJoiner_Send");

    bfl_frame = (phJal_sBFLUpperFrame_t*)psHwReference->pBoardDriver;
   
    /* Change of WFSPE() function in StartTargetMode - Addition of Handle: */
	if ((bfl_frame->ReceptionInfoHandle_STM != NULL) && 
        (bfl_frame->ReceptionInfoHandle_RHT != NULL) &&
		(bfl_frame->TargetInfoHandle != NULL))
    {
        command_status = phOsalNfc_ConsumeSemaphore(bfl_frame->TargetInfoHandle);
        if (command_status == NFCSTATUS_SUCCESS)
        {
			target_configuration = bfl_frame->ReceptionHandlingParams.TargetConfiguration;
			command_status = phOsalNfc_ProduceSemaphore(bfl_frame->TargetInfoHandle);
            if (target_configuration == TARGET_NOT_CONFIGURED)
            {
                if (SendLength <= PHJAL_TRXBL)
                {
                    command_status = 
                        phJal_SendDataToEndpoint(bfl_frame,
											    psDepAdditionalInfo,
											    pSendBuf,
						    			        SendLength);
                }
                else
                {
                    /* SendToLength exceeds max. length for ATR response */
                    command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_DEVICE_REQUEST);
                }
            }
            else if (bfl_frame->ReceptionHandlingParams.EndpointDEPMode == DEP_TRANSMISSION)
            {
                reportDbg0("send func:\t started\n");
                command_status = 
                    phJal_SendDataToEndpoint(bfl_frame,
											psDepAdditionalInfo,
											pSendBuf,
						    			    SendLength);
            }
		}
        else
        {
            reportDbg1("send func:\t current DEP-Mode %02X\n", bfl_frame->ReceptionHandlingParams.EndpointDEPMode);
            command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_DEVICE_REQUEST);
        }
    }
    else
    {
        command_status = (PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER));
    }
    reportDbg1("send func:\t returned with state %02X\n", command_status);
    return command_status;
}

#endif /* Guard */