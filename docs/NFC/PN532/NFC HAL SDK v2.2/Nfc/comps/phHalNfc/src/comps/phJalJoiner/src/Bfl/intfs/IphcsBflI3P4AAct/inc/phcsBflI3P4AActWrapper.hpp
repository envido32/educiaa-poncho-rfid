/* /////////////////////////////////////////////////////////////////////////////////////////////////
//                     Copyright (c) Philips Semiconductors
//
//                       (C)PHILIPS Electronics N.V.2004
//         All rights are reserved. Reproduction in whole or in part is
//        prohibited without the written consent of the copyright owner.
//    Philips reserves the right to make changes without notice at any time.
//   Philips makes no warranty, expressed, implied or statutory, including but
//   not limited to any implied warranty of merchantability or fitness for any
//  particular purpose, or that the use will not infringe any third party patent,
//   copyright or trademark. Philips must not be liable for any loss or damage
//                            arising from its use.
///////////////////////////////////////////////////////////////////////////////////////////////// */

/*! 
 * \if cond_generate_filedocu 
 * \file phcsBflI3P4AActWrapper.hpp
 *
 * Project: ISO 14443.4.
 *
 * Workfile: phcsBflI3P4AActWrapper.hpp
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Date: Thu Sep 27 09:38:57 2007 $
 * $KeysEnd$
 *
 * Comment:
 *  T=CL class framework wrapper to enable multi-instance operation.
 *
 * History:
 *  GK:  Generated 09. Sept. 2003
 *  MHa: Migrated to MoReUse September 2005
 * \endif
 *
 */

#ifndef PHCSBFLI3P4AACTWRAPPER_HPP
#define PHCSBFLI3P4AACTWRAPPER_HPP

#include <phcsBflIoWrapper.hpp>

extern "C"
{
    #include <phcsBflI3P4AAct.h>
}

namespace phcs_BFL
{

/*! \cond cond_Cpp_complete */
/* \ingroup iso14443_4A_entry
 *  Functionality according to T=CL (A) protocol entry/activation
 *  (abstract class, used for the C++ interface).
 */

class phcsBfl_I3P4AAct 
{
    public:
        virtual phcsBfl_Status_t Initialise(uint8_t                *p_trxbuffer,
                                          uint16_t                trxbuffersize,
                                          class phcsBfl_Io         *p_lower)                 = 0;
        virtual phcsBfl_Status_t Rats(phcsBflI3P4AAct_RatsParam_t *rats_param)                 = 0;
        virtual phcsBfl_Status_t Pps(phcsBflI3P4AAct_PpsParam_t *pps_param)                    = 0;
        virtual phcsBflI3P4_ProtParam_t *ProtocolParameters(void)                            = 0;

        /*! Lower edge interface: Not to be redefined in the derived class: */
        class phcsBfl_Io *mp_Lower;
};


////////////////////////////////////////////////////////////////////////////////////////////////////
/* \ingroup iso14443_4A_entry
 *  This class glues together the C-implementation of T=CL Protocol Entry and
 *  the underlying class framework. By declaring the member as _ static _ the
 *  method gets no implicit _ this _ pointer and can thus be called by any C
 *  function. However, the function must be informed about the target object
 *  of type phcsBfl_Io (casted to void*) to be called further down the stack.
 */
class phcsBfl_I3P4AActGlue
{
    public:
        phcsBfl_I3P4AActGlue(void);
        ~phcsBfl_I3P4AActGlue(void);
        
        //! \name Static function, enable to call into C++ again:
        //@{
        static phcsBfl_Status_t Transceive(phcsBflIo_TransceiveParam_t *transceive_param);
        //@}

        //! Structure accessible from the upper object's pure C-kernel.
        phcsBflIo_t m_GlueStruct;
};
/*! \endcond */


////////////////////////////////////////////////////////////////////////////////////////////////////
/*! \ingroup iso14443_4A_entry
 *  \brief ISO 14443.4 communication protocol (T=CL) entry class for PICC Type A.
 */
class phcsBflI3P4AAct_Wrapper : public phcsBfl_I3P4AAct 
{
    public:
        phcsBflI3P4AAct_Wrapper(void);
        ~phcsBflI3P4AAct_Wrapper(void);

        /*
         * \param[in] *p_trxbuffer      Pointer to the transmit/receive buffer to be used for protocol operation.
         * \param[in]  trxbuffersize    Size of p_trxbuffer.
         * \param[in] *p_lower          Pointer to the C++ object of the underlying layer.
         *
         * The alternative Constructor does setup required by the Component but not done
         * by the default constructor. The parameters of this function have the same purpose
         * as those of Iso14443_4A_Activation_Initialise.
         */
        phcsBflI3P4AAct_Wrapper(uint8_t            *p_trxbuffer,
                              uint16_t            trxbuffersize,
                              class phcsBfl_Io     *p_lower);

        /*!
         * \param[in] *p_trxbuffer      Pointer to the transmit/receive buffer to be used for protocol operation.
         * \param[in]  trxbuffersize    Size of p_trxbuffer.
         * \param[in] *p_lower          Pointer to the C++ object of the underlying layer.
         * \return                      phcsBfl_Status_t value depending on the C implementation
         *
         * The Initialise method does setup required by the Component but not done
         * by the default constructor. The parameters of this function have the same purpose
         * as those of Iso14443_4A_Activation_Initialise.
         */
        phcsBfl_Status_t Initialise(uint8_t          *p_trxbuffer,
                                  uint16_t          trxbuffersize,
                                  class phcsBfl_Io   *p_lower);


        phcsBfl_Status_t Rats(phcsBflI3P4AAct_RatsParam_t *rats_param);
        phcsBfl_Status_t Pps(phcsBflI3P4AAct_PpsParam_t *pps_param);

        /*!
         * \return                      Structure of protocol parameters filled.
         *
         * Protocol parameters retrieval function to be called before activating the protocol. */
        phcsBflI3P4_ProtParam_t *ProtocolParameters(void);

        /*! \brief Constructor status: This status gives information about whether the object has been
         *  allocated/configured correctly or not. It is a good idea to check this status (verify
         *  that it is set to PH_ERR_BFL_SUCCESS) before proceeding.
         */
        phcsBfl_Status_t            m_ConstructorStatus;

        /*! \brief Protocol parameters structure filled by the initialisation of a PICC. Hand this
         *  structure over to the phcsBfl_I3P4 protocol communication class object.
         */
        phcsBflI3P4_ProtParam_t     m_ProtocolParams;
        /*! \brief C-interface of this component. */
        phcsBflI3P4AAct_t           m_Interface;

    protected:
        /*! \brief Glue class object. */
        class phcsBfl_I3P4AActGlue  m_I3P4A_Act_Glue;
};

}   /* phcs_BFL */

#endif /* PHCSBFLI3P4AACTWRAPPER_HPP */

/* E.O.F. */
