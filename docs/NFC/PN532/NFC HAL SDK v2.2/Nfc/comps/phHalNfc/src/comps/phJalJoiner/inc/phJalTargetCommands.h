/*
 *         Copyright (c), NXP Semiconductors
 *
 *                       (C)NXP Electronics N.V.2004
 *         All rights are reserved. Reproduction in whole or in part is
 *        prohibited without the written consent of the copyright owner.
 *    NXP reserves the right to make changes without notice at any time.
 *   NXP makes no warranty, expressed, implied or statutory, including but
 *   not limited to any implied warranty of merchantability or fitness for any
 *  particular purpose, or that the use will not infringe any third party patent,
 *   copyright or trademark. NXP must not be liable for any loss or damage
 *                            arising from its use.
 *
 */
 
/*!
 * \file phJalTargetCommands.h
 * \brief JAL Functions implementing Target Endpoint Commands
 *
 *
 * Project: NFC MW / HAL
 *
 * $Date: Thu Sep 27 09:38:42 2007 $ 
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Aliases: SDK_HAL_2.2,SDK_HAL_2.2_071008,NFC_FRI_SDK_1.0_071031 $
 *
 */

#ifndef PHJALTARGETMODECOMMANDS_H
#define PHJALTARGETMODECOMMANDS_H
 
#define PHJALTARGETCOMMANDS_FILEREVISION "$Revision: 1.1 $"
#define PHJALTARGETCOMMANDS_FILEALIASES  "$Aliases: SDK_HAL_2.2,SDK_HAL_2.2_071008,NFC_FRI_SDK_1.0_071031 $"
 
#include <phNfcTypes.h>
#include <phNfcHalTypes.h>
 
#include <phcsBflNfc.h>
#include <phcsBflStatus.h>
#include <phJalJoinerBflFrame.h>
#include <phJalJoinerAux.h>

/**
\anchor TOX_MULTIPLIER
*/ 
#define TOX_MULTIPLIER  1
 
/**
 * \ingroup grp_jal_nfct_aux_functions
 * Structure containing state variables and handles for event handling.
 */
typedef struct phJal_WaitForUserEventParams
{
    phJal_ReceptionHandlingParams_t *psReceptionHandlingParams;
    uint32_t                        EventStateToWait;
    uint32_t                        WaitingTime;
    void                            *pEventHandle;
    uint32_t                        *pEventState;
    uint32_t                        *pDataState;
} phJal_WaitForUserEventParams_t;

/**
 * \ingroup grp_jal_nfct_aux_functions
 * \brief Function handling incoming ATR requests.
 *
 * Depending on the selected Response Mode (manual/automatic), the function
 * will dispatch to the corresponding sub-function.   
 */ 
phcsBfl_Status_t HandleAtrRequest(phJal_ReceptionHandlingParams_t *psReceptionHandlingParams,
                                  phcsBflNfc_TargetEndpointParam_t *psEndpointParam);

/**
 * \ingroup grp_jal_nfct_aux_functions
 * \brief Function handling the ATR Response if the manual mode was selected.
 *
 * At first the function will inform \ref phHalNfc_StartTargetMode that a ATR 
 * was received and thus will cause it to return. Because of the manual mode,
 * it now will what for the user to pass the response to the Endpoint. After
 * the response bytes were received, they will be copied to the TRX-buffer and
 * the function returns.
 */ 
phcsBfl_Status_t HandleAtrManualResponse(phJal_ReceptionHandlingParams_t    *psReceptionHandlingParams,
                                          phcsBflNfc_TargetEndpointParam_t  *psEndpointParam);

/**
 * \ingroup grp_jal_nfct_aux_functions
 * \brief Function handling the ATR Response if the automatic mode was selected.
 *
 * It just copies the previously provided (\ref phHalNfc_StartTargetMode) response bytes
 * to the TRX-buffer, sets the Waiting Time to be used by the Initiator, and returns.
 */ 
phcsBfl_Status_t HandleAtrAutomaticResponse(phJal_ReceptionHandlingParams_t   *psReceptionHandlingParams,
                                             phcsBflNfc_TargetEndpointParam_t *psEndpointParam);
 
/**
 * \ingroup grp_jal_nfct_aux_functions
 * \brief Function handling an ATR Request during re-connecting to a Target in passive mode.
 *
 * When the Target is connected to a Initiator the first time, the ATR Response will be stored
 * in \ref phJal_sBFLUpperFrame_t::AtrResponseBuffer to be later used by this function. 
 * In case of a \ref phHalNfc_Connect call, the ATR Response will be copied from the previously 
 * stated buffer to the TRX-buffer.
 */
phcsBfl_Status_t HandleAtrResponseForPassiveConnect(phJal_ReceptionHandlingParams_t   *psReceptionHandlingParams,
                                                     phcsBflNfc_TargetEndpointParam_t *psEndpointParam);
 
/**
 * \ingroup grp_jal_nfct_aux_functions
 * \brief Function handling a DSL Request.
 */ 
phcsBfl_Status_t HandleDslRequest(phJal_ReceptionHandlingParams_t   *psReceptionHandlingParams,
                                  phcsBflNfc_TargetEndpointParam_t  *psEndpointParam);

/**
 * \ingroup grp_jal_nfct_aux_functions
 * \brief Function handling a PSL Request.
 *
 * It sets the RX speed according to the received values.
 */  
phcsBfl_Status_t HandlePslRequest(phJal_ReceptionHandlingParams_t   *psReceptionHandlingParams,
                                   phcsBflNfc_TargetEndpointParam_t  *psEndpointParam);

/**
 * \ingroup grp_jal_nfct_aux_functions
 * \brief Function handling a PSL Response.
 *
 * It sets the TX speed according to the received values.
 */   
phcsBfl_Status_t HandlePslResponse(phJal_ReceptionHandlingParams_t  *psReceptionHandlingParams,
                                   phcsBflNfc_TargetEndpointParam_t *psEndpointParam);

/**
 * \ingroup grp_jal_nfct_aux_functions
 * \brief Function handling a DEP Request.
 *
 * All DEP Request will at first be forwarded to this function. Depending on
 * wether data has to be passed to or from the user, handling will be dispatched to
 * \ref phJal_HandleDataTransferToUser or \ref phJal_HandleDataTransferFromUser.
 * 
 */ 
phcsBfl_Status_t HandleDepRequest(phJal_ReceptionHandlingParams_t   *psReceptionHandlingParams,
                                  phcsBflNfc_TargetEndpointParam_t  *psEndpointParam);
 
/**
 * \ingroup grp_jal_nfct_aux_functions
 * \brief Function handling a RLS Request.
 *
 * In case of a Release Request to Reception Loop used to retrieve data from an
 * Initiator will be terminated. 
 * \note To allow proper destruction of all event handles \ref phHalNfc_Close shall
 * be called.
 */
phcsBfl_Status_t HandleRlsRequest(phJal_ReceptionHandlingParams_t   *psReceptionHandlingParams,
                                  phcsBflNfc_TargetEndpointParam_t  *psEndpointParam);
  
/**
 * \ingroup grp_jal_nfct_aux_functions
 * \brief Function retrieving user events according to the values of the passed structure.
 *
 */
NFCSTATUS RetrieveUserEvents(phJal_WaitForUserEventParams_t  *psUserParams);

/**
 * \ingroup grp_jal_nfct_aux_functions
 * \brief Function increasing the TOX by the value of \ref TOX_MULTIPLIER and
 * set it for a DEP Response.
 */
void SetWTXResponse(phJal_ReceptionHandlingParams_t  *psReceptionHandlingParams, 
                    phcsBflNfc_TargetEndpointParam_t *psEndpointParam);
 
/**
 * \ingroup grp_jal_nfct_aux_functions
 * \brief Function handling WTX during DEP Requests.
 *
 * It is used to increase the TOX value when the user needs additional processing time. 
 */
phcsBfl_Status_t HandleWTXForUserData(phJal_WaitForUserEventParams_t   *psUserParams,
                                      phcsBflNfc_TargetEndpointParam_t *psEndpointParam,
                                      uint32_t                          StatesToRequestWTX);

/**
 * \ingroup grp_jal_nfct_aux_functions
 * \brief Function used to wait for specific events including timeout indication.
 */  
void phJal_WaitForUserEvent(void *Parameters);

/**
 * \ingroup grp_jal_nfct_aux_functions
 * \brief Function transferring from Target Endpoint to user.
 *
 * It is used within the Target Endpoint to transfer data from the Reception Thread
 * to the Main (User) Thread.
 * Further details about how these two threads interact can be obtained in \ref grp_jal_target_events.
 */   
phcsBfl_Status_t phJal_HandleDataTransferToUser(phJal_ReceptionHandlingParams_t   *psReceptionHandlingParams,
                                                phcsBflNfc_TargetEndpointParam_t  *psEndpointParam);                                                

/**
 * \ingroup grp_jal_nfct_aux_functions
 * \brief Function transferring data from user to Target Endpoint.
 *
 * It is used within the Target Endpoint to ransfer data from the Main (User) Thread to the
 * Reception Thread
 * Further details about how these two threads interact can be obtained in \ref grp_jal_target_events.
 */    
phcsBfl_Status_t phJal_HandleDataTransferFromUser(phJal_ReceptionHandlingParams_t   *psReceptionHandlingParams,
                                                  phcsBflNfc_TargetEndpointParam_t  *psEndpointParam);
 
 
 #endif
