/*
 *         Copyright (c), NXP Semiconductors
 *
 *                       (C)NXP Electronics N.V.2004
 *         All rights are reserved. Reproduction in whole or in part is
 *        prohibited without the written consent of the copyright owner.
 *    NXP reserves the right to make changes without notice at any time.
 *   NXP makes no warranty, expressed, implied or statutory, including but
 *   not limited to any implied warranty of merchantability or fitness for any
 *  particular purpose, or that the use will not infringe any third party patent,
 *   copyright or trademark. NXP must not be liable for any loss or damage
 *                            arising from its use.
 *
 */


/*!
 * \file phJalJoinerAux.c
 * \brief For function declarations see \ref phJalJoinerAux.h
 *
 *
 * Project: NFC MW / HAL
 *
 * $Date: Thu Sep 27 09:38:43 2007 $ 
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Aliases: SDK_HAL_2.2,SDK_HAL_2.2_071008 $
 *
 */

#ifdef PHHAL_VERSION_ACQUISITION
    #define PHJALJOINERAUX_SOURCE_FILEREVISION "$Revision: 1.1 $"
    #define PHJALJOINERAUX_SOURCE_FILEALIASES  "$Aliases: SDK_HAL_2.2,SDK_HAL_2.2_071008 $"
#else /* PHHAL_VERSION_ACQUISITION */

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include <phJalJoinerAux.h>
#include <phJalDebugReportFunctions.h>

#include <phNfcStatus.h>
#include <phcsBflOpCtl_Hw1Ordinals.h>
#include <phcsBflHw1Reg.h>
#include <phcsBflOpCtl.h>
#include <phcsBflI3P4AAct.h>
#include <phcsBflI3P4.h>
#include <phcsBflNfcCommon.h>
#include <phcsBflNfc.h>
#include <phOsalNfc.h>

#define FORMAT_BYTE_POS         0x01

#define SAK_NFC_COMPARATOR      0x40
#define SAK_TCL_COMPARATOR      0x20
#define SAK_MIFARE_COMPARATOR   0x08


phcsBfl_Status_t phJal_ConfigureHardwareForPolling(phHal_sHwReference_t *psHwReference)
{
    phcsBfl_Status_t bfl_status = PH_ERR_BFL_SUCCESS;
    phJal_sBFLUpperFrame_t *bfl_frame = 
        (phJal_sBFLUpperFrame_t*)psHwReference->pBoardDriver;

    phJal_Trace(psHwReference, "phJal_ConfigureHardwareForPolling");
    
    if (bfl_frame != NULL)
    {
        bfl_frame->RcRegCtlSetRegParam.address = PHCS_BFL_JREG_CONTROL;
        bfl_frame->RcRegCtlSetRegParam.reg_data = PHCS_BFL_JBIT_INITIATOR;
        bfl_status = bfl_frame->RcRegCtl->SetRegister(&bfl_frame->RcRegCtlSetRegParam);
        if (bfl_status == PH_ERR_BFL_SUCCESS)
        {
            bfl_status = phJal_ActivateRfField(psHwReference);
        }
    }
    else
    {
        bfl_status = PH_ERR_BFL_INVALID_PARAMETER;
    }

    return bfl_status; 
}


phcsBfl_Status_t phJal_GetRfStatus(phHal_sHwReference_t *psHwReference,
                                   uint8_t              *rf_status)
{
    phcsBfl_Status_t bfl_status;

    phJal_sBFLUpperFrame_t *bfl_frame = 
        (phJal_sBFLUpperFrame_t*)psHwReference->pBoardDriver;
    
    phJal_Trace(psHwReference, "phJal_GetRfStatus");

    *rf_status = PHJAL_NORFON;

    /* Check internal RF status: */
    bfl_frame->RcRegCtlGetRegParam.address = PHCS_BFL_JREG_TXCONTROL;
    bfl_status = bfl_frame->RcRegCtl->GetRegister(&bfl_frame->RcRegCtlGetRegParam);
    if (bfl_status != PH_ERR_BFL_SUCCESS)
    {
        /* error */
    } else
    {
        /* check wether OWN RF field is activated or not */
        if ((bfl_frame->RcRegCtlGetRegParam.reg_data & (PHCS_BFL_JBIT_TX2RFEN | PHCS_BFL_JBIT_TX1RFEN)) ==
            (PHCS_BFL_JBIT_TX2RFEN | PHCS_BFL_JBIT_TX1RFEN))
        {
            *rf_status |= PHJAL_INTRFON;
        } else
        {
            /* No action. */
        }
        
        /* Check EXTERNAL RF status: */
        bfl_frame->RcRegCtlGetRegParam.address = PHCS_BFL_JREG_STATUS1;
        bfl_status = bfl_frame->RcRegCtl->GetRegister(&bfl_frame->RcRegCtlGetRegParam);
        if (bfl_status != PH_ERR_BFL_SUCCESS)
        {
            /* error */
        } else
        {
            if ((bfl_frame->RcRegCtlGetRegParam.reg_data & PHCS_BFL_JBIT_RFON) == PHCS_BFL_JBIT_RFON)
            {
                *rf_status |= PHJAL_EXTRFON;
            } else
            {
                /* No Action. */
            }
        }
    }
    return bfl_status;
}


phcsBfl_Status_t phJal_SwitchOffRf(phHal_sHwReference_t *psHwReference)
{
    phcsBfl_Status_t status;

    phJal_sBFLUpperFrame_t *bfl_frame = 
        (phJal_sBFLUpperFrame_t*)psHwReference->pBoardDriver;
    
    phJal_Trace(psHwReference, "phJal_SwitchOffRf");

    if (bfl_frame == NULL)
    {
        return PH_ERR_BFL_INVALID_PARAMETER;
    }

    bfl_frame->RcRegCtlModifyParam.address = PHCS_BFL_JREG_TXAUTO;
    bfl_frame->RcRegCtlModifyParam.self = bfl_frame->RcRegCtl;
    bfl_frame->RcRegCtlModifyParam.mask = PHCS_BFL_JBIT_TX1RFAUTOEN | PHCS_BFL_JBIT_TX2RFAUTOEN;
    bfl_frame->RcRegCtlModifyParam.set = 0;

    status = bfl_frame->RcRegCtl->ModifyRegister(&(bfl_frame->RcRegCtlModifyParam)); 


    bfl_frame->RcRegCtlModifyParam.address = PHCS_BFL_JREG_TXCONTROL;
    bfl_frame->RcRegCtlModifyParam.self = bfl_frame->RcRegCtl;
    bfl_frame->RcRegCtlModifyParam.mask = PHCS_BFL_JBIT_TX1RFEN | PHCS_BFL_JBIT_TX2RFEN;
    bfl_frame->RcRegCtlModifyParam.set = 0;

    if(status == PH_ERR_BFL_SUCCESS)
    {
        status = bfl_frame->RcRegCtl->ModifyRegister(&(bfl_frame->RcRegCtlModifyParam)); 
    }

    return status;
}

phcsBfl_Status_t phJal_ActivateRfField(phHal_sHwReference_t *psHwReference)
{
    phcsBfl_Status_t        bfl_status = PH_ERR_BFL_SUCCESS;
    phJal_sBFLUpperFrame_t *bfl_frame  = (phJal_sBFLUpperFrame_t*)psHwReference->pBoardDriver;
    uint8_t                 rf_status  = PHJAL_NORFON;

    phJal_Trace(psHwReference, "phJal_ActivateRfField");

    if (bfl_frame == NULL)
    {
        return PH_ERR_BFL_INVALID_PARAMETER;
    }

    /* RegDumpFile("fiel_act_dump.txt", "a","before activate rf", 0, 1, bfl_frame->RcRegCtl); // Debug Only */

    bfl_frame->RcRegCtlModifyParam.address = PHCS_BFL_JREG_TXAUTO;
    bfl_frame->RcRegCtlModifyParam.mask    = PHCS_BFL_JBIT_INITIALRFON | PHCS_BFL_JBIT_TX2RFAUTOEN | PHCS_BFL_JBIT_TX1RFAUTOEN;
    bfl_frame->RcRegCtlModifyParam.set     = 1;
    bfl_status = bfl_frame->RcRegCtl->ModifyRegister(&bfl_frame->RcRegCtlModifyParam);
    if (bfl_status != PH_ERR_BFL_SUCCESS)
    {
        return bfl_status;
    }

    bfl_status = SetTimeOut(bfl_frame->RcRegCtl, &bfl_frame->RcOpCtl, 
        FIELD_ACTIVATION_DELAY, USED_PRESCALER_VALUE);
    
    if (bfl_status != PH_ERR_BFL_SUCCESS)
    {
        return bfl_status;
    }
    else
    {
        if (bfl_status != PH_ERR_BFL_SUCCESS)
        {
            return bfl_status;
        }
        else
        {
            
            bfl_status = StartTimer(psHwReference);
        }
    }

    bfl_frame->RcRegCtlGetRegParam.address = PHCS_BFL_JREG_COMMIRQ;
    do 
    {
        bfl_status = bfl_frame->RcRegCtl->GetRegister(&bfl_frame->RcRegCtlGetRegParam);
    }
    while (!(bfl_frame->RcRegCtlGetRegParam.reg_data & PHCS_BFL_JBIT_TIMERI) && 
        (bfl_status == PH_ERR_BFL_SUCCESS));
    
    if (bfl_status != PH_ERR_BFL_SUCCESS)
    {
        return bfl_status;
    }

    bfl_frame->RcRegCtlSetRegParam.address  = PHCS_BFL_JREG_COMMIRQ;
    bfl_frame->RcRegCtlSetRegParam.reg_data = PHCS_BFL_JBIT_TIMERI;
    bfl_status = bfl_frame->RcRegCtl->SetRegister(&bfl_frame->RcRegCtlSetRegParam);
    if (bfl_status != PH_ERR_BFL_SUCCESS)
    {
        return bfl_status;
    }

    bfl_frame->RcRegCtlModifyParam.address = PHCS_BFL_JREG_TXAUTO;
    bfl_frame->RcRegCtlModifyParam.mask    = PHCS_BFL_JBIT_INITIALRFON;
    bfl_frame->RcRegCtlModifyParam.set     = 0;
    bfl_status = bfl_frame->RcRegCtl->ModifyRegister(&bfl_frame->RcRegCtlModifyParam); 
    if (bfl_status != PH_ERR_BFL_SUCCESS)
    {
        return bfl_status;
    }

    bfl_frame->RcRegCtlGetRegParam.address = PHCS_BFL_JREG_TXCONTROL;
    bfl_status = bfl_frame->RcRegCtl->GetRegister(&bfl_frame->RcRegCtlGetRegParam);
    if (bfl_status != PH_ERR_BFL_SUCCESS)
    {
        return bfl_status;
    }

    if ((phJal_GetRfStatus(psHwReference, &rf_status)) != PH_ERR_BFL_SUCCESS)
    {
        return bfl_status;
    }

    if ((rf_status & PHJAL_INTRFON) == 0)
    {
        #ifndef NDEBUG
        reportDbg1("RF Problem: %s RF ON!!!\n", (rf_status & PHJAL_EXTRFON) != 0 ? "EXTERNAL" : "NO");
        #endif

        return PH_ERR_BFL_RF_ERROR;
    } else
    {
        bfl_status = SetTimeOut(bfl_frame->RcRegCtl, &bfl_frame->RcOpCtl, 
            FIELD_ACTIVATION_DELAY, USED_PRESCALER_VALUE);
        if (bfl_status != PH_ERR_BFL_SUCCESS)
        {
            return bfl_status;
        }
    
        bfl_frame->RcRegCtlSetRegParam.address  = PHCS_BFL_JREG_COMMAND;
        bfl_frame->RcRegCtlSetRegParam.reg_data = PHCS_BFL_JCMD_NOCMDCHANGE;
        bfl_status = bfl_frame->RcRegCtl->SetRegister(&bfl_frame->RcRegCtlSetRegParam);
        return bfl_status;      
    }

    return bfl_status;
}

phcsBfl_Status_t phJal_SetBitrate(phHal_sHwReference_t *psHwReference,                                  
                                  NfcSpeeds             Speed,
                                  NfcModes              Mode)
{
    phcsBfl_Status_t            bfl_status = PH_ERR_BFL_SUCCESS;
    phJal_sBFLUpperFrame_t      *bfl_frame = NULL;

    if (psHwReference == NULL)
    {
        return PH_ERR_BFL_INVALID_PARAMETER;
    }

    phJal_Trace(psHwReference, "phJal_SetBitrate");

    bfl_frame = (phJal_sBFLUpperFrame_t*)psHwReference->pBoardDriver;

    if (bfl_frame == NULL)
    {
        return PH_ERR_BFL_INVALID_PARAMETER;
    }

    bfl_frame->RcOpCtlAttribParam.group_ordinal = PHCS_BFLOPCTL_GROUP_MODE;
    
    if (Mode == ACTIVE_MODE)
    {     
        bfl_frame->RcOpCtlAttribParam.attrtype_ordinal = PHCS_BFLOPCTL_ATTR_ACTIVE;
    }

    switch(Speed)
    {
        default: /* Default is 106: */
        case RF106K:
            bfl_frame->RcOpCtlAttribParam.param_a = PHCS_BFLOPCTL_VAL_RF106K;
            bfl_frame->RcOpCtlAttribParam.param_b = PHCS_BFLOPCTL_VAL_RF106K;
            if (Mode == PASSIVE_MODE)
            {
                bfl_frame->RcOpCtlAttribParam.attrtype_ordinal = PHCS_BFLOPCTL_ATTR_MFRD_A;
            }
            break;
        case RF212K:
            bfl_frame->RcOpCtlAttribParam.param_a = PHCS_BFLOPCTL_VAL_RF212K;
            bfl_frame->RcOpCtlAttribParam.param_b = PHCS_BFLOPCTL_VAL_RF212K;
            if (Mode == PASSIVE_MODE)
            {
                bfl_frame->RcOpCtlAttribParam.attrtype_ordinal = PHCS_BFLOPCTL_ATTR_PAS_POL;
            }
            break;
        case RF424K:
            bfl_frame->RcOpCtlAttribParam.param_a = PHCS_BFLOPCTL_VAL_RF424K;
            bfl_frame->RcOpCtlAttribParam.param_b = PHCS_BFLOPCTL_VAL_RF424K;
            if (Mode == PASSIVE_MODE)
            {
                bfl_frame->RcOpCtlAttribParam.attrtype_ordinal = PHCS_BFLOPCTL_ATTR_PAS_POL;
            }
            break;
    }
    bfl_status = bfl_frame->RcOpCtl.SetAttrib(&bfl_frame->RcOpCtlAttribParam);
    return bfl_status;
}

NFCSTATUS phJal_SetBitrateForOpMode(phHal_sHwReference_t    *psHwReference,
                                    phHal_eOpModes_t        OpMode)
{
    NFCSTATUS               command_status = NFCSTATUS_SUCCESS;
    phcsBfl_Status_t        bfl_status = PH_ERR_BFL_SUCCESS;
    phJal_sBFLUpperFrame_t  *bfl_frame = NULL; 

    if (psHwReference == NULL)
    {
        return (PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER));
    }

    phJal_Trace(psHwReference, "phJal_SetBitrateForOpMode");

    bfl_frame = (phJal_sBFLUpperFrame_t*)psHwReference->pBoardDriver;

    if (bfl_frame == NULL)
    {
        return (PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER));
    }

    bfl_frame->RcOpCtlAttribParam.group_ordinal = PHCS_BFLOPCTL_GROUP_MODE;

    switch(OpMode)
    {
        default: /* Default is MF: */
        case phHal_eOpModesMifare:
        case phHal_eOpModesISO14443_4A:
        case phHal_eOpModesNfcPassive106:
            bfl_frame->RcOpCtlAttribParam.param_a = PHCS_BFLOPCTL_VAL_RF106K;
            bfl_frame->RcOpCtlAttribParam.param_b = PHCS_BFLOPCTL_VAL_RF106K;
            bfl_frame->RcOpCtlAttribParam.attrtype_ordinal = PHCS_BFLOPCTL_ATTR_MFRD_A;
            break;
        case phHal_eOpModesNfcPassive212:
        case phHal_eOpModesFelica212:
            bfl_frame->RcOpCtlAttribParam.param_a = PHCS_BFLOPCTL_VAL_RF212K;
            bfl_frame->RcOpCtlAttribParam.param_b = PHCS_BFLOPCTL_VAL_RF212K;
            bfl_frame->RcOpCtlAttribParam.attrtype_ordinal = PHCS_BFLOPCTL_ATTR_PAS_POL;
            break;
        case phHal_eOpModesFelica424:
        case phHal_eOpModesNfcPassive424:
            bfl_frame->RcOpCtlAttribParam.param_a = PHCS_BFLOPCTL_VAL_RF424K;
            bfl_frame->RcOpCtlAttribParam.param_b = PHCS_BFLOPCTL_VAL_RF424K;
            bfl_frame->RcOpCtlAttribParam.attrtype_ordinal = PHCS_BFLOPCTL_ATTR_PAS_POL;
            break;
        case phHal_eOpModesNfcActive106:
            bfl_frame->RcOpCtlAttribParam.param_a = PHCS_BFLOPCTL_VAL_RF106K;
            bfl_frame->RcOpCtlAttribParam.param_b = PHCS_BFLOPCTL_VAL_RF106K;
            bfl_frame->RcOpCtlAttribParam.attrtype_ordinal = PHCS_BFLOPCTL_ATTR_ACTIVE;
            break;
        case phHal_eOpModesNfcActive212:
            bfl_frame->RcOpCtlAttribParam.param_a = PHCS_BFLOPCTL_VAL_RF212K;
            bfl_frame->RcOpCtlAttribParam.param_b = PHCS_BFLOPCTL_VAL_RF212K;
            bfl_frame->RcOpCtlAttribParam.attrtype_ordinal = PHCS_BFLOPCTL_ATTR_ACTIVE;
            break;
        case phHal_eOpModesNfcActive424:
            bfl_frame->RcOpCtlAttribParam.param_a = PHCS_BFLOPCTL_VAL_RF424K;
            bfl_frame->RcOpCtlAttribParam.param_b = PHCS_BFLOPCTL_VAL_RF424K;
            bfl_frame->RcOpCtlAttribParam.attrtype_ordinal = PHCS_BFLOPCTL_ATTR_ACTIVE;
            break;
    }

    bfl_status = bfl_frame->RcOpCtl.SetAttrib(&bfl_frame->RcOpCtlAttribParam);

    if (bfl_status == PH_ERR_BFL_SUCCESS)
    {
        command_status = NFCSTATUS_SUCCESS;
    }
    else
    {
        command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_BOARD_COMMUNICATION_ERROR);
    }

    return command_status;
}

phcsBfl_Status_t SetTimeOut(phcsBflRegCtl_t *rc_reg_ctl,
                            phcsBflOpCtl_t  *rc_op_ctl, 
                            int32_t         aMicroSeconds, 
                            uint8_t         aFlags)
{
    phcsBfl_Status_t            status;
    phcsBflOpCtl_AttribParam_t  opp;
    phcsBflRegCtl_ModRegParam_t	mrp;
    int                         regValues;

    phJal_Trace(NULL, "SetTimeOut");

    opp.self = rc_op_ctl;
    mrp.self = rc_reg_ctl;

    if (aFlags == PN511_STEPS_10US)
    {
        regValues = aMicroSeconds / 10;
    }
    else if ( (aFlags == PN511_STEPS_100US)  || (aFlags == TIMER_DEFAULT_SETTINGS) )
    {
        aFlags = PN511_STEPS_100US;
        regValues = aMicroSeconds / 100;
    }
    else if (aFlags == PN511_STEPS_500US)
    {
        regValues = aMicroSeconds / 500;
    }
    else
    {
        return PH_ERR_BFL_INVALID_PARAMETER;
    }

     /* Check if value is within the supported range 
      * NOTE: The supported hardware range is bigger, since the prescaler here is always set
      *       to 100us.
      */
    if (regValues >= 0xFFFF)
    {
        return PH_ERR_BFL_INVALID_PARAMETER;
    }

    /* 
     * Activate / deactivate timer
     */
    opp.group_ordinal    = PHCS_BFLOPCTL_GROUP_TMR;
    opp.attrtype_ordinal = PHCS_BFLOPCTL_ATTR_TMR_AUTO;
    if (aMicroSeconds == PN511_TIMER_OFF)
    {
        opp.param_a = PHCS_BFLOPCTL_VAL_OFF;
    }
    else
    {
        opp.param_a = PHCS_BFLOPCTL_VAL_ON;
    }
    status = rc_op_ctl->SetAttrib(&opp);
    if(status != PH_ERR_BFL_SUCCESS)
    {
        return status;
    }

    if (aMicroSeconds == PN511_TIMER_OFF)
    {
        /* We would just like to switch off the timer --> we are done */
        return status;
    }

    /* 
     * Set prescaler steps. A granularity of 100us is defined.
     */
    opp.group_ordinal    = PHCS_BFLOPCTL_GROUP_TMR;         
    opp.attrtype_ordinal = PHCS_BFLOPCTL_ATTR_TMR_PRESCALER;    
    if (aMicroSeconds == PN511_TIMER_MAX)
    {
        opp.param_a = 0xFF;
        opp.param_b = 0x0F;
    } 
    else
    {
        if (aFlags == PN511_STEPS_10US)
        {
            opp.param_a = 0x44;
            opp.param_b = 0x00;
        } else if (aFlags == PN511_STEPS_100US)
        {
            /* Calculation of the values: 100us = 6.78MHz/(677+1) = 6.78MHz/(0x2A5+1) */
            opp.param_a          = 0xA5;                        /* Low value  */
            opp.param_b          = 0x02;                        /* High value */
        } else if (aFlags == PN511_STEPS_500US)
        {
            opp.param_a = 0x3D;
            opp.param_b = 0x0D;
        } else
        {
            return PH_ERR_BFL_OTHER_ERROR;
        }
    }
    status = rc_op_ctl->SetAttrib(&opp);
   
    if(status != PH_ERR_BFL_SUCCESS)
    {
    return status;
    }

    /* 
     * Set the reload value 
     */
    if ( (aMicroSeconds == PN511_TIMER_MAX) || (aFlags == PN511_TIMER_MAX) )
    {
        regValues = 0xFFFF;
    }

    opp.group_ordinal    = PHCS_BFLOPCTL_GROUP_TMR;         /* set operation control group parameter to timer */
    opp.attrtype_ordinal = PHCS_BFLOPCTL_ATTR_TMR_RELOAD;   /* set to 50 for 5 ms */
    opp.param_a          = (unsigned char) ( regValues & 0xFF );        /* Low value */
    opp.param_b          = (unsigned char) ( (regValues >> 8) & 0xFF ); /* High value (max 0xFF) */
    status = rc_op_ctl->SetAttrib(&opp);

    mrp.set = 0;
    mrp.address = PHCS_BFL_JREG_CONTROL;
    mrp.mask = PHCS_BFL_JBIT_TSTARTNOW;
    status = rc_reg_ctl->ModifyRegister(&mrp);

    return status;
}

phcsBfl_Status_t StartTimer(phHal_sHwReference_t    *psHwReference)
{
    phcsBfl_Status_t bfl_status = PH_ERR_BFL_SUCCESS;
    phJal_sBFLUpperFrame_t  *bfl_frame = NULL;
    
    if (psHwReference != NULL)
    {
        phJal_Trace(psHwReference, "StartTimer");

        bfl_frame = (phJal_sBFLUpperFrame_t*)psHwReference->pBoardDriver;
        
        bfl_frame->RcRegCtlModifyParam.address = PHCS_BFL_JREG_CONTROL;
        bfl_frame->RcRegCtlModifyParam.set = 1;
        bfl_frame->RcRegCtlModifyParam.mask = PHCS_BFL_JBIT_TSTARTNOW;
        bfl_status = bfl_frame->RcRegCtl->ModifyRegister(&bfl_frame->RcRegCtlModifyParam);
    }
    else
    {
        bfl_status = PH_ERR_BFL_INVALID_PARAMETER;
    }
    return bfl_status;
}


NFCSTATUS phJal_PollForDevices(phHal_sHwReference_t              *psHwReference,
                                phHal_sRemoteDevInformation_t     psRemoteDevInfoList[],
                                phHal_sDevInputParam_t            *psDevInputParam,
                                phHal_eOpModes_t                  OpModes[],
                                uint8_t                           *pNbrOfRemoteDev,
                                uint8_t                           PollForAdditionalDevs)
{
    NFCSTATUS               activation_result = NFCSTATUS_SUCCESS;
    phcsBfl_Status_t        configure_status = PH_ERR_BFL_SUCCESS;
    phJal_sBFLUpperFrame_t  *bfl_frame = NULL;

    const phHal_eOpModes_t  *meta_mode_to_poll = NULL;
    uint8_t                 op_mode_counter = 0;
    uint8_t                 remaining_or_found_num_rdevs;
    uint8_t                 num_rdevs_found = 0;


    if ((pNbrOfRemoteDev != NULL) || 
        (psHwReference != NULL) ||
        (psDevInputParam != NULL) ||
        (psRemoteDevInfoList != NULL) ||
        (OpModes != NULL))
    {
        phJal_Trace(psHwReference, "phJal_PollForDevices");

        bfl_frame = (phJal_sBFLUpperFrame_t*)psHwReference->pBoardDriver;
        if (bfl_frame != NULL)
        {
            if ((PollForAdditionalDevs == 0x00) || (bfl_frame->InitialPollExecuted == 0))
            {
                // phHalNfc_Ioctl(psHwReference, 0x2001, NULL, 0, NULL, NULL); // MUST NOT DO HERE -- JUST TEST
                bfl_frame->CurrentMetaMode = NULL;
                configure_status = phJal_SwitchOffRf(psHwReference);
                phOsalNfc_Suspend(5); /* RF OFF for at least 5ms. */
                bfl_frame->CIDlessModeActivated = 0;
            }

            do
            {
                remaining_or_found_num_rdevs = *pNbrOfRemoteDev - num_rdevs_found;

                meta_mode_to_poll = phJal_GetMetaMode(OpModes[op_mode_counter]);

                configure_status = phJal_ConfigureHardwareForPolling(psHwReference);
                if (configure_status != PH_ERR_BFL_SUCCESS)
                {
                    if (configure_status == PH_ERR_BFL_RF_ERROR)
                    {
                        activation_result = 
                            PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_EXTERNAL_RF_DETECTED);
                    } else
                    {
                        activation_result = 
                            PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_BOARD_COMMUNICATION_ERROR);
                    }
                }
                else
                {
                    if (meta_mode_to_poll == PHJAL_P106)
                    {
                        reportDbg0("poll func:\t Polling with Passive 106k Meta Mode\n");
                        activation_result = phJal_P106ModeActivation(psHwReference,
                                                    &psRemoteDevInfoList[num_rdevs_found],
                                                    psDevInputParam,
                                                    OpModes,
                                                    &remaining_or_found_num_rdevs,
                                                    PollForAdditionalDevs);
                       if ((activation_result == NFCSTATUS_SUCCESS) ||
                           (activation_result == PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_MORE_INFORMATION)))
                       {
                            num_rdevs_found += remaining_or_found_num_rdevs;
                       }
                    }
                    else if (meta_mode_to_poll == PHJAL_P212_424)
                    {
                        reportDbg0("poll func:\t Polling with Passive 212k/424k Meta Mode\n");
                        activation_result = phJal_P212424ModeActivation(psHwReference,
                                                                        &psRemoteDevInfoList[num_rdevs_found],
                                                                        psDevInputParam,
                                                                        OpModes,
                                                                        &remaining_or_found_num_rdevs);
                       if ((activation_result == NFCSTATUS_SUCCESS) ||
                           (activation_result == PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_MORE_INFORMATION)))
                       {
                            num_rdevs_found += remaining_or_found_num_rdevs;
                       }
                    }
                    else if (meta_mode_to_poll == PHJAL_A)
                    {
                        reportDbg0("poll func:\t Polling with Active Meta Mode\n");
                        activation_result = phJal_NfcActiveModeActivation(psHwReference,
                                                                        &psRemoteDevInfoList[num_rdevs_found],
                                                                        psDevInputParam,
                                                                        OpModes,
                                                                        &remaining_or_found_num_rdevs);
                        
                       if ((activation_result == NFCSTATUS_SUCCESS) ||
                           (activation_result == PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_MORE_INFORMATION)))
                       {
                            num_rdevs_found += remaining_or_found_num_rdevs;
                       }
                    } else if (meta_mode_to_poll == PHJAL_P106B)
                    {
                        reportDbg0("poll func:\t Polling with Passive 106k Meta Mode\n");
                        activation_result = phJal_P106BModeActivation(psHwReference,
                                                                      &psRemoteDevInfoList[num_rdevs_found],
                                                                      psDevInputParam,
                                                                      OpModes,
                                                                      &remaining_or_found_num_rdevs);
                       if ((activation_result == NFCSTATUS_SUCCESS) ||
                           (activation_result == PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_MORE_INFORMATION)))
                       {
                            num_rdevs_found += remaining_or_found_num_rdevs;
                       }
                    }
                    else
                    {
                        activation_result = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER);
                    }
                }
                if ((activation_result == NFCSTATUS_SUCCESS) ||
                    (activation_result == PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_MORE_INFORMATION)))
                {
                    if (bfl_frame->InitialPollExecuted == 0)
                    {
                        bfl_frame->InitialPollExecuted = 1;
                    }
                }
                else
                {
                    bfl_frame->InitialPollExecuted = 0;
                }
                
                /* At the first iteration the result of this could be 0, which would
                 * a subfunction le return an error. However, from loop counter 1 on
                 * this condition would lead also to an error being returned thus we
                 * have to conditionally prevent this. */
                if ((op_mode_counter > 0) && (*pNbrOfRemoteDev <= num_rdevs_found))
                {
                    /* Iteration with op_mode_counter == 1..n has come into a condition where
                     * no more RDev indices in the list are free. To prevent the subfunctions
                     * to throw an error, we need to exit the loop:
                     */
                    break;
                }
                op_mode_counter++;
            } while ((OpModes[op_mode_counter] != phHal_eOpModesArrayTerminator) &&
                     ((activation_result != NFCSTATUS_SUCCESS) &&
                      (activation_result != PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_MORE_INFORMATION))));

            *pNbrOfRemoteDev = num_rdevs_found;
        }
        else
        {
            activation_result = (PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER));
        }
    }
    else
    {
        activation_result = (PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER));
    }
    
    return activation_result;
}

NFCSTATUS phJal_P106BModeActivation(phHal_sHwReference_t                     *psHwReference,
                                    phHal_sRemoteDevInformation_t            psRemoteDevInfoList[],
                                    phHal_sDevInputParam_t                   *psDevInputParam,
                                    phHal_eOpModes_t                         OpModes[],
                                    uint8_t                                  *pNbrOfRemoteDev)
{
    // TODO: Add code (BFL call to Type B activation):
    *pNbrOfRemoteDev = 0;
    return PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_COMMAND_NOT_SUPPORTED);
}


NFCSTATUS phJal_P106ModeActivation(phHal_sHwReference_t                     *psHwReference,
                                   phHal_sRemoteDevInformation_t            psRemoteDevInfoList[],
                                   phHal_sDevInputParam_t                   *psDevInputParam,
                                   phHal_eOpModes_t                         OpModes[],
                                   uint8_t                                  *pNbrOfRemoteDev,
                                   uint8_t                                  PollForAdditionalDevs)
{
    phJal_sBFLUpperFrame_t *bfl_frame = 
        (phJal_sBFLUpperFrame_t*)psHwReference->pBoardDriver;
    phcsBfl_Status_t                        command_status;
    phcsBflI3P3A_ReqAParam_t                request_param;
    phcsBflI3P3A_AnticollSelectParam_t      a_select_param = {0};
    phcsBflI3P3A_HaltAParam_t               halt_param;
    NFCSTATUS                               poll_status = NFCSTATUS_SUCCESS;
    uint8_t                                 remote_devs_found = 0;
    uint8_t                                 process_cache = 0;
    uint8_t                                 op_mode_index = 0;
    uint8_t                                 uid_buffer[PHCS_BFLNFCCOMMON_ID_LEN];
    uint8_t                                 more_information_available = 0;
    uint8_t                                 sak_comparator = 0;
    uint8_t                                 valid_device_found = 0;
    uint8_t                                 current_cache_pos = 0;
    phHal_eOpModes_t                        current_op_mode = phHal_eOpModesArrayTerminator;
    phJal_sUIDCacheEntry_t                  *current_cache_entry = NULL;


    phJal_Trace(psHwReference, "phJal_P106ModeActivation");

    if ((bfl_frame == NULL) || (psDevInputParam == NULL))
    {
        return (PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER));
    }
    
    request_param.self = &bfl_frame->Iso14443_3A;
    a_select_param.self = &bfl_frame->Iso14443_3A;
    halt_param.self = &bfl_frame->Iso14443_3A;

    command_status = SetTimeOut(bfl_frame->RcRegCtl, &bfl_frame->RcOpCtl, 
        ISO_14443_RF_COMMUNICATION_DELAY, USED_PRESCALER_VALUE);

    reportDbg1("poll func:\t prescaler setting: 0x%02X\n", USED_PRESCALER_VALUE);
    if (command_status != PH_ERR_BFL_SUCCESS)
    {
        return(PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_BOARD_COMMUNICATION_ERROR));
    }

    if (*pNbrOfRemoteDev == 0)
    {
         poll_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER);
         return poll_status;
    }

    command_status = phJal_SetBitrate(psHwReference, RF106K, PASSIVE_MODE);

    if (command_status != PH_ERR_BFL_SUCCESS)
    {
        return(PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_BOARD_COMMUNICATION_ERROR));
    }

    bfl_frame->TRxBuffer[0] = 0;              
    bfl_frame->TRxBuffer[1] = 0;
    request_param.atq       = bfl_frame->TRxBuffer;
    a_select_param.flags    = 0;
    current_cache_pos       = 0;

    do
    {
        if (PollForAdditionalDevs == 0x01)
        {
            if (bfl_frame->UIDCache.NumUIDs > 0)
            {            
                current_cache_entry = 
                    phJal_GetNextCacheEntry(&bfl_frame->UIDCache, &current_cache_pos);
                if (current_cache_entry == NULL)
                {
                    process_cache = 0;
                }
                else
                {
                    process_cache = 1;
                    request_param.req_code = PHCS_BFLI3P3A_REQALL;
                    a_select_param.uid = current_cache_entry->UID;
                    a_select_param.uid_length = current_cache_entry->UIDLength*8;
                }
            }
            else
            {
                process_cache = 0;
            }
        }
        else
        {
            process_cache = 0;
        }

        if (process_cache == 0)
        {
            request_param.req_code = PHCS_BFLI3P3A_REQIDL;
            a_select_param.uid = uid_buffer;
            a_select_param.uid_length  = 0;
            memset(uid_buffer, 0, PHCS_BFLNFCCOMMON_ID_LEN);                        
        }

        reportDbg0("poll func:\t starting poll cycle\n");
        /* Workaround to reset a pending interrupt */
        bfl_frame->RcRegCtlModifyParam.address = PHCS_BFL_JREG_CONTROL;
        bfl_frame->RcRegCtlModifyParam.set = 1;
        bfl_frame->RcRegCtlModifyParam.mask = PHCS_BFL_JBIT_TSTOPNOW;
        command_status = bfl_frame->RcRegCtl->ModifyRegister(&bfl_frame->RcRegCtlModifyParam);
    
        bfl_frame->RcRegCtlSetRegParam.address  = PHCS_BFL_JREG_COMMIRQ;
        bfl_frame->RcRegCtlSetRegParam.reg_data = 0x01;
        command_status = bfl_frame->RcRegCtl->SetRegister(&bfl_frame->RcRegCtlSetRegParam);
        /*  End of Workaround */

        command_status = bfl_frame->Iso14443_3A.RequestA(&request_param);

        if ((command_status == PH_ERR_BFL_SUCCESS) || 
            ((command_status & ~PH_ERR_BFL_COMP_MASK) == PH_ERR_BFL_COLLISION_ERROR))
        {
            a_select_param.flags = 0;
        }
        else
        {
            reportDbg0("poll func:\t RequestA returned with timeout\n");
            *pNbrOfRemoteDev = 0;
            poll_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_RF_TIMEOUT);
            break;
        } 
        command_status = bfl_frame->Iso14443_3A.AnticollSelect(&a_select_param);
        reportDbg1("poll func:\t AnticollisionSelect returned with status 0x%02X\n", command_status);
        
        if (command_status == PH_ERR_BFL_SUCCESS)
        {
            op_mode_index = 0;
            valid_device_found = 0;

            while ((OpModes[op_mode_index] != phHal_eOpModesArrayTerminator) && 
                    (poll_status == NFCSTATUS_SUCCESS) && 
                    (valid_device_found == 0))
            {
                reportDbg1("poll func:\t SAK Byte: 0x%02X\n", a_select_param.sak);
                current_op_mode = OpModes[op_mode_index++];
                if ( phJal_GetSakComparatorForOpMode(current_op_mode, &sak_comparator) != NULL)
                {            
                    if (a_select_param.sak & sak_comparator)
                    {                        
                        if ((phJal_IsOpModeCompatibleWithCurrentMetaMode(psHwReference, current_op_mode) ==
                            OP_MODE_COMPATIBLE) &&
                            (remote_devs_found < *pNbrOfRemoteDev))
                        {
                            valid_device_found = 1;                               
                        }
                        else
                        {
                            valid_device_found = 0;
                        }
                        
                    }
                    
                    else 
                    {
                        // check if it'a a mifare UltraLight
                        if ((phJal_IsOpModeCompatibleWithCurrentMetaMode(psHwReference, phHal_eOpModesMifare) ==
                             OP_MODE_COMPATIBLE) && 
                             (a_select_param.sak == 0x00) &&
                             (current_op_mode == phHal_eOpModesMifare) &&
                             (remote_devs_found < *pNbrOfRemoteDev))
                        {
                            valid_device_found = 1; 
                            current_op_mode = phHal_eOpModesMifare;
                        }
                        else
                        {
                            valid_device_found = 0;
                        }
                    }
                    
                    if (valid_device_found == 1)
                    {
                        phJal_Setup106kMetaModeDevice(psHwReference, 
                                                    &psRemoteDevInfoList[remote_devs_found],
                                                    psDevInputParam,
                                                    current_op_mode,
                                                    &request_param,
                                                    &a_select_param);
                    
                        if (poll_status == NFCSTATUS_SUCCESS)
                        {
                            ++remote_devs_found;         
                        }
                    }
                    else
                    {
                        reportDbg0("poll func:\t adding UID Cache entry\n");
                        phJal_AddUIDCacheEntry(&(bfl_frame->UIDCache),
                            a_select_param.uid, a_select_param.uid_length/8);
                    }
                }
            }
            
            halt_param.buffer = bfl_frame->TRxBuffer;
            halt_param.buffer_length = bfl_frame->TRxBufferSize;
            command_status = SetTimeOut(bfl_frame->RcRegCtl, &bfl_frame->RcOpCtl, 
            ISO_14443_RF_COMMUNICATION_DELAY, USED_PRESCALER_VALUE);

            poll_status = bfl_frame->Iso14443_3A.HaltA(&halt_param);
            poll_status = NFCSTATUS_SUCCESS;
        }
        else
        {
            poll_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_RF_TIMEOUT);
        }

        if ((remote_devs_found == *pNbrOfRemoteDev) && 
            ((a_select_param.flags & PHCS_BFLI3P3A_COLL_DETECTED) == 
            PHCS_BFLI3P3A_COLL_DETECTED))
        {
            more_information_available = 1;
        }

        if ((remote_devs_found == *pNbrOfRemoteDev) || 
            (bfl_frame->UIDCache.NumUIDs == PHJAL_MAX_UID_CACHE_ENTRIES))
        {
            break;
        }

    }while (((a_select_param.flags & PHCS_BFLI3P3A_COLL_DETECTED) == 
            PHCS_BFLI3P3A_COLL_DETECTED) ||
            (process_cache == 1));      
    
    if ((poll_status == NFCSTATUS_SUCCESS) || (remote_devs_found > 0))
    {
        if (remote_devs_found > 0)
        {   
            if (more_information_available == 1) 
            {
                poll_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_MORE_INFORMATION);
            }
            else
            {
                poll_status = NFCSTATUS_SUCCESS;
                *pNbrOfRemoteDev = remote_devs_found;
            }
        }
        else
        {
            poll_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_NO_DEVICE_FOUND);
            *pNbrOfRemoteDev = 0;
        }
    }
    return poll_status;
}

NFCSTATUS phJal_Setup106kMetaModeDevice(phHal_sHwReference_t                *psHwReference,
                                        phHal_sRemoteDevInformation_t       *psRemoteDevInfo,
                                        phHal_sDevInputParam_t              *psDevInputParam,
                                        phHal_eOpModes_t                    OpMode,
                                        phcsBflI3P3A_ReqAParam_t            *psRequestParam,
                                        phcsBflI3P3A_AnticollSelectParam_t  *psSelectParam)
{
    NFCSTATUS   config_status = NFCSTATUS_SUCCESS;
    uint8_t     remote_dev_found = 0;
    phJal_sBFLUpperFrame_t *bfl_frame = 
        (phJal_sBFLUpperFrame_t*)psHwReference->pBoardDriver;

    phJal_Trace(psHwReference, "phJal_Setup106kMetaModeDevice");
    
    if (OpMode == phHal_eOpModesNfcPassive106)
    {
        config_status = phJal_ConfigureHardwareForNfc106kProtocol(psHwReference);

        if (config_status == NFCSTATUS_SUCCESS)
        {
            if (psDevInputParam->NFCIDAuto == 0x01)
            {
                phJal_GenerateNfcId(psDevInputParam->NFCID3i);
            }
            
            reportDbg0("poll func:\t setting up NFC Passive 106k device\n");            
            config_status = 
                phJal_SetupNfcDevice(psHwReference, psRemoteDevInfo,
                    psDevInputParam, RF106K, PASSIVE_MODE, NULL);
            if (config_status == NFCSTATUS_SUCCESS)
            {
                config_status = 
                    phJal_InitialiseP106Startup(psRemoteDevInfo,
                        psRequestParam, psSelectParam, phHal_eOpModesNfcPassive106);

                if (config_status == NFCSTATUS_SUCCESS)
                {
                    bfl_frame->CurrentMetaMode = PHJAL_P106;
                }
            }
        }
    }
    else if(OpMode == phHal_eOpModesISO14443_4A) 
    {
        reportDbg0("poll func:\t ISO 14443-4A device found\n");
        if (bfl_frame->CIDlessModeActivated == 0)
        {
            config_status = phJal_SetupIso144434Device(psHwReference,
                psRemoteDevInfo, psDevInputParam, psRequestParam, psSelectParam, &remote_dev_found);
        }
    }
    else if(OpMode == phHal_eOpModesMifare) 
    {
        reportDbg0("poll func:\t Mifare device found\n");
        config_status = phJal_InitialiseMifareRemoteDevice(psRemoteDevInfo,
            psRequestParam, psSelectParam, bfl_frame->LogHandleEntries);        
    }

    if (config_status == NFCSTATUS_SUCCESS)
    {
        bfl_frame->CurrentMetaMode = PHJAL_P106;
    }

   return config_status;
}


uint8_t* phJal_GetSakComparatorForOpMode(phHal_eOpModes_t OpMode,                                        
                                         uint8_t          *pComparator)
{
    phJal_Trace(NULL, "phJal_GetSakComparatorForOpMode");
    
    if (OpMode != phHal_eOpModesArrayTerminator)
    {
        switch(OpMode)
        {
            case phHal_eOpModesNfcPassive106:
                *pComparator = SAK_NFC_COMPARATOR;
                break;
            case phHal_eOpModesISO14443_4A:
                *pComparator = SAK_TCL_COMPARATOR;
                break;
            case phHal_eOpModesMifare:
                *pComparator = SAK_MIFARE_COMPARATOR;
                break;
            default:
                pComparator = NULL;
                break;
        }
    }
    else
    {
        pComparator = NULL;
    }
    return pComparator;   
}


NFCSTATUS phJal_StartupPassive212424Mode(phHal_sHwReference_t           *psHwReference,
                                         phHal_sDevInputParam_t         *psDevInputParam,
                                         phHal_eOpModes_t               OpModes[],
                                         phcsBflPolAct_PollingParam_t    *psPollingParameter)
{
    phcsBfl_Status_t            bfl_status = PH_ERR_BFL_SUCCESS;
    NFCSTATUS                   poll_status = NFCSTATUS_SUCCESS;
    phJal_sBFLUpperFrame_t      *bfl_frame = NULL;

    if ((psHwReference == NULL) || 
        (psDevInputParam == NULL) ||
        (psPollingParameter == NULL))
    {
        return (PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER));
    }

    phJal_Trace(psHwReference, "phJal_StartupPassive212424Mode");

    bfl_frame = (phJal_sBFLUpperFrame_t*)psHwReference->pBoardDriver;

    if (bfl_frame == NULL)
    {
        return(PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER));
    }

    /* Apply Timeout: */
    bfl_status = SetTimeOut(bfl_frame->RcRegCtl, &bfl_frame->RcOpCtl, 
        FELICA_POLLNG_ANS_DLY_WITH_MGN + FELICA_POLLNG_TMESLT_DLY_WITH_MGN * NUM_FELICA_POLLNG_TMESLTS_WITH_MGN,
        USED_PRESCALER_VALUE);

    if (bfl_status != PH_ERR_BFL_SUCCESS)
    {
        return(PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_BOARD_COMMUNICATION_ERROR));
    }

    psPollingParameter->self = &bfl_frame->FeliCaPolling;
    bfl_frame->RcRegCtlSetRegParam.address  = PHCS_BFL_JREG_COMMAND;
    bfl_frame->RcRegCtlSetRegParam.reg_data = PHCS_BFL_JCMD_NOCMDCHANGE;
    bfl_status = bfl_frame->RcRegCtl->SetRegister(&bfl_frame->RcRegCtlSetRegParam);

    if (bfl_status != PH_ERR_BFL_SUCCESS)
    {
        return(PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_RF_TIMEOUT));
    }
    
    if (psDevInputParam->ControlFlags.UseCustomPayload == 0x01)
    {
        if ((OpModes[0] == phHal_eOpModesFelica212) ||
            (OpModes[0] == phHal_eOpModesFelica424))
        {
            memcpy(bfl_frame->TRxBuffer, psDevInputParam->FelicaPollPayload, 
                FELICA_PAYLOAD_SIZE);                
        }
        else
        {
            memcpy(bfl_frame->TRxBuffer, psDevInputParam->NfcPollPayload,
                FELICA_PAYLOAD_SIZE);
        }
    }
    else
    {
        bfl_frame->TRxBuffer[0] = FELICA_REQUEST_ALL_SYSTEM_CODES;
        bfl_frame->TRxBuffer[1] = FELICA_REQUEST_ALL_SYSTEM_CODES;
        bfl_frame->TRxBuffer[2] = FELICA_REQUEST_SYSTEM_CODE;
        bfl_frame->TRxBuffer[3] = NUM_FELICA_POLLING_TIMESLOTS;
    }

    psPollingParameter->tx_buffer      = bfl_frame->TRxBuffer;
    psPollingParameter->tx_buffer_size = 4;
    psPollingParameter->rx_buffer      = bfl_frame->TRxBuffer + 4;
    psPollingParameter->rx_buffer_size = PHJAL_TRXBL - 4;

    if ((phJal_IsOpModeInOpModesList(OpModes, phHal_eOpModesNfcPassive212) == 
        OP_MODE_IN_LIST) ||
        phJal_IsOpModeInOpModesList(OpModes, phHal_eOpModesFelica212) == 
        OP_MODE_IN_LIST)
    {
        bfl_status = phJal_SetBitrate(psHwReference, RF212K, PASSIVE_MODE);
    }
    else
    {
        bfl_status = phJal_SetBitrate(psHwReference, RF424K, PASSIVE_MODE);
    }
    bfl_status = bfl_frame->FeliCaPolling.Polling(psPollingParameter);

    if( bfl_status != PH_ERR_BFL_SUCCESS)
    {
        poll_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_RF_TIMEOUT);
    }
    else
    {
        poll_status = NFCSTATUS_SUCCESS;
    }
    return poll_status;
}

NFCSTATUS phJal_P212424ModeActivation(phHal_sHwReference_t           *psHwReference,
                                    phHal_sRemoteDevInformation_t    psRemoteDevInfoList[],
                                    phHal_sDevInputParam_t           *psDevInputParam,
                                    phHal_eOpModes_t                 OpModes[],
                                    uint8_t                          *pNbrOfRemoteDev)
{
    phcsBflPolAct_PollingParam_t polling_parameter = {0};
    NFCSTATUS                   poll_status = NFCSTATUS_SUCCESS;
    uint8_t                     remote_devs_found = 0;
    uint8_t                     num_devs_found_during_polling = 0;
    uint8_t                     device_index = 0;
    phJal_sBFLUpperFrame_t      *bfl_frame = NULL;
    NfcSpeeds                   current_device_speed = RF212K;
    uint8_t                     nfcid2t[PHCS_BFLNFCCOMMON_ID_LEN];
    uint8_t                     nfc_device_detected = 0x0;
    
    if ((psHwReference == NULL) || 
        (psRemoteDevInfoList == NULL) ||
        (psDevInputParam == NULL) ||
        (OpModes == NULL) ||
        (pNbrOfRemoteDev == NULL))
    {
        return (PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER));
    }

    phJal_Trace(psHwReference, "phJal_P212424ModeActivation");

    bfl_frame = (phJal_sBFLUpperFrame_t*)psHwReference->pBoardDriver;
    
    if (phJal_IsOpModeCompatibleWithCurrentMetaMode(psHwReference, phHal_eOpModesNfcPassive212) 
        != OP_MODE_COMPATIBLE)
    {
        return(PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_NO_DEVICE_FOUND));
    }

    if ((phJal_IsOpModeInOpModesList(OpModes, phHal_eOpModesNfcPassive212) == 
        OP_MODE_IN_LIST) ||
        phJal_IsOpModeInOpModesList(OpModes, phHal_eOpModesFelica212) == 
        OP_MODE_IN_LIST)
    {
        current_device_speed = RF212K;
    }
    else
    {
        current_device_speed = RF424K;
    }

    poll_status = phJal_StartupPassive212424Mode(psHwReference, psDevInputParam, 
                    OpModes, &polling_parameter);
    
    if (poll_status == NFCSTATUS_SUCCESS)
    {  
        num_devs_found_during_polling = 
            (uint8_t)(polling_parameter.rx_buffer_size / FELICA_TIMESLOT_LENGTH);
        for (device_index = 0; device_index < num_devs_found_during_polling; ++device_index)
        {
            if (*pNbrOfRemoteDev > remote_devs_found)
            {
                if (((phJal_IsOpModeInOpModesList(OpModes, phHal_eOpModesNfcPassive212) == 
                    OP_MODE_IN_LIST) || 
                    (phJal_IsOpModeInOpModesList(OpModes, phHal_eOpModesNfcPassive424) ==
                    OP_MODE_IN_LIST) ||
                    (phJal_IsOpModeInOpModesList(OpModes, phHal_eOpModesFelica212) == 
                    OP_MODE_IN_LIST) ||
                    (phJal_IsOpModeInOpModesList(OpModes, phHal_eOpModesFelica424) == 
                    OP_MODE_IN_LIST)))                   
                {
                    if ((polling_parameter.rx_buffer[(FELICA_TIMESLOT_LENGTH * device_index) + 2] == 
                           FELICA_NFC_DEVICE_ID1) && 
                           (polling_parameter.rx_buffer[(FELICA_TIMESLOT_LENGTH * device_index) + 3] ==
                           FELICA_NFC_DEVICE_ID2))
                    {
                       nfc_device_detected = 0x01;
                       reportDbg0("poll func:\t NFC Passive 212/424k device found\n");
                    }
                    else
                    {
                       reportDbg0("poll func:\t Felica Passive 212/424k device found\n");
                       nfc_device_detected = 0x00;
                    }

                    if( current_device_speed == RF424K)
                    {
                        poll_status =
                            phJal_InitialiseP212424Startup(&psRemoteDevInfoList[remote_devs_found],
                                &polling_parameter, FELICA_TIMESLOT_LENGTH * device_index, 
                                (nfc_device_detected == 0x01)? phHal_eOpModesNfcPassive424 : 
                                phHal_eOpModesFelica424);
                    }
                    else
                    {
                        poll_status =
                            phJal_InitialiseP212424Startup(&psRemoteDevInfoList[remote_devs_found],
                                &polling_parameter, FELICA_TIMESLOT_LENGTH * device_index, 
                                (nfc_device_detected == 0x01)? phHal_eOpModesNfcPassive212 : 
                                phHal_eOpModesFelica212);
                    }
                    if (poll_status == NFCSTATUS_SUCCESS)
                    {
                        if (nfc_device_detected == 0x01)
                        {
                            if ((phJal_IsOpModeInOpModesList(OpModes, phHal_eOpModesNfcPassive212) == 
                                OP_MODE_IN_LIST) || 
                                (phJal_IsOpModeInOpModesList(OpModes, phHal_eOpModesNfcPassive424) ==
                                OP_MODE_IN_LIST))
                            {                                                                
                                /* 
                                 * BugFix: The value of the NFCID2t has been correctly assigned with 10 bytes of data.
                                 * So, its enough to send this NFCID2t directly.
                                 */
                                memcpy(nfcid2t, psRemoteDevInfoList[remote_devs_found].RemoteDevInfo.
                                    NfcInfo212_424.Startup212_424.NFCID2t,FELICA_IDMM_LENGTH+2);
                            
                                poll_status = phJal_SetupNfcDevice(psHwReference, 
                                    &psRemoteDevInfoList[remote_devs_found], psDevInputParam, 
                                    current_device_speed, PASSIVE_MODE, nfcid2t);

                                if (poll_status == NFCSTATUS_SUCCESS)
                                {
                                    ++remote_devs_found;
                                }
                                else
                                {
                                    poll_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_BOARD_COMMUNICATION_ERROR);
                                }
                            }
                        }
                        else
                        {
                            ++remote_devs_found;
                        }
                    }
                }                    
            }
            else
            {
                poll_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INSUFFICIENT_RESOURCES);
                break;
            }
        }
    }
    else
    {
        poll_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_RF_TIMEOUT);
    }

    if (poll_status == NFCSTATUS_SUCCESS || remote_devs_found > 0)
    {
        if (remote_devs_found > 0)
        {
            poll_status = NFCSTATUS_SUCCESS;
            bfl_frame->CurrentMetaMode = PHJAL_P212_424;
            *pNbrOfRemoteDev = remote_devs_found;
        }
        else
        {
            poll_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_NO_DEVICE_FOUND);
            *pNbrOfRemoteDev = 0;
        }
    }
    return poll_status;
}

NFCSTATUS phJal_NfcActiveModeActivation(phHal_sHwReference_t               *psHwReference,                                  
                                        phHal_sRemoteDevInformation_t      psRemoteDevInfoList[],
                                        phHal_sDevInputParam_t             *psDevInputParam,
                                        phHal_eOpModes_t                   OpModes[],
                                        uint8_t                             *pNbrOfRemoteDev)
{
    phcsBfl_Status_t            bfl_status = PH_ERR_BFL_SUCCESS;
    NFCSTATUS                   poll_status = NFCSTATUS_SUCCESS;
    uint8_t                     remote_devs_found = 0;
    phJal_sBFLUpperFrame_t      *bfl_frame = NULL;
    NfcSpeeds                   current_device_speed = RF212K;

    uint8_t                     opmode_index = 0;
    
    if (psHwReference == NULL || 
        psRemoteDevInfoList == NULL ||
        psDevInputParam == NULL ||
        OpModes == NULL ||
        pNbrOfRemoteDev == NULL)
    {
        return (PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER));
    }

    phJal_Trace(psHwReference, "phJal_NfcActiveModeActivation");

    bfl_frame = (phJal_sBFLUpperFrame_t*)psHwReference->pBoardDriver;

    while (OpModes[opmode_index] != phHal_eOpModesArrayTerminator)  
    {
        if (phJal_IsOpModeCompatibleWithCurrentMetaMode(psHwReference, OpModes[opmode_index]) ==
            OP_MODE_COMPATIBLE)
        {
            switch(OpModes[opmode_index])
            {
                default: /* Default is 106: */
                case phHal_eOpModesNfcActive106:
                    reportDbg0("poll func:\t currently polling active 106k mode\n"); 
                    current_device_speed = RF106K;
                    break;
                case phHal_eOpModesNfcActive212:
                    reportDbg0("poll func:\t currently polling active 212k mode\n"); 
                    current_device_speed = RF212K;
                    break;
                case phHal_eOpModesNfcActive424:
                    reportDbg0("poll func:\t currently polling active 424k mode\n"); 
                    current_device_speed = RF424K;
                    break;
            }
            bfl_status = phJal_SetBitrate(psHwReference, current_device_speed, ACTIVE_MODE);

            if( bfl_status == PH_ERR_BFL_SUCCESS)
            {
                if (current_device_speed == RF106K)
                {
                     poll_status = 
                        phJal_ConfigureHardwareForNfc106kProtocol(psHwReference);
                }

                if (poll_status == NFCSTATUS_SUCCESS)
                {
                    if (psDevInputParam->NFCIDAuto == 0x01)
                    {
                        phJal_GenerateNfcId(psDevInputParam->NFCID3i);
                    }
                    poll_status = phJal_SetupNfcDevice(psHwReference, &psRemoteDevInfoList[remote_devs_found],
                        psDevInputParam, current_device_speed, ACTIVE_MODE, NULL);

                    if( poll_status == NFCSTATUS_SUCCESS)
                    {
                        ++remote_devs_found;
                        bfl_frame->CurrentMetaMode = PHJAL_A;
                    }
                }
                else
                {
                    poll_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_BOARD_COMMUNICATION_ERROR);
                    break;
                }
            }
            else
            {
                poll_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_BOARD_COMMUNICATION_ERROR);
                break;
            }
        }
        ++opmode_index;
    }

    if (poll_status == NFCSTATUS_SUCCESS || remote_devs_found > 0)
    {
        if (remote_devs_found > 0)
        {
            poll_status = NFCSTATUS_SUCCESS;
            *pNbrOfRemoteDev = remote_devs_found;
        }
        else
        {
            poll_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_NO_DEVICE_FOUND);
            phJal_SwitchOffRf(psHwReference);
            *pNbrOfRemoteDev = 0;
        }
    }
    return poll_status;
}

NFCSTATUS phJal_SetupIso144434Device(phHal_sHwReference_t               *psHwReference,
                                     phHal_sRemoteDevInformation_t      *psRemoteDevInfo,
                                     phHal_sDevInputParam_t             *psDevInputParam,
                                     phcsBflI3P3A_ReqAParam_t           *psRequestParam,
                                     phcsBflI3P3A_AnticollSelectParam_t *psSelectParam,                                     
                                     uint8_t                            *pRemoteDevFound)
{
    phcsBflI3P4AAct_RatsParam_t     rats_param = {0};
    phcsBflI3P4_t                   *iso_14443_4_protocol = NULL;
    NFCSTATUS                       command_status;
    int16_t                         cid = 0;
    phJal_sBFLUpperFrame_t          *bfl_frame = (phJal_sBFLUpperFrame_t*)psHwReference->pBoardDriver;

    phJal_Trace(psHwReference, "phJal_SetupIso144434Device");
    
    iso_14443_4_protocol = (phcsBflI3P4_t*)phJal_AquireProtocol(bfl_frame->ISO14443_4Entries);
    (*pRemoteDevFound) = 0;

    if (iso_14443_4_protocol != NULL)
    {
        if (psDevInputParam->CIDiUsed == 1)
        {
            cid = phJal_AquireHandle(bfl_frame->CidEntries);
        
            if (cid == NO_HANDLE_AVAILABLE)
            {
                command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INSUFFICIENT_RESOURCES);   
            }
            else
            {
                command_status = NFCSTATUS_SUCCESS;
            }
        }
        else
        {
            cid = 0;
            command_status = NFCSTATUS_SUCCESS;
        }

        if (command_status == NFCSTATUS_SUCCESS)
        {
            command_status = phJal_ActivateIso144434Protocol(psHwReference,
                iso_14443_4_protocol, &rats_param,(uint8_t)cid);
            
            if (command_status == NFCSTATUS_SUCCESS)
            {
                if (cid > 0)
                {
                    if (rats_param.ats[FORMAT_BYTE_POS] & 0x40)
                    {
                        if ((~(*rats_param.tc1) & CID_NOT_SUPPORTED) && (cid > 0))
                        {
                            phJal_ReleaseHandle(bfl_frame->CidEntries, (int8_t)cid);
                            bfl_frame->CIDlessModeActivated = 1;
                            cid = 0;
                        }
                    }
                    else
                    {
                        phJal_ReleaseHandle(bfl_frame->CidEntries, (int8_t)cid);
                        bfl_frame->CIDlessModeActivated = 1;
                        cid = 0;
                    }
                }
                else
                {
                    bfl_frame->CIDlessModeActivated = 1;
                }

                command_status = phJal_DeselectIso144434Device(psHwReference, iso_14443_4_protocol);
            }
                                                   
            if (command_status == NFCSTATUS_SUCCESS)
            {
                command_status =
                    phJal_InitialiseISO_144434RemoteDevice(psRemoteDevInfo,
                        psRequestParam, psSelectParam, bfl_frame->LogHandleEntries, &rats_param);

                if (command_status == NFCSTATUS_SUCCESS)
                {
                    phJal_RegisterDeviceIdForProtocol(bfl_frame->ISO14443_4Entries, iso_14443_4_protocol,
                        (uint8_t)cid);

                    phJal_RegisterLogicalHandleForProtocol(bfl_frame->ISO14443_4Entries,
                        iso_14443_4_protocol, psRemoteDevInfo->hLogHandle);
                    (*pRemoteDevFound) = 1;
                }
                else
                {
                    phJal_ReleaseHandle(bfl_frame->CidEntries, (int8_t)cid);
                    phJal_ReleaseProtocol(bfl_frame->ISO14443_4Entries, 
                        iso_14443_4_protocol);
                }
            }
            else
            {
                phJal_ReleaseHandle(bfl_frame->CidEntries, (int8_t)cid);
                phJal_ReleaseProtocol(bfl_frame->ISO14443_4Entries, 
                    iso_14443_4_protocol);
            }
        }
    }
    else
    {
        command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INSUFFICIENT_RESOURCES);
    }
    return command_status;
}

NFCSTATUS phJal_SetupNfcDevice(phHal_sHwReference_t                 *psHwReference,
                               phHal_sRemoteDevInformation_t        *psRemoteDevInfo,
                               phHal_sDevInputParam_t               *psDevInputParam,
                               NfcSpeeds                             Speed,
                               NfcModes                              Mode,
                               uint8_t                               pNfcId2t[PHCS_BFLNFCCOMMON_ID_LEN])
{
    NFCSTATUS                           command_status = NFCSTATUS_SUCCESS;
    phcsBfl_Status_t                    bfl_status = PH_ERR_BFL_SUCCESS;
    int16_t                             did = NO_HANDLE_AVAILABLE;
    phcsBflNfc_InitiatorAtrReqParam_t   attribut_request_param = {0};
    uint8_t                             send_attention_command = 1;

    phJal_sBFLUpperFrame_t  *bfl_frame = 
        (phJal_sBFLUpperFrame_t*)psHwReference->pBoardDriver;
    phJal_NfcIProtocol_t    *nfc_protocol = (phJal_NfcIProtocol_t*)
        phJal_AquireProtocol(bfl_frame->NfcInitiator_Entries);  

    phJal_Trace(psHwReference, "phJal_SetupNfcDevice");
    
    if (nfc_protocol != NULL)
    {
        if (psDevInputParam->DIDiUsed == 0x01)
        {
            did = phJal_AquireHandle(bfl_frame->DidEntries);
            if (did == NO_HANDLE_AVAILABLE)
            {
                command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INSUFFICIENT_RESOURCES);
            }
        }
        else
        {
            did = 0;
        }
        
        if (command_status == NFCSTATUS_SUCCESS)
        {
            switch(Mode)
            {
                case PASSIVE_MODE:
                    bfl_status = SetTimeOut(bfl_frame->RcRegCtl, &bfl_frame->RcOpCtl, 
                        NFC_PASSIVE_ACTIVATION_DELAY, USED_PRESCALER_VALUE);
                    send_attention_command = 0;
                    break;
                case ACTIVE_MODE:
                    bfl_status = SetTimeOut(bfl_frame->RcRegCtl, &bfl_frame->RcOpCtl, 
                        NFC_ACTIVE_ACTIVATION_DELAY, USED_PRESCALER_VALUE); 
                    send_attention_command = 1;
                    break;
                default:
                    bfl_status = PH_ERR_BFL_INVALID_PARAMETER;
                    break;
            }

            if (bfl_status == PH_ERR_BFL_SUCCESS)
            {
                command_status = 
                    phJal_ActivateNfcProtocol(psHwReference, psDevInputParam, nfc_protocol->Protocol,
                        &attribut_request_param, (uint8_t)did, pNfcId2t, send_attention_command);
    
                if (command_status == NFCSTATUS_SUCCESS)
                {
                    command_status = phJal_DeselectNfcDevice(psHwReference, nfc_protocol->Protocol);

                    if (command_status == NFCSTATUS_SUCCESS)
                    {
                        /* store TO value received by target */
                        nfc_protocol->ResponseWaitingTime = 
                            (uint32_t)(WAITING_TIME_UNIT * (0x3E8 << attribut_request_param.to_t));
                        reportDbg1("poll func:\t RWT for Target: %dus\n", nfc_protocol->ResponseWaitingTime);

                        command_status = 
                            phJal_InitialiseNfcProtocol(psHwReference, psRemoteDevInfo, 
                                &attribut_request_param, Speed, Mode);

                        if (command_status == NFCSTATUS_SUCCESS)
                        {
                            phJal_RegisterLogicalHandleForProtocol(bfl_frame->NfcInitiator_Entries,
                                nfc_protocol, psRemoteDevInfo->hLogHandle);

                            phJal_RegisterDeviceIdForProtocol(bfl_frame->NfcInitiator_Entries,
                                nfc_protocol, (uint8_t) did);

                        }
                        else
                        {
                            phJal_ReleaseHandle(bfl_frame->DidEntries, (uint8_t)did);
                            phJal_ReleaseProtocol(bfl_frame->NfcInitiator_Entries, nfc_protocol);
                        }
                    }
                    else
                    {
                        phJal_ReleaseHandle(bfl_frame->DidEntries, (uint8_t)did);
                        phJal_ReleaseProtocol(bfl_frame->NfcInitiator_Entries, nfc_protocol);
                    }
                }
                else
                {
                    phJal_ReleaseHandle(bfl_frame->DidEntries, (uint8_t)did);
                    phJal_ReleaseProtocol(bfl_frame->NfcInitiator_Entries, nfc_protocol);
                }
            }
            else
            { 
                phJal_ReleaseHandle(bfl_frame->DidEntries, (uint8_t)did);
                phJal_ReleaseProtocol(bfl_frame->NfcInitiator_Entries, nfc_protocol);
                command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_BOARD_COMMUNICATION_ERROR);
            }
        }
        else
        {
            phJal_ReleaseProtocol(bfl_frame->NfcInitiator_Entries, nfc_protocol);
        }
    }
    else
    {
        command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INSUFFICIENT_RESOURCES);
    }
    return command_status;
}

NFCSTATUS phJal_InitialiseMifareRemoteDevice(phHal_sRemoteDevInformation_t         *psRemoteDevInfo,
                                            phcsBflI3P3A_ReqAParam_t             *psRequestParam,
                                            phcsBflI3P3A_AnticollSelectParam_t   *psSelectParam,
                                            phJal_HandleEntry_t                    HandleEntries[])
{
    int16_t handle;
    
    if (psRemoteDevInfo == NULL ||
        psRequestParam == NULL ||
        psSelectParam == NULL ||
        HandleEntries == NULL)
    {
        return PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER);
    }

    phJal_Trace(NULL, "phJal_InitialiseMifareRemoteDevice");

    handle = phJal_AquireHandle(HandleEntries);

    if (handle != NO_HANDLE_AVAILABLE)
    {
        psRemoteDevInfo->hLogHandle = (uint8_t)handle;
        psRemoteDevInfo->OpMode = phHal_eOpModesMifare;
        psRemoteDevInfo->RemoteDevInfo.CardInfo106.CurrentState = phHal_eStateListPolledState;

        return phJal_InitialiseP106Startup(psRemoteDevInfo, psRequestParam, 
            psSelectParam, phHal_eOpModesMifare);
    }
    else
    {
        return PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INSUFFICIENT_RESOURCES);
    }
}

NFCSTATUS phJal_InitialiseISO_144434RemoteDevice(phHal_sRemoteDevInformation_t      *psRemoteDevInfo,
                                                phcsBflI3P3A_ReqAParam_t            *psRequestParam,
                                                phcsBflI3P3A_AnticollSelectParam_t  *psSelectParam,
                                                phJal_HandleEntry_t                 HandleEntries[],    
                                                phcsBflI3P4AAct_RatsParam_t         *psRatsParam)
{
    phcsBflI3P4AAct_t *activation = NULL;
    phcsBflI3P4_ProtParam_t *protocol_params = NULL;
    int16_t handle;

    if (psRemoteDevInfo == NULL || 
        psRatsParam == NULL ||
        psRequestParam == NULL ||
        psSelectParam == NULL)
    {
        return PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER);
    }

    phJal_Trace(NULL, "phJal_InitialiseISO_144434RemoteDevice");

    handle = phJal_AquireHandle(HandleEntries);

    if (handle == NO_HANDLE_AVAILABLE)
    {
        return PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INSUFFICIENT_RESOURCES);
    }

    psRemoteDevInfo->hLogHandle = (uint8_t)handle;
    psRemoteDevInfo->OpMode = phHal_eOpModesISO14443_4A;
    psRemoteDevInfo->RemoteDevInfo.CardInfo106.Startup106.NFCID1tLength = psSelectParam->uid_length/8;
    psRemoteDevInfo->SessionOpened = 0;
    memcpy(psRemoteDevInfo->RemoteDevInfo.ISO14443_4A_Info.Startup106.NFCID1t, 
            psSelectParam->uid, 
            psSelectParam->uid_length/8);

    psRemoteDevInfo->RemoteDevInfo.ISO14443_4A_Info.Startup106.SelRes = psSelectParam->sak;
    memcpy(psRemoteDevInfo->RemoteDevInfo.ISO14443_4A_Info.Startup106.SensRes, psRequestParam->atq, 2);

    activation = (phcsBflI3P4AAct_t*)psRatsParam->self;
    protocol_params = (phcsBflI3P4_ProtParam_t*)activation->mp_Members;
    
    psRemoteDevInfo->RemoteDevInfo.ISO14443_4A_Info.ISO14443_4A_Protocol.CIDUsed = 
        protocol_params->cid_supported;
    
    memcpy(psRemoteDevInfo->RemoteDevInfo.ISO14443_4A_Info.ISO14443_4A_Protocol.HistoBytes,
           psRatsParam->app_inf, psRatsParam->app_inf_len);
    
    psRemoteDevInfo->RemoteDevInfo.ISO14443_4A_Info.CurrentState = phHal_eStateListPolledState;
    psRemoteDevInfo->RemoteDevInfo.ISO14443_4A_Info.ISO14443_4A_Protocol.HistoBytesLength =
        psRatsParam->app_inf_len;
    psRemoteDevInfo->RemoteDevInfo.ISO14443_4A_Info.ISO14443_4A_Protocol.NADUsed =
        protocol_params->nad_supported;

    return NFCSTATUS_SUCCESS;
}

NFCSTATUS phJal_InitialiseP106Startup(phHal_sRemoteDevInformation_t         *psRemoteDevInfo,
                                      phcsBflI3P3A_ReqAParam_t            *psRequestParam,
                                      phcsBflI3P3A_AnticollSelectParam_t  *psSelectParam,
                                      phHal_eOpModes_t                      OpMode)
{
    uint8_t*    nfcid1t     =   NULL;
    uint8_t*    nfcid1tlen  =   NULL;

    if (psRemoteDevInfo == NULL ||
        psRequestParam == NULL ||
        psSelectParam == NULL)
    {
        return PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER);
    }

    phJal_Trace(NULL, "phJal_InitialiseP106Startup");

    switch(OpMode)
    {
        default: /* Default is 106: */
        case phHal_eOpModesNfcPassive106:
            psRemoteDevInfo->RemoteDevInfo.NfcInfo106.Startup106.SelRes = psSelectParam->sak;
            memcpy(psRemoteDevInfo->RemoteDevInfo.NfcInfo106.Startup106.SensRes, psRequestParam->atq, 2);
            psRemoteDevInfo->RemoteDevInfo.NfcInfo106.Startup106.NFCID1tLength     =   psSelectParam->uid_length/8;
               
            nfcid1t = psRemoteDevInfo->RemoteDevInfo.NfcInfo106.Startup106.NFCID1t;  
            nfcid1tlen =  &psRemoteDevInfo->RemoteDevInfo.NfcInfo106.Startup106.NFCID1tLength; 
            break;
        case phHal_eOpModesISO14443_4A:
            psRemoteDevInfo->RemoteDevInfo.ISO14443_4A_Info.Startup106.SelRes = psSelectParam->sak;
            memcpy(psRemoteDevInfo->RemoteDevInfo.ISO14443_4A_Info.Startup106.SensRes, psRequestParam->atq, 2);
            psRemoteDevInfo->RemoteDevInfo.ISO14443_4A_Info.Startup106.NFCID1tLength     =   psSelectParam->uid_length/8;
            nfcid1t     = psRemoteDevInfo->RemoteDevInfo.ISO14443_4A_Info.Startup106.NFCID1t;
            nfcid1tlen =  &psRemoteDevInfo->RemoteDevInfo.ISO14443_4A_Info.Startup106.NFCID1tLength; 
            break;
        case phHal_eOpModesMifare:
            psRemoteDevInfo->RemoteDevInfo.CardInfo106.Startup106.SelRes = psSelectParam->sak;
            memcpy(psRemoteDevInfo->RemoteDevInfo.CardInfo106.Startup106.SensRes, psRequestParam->atq, 2);
            psRemoteDevInfo->RemoteDevInfo.CardInfo106.Startup106.NFCID1tLength     =   psSelectParam->uid_length/8;
            nfcid1t = psRemoteDevInfo->RemoteDevInfo.CardInfo106.Startup106.NFCID1t;  
            nfcid1tlen =  &psRemoteDevInfo->RemoteDevInfo.CardInfo106.Startup106.NFCID1tLength; 
            break;
    }

      switch(*nfcid1tlen)
      {
        case 4:
        /*  CL1 only */
        memcpy(nfcid1t, psSelectParam->uid,4);
        break;

        default:
    
        /*  CL2or3: remove CT byte*/
        *nfcid1tlen -=1;
        memcpy(nfcid1t, &psSelectParam->uid[1],
            *nfcid1tlen);
        /*  Hack the nfcid1t last byte, by storing the CT byte we removed, in case we want to use it again! 
            Unfortunatly, this is the only way to store the CT information without interferring with existing structures! 
         */
        nfcid1t[*nfcid1tlen] =   psSelectParam->uid[0];
        break;
      }

    return NFCSTATUS_SUCCESS;
}

NFCSTATUS phJal_InitialiseNfcProtocol(phHal_sHwReference_t              *psHwReference,
                                      phHal_sRemoteDevInformation_t     *psRemoteDevInfo,
                                      phcsBflNfc_InitiatorAtrReqParam_t *psAttributRequestParam,
                                      NfcSpeeds                         Speed,
                                      NfcModes                          Mode)
{
    int16_t handle = NO_HANDLE_AVAILABLE;
    phJal_sBFLUpperFrame_t *bfl_frame = NULL;
    uint8_t didt_used = 0x0;
    uint8_t nadt_supported = 0x0;

    if (psHwReference == NULL ||
        psRemoteDevInfo == NULL ||
        psAttributRequestParam == NULL)
    {
        return PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER);
    }

    phJal_Trace(psHwReference, "phJal_InitialiseNfcProtocol");

    bfl_frame = (phJal_sBFLUpperFrame_t*)psHwReference->pBoardDriver;

    handle = phJal_AquireHandle(bfl_frame->LogHandleEntries);

    if (handle == NO_HANDLE_AVAILABLE)
    {
        return PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INSUFFICIENT_RESOURCES);
    }

    psRemoteDevInfo->hLogHandle = (uint8_t)handle;
    psRemoteDevInfo->SessionOpened = 0;

    if (psAttributRequestParam->pp_it & PHCS_BFLNFCCOMMON_ATR_PP_NAD_PRES)
    {
        nadt_supported = 0x01;
    }
    else
    {
        nadt_supported = 0x0;
    }

    if (psAttributRequestParam->did == 0)
    {
        didt_used = 0x0;
    }
    else
    {
        didt_used = 0x01;
    }

    switch(Mode)
    {
        default:
            /* Invalid mode: Defaults to PASSIVE. */
        case PASSIVE_MODE:
            switch(Speed)
            {
                default:
                    /* Invalid Speed: Defaults to 106. */
                case RF106K:
                    psRemoteDevInfo->OpMode = phHal_eOpModesNfcPassive106;
                    psRemoteDevInfo->RemoteDevInfo.NfcInfo106.CurrentState = phHal_eStateListPolledState;
                    psRemoteDevInfo->RemoteDevInfo.NfcInfo106.NfcProtocol.DIDtUsed = didt_used;
                    psRemoteDevInfo->RemoteDevInfo.NfcInfo106.NfcProtocol.NADtSup = nadt_supported;
                    memcpy(psRemoteDevInfo->RemoteDevInfo.NfcInfo106.NfcProtocol.NFCID3t, 
                        psAttributRequestParam->nfcid_it, PHCS_BFLNFCCOMMON_ID_LEN);
                    memcpy(psRemoteDevInfo->RemoteDevInfo.NfcInfo106.NfcProtocol.TgGeneralByte, 
                        psAttributRequestParam->g_it, psAttributRequestParam->glen_t);
                    psRemoteDevInfo->RemoteDevInfo.NfcInfo106.NfcProtocol.TgGeneralByteLength = 
                        psAttributRequestParam->glen_t;
                    break;
                case RF212K:
                    psRemoteDevInfo->OpMode = phHal_eOpModesNfcPassive212;
                    psRemoteDevInfo->RemoteDevInfo.NfcInfo212_424.CurrentState = phHal_eStateListPolledState;
                    psRemoteDevInfo->RemoteDevInfo.NfcInfo212_424.NfcProtocol.DIDtUsed = didt_used;
                    psRemoteDevInfo->RemoteDevInfo.NfcInfo212_424.NfcProtocol.NADtSup = nadt_supported;
                    memcpy(psRemoteDevInfo->RemoteDevInfo.NfcInfo212_424.NfcProtocol.NFCID3t, 
                        psAttributRequestParam->nfcid_it, PHCS_BFLNFCCOMMON_ID_LEN);
                    memcpy(psRemoteDevInfo->RemoteDevInfo.NfcInfo212_424.NfcProtocol.TgGeneralByte, 
                        psAttributRequestParam->g_it, psAttributRequestParam->glen_t);
                    psRemoteDevInfo->RemoteDevInfo.NfcInfo212_424.NfcProtocol.TgGeneralByteLength = 
                        psAttributRequestParam->glen_t;
                    break;
                case RF424K:
                    psRemoteDevInfo->OpMode = phHal_eOpModesNfcPassive424;
                    psRemoteDevInfo->RemoteDevInfo.NfcInfo212_424.CurrentState = phHal_eStateListPolledState;
                    psRemoteDevInfo->RemoteDevInfo.NfcInfo212_424.NfcProtocol.DIDtUsed = didt_used;
                    psRemoteDevInfo->RemoteDevInfo.NfcInfo212_424.NfcProtocol.NADtSup = nadt_supported;
                    memcpy(psRemoteDevInfo->RemoteDevInfo.NfcInfo212_424.NfcProtocol.NFCID3t, 
                        psAttributRequestParam->nfcid_it, PHCS_BFLNFCCOMMON_ID_LEN);
                    memcpy(psRemoteDevInfo->RemoteDevInfo.NfcInfo212_424.NfcProtocol.TgGeneralByte, 
                        psAttributRequestParam->g_it, psAttributRequestParam->glen_t);
                    psRemoteDevInfo->RemoteDevInfo.NfcInfo212_424.NfcProtocol.TgGeneralByteLength = 
                        psAttributRequestParam->glen_t;
                    break;
            }
            break;
        case ACTIVE_MODE:
            psRemoteDevInfo->RemoteDevInfo.NfcActiveInfo.NfcProtocol.DIDtUsed = didt_used;
            psRemoteDevInfo->RemoteDevInfo.NfcActiveInfo.NfcProtocol.NADtSup = nadt_supported;
            psRemoteDevInfo->RemoteDevInfo.NfcActiveInfo.CurrentState = phHal_eStateListPolledState;
            memcpy(psRemoteDevInfo->RemoteDevInfo.NfcActiveInfo.NfcProtocol.NFCID3t, 
                psAttributRequestParam->nfcid_it, PHCS_BFLNFCCOMMON_ID_LEN);
            switch(Speed)
            {
                default:
                    /* Invalid Speed: Defaults to 106. */
                case RF106K:
                    psRemoteDevInfo->OpMode = phHal_eOpModesNfcActive106;
                    break;
                case RF212K:
                    psRemoteDevInfo->OpMode = phHal_eOpModesNfcActive212;
                    break;
                case RF424K:
                    psRemoteDevInfo->OpMode = phHal_eOpModesNfcActive424;
                    break;
            }
            break;
    }

    return NFCSTATUS_SUCCESS;
}

NFCSTATUS phJal_InitialiseP212424Startup(phHal_sRemoteDevInformation_t  *psRemoteDevInfo,                                 
                                        phcsBflPolAct_PollingParam_t    *psFeliCaPollingParam,
                                        uint16_t                        RxBufferResponsePosOfTimeslot,
                                        phHal_eOpModes_t                OpMode)
{
    uint8_t system_code_available = 0x0;

    if (psRemoteDevInfo == NULL || 
        psFeliCaPollingParam == NULL)       
    {
        return PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER);
    }

    phJal_Trace(NULL, "phJal_InitialiseP212424Startup");

    if (psFeliCaPollingParam->rx_buffer[RxBufferResponsePosOfTimeslot] == FELICA_TIMESLOT_LENGTH)
    {
        system_code_available = 0x01;
    }
    else
    {
        system_code_available = 0x0;
    }

    if ((OpMode == phHal_eOpModesFelica212) ||
        (OpMode == phHal_eOpModesFelica424))
    {
        psRemoteDevInfo->OpMode = OpMode;                         
        psRemoteDevInfo->RemoteDevInfo.CardInfo212_424.CurrentState =
            phHal_eStateListPolledState;
       

        psRemoteDevInfo->RemoteDevInfo.CardInfo212_424.Startup212_424.RespCode = 
            psFeliCaPollingParam->rx_buffer[++RxBufferResponsePosOfTimeslot];

        ++RxBufferResponsePosOfTimeslot;
        memcpy(psRemoteDevInfo->RemoteDevInfo.CardInfo212_424.Startup212_424.NFCID2t,
            &psFeliCaPollingParam->rx_buffer[RxBufferResponsePosOfTimeslot], FELICA_IDMM_LENGTH);
        /* BugFix: For AtrReq, 10 bytes has to be sent and the last two bytes should be 00 */
        psRemoteDevInfo->RemoteDevInfo.CardInfo212_424.Startup212_424.NFCID2t[8] = 0x00;
        psRemoteDevInfo->RemoteDevInfo.CardInfo212_424.Startup212_424.NFCID2t[9] = 0x00;

        RxBufferResponsePosOfTimeslot += FELICA_IDMM_LENGTH;
        memcpy(psRemoteDevInfo->RemoteDevInfo.CardInfo212_424.Startup212_424.PMm,
            &psFeliCaPollingParam->rx_buffer[RxBufferResponsePosOfTimeslot], FELICA_PMM_LENGTH);

        if (system_code_available == 0x01)
        {
            psRemoteDevInfo->RemoteDevInfo.CardInfo212_424.Startup212_424.SystemCodeAvailable = 0x01;
            RxBufferResponsePosOfTimeslot += FELICA_PMM_LENGTH;
            memcpy(psRemoteDevInfo->RemoteDevInfo.CardInfo212_424.Startup212_424.SystemCode,
                &psFeliCaPollingParam->rx_buffer[RxBufferResponsePosOfTimeslot], FELICA_SYSTEM_CODE_LENGTH);
        }
        else
        {
            psRemoteDevInfo->RemoteDevInfo.CardInfo212_424.Startup212_424.SystemCodeAvailable = 0x00;
        }
    }
    else if ((OpMode == phHal_eOpModesNfcPassive212) ||
             (OpMode == phHal_eOpModesNfcPassive424))
    {
        psRemoteDevInfo->OpMode = OpMode;                         
        psRemoteDevInfo->RemoteDevInfo.NfcInfo212_424.CurrentState =
            phHal_eStateListPolledState;

        psRemoteDevInfo->RemoteDevInfo.NfcInfo212_424.Startup212_424.RespCode = 
        psFeliCaPollingParam->rx_buffer[++RxBufferResponsePosOfTimeslot];

        RxBufferResponsePosOfTimeslot += 1;
        memcpy(psRemoteDevInfo->RemoteDevInfo.NfcInfo212_424.Startup212_424.NFCID2t,
            &psFeliCaPollingParam->rx_buffer[RxBufferResponsePosOfTimeslot], FELICA_IDMM_LENGTH);
        /* BugFix: For AtrReq, 10 bytes has to be sent and the last two bytes should be 00 */
        psRemoteDevInfo->RemoteDevInfo.CardInfo212_424.Startup212_424.NFCID2t[8] = 0x00;
        psRemoteDevInfo->RemoteDevInfo.CardInfo212_424.Startup212_424.NFCID2t[9] = 0x00;

        RxBufferResponsePosOfTimeslot += FELICA_IDMM_LENGTH;
        memcpy(psRemoteDevInfo->RemoteDevInfo.NfcInfo212_424.Startup212_424.PMm,
            &psFeliCaPollingParam->rx_buffer[RxBufferResponsePosOfTimeslot], FELICA_PMM_LENGTH);
        
        if (system_code_available == 0x01)
        {
            psRemoteDevInfo->RemoteDevInfo.NfcInfo212_424.Startup212_424.SystemCodeAvailable = 0x01;

            RxBufferResponsePosOfTimeslot += FELICA_PMM_LENGTH;
            memcpy(psRemoteDevInfo->RemoteDevInfo.NfcInfo212_424.Startup212_424.SystemCode,
                &psFeliCaPollingParam->rx_buffer[RxBufferResponsePosOfTimeslot], FELICA_SYSTEM_CODE_LENGTH);
        }
        else
        {
            psRemoteDevInfo->RemoteDevInfo.NfcInfo212_424.Startup212_424.SystemCodeAvailable = 0x00;
        }
    }
    else
    {
        return PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER);
    }
    return NFCSTATUS_SUCCESS;
}

NFCSTATUS phJal_ActivateIso144434Protocol(phHal_sHwReference_t          *psHwReference,
                                          phcsBflI3P4_t                 *psProtocol,
                                          phcsBflI3P4AAct_RatsParam_t   *psRatsParam,
                                          uint8_t                       Cid)
{
    phcsBflI3P4_SetProtParam_t set_protocol_param = {0};
    phJal_sBFLUpperFrame_t *bfl_frame = NULL;
    NFCSTATUS command_status = NFCSTATUS_SUCCESS;
    phcsBfl_Status_t bfl_status = PH_ERR_BFL_SUCCESS;
    
    if ((psHwReference == NULL) || 
        (psRatsParam == NULL) ||
        (psProtocol == NULL))
    {
        return PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER);
    }

    phJal_Trace(psHwReference, "phJal_ActivateIso144434Protocol");

    bfl_frame = (phJal_sBFLUpperFrame_t*)psHwReference->pBoardDriver;

    psRatsParam->self  = &bfl_frame->ISO14443_4A_Act;
    psRatsParam->cid   = Cid;
    psRatsParam->fsi   = 4;	
    psRatsParam->ats   = bfl_frame->TRxBuffer;

    bfl_status = SetTimeOut(bfl_frame->RcRegCtl, &bfl_frame->RcOpCtl, 
        ISO_144434_RF_COMMUNICATION_DELAY, USED_PRESCALER_VALUE);
    
    if (bfl_status == PH_ERR_BFL_SUCCESS)
    {
        bfl_status = bfl_frame->ISO14443_4A_Act.Rats(psRatsParam);
    }

    if (bfl_status == PH_ERR_BFL_SUCCESS)
    {
        set_protocol_param.self = psProtocol;
        set_protocol_param.max_retry        = 1;
        set_protocol_param.p_protocol_param = &bfl_frame->ISO14443_4A_Pp;
        psProtocol->SetProtocol(&set_protocol_param); 
    }
    else
    {
        command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_RF_TIMEOUT);
    }
    
    return command_status;
}

NFCSTATUS phJal_DeselectIso144434Device(phHal_sHwReference_t     *psHwReference,
                                        phcsBflI3P4_t            *psProtocol)
{
    phcsBflI3P4_DeselectParam_t deselect_param = {0};
    phJal_sBFLUpperFrame_t      *bfl_frame = NULL;
    NFCSTATUS                   command_status;
    phcsBfl_Status_t            bfl_status;
    
    if ((psHwReference == NULL) || 
        (psProtocol == NULL))
    {
        return PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER);
    }

    phJal_Trace(psHwReference, "phJal_DeselectIso144434Device");

    bfl_frame = (phJal_sBFLUpperFrame_t*)psHwReference->pBoardDriver;

    deselect_param.self = psProtocol;
    bfl_status = psProtocol->Deselect(&deselect_param);
    
    if (bfl_status == PH_ERR_BFL_SUCCESS)
    {
        command_status = NFCSTATUS_SUCCESS;
    }
    else
    {
        command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_RF_TIMEOUT);
    }

    return command_status;
}

NFCSTATUS phJal_ConfigureHardwareForNfc106kProtocol(phHal_sHwReference_t    *psHwReference)
{
    phJal_sBFLUpperFrame_t      *bfl_frame = NULL;
    phcsBflRegCtl_ModRegParam_t modify_param = {0};
    phcsBfl_Status_t            bfl_status = PH_ERR_BFL_SUCCESS;
    NFCSTATUS                   command_status = NFCSTATUS_SUCCESS;

    if (psHwReference != NULL)
    {        
        phJal_Trace(psHwReference, "phJal_ConfigureHardwareForNfc106kProtocol");

        bfl_frame = (phJal_sBFLUpperFrame_t*)psHwReference->pBoardDriver;
        modify_param.self    = bfl_frame->RcRegCtl;
        modify_param.set     = 1;
        modify_param.address = PHCS_BFL_JREG_MODE;
        modify_param.mask    = PHCS_BFL_JBIT_DETECTSYNC;
        bfl_status = bfl_frame->RcRegCtl->ModifyRegister(&modify_param);

        if (bfl_status == PH_ERR_BFL_SUCCESS)
        {
            command_status = NFCSTATUS_SUCCESS;
        }
        else
        {
            command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_BOARD_COMMUNICATION_ERROR); 
        }
    }
    else
    {
        command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_DEVICE);
    }
    return command_status;
}

NFCSTATUS phJal_ActivateNfcProtocol(phHal_sHwReference_t                *psHwReference,
                                    phHal_sDevInputParam_t              *psDevInputParam,
                                    phcsBflNfc_Initiator_t              *psProtocol,
                                    phcsBflNfc_InitiatorAtrReqParam_t   *psAttributRequestParam,
                                    uint8_t                             Did,
                                    uint8_t                             pNfcId2t[PHCS_BFLNFCCOMMON_ID_LEN],
                                    uint8_t                             SendAttention)
{
    phJal_sBFLUpperFrame_t              *bfl_frame = NULL;
    phcsBflNfc_InitiatorAttReqParam_t   attention_param = {0};
    phcsBfl_Status_t                    bfl_status = PH_ERR_BFL_SUCCESS;
    NFCSTATUS                           command_status = NFCSTATUS_SUCCESS;
    
    if ((psHwReference == NULL) ||
        (psProtocol == NULL) ||
        (psDevInputParam == NULL))
    {
        return PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_DEVICE);
    }

    phJal_Trace(psHwReference, "phJal_ActivateNfcProtocol");

    bfl_frame = (phJal_sBFLUpperFrame_t*)psHwReference->pBoardDriver;

    psAttributRequestParam->self = psProtocol;
    
    if (pNfcId2t != NULL)
    {
        psAttributRequestParam->nfcid_it = pNfcId2t;
    }
    else
    {
        psAttributRequestParam->nfcid_it = psDevInputParam->NFCID3i;
    }

    /* Workaround to reset a pending interrupt */
    bfl_frame->RcRegCtlModifyParam.address = PHCS_BFL_JREG_CONTROL;
    bfl_frame->RcRegCtlModifyParam.set = 1;
    bfl_frame->RcRegCtlModifyParam.mask = PHCS_BFL_JBIT_TSTOPNOW;
    command_status = bfl_frame->RcRegCtl->ModifyRegister(&bfl_frame->RcRegCtlModifyParam);

    bfl_frame->RcRegCtlSetRegParam.address  = PHCS_BFL_JREG_COMMIRQ;
    bfl_frame->RcRegCtlSetRegParam.reg_data = 0x01;
    command_status = bfl_frame->RcRegCtl->SetRegister(&bfl_frame->RcRegCtlSetRegParam);
    /* End of Workaround */

    psAttributRequestParam->did = Did;
    psAttributRequestParam->bs_it = 0x00;
    psAttributRequestParam->br_it = 0x00;

    psAttributRequestParam->pp_it = PHCS_BFLNFCCOMMON_ATR_PP_LR_64;

    if (psDevInputParam->GeneralByteLength > 0)
    {
        psAttributRequestParam->pp_it |= PHCS_BFLNFCCOMMON_ATR_PP_GI_PRES;
    }

    if (psDevInputParam->NfcNADiUsed == 1)
    {
        psAttributRequestParam->pp_it |= PHCS_BFLNFCCOMMON_ATR_PP_NAD_PRES;
    }

    psAttributRequestParam->g_it     = psDevInputParam->GeneralByte;
    psAttributRequestParam->glen_i   = psDevInputParam->GeneralByteLength;
    bfl_status = psProtocol->AtrRequest(psAttributRequestParam);
    reportDbg1("atr status: %02X\n", bfl_status);
    
    if (bfl_status == PH_ERR_BFL_SUCCESS)
    {
        if (SendAttention == 1)
        {
            attention_param.self = psProtocol;
            bfl_status = psProtocol->AttRequest(&attention_param);
            reportDbg1("att status: %02X\n", bfl_status);
            if (bfl_status == PH_ERR_BFL_SUCCESS)
            {
                command_status = NFCSTATUS_SUCCESS;
            }
            else
            {
                command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_RF_TIMEOUT);
            }
        }
        else
        {
            command_status = NFCSTATUS_SUCCESS;
        }
    }
    else
    {
        command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_RF_TIMEOUT);
    }
    return command_status;
}


NFCSTATUS phJal_DeselectNfcDevice(phHal_sHwReference_t   *psHwReference,
                                  phcsBflNfc_Initiator_t *psProtocol)
{
    phJal_sBFLUpperFrame_t           *bfl_frame = NULL;
    phcsBfl_Status_t                  bfl_status = PH_ERR_BFL_SUCCESS;
    NFCSTATUS                         command_status = NFCSTATUS_SUCCESS;
    phcsBflNfc_InitiatorDslReqParam_t deselect_param;
    uint8_t                           rf_status = PHJAL_NORFON;

    if ((psHwReference == NULL) ||
        (psProtocol == NULL))
    {
        return PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_DEVICE);
    }

    phJal_Trace(psHwReference, "phJal_DeselectNfcDevice");

    bfl_frame = (phJal_sBFLUpperFrame_t*)psHwReference->pBoardDriver;

    /* Mantis #536, GK: Only send DSL when RF is ON (otherwise deadlock). */
    reportDbg0("Checking whether RF is ON (only sending in this case)...\n");

    bfl_status = phJal_GetRfStatus(psHwReference, &rf_status);

    if (PH_ERR_BFL_SUCCESS != bfl_status)
    {
        command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_BOARD_COMMUNICATION_ERROR);
    } else
    {
        /* Get Status OK */
        if ((rf_status & PHJAL_INTRFON) > 0)
        {
            reportDbg0("RF is ON.\n");
            deselect_param.self = psProtocol;
            bfl_status = psProtocol->DslRequest(&deselect_param);
            if (bfl_status == PH_ERR_BFL_SUCCESS)
            {
                command_status = NFCSTATUS_SUCCESS;
            }
            else
            {
                command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_RF_TIMEOUT);
            }
        } else
        {
            if ((rf_status & PHJAL_EXTRFON) > 0)
            {
                reportDbg0("EXTERNAL RF is ON.\n");
                command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_EXTERNAL_RF_DETECTED);
            } else
            {
                reportDbg0("NO RF is ON.\n");
                command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_RF_TIMEOUT);
            }
        }
    }
    return command_status;  
}

void phJal_GenerateNfcId(uint8_t pNfcId[PHCS_BFLNFCCOMMON_ID_LEN])
{
    uint8_t nfcid_index = 0;
    
    srand(0xFF);
    
    for(nfcid_index = 0; nfcid_index < PHCS_BFLNFCCOMMON_ID_LEN; ++nfcid_index)
    {
        pNfcId[nfcid_index] = (uint8_t)rand();
    }
}

phcsBfl_Status_t phJal_ClearPeripheral(phJal_sBFLUpperFrame_t   *bfl_frame)
{
    phcsBfl_Status_t            status;

    phJal_Trace(NULL, "phJal_ClearPeripheral");

    #define JAL_CHKOK(_status_) if (NFCSTATUS_SUCCESS != _status_){goto end;} /* */

    bfl_frame->RcRegCtlSetRegParam.address  = PHCS_BFL_JREG_COMMAND;
    bfl_frame->RcRegCtlSetRegParam.reg_data = PHCS_BFL_JCMD_IDLE;
    status = bfl_frame->RcRegCtl->SetRegister(&bfl_frame->RcRegCtlSetRegParam);
    JAL_CHKOK(status);

    bfl_frame->RcRegCtlSetRegParam.address  = PHCS_BFL_JREG_FIFOLEVEL;
    bfl_frame->RcRegCtlSetRegParam.reg_data = PHCS_BFL_JBIT_FLUSHBUFFER;
    status = bfl_frame->RcRegCtl->SetRegister(&bfl_frame->RcRegCtlSetRegParam);
    JAL_CHKOK(status);

    bfl_frame->RcRegCtlSetRegParam.address  = PHCS_BFL_JREG_COMMIRQ;
    bfl_frame->RcRegCtlSetRegParam.reg_data = 0x7F;
    status = bfl_frame->RcRegCtl->SetRegister(&bfl_frame->RcRegCtlSetRegParam);
    JAL_CHKOK(status);

    bfl_frame->RcRegCtlSetRegParam.address  = PHCS_BFL_JREG_DIVIRQ;
    bfl_frame->RcRegCtlSetRegParam.reg_data = 0x7F;
    status = bfl_frame->RcRegCtl->SetRegister(&bfl_frame->RcRegCtlSetRegParam);
    JAL_CHKOK(status);

    /* Clear INITIATOR bit and stop timer: */
    bfl_frame->RcRegCtlModifyParam.address  = PHCS_BFL_JREG_CONTROL;
    bfl_frame->RcRegCtlModifyParam.mask = 0x18;
    bfl_frame->RcRegCtlModifyParam.set  = 0;
    status = bfl_frame->RcRegCtl->ModifyRegister(&bfl_frame->RcRegCtlModifyParam);
    JAL_CHKOK(status);

    /* CLEAR IRQINV */
    bfl_frame->RcRegCtlModifyParam.address  = PHCS_BFL_JREG_COMMIEN;
    bfl_frame->RcRegCtlModifyParam.mask = 0x80;
    bfl_frame->RcRegCtlModifyParam.set  = 0;
    status = bfl_frame->RcRegCtl->ModifyRegister(&bfl_frame->RcRegCtlModifyParam);
    JAL_CHKOK(status);

    /* SET IRQPUSHPULL */
    bfl_frame->RcRegCtlModifyParam.address  = PHCS_BFL_JREG_DIVIEN;
    bfl_frame->RcRegCtlModifyParam.mask = 0x80;
    bfl_frame->RcRegCtlModifyParam.set  = 1;
    status = bfl_frame->RcRegCtl->ModifyRegister(&bfl_frame->RcRegCtlModifyParam);
    JAL_CHKOK(status);

    end: phJal_RegDump(bfl_frame);

    return status;
}

phcsBfl_Status_t phJal_RegDump(phJal_sBFLUpperFrame_t   *bfl_frame)
{
    phcsBfl_Status_t            status = PH_ERR_BFL_SUCCESS;

    #ifndef NDEBUG

    uint8_t addr;
    uint8_t page;

    reportDbg0("   00 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15\n");
    for (page = 0; page < 4; page++)
    {
        reportDbg1("%02d ", page);
        for (addr = 0; addr < 16; addr++)
        {
            bfl_frame->RcRegCtlGetRegParam.address = (page << 4) | addr;
            if (bfl_frame->RcRegCtlGetRegParam.address != PHCS_BFL_JREG_FIFODATA)
            {
                status = bfl_frame->RcRegCtl->GetRegister(&bfl_frame->RcRegCtlGetRegParam);
                reportDbg1("%02X ", bfl_frame->RcRegCtlGetRegParam.reg_data);
            } else
            {
                reportDbg0(".. ");
            }
        }
        reportDbg0("\n");
    }
    
    #endif /* NDEBUG */

    return status;
}

#endif /* Guard */