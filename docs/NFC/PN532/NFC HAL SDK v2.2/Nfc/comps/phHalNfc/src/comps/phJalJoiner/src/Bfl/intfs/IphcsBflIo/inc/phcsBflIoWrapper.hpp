/* /////////////////////////////////////////////////////////////////////////////////////////////////
//                     Copyright (c) Philips Semiconductors
//
//                       (C)PHILIPS Electronics N.V.2004
//         All rights are reserved. Reproduction in whole or in part is
//        prohibited without the written consent of the copyright owner.
//    Philips reserves the right to make changes without notice at any time.
//   Philips makes no warranty, expressed, implied or statutory, including but
//   not limited to any implied warranty of merchantability or fitness for any
//  particular purpose, or that the use will not infringe any third party patent,
//   copyright or trademark. Philips must not be liable for any loss or damage
//                            arising from its use.
///////////////////////////////////////////////////////////////////////////////////////////////// */

/*! 
 * \if cond_generate_filedocu 
 * \file phcsBflIoWrapper.hpp
 *
 * Project: Object Oriented Library Framework I/O Component.
 *
 *  Source: phcsBflIoWrapper.hpp
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Date: Thu Sep 27 09:39:00 2007 $
 *
 * Comment:
 *  C++ wrapper for I/O.
 *
 * History:
 *  GK:  Generated 12. March 2003
 *  MHa: Migrated to MoReUse September 2005
 * \endif
 *
 */

#ifndef PHCSBFLIOWRAPPER_HPP
#define PHCSBFLIOWRAPPER_HPP

#include <phcsBflStatus.h>
#include <phcsBflRegCtlWrapper.hpp>

// Include the meta-header file for all hardware variants supported by the C++ interface:
extern "C"
{
    #include <phcsBflIo.h>
    #include <phcsBflIo.h>
}

namespace phcs_BFL
{
 
/*! \cond cond_Cpp_complete */
/* \ingroup rcio
 *  Basic I/O functionality of the RF frontend
 *  (abstract class, used for the C++ interface).
 */
class phcsBfl_Io
{
    public:
        virtual void SetWaitEventCb(phcsBflIo_SetWecParam_t *set_wec_param)                      = 0;

        virtual phcsBfl_Status_t Transceive(phcsBflIo_TransceiveParam_t *transceive_param)         = 0;
        virtual phcsBfl_Status_t Transmit(phcsBflIo_TransmitParam_t *transmit_param)               = 0;
        virtual phcsBfl_Status_t Receive(phcsBflIo_ReceiveParam_t *receive_param)                  = 0;
        virtual phcsBfl_Status_t MfAuthent(phcsBflIo_MfAuthentParam_t *mfauthent_param)            = 0;

        /*! Lower edge interface: Not to be redefined in the derived class: */
        class phcsBfl_RegCtl *mp_Lower;
};


// Function pointer definition for C- phcsBflIo_Wrapper Initialise function.
typedef void (*pphcsBflIo_Initialize_t) (phcsBflIo_t*, void*, phcsBflRegCtl_t*, uint8_t);


/* \ingroup rcio
 *  This is the GLUE class the C-kernel I/O "sees" when calling down the stack. The glue class
 *  represents the C-API of the lower layer (RC Reg. Ctl.).
 */
class phcsBfl_IoGlue
{
    public:
        phcsBfl_IoGlue(void);
        ~phcsBfl_IoGlue(void);
        
        //! \name Static function for SetRegister, able to call into C++ again.
        //@{
        static phcsBfl_Status_t SetRegister(phcsBflRegCtl_SetRegParam_t *setreg_param);
        static phcsBfl_Status_t GetRegister(phcsBflRegCtl_GetRegParam_t *getreg_param);
        static phcsBfl_Status_t ModifyRegister(phcsBflRegCtl_ModRegParam_t *modify_param);
        static phcsBfl_Status_t SetRegisterMultiple(phcsBflRegCtl_SetMultiRegParam_t *setmultireg_param);
        static phcsBfl_Status_t GetRegisterMultiple(phcsBflRegCtl_GetMultiRegParam_t *getmultireg_param);
        //@}

        //! Structure accessible from the upper object's pure C-kernel.
        phcsBflRegCtl_t m_GlueStruct;
};
/*! \endcond */


/*! \ingroup rcio
 *  This is the main interface of I/O. The class is a template because it cannot be foreseen
 *  which type of C-kernel to wrap around. Depending on the implementation of this kernel, the
 *  type of the internal C-member structure changes (avoid multiple definitions of the same structure
 *  name for different structures when more than one type of functionality is used). The type of the
 *  MEMBERS (in kernel's .h file) structure is the one to specify when creating an instance
 *  of this class.
 */
template <class phcsBflIOcp> class phcsBflIo_Wrapper : public phcsBfl_Io
{
    public:
        phcsBflIo_Wrapper(void);
        ~phcsBflIo_Wrapper(void);
       /*
        * \ingroup rcio
        * \param[in]  c_rc_io_initialise    Pointer to the Initialise function of the type of C-kernel (for
        *                                   each hardware variant an own kernel must exist) to use with the
        *                                   current wrapper instance.
        * \param[in] *p_lower               Pointer to the C++ object of the underlying layer RcRegCtl.
        * \param[in] *initiator__not_target Specifier for operation mode (1 = Initiator, 0 = Target).
        *
        * \note The alternative Constructor does setup required by the Component but not done
        *       by the default constructor.
        */
        phcsBflIo_Wrapper(pphcsBflIo_Initialize_t  c_rc_io_initialise,
                        phcsBfl_RegCtl          *p_lower,
                        uint8_t                initiator__not_target);

       /*!
        * \ingroup rcio
        * \param[in]  c_rc_io_initialise    Pointer to the Initialise function of the type of C-kernel (for
        *                                   each hardware variant an own kernel must exist) to use with the
        *                                   current wrapper instance.
        * \param[in] *p_lower               Pointer to the C++ object of the underlying layer RcRegCtl.
        * \param[in]  initiator__not_target Specifier for operation mode (1 = Initiator, 0 = Target).
        *
        * \note The Initialise method does setup required by the Component but not done
        *       by the default constructor.
        */
        void Initialise(pphcsBflIo_Initialize_t  c_rc_io_initialise,
                        phcsBfl_RegCtl          *p_lower,
                        uint8_t                initiator__not_target);
       
        phcsBfl_Status_t Transceive(phcsBflIo_TransceiveParam_t *transceive_param);
        phcsBfl_Status_t Transmit(phcsBflIo_TransmitParam_t *transmit_param);
        phcsBfl_Status_t Receive(phcsBflIo_ReceiveParam_t *receive_param);
        phcsBfl_Status_t MfAuthent(phcsBflIo_MfAuthentParam_t *mfauthent_param);

        void SetWaitEventCb(phcsBflIo_SetWecParam_t *set_wec_param);
        
        /*! \brief This value stores the status of object construction. Since the Constructor cannot
         *  return a value the status is stored here and should be checked upon allocation.
         */
        phcsBfl_Status_t       m_ConstructorStatus;

        phcsBflIo_t            m_Interface;              /*!< \brief Reference of the C-kernel structure instance. */
        phcsBflIOcp            m_CommunicationParams;    /*!< \brief Pointer to the C-kernel operation variables. */

    protected:        
        class phcsBfl_IoGlue      m_RcIoGlue;
};


////////////////////////////////////////////////////////////////////////////////////////////////////
// phcsBflIo_Wrapper Wrapper Implementation, must reside here because of template usage:
////////////////////////////////////////////////////////////////////////////////////////////////////

/* \ingroup rcio
 * Template definition for phcsBflIo_Wrapper Constructor without initialisation */
template <class phcsBflIOcp>
phcsBflIo_Wrapper<phcsBflIOcp>::phcsBflIo_Wrapper(void)
{
    // This constructor does no initialisation. Initialise() is needed.
    m_ConstructorStatus = PH_ERR_BFL_SUCCESS;
}


/* \ingroup rcio
 * Template definition for phcsBflIo_Wrapper Constructor with initialisation */
template <class phcsBflIOcp>
phcsBflIo_Wrapper<phcsBflIOcp>::phcsBflIo_Wrapper(pphcsBflIo_Initialize_t   c_rc_io_initialise,
                                            phcsBfl_RegCtl           *p_lower,
                                            uint8_t                 initiator__not_target)
{
    m_ConstructorStatus = PH_ERR_BFL_SUCCESS;
    // Hand over the pointer of the C-kernel's initialise function:
    this->Initialise(c_rc_io_initialise, p_lower, initiator__not_target);
}


/* \ingroup rcio
 * Template definition for phcsBflIo_Wrapper Destructor */
template <class phcsBflIOcp>
phcsBflIo_Wrapper<phcsBflIOcp>::~phcsBflIo_Wrapper(void)
{
    // No action.
}


/* \ingroup rcio
 * Template definition for phcsBflIo_Wrapper Initialisation function */
template <class phcsBflIOcp>
void phcsBflIo_Wrapper<phcsBflIOcp>::Initialise(pphcsBflIo_Initialize_t   c_rc_io_initialise,
                                            phcsBfl_RegCtl           *p_lower,
                                            uint8_t                 initiator__not_target)
{
    // This binds the C-kernel to the C++ wrapper:
    c_rc_io_initialise(&m_Interface,
                       &m_CommunicationParams,
                       &(m_RcIoGlue.m_GlueStruct),
                       initiator__not_target);

    // Initialise C++ part, beyond of the scope of the C-kernel:
    this->mp_Lower = p_lower;
}


/* \ingroup rcio
 * Template definition for SetEventCb function. */
template <class phcsBflIOcp>
void phcsBflIo_Wrapper<phcsBflIOcp>::SetWaitEventCb(phcsBflIo_SetWecParam_t *set_wec_param)
{
    void *p_self;
    p_self = set_wec_param->self;
    set_wec_param->self = &m_Interface;
    m_Interface.SetWaitEventCb(set_wec_param);
    set_wec_param->self = p_self;
}


// Finally, the members (interface) calling into the C-kernel:

/* \ingroup rcio
 *  \param[in] *transceive_param    Pointer to the I/O parameter structure
 *  \return                         phcsBfl_Status_t value depending on the C implementation
 *
 * Template definition for Member function Transceive. */
template <class phcsBflIOcp>
phcsBfl_Status_t phcsBflIo_Wrapper<phcsBflIOcp>::Transceive(phcsBflIo_TransceiveParam_t *transceive_param)
{
    phcsBfl_Status_t   status;  
    void       *p_self; 
    
    p_self = transceive_param->self;
    transceive_param->self = &m_Interface;
    ((phcsBflRegCtl_t*)(((phcsBflIo_t*)(transceive_param->self))->mp_Lower))->mp_CallingObject = this;
    status = m_Interface.Transceive(transceive_param);
    transceive_param->self = p_self;

    return status;
}


/* \ingroup rcio
 *  \param[in] *transmit_param  Pointer to the I/O parameter structure.
 *  \return                     phcsBfl_Status_t value depending on the C implementation
 *
 * Template definition for Member function Transmit. */
template <class phcsBflIOcp>
phcsBfl_Status_t phcsBflIo_Wrapper<phcsBflIOcp>::Transmit(phcsBflIo_TransmitParam_t *transmit_param)
{
    phcsBfl_Status_t   status;  
    void       *p_self; 
    
    p_self = transmit_param->self;
    transmit_param->self = &m_Interface;
    ((phcsBflRegCtl_t*)(((phcsBflIo_t*)(transmit_param->self))->mp_Lower))->mp_CallingObject = this;
    status = m_Interface.Transmit(transmit_param);
    transmit_param->self = p_self;

    return status;
}


/* \ingroup rcio
 *  \param[in] *receive_param   Pointer to the I/O parameter structure
 *  \return                     phcsBfl_Status_t value depending on the C implementation
 *
 * Template definition for Member function Receive. */
template <class phcsBflIOcp>
phcsBfl_Status_t phcsBflIo_Wrapper<phcsBflIOcp>::Receive(phcsBflIo_ReceiveParam_t *receive_param)
{
    phcsBfl_Status_t   status;  
    void       *p_self; 
    
    p_self = receive_param->self;
    receive_param->self = &m_Interface;
    ((phcsBflRegCtl_t*)(((phcsBflIo_t*)(receive_param->self))->mp_Lower))->mp_CallingObject = this;
    status = m_Interface.Receive(receive_param);
    receive_param->self = p_self;

    return status;
}


/* \ingroup rcio
 *  \param[in] *mfauthent_param Pointer to the I/O parameter structure
 *  \return                     phcsBfl_Status_t value depending on the C implementation
 *
 * Template definition for Member function MfAuthent. */
template <class phcsBflIOcp>
phcsBfl_Status_t phcsBflIo_Wrapper<phcsBflIOcp>::MfAuthent(phcsBflIo_MfAuthentParam_t *mfauthent_param)
{
    phcsBfl_Status_t   status;  
    void       *p_self; 
    
    p_self = mfauthent_param->self;
    mfauthent_param->self = &m_Interface;
    ((phcsBflRegCtl_t*)(((phcsBflIo_t*)(mfauthent_param->self))->mp_Lower))->mp_CallingObject = this;
    status = m_Interface.MfAuthent(mfauthent_param);
    mfauthent_param->self = p_self;

    return status;
}

}   /* phcs_BFL */

#endif /* PHCSBFLIOWRAPPER_H */

/* E.O.F. */
