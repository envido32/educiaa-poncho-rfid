/*
 *         Copyright (c), NXP Semiconductors
 *
 *                       (C)NXP Electronics N.V.2004
 *         All rights are reserved. Reproduction in whole or in part is
 *        prohibited without the written consent of the copyright owner.
 *    NXP reserves the right to make changes without notice at any time.
 *   NXP makes no warranty, expressed, implied or statutory, including but
 *   not limited to any implied warranty of merchantability or fitness for any
 *  particular purpose, or that the use will not infringe any third party patent,
 *   copyright or trademark. NXP must not be liable for any loss or damage
 *                            arising from its use.
 *
 */


/*!
 * \file phJalConnectionFunctions.c
 * \brief JAL Connect/Disconnect Functions
 *
 *
 * Project: NFC MW / HAL
 *
 * $Date: Thu Sep 27 09:38:43 2007 $ 
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Aliases: SDK_HAL_2.2,SDK_HAL_2.2_071008,NFC_FRI_SDK_1.0_071031 $
 *
 */

#ifdef PHHAL_VERSION_ACQUISITION
    #define PHJALCONNECTIONFUNCTIONS_SOURCE_FILEREVISION "$Revision: 1.1 $"
    #define PHJALCONNECTIONFUNCTIONS_SOURCE_FILEALIASES  "$Aliases: SDK_HAL_2.2,SDK_HAL_2.2_071008,NFC_FRI_SDK_1.0_071031 $"
#else /* PHHAL_VERSION_ACQUISITION */

#include <memory.h>
#include <stdio.h>

#include <phNfcStatus.h>
#include <phcsBflI3P4AAct.h>
#include <phcsBflI3P4.h>
#include <phcsBflI3P3A.h>

#include <phJalConnectionFunctions.h>
#include <phJalJoinerBflFrame.h>
#include <phJalJoinerAux.h>
#include <phJalDebugReportFunctions.h>




NFCSTATUS phJal_ConnectToNfcPassive106Device(phHal_sHwReference_t             *psHwReference,
                                            phHal_sRemoteDevInformation_t     *psRemoteDevInfo,
                                            phHal_sDevInputParam_t            *psDevInputParam)
{
    phcsBfl_Status_t        bfl_status = PH_ERR_BFL_SUCCESS;
    NFCSTATUS               command_status = NFCSTATUS_SUCCESS;
    phJal_sBFLUpperFrame_t  *bfl_frame = NULL;
    
    if (psHwReference == NULL || 
        psRemoteDevInfo == NULL ||
        psDevInputParam == NULL)
    {
        return (PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER));
    }

    phJal_Trace(psHwReference, "phJal_ConnectToNfcPassive106Device");

    bfl_frame = (phJal_sBFLUpperFrame_t*)psHwReference->pBoardDriver;

    if (bfl_frame == NULL)
    {
        return (PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER));
    }

    command_status = 
        phJal_ReActivateIso144433BasedDevice(psHwReference, psRemoteDevInfo);
    reportDbg1("connect:\t OpMode of remote device: %02X\n",psRemoteDevInfo->OpMode);
    reportDbg1("connect:\t reactivation returned with NFC status: %02X\n",command_status);
    if (command_status == NFCSTATUS_SUCCESS)
    {
        if (psRemoteDevInfo->OpMode == phHal_eOpModesNfcPassive106)
        {
            bfl_status = phJal_SetBitrate(psHwReference, RF106K, PASSIVE_MODE);
            reportDbg1("connect:\t configured bitrate with BFL status: %02X\n", bfl_status);
            if (bfl_status == PH_ERR_BFL_SUCCESS)
            {
                command_status = 
                    phJal_ConfigureHardwareForNfc106kProtocol(psHwReference);
                reportDbg1("connect:\t activated DETECT_SYNC with NFC status: %02X\n", command_status);
            }
            else
            {
                command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_BOARD_COMMUNICATION_ERROR);
            }
        }

        if (command_status == NFCSTATUS_SUCCESS)
        {
            command_status = 
                phJal_ConnectToNfcDevice(psHwReference, psRemoteDevInfo, 
                psDevInputParam, PASSIVE_MODE);
        }
    }
    return command_status;
}

NFCSTATUS phJal_ConnectToNfcDevice(phHal_sHwReference_t           *psHwReference,
                                   phHal_sRemoteDevInformation_t  *psRemoteDevInfo,
                                   phHal_sDevInputParam_t         *psDevInputParam,
                                   NfcModes                       DeviceMode)
{
    phcsBfl_Status_t                    bfl_status = PH_ERR_BFL_SUCCESS;
    NFCSTATUS                           command_status = NFCSTATUS_SUCCESS;
    phJal_sBFLUpperFrame_t              *bfl_frame = NULL;
    phJal_ProtocolEntry_t               *protocol_entry = NULL;
    phcsBflNfc_InitiatorAtrReqParam_t   attribut_request_param = {0};
    phcsBflNfc_InitiatorWupReqParam_t   wup_param = {0};
    phcsBflNfc_Initiator_t              *nfc_protocol = NULL;
    uint8_t                             send_attention = 1;

    if (psHwReference == NULL || 
        psRemoteDevInfo == NULL ||
        psDevInputParam == NULL)
    {
        return (PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER));
    }

    phJal_Trace(psHwReference, "phJal_ConnectToNfcDevice");

    bfl_frame = (phJal_sBFLUpperFrame_t*)psHwReference->pBoardDriver;

    if (bfl_frame == NULL)
    {
        return (PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER));
    }

    protocol_entry =
        phJal_GetProtocolEntryForLogicalHandle(bfl_frame->NfcInitiator_Entries,
            psRemoteDevInfo->hLogHandle);

    if (protocol_entry != NULL)
    {
        nfc_protocol = ((phJal_NfcIProtocol_t*)protocol_entry->Protocol)->Protocol;
        command_status = phJal_SetBitrateForOpMode(psHwReference, psRemoteDevInfo->OpMode); 

        if (command_status == NFCSTATUS_SUCCESS)
        {
            if (DeviceMode == PASSIVE_MODE)
            {
                bfl_status = SetTimeOut(bfl_frame->RcRegCtl, &bfl_frame->RcOpCtl, 
                    NFC_PASSIVE_ACTIVATION_DELAY, USED_PRESCALER_VALUE);
                send_attention = 0;
            }
            else
            {
                bfl_status = SetTimeOut(bfl_frame->RcRegCtl, &bfl_frame->RcOpCtl, 
                    NFC_ACTIVE_ACTIVATION_DELAY, USED_PRESCALER_VALUE);
                send_attention = 1;
            }

            if (bfl_status == PH_ERR_BFL_SUCCESS)
            {
                if ((psDevInputParam->NFCIDAuto == 0x01) && 
                    (psRemoteDevInfo->OpMode != phHal_eOpModesNfcPassive212) &&
                    (psRemoteDevInfo->OpMode != phHal_eOpModesNfcPassive424))
                {
                    phJal_GenerateNfcId(psDevInputParam->NFCID3i);
                }

                if ((psRemoteDevInfo->OpMode == phHal_eOpModesNfcPassive212) ||
                    (psRemoteDevInfo->OpMode == phHal_eOpModesNfcPassive424))
                {
                    command_status = phJal_ActivateNfcProtocol(psHwReference,
                        psDevInputParam, nfc_protocol,                                    
                        &attribut_request_param, protocol_entry->DeviceId, 
                        psRemoteDevInfo->RemoteDevInfo.NfcInfo212_424.Startup212_424.NFCID2t,
                        send_attention);
                }
                else if (DeviceMode == ACTIVE_MODE)
                {                    
                    wup_param.self = nfc_protocol;
                    wup_param.did = protocol_entry->DeviceId;
                    bfl_status = nfc_protocol->WupRequest(&wup_param);

                    if (bfl_status == PH_ERR_BFL_SUCCESS)
                    {
                        command_status = NFCSTATUS_SUCCESS;
                    }
                    else
                    {
                        command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_RF_TIMEOUT);
                    }
                }
                else
                {
                    command_status = phJal_ActivateNfcProtocol(psHwReference,
                        psDevInputParam, nfc_protocol, &attribut_request_param, 
                        protocol_entry->DeviceId, NULL, send_attention);
                }
            }
            else
            {
                command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_BOARD_COMMUNICATION_ERROR);
            }
        }
        else
        {
            command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_BOARD_COMMUNICATION_ERROR);
        }
    }
    else
    {
        command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_DEVICE);
    }

    return command_status;
}

NFCSTATUS phJal_ConnectToNfcActiveDevice(phHal_sHwReference_t             *psHwReference,
                                        phHal_sRemoteDevInformation_t     *psRemoteDevInfo,
                                        phHal_sDevInputParam_t            *psDevInputParam)
{
    NFCSTATUS command_status = NFCSTATUS_SUCCESS;
    
    if (psHwReference == NULL || 
        psRemoteDevInfo == NULL ||
        psDevInputParam == NULL)
    {
        return (PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER));
    }

    phJal_Trace(psHwReference, "phJal_ConnectToNfcActiveDevice");

    if (psRemoteDevInfo->OpMode == phHal_eOpModesNfcActive106)
    {
        command_status = phJal_SetBitrateForOpMode(psHwReference, psRemoteDevInfo->OpMode);

        if (command_status == NFCSTATUS_SUCCESS)
        {
            reportDbg0("activate DETECT_SYNC\n");
            command_status =
                phJal_ConfigureHardwareForNfc106kProtocol(psHwReference);
        }
        else
        {
            command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_BOARD_COMMUNICATION_ERROR);
        }
    }
 
    if (command_status == NFCSTATUS_SUCCESS)
    {
        command_status = 
           phJal_ConnectToNfcDevice(psHwReference, psRemoteDevInfo, psDevInputParam, ACTIVE_MODE);
    }

    return command_status;
}

NFCSTATUS phJal_ConnectToIso144433Device(phHal_sHwReference_t              *psHwReference,
                                        phHal_sRemoteDevInformation_t     *psRemoteDevInfo,
                                        phHal_sDevInputParam_t            *psDevInputParam)
{
    NFCSTATUS               command_status = NFCSTATUS_SUCCESS;
    phJal_sBFLUpperFrame_t  *bfl_frame = NULL;

    if (psHwReference == NULL || 
        psRemoteDevInfo == NULL ||
        psDevInputParam == NULL)
    {
        return (PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER));
    }

    phJal_Trace(psHwReference, "phJal_ConnectToIso144433Device");

    bfl_frame = (phJal_sBFLUpperFrame_t*)psHwReference->pBoardDriver;

    if (bfl_frame == NULL)
    {
        return (PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER));
    }

    command_status = phJal_SetBitrateForOpMode(psHwReference, psRemoteDevInfo->OpMode);

    if (command_status == NFCSTATUS_SUCCESS)
    {
        command_status = 
            phJal_ReActivateIso144433BasedDevice(psHwReference, psRemoteDevInfo);
    }
    else
    {
        command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_BOARD_COMMUNICATION_ERROR);
    }
    return command_status;
}

NFCSTATUS phJal_ConnectToIso144434Device(phHal_sHwReference_t             *psHwReference,
                                        phHal_sRemoteDevInformation_t     *psRemoteDevInfo,
                                        phHal_sDevInputParam_t            *psDevInputParam)
{
    phcsBfl_Status_t                bfl_status = PH_ERR_BFL_SUCCESS;
    NFCSTATUS                       command_status = NFCSTATUS_SUCCESS;
    phJal_sBFLUpperFrame_t          *bfl_frame = NULL;
    phJal_ProtocolEntry_t           *protocol_entry = NULL;
    phcsBflI3P4AAct_RatsParam_t     rats_param = {0};

    if ((psHwReference != NULL)   && 
        (psRemoteDevInfo != NULL) &&
        (psDevInputParam != NULL))
    {
        phJal_Trace(psHwReference, "phJal_ConnectToIso144434Device");

        bfl_frame = (phJal_sBFLUpperFrame_t*)psHwReference->pBoardDriver;
        if (bfl_frame != NULL)
        {
            command_status = phJal_SetBitrateForOpMode(psHwReference, psRemoteDevInfo->OpMode);

            if (command_status == NFCSTATUS_SUCCESS)
            {
                command_status = 
                    phJal_ReActivateIso144433BasedDevice(psHwReference, psRemoteDevInfo);                
            }
            else
            {
                command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_BOARD_COMMUNICATION_ERROR);
            }
            
            if (command_status == NFCSTATUS_SUCCESS)
            {
                protocol_entry =
                    phJal_GetProtocolEntryForLogicalHandle(bfl_frame->ISO14443_4Entries,
                        psRemoteDevInfo->hLogHandle);
                
                if (protocol_entry != NULL)
                {
                    bfl_status = SetTimeOut(bfl_frame->RcRegCtl, &bfl_frame->RcOpCtl, 
                                    ISO_144434_RF_COMMUNICATION_DELAY, USED_PRESCALER_VALUE);

                    if (bfl_status == PH_ERR_BFL_SUCCESS)
                    {
                        if (psDevInputParam->NFCIDAuto == 0x01)
                        {
                            phJal_GenerateNfcId(psDevInputParam->NFCID3i);
                        }

                        command_status = phJal_ActivateIso144434Protocol(psHwReference,
                            (phcsBflI3P4_t*)protocol_entry->Protocol, &rats_param, protocol_entry->DeviceId);                       
                    }
                    else
                    {
                        command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_BOARD_COMMUNICATION_ERROR);
                    }
                }
                else
                {
                    command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_DEVICE); 
                }
            }
        }
        else
        {
            command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER);
        }
    }
    else
    {
     command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER);
    }

    return command_status;
}

NFCSTATUS phJal_ReActivateIso144433BasedDevice(phHal_sHwReference_t             *psHwReference,
                                               phHal_sRemoteDevInformation_t    *psRemoteDevInfo)
{
    phJal_sBFLUpperFrame_t                  *bfl_frame = NULL;
    phcsBfl_Status_t                        bfl_status = PH_ERR_BFL_SUCCESS;
    NFCSTATUS                               command_status = NFCSTATUS_SUCCESS;
    phcsBflI3P3A_ReqAParam_t                request_param = {0};
    phcsBflI3P3A_AnticollSelectParam_t      a_select_param = {0};

    uint8_t*     nfcid1t    =   NULL;
    uint8_t      nfcid1tlen =   0;
    uint8_t      nfcid1t_to_use[PHHAL_NFCID_LENGTH];

    
    if ((psHwReference == NULL) ||
       (psRemoteDevInfo == NULL))
    {
        return PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER);
    }

    phJal_Trace(psHwReference, "phJal_ReActivateIso144433BasedDevice");

    bfl_frame = (phJal_sBFLUpperFrame_t*)psHwReference->pBoardDriver;

    if (bfl_frame == NULL)
    {
        return (PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER));
    }

    bfl_status = SetTimeOut(bfl_frame->RcRegCtl, &bfl_frame->RcOpCtl, 
        ISO_14443_RF_COMMUNICATION_DELAY, USED_PRESCALER_VALUE);

    if (bfl_status != PH_ERR_BFL_SUCCESS)
    {
        return(PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_BOARD_COMMUNICATION_ERROR));
    }

    request_param.req_code  = PHCS_BFLI3P3A_REQALL;
    request_param.atq       = bfl_frame->TRxBuffer;
    request_param.self      = &bfl_frame->Iso14443_3A;
    a_select_param.self     = &bfl_frame->Iso14443_3A;

    bfl_status = bfl_frame->Iso14443_3A.RequestA(&request_param);
    
    if ((bfl_status == PH_ERR_BFL_SUCCESS) || 
        ((bfl_status & ~PH_ERR_BFL_COMP_MASK) == PH_ERR_BFL_COLLISION_ERROR))
    {
        memset(bfl_frame->TRxBuffer, 0, bfl_frame->TRxBufferSize);
        /* a_select_param.sel_lv1_code = SELECT_CASCADE_LEVEL_1;  // left here for testing/debugging */
        a_select_param.uid_length   = PHJAL_MAX_UID_LENGTH;
        a_select_param.flags        = 0;

        switch (psRemoteDevInfo->OpMode)
        {
        case phHal_eOpModesMifare:
            nfcid1t     = psRemoteDevInfo->RemoteDevInfo.CardInfo106.Startup106.NFCID1t;
            nfcid1tlen  = psRemoteDevInfo->RemoteDevInfo.CardInfo106.Startup106.NFCID1tLength;
            break;
        case phHal_eOpModesISO14443_4A:
            nfcid1t     = psRemoteDevInfo->RemoteDevInfo.ISO14443_4A_Info.Startup106.NFCID1t;
            nfcid1tlen  = psRemoteDevInfo->RemoteDevInfo.ISO14443_4A_Info.Startup106.NFCID1tLength;
            break;
        case phHal_eOpModesNfcPassive106:
            nfcid1t     = psRemoteDevInfo->RemoteDevInfo.NfcInfo106.Startup106.NFCID1t;
            nfcid1tlen  = psRemoteDevInfo->RemoteDevInfo.NfcInfo106.Startup106.NFCID1tLength;
            break;
        default:
            command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_DEVICE); 
            break;
        }

        /* Connect to Desfire BugFix (Mantis Entry 965): 
         * The opmode has to be checked for phHal_eOpModesISO14443_4A mode since its the only case where the nfcid1t has not been changed */
        if(psRemoteDevInfo->OpMode != phHal_eOpModesISO14443_4A && nfcid1tlen   !=  4)
        {
            /*  CL was more than 1 */
            /*  Let's retrieve the CT we removed from nfcid1t within phJal_InitialiseP106Startup function, and put as the last byte. */
            nfcid1t_to_use[0]   =   nfcid1t[nfcid1tlen];
            memcpy(&nfcid1t_to_use[1], nfcid1t, nfcid1tlen);
        }
        else
        {
            memcpy(&nfcid1t_to_use[0], nfcid1t, nfcid1tlen);
        }

        a_select_param.uid          = nfcid1t_to_use;
    }
    else
    {
        reportDbg1("connect:\t Reactivation with Request All failed with error 0x%02X.\n", bfl_status);
        command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_DEVICE); 
    }

    if (command_status == NFCSTATUS_SUCCESS)
    {
        bfl_status = bfl_frame->Iso14443_3A.AnticollSelect(&a_select_param);

        if (bfl_status != PH_ERR_BFL_SUCCESS)
        {
            command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_DEVICE);
        }
        else
        {
            command_status = NFCSTATUS_SUCCESS;
        }
    }
    return command_status;
}

NFCSTATUS phJal_ConnectToNfcPassive212424Device(phHal_sHwReference_t              *psHwReference,
                                                phHal_sRemoteDevInformation_t     *psRemoteDevInfo,
                                                phHal_sDevInputParam_t            *psDevInputParam)
{
    NFCSTATUS                    command_status = NFCSTATUS_SUCCESS;
    phHal_eOpModes_t             opmodes[2];
    phcsBflPolAct_PollingParam_t polling_parameter = {0};

    if ((psHwReference == NULL) ||
       (psRemoteDevInfo == NULL) ||
       (psDevInputParam == NULL))
    {
        return PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_DEVICE);
    }

    phJal_Trace(psHwReference, "phJal_ConnectToNfcPassive212424Device");

    opmodes[0] = psRemoteDevInfo->OpMode;
    opmodes[1] = phHal_eOpModesArrayTerminator;

    command_status = phJal_SetBitrateForOpMode(psHwReference, psRemoteDevInfo->OpMode);

    if (command_status == NFCSTATUS_SUCCESS)
    {
        command_status = phJal_StartupPassive212424Mode(psHwReference, psDevInputParam,
            opmodes, &polling_parameter);
    }
    else
    {
        command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_BOARD_COMMUNICATION_ERROR);
    }

    if (command_status == NFCSTATUS_SUCCESS)
    {
        command_status = 
            phJal_ConnectToNfcDevice(psHwReference, psRemoteDevInfo, psDevInputParam, PASSIVE_MODE);               
    }
    return command_status;
}

NFCSTATUS phJal_DisconnectNfcDevice(phHal_sHwReference_t              *psHwReference,
                                    phHal_sRemoteDevInformation_t     *psRemoteDevInfo)
{
    NFCSTATUS                command_status     = NFCSTATUS_SUCCESS;
    phJal_ProtocolEntry_t   *protocol_entry     = NULL;
    phJal_NfcIProtocol_t    *protocol = NULL;
    phJal_sBFLUpperFrame_t  *bfl_frame = NULL;

    if (psHwReference == NULL || 
        psRemoteDevInfo == NULL)
    {
        return (PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER));
    }

    phJal_Trace(psHwReference, "phJal_DisconnectNfcDevice");

    bfl_frame = (phJal_sBFLUpperFrame_t*)psHwReference->pBoardDriver;

    if (bfl_frame == NULL)
    {
        return (PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER));
    }

    protocol_entry =
        phJal_GetProtocolEntryForLogicalHandle(bfl_frame->NfcInitiator_Entries,
            psRemoteDevInfo->hLogHandle);
    
    if (protocol_entry != NULL)
    {
        protocol = (phJal_NfcIProtocol_t*)(protocol_entry->Protocol);
        
        if (protocol != NULL)
        {
            reportDbg1("disconnect:\t disconnect remote dev with Opmode 0x%04X\n", psRemoteDevInfo->OpMode);
            command_status = phJal_SetBitrateForOpMode(psHwReference, psRemoteDevInfo->OpMode);

            if (command_status == NFCSTATUS_SUCCESS)
            {
                command_status = 
                    phJal_DeselectNfcDevice(psHwReference, protocol->Protocol);
            }
            else
            {
                command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_BOARD_COMMUNICATION_ERROR);
            }
        }
        else
        {
            command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_DEVICE);
        }
    }
    else
    {
        command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_DEVICE);
    }

    
    return command_status;
}

NFCSTATUS phJal_DisconnectIso144434Device(phHal_sHwReference_t              *psHwReference,
                                          phHal_sRemoteDevInformation_t     *psRemoteDevInfo)
{
    NFCSTATUS                command_status = NFCSTATUS_SUCCESS;
    phJal_ProtocolEntry_t   *protocol_entry = NULL;
    phJal_sBFLUpperFrame_t  *bfl_frame = NULL;

    if (psHwReference == NULL || 
        psRemoteDevInfo == NULL)
    {
        return (PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER));
    }

    phJal_Trace(psHwReference, "phJal_DisconnectIso144434Device");

    bfl_frame = (phJal_sBFLUpperFrame_t*)psHwReference->pBoardDriver;

    if (bfl_frame == NULL)
    {
        return (PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER));
    }

    protocol_entry =
        phJal_GetProtocolEntryForLogicalHandle(bfl_frame->ISO14443_4Entries,
            psRemoteDevInfo->hLogHandle);
    
    if (protocol_entry != NULL)
    {
        command_status = phJal_SetBitrateForOpMode(psHwReference, psRemoteDevInfo->OpMode);

        if (command_status == NFCSTATUS_SUCCESS)
        {
            command_status = 
                phJal_DeselectIso144434Device(psHwReference,
                (phcsBflI3P4_t*)protocol_entry->Protocol);
        }
        else
        {
            command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_BOARD_COMMUNICATION_ERROR);
        }
    }
    else
    {
        command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_DEVICE);
    }
    return command_status;
}


#endif