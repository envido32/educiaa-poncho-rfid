/* /////////////////////////////////////////////////////////////////////////////////////////////////
//                     Copyright (c) Philips Semiconductors
//
//                       (C)PHILIPS Electronics N.V.2004
//         All rights are reserved. Reproduction in whole or in part is
//        prohibited without the written consent of the copyright owner.
//    Philips reserves the right to make changes without notice at any time.
//   Philips makes no warranty, expressed, implied or statutory, including but
//   not limited to any implied warranty of merchantability or fitness for any
//  particular purpose, or that the use will not infringe any third party patent,
//   copyright or trademark. Philips must not be liable for any loss or damage
//                            arising from its use.
///////////////////////////////////////////////////////////////////////////////////////////////// */

/*! 
 * \file phcsBflRegCtl_SpiHw1Int.h
 *
 * Project: Object Oriented Library Framework RegCtl Component for SPI on ATMega64.
 *
 *  Source: phcsBflRegCtl_SpiHw1Int.h
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Date: Thu Sep 27 09:39:20 2007 $
 *
 * Comment:
 *  None.
 *
 * History:
 *  MHa: Generated September 2005 
 *
 */


#ifndef PHCSBFLREGCTL_SPIHW1INT_H
#define PHCSBFLREGCTL_SPIHW1INT_H

#include <phcsBflRegCtl.h>


/* //////////////////////////////////////////////////////////////////////////////////////////////////
// Reg-Ctl Set Register for Joiner:
*/
/*!
* \ingroup spi
* \param[in] *setreg_param      Pointer to the parameter structure
*
* \retval  PH_ERR_BFL_SUCCESS                Operation successful.
*
* This function handles the write access to the PN51x.
*/
phcsBfl_Status_t phcsBflRegCtl_SpiHw1SetReg(phcsBflRegCtl_SetRegParam_t *setreg_param);


/* //////////////////////////////////////////////////////////////////////////////////////////////////
// Reg-Ctl Get Register for Joiner:
*/
/*!
* \ingroup spi
* \param[in,out] *getreg_param      Pointer to the parameter structure
*
* \retval  PH_ERR_BFL_SUCCESS                Operation successful.
*
* This function handles the read access to the PN51x.
*/
phcsBfl_Status_t phcsBflRegCtl_SpiHw1GetReg(phcsBflRegCtl_GetRegParam_t *getreg_param);


/* //////////////////////////////////////////////////////////////////////////////////////////////////
// Reg-Ctl Modify Register for Joiner:
*/
/*!
* \ingroup spi
* \param[in] *modify_param      Pointer to the parameter structure
*
* \retval  PH_ERR_BFL_SUCCESS                Operation successful.
*
* This function handles bit modifications on the PN51x. Therefore a JoinerRcRegCtlSpiXc167GetRegister
* command is performed before a JoinerRcRegCtlSpiXc167SetRegister. In between the modification is done.
* The bit which are 1 in the mask byte are set or cleared according to the parameter set. If set is 0, 
* the bits are cleared, otherwise they are set.
*/
phcsBfl_Status_t phcsBflRegCtl_SpiHw1ModReg(phcsBflRegCtl_ModRegParam_t *modify_param);


/* //////////////////////////////////////////////////////////////////////////////////////////////////
// Reg-Ctl Set Register Multiple for Joiner:
*/
/*!
* \ingroup spi
* \param[in] *setmultireg_param     Pointer to the parameter structure
*
* \retval  PH_ERR_BFL_SUCCESS                Operation successful.
*
* This function handles the write access for multiple data bytes from one register of the PN51x.
*/
phcsBfl_Status_t phcsBflRegCtl_SpiHw1SetMultiReg(phcsBflRegCtl_SetMultiRegParam_t *setmultireg_param);


/* //////////////////////////////////////////////////////////////////////////////////////////////////
// Reg-Ctl Get Register Multiple for Joiner:
*/
/*!
* \ingroup spi
* \param[in,out] *getmultireg_param     Pointer to the parameter structure
*
* \retval  PH_ERR_BFL_SUCCESS                Operation successful.
*
* This function handles the read access for multiple data bytes from one register from the PN51x.
*/
phcsBfl_Status_t phcsBflRegCtl_SpiHw1GetMultiReg(phcsBflRegCtl_GetMultiRegParam_t *getmultireg_param);


#endif /* PHCSBFLREGCTL_SPIHW1INT_H */

/* E.O.F. */
