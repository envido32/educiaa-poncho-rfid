/* /////////////////////////////////////////////////////////////////////////////////////////////////
//                     Copyright (c) Philips Semiconductors
//
//                       (C)PHILIPS Electronics N.V.2004
//         All rights are reserved. Reproduction in whole or in part is
//        prohibited without the written consent of the copyright owner.
//    Philips reserves the right to make changes without notice at any time.
//   Philips makes no warranty, expressed, implied or statutory, including but
//   not limited to any implied warranty of merchantability or fitness for any
//  particular purpose, or that the use will not infringe any third party patent,
//   copyright or trademark. Philips must not be liable for any loss or damage
//                            arising from its use.
///////////////////////////////////////////////////////////////////////////////////////////////// */


/*! \file FelicaReaderC.c
 *
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Date: Thu Sep 27 09:38:49 2007 $
 *
 * Comment:
 *  Example code for handling of BFL.
 *
 */


/* For debugging purposes (printf) */
#include <stdio.h>		

/* Common headers */
#include <phcsBflRefDefs.h>
#include <phcsBflHw1Reg.h>

/* Needed for any Operating Mode */
#include <phcsBflBal.h>
#include <phcsBflRegCtl.h>
#include <phcsBflIo.h>
#include <phcsBflOpCtl.h>
#include <phcsBflOpCtl_Hw1Ordinals.h>

/* Needed for this Operating Mode */
#include <phcsBflPolAct.h>

#include "ExampleUtilsC.h"
#include "ExampleGlobals.h"

/*
 * This example shows how to send a POLLING_REQ and how to receive
 * a POLLING_RES 
 */
uint16_t ActivateFelicaReader(void *comHandle)
{
	/* Declaration of RCL Component Structures (auto), C variant: */
    phcsBflBal_t            bal;
    phcsBflRegCtl_t         rc_reg_ctl;
    phcsBflIo_t             rcio;
    phcsBflOpCtl_t          rc_op_ctl;
    phcsBflPolAct_t  fp;
	/* Declaration of internal Component Structures: */
#ifdef WIN32
    phcsBflBal_Hw1SerWinInitParams_t  bal_params;
#else
    phcsBflBal_Hw1SerLinInitParams_t  bal_params;
#endif    
    phcsBflRegCtl_SerHw1Params_t      rc_reg_ctl_params;
    phcsBflIo_Hw1Params_t          rc_io_params;
    phcsBflOpCtl_Hw1Params_t          rc_op_ctl_params;
    phcsBflPolAct_Hw1Params_t         fp_params;

    /* Declaration of used parameter structures */
    /* Register control parameters */
    phcsBflRegCtl_SetRegParam_t         srp;
    phcsBflRegCtl_GetRegParam_t         grp;
    phcsBflRegCtl_ModRegParam_t         mrp;

    /* Operation control parameters */
    phcsBflOpCtl_AttribParam_t          opp;

    /* FeliCa Polling parameters */
    phcsBflPolAct_PollingParam_t        pol_p;

    /* 
	 * Declaration of variables, buffer, ... 
	 */
    /* Status variable */
    phcsBfl_Status_t     status = PH_ERR_BFL_SUCCESS;
    /* Buffer for data transfer (send/receive) */
    uint8_t	buffer[EXAMPLE_TRX_BUFFER_SIZE];
    /* Counter variables */
    uint8_t	i, index, length;
	/* Variable to calculate timeout value for timeslots */
    uint32_t           timeout;

	/* 
	 * This variable specifies the RF speed. This can be either
	 * PHCS_BFLOPCTL_VAL_RF212K for 212kbps or PHCS_BFLOPCTL_VAL_RF424K for 424kbps. 
	 */
    uint8_t opSpeed = PHCS_BFLOPCTL_VAL_RF212K;
    

	/* 
	 * Initialisation of self parameters. This is mandatory for all C implementations!
	 */
    srp.self   = &rc_reg_ctl;
    grp.self   = &rc_reg_ctl;
    mrp.self   = &rc_reg_ctl;
    opp.self   = &rc_op_ctl;
    pol_p.self = &fp;


    /* 
	 * Build up stack for serial communication. 
     *  Start with the lowest layer, so that the upper ones can refer to this already. 
	 */
#ifdef WIN32	
    bal_params.com_handle = (void*)(comHandle);
    bal_params.com_port   = 0;
    phcsBflBal_Hw1SerWinInit(&bal, &bal_params);
#else
    bal_params.com_handle = (void*)(comHandle);
    bal_params.com_port   = 0;
    phcsBflBal_Hw1SerLinInit(&bal, &bal_params);
#endif	
    /* Initialise Register Control Layer component */
    phcsBflRegCtl_SerHw1Init(&rc_reg_ctl, &rc_reg_ctl_params, &bal);
    /* Initialise RcIo component */
    phcsBflIo_Hw1Init(&rcio, &rc_io_params, &rc_reg_ctl, PHCS_BFLIO_INITIATOR); 
    /* Initialise Operation Control component */
    phcsBflOpCtl_Hw1Init(&rc_op_ctl, &rc_op_ctl_params, &rc_reg_ctl);
    /* Initialise FeliCa Polling component */
    phcsBflPolAct_Hw1Init(&fp, &fp_params, &rc_reg_ctl, PHCS_BFLPOLACT_INITIATOR);

    /* 
	 * Configure PN51x to act as a FeliCa Reader. All register which are set in other modes 
     * are configured to the right values. 
     * Remark: operation control function do not perform a softreset and does not know anything 
     *         about the antenna configuration, so these things have to be handled here!          
	 */
    opp.group_ordinal    = PHCS_BFLOPCTL_GROUP_MODE;
    opp.attrtype_ordinal = PHCS_BFLOPCTL_ATTR_PAS_POL;
    if(opSpeed == PHCS_BFLOPCTL_VAL_RF212K)
    {
        opp.param_a          = PHCS_BFLOPCTL_VAL_RF212K;
        opp.param_b          = PHCS_BFLOPCTL_VAL_RF212K;
    } else
    {
        opp.param_a          = PHCS_BFLOPCTL_VAL_RF424K;
        opp.param_b          = PHCS_BFLOPCTL_VAL_RF424K;
    }
	/* Apply the settings above */
    status = rc_op_ctl.SetAttrib(&opp);
    /* Check status and display error if something went wrong */
    if(status)
        printf("*** ERROR performing FeliCa Reader configuration! Status = %04X \n", status);

    /* Set Initiator bit of Joiner to act as a reader using register control functions. */
    srp.address = PHCS_BFL_JREG_CONTROL;
    srp.reg_data = PHCS_BFL_JBIT_INITIATOR;
    status = rc_reg_ctl.SetRegister(&srp);

    /* Set the timer to auto mode, 2s using operation control commands before HF is switched 
	* on to guarantee ECMA-340 compliance of Polling procedure for 212kbps. */

    /* 
	* Activate automatic start/stop of timer 
	*/
    opp.group_ordinal    = PHCS_BFLOPCTL_GROUP_TMR;         /* Set operation control group parameter to Timer */
    opp.attrtype_ordinal = PHCS_BFLOPCTL_ATTR_TMR_AUTO;     /* Set function for Timer automatic mode */
    opp.param_a          = PHCS_BFLOPCTL_VAL_ON;              /* Use parameter a to switch it on */
    status = rc_op_ctl.SetAttrib(&opp);
    /* Check status and display error if something went wrong */
    if(status != PH_ERR_BFL_SUCCESS)
        printf("*** ERROR! OperationControl settings Timer auto mode. Status = %04X \n", status);

    /* 
	* Set prescaler steps to 100us
	*/
	opp.group_ordinal    = PHCS_BFLOPCTL_GROUP_TMR;				/* Set operation control group parameter to Timer */
    opp.attrtype_ordinal = PHCS_BFLOPCTL_ATTR_TMR_PRESCALER;    /* Prescaler */
	/* Calculation of the values: 100us = 6.78MHz/681 = 6.78MHz/0x2A9 */
    opp.param_b          = 0x02;                        /* High value (max 0x0F) */
    opp.param_a          = 0xA9;                        /* Low value */
    status = rc_op_ctl.SetAttrib(&opp);
    /* Check status and display error if something went wrong */
    if(status != PH_ERR_BFL_SUCCESS)
        printf("*** ERROR! OperationControl settings Timer Prescaler. Status = %04X \n", status);

    /* Set the reload value */
    timeout = 20000;            /* set to 20000 for 2s (startup time definition of ECMA)  */
	opp.group_ordinal    = PHCS_BFLOPCTL_GROUP_TMR;				/* Set operation control group parameter to Timer */
    opp.attrtype_ordinal = PHCS_BFLOPCTL_ATTR_TMR_RELOAD;   
    opp.param_a          = (uint8_t)timeout;           /* Low value */
    opp.param_b          = (uint8_t)(timeout >> 8);    /* High value (max 0xFF) */
    status = rc_op_ctl.SetAttrib(&opp);
    /* Check status and display error if something went wrong */
    if(status != PH_ERR_BFL_SUCCESS)
        printf("*** ERROR! OperationControl settings Timer Reload. Status = %04X \n", status);

	/* Activate the field (automatically if there is no external field detected) */
    mrp.address = PHCS_BFL_JREG_TXAUTO;
    mrp.mask    = PHCS_BFL_JBIT_INITIALRFON | PHCS_BFL_JBIT_TX2RFAUTOEN | PHCS_BFL_JBIT_TX1RFAUTOEN;
    mrp.set     = 1;
    status = rc_reg_ctl.ModifyRegister(&mrp);

    /* After switching on the drivers wait until the timer Interrupt occures, so that 
       the field is on and the 5ms delay have been passed. This 5ms are defined in
	   the ECMA 340 standard 
	 */
    grp.address = PHCS_BFL_JREG_COMMIRQ;
    do {
        status = rc_reg_ctl.GetRegister(&grp);
    } 
    while(!(grp.reg_data & PHCS_BFL_JBIT_TIMERI) && status == PH_ERR_BFL_SUCCESS);
	/* Check status and display error if something went wrong */
    if(status != PH_ERR_BFL_SUCCESS)
        printf("*** ERROR! GetRegister: Address=%02X, Data=%02X, Status = %04X \n", 
                grp.address, grp.reg_data, status);

    /* Clear the status flag afterwards */
    srp.address  = PHCS_BFL_JREG_COMMIRQ;
    srp.reg_data = PHCS_BFL_JBIT_TIMERI;
    status = rc_reg_ctl.SetRegister(&srp);
    if(status != PH_ERR_BFL_SUCCESS)
        printf("*** ERROR! SetRegister: Address=%02X, Data=%02X, Status = %04X \n", 
                srp.address, srp.reg_data, status);

    /* Reset timer according to the timeslots used with operation control commands (AutoMode and Prescaler are the same) */
    opp.group_ordinal    = PHCS_BFLOPCTL_GROUP_TMR;         /* set operation control group parameter to Timer */
    opp.attrtype_ordinal = PHCS_BFLOPCTL_ATTR_TMR_RELOAD;   /* set to 36 (24 for Td, 12 for 1xTs) */
    opp.param_a          = 24+12;                   /* Low value */
    opp.param_b          = 0;                       /* High value (max 0x0f) */
    status = rc_op_ctl.SetAttrib(&opp);
    /* Check status and diplay error if something went wrong */
    if(status)
        printf("*** ERROR! OperationControl settings Timer Reload. Status = %04X \n", status);


    /* activate Receiver for Communication
       The RcvOff bit and the PowerDown bit are cleared, the command is not changed. */
    srp.address  = PHCS_BFL_JREG_COMMAND;
    srp.reg_data = PHCS_BFL_JCMD_NOCMDCHANGE;
    status = rc_reg_ctl.SetRegister(&srp);
    /* check status and diplay error if something went wrong */
    if(status != PH_ERR_BFL_SUCCESS)
        printf("*** ERROR! SetRegister: Address=%02X, Data=%02X, Status = %04X \n", 
                srp.address, srp.reg_data, status);

    /* 
	 * Now the timing conditions are met and the settings are appropriate for FeliCa.
	 * The commands can be configured now.
	 */

	/* 
	 * FeliCa Polling 
	 */
    
	/* Prepare data for FeliCa polling */
    buffer[0] = 0xFF;					/* Systemcode 0 */
    buffer[1] = 0xFF;					/* Systemcode 1 */
    buffer[2] = 0x00;					/* If systemcode is requested from card: 01, else: 00 */
    buffer[3] = 0x01;					/* Timeslotnumber (00, 01, 03, 07, 0F) */
    pol_p.tx_buffer      = buffer;      /* Let Polling use the initialised buffer */
    pol_p.tx_buffer_size = 4;           /* Number of bytes initialised in TxBuffer */
    pol_p.rx_buffer      = buffer + 4;  /* Let Polling use the defined return buffer */
    pol_p.rx_buffer_size = EXAMPLE_TRX_BUFFER_SIZE - 4;          /* Maximum rx buffer size */
    /* Start the POLLING_REQUEST command */
    status = fp.Polling(&pol_p);
    /* Check return code: 
     * If PH_ERR_BFL_SUCCESS is received, display IDm, PMm and Systemcode
     * else display error code. 
	 */
    if(status)
		/* Error has occured */
        printf("[E] Error in FeliCaPolling: Status = %04X \n", status);
    else
		/* No Error, display the values from the card */
    {
		/* Check whether buffer size is zero. If yes, there was no response
		   at all */
		if ( pol_p.rx_buffer_size == 0 )
		{
			printf("[I] No response.\n");
		} else
		{
			index = 0;
			while(pol_p.rx_buffer_size)
			{
				length = pol_p.rx_buffer[index];
                index++;
				printf("Length: 0x%02X\t IDm: 0x", length);
				/* Skip command byte */
				index++;
				/* Display the IDm */
				for(i=0;i<8;i++)
                {
					printf("%02X",pol_p.rx_buffer[index]);
                    index++;
                }
				printf("\tPMm: 0x");
				/* Display the PMm */
				for(i=0;i<8;i++)
                {
					printf("%02X",pol_p.rx_buffer[index]);
                    index++;
                }
				/* Display System Code if applicable, otherwise not */
				if(length == 0x14) {
					printf("\tSystemcode: 0x%02X%02X \n", pol_p.rx_buffer[index], pol_p.rx_buffer[index+1]);
					index+=2;
				} else {
					printf("\n");
				}
				pol_p.rx_buffer_size = (uint8_t)(pol_p.rx_buffer_size - length);
			}
		}
    }
    
    /* 
	 * Continue either with any FeliCa command or ATR_REQ now
	 */
	return status;
}
