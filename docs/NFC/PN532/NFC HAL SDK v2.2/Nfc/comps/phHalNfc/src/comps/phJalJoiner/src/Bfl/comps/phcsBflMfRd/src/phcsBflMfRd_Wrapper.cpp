/* /////////////////////////////////////////////////////////////////////////////////////////////////
//                     Copyright (c) Philips Semiconductors
//
//                       (C)PHILIPS Electronics N.V.2004
//         All rights are reserved. Reproduction in whole or in part is
//        prohibited without the written consent of the copyright owner.
//    Philips reserves the right to make changes without notice at any time.
//   Philips makes no warranty, expressed, implied or statutory, including but
//   not limited to any implied warranty of merchantability or fitness for any
//  particular purpose, or that the use will not infringe any third party patent,
//   copyright or trademark. Philips must not be liable for any loss or damage
//                            arising from its use.
///////////////////////////////////////////////////////////////////////////////////////////////// */

/*! \file phcsBflMfRd_Wrapper.cpp
 *
 * Project: Object Oriented Library Framework Mifare Component.
 *
 *  Source: phcsBflMfRd_Wrapper.cpp
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Date: Thu Sep 27 09:39:29 2007 $
 *
 * Comment:
 *  C++ wrapper for Mifare.
 *
 * History:
 *  GK:  Generated 29. April 2003
 *  MHa: Migrated to MoReUse September 2005
 *
 */

#include <phcsBflMfRdWrapper.hpp>

using namespace phcs_BFL;

////////////////////////////////////////////////////////////////////////////////////////////////////
// Glue:

phcsBfl_MfRdGlue::phcsBfl_MfRdGlue(void)
{
    // Initialise I/O struct members with static class members:
    m_GlueStruct.Transceive = &phcsBfl_MfRdGlue::Transceive;
    m_GlueStruct.MfAuthent  = &phcsBfl_MfRdGlue::MfAuthent;
    // We don't need the other members in the glue!
    m_GlueStruct.Receive    = NULL;
    m_GlueStruct.Transmit   = NULL;
    m_GlueStruct.mp_Members = NULL;
    m_GlueStruct.mp_Lower   = NULL;
}


phcsBfl_MfRdGlue::~phcsBfl_MfRdGlue(void)
{
    // For this implementation we don't have anything to clean up.
}


// Static class members, able to call into the lower device's C++ code again:

// Transceive and MfAthent are the only one we need for MIFARE:
phcsBfl_Status_t phcsBfl_MfRdGlue::Transceive(phcsBflIo_TransceiveParam_t *transceive_param)
{
    class phcsBfl_MfRd *mifare_wrapping_object = (class phcsBfl_MfRd*)
        (((phcsBflIo_t*)(transceive_param->self))->mp_CallingObject);
    return mifare_wrapping_object->mp_Lower->Transceive(transceive_param);
}

phcsBfl_Status_t phcsBfl_MfRdGlue::MfAuthent(phcsBflIo_MfAuthentParam_t *mfauthent_param)
{
    class phcsBfl_MfRd *mifare_wrapping_object = (class phcsBfl_MfRd*)
        (((phcsBflIo_t*)(mfauthent_param->self))->mp_CallingObject);
    return mifare_wrapping_object->mp_Lower->MfAuthent(mfauthent_param);
}


////////////////////////////////////////////////////////////////////////////////////////////////////
// Mifare Class Members:

phcsBflMfRd_Wrapper::phcsBflMfRd_Wrapper(void)
{
    m_ConstructorStatus = PH_ERR_BFL_SUCCESS;
}


phcsBflMfRd_Wrapper::phcsBflMfRd_Wrapper(phcsBfl_Io               *p_lower,
                           uint8_t       *p_trxbuffer)
{
    m_ConstructorStatus = PH_ERR_BFL_SUCCESS;
    // Hand over the pointer of the C-kernel's initialise function:
    Initialise(p_lower, p_trxbuffer);
}



phcsBflMfRd_Wrapper::~phcsBflMfRd_Wrapper(void)
{
    // No action.
}


void phcsBflMfRd_Wrapper::Initialise(phcsBfl_Io    *p_lower,
                                   uint8_t     *p_trxbuffer)
{
    // This binds the C-kernel to the C++ wrapper:
    phcsBflMfRd_Init(&m_Interface,
                   &m_CommunicationParams,
                   &(m_MifareGlue.m_GlueStruct),
                    p_trxbuffer);

    // Initialise C++ part, beyond of the scope of the C-kernel:
    mp_Lower = p_lower;
}


// Finally, the members (interface) calling into the C-kernel:
phcsBfl_Status_t phcsBflMfRd_Wrapper::Transaction(phcsBflMfRd_CommandParam_t *cmd_param)
{
    phcsBfl_Status_t   status;  
    void       *p_self; 
    
    p_self = cmd_param->self;
    cmd_param->self = &m_Interface;
    ((phcsBflIo_t*)(((phcsBflMfRd_t*)(cmd_param->self))->mp_Lower))->mp_CallingObject = this;
    status = m_Interface.Transaction(cmd_param);
    cmd_param->self = p_self;

    return status;
}


// E.O.F.
