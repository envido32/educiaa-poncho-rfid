/* /////////////////////////////////////////////////////////////////////////////////////////////////
//                     Copyright (c) Philips Semiconductors
//
//                       (C)PHILIPS Electronics N.V.2004
//         All rights are reserved. Reproduction in whole or in part is
//        prohibited without the written consent of the copyright owner.
//    Philips reserves the right to make changes without notice at any time.
//   Philips makes no warranty, expressed, implied or statutory, including but
//   not limited to any implied warranty of merchantability or fitness for any
//  particular purpose, or that the use will not infringe any third party patent,
//   copyright or trademark. Philips must not be liable for any loss or damage
//                            arising from its use.
///////////////////////////////////////////////////////////////////////////////////////////////// */

/*! 
 * \file phcsBflIoCardSimul.h
 *
 * Projekt: Object Oriented Reader Library Framework I/O component.
 *
 *  Source: phcsBflIoCardSimul.h
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Date: Thu Sep 27 09:39:13 2007 $
 *
 * Comment:
 *  None.
 *
 * History:
 *  KK:  Generated 11. Sept. 2003
 *  MHa: Migrated to MoReUse September 2005
 *
 */


#ifndef PHCSBFLIOCARDSIMUL_H
#define PHCSBFLIOCARDSIMUL_H

#include <phcsBflIo.h>


/*! \if use_html
 * Internal control variables: 
 * \endif
 */

typedef struct 
{
   uint16_t sTimeoutEtu;
} phcsBflIo_CardsimulParams_t;


/* //////////////////////////////////////////////////////////////////////////////////////////////
// I/O Initialise for Card Simulation:
*/
/*!
* \ingroup rcio
* \param[in] *cif                   C object interface structure
* \param[in] *rp                    Pointer to the internal control variables structure.
* \param[in] *p_lower               Pointer to the underlying layer, does not apply, set to NULL.
* \param[in]  initiator__not_target Initiator or Target, does not apply, set to 0.
* 
* This function shall be called first to initialise the IO component. There the C-Layer, the internal 
* variables, the underlaying layer and the device mode are initialised.
* An own function pointer is typedef'ed for this function to enable the call within
* a generic C++ I/O wrapper. 
*
*/
void          phcsBflIoCardSimul_Init(phcsBflIo_t      *cif,
                                    void           *rp,
                                    phcsBflRegCtl_t  *p_lower,
                                    uint8_t         initiator__not_target);


#endif /* PHCSBFLIOCARDSIMUL_H */

/* E.O.F. */