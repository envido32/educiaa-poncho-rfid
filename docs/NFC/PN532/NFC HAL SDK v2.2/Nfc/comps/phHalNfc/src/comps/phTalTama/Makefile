#######################################################################
#Description: Makefile to build HalNfc Component of PN53X Device
#Author     : Ravindra Upadhyaya
#Date       : 15th September 2006
#Comments   : Update the Macros only in the makefile.in files
#Revision   : 1.0
#######################################################################

########################################################################
##########- Settings that change from one Component to another -###########
########################################################################
include $(NFC_ROOT)/makefile.in
override COMP = TAL

######################################################################

######################################################################
###########- Local Variable for the working of Makefile -#############
######################################################################

######################################################################
##########-     Directories  For The Source Files     -###############
######################################################################
NFC_ROOT = 
NFC_COMMON_INC  = $(NFC_ROOT)/inc
NFC_HAL_DIR=$(NFC_ROOT)/comps/$(PREFIX)HalNfc

NFC_HAL_SRC_DIR = $(NFC_HAL_DIR)/src
NFC_HAL_INC_DIR = $(NFC_HAL_DIR)/inc
NFC_HAL_OBJ_DIR = $(NFC_HAL_DIR)/$(OBJ_DIR)

##############################################################
################ OS specific Macros ###########################
##############################################################

#Directory Separator '\' for windows '/' for unix

ifeq ($(OSYS),WIN)
DIR_SEP=\\
else
OSYS=UNIX
DIR_SEP=/
endif

WIN_RM_CMD=@erase /Q
WIN_COPY=copy
UNIX_RM_CMD=\rm -f
UNIX_COPY=\cp -f

RM= $($(OSYS)_RM_CMD)
COPY=$($(OSYS)_COPY)

#####################################################################


######################################################################
############# LOCAL PATH FOR UTILS AND DEPENDENCY FILES ##############
######################################################################
NFC_DAL_DIR = $(NFC_HAL_DIR)/src/comps/$(PREFIX)DalNfc

ifeq ($(OSYS),WIN)
NFC_DAL_SRC_DIR = $(NFC_DAL_DIR)/src/x86_nt
else
NFC_DAL_SRC_DIR = $(NFC_DAL_DIR)/src/x86_linux
endif
NFC_DAL_INC_DIR = $(NFC_DAL_DIR)/inc
NFC_DAL_OBJ_DIR = 

NFC_TAL_DIR     = $(NFC_HAL_DIR)/src/comps/$(PREFIX)TalTama
NFC_TAL_SRC_DIR = $(NFC_TAL_DIR)/src
NFC_TAL_INC_DIR = $(NFC_TAL_DIR)/inc
NFC_TAL_OBJ_DIR = $(NFC_TAL_DIR)/$(OBJ_DIR)

ifeq ($(OSYS),WIN)
NFC_OBJ_PATH = .\$(OBJ_DIR)
OBJECTS=$(NFC_OBJ_PATH)\*.o 
else
NFC_OBJ_PATH = $(NFC_$(COMP)_OBJ_DIR)
OBJECTS=$(NFC_OBJ_PATH)/*.o 
endif

# Component Dependent Include Files ###

NFC_TAL_DEP_INC += -I$(NFC_TAL_INC_DIR)
NFC_TAL_DEP_INC = $(NFC_COMMON_INC) -I$(NFC_DAL_INC_DIR)

ifneq ($(DISP),)
override DISP=display
endif

#########################################################################
######## This is done as some common file contain PHJALJOINER FLAG #######
#########################################################################


#debug_BUILD_OPT = 
#release_BUILD_OPT = 
#BUILD_OPT = $($(BUILD)_BUILD_OPT)

######################################################################


######################################################################
################# Find Component  Source Files ################
######################################################################

NFC_$(COMP)_FILES	= $(wildcard $(NFC_$(COMP)_SRC_DIR)/*.c)
NFC_$(COMP)_OBJ	= $(notdir $(NFC_$(COMP)_FILES))
NFC_$(COMP)_OBJ_FILES	= $(NFC_$(COMP)_OBJ:%.c=$(NFC_OBJ_PATH)/%.o)
#NFC_$(COMP)_OBJ_FILES	= $(NFC_$(COMP)_FILES:%.c=$(NFC_OBJ_PATH)/%.o)

##########################################################
############  Component Include Path Updates ############
##########################################################

ifeq ($(INC_DIR),)
INC_DIR = -I$(NFC_$(COMP)_INC_DIR) -I. 
endif

ifneq ($(NFC_$(COMP)_DEP_INC),)
INC_DIR += -I$(NFC_$(COMP)_DEP_INC)
endif


#######################################################################
################### Beginning of Makefile Processing   ################
#######################################################################

.PHONY: all objdir objs display clean

all: objdir objs $(DISP)


objs:$(NFC_$(COMP)_OBJ_FILES)
	@$(COPY) $(OBJECTS) $(NFC_HAL_OBJ_DIR)
	@echo "Object files created"

objdir:
ifneq ($(OSYS),UNIX)
	@if not exist $(NFC_OBJ_PATH) mkdir $(NFC_OBJ_PATH)
else
	@if ! [ -d $(NFC_OBJ_PATH) ];then mkdir -p $(NFC_OBJ_PATH); fi 
endif

display:$(NFC_$(COMP)_OBJ_FILES)
	@echo " NFC $(COMP) Source Files are $(NFC_$(COMP)_FILES)"
	@echo " NFC $(COMP) Object Files are $(NFC_$(COMP)_OBJ_FILES)"

#######################################################################
############### Component Specific Makefile Processing   ##############
#######################################################################


#######################################################################

$(NFC_OBJ_PATH)/%.o: $(NFC_$(COMP)_SRC_DIR)/%.c
	@echo "Compiling $<"
	$(CC) $(CFLAGS) $(DEFINES) $(INC_DIR) $(SRC_FLAGS)$< -o $@ >> $(NFC_LOG) 2>&1
	@echo "Compiled $<"

#######################################################################
.PHONY:clean
clean:
	@echo "Cleaning"
	@$(RM) $(OBJECTS)
	@echo "Completed Cleaning Object files"

#######################################################################

