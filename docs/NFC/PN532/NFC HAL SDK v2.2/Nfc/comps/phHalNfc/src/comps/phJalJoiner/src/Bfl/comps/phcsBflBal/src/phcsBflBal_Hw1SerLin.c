/* /////////////////////////////////////////////////////////////////////////////////////////////////
//                     Copyright (c) Philips Semiconductors
//
//                       (C)PHILIPS Electronics N.V.2004
//         All rights are reserved. Reproduction in whole or in part is
//        prohibited without the written consent of the copyright owner.
//    Philips reserves the right to make changes without notice at any time.
//   Philips makes no warranty, expressed, implied or statutory, including but
//   not limited to any implied warranty of merchantability or fitness for any
//  particular purpose, or that the use will not infringe any third party patent,
//   copyright or trademark. Philips must not be liable for any loss or damage
//                            arising from its use.
///////////////////////////////////////////////////////////////////////////////////////////////// */

/*! \file phcsBflBalHw1SerLin.c
 *
 * Project: Object Oriented Library Framework phcsBfl_Bal Component.
 *
 *  Source: phcsBflBalHw1SerLin.c
 * $Author: frq09147 $
 * $Revision: 1.1 $ 
 * $Date: Thu Sep 27 09:39:11 2007 $
 *
 * Comment:
 *  None
 *
 * History:
 *  blake: Generated 1. November 2003
 *  ct:    Adapted 12. March 2004
 *  MHa:   Migrated to MoReUse September 2005
 *  XK:    Update for HAL Linux WP February 2007
 *
 */

#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <assert.h>

#include <termios.h>
#include <sys/signal.h>
#include <sys/time.h>
#include <stdio.h>

#include <phcsBflBal.h>
#include "phcsBflBal_Hw1SerLinInt.h"

/* No Debug Prints */
#define NDEBUG

// Some PCs seem to have problems conveying the RI line to the OS
// Using CTS is probably better, but this yields touching the PCB.
// Hence by default use the RI (with the risk that you might have
// problems...
// #define CTS_HACK

#ifdef CTS_HACK
	#define EV_INTERRUPT_LINE EV_CTS
#else
	#define EV_INTERRUPT_LINE EV_RING
#endif

/* Timeout value used for serial configuration and to guarantee no deadlocks during 
   usage of the comport (ReadBus & WriteBus). */
#define RX_TIMEOUT_VAL      500

/* Internal functions: */
/* Creates file and configures new created interface */
int16_t phcsBflBal_Hw1SerLinInitPort(int8_t* aComPort, uint32_t aBaudRate);
/* Changes host data rate */
int16_t phcsBflBal_Hw1SerLinCHB(int16_t Port, uint32_t Baudrate);
/* Sets the timeouts for the com-port */
void phcsBflBal_Hw1SerLinSetCPT(int16_t aHandle);


/* Externally accessible functions: */

void phcsBflBal_Hw1SerLinInit(phcsBflBal_t  *cif,
                              void          *comm_params)
{
    phcsBflBal_Hw1SerLinInitParams_t *bp = comm_params;

    cif->mp_Members     = comm_params;
    cif->ReadBus        = phcsBflBal_Hw1SerLinReadBus;
    cif->WriteBus       = phcsBflBal_Hw1SerLinWriteBus;
    cif->ChangeHostBaud = phcsBflBal_Hw1SerLinHostBaud;
    cif->ClosePort      = phcsBflBal_Hw1SerLinClosePort;
    
    /* check com_handle and open serial interface if 0 */
    if(bp->com_handle == 0)
    {
        bp->com_handle = phcsBflBal_Hw1SerLinInitPort((int8_t *)bp->com_port, 9600);
    }
}

phcsBfl_Status_t phcsBflBal_Hw1SerLinWriteBus(phcsBflBal_WriteBusParam_t *writebus_param)
{
    #define             BUFFER_LENGTH                     1 /* */
    const uint8_t 	    bytes_to_send                   = BUFFER_LENGTH;
    uint8_t             buffer[BUFFER_LENGTH];
    int32_t            bytes_sent                      = 0;
    phcsBfl_Status_t    status                          = PH_ERR_BFL_SUCCESS;
    int16_t dwTmp = -1;
    fd_set writefs;
    struct timeval tv;

    tv.tv_usec = RX_TIMEOUT_VAL * 1000;
    tv.tv_sec = 0;
    
    phcsBflBal_Hw1SerLinInitParams_t *bp = ((phcsBflBal_t*)(writebus_param->self))->mp_Members;

    buffer[0] = (uint8_t)(writebus_param->bus_data);

    FD_ZERO(&writefs);
    FD_SET((int32_t)bp->com_handle, &writefs);
    /*  Attach file descriptor for Write operation completion */
    dwTmp = select((int32_t)bp->com_handle + 1, NULL, &writefs, NULL, &tv);

    /*  Error in communication */
    if (dwTmp < 0)
    {
        /*  failure of the operation */
#ifndef NDEBUG
        printf("[E] serial write returns errno 0x%x\n",errno);
#endif /* NDEBUG */
        bytes_sent = 0;  //no data written
        if (errno == EAGAIN)
        {
#ifndef NDEBUG
            printf("[E] no data can be written: serial buffer full\n");
#endif /* NDEBUG */
        }
    }
    else
    {
        if(dwTmp == 0)
        {
            /*  timeout: no data written */
            bytes_sent = 0;
        }
        else
        {
            if (FD_ISSET((int32_t)bp->com_handle, &writefs))
            {
                /* write operation slection */
                bytes_sent = write((int32_t)bp->com_handle,&buffer[0],bytes_to_send);       
            }
            if (bytes_sent != bytes_to_send)
            {
                status = PH_ERR_BFL_INTERFACE_ERROR;
            }
        }
    }
    return status;
    #undef BUFFER_LENGTH 
}


phcsBfl_Status_t phcsBflBal_Hw1SerLinReadBus(phcsBflBal_WriteBusParam_t *readbus_param)
{

    #define             BUFFER_LENGTH             1 /* */
    const uint8_t       bytes_to_read           = BUFFER_LENGTH;
    phcsBfl_Status_t      status                  = PH_ERR_BFL_SUCCESS;
    uint8_t             buffer[BUFFER_LENGTH];
    uint32_t            bytes_read              = 0;
    phcsBflBal_Hw1SerLinInitParams_t *bp           = (phcsBflBal_Hw1SerLinInitParams_t*)(((phcsBflBal_t*)(readbus_param->self))->mp_Members);
    int16_t dwTmp = -1;
    fd_set readfs;
    struct timeval tv;

    tv.tv_usec = RX_TIMEOUT_VAL * 1000;
    tv.tv_sec = 0;

    FD_ZERO(&readfs);
    FD_SET((int32_t)bp->com_handle, &readfs);
    /*  Attach file descriptor for Write operation completion */
    dwTmp = select((int32_t)bp->com_handle + 1, &readfs, NULL, NULL, &tv);

    /*  Error in communication */
    if (dwTmp < 0)
    {
        /*  failure of the operation */
#ifndef NDEBUG
        printf("[E] serial read returns errno 0x%x\n",errno);
#endif /* NDEBUG */
        bytes_read = 0;  //no data written
        if (errno == EAGAIN)
        {
#ifndef NDEBUG
            printf("[E] no data can be written: serial buffer full\n");
#endif /* NDEBUG */
        }
    }
    else
    {
        if(dwTmp == 0)
        {
            /*  timeout: no data written */
            bytes_read = 0;       
        }
        else
        {
            if (FD_ISSET((int32_t)bp->com_handle, &readfs))
            {
                /* read operation slection */
                FD_CLR((int32_t)bp->com_handle, &readfs);
                bytes_read = read((int32_t)bp->com_handle,&buffer[0],bytes_to_read);       
            }
            if (bytes_read != bytes_to_read)
            {
                status = PH_ERR_BFL_INTERFACE_ERROR;
            }
        }
    }

    if(status == PH_ERR_BFL_SUCCESS)
				{
        readbus_param->bus_data = (uint8_t)(buffer[0]);
				
        if (bytes_read != 0) 
        {
            /* OK: No further action req'd. */
								}
								else 
        {
            status = PH_ERR_BFL_INTERFACE_ERROR;
        }				
				}
    PHCS_BFL_ADD_COMPCODE(status, PH_ERR_BFL_BAL);
    return status;
#undef BUFFER_LENGTH 
}

void phcsBflBal_Hw1SerLinHostBaud(phcsBflBal_HostBaudParam_t *hostbaud_param)
{
    phcsBflBal_Hw1SerLinInitParams_t *bp = ((phcsBflBal_t*)(hostbaud_param->self))->mp_Members;

    phcsBflBal_Hw1SerLinCHB(bp->com_handle, hostbaud_param->baudrate);
    //phcsBflBal_Hw1SerLinSetCPT(bp->com_handle);
}


/* Internal functions */

int16_t phcsBflBal_Hw1SerLinInitPort(int8_t* aComPort, uint32_t aBaudRate)
{
    int16_t handle;
  
    /* check if errornous input (both handle and comport 0) */
    if (aComPort == NULL) 
    {
        handle = NULL;
        return handle;
    }

    handle = open(aComPort, O_RDWR | /*O_NOCTTY |*/ O_NONBLOCK);              /* Try user input depending on port */

    if (handle < 0) 
    {   // Error
        handle = NULL;
    } else
	{   
        /* Open OK - apply default settings to the port */
        phcsBflBal_Hw1SerLinCHB( handle, aBaudRate);
    }

	return handle;
}

/* 
 * Changes host baudrate and defines other important values (parity and
 * the like.
 */
int16_t phcsBflBal_Hw1SerLinCHB(int16_t Port, uint32_t Baudrate)
{
  int16_t result = -1;
  struct termios newtio;
  int8_t parity = 'N';

  // set timeout
  memset(&newtio, 0, sizeof(newtio));

  /* get current options */
  result = tcgetattr(Port, &newtio);
  switch (Baudrate) 
  {
	case 9600:                                               /* Baudrate 9600          */
		newtio.c_cflag = B9600;
		break;
	case 19200:                                              /* Baudrate 19200         */
		newtio.c_cflag = B19200;
		break;
	case 115200:                                              /* Baudrate 115200         */
		newtio.c_cflag = B115200;
		break;
	default:
		close(Port);
		return -1;
  }

  	/* Set the number of bits per character */
	newtio.c_cflag |= CS8;

  /*
   * Set the parity (Odd Even None)
   */
  switch (parity) 
  {

  case 'O':                                              /* Odd Parity               */
    newtio.c_cflag |= PARODD | PARENB | INPCK;
    break;

  case 'E':                                              /* Even Parity             */
    newtio.c_cflag &= (~PARODD);
    newtio.c_cflag |= PARENB | INPCK;
    break;

  case 'N':                                              /* No Parity               */
    break;

  default:
    close(Port);
    return -1;
  }

  /*
   * Setting Raw Input and Defaults
   */

  newtio.c_iflag &= ~(IGNPAR|PARMRK|INLCR|IGNCR|ICRNL|ISTRIP|IUCLC|IXANY|IXOFF|IXON);
  newtio.c_iflag |= BRKINT|INPCK;
  
  newtio.c_oflag  = 0;
  
  newtio.c_cflag |= CREAD|HUPCL|CLOCAL;
  
  newtio.c_lflag &= ~(ICANON|ECHO|ISTRIP);
  
  newtio.c_lflag  = 0;

  newtio.c_cc[VMIN]  = 1;
  newtio.c_cc[VTIME] = 0;

  if (tcflush(Port, TCIFLUSH) < 0)            /* Flush the serial port            */
  {
    close(Port);
    return -1;
  }

  if (tcsetattr(Port, TCSANOW, &newtio) < 0)  /* Set the parameters                */
  {
    close(Port);
    return -1;
  }
}

/*
 * Specify the timeout values
 */
void phcsBflBal_Hw1SerLinSetCPT(int16_t aHandle)
{
    int16_t result = -1;
    struct termios options;

    memset(&options, 0, sizeof(options));

    /* get current options */
    result = tcgetattr(aHandle, &options);
    if(result != -1)
    {
        options.c_cc[VMIN] = 1;	/* blocking to 1 byte */

        options.c_cc[VTIME] = 0;	/* timeout en 0.1s */
//        options.c_cc[VTIME] = RX_TIMEOUT_VAL;	/* timeout en 0.1s */

        /* Apply the settings */
        if (tcsetattr(aHandle, TCSANOW, &options) < 0)
        {
#ifndef NDEBUG
	    printf("tcsetattr fails\n");
#endif /* NDEBUG */
            result = -1 ;
        }
    }
}

/*
 * Closes the port via aquired handle
 */
void phcsBflBal_Hw1SerLinClosePort(phcsBflBal_ClosePortParam_t *closeport_param)
{
    phcsBflBal_Hw1SerLinInitParams_t *bp = ((phcsBflBal_t*)(closeport_param->self))->mp_Members;

    if (bp->com_handle != 0)
    {
        close(bp->com_handle);
    }
}

