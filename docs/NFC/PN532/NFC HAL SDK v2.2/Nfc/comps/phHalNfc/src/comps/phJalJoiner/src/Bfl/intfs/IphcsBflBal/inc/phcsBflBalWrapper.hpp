/* /////////////////////////////////////////////////////////////////////////////////////////////////
//                     Copyright (c) Philips Semiconductors
//
//                       (C)PHILIPS Electronics N.V.2004
//         All rights are reserved. Reproduction in whole or in part is
//        prohibited without the written consent of the copyright owner.
//    Philips reserves the right to make changes without notice at any time.
//   Philips makes no warranty, expressed, implied or statutory, including but
//   not limited to any implied warranty of merchantability or fitness for any
//  particular purpose, or that the use will not infringe any third party patent,
//   copyright or trademark. Philips must not be liable for any loss or damage
//                            arising from its use.
///////////////////////////////////////////////////////////////////////////////////////////////// */


/*! 
 * \if cond_generate_filedocu 
 * \file phcsBflBalWrapper.hpp
 *
 * Project: Object Oriented Library Framework phcsBfl_Bal Component.
 *
 *  Source: phcsBflBalWrapper.hpp
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Date: Thu Sep 27 09:38:56 2007 $
 *
 * Comment:
 *  C++ wrapper for phcsBfl_Bal.
 *
 * History:
 *  GK:  Generated 12. March 2003
 *  MHa: Migrated to MoReUse September 2005
 * \endif
 *
 */

#ifndef PHCSBFLBALWRAPPER_HPP
#define PHCSBFLBALWRAPPER_HPP

#include <phcsBflStatus.h>

// Include the meta-header file for all hardware variants supported by the C++ interface:
extern "C"
{
    #include <phcsBflBal.h>
}

namespace phcs_BFL
{

/*! \cond cond_Cpp_complete */
/***************************************************************************************************
* B A L
************************************************************************************************* */
/* \ingroup bal
 *  phcsBfl_Bal (Bus Abstraction Layer) abstract class, used for the C++ interface.
 */
class phcsBfl_Bal
{
    public:
        virtual phcsBfl_Status_t WriteBus(phcsBflBal_WriteBusParam_t *writebus_param)           = 0;
        virtual phcsBfl_Status_t ReadBus(phcsBflBal_ReadBusParam_t *readbus_param)              = 0;
        virtual void             ChangeHostBaud(phcsBflBal_HostBaudParam_t *hostbaud_param)     = 0;
        virtual void             ClosePort(phcsBflBal_ClosePortParam_t *closeport_param)        = 0;
        
};



// Upper layers define a Glue class, in order to enable the kernel (C) to call into C++ again, when
// a next lower object's functionality is required. Since the phcsBfl_Bal is the lowest instance, no Glue
// class is required.

/*
*  Function pointer definition for phcsBflBal_t Initialise:
*/
typedef void (*pphcsBflBal_Initialize_t) (phcsBflBal_t*, void*);
/*! \endcond */



/*! \ingroup bal
*  This is the main interface of Bal. The class is a template because it cannot be foreseen
*  which type of C-kernel to wrap around. Depending on the implementation of this kernel, the
*  type of the internal C-member structure changes (avoid multiple definitions of the same structure
*  name for different structures when more than one type of functionality is used). The type of the
*  MEMBERS (in kernel's .h file) structure is the one to specify when creating an instance
*  of this class. This structure is referenced by the mp_Members pointer in the C-structure
*  interface of a Component.
*  
*/
template <class phcsBflBcp> class phcsBflBal_Wrapper : public phcsBfl_Bal
{
    public:
        /* Constructor without initialisation */
        phcsBflBal_Wrapper(void);

        /*
        * \param[in]  c_bal_initialise 
        *                  Pointer to the Initialise function of the type of C-kernel (for
        *                  each hardware variant an own kernel must exist) to use with the
        *                  current wrapper instance.
        * \param[in,out] communication_parameters
        *                  Paramters for comport (input) and handle (input/output).
        *
        * \note The alternative Constructor does setup required by the Component but not done
        *       by the default constructor.
        *  
        */
        phcsBflBal_Wrapper(pphcsBflBal_Initialize_t  c_bal_initialise,
                           void                     *communication_parameters);
        ~phcsBflBal_Wrapper(void);
        
        /*!
        * \param[in]  c_bal_initialise 
        *                  Pointer to the Initialise function of the type of C-kernel (for
        *                  each hardware variant an own kernel must exist) to use with the
        *                  current wrapper instance.
        * \param[in,out] communication_parameters
        *                  Paramters for comport (input) and handle (input/output).
        *
        * \note The Initialise function does setup required by the Component but not done
        *       by the default constructor.
        *  
        */
        void Initialise(pphcsBflBal_Initialize_t  c_bal_initialise,
                        void                     *communication_parameters);

        phcsBfl_Status_t WriteBus(phcsBflBal_WriteBusParam_t *writebus_param);
        phcsBfl_Status_t ReadBus(phcsBflBal_ReadBusParam_t *readbus_param);
        void ChangeHostBaud(phcsBflBal_HostBaudParam_t *hostbaud_param);
        void ClosePort(phcsBflBal_ClosePortParam_t *closeport_param);

        /*! \brief Constructor status: This status gives information about whether the object has been
        *  allocated/configured correctly or not. It is a good idea to check this status (verify
        *  that it is set to PH_ERR_BFL_SUCCESS) before proceeding.
        */
        phcsBfl_Status_t m_ConstructorStatus;
        phcsBflBal_t     m_Interface;                /*!< \brief C-kernel interface structure instance. */
};


////////////////////////////////////////////////////////////////////////////////////////////////////
// Implementation (requ'd to be in the header when using templates)
////////////////////////////////////////////////////////////////////////////////////////////////////


/* \ingroup bal
*  Template definition for phcsBfl_Bal Constructor without initialisation. 
*/
template <class phcsBflBcp> \
phcsBflBal_Wrapper<phcsBflBcp>::phcsBflBal_Wrapper(void)
{
    m_ConstructorStatus = PH_ERR_BFL_SUCCESS;
}


/* \ingroup bal
*  Template definition for phcsBfl_Bal Constructor with initialisation. 
*/
template <class phcsBflBcp> \
phcsBflBal_Wrapper<phcsBflBcp>::phcsBflBal_Wrapper(pphcsBflBal_Initialize_t  c_bal_initialise,
                                                   void                     *communication_parameters)
{
    m_ConstructorStatus = PH_ERR_BFL_SUCCESS;
    // Hand over the pointer of the C-kernel's initialise function:
    this->Initialise(c_bal_initialise, communication_parameters);
}


/* \ingroup bal
*  Template definition for phcsBfl_Bal Destructor. 
*/
template <class phcsBflBcp> \
phcsBflBal_Wrapper<phcsBflBcp>::~phcsBflBal_Wrapper(void)
{
    // No action req'd.
}


/* \ingroup bal
*  Template definition for phcsBfl_Bal Initialise function.
*/
template <class phcsBflBcp> \
void phcsBflBal_Wrapper<phcsBflBcp>::Initialise(pphcsBflBal_Initialize_t  c_bal_initialise,
                                                void                     *communication_parameters)
{
    // This binds the C-kernel to the C++ wrapper:
    c_bal_initialise(&m_Interface,
                     communication_parameters);
}


// Finally, the members (interface) calling into the C-kernel:

/* \ingroup bal
*  \param[in] *writebus_param  Pointer to the I/O parameter structure.
*  \return                     phcsBfl_Status_t value depending on the C implementation
*
*  Template definition for Member function WriteBus.
*/
template <class phcsBflBcp> \
phcsBfl_Status_t phcsBflBal_Wrapper<phcsBflBcp>::WriteBus(phcsBflBal_WriteBusParam_t *writebus_param)
{
    phcsBfl_Status_t   status;
    void       *p_self;

    p_self = writebus_param->self;
    writebus_param->self = &m_Interface;
    status = m_Interface.WriteBus(writebus_param);
    writebus_param->self = p_self;

    return status;
}


/* \ingroup bal
*  \param[in] *readbus_param   Pointer to the I/O parameter structure.
*  \return                     phcsBfl_Status_t value depending on the C implementation
*
*  Template definition for Member function ReadBus.
*/
template <class phcsBflBcp> \
phcsBfl_Status_t phcsBflBal_Wrapper<phcsBflBcp>::ReadBus(phcsBflBal_ReadBusParam_t *readbus_param)
{
    phcsBfl_Status_t   status;
    void       *p_self;

    p_self = readbus_param->self;
    readbus_param->self = &m_Interface;
    status = m_Interface.ReadBus(readbus_param);
    readbus_param->self = p_self;

    return status;
}


/* \ingroup bal
*  \param[in] *hostbaud_param  Pointer to the I/O parameter structure.
*
*  Template definition for Member function ChangeHostBaud.
*/
template <class phcsBflBcp> \
void phcsBflBal_Wrapper<phcsBflBcp>::ChangeHostBaud(phcsBflBal_HostBaudParam_t *hostbaud_param)
{
    void              *p_self;

    p_self = hostbaud_param->self;
    hostbaud_param->self = &m_Interface;
    m_Interface.ChangeHostBaud(hostbaud_param);
    hostbaud_param->self = p_self;
}


/* \ingroup bal
*  \param[in] *hostbaud_param  Pointer to the I/O parameter structure.
*
*  Template definition for Member function ChangeHostBaud.
*/
template <class phcsBflBcp> \
void phcsBflBal_Wrapper<phcsBflBcp>::ClosePort(phcsBflBal_ClosePortParam_t *closeport_param)
{
    void              *p_self;

    p_self = closeport_param->self;
    closeport_param->self = &m_Interface;
    m_Interface.ClosePort(closeport_param);
    closeport_param->self = p_self;
}

}   /* phcs_BFL */
    
#endif /* PHCSBFLBALWRAPPER_HPP */
