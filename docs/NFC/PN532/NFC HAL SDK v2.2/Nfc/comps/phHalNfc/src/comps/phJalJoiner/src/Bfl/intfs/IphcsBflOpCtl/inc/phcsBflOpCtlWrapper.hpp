/* /////////////////////////////////////////////////////////////////////////////////////////////////
//                     Copyright (c) Philips Semiconductors
//
//                       (C)PHILIPS Electronics N.V.2004
//         All rights are reserved. Reproduction in whole or in part is
//        prohibited without the written consent of the copyright owner.
//    Philips reserves the right to make changes without notice at any time.
//   Philips makes no warranty, expressed, implied or statutory, including but
//   not limited to any implied warranty of merchantability or fitness for any
//  particular purpose, or that the use will not infringe any third party patent,
//   copyright or trademark. Philips must not be liable for any loss or damage
//                            arising from its use.
///////////////////////////////////////////////////////////////////////////////////////////////// */

/*! 
 * \if cond_generate_filedocu 
 * \file phcsBflOpCtlWrapper.hpp
 *
 * Project: Object Oriented Library Framework OperationControl Component.
 *
 *  Source: phcsBflOpCtlWrapper.hpp
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Date: Thu Sep 27 09:39:06 2007 $
 *
 * Comment:
 *  C++ wrapper for OperationControl.
 *
 * History:
 *  MHa: Generated 13. May 2003
 *  MHa: Migrated to MoReUse September 2005
 * \endif
 *
 */

#ifndef PHCSBFLOPCTLWRAPPER_HPP
#define PHCSBFLOPCTLWRAPPER_HPP

#include <phcsBflRegCtlWrapper.hpp>

/* Include the meta-header file for all hardware variants supported by the C++ interface: */
extern "C"
{
    #include <phcsBflOpCtl.h>
    #include <phcsBflOpCtl.h>
}

namespace phcs_BFL
{

/*! \cond cond_Cpp_complete */
/* \ingroup opctl
 *  \brief 
 *  Basic Configuration functionality of the RF frontend
 *  (abstract class, used for the C++ interface).
 */
class phcsBfl_OpCtl
{
    public:
        virtual void SetWaitEventCb(phcsBflOpCtl_SetWecParam_t *set_wec_param)           = 0;

        virtual phcsBfl_Status_t SetAttrib(phcsBflOpCtl_AttribParam_t *attrib_param)                     = 0;
        virtual phcsBfl_Status_t GetAttrib(phcsBflOpCtl_AttribParam_t *attrib_param)                     = 0;

        /*! \brief Lower edge interface: Not to be redefined in the derived class: */
        class phcsBfl_RegCtl *mp_Lower;
};



/*
 * Function pointer definition for C-OperationControl Initialise function: The C function must
 * stick to the pointer definition.
 */
typedef void (*pphcsBflOpCtl_Initialize_t) (phcsBflOpCtl_t*, void*, phcsBflRegCtl_t*);


/* \ingroup opctl
 *  This is the GLUE class the C-kernel OperationControl "sees" when calling down the stack. The 
 *  glue class represents the C-API of the lower layer (RC Reg. Ctl.). */
class phcsBfl_OpCtlGlue
{
    public:
        phcsBfl_OpCtlGlue(void);
        ~phcsBfl_OpCtlGlue(void);
        
        //! \name Static functions SetRegister, able to call into C++ again.
        //@{
        static phcsBfl_Status_t SetRegister(phcsBflRegCtl_SetRegParam_t *setreg_param);
        static phcsBfl_Status_t GetRegister(phcsBflRegCtl_GetRegParam_t *getreg_param);
        static phcsBfl_Status_t ModifyRegister(phcsBflRegCtl_ModRegParam_t *modify_param);
        static phcsBfl_Status_t SetRegisterMultiple(phcsBflRegCtl_SetMultiRegParam_t *setmultireg_param);
        static phcsBfl_Status_t GetRegisterMultiple(phcsBflRegCtl_GetMultiRegParam_t *getmultireg_param);
        //@}

        //! Structure accessible from the upper object's pure C-kernel.
        phcsBflRegCtl_t m_GlueStruct;
};
/*! \endcond */


/*! \ingroup opctl
 *  This is the main interface of OperationControl. The class is a template because it cannot be 
 *  foreseen which type of C-kernel to wrap around. Depending on the implementation of this kernel, the
 *  type of the internal C-member structure changes (avoid multiple definitions of the same structure
 *  name for different structures when more than one type of functionality is used). The type of the
 *  MEMBERS (in kernel's .h file) structure is the one to specify when creating an instance
 *  of this class.
 */
template <class phcsBflOCcp> class phcsBflOpCtl_Wrapper : public phcsBfl_OpCtl
{
    public:
        phcsBflOpCtl_Wrapper(void);
        ~phcsBflOpCtl_Wrapper(void);

        /*
        * \ingroup opctl
        * \param[in]  c_rc_op_ctl_initialise 
        *                  Pointer to the Initialise function of the type of C-kernel (for
        *                  each hardware variant an own kernel must exist) to use with the
        *                  current wrapper instance.
        * \param[in] *p_lower 
        *                  Pointer to the C++ object of the underlying layer I/O.
        *
        * \note The alternative Constructor does setup required by the Component but not done
        *       by the default constructor.
        *  
        */
        phcsBflOpCtl_Wrapper(pphcsBflOpCtl_Initialize_t  c_rc_op_ctl_initialise,
                           phcsBfl_RegCtl             *p_lower);

        /*!
        * \ingroup opctl
        * \param[in]  c_rc_op_ctl_initialise
        *                  Pointer to the Initialise function of the type of C-kernel (for
        *                  each hardware variant an own kernel must exist) to use with the
        *                  current wrapper instance.
        * \param[in] *p_lower
        *                  Pointer to the C++ object of the underlying layer I/O.
        *
        * \note The Initialise function does setup required by the Component but not done
        *       by the default constructor.
        *  
        */
        void Initialise(pphcsBflOpCtl_Initialize_t  c_rc_op_ctl_initialise,
                        phcsBfl_RegCtl             *p_lower);

        phcsBfl_Status_t SetAttrib(phcsBflOpCtl_AttribParam_t *attrib_param);
        phcsBfl_Status_t GetAttrib(phcsBflOpCtl_AttribParam_t *attrib_param);
        void SetWaitEventCb(phcsBflOpCtl_SetWecParam_t *set_wec_param);

        /*! \brief Constructor status: This status gives information about whether the object has been
         *  allocated/configured correctly or not. It is a good idea to check this status (verify
         *  that it is set to PH_ERR_BFL_SUCCESS) before proceeding.
         */
        phcsBfl_Status_t          m_ConstructorStatus;
        phcsBflOpCtl_t            m_Interface;              /*!< \brief C-kernel instance. */
        phcsBflOCcp               m_CommunicationParams;    /*!< \brief C-kernel internal variables. */

    protected:
        class phcsBfl_OpCtlGlue   m_RcOpCtlGlue;      /*!< \brief Glue class for connecting the C-part
                                                       *   with the C++ wrapper.
                                                       */
};


////////////////////////////////////////////////////////////////////////////////////////////////////
// phcsBflOpCtl_Wrapper Wrapper Implementation, must reside here because of template usage:
////////////////////////////////////////////////////////////////////////////////////////////////////

/* \ingroup opctl
 * Template definition for phcsBflOpCtl_Wrapper Constructor without initialisation. */
template <class phcsBflOCcp>
phcsBflOpCtl_Wrapper<phcsBflOCcp>::phcsBflOpCtl_Wrapper(void)
{
    m_ConstructorStatus = PH_ERR_BFL_SUCCESS;
    // This constructor does no initialisation. Initialise() is needed.
}


/* \ingroup opctl
 * Template definition for phcsBflOpCtl_Wrapper Constructor with initialisation. */
template <class phcsBflOCcp>
phcsBflOpCtl_Wrapper<phcsBflOCcp>::phcsBflOpCtl_Wrapper(pphcsBflOpCtl_Initialize_t  c_rc_op_ctl_initialise,
                                                  phcsBfl_RegCtl             *p_lower)
{
    m_ConstructorStatus = PH_ERR_BFL_SUCCESS;
    // Hand over the pointer of the C-kernel's initialise function:
    this->Initialise(c_rc_op_ctl_initialise, p_lower);
}


/* \ingroup opctl
 * Template definition for phcsBflOpCtl_Wrapper Destructor. */
template <class phcsBflOCcp>
phcsBflOpCtl_Wrapper<phcsBflOCcp>::~phcsBflOpCtl_Wrapper(void)
{
    // No action.
}


/* \ingroup opctl
 * Template definition for phcsBflOpCtl_Wrapper Initialisation function. */
template <class phcsBflOCcp>
void phcsBflOpCtl_Wrapper<phcsBflOCcp>::Initialise(pphcsBflOpCtl_Initialize_t  c_rc_op_ctl_initialise, 
                                               phcsBfl_RegCtl             *p_lower)
{
    // This binds the C-kernel to the C++ wrapper:
    c_rc_op_ctl_initialise(&m_Interface,
                           &m_CommunicationParams,
                           &(m_RcOpCtlGlue.m_GlueStruct));

    // Initialise C++ part, beyond of the scope of the C-kernel:
    this->mp_Lower = p_lower;
}


/* \ingroup opctl
 * Template definition for SetEventCb function. */
template <class phcsBflOCcp>
void phcsBflOpCtl_Wrapper<phcsBflOCcp>::SetWaitEventCb(phcsBflOpCtl_SetWecParam_t *set_wec_param)
{
    void *p_self;
    p_self = set_wec_param->self;
    set_wec_param->self = &m_Interface;
    m_Interface.SetWaitEventCb(set_wec_param);
    set_wec_param->self = p_self;
}


// Finally, the members (interface) calling into the C-kernel:

/* \ingroup opctl
 *  \param[in] *attrib_param    Pointer to the I/O parameter structure
 *  \return                     phcsBfl_Status_t value depending on the C implementation
 *
 * Template definition for Member function SetAttrib. */
template <class phcsBflOCcp>
phcsBfl_Status_t phcsBflOpCtl_Wrapper<phcsBflOCcp>::SetAttrib(phcsBflOpCtl_AttribParam_t *attrib_param)
{
    phcsBfl_Status_t   status;
    void       *p_self;

    p_self = attrib_param->self;
    attrib_param->self = &m_Interface;
    ((phcsBflRegCtl_t*)(((phcsBflOpCtl_t*)(attrib_param->self))->mp_Lower))->mp_CallingObject = this;
    status = m_Interface.SetAttrib(attrib_param);
    attrib_param->self = p_self;

    return status;
}


/* \ingroup opctl
 *  \param[in] *attrib_param    Pointer to the I/O parameter structure
 *  \return                     phcsBfl_Status_t value depending on the C implementation
 *
 * Template definition for Member function GetAttrib. */
template <class phcsBflOCcp>
phcsBfl_Status_t phcsBflOpCtl_Wrapper<phcsBflOCcp>::GetAttrib(phcsBflOpCtl_AttribParam_t *attrib_param)
{
    phcsBfl_Status_t   status;
    void       *p_self;

    p_self = attrib_param->self;
    attrib_param->self = &m_Interface;
    ((phcsBflRegCtl_t*)(((phcsBflOpCtl_t*)(attrib_param->self))->mp_Lower))->mp_CallingObject = this;
    status = m_Interface.GetAttrib(attrib_param);
    attrib_param->self = p_self;

    return status;
}

}   /* phcs_BFL */

#endif /* PHCSBFLOPCTLWRAPPER_HPP */

/* E.O.F. */
