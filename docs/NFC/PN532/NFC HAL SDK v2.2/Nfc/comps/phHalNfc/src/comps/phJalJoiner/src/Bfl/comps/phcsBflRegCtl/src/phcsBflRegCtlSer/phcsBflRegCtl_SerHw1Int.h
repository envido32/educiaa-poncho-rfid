/* /////////////////////////////////////////////////////////////////////////////////////////////////
//                     Copyright (c) NXP Semiconductors
//
//                       (C)NXP Electronics N.V.2004
//         All rights are reserved. Reproduction in whole or in part is
//        prohibited without the written consent of the copyright owner.
//    NXP reserves the right to make changes without notice at any time.
//   NXP makes no warranty, expressed, implied or statutory, including but
//   not limited to any implied warranty of merchantability or fitness for any
//  particular purpose, or that the use will not infringe any third party patent,
//   copyright or trademark. NXP must not be liable for any loss or damage
//                            arising from its use.
///////////////////////////////////////////////////////////////////////////////////////////////// */

/*! 
 * \file phcsBflRegCtl_SerHw1Int.h
 *
 * Project: Object Oriented Library Framework RegCtl Component for Rs232 on PC.
 *
 *  Source: phcsBflRegCtl_SerHw1Int.h
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Date: Thu Sep 27 09:39:20 2007 $
 *
 * Comment:
 *  None.
 *
 * History:
 *  GK: Generated September 2005
 *
 */


#ifndef PHCSBFLREGCTL_SERHW1INT_H
#define PHCSBFLREGCTL_SERHW1INT_H

#include <phcsBflRegCtl.h>


/* //////////////////////////////////////////////////////////////////////////////////////////////////
// Reg-Ctl Set Register for Joiner:
*/
/*!
* \ingroup rs232
* \param[in] *setreg_param  Pointer to the parameter structure
*
* \retval  PH_ERR_BFL_SUCCESS                Operation successful.
* \retval  PH_ERR_BFL_INTERFACE_ERROR        Error on input parameters.
* \retval  Other                            phcsBfl_Status_t values depending on the underlying components.
*
* This function handles the write access to the PN51x according to the hardware specification for 
* a serial (RS232) interface.
*/
phcsBfl_Status_t phcsBflRegCtl_SerHw1SetReg(phcsBflRegCtl_SetRegParam_t *setreg_param);


/* //////////////////////////////////////////////////////////////////////////////////////////////////
// Reg-Ctl Get Register for Joiner:
*/
/*!
* \ingroup rs232
* \param[in,out] *getreg_param  Pointer to the parameter structure
*
* \retval  PH_ERR_BFL_SUCCESS                Operation successful.
* \retval  Other                            phcsBfl_Status_t values depending on the underlying components.
*
* This function handles the read access to the PN51x according to the hardware specification for 
* a serial (RS232) interface.
* \note Reading from the serial interface does not generate an error because data is read from an 
*       internal buffer. Therefore an error might not be detected and lead to missinterpretation.
*/
phcsBfl_Status_t phcsBflRegCtl_SerHw1GetReg(phcsBflRegCtl_GetRegParam_t *getreg_param);


/* //////////////////////////////////////////////////////////////////////////////////////////////////
// Reg-Ctl Modify Register for Joiner:
*/
/*!
* \ingroup rs232
* \param[in] *modify_param      Pointer to the parameter structure
*
* \retval  PH_ERR_BFL_SUCCESS                Operation successful.
* \retval  PH_ERR_BFL_INTERFACE_ERROR        Error on input parameters.
* \retval  Other                            phcsBfl_Status_t values depending on the underlying components.
*
* This function handles bit modifications on the PN51x. Therefore a JoinerRcRegCtlRs232PcGetRegister
* command is performed before a JoinerRcRegCtlRs232PcSetRegister command. In between the modification is done.
* The bit which are 1 in the mask byte are set or cleared according to the parameter set. If set is 0, 
* the bits are cleared, otherwise they are set.
* \note Be aware that some register content may change in between read and write. Fot these regsters use an
*       apropriate way of bitwise modification.
*/
phcsBfl_Status_t phcsBflRegCtl_SerHw1ModReg(phcsBflRegCtl_ModRegParam_t *modify_param);


/* //////////////////////////////////////////////////////////////////////////////////////////////////
// Reg-Ctl Set Register Multiple for Joiner:
*/
/*!
* \ingroup rs232
* \param[in] *setmultireg_param     Pointer to the parameter structure
*
* \retval  PH_ERR_BFL_SUCCESS                Operation successful.
* \retval  PH_ERR_BFL_INTERFACE_ERROR        Error on input parameters.
* \retval  Other                            phcsBfl_Status_t values depending on the underlying components.
*
* This function handles the write access of more than one data byte to the PN51x. The address is the
* same for all data bytes. This function is for example used to fill up the FIFO. \n
* 
*/
phcsBfl_Status_t phcsBflRegCtl_SerHw1SetMultiReg(phcsBflRegCtl_SetMultiRegParam_t *setmultireg_param);


/* //////////////////////////////////////////////////////////////////////////////////////////////////
// Reg-Ctl Get Register Multiple for Joiner:
*/
/*!
* \ingroup rs232
* \param[in,out] *getmultireg_param     Pointer to the parameter structure
*
* \retval  PH_ERR_BFL_SUCCESS                Operation successful.
* \retval  Other                            phcsBfl_Status_t values depending on the underlying components.
*
* This function handles the read access of multiple bytes from one register from the PN51x. The address
* read from is the same for all data bytes. This function is for example used to get the FIFO content.
* \note Reading from the serial interface does not generate an error because data is read from an 
*       internal buffer. Therefore an error might not be detected and lead to missinterpretation.
*/
phcsBfl_Status_t phcsBflRegCtl_SerHw1GetMultiReg(phcsBflRegCtl_GetMultiRegParam_t *getmultireg_param);


#endif /* PHCSBFLREGCTL_SERHW1INT_H */
