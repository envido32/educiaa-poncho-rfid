/*
 *         Copyright (c), NXP Semiconductors
 *
 *                       (C)NXP Electronics N.V.2004
 *         All rights are reserved. Reproduction in whole or in part is
 *        prohibited without the written consent of the copyright owner.
 *    NXP reserves the right to make changes without notice at any time.
 *   NXP makes no warranty, expressed, implied or statutory, including but
 *   not limited to any implied warranty of merchantability or fitness for any
 *  particular purpose, or that the use will not infringe any third party patent,
 *   copyright or trademark. NXP must not be liable for any loss or damage
 *                            arising from its use.
 *
 */


/*!
 * \file phJalTargetSendReceiveFunctions.c
 * \brief JAL functions for Target Mode handling
 *
 *
 * Project: NFC MW / HAL
 *
 * $Date: Thu Sep 27 09:38:43 2007 $ 
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Aliases: SDK_HAL_2.2,SDK_HAL_2.2_071008,NFC_FRI_SDK_1.0_071031 $
 *
 */

#ifdef PHHAL_VERSION_ACQUISITION
    #define PHJALTARGETSENDRECEIVEFUNCTIONS_SOURCE_FILEREVISION "$Revision: 1.1 $"
    #define PHJALTAARGETSENDRECEIVEFUNCTIONS_SOURCE_FILEALIASES  "$Aliases: SDK_HAL_2.2,SDK_HAL_2.2_071008,NFC_FRI_SDK_1.0_071031 $"
#else /* PHHAL_VERSION_ACQUISITION */


#include <string.h>

#include <phJalTargetSendReceiveFunctions.h>
#include <phJalTargetModeFunctions.h>
#include <phJalDebugReportFunctions.h>

#include <phNfcStatus.h>
#include <phOsalNfc.h>

NFCSTATUS phJal_ReceiveDataFromEndpoint(phJal_sBFLUpperFrame_t      *bfl_frame,
                                        phHal_sDepAdditionalInfo_t  *psDepAdditionalInfo,
                                        uint8_t                     *pRecvBuf,
                                        uint16_t                    *pRecvLength)
{
    NFCSTATUS   command_status = NFCSTATUS_SUCCESS;
    uint32_t    data_state = NO_STATE;
    uint16_t    current_data_index = 0;
    uint8_t     user_buffer_overflow = 0;

    phJal_Trace(bfl_frame->ReceptionHandlingParams.psHwReference, "phJal_ReceiveDataFromEndpoint");
    
    if (bfl_frame->ReceptionHandlingParams.MetaChainingActive == 1)
    {
        reportDbg0("receive func:\t signal to be ready for further chaining\n");
        bfl_frame->ReceptionHandlingParams.ReceptionUserReadyForMetaChaining = USER_READY;
        phOsalNfc_ProduceSemaphore(bfl_frame->ReceptionUserReadyForMetaChaining);
    }

    do
    {
        reportDbg0("receive func:\t wait for data event\n");

        /* WaitForSemaphoreProtectedEvent Function Change -  ReceptionEndpointToUserHandle */

        //command_status = WaitForSemaphoreProtectedEvent(bfl_frame->ReceptionEndpointToUserHandle, 
        //    &bfl_frame->ReceptionHandlingParams.ReceptionEndpointToUserState,
        //    DATA_RECEIVED|RECEPTION_TIMEOUT|MORE_DATA_AVAILABLE|TERMINATE_RECEPTION|TARGET_RELEASED_NOW, 
        //    &data_state,
        //    WAIT_INFINITE);
        
        phOsalNfc_ConsumeSemaphore(bfl_frame->ReceptionEndpointToUserHandle);
        data_state = bfl_frame->ReceptionHandlingParams.ReceptionEndpointToUserState;
        bfl_frame->ReceptionHandlingParams.ReceptionEndpointToUserState = NO_STATE;

        if (command_status == NFCSTATUS_SUCCESS)
        {           
            if (data_state == DATA_RECEIVED)
            {               
                reportDbg1("receive func:\t %d data bytes received\n",
                    bfl_frame->ReceptionHandlingParams.UserDataLength);
                if (bfl_frame->ReceptionHandlingParams.UserDataLength <= *pRecvLength)
                {
                    memcpy(&pRecvBuf[current_data_index], bfl_frame->ReceptionHandlingParams.pUserData,
                        bfl_frame->ReceptionHandlingParams.UserDataLength);
                    current_data_index = current_data_index + bfl_frame->ReceptionHandlingParams.UserDataLength;
                    *pRecvLength = current_data_index;
                    bfl_frame->ReceptionHandlingParams.ReceptionUserToEndpointState = USER_READY;           
                }
                else
                {
                    bfl_frame->ReceptionHandlingParams.ReceptionUserToEndpointState = USER_BUFFER_TOO_SMALL;
                    reportDbg0("receive func:\t user buffer too small\n");
                    user_buffer_overflow = 1;
                    *pRecvLength = 0;
                    command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_BUFFER_TOO_SMALL);
                }
                phOsalNfc_ProduceSemaphore(bfl_frame->ReceptionUserToEndpointHandle);
            }
            else if (data_state == MORE_DATA_AVAILABLE)
            {
                 reportDbg1("receive func:\t %d data bytes received and more available\n",
                    bfl_frame->ReceptionHandlingParams.UserDataLength);

                if (bfl_frame->ReceptionHandlingParams.UserDataLength + current_data_index <= *pRecvLength)
                {                    
                    memcpy(&pRecvBuf[current_data_index], bfl_frame->ReceptionHandlingParams.pUserData,
                        bfl_frame->ReceptionHandlingParams.UserDataLength);
                    current_data_index = current_data_index + bfl_frame->ReceptionHandlingParams.UserDataLength;
                  
                    if (current_data_index + PHJAL_TARGET_BUFFER_SIZE > *pRecvLength)
                    {
                        command_status = phOsalNfc_ConsumeSemaphore(bfl_frame->ReceptionUserReadyForMetaChaining);
                        bfl_frame->ReceptionHandlingParams.ReceptionUserToEndpointState = USER_META_CHAINING;
                        psDepAdditionalInfo->DepFlags.MetaChaining = 1;
                        bfl_frame->ReceptionHandlingParams.MetaChainingActive = 1;
                        user_buffer_overflow = 1;
                        command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_MORE_INFORMATION);
                        reportDbg0("receive func:\t user buffer overflow detected\n");
                       *pRecvLength = current_data_index;
                    }
                    else
                    {
                        psDepAdditionalInfo->DepFlags.MetaChaining = 1;
                        bfl_frame->ReceptionHandlingParams.ReceptionUserToEndpointState = USER_READY;
                        reportDbg0("receive func:\t waiting for more data\n");
                    }
                    phOsalNfc_ProduceSemaphore(bfl_frame->ReceptionUserToEndpointHandle);                    
                }
                else
                {   
                    command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_BUFFER_TOO_SMALL);
                    reportDbg0("receive func:\t user buffer too small\n");
                    user_buffer_overflow = 1;
                    *pRecvLength = 0;
                    bfl_frame->ReceptionHandlingParams.ReceptionUserToEndpointState = 
                        USER_BUFFER_TOO_SMALL;
                    phOsalNfc_ProduceSemaphore(bfl_frame->ReceptionUserToEndpointHandle);                    
                }
            }
            else if (data_state == RECEPTION_TIMEOUT)
	        {
                reportDbg0("receive func:\t reception timeout\n");
		        command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_RF_TIMEOUT);
	        }
            else if (data_state == TARGET_DESELECTED)
            {
                reportDbg0("receive func:\t target deselected\n");
                command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_DESELECTED);
            }
            else if (data_state == TARGET_RELEASED)
            {
                reportDbg0("receive func:\t target released\n");
                command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_RELEASED);
            }
            else
            {
                reportDbg0("receive func:\t other state\n");
                command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_RF_TIMEOUT);
            }
        }
        else
        {
            command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_CMD_ABORTED);
        }

    }while ((data_state == MORE_DATA_AVAILABLE) && (user_buffer_overflow == 0));

    /* Clear Meta-Chaining flag in case that there is no meta-chaining: */
    if (command_status != PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_MORE_INFORMATION))
    {
        psDepAdditionalInfo->DepFlags.MetaChaining = 0;
    }

    if ((command_status != NFCSTATUS_SUCCESS) && 
        (command_status != PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_MORE_INFORMATION)))
    {
        /* no data has been received */
        *pRecvLength = 0;
    }
    return command_status;
}

NFCSTATUS phJal_SendDataToEndpoint(phJal_sBFLUpperFrame_t      *bfl_frame,
                                   phHal_sDepAdditionalInfo_t  *psDepAdditionalInfo,
                                   uint8_t                     *pSendBuf,
                                   uint16_t                    SendLength)
{
	NFCSTATUS   command_status = NFCSTATUS_SUCCESS;
    uint32_t    data_state = NO_STATE;
    uint16_t    current_data_index = 0;
	uint16_t    current_send_length = 0;

	phJal_Trace(bfl_frame->ReceptionHandlingParams.psHwReference, "phJal_SendDataToEndpoint");
    
    if (SendLength <= PHJAL_TARGET_BUFFER_SIZE)
	{
		if (command_status == NFCSTATUS_SUCCESS)
		{
			/* all data fits into target buffer - no cha�ning needed */
            reportDbg1("send func:\t prepare %d data bytes to be sent\n", SendLength);
			bfl_frame->ReceptionHandlingParams.pUserData = pSendBuf;
			bfl_frame->ReceptionHandlingParams.UserDataLength = SendLength;			
            bfl_frame->ReceptionHandlingParams.TransmissionUserToEndpointState = 
                (psDepAdditionalInfo->DepFlags.MetaChaining == 1)? USER_META_CHAINING : USER_READY;
			
            command_status = phOsalNfc_ProduceSemaphore(bfl_frame->TransmissionUserToEndpointHandle);
			if (command_status == NFCSTATUS_SUCCESS)
			{
                /* Change Of WFSPE function - TransmissionEndpointToUserHandle
                 *      This semaphore is always consumed in the phJal_SendDataToEndpoint function 
                 *      which used the WFSPE function. The semaphore blocks there and will be produced
                 *      in the phJal_DispatchTargetCommand function. So, all other places where this
                 *      Semaphore is consumed are commented out.
                 *
                 *      When creating the semaphore, the initial value is also set to O in order to
                 *      block the first time.
                 */
				//command_status = 
                //      WaitForSemaphoreProtectedEvent(bfl_frame->TransmissionEndpointToUserHandle, 
				//	    &bfl_frame->ReceptionHandlingParams.TransmissionEndpointToUserState,
				//	    USER_DATA_SENT | USER_ATR_SENT | RECEPTION_TIMEOUT | 
                //      TRANSMISSION_ERROR | TERMINATE_RECEPTION,
				//	    &data_state,
				//	    WAIT_INFINITE);

                command_status = phOsalNfc_ConsumeSemaphore(bfl_frame->TransmissionEndpointToUserHandle);
                data_state = bfl_frame->ReceptionHandlingParams.TransmissionEndpointToUserState;
                bfl_frame->ReceptionHandlingParams.TransmissionEndpointToUserState = NO_STATE;

				if (command_status == NFCSTATUS_SUCCESS)
				{
					if ((data_state == RECEPTION_TIMEOUT) || 
                        (data_state == TRANSMISSION_ERROR) ||
                        (data_state == TERMINATE_RECEPTION))
					{   
						command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_RF_TIMEOUT);
					}
				}
			}
		}
	}
	else
	{
        /* data doesn't fit into target buffer thus chaining is applied */
        reportDbg0("send func:\t user data bytes have to be chained\n");
		while(current_data_index < SendLength)
		{
			if (command_status == NFCSTATUS_SUCCESS)
			{
				if((current_data_index + PHJAL_TARGET_BUFFER_SIZE) < SendLength)
				{
					current_send_length = PHJAL_TARGET_BUFFER_SIZE;
					bfl_frame->ReceptionHandlingParams.TransmissionUserToEndpointState = MORE_DATA_AVAILABLE;
				}
				else
				{
					current_send_length = SendLength - current_data_index;
					bfl_frame->ReceptionHandlingParams.TransmissionUserToEndpointState = 
                        (psDepAdditionalInfo->DepFlags.MetaChaining == 1)? USER_META_CHAINING : USER_READY;
				}

				bfl_frame->ReceptionHandlingParams.pUserData = &pSendBuf[current_data_index];
				bfl_frame->ReceptionHandlingParams.UserDataLength = current_send_length;
				
				command_status = phOsalNfc_ProduceSemaphore(bfl_frame->TransmissionUserToEndpointHandle);				
				if (command_status == NFCSTATUS_SUCCESS)
				{
                    reportDbg0("send func:\t wait for endpoint transmission confirmation\n");

                    /* Change Of WFSPE function - TransmissionEndpointToUserHandle
                     *      This semaphore is always consumed in the phJal_SendDataToEndpoint function 
                     *      which used the WFSPE function. The semaphore blocks there and will be produced
                     *      in the phJal_DispatchTargetCommand function. So, all other places where this
                     *      Semaphore is consumed are commented out.
                     *
                     *      When creating the semaphore, the initial value is also set to O in order to
                     *      block the first time.
                     */
					//command_status = WaitForSemaphoreProtectedEvent(bfl_frame->TransmissionEndpointToUserHandle, 
					//	&bfl_frame->ReceptionHandlingParams.TransmissionEndpointToUserState,
					//	USER_DATA_SENT | RECEPTION_TIMEOUT | TERMINATE_RECEPTION,
					//	&data_state,
					//	WAIT_INFINITE);

                    command_status = phOsalNfc_ConsumeSemaphore(bfl_frame->TransmissionEndpointToUserHandle);
                    data_state = bfl_frame->ReceptionHandlingParams.TransmissionEndpointToUserState;
                    bfl_frame->ReceptionHandlingParams.TransmissionEndpointToUserState = NO_STATE;

					if (command_status == NFCSTATUS_SUCCESS)
					{
						if (data_state == USER_DATA_SENT)
						{							
						    current_data_index = current_data_index + current_send_length;
						}
						else
						{
							command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_RF_TIMEOUT);
							break;
						}
					}
				}
			}
		}
	}
	return command_status;
}

#endif