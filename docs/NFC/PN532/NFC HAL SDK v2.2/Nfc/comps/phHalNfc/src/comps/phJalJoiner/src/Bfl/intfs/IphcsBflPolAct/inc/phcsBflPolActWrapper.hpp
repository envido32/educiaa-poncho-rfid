/* /////////////////////////////////////////////////////////////////////////////////////////////////
//                     Copyright (c) Philips Semiconductors
//
//                       (C)PHILIPS Electronics N.V.2004
//         All rights are reserved. Reproduction in whole or in part is
//        prohibited without the written consent of the copyright owner.
//    Philips reserves the right to make changes without notice at any time.
//   Philips makes no warranty, expressed, implied or statutory, including but
//   not limited to any implied warranty of merchantability or fitness for any
//  particular purpose, or that the use will not infringe any third party patent,
//   copyright or trademark. Philips must not be liable for any loss or damage
//                            arising from its use.
///////////////////////////////////////////////////////////////////////////////////////////////// */

/*! 
 * \if cond_generate_filedocu 
 * \file phcsBflPolActWrapper.hpp
 *
 * Project: Object Oriented Library Framework FeliCa Polling Component.
 *
 *  Source: phcsBflPolActWrapper.hpp
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Date: Thu Sep 27 09:38:58 2007 $
 *
 * Comment:
 *  C++ wrapper for FeliCaPolling.
 *
 * History:
 *  MHa: Generated 07. May 2003
 *  MHa: Migrated to MoReUse September 2005
 * \endif
 *
 */

#ifndef PHCSBFLPOLACTWRAPPER_HPP
#define PHCSBFLPOLACTWRAPPER_HPP

#include <phcsBflRegCtlWrapper.hpp>

// Include the meta-header file for all hardware variants supported by the C++ interface:
extern "C"
{
    #include <phcsBflPolAct.h>
    #include <phcsBflPolAct.h>
}

namespace phcs_BFL
{

/*! \cond cond_Cpp_complete */
/* \ingroup felpol
 *  FeliCa activation functionality ("Polling"), abstract class, used for the C++ interface.
 */
class phcsBfl_PolAct
{
    public:
        virtual void SetWaitEventCb(phcsBflPolAct_SetWecParam_t *set_wec_param)             = 0;

        virtual phcsBfl_Status_t Polling(phcsBflPolAct_PollingParam_t *polling_param)         = 0;

        /*! Lower edge interface: Not to be redefined in the derived class: */
        class phcsBfl_RegCtl *mp_Lower;
};


// Function pointer definition for C- phcsBflPolAct_Wrapper Initialise function:
typedef void (*pphcsBflPolAct_Initialize_t) (phcsBflPolAct_t*, void*, phcsBflRegCtl_t*, uint8_t);


/* \ingroup felpol
 *  This is the GLUE class the C-kernel Felica Polling "sees" when calling down the stack. The glue class
 *  represents the C-API of the lower layer (RC Reg. Ctl.).
 */
class phcsBfl_PolActGlue
{
    public:
        phcsBfl_PolActGlue(void);
        ~phcsBfl_PolActGlue(void);
        
        //! \name Static function to be able to call into C++ again.
        //@{
        static phcsBfl_Status_t SetRegister(phcsBflRegCtl_SetRegParam_t *setreg_param);
        static phcsBfl_Status_t GetRegister(phcsBflRegCtl_GetRegParam_t *getreg_param);
        static phcsBfl_Status_t ModifyRegister(phcsBflRegCtl_ModRegParam_t *modify_param);
        static phcsBfl_Status_t SetRegisterMultiple(phcsBflRegCtl_SetMultiRegParam_t *setmultireg_param);
        static phcsBfl_Status_t GetRegisterMultiple(phcsBflRegCtl_GetMultiRegParam_t *getmultireg_param);
        //@}

        //! Structure accessible from the upper object's pure C-kernel.
        phcsBflRegCtl_t m_GlueStruct;
};
/*! \endcond */


/*! \ingroup felpol
 *  This is the main interface of phcsBflPolAct_Wrapper. The class is a template because it cannot be foreseen
 *  which type of C-kernel to wrap around. Depending on the implementation of this kernel, the
 *  type of the internal C-member structure changes (avoid multiple definitions of the same structure
 *  name for different structures when more than one type of functionality is used). The type of the
 *  MEMBERS (in kernel's .h file) structure is the one to specify when creating an instance
 *  of this class.
 */
template <class phcsBflFPcp> class phcsBflPolAct_Wrapper : public phcsBfl_PolAct
{
    public:
        phcsBflPolAct_Wrapper(void);
        ~phcsBflPolAct_Wrapper(void);

        /*
        * \param[in]  c_polling_initialise 
        *                  Pointer to the Initialise function of the type of C-kernel (for
        *                  each hardware variant an own kernel must exist) to use with the
        *                  current wrapper instance.
        * \param[in] *p_lower 
        *                  Pointer to the C++ object of the underlying layer RcRegCtl.
        * \param[in]  initiator__not_target
        *                  Specifier for operation mode (1 = Initiator, 0 = Target).
        *
        * \note The alternative Constructor does setup required by the Component but not done
        *       by the default constructor.
        */
        phcsBflPolAct_Wrapper(pphcsBflPolAct_Initialize_t   c_polling_initialise,
                            phcsBfl_RegCtl               *p_lower,
                            uint8_t                     initiator__not_target);

        /*!
        * \param[in]  c_polling_initialise
        *                  Pointer to the Initialise function of the type of C-kernel (for
        *                  each hardware variant an own kernel must exist) to use with the
        *                  current wrapper instance.
        * \param[in] *p_lower 
        *                  Pointer to the C++ object of the underlying layer RcRegCtl.
        * \param[in]  initiator__not_target
        *                  Specifier for operation mode (1 = Initiator, 0 = Target).
        *
        * \note The Initialise method does setup required by the Component but not done
        *       by the default constructor.
        */
        void Initialise(pphcsBflPolAct_Initialize_t  c_polling_initialise,
                        phcsBfl_RegCtl              *p_lower,
                        uint8_t                    initiator__not_target);


        phcsBfl_Status_t Polling(phcsBflPolAct_PollingParam_t *polling_param);
        void SetWaitEventCb(phcsBflPolAct_SetWecParam_t *set_wec_param);


        /*! \brief Constructor status: This status gives information about whether the object has been
         *  allocated/configured correctly or not. It is a good idea to check this status (verify
         *  that it is set to PH_ERR_BFL_SUCCESS) before proceeding.
         */
        phcsBfl_Status_t            m_ConstructorStatus;
        phcsBflPolAct_t             m_Interface;           /*!< \brief C-kernel interface. */
        phcsBflFPcp                 m_CommunicationParams; /*!< \brief C-kernel internal variables. */

    protected:
        //! \brief Glue class, connecting the C-kernel with the underlying C++ feature.
        class phcsBfl_PolActGlue    m_FelicaPollingGlue;
};


////////////////////////////////////////////////////////////////////////////////////////////////////
// phcsBflPolAct_Wrapper Wrapper Implementation, must reside here because of template usage:
////////////////////////////////////////////////////////////////////////////////////////////////////


/* \ingroup felpol
 * Template definition for phcsBflPolAct_Wrapper Constructor without initialisation. */
template <class phcsBflFPcp>
phcsBflPolAct_Wrapper<phcsBflFPcp>::phcsBflPolAct_Wrapper(void)
{
    // This constructor does no initialisation. Initialise() is needed.
    m_ConstructorStatus = PH_ERR_BFL_SUCCESS;
}


/* \ingroup felpol
 * Template definition for phcsBflPolAct_Wrapper Constructor with initialisation. */
template <class phcsBflFPcp>
phcsBflPolAct_Wrapper<phcsBflFPcp>::phcsBflPolAct_Wrapper(pphcsBflPolAct_Initialize_t  c_polling_initialise,
                                                    phcsBfl_RegCtl              *p_lower,
                                                    uint8_t                    initiator__not_target)
{
    m_ConstructorStatus = PH_ERR_BFL_SUCCESS;
    // Hand over the pointer of the C-kernel's initialise function:
    this->Initialise(c_polling_initialise, p_lower, initiator__not_target);
}


/* \ingroup felpol
 * Template definition for phcsBflPolAct_Wrapper Destructor. */
template <class phcsBflFPcp>
phcsBflPolAct_Wrapper<phcsBflFPcp>::~phcsBflPolAct_Wrapper(void)
{
    // No action req'd.
}


/* \ingroup felpol
 * Template definition for phcsBflPolAct_Wrapper Initialisation function. */
template <class phcsBflFPcp>
void phcsBflPolAct_Wrapper<phcsBflFPcp>::Initialise(pphcsBflPolAct_Initialize_t  c_polling_initialise,
                                                phcsBfl_RegCtl              *p_lower,
                                                uint8_t                    initiator__not_target)
{
    // This binds the C-kernel to the C++ wrapper:
    c_polling_initialise(&m_Interface,
                         &m_CommunicationParams,
                         &(m_FelicaPollingGlue.m_GlueStruct),
                          initiator__not_target);

    // Initialise C++ part, beyond of the scope of the C-kernel:
    this->mp_Lower  = p_lower;
}


/* \ingroup felpol
 * Template definition for SetEventCb function. */
template <class phcsBflFPcp>
void phcsBflPolAct_Wrapper<phcsBflFPcp>::SetWaitEventCb(phcsBflPolAct_SetWecParam_t *set_wec_param)
{
    void *p_self;
    p_self = set_wec_param->self;
    set_wec_param->self = &m_Interface;
    m_Interface.SetWaitEventCb(set_wec_param);
    set_wec_param->self = p_self;
}


// Finally, the members (interface) calling into the C-kernel:

/* \ingroup felpol
 *  \param[in] *polling_param   Pointer to the I/O parameter structure
 *  \return                     phcsBfl_Status_t value depending on the C implementation
 *
 * Template definition for Member function Polling. */
template <class phcsBflFPcp>
phcsBfl_Status_t phcsBflPolAct_Wrapper<phcsBflFPcp>::Polling(phcsBflPolAct_PollingParam_t *polling_param)
{
    phcsBfl_Status_t   status;  
    void       *p_self; 
    
    p_self = polling_param->self;
    polling_param->self = &m_Interface;
    ((phcsBflRegCtl_t*)(((phcsBflPolAct_t*)(polling_param->self))->mp_Lower))->mp_CallingObject = this;
    status = m_Interface.Polling(polling_param);
    polling_param->self = p_self;

    return status;
}

}   /* phcs_BFL */

#endif /* PHCSBFLPOLACTWRAPPER_HPP */

/* E.O.F. */
