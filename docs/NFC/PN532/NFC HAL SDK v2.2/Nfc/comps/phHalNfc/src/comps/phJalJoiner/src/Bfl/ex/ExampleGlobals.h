/* /////////////////////////////////////////////////////////////////////////////////////////////////
//                     Copyright (c) Philips Semiconductors
//
//                       (C)PHILIPS Electronics N.V.2005
//         All rights are reserved. Reproduction in whole or in part is
//        prohibited without the written consent of the copyright owner.
//    Philips reserves the right to make changes without notice at any time.
//   Philips makes no warranty, expressed, implied or statutory, including but
//   not limited to any implied warranty of merchantability or fitness for any
//  particular purpose, or that the use will not infringe any third party patent,
//   copyright or trademark. Philips must not be liable for any loss or damage
//                            arising from its use.
///////////////////////////////////////////////////////////////////////////////////////////////// */

/*! \file ExampleGlobals.h
 *
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Date: Thu Sep 27 09:38:48 2007 $
 *
 */

#ifndef __EXAMPLE_GLOBALS_H__
#define __EXAMPLE_GLOBALS_H__

#ifdef __cplusplus
extern "C" {
#endif

#define EXAMPLE_TARGET_BUFFER_SIZE    80    
#define EXAMPLE_USER_BUFFER_SIZE      128    
#define EXAMPLE_TRX_BUFFER_SIZE       64      

	
extern const int8_t			gHelpString[];

#ifdef __cplusplus
}
#endif

#endif /* __EXAMPLE_GLOBALS_H__ */
