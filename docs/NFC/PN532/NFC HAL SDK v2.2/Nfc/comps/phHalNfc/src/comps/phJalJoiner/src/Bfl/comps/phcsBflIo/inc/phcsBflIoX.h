/* /////////////////////////////////////////////////////////////////////////////////////////////////
//                     Copyright (c) Philips Semiconductors
//
//                       (C)PHILIPS Electronics N.V.2004
//         All rights are reserved. Reproduction in whole or in part is
//        prohibited without the written consent of the copyright owner.
//    Philips reserves the right to make changes without notice at any time.
//   Philips makes no warranty, expressed, implied or statutory, including but
//   not limited to any implied warranty of merchantability or fitness for any
//  particular purpose, or that the use will not infringe any third party patent,
//   copyright or trademark. Philips must not be liable for any loss or damage
//                            arising from its use.
///////////////////////////////////////////////////////////////////////////////////////////////// */

/*! \file phcsBflIoX.h
 *
 * Project: 
 *
 *  Source: phcsBflIoX.h
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Date: Thu Sep 27 09:39:13 2007 $
 * 
 * Comment:
 *  None.
 *
 * History:
 *  MHa: Migrated to MoReUse September 2005
 *
 */


//#ifndef PHCSBFLIOX_H
//#define PHCSBFLIOX_H
//
//
//#include <phcsBflIo.h>
//
//// Should be done in a better way... this is just a workaround
//#ifdef WIN32
//#include <windows.h>
//#define PHCS_BFLIOX_HANDLE  HANDLE
//#else
//#define PHCS_BFLIOX_HANDLE void* 
//#endif
//
///*! \ingroup rcio
// *  Internal control variables: 
// */
//typedef struct 
//{
//    PHCS_BFLIOX_HANDLE  hEventArray[3];
//    uint8_t             m_InitiatorNotTarget; 
//
//}phcsBflIo_XParams_t;
//
//
//
///* //////////////////////////////////////////////////////////////////////////////////////////////
//// I/O Initialise for Simulation:
//*/
///*!
//* \ingroup rcio
//* \param[in] *cif                   C object interface structure
//* \param[in] *rp                    Pointer to the internal control variables structure.
//* \param[in] *p_lower               Pointer to the underlying layer, in this case no underlaying layer exists.
//* \param[in]  initiator__not_target Specifier for mode whether to support Initiator or Target.
//* 
//* This function shall be called first to initialise the IO component. There the C-Layer, the internal 
//* variables, the underlaying layer and the device mode are initialised.
//*
//*/
//void          phcsBflIoX_Init(phcsBflIo_t      *cif,
//                            void           *rp,
//                            phcsBflRegCtl_t  *p_lower,
//                            uint8_t         initiator__not_target);
//
//
//#endif /* PHCSBFLIOX_H */
//
///* E.O.F. */