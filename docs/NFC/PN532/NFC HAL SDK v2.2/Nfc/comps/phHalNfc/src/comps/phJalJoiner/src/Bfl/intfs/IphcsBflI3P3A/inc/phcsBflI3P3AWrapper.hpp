/* /////////////////////////////////////////////////////////////////////////////////////////////////
//                     Copyright (c) Philips Semiconductors
//
//                       (C)PHILIPS Electronics N.V.2004
//         All rights are reserved. Reproduction in whole or in part is
//        prohibited without the written consent of the copyright owner.
//    Philips reserves the right to make changes without notice at any time.
//   Philips makes no warranty, expressed, implied or statutory, including but
//   not limited to any implied warranty of merchantability or fitness for any
//  particular purpose, or that the use will not infringe any third party patent,
//   copyright or trademark. Philips must not be liable for any loss or damage
//                            arising from its use.
///////////////////////////////////////////////////////////////////////////////////////////////// */

/*! 
 * \if cond_generate_filedocu 
 * \file phcsBflI3P3AWrapper.hpp
 *
 * Project: Object Oriented Library Framework phcsBfl_I3P3A Component.
 *
 *  Source: phcsBflI3P3AWrapper.hpp
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Date: Thu Sep 27 09:38:53 2007 $
 *
 * Comment:
 *  C++ wrapper for phcsBfl_I3P3A.
 *
 * History:
 *  MHa: Generated 13. May 2003
 *  MHa: Migrated to MoReUse September 2005
 * \endif
 *
 */

#ifndef PHCSBFLI3P3AWRAPPER_HPP
#define PHCSBFLI3P3AWRAPPER_HPP

#include <phcsBflRegCtlWrapper.hpp>

// Include the meta-header file for all hardware variants supported by the C++ interface:
extern "C"
{
    #include <phcsBflI3P3A.h>
    #include <phcsBflI3P3A.h>
}

namespace phcs_BFL
{

/*! \cond cond_Cpp_complete */
/* \ingroup iso14443_3
 *  Functionality (activation/deactivation) according to ISO 14443-3 specification
 *  (abstract class, used for the C++ interface).
 */
class phcsBfl_I3P3A
{
    public:
        virtual void SetWaitEventCb(phcsBflI3P3A_SetWecParam_t *set_wec_param)        = 0;

        virtual phcsBfl_Status_t RequestA(phcsBflI3P3A_ReqAParam_t *request_a_param)    = 0;
        virtual phcsBfl_Status_t AnticollSelect(phcsBflI3P3A_AnticollSelectParam_t
                                                *AnticollSelect_param)              = 0;
        virtual phcsBfl_Status_t Select(phcsBflI3P3A_SelectParam_t *select_param)       = 0;
        virtual phcsBfl_Status_t HaltA(phcsBflI3P3A_HaltAParam_t *halt_a_param)         = 0;

        /*! Lower edge interface: Not to be redefined in the derived class: */
        class phcsBfl_RegCtl *mp_Lower;
};


/* \ingroup iso14443_3 
 *  Function pointer definition for C-phcsBfl_I3P3A Initialise function: */
typedef void (*pphcsBflI3P3A_Initialize_t) (phcsBflI3P3A_t*, void*, phcsBflRegCtl_t*, uint8_t);


/* \ingroup iso14443_3
 *  This is the GLUE class the C-kernel phcsBfl_I3P3A "sees" when calling down the stack. The glue class
 *  represents the C-API of the lower layer (RC Reg. Ctl.). 
 *
 */
class phcsBfl_I3P3AGlue
{
    public:
        phcsBfl_I3P3AGlue(void);
        ~phcsBfl_I3P3AGlue(void);
        
        /*! \name Static functions able to call into C++ again. */
        /*@{*/
        static phcsBfl_Status_t SetRegister(phcsBflRegCtl_SetRegParam_t *setreg_param);
        static phcsBfl_Status_t GetRegister(phcsBflRegCtl_GetRegParam_t *getreg_param);
        static phcsBfl_Status_t ModifyRegister(phcsBflRegCtl_ModRegParam_t *modify_param);
        static phcsBfl_Status_t SetRegisterMultiple(phcsBflRegCtl_SetMultiRegParam_t *setmultireg_param);
        static phcsBfl_Status_t GetRegisterMultiple(phcsBflRegCtl_GetMultiRegParam_t *getmultireg_param);
        /*@}*/

        /*! Structure accessible from the upper object's pure C-kernel. */
        phcsBflRegCtl_t m_GlueStruct;
};
/*! \endcond */


/*! \ingroup iso14443_3 
 * This is the main interface of phcsBfl_I3P3A. The class is a template because it cannot be foreseen
 * which type of C-kernel to wrap around. Depending on the implementation of this kernel, the
 * type of the internal C-member structure changes (avoid multiple definitions of the same structure
 * name for different structures when more than one type of functionality is used). The type of the
 * MEMBERS (in kernel's .h file) structure is the one to specify when creating an instance
 * of this class. */
template <class phcsBflIsoAcp> class phcsBflI3P3A_Wrapper : public phcsBfl_I3P3A
{
    public:
        phcsBflI3P3A_Wrapper(void);
        ~phcsBflI3P3A_Wrapper(void);

        /*
        * \ingroup iso14443_3
        * \param[in]  c_iso_14443_3_initialise  
        *                           Pointer to the Initialise function of the type of C-kernel (for
        *                           each hardware variant an own kernel must exist) to use with the
        *                           current wrapper instance.
        * \param[in] *p_lower       Pointer to the C++ object of the underlying layer.
        * \param[in] *initiator__not_target 
        *                           Specifier for operation mode (1 = Initiator, 0 = Target).
        *
        * \note The alternative Constructor does setup required by the Component but not done
        *       by the default constructor.
        *  
        */
        phcsBflI3P3A_Wrapper(pphcsBflI3P3A_Initialize_t  c_iso_14443_3_initialise,
                           phcsBfl_RegCtl             *p_lower,
                           uint8_t                   initiator__not_target);

       /*!
        * \ingroup iso14443_3
        * \param[in]  c_iso_14443_3_initialise
        *                           Pointer to the Initialise function of the type of C-kernel (for
        *                           each hardware variant an own kernel must exist) to use with the
        *                           current wrapper instance.
        * \param[in] *p_lower       Pointer to the C++ object of the underlying layer.
        * \param[in] *initiator__not_target
        *                           Specifier for operation mode (1 = Initiator, 0 = Target).
        *
        * \note The Initialise method does setup required by the Component but not done
        *       by the default constructor.
        *  
        */
        void Initialise(pphcsBflI3P3A_Initialize_t  c_iso_14443_3_initialise,
                        phcsBfl_RegCtl             *p_lower,
                        uint8_t                   initiator__not_target);
                        

        phcsBfl_Status_t RequestA(phcsBflI3P3A_ReqAParam_t *request_a_param);
        phcsBfl_Status_t AnticollSelect(phcsBflI3P3A_AnticollSelectParam_t *AnticollSelect_param);
        phcsBfl_Status_t Select(phcsBflI3P3A_SelectParam_t *select_param);
        phcsBfl_Status_t HaltA(phcsBflI3P3A_HaltAParam_t *halt_a_param);
        void SetWaitEventCb(phcsBflI3P3A_SetWecParam_t *set_wec_param);
        

        /*! \brief Constructor status: This status gives information about whether the object has been
         *  allocated/configured correctly or not. It is a good idea to check this status (verify
         *  that it is set to PH_ERR_BFL_SUCCESS) before proceeding.
         */
        phcsBfl_Status_t            m_ConstructorStatus;

        phcsBflI3P3A_t              m_Interface;            /*!< \brief C-kernel instance. */
        phcsBflIsoAcp               m_CommunicationParams;  /*!< \brief C-kernel internal variables. */

    protected:
        class phcsBfl_I3P3AGlue  m_I3_3Glue;    /*!< \brief Glue class for connecting the C-part
                                                             *   with the C++ wrapper. */
};


////////////////////////////////////////////////////////////////////////////////////////////////////
// phcsBflIo_Wrapper Wrapper Implementation, must reside here because of template usage:
////////////////////////////////////////////////////////////////////////////////////////////////////

/* \ingroup iso14443_3
 * Template definition for phcsBflI3P3A_Wrapper Constructor without initialisation. */
template <class phcsBflIsoAcp>
phcsBflI3P3A_Wrapper<phcsBflIsoAcp>::phcsBflI3P3A_Wrapper(void)
{
    // This constructor does no initialisation. Initialise() is needed.
    m_ConstructorStatus = PH_ERR_BFL_SUCCESS;        
}


/* \ingroup iso14443_3
 * Template definition for phcsBflI3P3A_Wrapper Constructor with initialisation */
template <class phcsBflIsoAcp>
phcsBflI3P3A_Wrapper<phcsBflIsoAcp>::phcsBflI3P3A_Wrapper(pphcsBflI3P3A_Initialize_t     c_iso_14443_3_initialise,
                                                    phcsBfl_RegCtl                *p_lower,
                                                    uint8_t                      initiator__not_target)
{
    m_ConstructorStatus = PH_ERR_BFL_SUCCESS;
    // Hand over the pointer of the C-kernel's initialise function:
    this->Initialise(c_iso_14443_3_initialise, p_lower, initiator__not_target);
}


/* \ingroup iso14443_3
 * Template definition for phcsBflI3P3A_Wrapper Destructor. */
template <class phcsBflIsoAcp>
phcsBflI3P3A_Wrapper<phcsBflIsoAcp>::~phcsBflI3P3A_Wrapper(void)
{
    // No action.
}


/* \ingroup iso14443_3
 * Template definition for phcsBflI3P3A_Wrapper Initialisation function. */
template <class phcsBflIsoAcp>
void phcsBflI3P3A_Wrapper<phcsBflIsoAcp>::Initialise(pphcsBflI3P3A_Initialize_t  c_iso_14443_3_initialise,
                                                 phcsBfl_RegCtl             *p_lower,
                                                 uint8_t                   initiator__not_target)
{
    // This binds the C-kernel to the C++ wrapper:
    c_iso_14443_3_initialise(&m_Interface,
                             &m_CommunicationParams,
                             &(m_I3_3Glue.m_GlueStruct),
                             initiator__not_target);

    // Initialise C++ part, beyond of the scope of the C-kernel:
    this->mp_Lower = p_lower;
}


/* \ingroup iso14443_3
 * Template definition for SetEventCb function. */
template <class phcsBflIsoAcp>
void phcsBflI3P3A_Wrapper<phcsBflIsoAcp>::SetWaitEventCb(phcsBflI3P3A_SetWecParam_t *set_wec_param)
{
    void *p_self;
    p_self = set_wec_param->self;
    set_wec_param->self = &m_Interface;
    m_Interface.SetWaitEventCb(set_wec_param);
    set_wec_param->self = p_self;
}


// Finally, the members (interface) calling into the C-kernel:

/* \ingroup iso14443_3
 *  \param[in] *request_a_param     Pointer to the I/O parameter structure
 *  \return                         phcsBfl_Status_t value depending on the C implementation
 *
 * Template definition for Member function RequestA. */
template <class phcsBflIsoAcp>
phcsBfl_Status_t phcsBflI3P3A_Wrapper<phcsBflIsoAcp>::RequestA(phcsBflI3P3A_ReqAParam_t *request_a_param)
{
    phcsBfl_Status_t   status;  
    void       *p_self; 
    
    p_self = request_a_param->self;
    request_a_param->self = &m_Interface;
    ((phcsBflRegCtl_t*)(((phcsBflI3P3A_t*)(request_a_param->self))->mp_Lower))->mp_CallingObject = this;
    status = m_Interface.RequestA(request_a_param);
    request_a_param->self = p_self;
    
    return status;
}


/* \ingroup iso14443_3
 *  \param[in] *AnticollSelect_param   Pointer to the I/O parameter structure
 *  \return                                 phcsBfl_Status_t value depending on the C implementation
 *
 * Template definition for Member function AnticollSelect. */
template <class phcsBflIsoAcp>
phcsBfl_Status_t phcsBflI3P3A_Wrapper<phcsBflIsoAcp>::AnticollSelect(phcsBflI3P3A_AnticollSelectParam_t *AnticollSelect_param)
{
    phcsBfl_Status_t   status;  
    void       *p_self; 
    
    p_self = AnticollSelect_param->self;
    AnticollSelect_param->self = &m_Interface;
    ((phcsBflRegCtl_t*)(((phcsBflI3P3A_t*)(AnticollSelect_param->self))->mp_Lower))->mp_CallingObject = this;
    status = m_Interface.AnticollSelect(AnticollSelect_param);
    AnticollSelect_param->self = p_self;

    return status;
}


/* \ingroup iso14443_3
 *  \param[in] *select_param    Pointer to the I/O parameter structure
 *  \return                     phcsBfl_Status_t value depending on the C implementation
 *
 * Template definition for Member function Select. */
template <class phcsBflIsoAcp>
phcsBfl_Status_t phcsBflI3P3A_Wrapper<phcsBflIsoAcp>::Select(phcsBflI3P3A_SelectParam_t *select_param)
{
    phcsBfl_Status_t   status;  
    void       *p_self; 
    
    p_self = select_param->self;
    select_param->self = &m_Interface;
    ((phcsBflRegCtl_t*)(((phcsBflI3P3A_t*)(select_param->self))->mp_Lower))->mp_CallingObject = this;
    status = m_Interface.Select(select_param);
    select_param->self = p_self;

    return status;
}


/* \ingroup iso14443_3
 *  \param[in] *halt_a_param    Pointer to the I/O parameter structure
 *  \return                     phcsBfl_Status_t value depending on the C implementation
 *
 * Template definition for Member function HaltA. */
template <class phcsBflIsoAcp>
phcsBfl_Status_t phcsBflI3P3A_Wrapper<phcsBflIsoAcp>::HaltA(phcsBflI3P3A_HaltAParam_t *halt_a_param)
{
    phcsBfl_Status_t   status;  
    void       *p_self; 
    
    p_self = halt_a_param->self;
    halt_a_param->self = &m_Interface;
    ((phcsBflRegCtl_t*)(((phcsBflI3P3A_t*)(halt_a_param->self))->mp_Lower))->mp_CallingObject = this;
    status = m_Interface.HaltA(halt_a_param);
    halt_a_param->self = p_self;

    return status;
}

}   /* phcs_BFL */

#endif /* PHCSBFLI3P3AWRAPPER_HPP */

/* E.O.F. */
