/* /////////////////////////////////////////////////////////////////////////////////////////////////
//                     Copyright (c) NXP Semiconductors
//
//                       (C)NXP Electronics N.V.2004
//         All rights are reserved. Reproduction in whole or in part is
//        prohibited without the written consent of the copyright owner.
//    NXP reserves the right to make changes without notice at any time.
//   NXP makes no warranty, expressed, implied or statutory, including but
//   not limited to any implied warranty of merchantability or fitness for any
//  particular purpose, or that the use will not infringe any third party patent,
//   copyright or trademark. NXP must not be liable for any loss or damage
//                            arising from its use.
///////////////////////////////////////////////////////////////////////////////////////////////// */

/*! 
 * \if cond_generate_filedocu 
 * \file phcsBflFelRd_Int.h
 *
 * Project: Object Oriented Library Framework FeliCa Component.
 *
 *  Source: phcsBflFelRd_Int.h
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Date: Thu Sep 27 09:39:23 2007 $
 *
 * Comment:
 *  None.
 *
 * History:
 *  GK:  Generated 29. April 2003
 *  MHa: Migrated to MoReUse September 2005
 * \endif
 *
 */


#ifndef PHCSBFLFELRD_INT_H
#define PHCSBFLFELRD_INT_H



/*! 
 *  \name Control Behaviour
 *  \brief  Control behaviour of the protocol upon reply: \n
 *   OR-ing of values allows the protocol to be prepared for multiple reply types to create a
 *   positive result (e.g. some cards return an ACK, some nothing upon certain commands).
 */
/*@{*/
/*!  \ingroup felrd */
#define PHCS_BFLFELRD_NAME       ((uint8_t)0x01)   /*!< \brief <description>. */
/*! @} */


/* //////////////////////////////////////////////////////////////////////////////////////////////////
// Transcations according to FELICA spec:
*/
/*!
* \ingroup felrd
* \param[in,out] *cmd_param     Struct with communication parameters 
*
* \retval  PH_ERR_BFL_SUCCESS                Operation successful.
* \retval  Other                             phcsBfl_Status_t values depending on the underlying components.
*
*  The main FELICA reader protocol entry point for CHECK. All FELICA functionality is concentrated in this
*  place. According to the cmd parameter all possible FeliCa (R) commands are handled. \n
*  For a detailed description of all commands have a look at a FeliCa documantation.
*/
phcsBfl_Status_t phcsBflFelRd_Check(phcsBflFelRd_CommandParam_t *check_param);


/* //////////////////////////////////////////////////////////////////////////////////////////////////
// Transcations according to FELICA spec:
*/
/*!
* \ingroup felrd
* \param[in,out] *cmd_param     Struct with communication parameters 
*
* \retval  PH_ERR_BFL_SUCCESS                Operation successful.
* \retval  Other                             phcsBfl_Status_t values depending on the underlying components.
*
*  The main FELICA reader protocol entry point for UPDATE. All FELICA functionality is concentrated in this
*  place. According to the cmd parameter all possible FeliCa (R) commands are handled. \n
*  For a detailed description of all commands have a look at a FeliCa documantation.
*/
phcsBfl_Status_t phcsBflFelRd_Update(phcsBflFelRd_CommandParam_t *check_param);


#endif  /* PHCSBFLFELRD_INT_H */

/* E.O.F. */
