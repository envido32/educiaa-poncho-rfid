/* /////////////////////////////////////////////////////////////////////////////////////////////////
//                     Copyright (c) Philips Semiconductors
//
//                       (C)PHILIPS Electronics N.V.2004
//         All rights are reserved. Reproduction in whole or in part is
//        prohibited without the written consent of the copyright owner.
//    Philips reserves the right to make changes without notice at any time.
//   Philips makes no warranty, expressed, implied or statutory, including but
//   not limited to any implied warranty of merchantability or fitness for any
//  particular purpose, or that the use will not infringe any third party patent,
//   copyright or trademark. Philips must not be liable for any loss or damage
//                            arising from its use.
///////////////////////////////////////////////////////////////////////////////////////////////// */

/*! \file phcsBflI3P4_PcdWrapper.cpp
 *
 * Project: ISO 14443.4.
 *
 * Workfile: phcsBflI3P4_PcdWrapper.cpp
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Date: Thu Sep 27 09:39:17 2007 $
 * $KeysEnd$
 *
 * Comment:
 *  T=CL class framework wrapper to enable multi-instance operation.
 *
 * History:
 *  GK:  Generated 8. Sept. 2003
 *  MHa: Migrated to MoReUse September 2005
 *
 */


#include <phcsBflI3P4Wrapper.hpp>

using namespace phcs_BFL;

////////////////////////////////////////////////////////////////////////////////////////////////////
// Glue:

phcsBfl_I3P4Glue::phcsBfl_I3P4Glue(void)
{
    // Initialise struct members with static class members:
    m_GlueStruct.Transceive = &phcsBfl_I3P4Glue::Transceive;

    // We don't need other members in the glue!
    m_GlueStruct.MfAuthent      = NULL;
    m_GlueStruct.Transmit       = NULL;
    m_GlueStruct.Receive        = NULL;
    m_GlueStruct.mp_Lower       = NULL;
    m_GlueStruct.mp_Members     = NULL;
}


phcsBfl_I3P4Glue::~phcsBfl_I3P4Glue(void)
{
}


// Static class members, able to call into C++ again:

phcsBfl_Status_t phcsBfl_I3P4Glue::Transceive(phcsBflIo_TransceiveParam_t *transceive_param)
{
    // Resolve the T=CL wrapper object location:
    class phcsBfl_I3P4 *iso14443_4_wrapping_object = (class phcsBfl_I3P4*)
        (((phcsBflIo_t*)(transceive_param->self))->mp_CallingObject);

    // Call the lower instance:
    return iso14443_4_wrapping_object->mp_Lower->Transceive(transceive_param);
}



////////////////////////////////////////////////////////////////////////////////////////////////////
// T=CL Wrapper Implemenation:

phcsBflI3P4_Wrapper::phcsBflI3P4_Wrapper(void)
{
    m_ConstructorStatus = PH_ERR_BFL_SUCCESS;
}


phcsBflI3P4_Wrapper::phcsBflI3P4_Wrapper(class phcsBfl_Io *p_lower)
{
    m_ConstructorStatus = Initialise(p_lower);
}


phcsBflI3P4_Wrapper::~phcsBflI3P4_Wrapper(void)
{
    // No action.
}


phcsBfl_Status_t phcsBflI3P4_Wrapper::Initialise( class phcsBfl_Io  *p_lower)
{
    phcsBfl_Status_t status;

    status =  phcsBflI3P4_Init(&m_Interface,
                                           &m_CommunicationParams,
                                           &(m_I3_4_Glue.m_GlueStruct)); // C++: C struct reference within glue class.
    // Additional initialisation beyond the scope of the C part:
    // Initialise C++ underlying object reference.
    mp_Lower = p_lower;
    // Initialise wrapper object reference:
    m_Interface.mp_CppWrapper = this;

    return status;
}


void phcsBflI3P4_Wrapper::SetCallbacks(phcsBflI3P4_SetCbParam_t *set_cb_param)
{
    void       *p_self; 
    
    p_self = set_cb_param->self;
    set_cb_param->self = &m_Interface;
    m_Interface.SetCallbacks(set_cb_param);
    set_cb_param->self = p_self;
}


void phcsBflI3P4_Wrapper::SetProtocol(phcsBflI3P4_SetProtParam_t *set_protocol_param)
{
    void *p_self;

	p_self = set_protocol_param->self;
    set_protocol_param->self = &m_Interface;
	m_Interface.SetProtocol(set_protocol_param);
	set_protocol_param->self = p_self;
}


void phcsBflI3P4_Wrapper::ResetProt(phcsBflI3P4_ResetProtParam_t *reset_protocol_param)
{
    void *p_self;

	p_self = reset_protocol_param->self;
    reset_protocol_param->self = &m_Interface;
	m_Interface.ResetProt(reset_protocol_param);
	reset_protocol_param->self = p_self;
}

void phcsBflI3P4_Wrapper::SetPUser(phcsBflI3P4_SetPUserParam_t *set_p_user_param)
{
    void *p_self;
    p_self = set_p_user_param->self;
    set_p_user_param->self = &m_Interface;
    m_Interface.SetPUser(set_p_user_param);
    set_p_user_param->self = p_self;

}


phcsBfl_Status_t phcsBflI3P4_Wrapper::Exchange(phcsBflI3P4_ExchangeParam_t *exchange_param)
{
    phcsBfl_Status_t   status;  
    void       *p_self; 
    
    p_self = exchange_param->self;
    exchange_param->self = &m_Interface;
    ((phcsBflIo_t*)(((phcsBflI3P4_t*)(exchange_param->self))->mp_Lower))->mp_CallingObject = this;
    status = m_Interface.Exchange(exchange_param);
    exchange_param->self = p_self;
    
    return status;
}


phcsBfl_Status_t phcsBflI3P4_Wrapper::PresenceCheck(phcsBflI3P4_PresCheckParam_t *presence_check_param)
{
    phcsBfl_Status_t  status;
    void      *p_self;

    p_self = presence_check_param->self;
    presence_check_param->self = &m_Interface;
    ((phcsBflIo_t*)(((phcsBflI3P4_t*)(presence_check_param->self))->mp_Lower))->mp_CallingObject = this;
    status = m_Interface.PresenceCheck(presence_check_param);

    return status;
}


phcsBfl_Status_t phcsBflI3P4_Wrapper::Deselect(phcsBflI3P4_DeselectParam_t *deselect_param)
{
    phcsBfl_Status_t    status;
    void        *p_self;
    
    p_self = deselect_param->self;
    deselect_param->self = &m_Interface;
    ((phcsBflIo_t*)(((phcsBflI3P4_t*)(deselect_param->self))->mp_Lower))->mp_CallingObject = this;
    status = m_Interface.Deselect(deselect_param);

    return status;
}


// EOF
