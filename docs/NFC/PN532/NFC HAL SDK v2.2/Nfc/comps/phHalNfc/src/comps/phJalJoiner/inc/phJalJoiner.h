/*
 *         Copyright (c), NXP Semiconductors
 *
 *                       (C)NXP Electronics N.V.2004
 *         All rights are reserved. Reproduction in whole or in part is
 *        prohibited without the written consent of the copyright owner.
 *    NXP reserves the right to make changes without notice at any time.
 *   NXP makes no warranty, expressed, implied or statutory, including but
 *   not limited to any implied warranty of merchantability or fitness for any
 *  particular purpose, or that the use will not infringe any third party patent,
 *   copyright or trademark. NXP must not be liable for any loss or damage
 *                            arising from its use.
 *
 */


/**
 * \file phJalJoiner.h
 * \brief JAL Function Prototypes
 *
 * This API allows the HAL layer to use the PN511 NFC chip with the same API
 * as HAL itself. The implementation is dedicated to the PN511.
 *  
 * Project: NFC MW / HAL
 *
 * $Date: Thu Sep 27 09:38:42 2007 $ 
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Aliases: SDK_HAL_2.2,SDK_HAL_2.2_071008,NFC_FRI_SDK_1.0_071031 $
 *
 */


#ifndef PHJALJOINER_H
#define PHJALJOINER_H

/** 
 *  \name JAL Joiner
 *  File: \ref phJalJoiner.h
 */
/*@{*/
#define PHJALJOINER_FILEREVISION "$Revision: 1.1 $" /**< \ingroup grp_file_attributes */
#define PHJALJOINER_FILEALIASES  "$Aliases: SDK_HAL_2.2,SDK_HAL_2.2_071008,NFC_FRI_SDK_1.0_071031 $"     /**< \ingroup grp_file_attributes */
/*@}*/

#include <phNfcTypes.h>
#include <phNfcCompId.h>
#include <phNfcHalTypes.h>

/** \todo Timeout value shall be added to TargetInfo structure for StartTargetMode */
#define TARGET_RECEPTION_LOOP_TIMEOUT   10000 /* User timeout value to be used by event callback */

#define MTU_SIZE    65535

/**
 * \ingroup grp_jal_common 
 *
 * This JAL-related function is wrapped by the HAL.
 * \ref phHalNfc_Open describes its behaviour.
 *
 * \copydoc phHalNfc_Open
 *   
 */
extern NFCSTATUS phJalJoiner_Open(phHal_sHwReference_t *psHwReference);

                                                                                             

/** \ingroup grp_jal_common
 * 
 * This JAL-related function is wrapped by the HAL.
 * \ref phHalNfc_GetDeviceCapabilities describes its behaviour.
 * 
 * \copydoc phHalNfc_GetDeviceCapabilities
 *   
 */
extern NFCSTATUS phJalJoiner_GetDeviceCapabilities(phHal_sHwReference_t          *psHwReference,
                                                   phHal_sDeviceCapabilities_t   *psDevCapabilities);


/** \ingroup grp_jal_nfci
 *
 * This JAL-related function is wrapped by the HAL.
 * \ref phHalNfc_Poll describes its behaviour at global scope. However, the special strategy of
 * this function is designed to be optimised for PN 511.
 *
 * \copydoc phHalNfc_Poll
 *   
 */
extern NFCSTATUS phJalJoiner_Poll(phHal_sHwReference_t            *psHwReference,
                                  phHal_eOpModes_t                 OpModes[],
                                  phHal_sRemoteDevInformation_t    psRemoteDevInfoList[],
                                  uint8_t                         *pNbrOfRemoteDev,
                                  phHal_sDevInputParam_t          *psDevInputParam);


/** \ingroup grp_jal_nfci
 *
 * This JAL-related function is wrapped by the HAL.
 * \ref phHalNfc_Connect describes its behaviour.
 *
 * \copydoc phHalNfc_Connect
 *   
 */
extern NFCSTATUS phJalJoiner_Connect(phHal_sHwReference_t           *psHwReference,
                                     phHal_eOpModes_t                OpMode,
                                     phHal_sRemoteDevInformation_t  *psRemoteDevInfo,
                                     phHal_sDevInputParam_t         *psDevInputParam);


/** \ingroup grp_jal_nfci
 *
 * This JAL-related function is wrapped by the HAL.
 * \ref phHalNfc_Transceive describes its behaviour.
 *
 * \copydoc phHalNfc_Transceive
 *   
 */
extern NFCSTATUS phJalJoiner_Transceive(phHal_sHwReference_t           *psHwReference,
                                        phHal_sRemoteDevInformation_t  *psRemoteDevInfo,
                                        phHal_uCmdList_t                Cmd,
                                        phHal_sDepAdditionalInfo_t     *psDepAdditionalInfo,
                                        uint8_t                        *pSendBuf,
                                        uint16_t                        SendLength,
                                        uint8_t                        *pRecvBuf,
                                        uint16_t                       *pRecvLength);



/** \ingroup grp_jal_nfci
 *
 * This JAL-related function is wrapped by the HAL.
 * \ref phHalNfc_Disconnect describes its behaviour.
 *
 * \copydoc phHalNfc_Disconnect
 *   
 */
extern NFCSTATUS phJalJoiner_Disconnect(phHal_sHwReference_t          *psHwReference,
                                        phHal_sRemoteDevInformation_t *psRemoteDevInfo);



/** \ingroup grp_jal_common
 *
 * This JAL-related function is wrapped by the HAL.
 * \ref phHalNfc_Ioctl describes its behaviour.
 *
 * \copydoc phHalNfc_Ioctl
 */
extern NFCSTATUS phJalJoiner_Ioctl(phHal_sHwReference_t     *psHwReference,
                                   uint16_t                  IoctlCode,
                                   uint8_t                  *pInBuf,
                                   uint16_t                  InLength,
                                   uint8_t                  *pOutBuf,
                                   uint16_t                 *pOutLength);



/** 
 * \ingroup grp_jal_common
 *
 * This JAL-related function is wrapped by the HAL.
 * \ref phHalNfc_Close describes its behaviour.
 *
 * \copydoc phHalNfc_Close
 *   
 */
extern NFCSTATUS phJalJoiner_Close(phHal_sHwReference_t *psHwReference);


/** 
 *  \ingroup grp_jal_nfct     
 *
 * This JAL-related function is wrapped by the HAL.
 * \ref phHalNfc_StartTargetMode describes its behaviour.
 *
 * \copydoc phHalNfc_StartTargetMode
 *   
 */
extern NFCSTATUS phJalJoiner_StartTargetMode(phHal_sHwReference_t       *psHwReference,
                                             phHal_sTargetInfo_t        *pTgInfo,
                                             phHal_eOpModes_t            OpModes[],
                                             uint8_t                    *pConnectionReq,
                                             uint8_t                    *pConnectionReqBufLength );


/** 
 *  \ingroup grp_jal_nfct
 *
 * This JAL-related function is wrapped by the HAL.
 * \ref phHalNfc_Receive describes its behaviour.
 *
 * \copydoc phHalNfc_Receive
 *   
 */
extern NFCSTATUS phJalJoiner_Receive(phHal_sHwReference_t        *psHwReference,
                                     phHal_sDepAdditionalInfo_t  *psDepAdditionalInfo,
                                     uint8_t                     *pRecvBuf,
                                     uint16_t                    *pRecvLength);


/** 
 *  \ingroup grp_jal_nfct
 *
 * This JAL-related function is wrapped by the HAL.
 * \ref phHalNfc_Send describes its behaviour.
 *
 * \copydoc phHalNfc_Send
 *   
 */
extern NFCSTATUS phJalJoiner_Send(phHal_sHwReference_t        *psHwReference,
                                  phHal_sDepAdditionalInfo_t  *psDepAdditionalInfo,
                                  uint8_t                     *pSendBuf,
                                  uint16_t                     SendLength);

#endif


