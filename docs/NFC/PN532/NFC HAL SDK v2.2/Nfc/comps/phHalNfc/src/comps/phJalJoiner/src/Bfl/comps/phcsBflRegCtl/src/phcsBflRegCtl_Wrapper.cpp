/* /////////////////////////////////////////////////////////////////////////////////////////////////
//                     Copyright (c) Philips Semiconductors
//
//                       (C)PHILIPS Electronics N.V.2004
//         All rights are reserved. Reproduction in whole or in part is
//        prohibited without the written consent of the copyright owner.
//    Philips reserves the right to make changes without notice at any time.
//   Philips makes no warranty, expressed, implied or statutory, including but
//   not limited to any implied warranty of merchantability or fitness for any
//  particular purpose, or that the use will not infringe any third party patent,
//   copyright or trademark. Philips must not be liable for any loss or damage
//                            arising from its use.
///////////////////////////////////////////////////////////////////////////////////////////////// */

/*! \file phcsBflRegCtl_Wrapper.cpp
 *
 * Project: Object Oriented Library Framework Register Control Component.
 *
 *  Source: phcsBflRegCtl_Wrapper.cpp
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Date: Thu Sep 27 09:39:19 2007 $
 *
 * Comment:
 *  Only the Glue class implementation resides here.
 *  Template implementation is located in the header!
 *
 * History:
 *  GK:  Generated 12. March 2003
 *  MHa: Migrated to MoReUse September 2005
 *
 */

#include <phcsBflBalWrapper.hpp>
#include <phcsBflRegCtlWrapper.hpp>


using namespace phcs_BFL;

////////////////////////////////////////////////////////////////////////////////////////////////////
// Glue:

phcsBfl_RegCtlGlue::phcsBfl_RegCtlGlue(void)
{
    // Initialise RcRegCtl struct members with static class members:
    m_GlueStruct.WriteBus = &phcsBfl_RegCtlGlue::WriteBus;
    m_GlueStruct.ReadBus  = &phcsBfl_RegCtlGlue::ReadBus;
    // We don't need the other members in the glue!
    m_GlueStruct.mp_Members = NULL;
}


phcsBfl_RegCtlGlue::~phcsBfl_RegCtlGlue(void)
{
    // For this implementation we don't have anything to clean up.
}


phcsBfl_Status_t phcsBfl_RegCtlGlue::WriteBus(phcsBflBal_WriteBusParam_t *writebus_param)
{
    class phcsBfl_RegCtl *rcregctl_wrapping_object = (class phcsBfl_RegCtl*)
        (((phcsBflBal_t*)(writebus_param->self))->mp_CallingObject);
    return ((phcsBfl_Bal*)rcregctl_wrapping_object->mp_Lower)->WriteBus(writebus_param);
}


phcsBfl_Status_t phcsBfl_RegCtlGlue::ReadBus(phcsBflBal_ReadBusParam_t *readbus_param)
{
    class phcsBfl_RegCtl *rcregctl_wrapping_object = (class phcsBfl_RegCtl*)
        (((phcsBflBal_t*)(readbus_param->self))->mp_CallingObject);
    return ((phcsBfl_Bal*)rcregctl_wrapping_object->mp_Lower)->ReadBus(readbus_param);
}

// E.O.F.
