/* /////////////////////////////////////////////////////////////////////////////////////////////////
//                     Copyright (c) Philips Semiconductors
//
//                       (C)PHILIPS Electronics N.V.2005
//         All rights are reserved. Reproduction in whole or in part is
//        prohibited without the written consent of the copyright owner.
//    Philips reserves the right to make changes without notice at any time.
//   Philips makes no warranty, expressed, implied or statutory, including but
//   not limited to any implied warranty of merchantability or fitness for any
//  particular purpose, or that the use will not infringe any third party patent,
//   copyright or trademark. Philips must not be liable for any loss or damage
//                            arising from its use.
///////////////////////////////////////////////////////////////////////////////////////////////// */

/*! \file phcsBflRegCtl_SpiHw1.c
 *
 * Projekt: Object Oriented Reader Library Framework RegCtl component.
 *
 *  Source: phcsBflRegCtl_SpiHw1.c
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Date: Thu Sep 27 09:39:20 2007 $
 *
 * Comment:
 *  Sample implementation for SPI communication.
 *
 * History:
 *  MHa: Generated 6. September 2004
 *  MHa: Migrated to MoReUse September 2005
 *                            
 */


/* Needed for the ATMEL SFRs */
#include <avr/interrupt.h>
#include <avr/io.h>

/* BFL related */
#include <phcsBflHw1Reg.h>
#include <phcsBflRegCtl.h>
#include "phcsBflRegCtl_SpiHw1Int.h"


/* Internal functions, doesn't have to be exposed to the outside */
// void phcsBflRegCtl_SpiHw1_Read (uint8_t *buff, const uint16_t len);
// void phcsBflRegCtl_SpiHw1_Write(uint8_t *buff, const uint16_t len);

/* Reads data from MISO */
void phcsBflRegCtl_SpiHw1_Read (uint8_t *buff, const uint16_t len)
{
    uint16_t    count;
    uint8_t     tmp;
    
    /* First entry in buff saves the read command (including the address)
     * If multiple read is performed (len>1) this value has to be reused
     * for MOSI 
     */
    tmp = buff[0];
    
    /* Disable interrupts */
    cli ();
    for (count = 0; count < len; count++, buff++)
    {
    	if (count == len-1)
    		/* If this is the last read, do not transfer useful data via MOSI */
        	SPDR = 0;
        else
        	/* Use the address for optimized reading */
        	SPDR = tmp;
        	
	    /* Wait until data has been received */
	    while (!(SPSR & (1<<SPIF)));
	    
	    /* Save the data into the buffer */
        *buff = SPDR;
    }
    /* Enable interrupts */
	sei();
	
}

/* Writes data to MOSI */
void phcsBflRegCtl_SpiHw1_Write (uint8_t *buff, const uint16_t len)
{
    uint16_t    count;
    
    /* Disable interrupts */
    cli ();
    
    for (count = 0; count < len; count++, buff++)
    {
        /* Write to MOSI */
        SPDR = *buff;
        /* Wait until data has been sent */
	    while (!(SPSR & (1<<SPIF)));
    }
    
    /* Enable interrupts */
	sei();

}

void phcsBflRegCtl_SpiHw1Init(phcsBflRegCtl_t       *cif,
                            void                *p_params,
                            phcsBflBal_t          *p_lower)
{
    /* Glue together and init the operation parameters: */
    cif->mp_Members     = p_params;
    cif->mp_Lower       = p_lower;
    /* Init the function pointers: */
    cif->GetRegister            = phcsBflRegCtl_SpiHw1GetReg;
    cif->GetRegisterMultiple    = phcsBflRegCtl_SpiHw1GetMultiReg;
    cif->SetRegister            = phcsBflRegCtl_SpiHw1SetReg;
    cif->SetRegisterMultiple    = phcsBflRegCtl_SpiHw1SetMultiReg;
    cif->ModifyRegister         = phcsBflRegCtl_SpiHw1ModReg;
    
	/* Set nReset, MOSI, NSS and SCK as output */
	PHCS_BFLREGCTL_SPIHW1_DATA_DIR_PORT |= (1<<PHCS_BFLREGCTL_SPIHW1_NRESET) | 
	                                                  (1<<PHCS_BFLREGCTL_SPIHW1_MOSI) | 
	                                                  (1<<PHCS_BFLREGCTL_SPIHW1_SCK) | 
	                                                  (1<<PHCS_BFLREGCTL_SPIHW1_NSS); 
	
	/* Set nReset to 1 */
	PHCS_BFLREGCTL_SPIHW1_DATA_PORT |= (1<<PHCS_BFLREGCTL_SPIHW1_NRESET);
	
	/* Enable SPI, Master, set clock rate to fck/16 */
	SPCR = (1<<SPE)|(1<<MSTR)|(1<<SPR0);
	
	/* Set NSS to one */
	PHCS_BFLREGCTL_SPIHW1_DATA_PORT |= (1<<PHCS_BFLREGCTL_SPIHW1_NSS);
	
	/* For testing purposes - test pins */
	/* DDRA |= (1<<PA5) | (1<<PA6); */
}


phcsBfl_Status_t phcsBflRegCtl_SpiHw1ModReg(phcsBflRegCtl_ModRegParam_t *modify_param)
{
    phcsBflRegCtl_GetRegParam_t   getreg_param;
    phcsBflRegCtl_SetRegParam_t   setreg_param;
    uint8_t           reg_data;
    phcsBfl_Status_t               status          = PH_ERR_BFL_SUCCESS;

    getreg_param.address = modify_param->address;
    getreg_param.self    = modify_param->self;
    setreg_param.address = modify_param->address;
    setreg_param.self    = modify_param->self;

    status = phcsBflRegCtl_SpiHw1GetReg(&getreg_param);
    if (status == PH_ERR_BFL_SUCCESS)
    {
        reg_data = getreg_param.reg_data;

        if (modify_param->set)
        {
            /* The bits of the mask, set to one are set in the new data: */
            reg_data |= modify_param->mask;
        } else
        {
            /* The bits of the mask, set to one are cleared in the new data: */
            reg_data &= (~modify_param->mask);
        }

        setreg_param.reg_data = reg_data;
        status = phcsBflRegCtl_SpiHw1SetReg(&setreg_param);

    } else
    {
        /* Error @ GetRegister!*/
    }
    PHCS_BFL_ADD_COMPCODE(status, PH_ERR_BFL_REGCTL);
    return status;
}


phcsBfl_Status_t phcsBflRegCtl_SpiHw1GetReg(phcsBflRegCtl_GetRegParam_t *getreg_param)
{
	uint8_t buffer;
	
	buffer = (uint8_t)((getreg_param->address << 1) | 0x80);

    /* Set nSS to LOW */
    PHCS_BFLREGCTL_SPIHW1_DATA_PORT &= ~(1<<PHCS_BFLREGCTL_SPIHW1_NSS);
    
	/* Wite the address */
	phcsBflRegCtl_SpiHw1_Write(&buffer, 1);
	
	/* Read the data */
	phcsBflRegCtl_SpiHw1_Read(&buffer, 1);
	
    /* Set nSS to HIGH */
    PHCS_BFLREGCTL_SPIHW1_DATA_PORT |= (1<<PHCS_BFLREGCTL_SPIHW1_NSS);

	getreg_param->reg_data = buffer;
	
    return PH_ERR_BFL_SUCCESS;
}


phcsBfl_Status_t phcsBflRegCtl_SpiHw1SetReg(phcsBflRegCtl_SetRegParam_t *setreg_param)
{
    uint8_t buf;
    
    buf = (uint8_t)((setreg_param->address << 1) & 0x7F);

    /* Set NSS to LOW */
    PHCS_BFLREGCTL_SPIHW1_DATA_PORT &= ~(1<<PHCS_BFLREGCTL_SPIHW1_NSS);

	/* Write the address */
	phcsBflRegCtl_SpiHw1_Write(&buf, 1);
	
	buf = setreg_param->reg_data;
	
	/* Write the data */
	phcsBflRegCtl_SpiHw1_Write(&buf, 1);

    /* Set NSS to HIGH */
    PHCS_BFLREGCTL_SPIHW1_DATA_PORT |= (1<<PHCS_BFLREGCTL_SPIHW1_NSS);

    return PH_ERR_BFL_SUCCESS;
}

phcsBfl_Status_t phcsBflRegCtl_SpiHw1SetMultiReg(phcsBflRegCtl_SetMultiRegParam_t *setmultireg_param)
{
    uint8_t             address;
    phcsBfl_Status_t    status = PH_ERR_BFL_SUCCESS;
    
    address = (uint8_t)((setmultireg_param->address << 1) & 0x7F);

    /* Check if length exceeds 64 bytes. HW1 only supports length of 64 byte in the FIFO! */
    if(setmultireg_param->length > 64 && setmultireg_param->address == PHCS_BFL_JREG_FIFODATA)
    {
        status = PH_ERR_BFL_INVALID_PARAMETER;
    } else
    {
        PHCS_BFLREGCTL_SPIHW1_DATA_PORT &= ~(1<<PHCS_BFLREGCTL_SPIHW1_NSS);

	    /* The address */
	    phcsBflRegCtl_SpiHw1_Write(&address, 1);
    	
	    /* (Multiple) data */
	    phcsBflRegCtl_SpiHw1_Write(setmultireg_param->buffer, setmultireg_param->length);

        PHCS_BFLREGCTL_SPIHW1_DATA_PORT |= (1<<PHCS_BFLREGCTL_SPIHW1_NSS);
    }

    PHCS_BFL_ADD_COMPCODE(status, PH_ERR_BFL_REGCTL);
    return status;
}


phcsBfl_Status_t phcsBflRegCtl_SpiHw1GetMultiReg(phcsBflRegCtl_GetMultiRegParam_t *getmultireg_param)
{
	uint8_t             address;
    phcsBfl_Status_t    status = PH_ERR_BFL_SUCCESS;
	
	address = (uint8_t)((getmultireg_param->address << 1) | 0x80);

    /* Check if length exceeds 64 bytes. HW1 only supports length of 64 byte in the FIFO! */
    if(setmultireg_param->length > 64 && setmultireg_param->address == PHCS_BFL_JREG_FIFODATA)
    {
        status = PH_ERR_BFL_INVALID_PARAMETER;
    } else
    {
        /* Set nSS to LOW */
        PHCS_BFLREGCTL_SPIHW1_DATA_PORT &= ~(1<<PHCS_BFLREGCTL_SPIHW1_NSS);
        
	    phcsBflRegCtl_SpiHw1_Write(&address, 1);
    	
	    getmultireg_param->buffer[0] = address;
    	
	    phcsBflRegCtl_SpiHw1_Read(getmultireg_param->buffer, getmultireg_param->length);
    	
        /* Set nSS to HIGH */
        PHCS_BFLREGCTL_SPIHW1_DATA_PORT |= (1<<PHCS_BFLREGCTL_SPIHW1_NSS);
    }

    PHCS_BFL_ADD_COMPCODE(status, PH_ERR_BFL_REGCTL);
    return status;
}

/* E.O.F. */
