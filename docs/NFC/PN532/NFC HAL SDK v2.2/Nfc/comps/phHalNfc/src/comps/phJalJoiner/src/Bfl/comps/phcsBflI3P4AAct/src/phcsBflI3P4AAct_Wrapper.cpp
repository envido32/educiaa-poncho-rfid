/* /////////////////////////////////////////////////////////////////////////////////////////////////
//                     Copyright (c) Philips Semiconductors
//
//                       (C)PHILIPS Electronics N.V.2004
//         All rights are reserved. Reproduction in whole or in part is
//        prohibited without the written consent of the copyright owner.
//    Philips reserves the right to make changes without notice at any time.
//   Philips makes no warranty, expressed, implied or statutory, including but
//   not limited to any implied warranty of merchantability or fitness for any
//  particular purpose, or that the use will not infringe any third party patent,
//   copyright or trademark. Philips must not be liable for any loss or damage
//                            arising from its use.
///////////////////////////////////////////////////////////////////////////////////////////////// */

/*! \file phcsBflI3P4AAct_Wrapper.cpp
 *
 * Project: ISO 14443.4.
 *
 * Workfile: phcsBflI3P4AAct_Wrapper.cpp
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Date: Thu Sep 27 09:39:24 2007 $
 * $KeysEnd$
 *
 * Comment:
 *  T=CL class framework wrapper to enable multi-instance operation.
 *
 * History:
 *  GK:  Generated 8. Sept. 2003
 *  MHa: Migrated to MoReUse September 2005
 *
 */


#include <phcsBflI3P4AActWrapper.hpp>

using namespace phcs_BFL;

////////////////////////////////////////////////////////////////////////////////////////////////////
// Glue:

phcsBfl_I3P4AActGlue::phcsBfl_I3P4AActGlue(void)
{
    // Initialise struct members with static class members:
    m_GlueStruct.Transceive = &phcsBfl_I3P4AActGlue::Transceive;

    // We don't need other members in the glue!
    m_GlueStruct.MfAuthent      = NULL;
    m_GlueStruct.Transmit       = NULL;
    m_GlueStruct.Receive        = NULL;
    m_GlueStruct.mp_Lower       = NULL;
    m_GlueStruct.mp_Members     = NULL;
}


phcsBfl_I3P4AActGlue::~phcsBfl_I3P4AActGlue(void)
{
}


// Static class members, able to call into C++ again:

phcsBfl_Status_t phcsBfl_I3P4AActGlue::Transceive(phcsBflIo_TransceiveParam_t *transceive_param)
{
    // Resolve the T=CL wrapper object location:
    class phcsBfl_I3P4AAct  *iso14443_4A_activation_wrapping_object =
        (class phcsBfl_I3P4AAct *)
            (((phcsBflIo_t*)(transceive_param->self))->mp_CallingObject);

    // Call the lower instance:
    return iso14443_4A_activation_wrapping_object->mp_Lower->Transceive(transceive_param);
}



////////////////////////////////////////////////////////////////////////////////////////////////////
// T=CL Wrapper Implemenation:

phcsBflI3P4AAct_Wrapper::phcsBflI3P4AAct_Wrapper(void)
{
    m_ConstructorStatus = PH_ERR_BFL_SUCCESS;
}


phcsBflI3P4AAct_Wrapper::phcsBflI3P4AAct_Wrapper(uint8_t        *p_trxbuffer,
                                             uint16_t        trxbuffersize,
                                             class phcsBfl_Io *p_lower)
{
    m_ConstructorStatus = this->Initialise(p_trxbuffer,
                                           trxbuffersize,
                                           p_lower);
}


phcsBflI3P4AAct_Wrapper::~phcsBflI3P4AAct_Wrapper(void)
{
    // No action.
}


phcsBfl_Status_t phcsBflI3P4AAct_Wrapper::Initialise(uint8_t          *p_trxbuffer,
                                                 uint16_t          trxbuffersize,
                                                 class phcsBfl_Io   *p_lower)
{
    phcsBfl_Status_t status;
    status =  phcsBflI3P4AAct_Init(&m_Interface,
                                 &m_ProtocolParams,
                                  p_trxbuffer,
                                  trxbuffersize,
                                 &(m_I3P4A_Act_Glue.m_GlueStruct)); // C++: C struct reference within glue class.
              
    // Additional initialisation beyond the scope of the C part:
    mp_Lower = p_lower; // Initialise C++ underlying object reference.
    return status;
}


phcsBfl_Status_t phcsBflI3P4AAct_Wrapper::Rats(phcsBflI3P4AAct_RatsParam_t *rats_param)
{
    phcsBfl_Status_t   status;  
    void       *p_self; 
    
    p_self = rats_param->self;
    rats_param->self = &m_Interface;
    ((phcsBflIo_t*)(((phcsBflI3P4AAct_t*)(rats_param->self))->mp_Lower))->mp_CallingObject = this;
    status = m_Interface.Rats(rats_param);
    rats_param->self = p_self;

    return status;
}


phcsBfl_Status_t phcsBflI3P4AAct_Wrapper::Pps(phcsBflI3P4AAct_PpsParam_t *pps_param)
{
    phcsBfl_Status_t   status;  
    void       *p_self; 
    
    p_self = pps_param->self;
    pps_param->self = &m_Interface;
    ((phcsBflIo_t*)(((phcsBflI3P4AAct_t*)(pps_param->self))->mp_Lower))->mp_CallingObject = this;
    status = m_Interface.Pps(pps_param);
    pps_param->self = p_self;

    return status;
}


phcsBflI3P4_ProtParam_t *phcsBflI3P4AAct_Wrapper::ProtocolParameters(void)
{
    return &m_ProtocolParams;
}

/* E.O.F. */
