/* /////////////////////////////////////////////////////////////////////////////////////////////////
//                     Copyright (c) NXP Semiconductors
//
//                       (C)NXP Electronics N.V.2004
//         All rights are reserved. Reproduction in whole or in part is
//        prohibited without the written consent of the copyright owner.
//    NXP reserves the right to make changes without notice at any time.
//   NXP makes no warranty, expressed, implied or statutory, including but
//   not limited to any implied warranty of merchantability or fitness for any
//  particular purpose, or that the use will not infringe any third party patent,
//   copyright or trademark. NXP must not be liable for any loss or damage
//                            arising from its use.
///////////////////////////////////////////////////////////////////////////////////////////////// */

/*! \file phcsBflRegCtl_JoinerRs232Pc.c
 *
 * Projekt: Object Oriented Reader Library Framework RegCtl component.
 *
 *  Source: phcsBflRegCtl_JoinerRs232Pc.c
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Date: Thu Sep 27 09:39:20 2007 $
 *
 * Comment:
 *  None.
 *
 * History:
 *  GK:  Generated 12. March 2003
 *  MHa: Migrated to MoReUse September 2005
 *                            
 */

#include <phcsBflHw1Reg.h>
#include <phcsBflRegCtl.h>
#include "phcsBflRegCtl_SerHw1Int.h"


void phcsBflRegCtl_SerHw1Init(phcsBflRegCtl_t     *cif,
                              void              *p_params,
                              phcsBflBal_t        *p_lower)
{
    /* Glue together and init the operation parameters: */
    cif->mp_Members     = p_params;
    cif->mp_Lower       = p_lower;
    ((phcsBflRegCtl_SerHw1Params_t*)cif->mp_Members)->dummy = 0;
    /* Initialize the function pointers: */
    cif->GetRegister            = phcsBflRegCtl_SerHw1GetReg;
    cif->SetRegister            = phcsBflRegCtl_SerHw1SetReg;
    cif->ModifyRegister         = phcsBflRegCtl_SerHw1ModReg;
    cif->GetRegisterMultiple    = phcsBflRegCtl_SerHw1GetMultiReg;
    cif->SetRegisterMultiple    = phcsBflRegCtl_SerHw1SetMultiReg;
}


phcsBfl_Status_t phcsBflRegCtl_SerHw1ModReg(phcsBflRegCtl_ModRegParam_t *modify_param)
{
    phcsBflRegCtl_GetRegParam_t   getreg_param;
    phcsBflRegCtl_SetRegParam_t   setreg_param;
    uint8_t                       reg_data;
    phcsBfl_Status_t              status;

    getreg_param.address = modify_param->address;
    getreg_param.self    = modify_param->self;
    setreg_param.address = modify_param->address;
    setreg_param.self    = modify_param->self;

    status = phcsBflRegCtl_SerHw1GetReg(&getreg_param);
    if (status == PH_ERR_BFL_SUCCESS)
    {
        reg_data = getreg_param.reg_data;

        if (modify_param->set)
        {
            /* The bits of the mask, set to one are set in the new data: */
            reg_data |= modify_param->mask;
        } else
        {
            /* The bits of the mask, set to one are cleared in the new data: */
            reg_data &= (uint8_t)(~modify_param->mask);
        }

        setreg_param.reg_data = reg_data;
        status = phcsBflRegCtl_SerHw1SetReg(&setreg_param);

    } else
    {
        /* Error @ GetRegister!*/
    }
    PHCS_BFL_ADD_COMPCODE(status, PH_ERR_BFL_REGCTL);
    return status;
}


phcsBfl_Status_t phcsBflRegCtl_SerHw1GetReg(phcsBflRegCtl_GetRegParam_t *getreg_param)
{
    const uint8_t set_msb_mask  = 0x80; /* 1000 0000 */
    phcsBfl_Status_t            status;
    phcsBflBal_WriteBusParam_t  wbp; 
    phcsBflBal_ReadBusParam_t   rbp;
    uint8_t addr_byte;
    
    phcsBflBal_t *lower = (phcsBflBal_t*)(((phcsBflRegCtl_t*)(getreg_param->self))->mp_Lower);
    /*
     * Uncomment if parameter should be used:
     * C_JOINER_RS_232_PC_RC_REG_CTL_PARAMS *rccp = (C_JOINER_RS_232_PC_RC_REG_CTL_PARAMS*) ((phcsBflRegCtl_t*)(getreg_param->self))->mp_Members;
     */

    /* Product chip: */
    addr_byte = (uint8_t)(getreg_param->address | set_msb_mask);

    wbp.bus_data = addr_byte;
	rbp.self = wbp.self = lower;
	rbp.bus_data = 0x00;
    status = lower->WriteBus(&wbp);
    if (status == PH_ERR_BFL_SUCCESS)
    {
        /* Register address byte written successfully: Read back data byte: */
        status = lower->ReadBus(&rbp);
        getreg_param->reg_data = rbp.bus_data;
    }
    PHCS_BFL_ADD_COMPCODE(status, PH_ERR_BFL_REGCTL);
    return status;
}


phcsBfl_Status_t phcsBflRegCtl_SerHw1SetReg(phcsBflRegCtl_SetRegParam_t *setreg_param)
{
    const uint8_t set_reg_addr_mask = 0x7F; /* 0111 1111 */
    phcsBfl_Status_t            status;
    phcsBflBal_WriteBusParam_t  wbp; 
    phcsBflBal_ReadBusParam_t   rbp;
    uint8_t       addr_byte;

    phcsBflBal_t *lower = (phcsBflBal_t*)(((phcsBflRegCtl_t*)(setreg_param->self))->mp_Lower);
    /*
     * Uncomment if parameter should be used:
     * C_JOINER_RS_232_PC_RC_REG_CTL_PARAMS *rccp = (C_JOINER_RS_232_PC_RC_REG_CTL_PARAMS*) ((phcsBflRegCtl_t*)(setreg_param->self))->mp_Members;
     */

    /*
     * Write address: Write: MSB = 0, next bit (RFU) = 0:
     */
    addr_byte = (uint8_t)(setreg_param->address & set_reg_addr_mask);
    
    wbp.bus_data = addr_byte;
    wbp.self = lower;
    status = lower->WriteBus(&wbp);
    if (status == PH_ERR_BFL_SUCCESS)
    {
        /* Register address byte written successfully: Write data byte: */
        wbp.self = lower;
        wbp.bus_data = setreg_param->reg_data;
        status = lower->WriteBus(&wbp);
        if (status == PH_ERR_BFL_SUCCESS)
        {
            /* Register content byte written successfully: We must read back the address echo: */
            rbp.self = lower;
            status = lower->ReadBus(&rbp);
            if (status == PH_ERR_BFL_SUCCESS)
            {
                /* We read something, however is it the address echo expected ? */
                if (rbp.bus_data == addr_byte)
                {
                    /* OK: No further action req'd. */
                } else
                {
                    /*
                     * Nope. What ever the source of data in the input FIFO is, it is
                     * not the (properly working) chip. 
                     */
                    status = PH_ERR_BFL_INTERFACE_ERROR;
                }
            }
        }
    }
    PHCS_BFL_ADD_COMPCODE(status, PH_ERR_BFL_REGCTL);
    return status;
}

/* Write multiple data to one register address. 
   In Rs232 it is done using the single register write command (JoinerRcRegCtlRs232PcSetRegister) */
phcsBfl_Status_t phcsBflRegCtl_SerHw1SetMultiReg(phcsBflRegCtl_SetMultiRegParam_t *setmultireg_param)
{
    phcsBflRegCtl_SetRegParam_t setreg_param;
    uint16_t                    i = 0;
    phcsBfl_Status_t            status          = PH_ERR_BFL_SUCCESS;

    setreg_param.self = setmultireg_param->self;

    /* configure address to be written only opnce at the beginning */
    setreg_param.address = setmultireg_param->address;

    /* Check if length exceeds 64 bytes. HW1 only supports length of 64 byte in the FIFO! */
    if(setmultireg_param->length > 64 && setmultireg_param->address == PHCS_BFL_JREG_FIFODATA)
    {
        status = PH_ERR_BFL_INVALID_PARAMETER;
    }

    /* loop write procedure as long as there is no failure and as long as data is available */
    while(status == PH_ERR_BFL_SUCCESS && i < setmultireg_param->length)
    {
        setreg_param.reg_data = setmultireg_param->buffer[i];
        i++;
        status = phcsBflRegCtl_SerHw1SetReg(&setreg_param);
    }
    PHCS_BFL_ADD_COMPCODE(status, PH_ERR_BFL_REGCTL);
    return status;
}


/* Read multiple data from one register address. 
   In Rs232 it is done using the single register read command (JoinerRcRegCtlRs232PcGetRegister) */
phcsBfl_Status_t phcsBflRegCtl_SerHw1GetMultiReg(phcsBflRegCtl_GetMultiRegParam_t *getmultireg_param)
{
    phcsBflRegCtl_GetRegParam_t     getreg_param;
    uint16_t                        i = 0;
    phcsBfl_Status_t                status          = PH_ERR_BFL_SUCCESS;

    getreg_param.self = getmultireg_param->self;

    /* configure address to be written only opnce at the beginning */
    getreg_param.address = getmultireg_param->address;

    /* loop write procedure as long as there is no failure and as long as data is available */
    while(status == PH_ERR_BFL_SUCCESS && i < getmultireg_param->length)
    {
        status = phcsBflRegCtl_SerHw1GetReg(&getreg_param);
        if(status == PH_ERR_BFL_SUCCESS)
        {
            getmultireg_param->buffer[i] = getreg_param.reg_data;
            i++;
        }
    }
    PHCS_BFL_ADD_COMPCODE(status, PH_ERR_BFL_REGCTL);
    return status;
}

/* E.O.F. */
