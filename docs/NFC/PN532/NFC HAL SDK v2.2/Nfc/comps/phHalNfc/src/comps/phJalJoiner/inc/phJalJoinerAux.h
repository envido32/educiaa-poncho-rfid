/*
 *         Copyright (c), NXP Semiconductors
 *
 *                       (C)NXP Electronics N.V.2004
 *         All rights are reserved. Reproduction in whole or in part is
 *        prohibited without the written consent of the copyright owner.
 *    NXP reserves the right to make changes without notice at any time.
 *   NXP makes no warranty, expressed, implied or statutory, including but
 *   not limited to any implied warranty of merchantability or fitness for any
 *  particular purpose, or that the use will not infringe any third party patent,
 *   copyright or trademark. NXP must not be liable for any loss or damage
 *                            arising from its use.
 *
 */

/*!
 * \file phJalJoinerAux.h
 * \brief This file contains all functions related to the JAL Poll function
 *
 *
 * Project: NFC MW / HAL
 *
 * $Date: Thu Sep 27 09:38:42 2007 $ 
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Aliases: SDK_HAL_2.2,SDK_HAL_2.2_071008,NFC_FRI_SDK_1.0_071031 $
 *
 */

#ifndef PHJALJOINERAUX_H
#define PHJALJOINERAUX_H

/*! \ingroup grp_file_attributes
 *  \name JAL Joiner
 *  File: \ref phJalJoinerAux.h
 */
/*@{*/
#define PHJALJOINERAUX_FILEREVISION "$Revision: 1.1 $"
#define PHJALJOINERAUX_FILEALIASES  "$Aliases: SDK_HAL_2.2,SDK_HAL_2.2_071008,NFC_FRI_SDK_1.0_071031 $"
/*@}*/


#include <phNfcHalTypes.h>

#include <phcsBflOpCtl.h>
#include <phcsBflI3P3A.h>
#include <phcsBflIo.h>
#include <phcsBflNfc.h>
#include <phcsBflI3P4AAct.h>
#include <phcsBflI3P4.h>
#include <phcsBflMfRd.h>
#include <phcsBflRegCtl.h>
#include <phcsBflPolAct.h>
#include <phcsBflNfcCommon.h>

#include <phJalJoinerBflFrame.h>

#define TIMER_DEFAULT_SETTINGS	0x00

#define PN511_STEPS_10US	0x01
#define PN511_STEPS_100US	0x02
#define PN511_STEPS_500US	0x03

#define USED_PRESCALER_VALUE    PN511_STEPS_500US

#define PN511_TIMER_MAX		0x10
#define PN511_TIMER_OFF		0x20

#define CARD_INIT_FAILED    0x02

#define MULTI_POLL_SUPPORTED                0x01

#define NUM_FELICA_POLLING_TIMESLOTS        0x03
#define FELICA_POLLING_TIMESLOT_DELAY       1208
#define FELICA_READ_WRITE_TIMEOUT_DELAY     51000
#define FELICA_POLLING_ANSWER_DELAY         2417
#define FELICA_REQUEST_ALL_SYSTEM_CODES     0xFF
#define FELICA_REQUEST_SYSTEM_CODE          0x01

#define NUM_FELICA_POLLNG_TMESLTS_WITH_MGN  0X04    // 0x03 + 1
#define FELICA_POLLNG_TMESLT_DLY_WITH_MGN   2000    // 1208 + Margin
#define FELICA_POLLNG_ANS_DLY_WITH_MGN      4000    // 2417 + Margin

#define FIELD_ACTIVATION_DELAY              5000
#define ISO_14443_RF_COMMUNICATION_DELAY    600
#define ISO_144434_RF_COMMUNICATION_DELAY   5000
#define NFC_PASSIVE_ACTIVATION_DELAY        5000000
#define NFC_ACTIVE_ACTIVATION_DELAY         5000000

/* Timeout values (see WT ISO18092) */
#define JAL_MIN_WT                          0x00
#define JAL_DEFAULT_WT                      0x0D
#define JAL_MAX_WT                          0x0E
#define ATR_WAITING_TIME_VALUE              4949    /* Max value, used for ATR only! */
/* Waiting time elementary unit in [ms]: */
#define WAITING_TIME_UNIT                   0.30206 /* t == (256 * 16) / fc[Hz] */

#define FELICA_TIMESLOT_LENGTH              0x14
#define FELICA_NFC_DEVICE_ID1               0x01
#define FELICA_NFC_DEVICE_ID2               0xFE
#define FELICA_IDMM_LENGTH                  0x08
#define FELICA_PMM_LENGTH                   0x08
#define FELICA_SYSTEM_CODE_LENGTH           0x02
#define FELICA_PAYLOAD_SIZE                 0x05
#define MIFARE_COMMAND_TIMEOUT              100000

#define CID_NOT_SUPPORTED                   0x02

#define PHJAL_NORFON                        0x00
#define PHJAL_EXTRFON                       0x01
#define PHJAL_INTRFON                       0x02

/**
 * \ingroup grp_jal_nfci_aux_functions
 * \brief Function delegating to subfunctions handling polling for each Meta Mode.
 *
 * As described in \ref phHalNfc_Poll the user passes a list of OpModes to the Poll function
 * according to what modes it has to poll for. OpModes are internally grouped into so called
 * Meta Modes. A Meta Mode contains all OpModes which are compatible and therefore could be 
 * polled with the same hardware configuration.
 * The main purpose of this function is to delegate the call according to the given OpMode
 * to a sub-function capable of polling for targets supporting this OpMode.
 *
 */
NFCSTATUS phJal_PollForDevices(phHal_sHwReference_t             *psHwReference,
                                phHal_sRemoteDevInformation_t   psRemoteDevInfoList[],
                                phHal_sDevInputParam_t          *psDevInputParam,
                                phHal_eOpModes_t                OpModes[],
                                uint8_t                         *pNbrOfRemoteDev,
                                uint8_t                         PollForAdditionalDevs);

/**
 * \ingroup grp_jal_nfci_aux_functions
 * \brief After a T=CL target was found by \ref phJal_P106ModeActivation, this function
 * will call the T=CL activation and initialisation of the \ref phHal_sRemoteDevInformation_t structure.
 *
 * At first a new T=CL protocol instance will be acquired which is used to communicate with the device.
 * Subsequently the T=CL activation will be executed and immediately deselected.
 * Protocol details received by the target will be stored in the \ref phHal_sRemoteDevInformation_t structure.
 * When all steps succeeded, the protocol instance will be associated with a logical handle within the BFL Frame. 
 *
 */
NFCSTATUS phJal_SetupIso144434Device(phHal_sHwReference_t               *psHwReference,
                                    phHal_sRemoteDevInformation_t       *psRemoteDevInfo,
                                    phHal_sDevInputParam_t              *psDevInputParam,
                                    phcsBflI3P3A_ReqAParam_t            *psRequestParam,
                                    phcsBflI3P3A_AnticollSelectParam_t  *psSelectParam,
                                    uint8_t                             *pRemoteDevFound);

/**
 * \ingroup grp_jal_nfci_aux_functions
 * \brief After a NFC target (all NFC modes) was found, this function
 * will call the NFC activation and initialisation of the \ref phHal_sRemoteDevInformation_t structure.
 *
 * This function sets up NFC devices supporting both passive and active modes.
 * After acquiring a NFC protocol instance, the device will be activated with an ATR request followed by
 * an ATTENTION request and finalised by a DESELECT request.
 * Protocol details received by the target will be stored in the \ref phHal_sRemoteDevInformation_t structure.
 * When all steps succeeded, the protocol instance will be associated with a logical handle within the BFL Frame. 
 *
 */
NFCSTATUS phJal_SetupNfcDevice(phHal_sHwReference_t             *psHwReference,
                                phHal_sRemoteDevInformation_t   *psRemoteDevInfo,
                                phHal_sDevInputParam_t          *psDevInputParam,
                                NfcSpeeds                       Speed,
                                NfcModes                        Mode,
                                uint8_t                         pNfcId2t[PHCS_BFLNFCCOMMON_ID_LEN]);


/**
 * \ingroup grp_jal_nfci_aux_functions
 * \brief Function setting up the Initiator mode for subsequent calls of a poll function.
 *
 */
phcsBfl_Status_t phJal_ConfigureHardwareForPolling(phHal_sHwReference_t *psHwReference);

/**
 * \ingroup grp_jal_nfci_aux_functions
 * \brief Sets the bit rate for a NFC mode (Passive/Active).
 *
 */
phcsBfl_Status_t phJal_SetBitrate(phHal_sHwReference_t    *psHwReference,
                                  NfcSpeeds               Speed,
                                  NfcModes                Mode);

/**
 * \ingroup grp_jal_nfci_aux_functions
 * \brief Sets the bit rate for a OpMode.
 *
 */
NFCSTATUS phJal_SetBitrateForOpMode(phHal_sHwReference_t    *psHwReference,
                                    phHal_eOpModes_t        OpMode);

/**
 * \ingroup grp_jal_nfci_aux_functions
 * \brief Activates the RF Field and checks wether the field activation succeeded or not.
 *
 * \note This function also handles the Initial RF Collision Avoidance to not disturb
 * a target which is currently communicating.
 */
phcsBfl_Status_t phJal_ActivateRfField(phHal_sHwReference_t *psHwReference);


/**
 * \ingroup grp_jal_nfci_aux_functions
 * \brief Checks whether the periperal or an external device has switched ON its RF.
 * \param [in]  psHwReference  Pointer to the HW Reference.
 * \param [out] rf_on__not_off Set to \ref PHJAL NORFON if the RF is OFF or no
 *                             external field is present. \ref PHJAL_INT_RF indicates
 *                             that the RF output stages of the peripheral are on. The
 *                             value \ref PHJAL_EXTRFON reflects the presence of an
 *                             external RF field.
 *
 * \retval Appropriate BFL status.
 */
phcsBfl_Status_t phJal_GetRfStatus(phHal_sHwReference_t *psHwReference,
                                   uint8_t              *rf_status);


/**
 * \ingroup grp_jal_nfci_aux_functions
 * \brief Switches off the RF Field.
 */
phcsBfl_Status_t phJal_SwitchOffRf(phHal_sHwReference_t *psHwReference);

/**
 * \ingroup grp_jal_nfci_aux_functions
 * \brief Configures the hardware to poll for devices supporting an OpMode belonging
 * to the \ref PHJAL_P106 Meta Mode.
 */
NFCSTATUS phJal_ConfigureHardwareForNfc106kProtocol(phHal_sHwReference_t *psHwReference);


/**
 * \ingroup grp_jal_nfci_aux_functions
 * \brief Polls for remote devices supporting an OpMode belonging to the \ref PHJAL_P106 Meta Mode.
 *
 * Main purpose of this function is to execute the ISO-14443-3 activation and to delegate 
 * initialisation and other configuration of remote devices to sub-functions. Delegation
 * is based on the evaluation of the SAK byte. The Poll cycle is repeated as long as
 * collisions were detected by the AnticollsionSelect function. After each cycle all devices
 * will be set to the HALT state.
 *
 * \note For ISO-14443-3 activation the REQIDL command will be used, thus only new 
 * (i.e. cards which have not been set to HALT Mode before) cards will respond. 
 * Cards supporting an OpMode currently not in the list of OpModes of the Poll function
 * will get lost because they would not respond to a REQIDL command anymore. To avoid
 * this behaviour a so called UID Cache was implemented as part of the BFL Frame 
 * (see \ref phJal_sBFLUpperFrame_t::UIDCache). If a not required card is detected, its
 * UID will be stored in the UID Cache and afterwards set to HALT.
 * In case of Mulltipolling (\ref phHal_sDevInputParamControlFlags_t::PollingAddsDevs)
 * is selected by the user, at first all remote devices available in the UID Cache will
 * be activated and checked wether they are required or not. Afterwards another Poll
 * cycle is started to detect new cards. 
 */
NFCSTATUS phJal_P106ModeActivation(phHal_sHwReference_t             *psHwReference,                                  
                                    phHal_sRemoteDevInformation_t   psRemoteDevInfoList[],
                                    phHal_sDevInputParam_t          *psDevInputParam,
                                    phHal_eOpModes_t                OpModes[],
                                    uint8_t                         *pNbrOfRemoteDev,                                   
                                    uint8_t                         PollForAdditionalDevs);

/**
 * \ingroup grp_jal_nfci_aux_functions
 * \brief Polls for remote devices supporting an OpMode belonging to the ISO 14443-4 B Meta Mode. 
 *
 * This function supports polling for Type B cards.
 */
NFCSTATUS phJal_P106BModeActivation(phHal_sHwReference_t                     *psHwReference,
                                    phHal_sRemoteDevInformation_t            psRemoteDevInfoList[],
                                    phHal_sDevInputParam_t                   *psDevInputParam,
                                    phHal_eOpModes_t                         OpModes[],
                                    uint8_t                                  *pNbrOfRemoteDev);

/**
 * \ingroup grp_jal_nfci_aux_functions
 * \brief Polls for remote devices supporting an OpMode belonging to the \ref PHJAL_P212_424 Meta Mode. 
 *
 * This function supports polling for Felica and NFC Passive 212k/424k devices.
 * As for other Poll functions, initialisation is delegated to a sub-function
 * (\ref phJal_InitialiseP212424Startup) 
 */
NFCSTATUS phJal_P212424ModeActivation(phHal_sHwReference_t          *psHwReference,                                  
                                    phHal_sRemoteDevInformation_t   psRemoteDevInfoList[],
                                    phHal_sDevInputParam_t          *psDevInputParam,
                                    phHal_eOpModes_t                OpModes[],
                                    uint8_t                         *pNbrOfRemoteDev);

/**
 * \ingroup grp_jal_nfci_aux_functions
 * \brief Polls for remote devices supporting an OpMode belonging to the \ref PHJAL_A Meta Mode. 
 *
 * This function provides polling for NFC Active devices. NFC Active Mode activation and initialisation
 * is delegated to (\ref phJal_SetupNfcDevice). It mainly configures the hardware for polling.
 */
NFCSTATUS phJal_NfcActiveModeActivation(phHal_sHwReference_t            *psHwReference,                                  
                                        phHal_sRemoteDevInformation_t   psRemoteDevInfoList[],
                                        phHal_sDevInputParam_t          *psDevInputParam,
                                        phHal_eOpModes_t                OpModes[],
                                        uint8_t                         *pNbrOfRemoteDev);

/**
 * \ingroup grp_jal_nfci_aux_functions
 * \brief Returns the SAK value associated for a given OpMode.
 *
 * It is used by \ref phJal_P106ModeActivation to compare SAK values for determining the
 * type of a detected remote device.
 */
uint8_t* phJal_GetSakComparatorForOpMode(phHal_eOpModes_t OpMode,                                        
                                        uint8_t           *pComparator);

/**
 * \ingroup grp_jal_nfci_aux_functions
 * \brief Configures a remote device for a given OpMode and initialises the \ref phHal_sRemoteDevInformation_t structure.   
 */
NFCSTATUS phJal_Setup106kMetaModeDevice(phHal_sHwReference_t                *psHwReference,
                                        phHal_sRemoteDevInformation_t       *psRemoteDevInfo,
                                        phHal_sDevInputParam_t              *psDevInputParam,
                                        phHal_eOpModes_t                    OpMode,
                                        phcsBflI3P3A_ReqAParam_t            *psRequestParam,
                                        phcsBflI3P3A_AnticollSelectParam_t  *psSelectParam);

/**
 * \ingroup grp_jal_nfci_aux_functions
 * \brief Executes the ISO-14443-4 activation using the passed protocol instance.
 */
NFCSTATUS phJal_ActivateIso144434Protocol(phHal_sHwReference_t       *psHwReference,
                                        phcsBflI3P4_t                *psProtocol,
                                        phcsBflI3P4AAct_RatsParam_t  *psRatsParam,
                                        uint8_t                      Cid);

/**
 * \ingroup grp_jal_nfci_aux_functions
 * \brief Executes the NFC activation (ATR request and subsequent ATTENTION request) using the passed protocol instance
 */
NFCSTATUS phJal_ActivateNfcProtocol(phHal_sHwReference_t                 *psHwReference,
                                    phHal_sDevInputParam_t               *psDevInputParam,
                                    phcsBflNfc_Initiator_t               *psProtocol,                                           
                                    phcsBflNfc_InitiatorAtrReqParam_t    *psAttributRequestParam,
                                    uint8_t                              Did,
                                    uint8_t                              pNfcId2t[PHCS_BFLNFCCOMMON_ID_LEN],
                                    uint8_t                              SendAttention);

/**
 * \ingroup grp_jal_nfci_aux_functions
 * \brief Executes the Felica/NFC Passive activation.
 */
NFCSTATUS phJal_StartupPassive212424Mode(phHal_sHwReference_t           *psHwReference,
                                        phHal_sDevInputParam_t          *psDevInputParam,
                                        phHal_eOpModes_t                OpModes[],
                                        phcsBflPolAct_PollingParam_t    *psPollingParameter);

/**
 * \ingroup grp_jal_nfci_aux_functions
 * \brief Initialises  the \ref phHal_sPassive106MfCardInformation_t structure.
 */
NFCSTATUS phJal_InitialiseMifareRemoteDevice(phHal_sRemoteDevInformation_t        *psRemoteDevInfo,
                                            phcsBflI3P3A_ReqAParam_t              *psRequestParam,
                                            phcsBflI3P3A_AnticollSelectParam_t    *psSelectParam,
                                            phJal_HandleEntry_t                   HandleEntries[]);

/**
 * \ingroup grp_jal_nfci_aux_functions
 * \brief Initialises the \ref phHal_sNfcActiveInformation_t or \ref phHal_sPassive106NfcInformation_t structure depending on the OpMode.
 */
NFCSTATUS phJal_InitialiseNfcProtocol(phHal_sHwReference_t             *psHwReference,
                                    phHal_sRemoteDevInformation_t      *psRemoteDevInfo,
                                    phcsBflNfc_InitiatorAtrReqParam_t  *psAttributRequestParam,
                                    NfcSpeeds                          Speed,
                                    NfcModes                           Mode);

/**
 * \ingroup grp_jal_nfci_aux_functions
 * \brief Initialises the \ref phHal_sISO_14443_4_Protocol_t structure.
 */
NFCSTATUS phJal_InitialiseISO_144434RemoteDevice(phHal_sRemoteDevInformation_t      *psRemoteDevInfo,
                                                phcsBflI3P3A_ReqAParam_t            *psRequestParam,
                                                phcsBflI3P3A_AnticollSelectParam_t  *psSelectParam,
                                                phJal_HandleEntry_t                 HandleEntries[],    
                                                phcsBflI3P4AAct_RatsParam_t         *psRatsParam);

/**
 * \ingroup grp_jal_nfci_aux_functions
 * \brief Initialises the \ref phHal_sPassive106Startup_t structure.
 *
 * Depending on the OpMode the Startup structure of \ref phHal_sISO_14443_4_Protocol_t, 
 * \ref phHal_sPassive106NfcInformation_t or \ref phHal_sPassive106MfCardInformation_t structure will
 * be initialised.
 */
NFCSTATUS phJal_InitialiseP106Startup(phHal_sRemoteDevInformation_t      *psRemoteDevInfo,
                                    phcsBflI3P3A_ReqAParam_t             *psRequestParam,
                                    phcsBflI3P3A_AnticollSelectParam_t   *psSelectParam,
                                    phHal_eOpModes_t                      OpMode);

/**
 * \ingroup grp_jal_nfci_aux_functions
 * \brief Initialises the \ref phHal_sPassive212_424Startup_t structure.
 *
 * * Depending on the OpMode the Startup structure of \ref phHal_sPassive212_424NfcInformation_t, 
 * or \ref phHal_sPassive212_424CardInformation_t structure will
 * be initialised.
 */
NFCSTATUS phJal_InitialiseP212424Startup(phHal_sRemoteDevInformation_t      *psRemoteDevInfo,                                             
                                        phcsBflPolAct_PollingParam_t        *psFeliCaPollingParam,
                                        uint16_t                            RxBufferResponsePosOfTimeslot,
                                        phHal_eOpModes_t                    OpMode);

/**
 * \ingroup grp_jal_nfci_aux_functions
 * \brief Deselects a NFC device.
 */
NFCSTATUS phJal_DeselectNfcDevice(phHal_sHwReference_t          *psHwReference,
                                  phcsBflNfc_Initiator_t        *psProtocol);

/**
 * \ingroup grp_jal_nfci_aux_functions
 * \brief Deselects a ISO-14443-4 device.
 */
NFCSTATUS phJal_DeselectIso144434Device(phHal_sHwReference_t     *psHwReference,
                                        phcsBflI3P4_t            *psProtocol);

phcsBfl_Status_t SetTimeOut(phcsBflRegCtl_t *rc_reg_ctl, 
                            phcsBflOpCtl_t  *rc_op_ctl, 
                            int32_t         aMicroSeconds, 
                            uint8_t         aFlags);

phcsBfl_Status_t StartTimer(phHal_sHwReference_t    *psHwReference);

void phJal_GenerateNfcId(uint8_t pNfcId[PHCS_BFLNFCCOMMON_ID_LEN]);

/**
 * \ingroup grp_jal_nfci_aux_functions
 * \brief Clears a NFC device. Does no soft reset, but a resetting of any event and active command.
 */
phcsBfl_Status_t phJal_ClearPeripheral(phJal_sBFLUpperFrame_t   *bfl_frame);

/**
 * \ingroup grp_jal_nfci_aux_functions
 * \brief Prints the register content of PN51x in DEBUG mode only. No effect in Release mode.
 */
phcsBfl_Status_t phJal_RegDump(phJal_sBFLUpperFrame_t   *bfl_frame);

#endif