/* /////////////////////////////////////////////////////////////////////////////////////////////////
//                     Copyright (c) NXP Semiconductors
//
//                       (C)NXP Electronics N.V.2004
//         All rights are reserved. Reproduction in whole or in part is
//        prohibited without the written consent of the copyright owner.
//    NXP reserves the right to make changes without notice at any time.
//   NXP makes no warranty, expressed, implied or statutory, including but
//   not limited to any implied warranty of merchantability or fitness for any
//  particular purpose, or that the use will not infringe any third party patent,
//   copyright or trademark. NXP must not be liable for any loss or damage
//                            arising from its use.
///////////////////////////////////////////////////////////////////////////////////////////////// */

/*! \file phcsBflFelRd.c
 *
 * Project: Object Oriented Library Framework Mifare Component.
 *
 *  Source: phcsBflFelRd.c
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Date: Thu Sep 27 09:39:23 2007 $
 *
 * Comment:
 *  Mifare implementation.
 *
 * History:
 *  GK:  Generated 12. March 2003
 *  MHa: Migrated to MoReUse September 2005
 *
 */

#include <phcsBflFelRd.h>
#include "phcsBflFelRd_Int.h"
#include <memory.h>


void phcsBflFelRd_Init(phcsBflFelRd_t   *cif,
                       void             *mp,
                       phcsBflIo_t      *p_lower,
                       uint8_t          *p_trxbuffer)
{
    /* Glue together and init the operation parameters: */
    ((phcsBflFelRd_InternalParam_t*)mp)->m_TrxBuffer = p_trxbuffer;
    cif->mp_Members                                  = mp;
    cif->mp_Lower                                    = p_lower;
    /* Initialize the function pointers: */
    cif->Check                                      = phcsBflFelRd_Check;
    cif->Update                                     = phcsBflFelRd_Update;
}

phcsBfl_Status_t phcsBflFelRd_Check(phcsBflFelRd_CommandParam_t *check_param)
{
    phcsBfl_Status_t    status          = PH_ERR_BFL_SUCCESS;

    // uint16_t            check_max_time;                     // Check maximum response time            

    phcsBflIo_TransceiveParam_t
                        trxparam;
    phcsBflFelRd_t     *self            =  (phcsBflFelRd_t*)(check_param->self);
    phcsBflFelRd_InternalParam_t
                       *members         = ((self->mp_Members));
    uint8_t            *tx_buffer       = members->m_TrxBuffer;
    uint8_t            *rx_buffer       = members->m_TrxBuffer;
    uint8_t            idm[PH_RFPROT_FERD_IDM_LENGTH];

    uint8_t             index           = 0;
    uint8_t             i               = 0;

    const uint8_t       idm_start_pos = PH_RFPROT_FERD_IDM_POS - 1;
    const uint8_t       flag_pos      = idm_start_pos + 8;
    const uint8_t       res_num_blocks_pos = flag_pos + 2;

    /* Store the IDM value in order to compare the value later in the response */
    memcpy(idm, &(check_param->txBuffer[idm_start_pos]), PH_RFPROT_FERD_IDM_LENGTH);

    /* Transmission and reception process */
    memcpy(tx_buffer, check_param->txBuffer, check_param->txLength);
    trxparam.self = self->mp_Lower;
    trxparam.tx_buffer = tx_buffer;
    trxparam.rx_buffer = rx_buffer;
    trxparam.rx_bits   = 0;
    trxparam.rx_buffer_size = PH_RFPROT_FERD_CHECK_RSP_MAX_LENGTH;
    trxparam.tx_buffer_size = check_param->txLength;
    status = self->mp_Lower->Transceive(&trxparam);
    
    /* Handle Response: */
    if (PH_ERR_BFL_SUCCESS == status)
    {
        // Get length:
        check_param->rxLength = rx_buffer[0];

        // Check Resp. code:
        if (rx_buffer[1] == PH_RFPROT_FERD_CHECK_RSP)
        {
            // Check IDm
            if (memcmp(&rx_buffer[idm_start_pos], idm, PHCS_BFLFELRD_IDM_LEN) == 0)
            {
                /* Get the flags */
                check_param->flags[0] = rx_buffer[flag_pos]; 
                check_param->flags[1] = rx_buffer[flag_pos + 1];

                if (trxparam.rx_buffer_size >= res_num_blocks_pos)
                {
                    memcpy(check_param->rxBuffer, &rx_buffer[res_num_blocks_pos], trxparam.rx_buffer_size - res_num_blocks_pos);
                    check_param->rxLength = rx_buffer[0] - res_num_blocks_pos;
                } else
                {
                    status = PH_ERR_BFL_INVALID_FORMAT;
                }
            } else
            {
                // Error:
                status = PH_ERR_BFL_INVALID_FORMAT;
            }

        } else
        {
            // Error:
            status = PH_ERR_BFL_INVALID_FORMAT;
        }
    } else
    {
        // Error in TRX: Just return status!
    }

    if (status != PH_ERR_BFL_SUCCESS)
    {
        check_param->rxLength = 0;
    }

    return status;
}


phcsBfl_Status_t phcsBflFelRd_Update(phcsBflFelRd_CommandParam_t *update_param)
{
    phcsBfl_Status_t    status          = PH_ERR_BFL_SUCCESS;

    //uint16_t            update_max_time;           // Update maximum response time            
    //uint16_t            nb_block_list;             // Number of block list bytes

    phcsBflIo_TransceiveParam_t
                        trxparam;
    phcsBflFelRd_t     *self            =  (phcsBflFelRd_t*)(update_param->self);
    phcsBflFelRd_InternalParam_t
                       *members         = ((self->mp_Members));
    uint8_t            *tx_buffer       = members->m_TrxBuffer;
    uint8_t            *rx_buffer       = members->m_TrxBuffer;
    uint8_t            idm[PH_RFPROT_FERD_IDM_LENGTH];

    uint8_t             index           = 0;
    uint8_t             i               = 0;
    const uint8_t       idm_start_pos = PH_RFPROT_FERD_IDM_POS - 1;
    const uint8_t       flag_pos      = idm_start_pos + 8;
    const uint8_t       num_blocks_pos  = flag_pos + 2;

    /* Store the IDM value in order to compare the value later in the response */
    memcpy(idm, &(update_param->txBuffer[idm_start_pos]), PH_RFPROT_FERD_IDM_LENGTH);

    /* Transmission and reception process */
    memcpy(tx_buffer,update_param->txBuffer,update_param->txLength);
    trxparam.self = self->mp_Lower;
    trxparam.tx_buffer = tx_buffer;
    trxparam.rx_buffer = rx_buffer;
    trxparam.rx_bits   = 0;
    trxparam.rx_buffer_size = PH_RFPROT_FERD_UPDATE_RSP_LENGTH;
    trxparam.tx_buffer_size = update_param->txLength;
    status = self->mp_Lower->Transceive(&trxparam);

    /* Handle Response: */
    if (PH_ERR_BFL_SUCCESS == status)
    {
        // Get length:
        update_param->rxLength = rx_buffer[0];

        // Check Resp. code:
        if (rx_buffer[1] == PH_RFPROT_FERD_UPDATE_RSP)
        {
            // Check IDm
            if (memcmp(&rx_buffer[idm_start_pos], idm, PHCS_BFLFELRD_IDM_LEN) == 0)
            {
                /* RF and resp. OK: Get the flags */
                update_param->flags[0] = rx_buffer[flag_pos]; 
                update_param->flags[1] = rx_buffer[flag_pos + 1];
                //update_param->rxLength = 0;
            } else
            {
                // Error:
                status = PH_ERR_BFL_INVALID_FORMAT;
            }

        } else
        {
            // Error:
            status = PH_ERR_BFL_INVALID_FORMAT;
        }
    } else
    {
        // Error in TRX: Just return status!
    }

    if (status != PH_ERR_BFL_SUCCESS)
    {
        update_param->rxLength = 0;
    }

    return status;
}

/* E.O.F. */
