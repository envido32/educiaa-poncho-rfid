/* /////////////////////////////////////////////////////////////////////////////////////////////////
//                     Copyright (c) Philips Semiconductors
//
//                       (C)PHILIPS Electronics N.V.2004
//         All rights are reserved. Reproduction in whole or in part is
//        prohibited without the written consent of the copyright owner.
//    Philips reserves the right to make changes without notice at any time.
//   Philips makes no warranty, expressed, implied or statutory, including but
//   not limited to any implied warranty of merchantability or fitness for any
//  particular purpose, or that the use will not infringe any third party patent,
//   copyright or trademark. Philips must not be liable for any loss or damage
//                            arising from its use.
///////////////////////////////////////////////////////////////////////////////////////////////// */

/*! \file phcsBflRegCtl_I2CHw1.c
 *
 * Projekt: Object Oriented Reader Library Framework RegCtl component.
 *
 *  Source: phcsBflRegCtl_I2CHw1.c
 * $Author: ct $
 * $Revision: 1.1 $
 * $Date: Tue Aug 17 10:20:44 2004 $
 *
 * Comment:
 *  Sample implementation for I2C communication.
 *
 * History:
 *  MHa: Generated 6. September 2004
 *  MHa: Migrated to MoReUse September 2005
 *                            
 */

#include <phcsBflHw1Reg.h>
#include <phcsBflRegCtl.h>
#include "phcsBflRegCtl_I2CHw1Int.h"

#include <.\Philips\Reg66X.h>

/* HW conection of Joiner and �C */
sbit gphcsBflRegCtl_I2CHw1NCS = P1^4;
sbit gphcsBflRegCtl_I2CHw1RST = P3^5;

/* I2C Status values */
#define FALSE   0
#define TRUE    1
#define I2C_WR  0
#define I2C_RD  1

/* I2C Status Errors */
#define I2C_OK                   0 /* transfer ended No Errors */
#define I2C_BUSY                 1 /* transfer busy */
#define I2C_NACK_ON_DATA         4 /* err: No ack on data */
#define I2C_NACK_ON_ADDRESS      5 /* err: No ack on address */
#define I2C_TIME_OUT             8 /* err: Time out occurred */

/* I2C States */
#define ST_IDLE         0
#define ST_SENDING      1
#define ST_AWAIT_ACK    2

/* I2C masks */
#define SI_MASK     0x08


typedef struct
{
   uint8_t address; /* slave address to sent/receive message */
   uint8_t nrBytes; /* number of bytes in message buffer */
   uint8_t *buf; /* pointer to application message buffer */
   uint8_t status; /* completion status */
} I2C_MESSAGE;


/* global variables and functions for I2C implementation */
uint8_t              gphcsBflRegCtl_I2CHw1Master;
static uint8_t       drvStatus;                             /* Status returned by driver */
static uint8_t       dataCount;                             /* nr of bytes of current message */
static uint8_t       state;                                 /* state of the I�C driver */
static void          (*readyProc)(uint8_t, uint8_t);        /* proc. to call if transfer ended */
static I2C_MESSAGE   *msg;                                  /* ptr to active message block */
I2C_MESSAGE          gphcsBflRegCtl_I2CHw1GMsg;      /* global Message */


/************************************
* Input(s)    : status of the driver.
* Output(s)   : None.
* Returns     : None.
* Description : Generate a stop condition and calls readyProc.
***************************************************************************/
static void phcsBflRegCtl_I2CHw1GenStop(uint8_t status)
{
    STO = 1;              /* generate stop condition */
    gphcsBflRegCtl_I2CHw1Master = FALSE;       /* deactivate master */
    state  = ST_IDLE;     /* set state machin to IDLE */
    readyProc(status);    /* Signal driver is finished */
}

/**********************************
* Input(s)    : None.
* Returns     : None.
* Description : Master mode state handler for I2C bus.
***************************************************************************/
static void phcsBflRegCtl_I2CHw1HandleMaster(void)
{
   switch (S1STA)
   {
     case 0x10:
     case 0x08: STA = 0; break;
     case 0x18:
     case 0x28: 
            if (dataCount < msg->nrBytes)
            {
                S1DAT = msg->buf[dataCount]; /* sent next byte */
                dataCount++;
            } else
            {
                phcsBflRegCtl_I2CHw1GenStop(I2C_OK);          /* transfer ready */
            }
            break;
     case 0x20: 
            phcsBflRegCtl_I2CHw1GenStop(I2C_NACK_ON_ADDRESS); 
            break;
     case 0x30: 
            phcsBflRegCtl_I2CHw1GenStop(I2C_NACK_ON_DATA); 
            break;
     case 0x38:     /* arbitration lost - do nothing */
            break;
     case 0x68:     /* call Slave procedure here, if addressed as slave */
            break;
     case 0x40:     /* prepare data receiption */
            if (msg->nrBytes == 0)
            {
                phcsBflRegCtl_I2CHw1GenStop(I2C_OK);    /* transfer ready */
            } else if (msg->nrBytes == 1) /* only 1 byte to read */
            {
                AA = 0;
            } else
            {
                AA = 1;
            }
            break;
     case 0x48: 
            phcsBflRegCtl_I2CHw1GenStop(I2C_NACK_ON_ADDRESS); 
            break;
     case 0x50:
            msg->buf[dataCount] = S1DAT;
            dataCount++;
            if (dataCount + 1  == msg->nrBytes)
            {
               AA = 0;  /* clear ACK for last byte */
            }
            break;
     case 0x58:
            phcsBflRegCtl_I2CHw1GenStop(I2C_OK); /* transfer ready */
            msg->buf[dataCount] = S1DAT;
            break;
   }
   SI = 0;
}

/************************************
* Input(s)   : *cr  speed clock register value for bus speed.
* Output(s)  : None.
* Returns    : None.
* Description: Initialize the controller as I2C bus master.
***************************************************************************/
void phcsBflRegCtl_I2CHw1InitMaster(uint8_t *cr)
{
   state     = ST_IDLE;
   readyProc = NULL;

   gphcsBflRegCtl_I2CHw1NCS = 0;         /* NCS of Joiner to 0 */
   gphcsBflRegCtl_I2CHw1RST = 1;         /* RST of Joiner to 1 */

   S1CON  = 0x00;      /* stop i2c controller before initialising */
   S1ADR  = 0x26;      /* dummy own slave address for master operation */

   CR0    = cr[0];     /* select speed by setting clock registers */
   CR1    = cr[1];
   CR2    = cr[2];

   ENS1   = 1;         /* start I2C communication */

   gphcsBflRegCtl_I2CHw1Master = FALSE;
}


/***********************************************
* Input(s)   : status   Status of the driver at completion time
*              msgsDone Number of messages completed by the driver
* Output(s)  : None.
* Returns    : None.
* Description: Signal the completion of an I�C transfer. This function is
*              passed (as parameter) to the driver and called by the
*              drivers state handler (!).
***************************************************************************/
static void phcsBflRegCtl_I2CHw1Ready(uint8_t status)
{
   drvStatus = status;
}

/**********************************
* Input(s)   : p    address of I�C transfer parameter block.
*              proc procedure to call when transfer completed,
*                   with the driver status passed as parameter.
* Output(s)  : None.
* Returns    : None.
* Description: Start an I�C transfer. The application must 
*              leave the transfer parameter block untouched 
*              until the ready procedure is called.
***************************************************************************/
void phcsBflRegCtl_I2CHw1Transfer(I2C_MESSAGE *p, void (*proc)(uint8_t, uint8_t))
{
    readyProc = proc;
    dataCount = 0;
    gphcsBflRegCtl_I2CHw1Master    = TRUE;
    msg   = p;
    state = (msg->address & 1) ? ST_AWAIT_ACK : ST_SENDING;
    STA   = 1;
    S1DAT = msg->address;
}

/******************************
* Input(s)   : None.
* Output(s)  : statusfield contains the driver status:
*               I2C_OK Transfer was successful.
*               I2C_TIME_OUT Timeout occurred
*               Otherwise Some error occurred.
* Returns    : None.
* Description: Start I�C transfer and wait (with timeout) until the
*              driver has completed the transfer(s).
***************************************************************************/
void phcsBflRegCtl_I2CHw1StartTransfer(I2C_MESSAGE *Msg)
{
    uint32_t    timeOut;
    uint8_t     retries = 1;
    
    do
    {
        drvStatus = I2C_BUSY;
        phcsBflRegCtl_I2CHw1Transfer(Msg, phcsBflRegCtl_I2CHw1Ready);
        timeOut = 0;
        while (drvStatus == I2C_BUSY)
        {
            if (++timeOut > 60000)
                drvStatus = I2C_TIME_OUT;
            if (S1CON & SI_MASK) /* wait until interrupt occured*/
                phcsBflRegCtl_I2CHw1HandleMaster();
        }
        if ( drvStatus != I2C_OK )
        {
            retries--;
        }
    } while (drvStatus != I2C_OK && retries );
    Msg->status = drvStatus;
}

/******************************
* Input(s)   : address to be written
*              slave address (Joiner)
* Output(s)  : None.
* Returns    : statusfield contains the driver status:
* Description: Start I�C transfer of address to read data from Joiner.
***************************************************************************/
phcsBfl_Status_t phcsBflRegCtl_I2CHw1WriteRegAddr(uint8_t reg_addr, uint8_t slv_addr)
{
    phcsBfl_Status_t status;
    
    gphcsBflRegCtl_I2CHw1GMsg.address = slv_addr << 1;    /* write access to internal address 0 */
    gphcsBflRegCtl_I2CHw1GMsg.nrBytes = 1;
    gphcsBflRegCtl_I2CHw1GMsg.buf = &reg_addr;
    
    phcsBflRegCtl_I2CHw1StartTransfer(&gphcsBflRegCtl_I2CHw1GMsg);
    status = gphcsBflRegCtl_I2CHw1GMsg.status;
    
    PHCS_BFL_ADD_COMPCODE(status, PH_ERR_BFL_REGCTL);
    return status;
}

/******************************
* Input(s)   : data buffer and data buffer length for read operation
*              slave address (Joiner)
* Output(s)  : None.
* Returns    : statusfield contains the driver status:
* Description: Start I�C transfer to read data from Joiner.
***************************************************************************/
phcsBfl_Status_t phcsBflRegCtl_I2CHw1ReadData(uint8_t *buf, uint8_t len, uint8_t slv_addr)
{
    phcsBfl_Status_t status;
    
    gphcsBflRegCtl_I2CHw1GMsg.address = (slv_addr << 1) | 0x01; /* read access to internal address 1 */
    gphcsBflRegCtl_I2CHw1GMsg.nrBytes = len;
    gphcsBflRegCtl_I2CHw1GMsg.buf  = buf;
    *gphcsBflRegCtl_I2CHw1GMsg.buf = 0x00;
    
    phcsBflRegCtl_I2CHw1StartTransfer(&gphcsBflRegCtl_I2CHw1GMsg);
    status = gphcsBflRegCtl_I2CHw1GMsg.status;
    
    PHCS_BFL_ADD_COMPCODE(status, PH_ERR_BFL_REGCTL);
    return status;
}

/* Function for initialising I2C communication */
void phcsBflRegCtl_I2CHw1Init(phcsBflRegCtl_t       *cif,
                              void                *p_params,
                              phcsBflBal_t          *p_lower)
{
    /* Glue together and init the operation parameters: */
    cif->mp_Members = p_params;
    cif->mp_Lower   = p_lower  = NULL;  /* no lower interface, set to 0 */
    /* Init the function pointers: */
    cif->GetRegister         = phcsBflRegCtl_I2CHw1GetReg;
    cif->SetRegister         = phcsBflRegCtl_I2CHw1SetReg;
    cif->ModifyRegister      = phcsBflRegCtl_I2CHw1ModReg;
    cif->GetRegisterMultiple = phcsBflRegCtl_I2CHw1GetMultiReg;
    cif->SetRegisterMultiple = phcsBflRegCtl_I2CHw1SetMultiReg;

    /* differ between dedicated speed settings from user or default speed settings */
    if((((phcsBflRegCtl_I2CHw1Params_t*)cif->mp_Members)->speed) != NULL)
        phcsBflRegCtl_I2CHw1InitMaster(((phcsBflRegCtl_I2CHw1Params_t*)cif->mp_Members)->speed);
    else
    {
        uint8_t speed_buffer[3] = {1, 1, 0};  /* set default data rate to 533 kBit */
        phcsBflRegCtl_I2CHw1InitMaster(speed_buffer);
    }
}

/* function to read data from Joiner */
phcsBfl_Status_t phcsBflRegCtl_I2CHw1GetReg(phcsBflRegCtl_GetRegParam_t *getreg_param)
{
    phcsBfl_Status_t     status = PH_ERR_BFL_SUCCESS;
    phcsBflRegCtl_I2CHw1Params_t *rccp = (phcsBflRegCtl_I2CHw1Params_t*) ((phcsBflRegCtl_t*)(getreg_param->self))->mp_Members;

    /* write first addres to hardware and read afterwards one data byte from address */
    if (phcsBflRegCtl_I2CHw1WriteRegAddr(getreg_param->address, rccp->slave_address) == 0)
    {
        phcsBflRegCtl_I2CHw1ReadData(&getreg_param->reg_data, 1, rccp->slave_address);
    } else 
    {
        status =  PH_ERR_BFL_IO_TIMEOUT;
    }

    /* redefine status for upper components */
    if (status != PH_ERR_BFL_SUCCESS)
        status = PH_ERR_BFL_IO_TIMEOUT;

    PHCS_BFL_ADD_COMPCODE(status, PH_ERR_BFL_REGCTL);
    return status; 
}

/* function to write one register byte */
phcsBfl_Status_t phcsBflRegCtl_I2CHw1SetReg(phcsBflRegCtl_SetRegParam_t *setreg_param)
{
    phcsBfl_Status_t     status;
    uint8_t buffer[2];
    phcsBflRegCtl_I2CHw1Params_t *rccp = (phcsBflRegCtl_I2CHw1Params_t*) ((phcsBflRegCtl_t*)(setreg_param->self))->mp_Members;

    buffer[0] = setreg_param->address;
    buffer[1] = setreg_param->reg_data;

    gphcsBflRegCtl_I2CHw1GMsg.address = rccp->slave_address << 1;    /* write access to internal address 1 */
    gphcsBflRegCtl_I2CHw1GMsg.nrBytes = 2;
    gphcsBflRegCtl_I2CHw1GMsg.buf     = buffer;

    phcsBflRegCtl_I2CHw1StartTransfer(&gphcsBflRegCtl_I2CHw1GMsg);

    /* redefine status for upper components */
    if (gphcsBflRegCtl_I2CHw1GMsg.status != 0)
    {
        status = PH_ERR_BFL_IO_TIMEOUT;
    }else
    {
        status = gphcsBflRegCtl_I2CHw1GMsg.status;
    }
    PHCS_BFL_ADD_COMPCODE(status, PH_ERR_BFL_REGCTL);
    return status;
}

phcsBfl_Status_t phcsBflRegCtl_I2CHw1ModReg(phcsBflRegCtl_ModRegParam_t *modify_param)
{
    phcsBflRegCtl_GetRegParam_t   getreg_param;
    phcsBflRegCtl_SetRegParam_t   setreg_param;
    uint8_t                       reg_data;
    phcsBfl_Status_t              status          = PH_ERR_BFL_SUCCESS;

    getreg_param.address = modify_param->address;
    getreg_param.self    = modify_param->self;
    setreg_param.address = modify_param->address;
    setreg_param.self    = modify_param->self;

    status = phcsBflRegCtl_I2CHw1GetReg(&getreg_param);
    if (status == PH_ERR_BFL_SUCCESS)
    {
        reg_data = getreg_param.reg_data;

        if (modify_param->set)
        {
            /* The bits of the mask, set to one are set in the new data: */
            reg_data |= modify_param->mask;
        } else
        {
            /* The bits of the mask, set to one are cleared in the new data: */
            reg_data &= (~modify_param->mask);
        }

        setreg_param.reg_data = reg_data;
        status = phcsBflRegCtl_I2CHw1SetReg(&setreg_param);

    } else
    {
        /* Error @ GetRegister!*/
    }
    PHCS_BFL_ADD_COMPCODE(status, PH_ERR_BFL_REGCTL);
    return status;
}


/* Write multiple data bytes to one register address. 
   For parallel interface it is done using the single register write command (JoinerRcRegCtlRs232PcSetRegister) */
phcsBfl_Status_t phcsBflRegCtl_I2CHw1SetMultiReg(phcsBflRegCtl_SetMultiRegParam_t *setmultireg_param)
{
    phcsBflRegCtl_SetRegParam_t     setreg_param;
    uint16_t                        i = 0;
    phcsBfl_Status_t                status          = PH_ERR_BFL_SUCCESS;

    setreg_param.self = setmultireg_param->self;

    /* configure address to be written only opnce at the beginning */
    setreg_param.address = setmultireg_param->address;

    /* Check if length exceeds 64 bytes. HW1 only supports length of 64 byte in the FIFO! */
    if(setmultireg_param->length > 64 && setmultireg_param->address == PHCS_BFL_JREG_FIFODATA)
    {
        status = PH_ERR_BFL_INVALID_PARAMETER;
    }

    /* loop write procedure as long as there is no failure and as long as data is available */
    while(status == PH_ERR_BFL_SUCCESS && i < setmultireg_param->length)
    {
        setreg_param.reg_data = setmultireg_param->buffer[i];
        i++;
        status = phcsBflRegCtl_I2CHw1SetReg(&setreg_param);
    }
    PHCS_BFL_ADD_COMPCODE(status, PH_ERR_BFL_REGCTL);
    return status;
}


/* Read multiple data bytes from one register address. 
   For parallel interface it is done using the single register read command (JoinerRcRegCtlRs232PcGetRegister) */
phcsBfl_Status_t phcsBflRegCtl_I2CHw1GetMultiReg(phcsBflRegCtl_GetMultiRegParam_t *getmultireg_param)
{
    phcsBflRegCtl_GetRegParam_t     getreg_param;
    uint16_t                        i = 0;
    phcsBfl_Status_t                status          = PH_ERR_BFL_SUCCESS;

    getreg_param.self = getmultireg_param->self;

    /* configure address to be written only opnce at the beginning */
    getreg_param.address = getmultireg_param->address;
    /* loop write procedure as long as there is no failure and as long as data is available */
    while(status == PH_ERR_BFL_SUCCESS && i < getmultireg_param->length)
    {
        status = phcsBflRegCtl_I2CHw1GetReg(&getreg_param);
        if(status == PH_ERR_BFL_SUCCESS)
        {
            getmultireg_param->buffer[i] = getreg_param.reg_data;
            i++;
        }
    }
    PHCS_BFL_ADD_COMPCODE(status, PH_ERR_BFL_REGCTL);
    return status;
}

/* E.O.F. */
