/* /////////////////////////////////////////////////////////////////////////////////////////////////
//                     Copyright (c) Philips Semiconductors
//
//                       (C)PHILIPS Electronics N.V.2004
//         All rights are reserved. Reproduction in whole or in part is
//        prohibited without the written consent of the copyright owner.
//    Philips reserves the right to make changes without notice at any time.
//   Philips makes no warranty, expressed, implied or statutory, including but
//   not limited to any implied warranty of merchantability or fitness for any
//  particular purpose, or that the use will not infringe any third party patent,
//   copyright or trademark. Philips must not be liable for any loss or damage
//                            arising from its use.
///////////////////////////////////////////////////////////////////////////////////////////////// */

/*! 
 * \if cond_generate_filedocu 
 * \file phcsBflI3P4Wrapper.hpp
 *
 * Project: ISO 14443.4.
 *
 * Workfile: phcsBflI3P4Wrapper.hpp
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Date: Thu Sep 27 09:39:04 2007 $
 * $KeysEnd$
 *
 * Comment:
 *  T=CL class framework wrapper to enable multi-instance operation.
 *
 * History:
 *  GK:  Generated 09. Sept. 2003
 *  MHa: Migrated to MoReUse September 2005
 * \endif
 *
 */

#ifndef PHCSBFLI3P4WRAPPER_HPP
#define PHCSBFLI3P4WRAPPER_HPP

#include <phcsBflIoWrapper.hpp>

extern "C"
{
    #include <phcsBflI3P4.h>
}

namespace phcs_BFL
{

/*! \cond cond_Cpp_complete */
/* \ingroup iso14443_4
 *  Functionality according to the T=CL (ISO 14443.4) specification
 *  (abstract class, used for the C++ interface).
 */
class phcsBfl_I3P4
{
    public:
        virtual phcsBfl_Status_t Initialise(class phcsBfl_Io *p_lower)                              = 0;
        virtual void SetCallbacks(phcsBflI3P4_SetCbParam_t *set_cb_param)                         = 0;
        virtual void SetProtocol(phcsBflI3P4_SetProtParam_t *set_protocol_param)                  = 0;
        virtual void ResetProt(phcsBflI3P4_ResetProtParam_t *reset_protocol_param)                = 0;
        virtual void SetPUser(phcsBflI3P4_SetPUserParam_t *set_p_user_param)                      = 0;
        virtual phcsBfl_Status_t Exchange(phcsBflI3P4_ExchangeParam_t *exchange_param)              = 0;
        virtual phcsBfl_Status_t PresenceCheck(phcsBflI3P4_PresCheckParam_t *presence_check_param)  = 0;
        virtual phcsBfl_Status_t Deselect(phcsBflI3P4_DeselectParam_t *deselect_param)              = 0;

        /*! Member pointer variable for general purpose (user-defined). */
        void        *mp_UserRef;
        
        /*! Lower edge interface: Not to be redefined in the derived class. */
        class phcsBfl_Io *mp_Lower;
};


////////////////////////////////////////////////////////////////////////////////////////////////////
/* \ingroup iso14443_4
 *  This class glues together the C-implementation of T=CL Reader Protocol and
 *  the underlying class framework. By declaring the member as _ static _ the
 *  method gets no implicit _ this _ pointer and can thus be called by any C
 *  function. However, the function must be informed about the target object
 *  of type phcsBfl_Io (casted to void*) to be called further down the stack.
 */
class phcsBfl_I3P4Glue
{
    public:
        phcsBfl_I3P4Glue(void);
        ~phcsBfl_I3P4Glue(void);
        
        //! \name Static function, enable to call into C++ again.
        //@{
        static phcsBfl_Status_t Transceive(phcsBflIo_TransceiveParam_t *transceive_param);
        //@}

        //! Structure accessible from the upper object's pure C-kernel.
        phcsBflIo_t     m_GlueStruct;
};
/*! \endcond */


////////////////////////////////////////////////////////////////////////////////////////////////////
/*! \ingroup iso14443_4
 *  \brief ISO 14443.4 communication protocol (T=CL) class. 
 */
class phcsBflI3P4_Wrapper : public phcsBfl_I3P4
{
    public:
        phcsBflI3P4_Wrapper(void);
        ~phcsBflI3P4_Wrapper(void);

        /*
         * \param[in] *p_lower Pointer to the C++ object of the underlying layer.
         *
         * \note The alternative constructor does setup required by the Component but not done
         *       by the default constructor.
         */
        phcsBflI3P4_Wrapper(class phcsBfl_Io *p_lower);

        /*!
         * \param[in] *p_lower  Pointer to the C++ object of the underlying layer.
         * \return              phcsBfl_Status_t value depending on the C implementation
         *
         * \note The Initialise method does setup required by the Component but not done
         *       by the default constructor.
         */
        phcsBfl_Status_t Initialise(class phcsBfl_Io   *p_lower);

        phcsBfl_Status_t Exchange(phcsBflI3P4_ExchangeParam_t *exchange_param);
        phcsBfl_Status_t PresenceCheck(phcsBflI3P4_PresCheckParam_t *presence_check_param);
        phcsBfl_Status_t Deselect(phcsBflI3P4_DeselectParam_t *deselect_param);
        void SetProtocol(phcsBflI3P4_SetProtParam_t *set_protocol_param);                        
        void ResetProt(phcsBflI3P4_ResetProtParam_t *reset_protocol_param);
        void SetPUser(phcsBflI3P4_SetPUserParam_t *set_p_user_param);
        void SetCallbacks(phcsBflI3P4_SetCbParam_t *set_cb_param);
        
        /*! \brief Constructor status: This status gives information about whether the object has been
         *  allocated/configured correctly or not. It is a good idea to check this status (verify
         *  that it is set to PH_ERR_BFL_SUCCESS) before proceeding.
         */
        phcsBfl_Status_t            m_ConstructorStatus;

        /*! \brief C-kernel internal variables. */
        phcsBflI3P4_CommParam_t     m_CommunicationParams;

        /*! \brief C-kernel structure instance. */
        phcsBflI3P4_t                        m_Interface;
        
    protected:
        /*! \brief C to C++ glue class instance of this component. */
        class phcsBfl_I3P4Glue               m_I3_4_Glue;
};

}   /* phcs_BFL */

#endif /* PHCSBFLI3P4WRAPPER_HPP */

/* E.O.F. */
