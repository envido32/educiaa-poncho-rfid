/*
 *         Copyright (c), NXP Semiconductors
 *
 *                       (C)NXP Electronics N.V.2004
 *         All rights are reserved. Reproduction in whole or in part is
 *        prohibited without the written consent of the copyright owner.
 *    NXP reserves the right to make changes without notice at any time.
 *   NXP makes no warranty, expressed, implied or statutory, including but
 *   not limited to any implied warranty of merchantability or fitness for any
 *  particular purpose, or that the use will not infringe any third party patent,
 *   copyright or trademark. NXP must not be liable for any loss or damage
 *                            arising from its use.
 *
 */

/*!
 * \file phJalTargetModeFunctions.h
 * \brief Target Mode functions
 *
 * Project: NFC MW / HAL
 *
 * $Date: Thu Sep 27 09:38:41 2007 $
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Aliases: SDK_HAL_2.2,SDK_HAL_2.2_071008,NFC_FRI_SDK_1.0_071031 $
 *
 */

#ifndef PHJALTARGETMODEFUNCTIONS_H
#define PHJALTARGETMODEFUNCTIONS_H

/*! \ingroup grp_file_attributes
 *  \name JAL Target Mode Functions
 *  File: \ref phJalTargetModeFunctions.h
 */
/*@{*/
#define PHJALTARGETMODEFUNCTIONS_FILEREVISION "$Revision: 1.1 $"
#define PHJALTARGETMODEFUNCTIONS_FILEALIASES  "$Aliases: SDK_HAL_2.2,SDK_HAL_2.2_071008,NFC_FRI_SDK_1.0_071031 $"
/*@}*/

#include <phNfcTypes.h>
#include <phNfcHalTypes.h>

#include <phcsBflStatus.h>
#include <phcsBflNfc.h>

#include <phJalJoinerBflFrame.h>

/* Modes the Target can be configured */
#define CONFIGURE_PASSIVE_MODE      0x01
#define CONFIGURE_ACTIVE_MODE       0x02


/**
 * \ingroup grp_jal_nfct_aux_functions
 * \brief Function handling all reception events of the Target.
 *
 * This function is intended to run as a thread to be independent of the
 * user. All data transmitted by the Initiator will be received and delegated
 * to the Target Endpoint for processing. One reception loop is finished as soon
 * as the Endpoint call returns and in some cases a response is sent back. At next
 * the function will block an wait for a new data. This cycle will continue as long
 * as no reception error occurs or a explicit termination interrupts the function.
 */
void phJal_ReceptionHandling(void *Parameters);

/**
 * \ingroup grp_jal_nfct_aux_functions
 * \brief Function implementing a callback which will be used by the BFL
 * to notify the user about incoming NFC commands.
 *
 * It is more or less a command switcher delegating NFC commands to their
 * appropriate handler function. All supported NFC commands can therefore be
 * found in this function. To not overload this callback, each command's implementation
 * was placed in a different file.
 */
phcsBfl_Status_t TargetEndpointCallback(phcsBflNfc_TargetEndpointParam_t *endpoint_param);

/**
 * \ingroup grp_jal_nfct_aux_functions
 * \brief Function configuring the Target hardware for either Passive or Active Mode.
 *
 * Configurations common to both modes will be executed in this function. All other
 * tasks will be delegated to \ref ConfigureTargetForPassiveMode or \ref ConfigureTargetForActiveMode.
 */
phcsBfl_Status_t ConfigureTarget(phJal_ReceptionHandlingParams_t *psReceptionHandlingParams);

/**
 * \ingroup grp_jal_nfct_aux_functions
 * \brief Function configuring the Target hardware for Passive Mode.
 *
 * \note Depending on the flag \ref phJal_ReceptionHandlingParams_t::InitialiseTarget, the
 * Target will be completely reset and initialised using a BFL function.
 */
phcsBfl_Status_t ConfigureTargetForPassiveMode(phJal_ReceptionHandlingParams_t *psReceptionHandlingParams);

/**
 * \ingroup grp_jal_nfct_aux_functions
 * \brief Function configuring the Target hardware for Active Mode.
 *
 * \note Depending on the flag \ref phJal_ReceptionHandlingParams_t::InitialiseTarget, the
 * Target will be completely reset and initialised using a BFL function.
 */
phcsBfl_Status_t ConfigureTargetForActiveMode(phJal_ReceptionHandlingParams_t *psReceptionHandlingParams);

/**
 * \ingroup grp_jal_nfct_aux_functions
 * \brief Function configuring the Target with all IDs passed by the user.
 *
 * The Target will be configured with the IDs passed by the \ref phHalNfc_StartTargetMode.
 */
phcsBfl_Status_t ConfigureTargetIds(phJal_ReceptionHandlingParams_t *psReceptionHandlingParams);

/**
 * \ingroup grp_jal_nfct_aux_functions
 * \brief Function initialising the Target from scratch.
 *
 * For initialisation a BFL function will be used.
 */
phcsBfl_Status_t InitialiseNfcTarget(phJal_ReceptionHandlingParams_t *psReceptionHandlingParams,
                                     uint8_t                         Mode);


/**
 * \ingroup grp_jal_nfct_aux_functions
 * \brief Function configuring the Joiner registers to be ready for a communication with an
 * Initiator.
 *
 * BFL functions will be used to configure registers. It is used to configure the Target
 * for a new communication session.
 * \note This function will set the DETECT_SYNC bit to zero.
 */
phcsBfl_Status_t ConfigureTargetCommunicationUnit(phJal_sBFLUpperFrame_t *psBflFrame);

/**
 * \ingroup grp_jal_nfct_aux_functions
 * \brief Function blocking the current thread until an semaphore-protected event has
 * reached a certain state.
 *
 * This function will block until either the specified state has been reached or a
 * timeout occurred.
 */
NFCSTATUS WaitForSemaphoreProtectedEvent(void       *hSemaphore,
                                         uint32_t   *pHandleToEventState,
                                         uint32_t   EventStateToWait,
                                         uint32_t   *pCurrentEventState,
                                         uint32_t   TimeoutValue);

/**
 * \ingroup grp_jal_nfct_aux_functions
 * \brief Function delegating requests from an Initiator to the Target Endpoint and
 * updating internal states.
 *
 * All incoming requests will be forwarded to this function which itself passes the
 * requests to the actual Target implementation. After the call to the Endpoint returns
 * internal states of the Target will be updated according the return value.
 *
 */
NFCSTATUS phJal_DispatchTargetCommand(phJal_ReceptionHandlingParams_t *psReceptionHandlingParams);

/**
 * \ingroup grp_jal_nfct_aux_functions
 * \brief Function for creating and initialising all semaphores/threads and associated state variables
 * needed to handle reception events.
 *
 * If an error occurs all semaphore handles will be closed.
 */
NFCSTATUS phJal_SetupTargetReceptionHandling(phJal_sBFLUpperFrame_t *psBflFrame);


/**
 * \ingroup grp_jal_nfct_aux_functions
 * \brief Function for closing all semaphores/threads
 *  needed to handle reception events.
 */
void phJal_CloseTargetReceptionHandling(phJal_sBFLUpperFrame_t *psBflFrame);


/**
 * \ingroup grp_jal_nfct_aux_functions
 * \brief Function creating and initialising all semaphores and associated state variables
 * specific to handle DEP requests/responses.
 *
 * If an error occurs all semaphore handles will be closed.
 */
NFCSTATUS phJal_SetupTargetDepHandlingEvents(phJal_sBFLUpperFrame_t *psBflFrame);

/**
 * \ingroup grp_jal_nfct_aux_functions
 * \brief Function closing all DEP specific handles.
 */
void phJal_CloseTargetDepHandlingEvents(phJal_sBFLUpperFrame_t *psBflFrame);

/**
 * \ingroup grp_jal_nfct_aux_functions
 * \brief Function setting the TargetConfiguration variable to specific state.
 *
 * This function is thread-safe and therefore acquires a semaphore before writing
 * a new value to the variable.
 */
NFCSTATUS phJal_SetTargetConfiguration(phJal_sBFLUpperFrame_t   *psBflFrame,
                                       uint8_t                  TargetConfiguration);

/**
 * \ingroup grp_jal_nfct_aux_functions
 * \brief Function returning the OpMode corresponding to passed parameters
 */
phHal_eOpModes_t phJal_GetRequestedOpMode(uint8_t Mode,
                                          uint8_t TargetType,
                                          uint8_t Speed);

#endif