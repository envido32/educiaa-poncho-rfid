/* /////////////////////////////////////////////////////////////////////////////////////////////////
//                     Copyright (c) Philips Semiconductors
//
//                       (C)PHILIPS Electronics N.V.2004
//         All rights are reserved. Reproduction in whole or in part is
//        prohibited without the written consent of the copyright owner.
//    Philips reserves the right to make changes without notice at any time.
//   Philips makes no warranty, expressed, implied or statutory, including but
//   not limited to any implied warranty of merchantability or fitness for any
//  particular purpose, or that the use will not infringe any third party patent,
//   copyright or trademark. Philips must not be liable for any loss or damage
//                            arising from its use.
///////////////////////////////////////////////////////////////////////////////////////////////// */

/*! 
 * \file phcsBflBal_Hw1SerLinInternal.h
 *
 * Project: Object Oriented Library Framework phcsBfl_Bal Component on Linux environment.
 *
 *  Source: phcsBflBal_Hw1SerLinInternal.h
 * $Author: frq09147 $
 * $Revision: 1.1 $ 
 * $Date: Thu Sep 27 09:39:11 2007 $
 *
 * Comment:
 *  None
 *
 * History:
 *  blake: Generated September 2005
 *
 */

#ifndef PHCSBFLBAL_HW1SERLININT_H
#define PHCSBFLBAL_HW1SERLININT_H

#include <phcsBflBal.h>


/* //////////////////////////////////////////////////////////////////////////////////////////////////
// phcsBfl_Bal WriteBus:
*/
/*!
* \ingroup bal
* \param[in,out] *writebus_param  Pointer to the I/O parameter structure
* 
* \retval  PH_ERR_BFL_SUCCESS                Operation successful.
* \retval  PH_ERR_BFL_INVALID_DEVICE_STATE   Internal state machine in different state.
* \retval  PH_ERR_BFL_INTERFACE_ERROR        Error on input parameters.
* \retval  PH_ERR_BFL_INSUFFICIENT_RESOURCES Not enough resources for component generation.
*
* This function writes the data byte directly to the RS232 interface of a Linux OS. 
* Interface specific handling as mentioned in the data sheet is done in this function.
*
*/
phcsBfl_Status_t phcsBflBal_Hw1SerLinWriteBus(phcsBflBal_WriteBusParam_t *writebus_param);


/* //////////////////////////////////////////////////////////////////////////////////////////////////
// phcsBfl_Bal ReadBus:
*/
/*!
* \ingroup bal
* \param[in,out] *readbus_param  Pointer to the I/O parameter structure
*
* \retval  PH_ERR_BFL_SUCCESS                Operation successful.
* \retval  PH_ERR_BFL_INVALID_DEVICE_STATE   Internal state machine in different state.
* \retval  PH_ERR_BFL_INTERFACE_ERROR        Error on input parameters.
* \retval  PH_ERR_BFL_INSUFFICIENT_RESOURCES Not enough resources for component generation.
* 
* This function reads one byte from the hardware using the RS232 interface on a Linux OS. 
* Interface specific handling as mentioned in the data sheet is done in this function.
*
*/
phcsBfl_Status_t phcsBflBal_Hw1SerLinReadBus(phcsBflBal_WriteBusParam_t *readbus_param);

/* //////////////////////////////////////////////////////////////////////////////////////////////////
// phcsBfl_Bal HostBaud:
*/
/*!
* \ingroup bal
* \param[in] *hostbaud_param  Pointer to the I/O parameter structure
*
 * This function adjusts the host data rate of the com port to the value of phcsBflBal_HostBaudParam_t.
*/
void phcsBflBal_Hw1SerLinHostBaud(phcsBflBal_HostBaudParam_t *hostbaud_param);


/* //////////////////////////////////////////////////////////////////////////////////////////////////
// phcsBfl_Bal ClosePort:
*/
/*!
* \ingroup bal
* \param[in] *closeport_param  Pointer to the I/O parameter structure
*
* This function closes the com port used in the bal if opened or activated before.
*/
void phcsBflBal_Hw1SerLinClosePort(phcsBflBal_ClosePortParam_t *closeport_param);


#endif /* PHCSBFLBAL_HW1SERLININT_H */

/* E.O.F. */
