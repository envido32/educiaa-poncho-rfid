/* /////////////////////////////////////////////////////////////////////////////////////////////////
//                     Copyright (c) Philips Semiconductors
//
//                       (C)PHILIPS Electronics N.V.2004
//         All rights are reserved. Reproduction in whole or in part is
//        prohibited without the written consent of the copyright owner.
//    Philips reserves the right to make changes without notice at any time.
//   Philips makes no warranty, expressed, implied or statutory, including but
//   not limited to any implied warranty of merchantability or fitness for any
//  particular purpose, or that the use will not infringe any third party patent,
//   copyright or trademark. Philips must not be liable for any loss or damage
//                            arising from its use.
///////////////////////////////////////////////////////////////////////////////////////////////// */


/*! \file MifareReader.cpp
 *
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Date: Thu Sep 27 09:38:46 2007 $
 *
 * Comment:
 *  Example code for handling of BFL.
 *
 */

/* Include all necessary component headers for Mifare Reader operation and additional definitions.
   (BAL, RegisterControl, Iso14443Part3, OperationControl, RcIo, Mifare)                 */

#include <stdio.h>		/* For debugging purposes (printf) */

/* Common headers */
#include <phcsBflRefDefs.h>
#include <phcsBflHw1Reg.h>

/* Needed for any Operating Mode */
#include <phcsBflBalWrapper.hpp>
#include <phcsBflRegCtlWrapper.hpp>
#include <phcsBflOpCtlWrapper.hpp>
#include <phcsBflOpCtl_Hw1Ordinals.h>
#include <phcsBflIoWrapper.hpp>

/* Needed for this Operating Mode */
#include <phcsBflI3P3AWrapper.hpp>
#include <phcsBflMfRdWrapper.hpp>

#include "ExampleUtils.h"
#include "ExampleGlobals.h"


/*
 * This example shows how to act as Mifare reader 
 */
uint16_t ActivateMifareReader(void *comHandle, uint32_t aSettings)
{
    using namespace phcs_BFL;
    
	/* BAL either for Windows xor for Linux */
#ifdef WIN32    
	phcsBflBal_Wrapper<phcsBflBal_Hw1SerWinInitParams_t>   bal;
#else    
	phcsBflBal_Wrapper<phcsBflBal_Hw1SerLinInitParams_t>     bal;
#endif
    phcsBflRegCtl_Wrapper<phcsBflRegCtl_SerHw1Params_t>    rc_reg_ctl;
    phcsBflOpCtl_Wrapper<phcsBflOpCtl_Hw1Params_t>             rc_op_ctl;
    phcsBflIo_Wrapper<phcsBflIo_Hw1Params_t>                   rcio;
    phcsBflI3P3A_Wrapper<phcsBflI3P3A_Hw1Params_t> iso14443_3;
    phcsBflMfRd_Wrapper                                           mfrd;

    /* Register control parameters */
    phcsBflRegCtl_SetRegParam_t         srp;
    phcsBflRegCtl_GetRegParam_t         grp;
    phcsBflRegCtl_ModRegParam_t         mrp;

    /* Operation control parameters */
    phcsBflOpCtl_AttribParam_t          opp;

    /* ISO14443-3A parameters       */
    phcsBflI3P3A_ReqAParam_t            req_p;
    phcsBflI3P3A_AnticollSelectParam_t  sel_p;
    phcsBflI3P3A_HaltAParam_t           hlt_p;

    /* Mifare parameters            */
    phcsBflMfRd_CommandParam_t          mfr_p;

	/*
	 * This is an example implementation how to use the event sensing mechanism 
	 * This is an interrupt handling mechanism which looks after the IRQ pin of the PN51x
	 * UNCOMMENT the following part in order to use the event mechanism on Windows platforms.
	 *
	 * REMARK: See also the same comment below for the main functionality!!
	 */
	/*	
	phcsBflAux_WaitEventCbParam_t cbEvent;
	*/

	/* Declaration of variables, buffer, ... */
    /* Status variable */
    phcsBfl_Status_t     status = PH_ERR_BFL_SUCCESS;
    /* Buffer for data transfer (send/receive)  */
    uint8_t buffer[EXAMPLE_TRX_BUFFER_SIZE];
    /* Buffer serial number						*/
    uint8_t serial[12];
    /* Counter variable							*/
    uint8_t i, index;

    /* 
	 * Build up stack for serial communication. 
     * Start with the lowest layer, so that the upper ones may refer to this already. 
	 */
#ifdef WIN32
    phcsBflBal_Hw1SerWinInitParams_t init_params;
    init_params.com_handle = (void*)(comHandle);
    init_params.com_port   = 0;
    bal.Initialise(phcsBflBal_Hw1SerWinInit, (void*)(&init_params)); 
#else
    phcsBflBal_Hw1SerLinInitParams_t init_params;
    init_params.com_handle = (void*)(comHandle);
    init_params.com_port   = 0;
	bal.Initialise(phcsBflBal_Hw1SerLinInit, (void*)(&init_params)); 
#endif
    /* Initialise Register Control Layer component (same for Initiator and Target) */
    rc_reg_ctl.Initialise(phcsBflRegCtl_SerHw1Init, &bal);
    /* Initialise Operation Control component (same for Initiator and Target) */
    rc_op_ctl.Initialise(phcsBflOpCtl_Hw1Init, &rc_reg_ctl); 
    /* Initialise RcIo component (choose Initiator behaviour) */
    rcio.Initialise(phcsBflIo_Hw1Init, &rc_reg_ctl, PHCS_BFLIO_INITIATOR); 
    /* Initialise ISO14443-3 component (choose Initiator == PCD behaviour) */
    iso14443_3.Initialise(phcsBflI3P3A_Hw1Initialise, &rc_reg_ctl, PHCS_BFLI3P3A_INITIATOR);
    /* Initialise Mifare component */
    mfrd.Initialise(&rcio, buffer+20);

	/*
	 * This is an example implementation how to use the event sensing mechanism 
	 * This is an interrupt handling mechanism which looks after the IRQ pin of the PN51x
	 * UNCOMMENT the following part in order to use the event mechanism on Windows platforms
	 *
	 * REMARK: See also the declaration at the beginning of this function!!
	 */
	/*	
	//cbEvent.wait_event_cb = WaitForPN51xInterruptCb;
	cbEvent.wait_event_cb = WaitEventCB;
	cbEvent.user_ref = (void*) comHandle;
	
	rc_op_ctl.SetWaitEventCb(&cbEvent);
	rcio.SetWaitEventCb(&cbEvent);
	iso14443_3.SetWaitEventCb(&cbEvent);
	*/	
	
	/* Configure PN51x to act as a Mifare Reader. All register which are set in other modes 
     * are configured to the right values. 
     * Remark: The operation control function does not perform a softreset and does not know anything 
     *         about the antanna configuration, so these things have to be handled separately!          
	 */
    opp.group_ordinal    = PHCS_BFLOPCTL_GROUP_MODE;
    opp.attrtype_ordinal = PHCS_BFLOPCTL_ATTR_MFRD_A;
    opp.param_a          = PHCS_BFLOPCTL_VAL_RF106K;
    opp.param_b          = PHCS_BFLOPCTL_VAL_RF106K;
    status = rc_op_ctl.SetAttrib(&opp);
    /* Check status and display error if something went wrong */
    if(status != PH_ERR_BFL_SUCCESS)
        printf("*** ERROR performing Mifare Reader configuration! Status = %04X \n", status);

    /* Set Initiator bit of Joiner to act as a reader using register control functions. */
    srp.address = PHCS_BFL_JREG_CONTROL;
    srp.reg_data = PHCS_BFL_JBIT_INITIATOR;
    status = rc_reg_ctl.SetRegister(&srp);

    /* Set the timer to auto mode, 5ms using operation control commands before HF is switched on to 
     * guarantee Iso14443-3 compliance of Polling procedure 
	 */
	status = SetTimeOut(&rc_reg_ctl, &rc_op_ctl, 5000, 1);
	if (status != PH_ERR_BFL_SUCCESS)
    {
		printf("[E] Could not set the timer of the PN51x.\n");
    } else
    {
		printf("[I] Timer of the PN51x reconfigured.\n");
    }
	
    /* Activate the field (automatically if there is no external field detected) */
    mrp.address = PHCS_BFL_JREG_TXAUTO;
    mrp.mask    = PHCS_BFL_JBIT_INITIALRFON | PHCS_BFL_JBIT_TX2RFAUTOEN | PHCS_BFL_JBIT_TX1RFAUTOEN;
    mrp.set     = 1;
    status = rc_reg_ctl.ModifyRegister(&mrp);
    if(status != PH_ERR_BFL_SUCCESS)
        printf("*** ERROR! ModifyRegister: Set=%1d, Address=%02X, Mask=%02X, Status = %04X \n", 
                mrp.set, mrp.address, mrp.mask, status);

    /* 
	 * After switching on the drivers wait until the timer interrupt occures, so that 
     * the field is on and the 5ms delay have been passed. 
	 */
    grp.address = PHCS_BFL_JREG_COMMIRQ;
    do {
        status = rc_reg_ctl.GetRegister(&grp);
    } 
    while(!(grp.reg_data & PHCS_BFL_JBIT_TIMERI) && status == PH_ERR_BFL_SUCCESS);
    /* Check status and diplay error if something went wrong */
    if(status != PH_ERR_BFL_SUCCESS)
        printf("*** ERROR! GetRegister: Address=%02X, Data=%02X, Status = %04X \n", 
                grp.address, grp.reg_data, status);

    /* Clear the status flag afterwards */
    srp.address  = PHCS_BFL_JREG_COMMIRQ;
    srp.reg_data = PHCS_BFL_JBIT_TIMERI;
    status = rc_reg_ctl.SetRegister(&srp);

    status = SetTimeOut(&rc_reg_ctl, &rc_op_ctl, 5000, 0);
	if (status != PH_ERR_BFL_SUCCESS)
    {
		printf("[E] Could not set the timer of the PN51x.\n");
    } else
    {
		printf("[I] Timer of the PN51x reconfigured.\n");
    }


    /* Note that this is only needed if the SIGIN/SIGOUT feature is used! */
	if (aSettings & 0x03)
	{
		if ( aSettings == 1 )
			opp.param_b = PHCS_BFLOPCTL_VAL_NOSIGINOUT;
		else if ( aSettings == 2 )
			opp.param_b = PHCS_BFLOPCTL_VAL_ONLYSIGINOUT;
		else
			opp.param_b = PHCS_BFLOPCTL_VAL_SIGINOUT;

		opp.group_ordinal = PHCS_BFLOPCTL_GROUP_SIGNAL;
		opp.attrtype_ordinal = PHCS_BFLOPCTL_ATTR_SIGINOUT;
		opp.param_a = PHCS_BFLOPCTL_VAL_READER; 
		status = rc_op_ctl.SetAttrib(&opp);
		if(status != PH_ERR_BFL_SUCCESS)
			printf("[E] Failed to set SIGINOUT features. Status = %04X \n", status);
	}
	
	/* 
	 * Following two register accesses are only needed if the interrupt pin is used 
	 * It doesn't hurt if polling is used instead.
	 */
	/* Set IRQInv to zero -> interrupt signal won't be inverted */
	mrp.address = PHCS_BFL_JREG_COMMIEN;
	mrp.mask    = 0x80;
	mrp.set     = 0x00;
	status = rc_reg_ctl.ModifyRegister(&mrp);

	/* Enable IRQPushPull so that the PN51x actively drives the signal */
	mrp.address = PHCS_BFL_JREG_DIVIEN;
	mrp.mask    = 0x80;
	mrp.set     = 0xFF;
	status = rc_reg_ctl.ModifyRegister(&mrp);

    /* Activate receiver for communication
       The RcvOff bit and the PowerDown bit are cleared, the command is not changed. */
    srp.address  = PHCS_BFL_JREG_COMMAND;
    srp.reg_data = PHCS_BFL_JCMD_NOCMDCHANGE;
    status = rc_reg_ctl.SetRegister(&srp);
    if(status != PH_ERR_BFL_SUCCESS)
        printf("*** ERROR! SetRegister: Address=%02X, Data=%02X, Status = %04x \n", 
                srp.address, srp.reg_data, status);

	/* Set timeout for REQA, ANTICOLL, SELECT to 200us */
	status = SetTimeOut(&rc_reg_ctl, &rc_op_ctl, 200, 0);
	if (status != PH_ERR_BFL_SUCCESS)
    {
		printf("[E] Could not set the timer of the PN51x.\n");
    } else
    {
		printf("[I] Timer of the PN51x reconfigured.\n");
    }

	/* 
	 * Do activation according to ISO14443A Part3 
     * Prepare the Request command 
	 */
    buffer[0] = 0;              
    buffer[1] = 0;
    req_p.req_code = PHCS_BFLI3P3A_REQIDL;    /* Set Request command code (or Wakeup: ISO14443_3_REQALL) */
    req_p.atq      = buffer;                        /* Let Request use the defined return buffer */
    status = iso14443_3.RequestA(&req_p);           /* Start Request command */

    /* 
	 * In normal operation this command returns one of the following three return codes:
	 * - PH_ERR_BFL_SUCCESS				At least one card has responded
	 * - PH_ERR_BFL_COLLISION_ERROR		At least two cards have responded
	 * - PH_ERR_BFL_IO_TIMEOUT			No response at all
	 */
    if(status == PH_ERR_BFL_SUCCESS || (status & ~PH_ERR_BFL_COMP_MASK) == PH_ERR_BFL_COLLISION_ERROR)
    {
        /* There was at least one response */
        if((status & ~PH_ERR_BFL_COMP_MASK) == PH_ERR_BFL_COLLISION_ERROR)
            printf("[W] Multiple Cards detected!! Collision occured during Request!\n");
        printf("[I] ATQA = %02X %02X \n", req_p.atq[0],req_p.atq[1]);
    } else
		/* No response at all */
        printf("*** ERROR! RequestA: RequestCode=%02X, ATQA=%02X %02X, Status = %04X \n", 
                req_p.req_code, req_p.atq[0],req_p.atq[1], status);

	/* Prepare data for combined anticollision/select command to get one complete ID */
    for(i=0;i<12;i++)
        serial[i]      = 0x00;      

    sel_p.uid          = serial;                 /* Put serial buffer for ID */
    sel_p.uid_length   = 0;                      /* No bits are known from ID, so set to 0 */
    /* Start Anticollision/Select command */
    status = iso14443_3.AnticollSelect(&sel_p);
    /* Check return code: If the command was OK, is always returns PH_ERR_BFL_SUCCESS.
     * If nothing was received, parameter uid_length is 0. */
    if(status == PH_ERR_BFL_SUCCESS)
    {
        /* Display UID and SAK if everything was ok */
        printf("[I] UID: ");
        for(i=0;i<(sel_p.uid_length/8);i++)
            printf("%02X", sel_p.uid[i]);
        printf("\tSAK: %02X \n",sel_p.sak);

		if ( (sel_p.flags & PHCS_BFLI3P3A_COLL_DETECTED) == PHCS_BFLI3P3A_COLL_DETECTED )
		{
			printf("[I] More than one card detected, collision has been resolved.\n");
		}

    } else
	{
        printf("*** ERROR! Anticollision/Select: Status = %04X \n", status);
	}

    /* 
	 * Up to this point the ISO14443-3A protocol has been used
	 * Now part 3 is finished and one has to choose between the several branches. Here is the
	 * example how to use the Mifare Standard specific command set of the BFL 
	 */

	if ( (sel_p.sak == 0x00) && (sel_p.uid_length/8 == 8) )
	{
		/* Looks like a Mifare UltraLight. Please consult the document "Type Identification
		 * Procedure" to work out the differences between Mifare UL and other cards. 
		 */
		printf("[I] This is a Mifare UltraLight (recognized by length of UID and content of second SAK).\n");

		/* This is just an example implementation how to write to a Mifare UltraLight card */
		/* perform Mifare Write 4 (only valid for Mifare Ultralight cards!!) */

		/* Response shall now be sent within 5ms */
		status = SetTimeOut(&rc_reg_ctl, &rc_op_ctl, 5000, 0);
	    if (status != PH_ERR_BFL_SUCCESS)
        {
		    printf("[E] Could not set the timer of the PN51x.\n");
        } else
        {
		    printf("[I] Timer of the PN51x reconfigured.\n");
        }

		/* Prepare and execute Mifare Read */
		mfr_p.cmd           = PHCS_BFLMFRD_READ;          
		mfr_p.addr          = 0x05;                 
		mfr_p.buffer        = buffer;               
		mfr_p.buffer_length = 0;                    
		status = mfrd.Transaction(&mfr_p);
		if(status != PH_ERR_BFL_SUCCESS)
			printf("[E] Error in MifareRead: Block=%02X, Status = %04X \n", mfr_p.addr, status);
		else
		{
			printf("[I] Mifare Read: Block=%02X, Data: ", mfr_p.addr);
			for(i=0;i<mfr_p.buffer_length;i++)
				printf("%02X",mfr_p.buffer[i]);
			printf("\n");
		}

		/* Response shall now be sent within 10ms */
		status = SetTimeOut(&rc_reg_ctl, &rc_op_ctl, 10000, 0);
	    if (status != PH_ERR_BFL_SUCCESS)
        {
		    printf("[E] Could not set the timer of the PN51x.\n");
        } else
        {
		    printf("[I] Timer of the PN51x reconfigured.\n");
        }

		/* Prepare and execute Mifare Write 4 */
		mfr_p.cmd           = PHCS_BFLMFRD_WRITE4;        
		mfr_p.addr          = 0x05;                 
		mfr_p.buffer_length = PHCS_BFLMFRD_UL_PAGE_SIZE;  
		for(i=0; i < mfr_p.buffer_length; i++)
			buffer[i] = (uint8_t)(i + 0x90);  
		mfr_p.buffer        = buffer;               
		status = mfrd.Transaction(&mfr_p);
		if(status != PH_ERR_BFL_SUCCESS)
			printf("[E] Error in MifareWrite4: Page=%02X, Status = %04X \n", mfr_p.addr, status);
		else
			printf("[I] Write4 successful.\n");

	} else
	{
		printf("[I] Assuming that this is a Mifare Standard card.\n");

		/* Set timeout for Authentication */
		status = SetTimeOut(&rc_reg_ctl, &rc_op_ctl, 10000, 0);
	    if (status != PH_ERR_BFL_SUCCESS)
        {
		    printf("[E] Could not set the timer of the PN51x.\n");
        } else
        {
		    printf("[I] Timer of the PN51x reconfigured.\n");
        }

		/* Prepare data for Mifare Authentication */
		index = 6;
		/* Copy UID to Tx buffer */
		for(i=0;i<4;i++)
        {
			buffer[index] = serial[i];
            index++;
        }
		/* Copy secret key to buffer */
		for(i=0;i<6;i++)
        {
			buffer[i] = 0xFF;
        }
		/* Initialize all the variables of authentication structure */
		mfr_p.cmd           = PHCS_BFLMFRD_AUTHENT_A;     // Set Authenticate with KeyA 
		mfr_p.addr          = 0x06;                 // Block 7
		mfr_p.buffer        = buffer;               // Set the mifare buffer to the one initialised
		mfr_p.buffer_length = 10;                   // 6 bytes of key and 4 bytes SNR in buffer
		/* Start the Mifare authentication */
		status = mfrd.Transaction(&mfr_p);
		/* Check return code and display error number if an error occured */
		if(status != PH_ERR_BFL_SUCCESS)
			printf("*** ERROR! Mifare Authentication: Command=%01X, Block=%02X, Status = %04X \n", 
					mfr_p.cmd, mfr_p.addr, status);
		else
			printf("Authentication of block %02X successful, continue operation.\n", mfr_p.addr);

	    /* Response shall now be sent within 5ms */
	    status = SetTimeOut(&rc_reg_ctl, &rc_op_ctl, 5000, 0);
	    if (status != PH_ERR_BFL_SUCCESS)
        {
		    printf("[E] Could not set the timer of the PN51x.\n");
        } else
        {
		    printf("[I] Timer of the PN51x reconfigured.\n");
        }

		/* Prepare data for Mifare Read */
		mfr_p.cmd           = PHCS_BFLMFRD_READ;          // set Read command
		mfr_p.addr          = 0x06;                 // Block 5, must be authenticated before
		mfr_p.buffer        = buffer;               // set return buffer to buffer
		mfr_p.buffer_length = 0;                    // nothing is handed over in the buffer
		/* Start Mifare Read */
		status = mfrd.Transaction(&mfr_p);
		/* Check return code and display error number if an error occured, else display data */
		if(status != PH_ERR_BFL_SUCCESS)
			printf("*** ERROR! MifareRead: Block=%02X, Status = %04X \n", mfr_p.addr, status);
		else
		{
			printf("Mifare Read: Block=%02X, Data: ", mfr_p.addr);
			for(i=0;i<mfr_p.buffer_length;i++)
				printf("%02X",mfr_p.buffer[i]);
			printf("\n");
		}

		/* Response shall now be sent within 10ms */
		status = SetTimeOut(&rc_reg_ctl, &rc_op_ctl, 10000, 0);
	    if (status != PH_ERR_BFL_SUCCESS)
        {
		    printf("[E] Could not set the timer of the PN51x.\n");
        } else
        {
		    printf("[I] Timer of the PN51x reconfigured.\n");
        }

		/* Prepare data for Mifare Write */
		mfr_p.cmd           = PHCS_BFLMFRD_WRITE ;        // set Write command
		mfr_p.addr          = 0x05;                 // Block 5, must be authenticated before
		mfr_p.buffer_length = PHCS_BFLMFRD_STD_BLOCK_SIZE; // 16 bytes in the buffer valid
		for(i=0; i < mfr_p.buffer_length; i++)
			buffer[i] = (uint8_t)(i + 0x10);  // configure data to write
		mfr_p.buffer        = buffer;               // set send buffer to buffer
		/* Start Mifare Write */
		status = mfrd.Transaction(&mfr_p);
		/* Check return code and display error number if an error occured, else display data */
		if(status != PH_ERR_BFL_SUCCESS)
			printf("*** ERROR! MifareWrite: Block=%02X, Status = %04X \n", mfr_p.addr, status);
		else
			printf("Write of block %02X successful, continue operation.\n", mfr_p.addr);

		/* Response shall be sent within 5ms */
		status = SetTimeOut(&rc_reg_ctl, &rc_op_ctl, 5000, 0);
	    if (status != PH_ERR_BFL_SUCCESS)
        {
		    printf("[E] Could not set the timer of the PN51x.\n");
        } else
        {
		    printf("[I] Timer of the PN51x reconfigured.\n");
        }

		/* Prepare data for Mifare Read again to check what has been written */
		mfr_p.cmd           = PHCS_BFLMFRD_READ;          
		mfr_p.addr          = 0x05;                 
		mfr_p.buffer        = buffer;               
		mfr_p.buffer_length = 0;                    
		status = mfrd.Transaction(&mfr_p);
 	    /* Check return code and display error number if an error occurred, else display data */
		if(status != PH_ERR_BFL_SUCCESS)
			printf("*** ERROR! MifareRead: Block=%02X, Status = %04X \n", mfr_p.addr, status);
		else
		{
			printf("Mifare Read: Block=%02X, Data: ", mfr_p.addr);
			for(i=0;i<mfr_p.buffer_length;i++)
				printf("%02X",mfr_p.buffer[i]);
			printf("\n");
		}


		/* 
		* Mifare Value Block operation example 
		*/

		/* Set timeout for authentication */
		status = SetTimeOut(&rc_reg_ctl, &rc_op_ctl, 1000, 0);
	    if (status != PH_ERR_BFL_SUCCESS)
        {
		    printf("[E] Could not set the timer of the PN51x.\n");
        } else
        {
		    printf("[I] Timer of the PN51x reconfigured.\n");
        }

		/* 
		* Prepare data for Mifare Authentication for example of Value block operation.
		* The same as in the previous example.
		*/
		index = 6;      
		for(i=0;i<4;i++)
        {
			buffer[index] = serial[i];
            index++;
        }
		for(i=0;i<6;i++)
        {
			buffer[i] = 0xFF;
        }
		mfr_p.cmd           = PHCS_BFLMFRD_AUTHENT_A;     
		mfr_p.addr          = 0x09;                 
		mfr_p.buffer        = buffer;               
		mfr_p.buffer_length = 10;                   
		status = mfrd.Transaction(&mfr_p);
		if(status != PH_ERR_BFL_SUCCESS)
			printf("*** ERROR! Mifare Authentication: Command=%01X, Block=%02X, Status = %04X \n", 
					mfr_p.cmd, mfr_p.addr, status);
		else
			printf("Authentication of block %02X successful, continue operation.\n", mfr_p.addr);

		/* Set timeouts */
		status = SetTimeOut(&rc_reg_ctl, &rc_op_ctl, 10000, 0);
	    if (status != PH_ERR_BFL_SUCCESS)
        {
		    printf("[E] Could not set the timer of the PN51x.\n");
        } else
        {
		    printf("[I] Timer of the PN51x reconfigured.\n");
        }

        /* Initialize Block 9 as value block with value 100 with Mifare write
		* Data in hex: 64 00 00 00 9B FF FF FF 64 00 00 00 09 F6 09 F6 */
		mfr_p.cmd           = PHCS_BFLMFRD_WRITE ;        
		mfr_p.addr          = 0x09;                 
		mfr_p.buffer_length = PHCS_BFLMFRD_STD_BLOCK_SIZE; 

		i=0;  
		/* init 1st 4 bytes with the specified value */
		buffer[i] = 0x64;  // 0x64 == 100
        i++;
		buffer[i] = 0x00;
        i++;
		buffer[i] = 0x00;
        i++;
		buffer[i] = 0x00;
        i++;
		/* 2nd 4 bytes must have bit-inverted value */
		for( ; i<8; i++)
        {
			buffer[i] = (uint8_t)(~buffer[i-4]);
        }
		/* 3rd 4 bytes must have same value as 1st 4 bytes */
		for( ; i<12; i++)
        {
			buffer[i] = buffer[i-8];
        }
		/* 4th 4 byte contain the address, twice plain, twice bit inverted */
		buffer[i] = mfr_p.addr;
        i++;
		buffer[i] = (uint8_t)(~mfr_p.addr);
        i++;
		buffer[i] = mfr_p.addr;
        i++;
		buffer[i] = (uint8_t)(~mfr_p.addr);
        i++;

		/* Set the send buffer pointer to the user buffer pointer */
		mfr_p.buffer        = buffer;               
		/* Start Mifare Write */
		status = mfrd.Transaction(&mfr_p);
		/* Check return code and display error number if an error occured, else display data */
		if(status != PH_ERR_BFL_SUCCESS)
			printf("*** ERROR! MifareWrite: Block=%02X, Status = %04X \n", mfr_p.addr, status);
		else
			printf("Write successful, continue operation.\n");

		/* Response shall be sent within 5ms */
		status = SetTimeOut(&rc_reg_ctl, &rc_op_ctl, 5000, 0);
	    if (status != PH_ERR_BFL_SUCCESS)
        {
		    printf("[E] Could not set the timer of the PN51x.\n");
        } else
        {
		    printf("[I] Timer of the PN51x reconfigured.\n");
        }

		/* Prepare data for Mifare Read again to check what has been written */
		mfr_p.cmd           = PHCS_BFLMFRD_READ;          
		mfr_p.addr          = 0x09;                 
		mfr_p.buffer        = buffer;               
		mfr_p.buffer_length = 0;                    
		status = mfrd.Transaction(&mfr_p);
		if(status != PH_ERR_BFL_SUCCESS)
			printf("*** ERROR! MifareRead: Block=%02X, Status = %04X \n", mfr_p.addr, status);
		else
		{
			printf("Mifare Read: Block=%02X, Data: ", mfr_p.addr);
			for(i=0;i<mfr_p.buffer_length;i++)
				printf("%02X",mfr_p.buffer[i]);
			printf("\n");
		}

		/* Set timeouts */
		status = SetTimeOut(&rc_reg_ctl, &rc_op_ctl, 5000, 0);
	    if (status != PH_ERR_BFL_SUCCESS)
        {
		    printf("[E] Could not set the timer of the PN51x.\n");
        } else
        {
		    printf("[I] Timer of the PN51x reconfigured.\n");
        }

		/* Use Mifare Increment to increase the value */
		mfr_p.cmd           = PHCS_BFLMFRD_INCREMENT;         
		mfr_p.addr          = 0x09;                     
		mfr_p.buffer_length = PHCS_BFLMFRD_VALUE_DATA_SIZE;   
		/* Increment by the value 16 */
		buffer[0] = 0x10;
		buffer[1] = 0x00;
		buffer[2] = 0x00;
		buffer[3] = 0x00;
		mfr_p.buffer        = buffer;
		status = mfrd.Transaction(&mfr_p);
		if(status != PH_ERR_BFL_SUCCESS)
			printf("*** ERROR! MifareIncrement: Block=%02X, Status = %04X \n", mfr_p.addr, status);
		else
			printf("Increment successful, continue operation.\n");

		/* Timeout set to 10ms */
		status = SetTimeOut(&rc_reg_ctl, &rc_op_ctl, 10000, 0);
	    if (status != PH_ERR_BFL_SUCCESS)
        {
		    printf("[E] Could not set the timer of the PN51x.\n");
        } else
        {
		    printf("[I] Timer of the PN51x reconfigured.\n");
        }

		/* Use the transfer directly afterwards to store data in EEProm */
		mfr_p.cmd           = PHCS_BFLMFRD_TRANSFER;
		mfr_p.addr          = 0x09;           
		/* Start Mifare Transfer command */
		status = mfrd.Transaction(&mfr_p);
        /* Check return code and display error number if an error occurred, else display data */
		if(status != PH_ERR_BFL_SUCCESS)
			printf("*** ERROR! MifareTransfer: Block=%02X, Status = %04X \n", mfr_p.addr, status);
		else
			printf("Transfer successful, continue operation.\n");

		/* Response shall be sent within 5ms */
		status = SetTimeOut(&rc_reg_ctl, &rc_op_ctl, 5000, 0);
	    if (status != PH_ERR_BFL_SUCCESS)
        {
		    printf("[E] Could not set the timer of the PN51x.\n");
        } else
        {
		    printf("[I] Timer of the PN51x reconfigured.\n");
        }

		/* Prepare data for Mifare Read again to check what has been written with increment */
		mfr_p.cmd           = PHCS_BFLMFRD_READ;          
		mfr_p.addr          = 0x09;                 
		mfr_p.buffer        = buffer;               
		mfr_p.buffer_length = 0;                    
		/* Start Mifare Read */
		status = mfrd.Transaction(&mfr_p);
        /* Check return code and display error number if an error occurred, else display data */
		if(status != PH_ERR_BFL_SUCCESS)
			printf("*** ERROR! MifareRead: Block=%02X, Status = %04X \n", mfr_p.addr, status);
		else
		{
			printf("Mifare Read: Block=%02X, Data: ", mfr_p.addr);
			for(i=0;i<mfr_p.buffer_length;i++)
				printf("%02X",mfr_p.buffer[i]);
			printf("\n");
		}

		/* Set timeouts */
		status = SetTimeOut(&rc_reg_ctl, &rc_op_ctl, 5000, 0);
	    if (status != PH_ERR_BFL_SUCCESS)
        {
		    printf("[E] Could not set the timer of the PN51x.\n");
        } else
        {
		    printf("[I] Timer of the PN51x reconfigured.\n");
        }

		/* Use Mifare Decrement to reduce the value */
        /* Attention! 
           There are 2 different types of blocks to be able to be used. One are standard value 
           blocks, the second one are value debit blocks which need to be initilized especially.
           For the value debit block NO transfer afterwards is needed! This command will fail! */
		mfr_p.cmd           = PHCS_BFLMFRD_DECREMENT;         
		mfr_p.addr          = 0x09;                     
		mfr_p.buffer_length = PHCS_BFLMFRD_VALUE_DATA_SIZE;   
		/* Decrement by the value 32 */
		buffer[0] = 0x20;
		buffer[1] = 0x00;
		buffer[2] = 0x00;
		buffer[3] = 0x00;
		mfr_p.buffer        = buffer;
		status = mfrd.Transaction(&mfr_p);
		if(status != PH_ERR_BFL_SUCCESS)
			printf("*** ERROR! MifareDecrement: Block=%02X, Status = %04X \n", mfr_p.addr, status);
		else
			printf("Decrement of block %02X successful, continue operation.\n", mfr_p.addr);

		/* Timeout set to 10ms */
		status = SetTimeOut(&rc_reg_ctl, &rc_op_ctl, 10000, 0);
	    if (status != PH_ERR_BFL_SUCCESS)
        {
		    printf("[E] Could not set the timer of the PN51x.\n");
        } else
        {
		    printf("[I] Timer of the PN51x reconfigured.\n");
        }

		/* Use transfer directly afterwards to store data in EEProm */
        /* Attention! 
           There are 2 different types of blocks to be able to be used. One are standard value 
           blocks, the second one are value debit blocks which need to be initilized especially.
           For the value debit block NO transfer afterwards is needed! This command will fail! */
		mfr_p.cmd           = PHCS_BFLMFRD_TRANSFER;          
		mfr_p.addr          = 0x09;                     
		/* Start Mifare Transfer */
		status = mfrd.Transaction(&mfr_p);
        /* Check return code and display error number if an error occurred, else display data */
		if(status != PH_ERR_BFL_SUCCESS)
			printf("*** ERROR! MifareTransfer: Block=%02X, Status = %04X \n", mfr_p.addr, status);
		else
			printf("Transfer to block %02X successful, continue operation.\n", mfr_p.addr);

		/* Response shall be sent within 5ms */
		status = SetTimeOut(&rc_reg_ctl, &rc_op_ctl, 5000, 0);
	    if (status != PH_ERR_BFL_SUCCESS)
        {
		    printf("[E] Could not set the timer of the PN51x.\n");
        } else
        {
		    printf("[I] Timer of the PN51x reconfigured.\n");
        }

		/* Prepare data for Mifare Read again to check what has been written with decrement */
		mfr_p.cmd           = PHCS_BFLMFRD_READ;          
		mfr_p.addr          = 0x09;                 
		mfr_p.buffer        = buffer;               
		mfr_p.buffer_length = 0;                    
		status = mfrd.Transaction(&mfr_p);
		if(status != PH_ERR_BFL_SUCCESS)
			printf("*** ERROR! MifareRead: Block=%02X, Status = %04X \n", mfr_p.addr, status);
		else
		{
			printf("Mifare Read: Block=%02X, Data: ", mfr_p.addr);
			for(i=0;i<mfr_p.buffer_length;i++)
				printf("%02X",mfr_p.buffer[i]);
			printf("\n");
		}

		/* Set timeouts */
		status = SetTimeOut(&rc_reg_ctl, &rc_op_ctl, 5000, 0);
	    if (status != PH_ERR_BFL_SUCCESS)
        {
		    printf("[E] Could not set the timer of the PN51x.\n");
        } else
        {
		    printf("[I] Timer of the PN51x reconfigured.\n");
        }

		/* Use restore to copy one block to another */
		mfr_p.cmd           = PHCS_BFLMFRD_RESTORE;         
		mfr_p.addr          = 0x09;                   
		status = mfrd.Transaction(&mfr_p);
		if(status != PH_ERR_BFL_SUCCESS)
			printf("*** ERROR! MifareRestore: Block=%02X, Status = %04X \n", mfr_p.addr, status);
		else
			printf("Restore of block %02X successful, continue operation.\n", mfr_p.addr);

		/* Set timeouts */
		status = SetTimeOut(&rc_reg_ctl, &rc_op_ctl, 10000, 0);
	    if (status != PH_ERR_BFL_SUCCESS)
        {
		    printf("[E] Could not set the timer of the PN51x.\n");
        } else
        {
		    printf("[I] Timer of the PN51x reconfigured.\n");
        }

		/* Use transfer directly afterwards to store data in EEProm */
		mfr_p.cmd           = PHCS_BFLMFRD_TRANSFER;          
		mfr_p.addr          = 0x08;                     
		/* Start Mifare Transfer */
		status = mfrd.Transaction(&mfr_p);
        /* Check return code and display error number if an error occurred, else display data */
		if(status != PH_ERR_BFL_SUCCESS)
			printf("*** ERROR! MifareTransfer: Block=%02X, Status = %04X \n", mfr_p.addr, status);
		else
			printf("Transfer to block %02X successful, continue operation.\n", mfr_p.addr);

		/* Response shall be sent within 5ms */
		status = SetTimeOut(&rc_reg_ctl, &rc_op_ctl, 5000, 0);
	    if (status != PH_ERR_BFL_SUCCESS)
        {
		    printf("[E] Could not set the timer of the PN51x.\n");
        } else
        {
		    printf("[I] Timer of the PN51x reconfigured.\n");
        }

		/* Prepare data for Mifare Read again to check what has been written */
		mfr_p.cmd           = PHCS_BFLMFRD_READ;          
		mfr_p.addr          = 0x08;                 
		mfr_p.buffer        = buffer;               
		mfr_p.buffer_length = 0;                    
		status = mfrd.Transaction(&mfr_p);
		if(status != PH_ERR_BFL_SUCCESS)
			printf("*** ERROR! MifareRead: Block=%02X, Status = %04X \n", mfr_p.addr, status);
		else
		{
			printf("Mifare Read: Block=%02X, Data: ", mfr_p.addr);
			for(i=0;i<mfr_p.buffer_length;i++)
				printf("%02X",mfr_p.buffer[i]);
			printf("\n");
		}

	/* This is just an example implementation how to write to a Mifare UltraLight card */
    /* perform Mifare Write 4 (only valid for Mifare UltraLight cards!!) */
	/*
	status = SetTimeOut(&rc_reg_ctl, &rc_op_ctl, 10000, 0);
	if (status != PH_ERR_BFL_SUCCESS)
    {
		printf("[E] Could not set the timer of the PN51x.\n");
    } else
    {
		printf("[I] Timer of the PN51x reconfigured.\n");
    }

	mfr_p.cmd           = PHCS_BFLMFRD_WRITE4;        
    mfr_p.addr          = 0x07;                 
    mfr_p.buffer_length = PHCS_BFLMFRD_UL_PAGE_SIZE;  
    for(i=0; i < mfr_p.buffer_length; i++)
        buffer[i] = (uint8_t)(i + 0x90);  
    mfr_p.buffer        = buffer;     */          
    /* Start Mifare Write 4 */
    /*
    status = mfrd.Transaction(&mfr_p);
    if(status != PH_ERR_BFL_SUCCESS)
        printf("*** ERROR! MifareWrite4: Page=%02X, Status = %04x \n", mfr_p.addr, status);
    else
        printf("Write4 successful, continue operation.\n");
    */
    
    /* 
	 * Do ISO14443-3 HaltA for deactivation of the card
	 */

		/* Set timeouts */
		status = SetTimeOut(&rc_reg_ctl, &rc_op_ctl, 1000, 0);
	    if (status != PH_ERR_BFL_SUCCESS)
        {
		    printf("[E] Could not set the timer of the PN51x.\n");
        } else
        {
		    printf("[I] Timer of the PN51x reconfigured.\n");
        }

		status = iso14443_3.HaltA(&hlt_p);
		/* Check return code: If timeout occures, PH_ERR_BFL_SUCCESS is returned */
		if(status == PH_ERR_BFL_SUCCESS)
		{
			printf("Halt probably successful, timeout occured\n");
		}
		else
		{
			printf("*** ERROR! HaltA: Status = %04X \n", status);
		}
	}

	return status;
}
