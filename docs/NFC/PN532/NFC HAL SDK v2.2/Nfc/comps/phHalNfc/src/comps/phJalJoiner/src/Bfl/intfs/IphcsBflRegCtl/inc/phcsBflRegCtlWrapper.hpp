/* /////////////////////////////////////////////////////////////////////////////////////////////////
//                     Copyright (c) Philips Semiconductors
//
//                       (C)PHILIPS Electronics N.V.2004
//         All rights are reserved. Reproduction in whole or in part is
//        prohibited without the written consent of the copyright owner.
//    Philips reserves the right to make changes without notice at any time.
//   Philips makes no warranty, expressed, implied or statutory, including but
//   not limited to any implied warranty of merchantability or fitness for any
//  particular purpose, or that the use will not infringe any third party patent,
//   copyright or trademark. Philips must not be liable for any loss or damage
//                            arising from its use.
///////////////////////////////////////////////////////////////////////////////////////////////// */

/*! 
 * \if cond_generate_filedocu 
 * \file phcsBflRegCtlWrapper.hpp
 *
 * Project: Object Oriented Library Framework RegCtl Component.
 *
 *  Source: phcsBflRegCtlWrapper.hpp
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Date: Thu Sep 27 09:39:01 2007 $
 *
 * Comment:
 *  None.
 *
 * History:
 *  GK:  Generated 12. March 2003
 *  MHa: Migrated to MoReUse September 2005
 * \endif
 *
 */

#ifndef PHCSBFLREGCTLWRAPPER_HPP
#define PHCSBFLREGCTLWRAPPER_HPP

#include <phcsBflStatus.h>

// Include the C-header files for used implementation:
extern "C"
{
    #include <phcsBflRegCtl.h>
    #include <phcsBflRegCtl.h>
//    #include <phcsBflRegCtl_I2CHw1.c>
//    #include <phcsBflRegCtl_SpiHw1.c>
//    #include <phcsBflRegCtl_ParaHw1.c>
}

/* \ingroup regctl
 * \brief Namespace used for complete library to avoid namspace clashes.
 */
namespace phcs_BFL
{

/*! \cond cond_Cpp_complete */
/* \ingroup regctl
 *  \brief RC register control abstract class, used for the C++ interface.
 */
class phcsBfl_RegCtl
{
    public:
        virtual phcsBfl_Status_t SetRegister(phcsBflRegCtl_SetRegParam_t *setreg_param)                   = 0;
        virtual phcsBfl_Status_t GetRegister(phcsBflRegCtl_GetRegParam_t *getreg_param)                   = 0;
        virtual phcsBfl_Status_t ModifyRegister(phcsBflRegCtl_ModRegParam_t *modify_param)                = 0;
        virtual phcsBfl_Status_t SetRegisterMultiple(phcsBflRegCtl_SetMultiRegParam_t *setmultireg_param) = 0;
        virtual phcsBfl_Status_t GetRegisterMultiple(phcsBflRegCtl_GetMultiRegParam_t *getmultireg_param) = 0;

        /* Lower edge interface: Not to be redefined in the derived class: */
        class phcsBfl_Bal *mp_Lower;
};


// Function pointer definition for C- RegCtlWrapper Initialise function:
typedef void (*pphcsBflRegCtl_Initialize_t) (phcsBflRegCtl_t*, void*, phcsBflBal_t*);

/* \ingroup regctl
 *  This is the GLUE class the C-kernel RegCtl "sees" when calling down the stack. The glue class
 *  represents the C-API of the lower layer (Bal).
 */
class phcsBfl_RegCtlGlue
{
    public:
        phcsBfl_RegCtlGlue(void);
        ~phcsBfl_RegCtlGlue(void);
        
        // Static functions WriteBus, able to call into C++ again.
        static phcsBfl_Status_t WriteBus(phcsBflBal_WriteBusParam_t *writebus_param);
        static phcsBfl_Status_t ReadBus(phcsBflBal_ReadBusParam_t *readbus_param);
        
        // Structure accessible from the upper object's pure C-kernel.
        phcsBflBal_t m_GlueStruct;
};
/*! \endcond */


/*! \ingroup regctl
 *  This is the main interface of RegCtl. The class is a template because it cannot be foreseen
 *  which type of C-kernel to wrap around. Depending on the implementation of this kernel, the
 *  type of the internal C-member structure changes (avoid multiple definitions of the same structure
 *  name for different structures when more than one type of functionality is used). The type of the
 *  MEMBERS (in kernel's .h file) structure is the one to specify when creating an instance
 *  of this class.
 */
template <class phcsBflRCcp> class phcsBflRegCtl_Wrapper : public phcsBfl_RegCtl
{
    public:
        phcsBflRegCtl_Wrapper(void);
        ~phcsBflRegCtl_Wrapper(void);

        /*
        * \ingroup regctl
        * \param[in]  c_rc_reg_ctl_initialise 
        *                  Pointer to the Initialise function of the type of C-kernel (for
        *                  each hardware variant an own kernel must exist) to use with the
        *                  current wrapper instance.
        * \param[in] *p_lower 
        *                  Pointer to the C++ object of the underlying layer.
        *
        * \note The alternative Constructor does setup required by the Component but not done
        *       by the default constructor.
        *  
        */
        phcsBflRegCtl_Wrapper(pphcsBflRegCtl_Initialize_t  c_rc_reg_ctl_initialise,
                              phcsBfl_Bal                 *p_lower);

        /*!
        * \ingroup regctl
        * \param[in]  c_rc_reg_ctl_initialise
        *                  Pointer to the Initialise function of the type of C-kernel (for
        *                  each hardware variant an own kernel must exist) to use with the
        *                  current wrapper instance.
        * \param[in] *p_lower
        *                  Pointer to the C++ object of the underlying layer.
        *
        * \note The Initialise method does setup required by the Component but not done
        *       by the default constructor.
        *  
        */
        void Initialise(pphcsBflRegCtl_Initialize_t  c_rc_reg_ctl_initialise,
                        phcsBfl_Bal                 *p_lower);
        
        phcsBfl_Status_t SetRegister(phcsBflRegCtl_SetRegParam_t *setreg_param);
        phcsBfl_Status_t GetRegister(phcsBflRegCtl_GetRegParam_t *getreg_param);
        phcsBfl_Status_t ModifyRegister(phcsBflRegCtl_ModRegParam_t *modify_param);
        phcsBfl_Status_t SetRegisterMultiple(phcsBflRegCtl_SetMultiRegParam_t *setmultireg_param);
        phcsBfl_Status_t GetRegisterMultiple(phcsBflRegCtl_GetMultiRegParam_t *getmultireg_param);

        /*! \brief Constructor status: This status gives information about whether the object has been
         *  allocated/configured correctly or not. It is a good idea to check this status (verify
         *  that it is set to PH_ERR_BFL_SUCCESS) before proceeding.
         */
        phcsBfl_Status_t    m_ConstructorStatus;
        phcsBflRegCtl_t     m_Interface;                  /*!< \brief C-interface instance, keeps a possibility 
                                                           *   to directly manipulate the pure C instance. */
        phcsBflRCcp         m_CommunicationParameters;    /*!< \brief C-interface internal variables. */

    protected:
        class phcsBfl_RegCtlGlue   m_RcRegCtlGlue;        /*!< \brief Glue class for connecting the C-part
                                                           *   with the C++ wrapper. */
};


////////////////////////////////////////////////////////////////////////////////////////////////////
// phcsBflRegCtl_Wrapper Wrapper Implementation, must reside here because of template usage:
////////////////////////////////////////////////////////////////////////////////////////////////////

/* \ingroup regctl
 * Template definition for phcsBflRegCtl_Wrapper Constructor without initialisation. */
template <class phcsBflRCcp>
phcsBflRegCtl_Wrapper<phcsBflRCcp>::phcsBflRegCtl_Wrapper(void)
{
    m_ConstructorStatus = PH_ERR_BFL_SUCCESS;
    // This constructor does no initialisation. Initialise() is needed.
}


/* \ingroup regctl
 * Template definition for phcsBflRegCtl_Wrapper Constructor with initialisation. */
template <class phcsBflRCcp>
phcsBflRegCtl_Wrapper<phcsBflRCcp>::phcsBflRegCtl_Wrapper(pphcsBflRegCtl_Initialize_t  c_rc_reg_ctl_initialise,
                                                    phcsBfl_Bal                 *p_lower)
{
    m_ConstructorStatus = PH_ERR_BFL_SUCCESS;
    // Hand over the pointer of the C-kernel's initialise function:
    this->Initialise(c_rc_reg_ctl_initialise, p_lower);
}


/* \ingroup regctl
 * Template definition for phcsBflRegCtl_Wrapper Destructor */
template <class phcsBflRCcp>
phcsBflRegCtl_Wrapper<phcsBflRCcp>::~phcsBflRegCtl_Wrapper(void)
{
    // No action.
}


/* \ingroup regctl
 * Template definition for phcsBflRegCtl_Wrapper Initialisation function. */
template <class phcsBflRCcp>
void phcsBflRegCtl_Wrapper<phcsBflRCcp>::Initialise(pphcsBflRegCtl_Initialize_t  c_rc_reg_ctl_initialise,
                                                phcsBfl_Bal                 *p_lower)
{
    // This binds the C-kernel to the C++ wrapper:
    c_rc_reg_ctl_initialise(&m_Interface,
                            &m_CommunicationParameters,
                            &(m_RcRegCtlGlue.m_GlueStruct));

    // Initialise C++ part, beyond of the scope of the C-kernel:
    this->mp_Lower = p_lower;
}


// Finally, the members (interface) calling into the C-kernel:

/*  \ingroup regctl
 *  \param[in] *setreg_param    Pointer to the I/O parameter structure
 *  \return                     phcsBfl_Status_t value depending on the C implementation
 *
 * Template definition for Member function SetRegister. */
template <class phcsBflRCcp>
phcsBfl_Status_t phcsBflRegCtl_Wrapper<phcsBflRCcp>::SetRegister(phcsBflRegCtl_SetRegParam_t *setreg_param)
{
    phcsBfl_Status_t   status;
    void       *p_self;

    p_self = setreg_param->self;
    setreg_param->self = &m_Interface;
    ((phcsBflBal_t*)(((phcsBflRegCtl_t*)(setreg_param->self))->mp_Lower))->mp_CallingObject = this;
    status = m_Interface.SetRegister(setreg_param);
    setreg_param->self = p_self;

    return status;
}


/*  \ingroup regctl
 *  \param[in] *getreg_param    Pointer to the I/O parameter structure
 *  \return                     phcsBfl_Status_t value depending on the C implementation
 *
 * Template definition for Member function GetRegister. */
template <class phcsBflRCcp>
phcsBfl_Status_t phcsBflRegCtl_Wrapper<phcsBflRCcp>::GetRegister(phcsBflRegCtl_GetRegParam_t *getreg_param)
{
    phcsBfl_Status_t   status;
    void       *p_self;

    p_self = getreg_param->self;
	// Pointer to phcsBflRegCtl_t
    getreg_param->self = &m_Interface;
	// Provide the phcsBfl_Bal the information where the phcsBflRegCtl_Wrapper can be found
    (((phcsBflBal_t*)(((phcsBflRegCtl_t*)(getreg_param->self))->mp_Lower))->mp_CallingObject) = this;
    status = m_Interface.GetRegister(getreg_param);
    getreg_param->self = p_self;

    return status;
}


/*  \ingroup regctl
 *  \param[in] *modify_param    Pointer to the I/O parameter structure
 *  \return                     phcsBfl_Status_t value depending on the C implementation
 *
 * Template definition for Member function ModifyRegister. */
template <class phcsBflRCcp>
phcsBfl_Status_t phcsBflRegCtl_Wrapper<phcsBflRCcp>::ModifyRegister(phcsBflRegCtl_ModRegParam_t *modify_param)
{
    phcsBfl_Status_t   status;
    void       *p_self;

    p_self = modify_param->self;
    modify_param->self = &m_Interface;
    ((phcsBflBal_t*)(((phcsBflRegCtl_t*)(modify_param->self))->mp_Lower))->mp_CallingObject = this;
    status = m_Interface.ModifyRegister(modify_param);
    modify_param->self = p_self;

    return status;
}


/*  \ingroup regctl
 *  \param[in] *setmultireg_param   Pointer to the I/O parameter structure
 *  \return                         phcsBfl_Status_t value depending on the C implementation
 *
 * Template definition for Member function SetRegisterMultiple. */
template <class phcsBflRCcp>
phcsBfl_Status_t phcsBflRegCtl_Wrapper<phcsBflRCcp>::SetRegisterMultiple(phcsBflRegCtl_SetMultiRegParam_t *setmultireg_param)
{
    phcsBfl_Status_t   status;
    void       *p_self;

    p_self = setmultireg_param->self;
    setmultireg_param->self = &m_Interface;
    ((phcsBflBal_t*)(((phcsBflRegCtl_t*)(setmultireg_param->self))->mp_Lower))->mp_CallingObject = this;
    status = m_Interface.SetRegisterMultiple(setmultireg_param);
    setmultireg_param->self = p_self;

    return status;
}


/*  \ingroup regctl
 *  \param[in] *getmultireg_param   Pointer to the I/O parameter structure
 *  \return                         phcsBfl_Status_t value depending on the C implementation
 *
 * Template definition for Member function GetRegisterMultiple. */
template <class phcsBflRCcp>
phcsBfl_Status_t phcsBflRegCtl_Wrapper<phcsBflRCcp>::GetRegisterMultiple(phcsBflRegCtl_GetMultiRegParam_t *getmultireg_param)
{
    phcsBfl_Status_t   status;
    void       *p_self;

    p_self = getmultireg_param->self;
    getmultireg_param->self = &m_Interface;
    ((phcsBflBal_t*)(((phcsBflRegCtl_t*)(getmultireg_param->self))->mp_Lower))->mp_CallingObject = this;
    status = m_Interface.GetRegisterMultiple(getmultireg_param);
    getmultireg_param->self = p_self;

    return status;
}

}   /* phcs_BFL */

#endif /* PHCSBFLREGCTLWRAPPER_HPP */
