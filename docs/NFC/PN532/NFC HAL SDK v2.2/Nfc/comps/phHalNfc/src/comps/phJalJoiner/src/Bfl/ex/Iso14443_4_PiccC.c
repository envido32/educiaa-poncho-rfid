/* /////////////////////////////////////////////////////////////////////////////////////////////////
//                     Copyright (c) Philips Semiconductors
//
//                       (C)PHILIPS Electronics N.V.2004
//         All rights are reserved. Reproduction in whole or in part is
//        prohibited without the written consent of the copyright owner.
//    Philips reserves the right to make changes without notice at any time.
//   Philips makes no warranty, expressed, implied or statutory, including but
//   not limited to any implied warranty of merchantability or fitness for any
//  particular purpose, or that the use will not infringe any third party patent,
//   copyright or trademark. Philips must not be liable for any loss or damage
//                            arising from its use.
///////////////////////////////////////////////////////////////////////////////////////////////// */


/*! \file Iso14443_4_PiccC.c
 *
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Date: Thu Sep 27 09:38:48 2007 $
 *
 */

/* For debugging reasons */
#include <stdio.h>

/* Common headers */
#include <phcsBflHw1Reg.h>
// #include <rclstruct.h>

#include "iso14443_4_picc.h"

#include "Iso14443_4_PiccC.h"

const unsigned char gATS[] = {	0x09,	/* Length of ATS including length itself */
								0x47,	/* TC(1) follows, 7 == FSCI == 128 Byte Buffer */
								0x00,   /* NAD, CID not supported */
								'P',	/* T1 ... */
								'N',
								'5',
								'1',
								'1',
								'\0' };

unsigned char gMemoryCard[48];

void InitMemoryCard()
{
	int i;

	for (i = 0; i < 48; i++)
		gMemoryCard[i] = (unsigned char) (48 - i);
}


RCLSTATUS Iso14443_4_Picc_Callback(C_I3P4_PICC_ENDPOINT_PARAM *endpoint_params)
{
	RCLSTATUS status;
	int i;
	unsigned char blockNr;

	switch(endpoint_params->action_type)
	{
	case PCD_RATS:
		/* Do some initialization for the memory card */
		InitMemoryCard();

		// Now prepare the real response
		printf("[I] Got an RATS. Preparing an ATS.\n");
		for (i = 0; i < gATS[0]; i++)
			endpoint_params->snd_buffer[i] = gATS[i];
		endpoint_params->snd_buffer_length = gATS[0];
		status = RCLSTATUS_SUCCESS;
		break;
	
	case PCD_PPS:
		printf("[I] Got a PPS.\n");
		status = RCLSTATUS_SUCCESS;
		break;

	case PCD_PCB_S_WTX:
		printf("[I] Got a WTX S-Block.\n");
		/* Two options are legal:
		 * - Send I data
		 * - Send another S(WTX)
		 */
		status = RCLSTATUS_DESELECTED;
		break;

	case PCD_PCB_S_DSL:
		/*
		 * DSL_RES has to be returned, there is no other valid option.
		 */
		printf("[I] Got a Deselect S-Block.\n");
		status = RCLSTATUS_DESELECTED;
		break;

	case PCD_PCB_I_0:
	case PCD_PCB_I_1:
		printf("[I] Got an I-Block.\n");
		/* Following options are valid:
		 * - Send an I(0)
		 * - Send an I(1)
		 * - Send a S(WTX)
		 * - Send a R(ACK)
		 */

		/* PCB, CID, NAD is done automatically */

		switch(endpoint_params->rcv_buffer[0])
		{
		case 0x30:
			blockNr = endpoint_params->rcv_buffer[1];
			for (i=0; i < 16; i++)
				endpoint_params->snd_buffer[i] = gMemoryCard[blockNr*4 + i];
			endpoint_params->snd_buffer[16] = 0x90;
			endpoint_params->snd_buffer[17] = 0x00;
			endpoint_params->snd_buffer_length = 18;
			break;
		case 0xA2:
			blockNr = endpoint_params->rcv_buffer[1];
			for (i=0; i < 4; i++)
				gMemoryCard[blockNr*4+i] = endpoint_params->rcv_buffer[i+3];
			endpoint_params->snd_buffer[0] = 0x90;
			endpoint_params->snd_buffer[1] = 0x00;
			endpoint_params->snd_buffer_length = 2;
			break;
		default:
			endpoint_params->snd_buffer[16] = 0x88;
			endpoint_params->snd_buffer[17] = 0x00;
			endpoint_params->snd_buffer_length = 2;
			break;
		}

		status = RCLSTATUS_SUCCESS;
		/* status = RCLSTATUS_TARGET_SET_TOX; */
		break;


	case PCD_PCB_R_ACK_RETRANS:
	case PCD_PCB_R_NAK_RETRANS:

	case PCD_PCB_R_I:
	case PCD_PCB_R_ACK:
		printf("[I] Got an R-Block.\n");
		/* R-Block could mean:
		 * - Retransmit last block
		 * - Continue chaining
		 * - Send an R(ACK)
		 */
		status = RCLSTATUS_SUCCESS;
		break;

	default:
		printf("[E] Unsupported Command.\n");
		status = RCLSTATUS_PROTOCOL_ERROR;
	}

	return status;
}
