/* /////////////////////////////////////////////////////////////////////////////////////////////////
//                     Copyright (c) Philips Semiconductors
//
//                       (C)PHILIPS Electronics N.V.2004
//         All rights are reserved. Reproduction in whole or in part is
//        prohibited without the written consent of the copyright owner.
//    Philips reserves the right to make changes without notice at any time.
//   Philips makes no warranty, expressed, implied or statutory, including but
//   not limited to any implied warranty of merchantability or fitness for any
//  particular purpose, or that the use will not infringe any third party patent,
//   copyright or trademark. Philips must not be liable for any loss or damage
//                            arising from its use.
///////////////////////////////////////////////////////////////////////////////////////////////// */

/*! 
 * \if cond_generate_filedocu 
 * \file phcsBflIdManWrapper.hpp
 *
 * Project: ID Manager.
 *
 * Workfile: phcsBflIdManWrapper.hpp
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Date: Thu Sep 27 09:38:53 2007 $
 * $KeysEnd$
 *
 * Comment:
 *  
 *
 * History:
 *  BS:  Generated 24.Mar.2004
 *  MHa: Migrated to MoReUse September 2005
 * \endif
 *
 */

#ifndef PHCSBFLIDMANWRAPPER_HPP
#define PHCSBFLIDMANWRAPPER_HPP


extern "C"
{
    #include <phcsBflIdMan.h>
}

namespace phcs_BFL
{

/*! \cond cond_Cpp_complete */
/*
 *  \ingroup id_man
 *  \brief C++ wrapper for ID-Manager:
 */
class phcsBfl_IdManger
{
	public:
		virtual phcsBfl_Status_t GetFreeID(phcsBflIdMan_Param_t *idman_param)                               = 0;
		virtual phcsBfl_Status_t IsFreeID(phcsBflIdMan_Param_t *idman_param)                                = 0;
		virtual phcsBfl_Status_t AssignID(phcsBflIdMan_Param_t *idman_param)                                = 0;
		virtual phcsBfl_Status_t FreeIDByNumber(phcsBflIdMan_Param_t *idman_param)                          = 0;
		virtual phcsBfl_Status_t FreeIDByInstance(phcsBflIdMan_Param_t *idman_param)                        = 0;
};
/*! \endcond */

////////////////////////////////////////////////////////////////////////////////////////////////////
/*! \ingroup id_man
 *  \brief The ID Manager class wrapper is a helper service for the ISO 144443-4 and NFC Initiator
 *  protocols.
 */
class phcsBflIdMan_Wrapper : public phcsBfl_IdManger
{
    public : 
        phcsBflIdMan_Wrapper(void);
        ~phcsBflIdMan_Wrapper(void);
     
        phcsBfl_Status_t GetFreeID(phcsBflIdMan_Param_t *idman_param);
        phcsBfl_Status_t IsFreeID(phcsBflIdMan_Param_t *idman_param);
        phcsBfl_Status_t AssignID(phcsBflIdMan_Param_t *idman_param);
        phcsBfl_Status_t FreeIDByNumber(phcsBflIdMan_Param_t *idman_param);
        phcsBfl_Status_t FreeIDByInstance(phcsBflIdMan_Param_t *idman_param);

        /*! \brief Constructor status: This status gives information about whether the object has been
         *  allocated/configured correctly or not. It is a good idea to check this status (verify
         *  that it is set to PH_ERR_BFL_SUCCESS) before proceeding.
         */
        phcsBfl_Status_t             m_ConstructorStatus;

        /*! \brief C-interface internal members, referenced by the mp_Members pointer in the C-structure. */
        phcsBflIdMan_InitParams_t    m_IdmanParam;

        /*! \brief The C-interface (kernel and function implementation) itself. */
        phcsBflIdMan_t    m_Interface;
};

}   /* phcs_BFL */

#endif  /* PHCSBFLIDMANWRAPPER_HPP */

/* E.O.F. */
