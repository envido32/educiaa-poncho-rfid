/* /////////////////////////////////////////////////////////////////////////////////////////////////
//                     Copyright (c) NXP Semiconductors
//
//                       (C)NXP Electronics N.V.2004
//         All rights are reserved. Reproduction in whole or in part is
//        prohibited without the written consent of the copyright owner.
//    NXP reserves the right to make changes without notice at any time.
//   NXP makes no warranty, expressed, implied or statutory, including but
//   not limited to any implied warranty of merchantability or fitness for any
//  particular purpose, or that the use will not infringe any third party patent,
//   copyright or trademark. NXP must not be liable for any loss or damage
//                            arising from its use.
///////////////////////////////////////////////////////////////////////////////////////////////// */

/*! 
 * \if cond_generate_filedocu 
 * \file phcsBflFelRd.h
 *
 * Project: Object Oriented Library Framework FeliCa Component.
 *
 *  Source: phcsBflFelRd.h
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Date: Thu Sep 27 09:38:52 2007 $
 *
 * Comment:
 *  None.
 *
 * History:
 *  GK:  Generated 29. April 2003
 *  MHa: Migrated to MoReUse September 2005
 * \endif
 *
 */


#ifndef PHCSBFLFELRD_H
#define PHCSBFLFELRD_H

#include <phcsBflIo.h>

/* USER - relevant definitions, understood by this component: */

/*! \name Size Definitions
 *  \brief These definitions are important for the caller (user). The sizes are also internally checked 
 *  and used for request/response processing.
 */
/*@{*/
/*!  \ingroup felrd */
    #define PHCS_BFLFELRD_MAX_NB_SERV                    ((uint8_t)0x10)           //!< Maximum number of service
    #define PHCS_BFLFELRD_MAX_CHECK_NB_BLOCK             ((uint8_t)0x0C)           //!< Maximum number of block within check command
    #define PHCS_BFLFELRD_CHECK_TIME_POS                 ((uint8_t)0x0D)           //!< Maximum check response time position in Manufacture ID
    #define PHCS_BFLFELRD_IDM_LEN                        ((uint8_t)0x08)           //!< Maximum Length of Manufacture ID
    #define PHCS_BFLFELRD_PART_A_MASK                    ((uint8_t)0x07)           //!< Mask for part A in Maximum response time
    #define PHCS_BFLFELRD_PART_B_MASK                    ((uint8_t)0x38)           //!< Mask for part B in Maximum response time
    #define PHCS_BFLFELRD_PART_B_SHIFT                   ((uint8_t)0x03)           //!< Shift for part B in Maximum response time
    #define PHCS_BFLFELRD_PART_E_MASK                    ((uint8_t)0xC0)           //!< Mask for part E in Maximum response time
    #define PHCS_BFLFELRD_PART_E_SHIFT                   ((uint8_t)0x06)           //!< Shift for part E in Maximum response time

    #define PHCS_BFLFELRD_MAX_UPDATE_NB_BLOCK            ((uint8_t)0x08)           //!< Maximum number of block within update command
    #define PHCS_BFLFELRD_ACCESS_ATTR_MASK               ((uint8_t)0x3F)           //!< Access attribute mask
    #define PHCS_BFLFELRD_READ_WRITE_ACCESS              ((uint8_t)0x09)           //!< Access attribute is read/write
    #define PHCS_BFLFELRD_ACCESS_CODE                    ((uint8_t)0x00)           //!< Access code
    #define PHCS_BFLFELRD_ACCESS_CODE_MASK               ((uint8_t)0x70)           //!< Access code mask
    #define PHCS_BFLFELRD_LEN_MASK                       ((uint8_t)0x80)           //!< Len mask
    #define PHCS_BFLFELRD_LEN_TWO_BYTE                   ((uint8_t)0x80)           //!< Block list element is two bytes long
    #define PHCS_BFLFELRD_UPDATE_TIME_POS                ((uint8_t)0x0E)           //!< Maximum update response time position in Manufacture ID


    #define PH_RFPROT_FERD_POLLING_CMD                    ((uint8_t)0x00)           //!< Polling command code
    #define PH_RFPROT_FERD_POLLING_CMD_LENGTH             ((uint8_t)0x06)           //!< Polling command length
    #define PH_RFPROT_FERD_POLLING_NO_REQ                 ((uint8_t)0x00)           //!< No request
    #define PH_RFPROT_FERD_POLLING_SYSTEM_CODE_REQ        ((uint8_t)0x01)           //!< System code is requested
    #define PH_RFPROT_FERD_POLLING_BIT_RATE_REQ           ((uint8_t)0x02)           //!< Bit rate is requested


    /* Polling response */
    #define PH_RFPROT_FERD_POLLING_RSP                    ((uint8_t)0x01)           //!< Polling response code
    #define PH_RFPROT_FERD_POLLING_RSP_LENGTH_1           ((uint8_t)0x12)           //!< Polling response length
    #define PH_RFPROT_FERD_POLLING_RSP_LENGTH_2           ((uint8_t)0x14)

    /* IDM */
    #define PH_RFPROT_FERD_IDM_LENGTH                     ((uint8_t)0x08)           //!< Felica IDM Length
    #define PH_RFPROT_FERD_IDM_POS                        ((uint8_t)0x03)           //!< Felica IDM Starting Position in the bit Sequence


    /* Check command */
    #define PH_RFPROT_FERD_CHECK_CMD                      ((uint8_t)0x06)           //!< Check command code
    #define PH_RFPROT_FERD_CHECK_RSP                      ((uint8_t)0x07)           //!< Check response code
    #define PH_RFPROT_FERD_CHECK_RSP_MAX_LENGTH           ((uint8_t)0x40)           //!< Maximum Check response length

    /* Update command */
    #define PH_RFPROT_FERD_UPDATE_CMD                     ((uint8_t)0x08)           //!< Update command code
    #define PH_RFPROT_FERD_UPDATE_RSP                     ((uint8_t)0x09)           //!< Update response code
    #define PH_RFPROT_FERD_UPDATE_RSP_LENGTH              ((uint8_t)0x40)           //!< Update response length
    #define PH_RFPROT_FERD_UPDATE_REQ_LENGTH              ((uint8_t)0x40)           //!< Maximum Check request length
/*@}*/

/*! \ingroup felrd
 *  \brief Parameter structure for FeliCa Reader functionality.
 */
typedef struct 
{
    /*! \brief [In] References data received from NFC Forum 
                    Type 3 Tag(s) in following order:       \n
                    byte  00   :   length byte of first received   
                    polling response data stream                \n
                        Valid range:   POLLING_RESPONSE_LENGTH_1 \n
                                        POLLING_RESPONSE_LENGTH_2 \n\n

                    bytes 01   :   Polling response code        \n
                        Valid range:   0x01                      \n\n

                    bytes 02-09:    IDm of the NFC Forum Type 3 Tag  \n
                    bytes 10-17:    PMm of the NFC Forum Type 3 Tag  \n
                    bytes 18-19:    Request data (if any length will be 20)*/
    uint8_t     *poll_resp;

    void     *self;  /*!< \brief [in] Pointer to the C-interface of this component in   
                                     order to let this member function find its "object". 
                                     Should be left dangling when calling the C++ wrapper.        */

    /*! \brief [In] Buffer to send            \n
               [Out] Received Buffer */
    uint8_t     *txBuffer;

    /*! \brief [In] Buffer to send            \n
               [Out] Received Buffer */
    uint8_t     *rxBuffer;

    /*! \brief [In] Number of bytes to send                  \n
               [Out] Number of received bytes */
    uint8_t     txLength;

    /*! \brief [In] Number of bytes to send                  \n
               [Out] Number of received bytes */
    uint8_t     rxLength;

    /* [Out] Status-flag1 and Status-flag2 */
    uint8_t    flags[2];

} phcsBflFelRd_CommandParam_t;

/*  C-interface member function pointer types: */

/*!
 * \ingroup felrd
 *  \par Parameters:
 *  \li \ref phcsBflFelRd_CommandParam_t [in]: Pointer to the I/O parameter structure.
 *
 *  \retval  PH_ERR_BFL_SUCCESS                Operation successful.
 *  \retval  Other                             Depending on implementation and underlaying component.
 *
 *  \brief The main FELICA CHECK reader protocol entry point. 
 *
 *  All FELICA functionality is concentrated in this place. According to the cmd parameter all 
 *  possible FELICA commands are handled. \n
 *  For a detailed description of all commands refer to a FELICA documantation.
 *
 */
typedef phcsBfl_Status_t (*pphcsBflFelRd_Check_t)   (phcsBflFelRd_CommandParam_t *); /* Felica Update command */


/*!
 *
 * \ingroup felrd
 *  \par Parameters:
 *  \li \ref phcsBflFelRd_CommandParam_t [in]: Pointer to the I/O parameter structure.
 *
 *  \retval  PH_ERR_BFL_SUCCESS                Operation successful.
 *  \retval  Other                             Depending on implementation and underlaying component.
 *
 *  \brief The main FELICA UPDATE reader protocol entry point. 
 *
 *  All FELICA functionality is concentrated in this place. According to the cmd parameter all 
 *  possible FELICA commands are handled. \n
 *  For a detailed description of all commands refer to a FELICA documantation.
 *
 */
typedef phcsBfl_Status_t (*pphcsBflFelRd_Update_t)   (phcsBflFelRd_CommandParam_t *); /* Felica Check command */



/*! \ifnot sec_PHFL_BFL_CPP
 * 
 *  \ingroup felrd
 *  \brief C - Component interface: Structure to call MIFARE Reader functionality.
 *
 *  This interface exposes the MIFARE Reader functionality without the ISO 14443.3 Initialisation
 *  and Anticollision part.
 * 
 * \endif
 */
typedef struct 
{
    /* Methods: */
    pphcsBflFelRd_Check_t   Check;
    pphcsBflFelRd_Update_t  Update;

    void *mp_Members;   /* Internal variables of the C-interface. Usually a structure is behind
                           this pointer. The type of the structure depends on the implementation
                           requirements. */
    #ifdef PHFL_BFL_CPP
        void       *mp_CallingObject;   /* Used by the "Glue-Class" to reference the wrapping
                                           C++ object, calling into the C-interface. */
    #endif

    phcsBflIo_t *mp_Lower;          /* Point to the lower I/O device: */
} phcsBflFelRd_t;




/*! \ingroup felrd
 *  \brief Internal control variables: This structure holds all component-related variables
 *         and is referenced by the mp_Members element of phcsBflFelRd_t.
 */
typedef struct 
{
    /*! \brief
     * The TX/RX buffer used for underlying operations. Since the minimum size of this buffer
     * is specified to be 64 bytes no length information needs to be present because all FeliCa
     * requests / responses are well below this boundary.
     */
    uint8_t  *m_TrxBuffer;
} phcsBflFelRd_InternalParam_t;



/* //////////////////////////////////////////////////////////////////////////////////////////////
// FeliCa Initialise:
*/
/*!
* \ingroup felrd
* \param[in] *cif           Pointer to an instance of the C object interface structure
* \param[in] *mp            Pointer to the alredy allocated internal control variables structure.
* \param[in] *p_lower       Pointer to the underlying layer I/O.
* \param[in] *p_trxbuffer   Pointer to the system-wide TRx buffer, allocated and managed by the embedding
*                           software. The buffer serves as the source/destination buffer for the
*                           underlying I/O Transceive functionality.
* 
* \brief This function shall be called first to initialise the Felica Reader component. 
*
* There the C-Layer, the internal variables, the underlaying layer and the device mode are initialised.
* An own function pointer is typedef'ed for this function to enable the call within
* a generic C++ Felica wrapper. 
*
*/
void phcsBflFelRd_Init(phcsBflFelRd_t      *cif,
                       void                *mp,
                       phcsBflIo_t         *p_lower,
                       uint8_t             *p_trxbuffer);


#endif /* PHCSBFLFELRD_H */

/* E.O.F. */
