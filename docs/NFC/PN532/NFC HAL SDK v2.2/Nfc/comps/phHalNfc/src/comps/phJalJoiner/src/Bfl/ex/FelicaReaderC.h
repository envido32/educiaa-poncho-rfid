/* /////////////////////////////////////////////////////////////////////////////////////////////////
//                     Copyright (c) Philips Semiconductors
//
//                       (C)PHILIPS Electronics N.V.2004
//         All rights are reserved. Reproduction in whole or in part is
//        prohibited without the written consent of the copyright owner.
//    Philips reserves the right to make changes without notice at any time.
//   Philips makes no warranty, expressed, implied or statutory, including but
//   not limited to any implied warranty of merchantability or fitness for any
//  particular purpose, or that the use will not infringe any third party patent,
//   copyright or trademark. Philips must not be liable for any loss or damage
//                            arising from its use.
///////////////////////////////////////////////////////////////////////////////////////////////// */


/*! \file FelicaReaderC.h
 *
 * Projekt: Object Oriented Reader Library Framework BAL component.
 *
 *  Source: FelicaReaderC.h
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Date: Thu Sep 27 09:38:46 2007 $
 *
 * Comment:
 *  Example code for handling of BFL.
 *
 */

#ifndef __FELICA_READER_C_H__
#define __FELICA_READER_C_H__

/*
 * This example shows how to send a POLLING_REQ and how to receive
 * a POLLING_RES 
 */
uint16_t ActivateFelicaReader(void *comHandle);

#endif /* __FELICA_READER_C_H__ */
