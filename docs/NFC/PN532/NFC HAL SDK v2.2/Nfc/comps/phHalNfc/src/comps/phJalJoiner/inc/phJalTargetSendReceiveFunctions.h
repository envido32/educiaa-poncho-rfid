/*
 *         Copyright (c), NXP Semiconductors
 *
 *                       (C)NXP Electronics N.V.2004
 *         All rights are reserved. Reproduction in whole or in part is
 *        prohibited without the written consent of the copyright owner.
 *    NXP reserves the right to make changes without notice at any time.
 *   NXP makes no warranty, expressed, implied or statutory, including but
 *   not limited to any implied warranty of merchantability or fitness for any
 *  particular purpose, or that the use will not infringe any third party patent,
 *   copyright or trademark. NXP must not be liable for any loss or damage
 *                            arising from its use.
 *
 */

/*!
 * \file phJalTargetSendReceiveFunctions.h
 * \brief Target Send/Receive functions
 *
 * Project: NFC MW / HAL
 *
 * $Date: Thu Sep 27 09:38:42 2007 $ 
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Aliases: SDK_HAL_2.2,SDK_HAL_2.2_071008,NFC_FRI_SDK_1.0_071031 $
 *
 */

#ifndef PHJALTARGETSENDRECEIVEFUNCTIONS_H
#define PHJALTARGETSENDRECEIVEFUNCTIONS_H

/*! \ingroup grp_file_attributes
 *  \name JAL Target Send and Receive Functions
 *  File: \ref phJalTargetSendReceiveFunctions.h
 */
/*@{*/
#define PHJALTARGETSENDRECEIVEFUNCTIONS_FILEREVISION "$Revision: 1.1 $"
#define PHJALTARGETSENDRECEIVEFUNCTIONS_FILEALIASES  "$Aliases: SDK_HAL_2.2,SDK_HAL_2.2_071008,NFC_FRI_SDK_1.0_071031 $"
/*@}*/

#include <phNfcTypes.h>
#include <phNfcHalTypes.h>
#include <phJalJoinerBflFrame.h>

/**
 * \ingroup grp_jal_nfct_aux_functions
 * \brief Function responsible for transferring data from the Target Endpoint to the user.
 * 
 * Functionality is subdivided into two states. The first one handles
 * data which has a size not greater as the one of the internal Target buffer, the other
 * one handles an arbitrary number of bytes which will be chained.
 * In both cases first a check will be done if the number of received data bytes fits
 * into the user buffer. If this check fails the function will return with an error and
 * no data is copied.
 * In the first state data is just copied from the Target buffer into the user buffer, whereas
 * in the second one chaining has to be handled. In case of chaining after every successful 
 * data transfer it will be checked if another data packet with the size of the internal Target
 * buffer fits into the user buffer. If this check fails the function will return notifying the
 * user that more data is available.
 */
 
NFCSTATUS phJal_ReceiveDataFromEndpoint(phJal_sBFLUpperFrame_t      *bfl_frame,
                                        phHal_sDepAdditionalInfo_t  *psDepAdditionalInfo,
                                        uint8_t                     *pRecvBuf,
                                        uint16_t                    *pRecvLength);

/**
 * \ingroup grp_jal_nfct_aux_functions
 * \brief Function responsible for transferring data from the user to the Target Endpoint.
 * 
 * This function acts as the counterpart to \ref phJal_ReceiveDataFromEndpoint implementing
 * the same behavior but transferring the data in the reverse direction from the user to the Target.
 * Meta Chaining will be used if more has to be transferred than fitting into the Target buffer.
 */
NFCSTATUS phJal_SendDataToEndpoint(phJal_sBFLUpperFrame_t      *bfl_frame,
                                   phHal_sDepAdditionalInfo_t  *psDepAdditionalInfo,
                                   uint8_t                     *pSendBuf,
                                   uint16_t                    SendLength);



#endif