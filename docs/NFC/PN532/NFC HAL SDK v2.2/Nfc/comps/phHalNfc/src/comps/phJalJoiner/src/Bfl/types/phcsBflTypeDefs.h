/* /////////////////////////////////////////////////////////////////////////////////////////////////
//                     Copyright (c) NXP Semiconductors
//
//                       (C)NXP Electronics N.V.2004
//         All rights are reserved. Reproduction in whole or in part is
//        prohibited without the written consent of the copyright owner.
//    NXP reserves the right to make changes without notice at any time.
//   NXP makes no warranty, expressed, implied or statutory, including but
//   not limited to any implied warranty of merchantability or fitness for any
//  particular purpose, or that the use will not infringe any third party patent,
//   copyright or trademark. NXP must not be liable for any loss or damage
//                            arising from its use.
///////////////////////////////////////////////////////////////////////////////////////////////// */

/*! 
 * \if cond_generate_filedocu 
 * \file phcsBflTypeDefs.h
 *
 * Project: Basic Function Library type definitions.
 *
 * Workfile: phcsBflTypeDefs.h
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Date: Thu Sep 27 09:38:45 2007 $
 *
 * Comment:
 *  None.
 *
 * History:
 *  MHa: Generated September 2005
 * \endif
 *
*/

#ifndef PHCSBFLTYPEDEFS_H
#define PHCSBFLTYPEDEFS_H

/* unsigned types */
#ifndef __uint8_t_defined    
#define __uint8_t_defined    
typedef unsigned char   uint8_t;
#endif

#ifndef __uint16_t_defined    
#define __uint16_t_defined    
typedef unsigned short  uint16_t;
#endif

#ifndef __uint32_t_defined    
#define __uint32_t_defined    
typedef unsigned long   uint32_t;
#endif

/* signed types */
/* Exclude int8_t and int32_t defintion if used in Linux environment. 
   The same name is already defined in the includes for serial interface. */ 
#ifndef __int8_t_defined    
#define __int8_t_defined    
typedef char            int8_t;
#endif

#ifndef __int16_t_defined    
#define __int16_t_defined    
typedef short           int16_t;
#endif

#ifdef WIN32
    #ifndef __int32_t_defined
    #define __int32_t_defined
    typedef long            int32_t;
    #endif
#else
    #ifndef __int8_t_defined
    #define __int8_t_defined    
    typedef long            int32_t;
    #endif
#endif


#endif /* PHCSBFLTYPEDEFS_H */
