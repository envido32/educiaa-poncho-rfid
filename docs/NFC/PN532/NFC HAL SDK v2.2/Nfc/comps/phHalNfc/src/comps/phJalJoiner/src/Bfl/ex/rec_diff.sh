#!/bin/ksh

{
for j in h c cpp vcproj
do
  for i in `find . -name "*.$j" -print`
  do
    file=${i%%.${j}}_$j.txt
    file_loc=`basename $file`
    print "=============================================="
    print ""
    print $i $file_loc
    dss diff -file1 $i\;SW_release_3_0_RFS_v1c -file2 $i\;SW_release_3_1 -embed -white -output $file_loc
  done
done
} 2>&1 | tee $0.log

