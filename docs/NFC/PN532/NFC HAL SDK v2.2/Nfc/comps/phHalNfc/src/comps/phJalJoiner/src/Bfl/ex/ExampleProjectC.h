/* /////////////////////////////////////////////////////////////////////////////////////////////////
//                     Copyright (c) Philips Semiconductors
//
//                       (C)PHILIPS Electronics N.V.2004
//         All rights are reserved. Reproduction in whole or in part is
//        prohibited without the written consent of the copyright owner.
//    Philips reserves the right to make changes without notice at any time.
//   Philips makes no warranty, expressed, implied or statutory, including but
//   not limited to any implied warranty of merchantability or fitness for any
//  particular purpose, or that the use will not infringe any third party patent,
//   copyright or trademark. Philips must not be liable for any loss or damage
//                            arising from its use.
///////////////////////////////////////////////////////////////////////////////////////////////// */


/*! \file ExampleProjectC.h
 *
 * Projekt: Object Oriented Reader Library Framework BAL component.
 *
 *  Source: ExampleProjectC.h
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Date: Thu Sep 27 09:38:49 2007 $
 *
 * Comment:
 *  Example code for handling of BFL.
 *
 */

#ifndef __EXAMPLE_PROJECT_C_H__
#define __EXAMPLE_PROJECT_C_H__

/* 
 * Used for debugging reasons 
 */
#include <stdio.h>		// e.g. printf
#include <string.h>		// e.g. atoi


/*
 * The startup procedure (before selecting the operating mode) just needs access
 * to the registers. That's why the bal and the rcregctl is enough
 */
#include <phcsBflBal.h>
#include <phcsBflRegCtl.h>

/*
 * Main entry point. 
 */
int main(int, char *argv[]);

#endif /* __EXAMPLE_PROJECT_C_H__ */
