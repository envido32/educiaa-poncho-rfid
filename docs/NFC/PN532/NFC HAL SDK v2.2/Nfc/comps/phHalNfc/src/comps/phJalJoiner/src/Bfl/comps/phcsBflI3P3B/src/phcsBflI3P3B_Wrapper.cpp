/* /////////////////////////////////////////////////////////////////////////////////////////////////
//                     Copyright (c) Philips Semiconductors
//
//                       (C)PHILIPS Electronics N.V.2004
//         All rights are reserved. Reproduction in whole or in part is
//        prohibited without the written consent of the copyright owner.
//    Philips reserves the right to make changes without notice at any time.
//   Philips makes no warranty, expressed, implied or statutory, including but
//   not limited to any implied warranty of merchantability or fitness for any
//  particular purpose, or that the use will not infringe any third party patent,
//   copyright or trademark. Philips must not be liable for any loss or damage
//                            arising from its use.
///////////////////////////////////////////////////////////////////////////////////////////////// */

/*! \file phcsBflI3P3B_Wrapper.cpp
 *
 * Project: Object Oriented Library Framework ISO14443_3B Component.
 *
 *  Source: phcsBflI3P3B_Wrapper.cpp
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Date: Thu Sep 27 09:39:31 2007 $
 *
 * Comment:
 *  C++ wrapper for ISO14443_3B component.
 *
 * History:
 *  MHa: Generated 17. Jannuary 2005
 *  MHa: Migrated to MoReUse September 2005
 *
 */

#include <phcsBflI3P3BWrapper.hpp>

using namespace phcs_BFL;

////////////////////////////////////////////////////////////////////////////////////////////////////
// Glue:

phcsBfl_I3P3BGlue::phcsBfl_I3P3BGlue(void)
{
    m_GlueStruct.Transceive          = &phcsBfl_I3P3BGlue::Transceive;
    
    // We don't need other members in the glue!
    m_GlueStruct.MfAuthent      = NULL;
    m_GlueStruct.Transmit       = NULL;
    m_GlueStruct.Receive        = NULL;
    m_GlueStruct.mp_Lower       = NULL;
    m_GlueStruct.mp_Members     = NULL;
    
}


phcsBfl_I3P3BGlue::~phcsBfl_I3P3BGlue(void)
{
    // For this implementation we don't have anything to clean up.
}


// Static class members, able to call into the lower device's C++ code again:

phcsBfl_Status_t phcsBfl_I3P3BGlue::Transceive(phcsBflIo_TransceiveParam_t *transceive_param)
{
    // Resolve the T=CL wrapper object location:
    class phcsBfl_I3P3B *iso14443_3B_wrapping_object =
        (class phcsBfl_I3P3B*)(((phcsBflIo_t*)(transceive_param->self))->mp_CallingObject);
    
    // Call the lower instance:
    return iso14443_3B_wrapping_object->mp_Lower->Transceive(transceive_param);
    
}


// T=CL Wrapper Implemenation:


phcsBflI3P3B_Wrapper::phcsBflI3P3B_Wrapper(void)
{
    // This constructor does no initialisation. Initialise() is needed.
    m_ConstructorStatus = PH_ERR_BFL_SUCCESS;        
}



phcsBflI3P3B_Wrapper::phcsBflI3P3B_Wrapper(class phcsBfl_Io     *p_lower,
                                       uint8_t             initiator__not_target,
                                       uint8_t            *p_trxbuffer,
                                       uint16_t            trxbuffersize)
{
     m_ConstructorStatus = PH_ERR_BFL_SUCCESS;
    // Hand over the pointer of the C-kernel's initialise function:
    this->Initialise(p_lower, initiator__not_target, p_trxbuffer, trxbuffersize);
}

phcsBflI3P3B_Wrapper::~phcsBflI3P3B_Wrapper(void)
{
    // No action.
}

void phcsBflI3P3B_Wrapper::Initialise(class phcsBfl_Io       *p_lower,
                                    uint8_t               initiator__not_target,
                                    uint8_t              *p_trxbuffer,
                                    uint16_t              trxbuffersize)
{
    
    phcsBflI3P3B_Init(&m_Interface,
                          &m_CommunicationParams,
                          &m_ProtocolParams,
                          &(m_I3_3BGlue.m_GlueStruct),
                           initiator__not_target,
                           p_trxbuffer,
                           trxbuffersize);

    // Additional initialisation beyond the scope of the C part:
    mp_Lower = p_lower; // Initialise C++ underlying object reference.

}

phcsBfl_Status_t phcsBflI3P3B_Wrapper::RequestB(phcsBflI3P3B_RequestParam_t *request_b_param)
{
    phcsBfl_Status_t   status;  
    void       *p_self; 
    
    p_self = request_b_param->self;
    request_b_param->self = &m_Interface;
    ((phcsBflIo_t*)(((phcsBflI3P3B_t*)(request_b_param->self))->mp_Lower))->mp_CallingObject = this;
    status = m_Interface.RequestB(request_b_param);
    request_b_param->self = p_self;
    
    return status;
}

phcsBfl_Status_t phcsBflI3P3B_Wrapper::SlotMarker(phcsBflI3P3B_SlotmarkerParam_t *slotmarker_param)
{
    phcsBfl_Status_t   status;  
    void       *p_self; 
    
    p_self = slotmarker_param->self;
    slotmarker_param->self = &m_Interface;
    ((phcsBflIo_t*)(((phcsBflI3P3B_t*)(slotmarker_param->self))->mp_Lower))->mp_CallingObject = this;
    status = m_Interface.SlotMarker(slotmarker_param);
    slotmarker_param->self = p_self;

    return status;
}


phcsBfl_Status_t phcsBflI3P3B_Wrapper::Attrib(phcsBflI3P3B_AttribParam_t *attrib_param)
{
    phcsBfl_Status_t   status;  
    void       *p_self; 
    
    p_self = attrib_param->self;
    attrib_param->self = &m_Interface;
    ((phcsBflIo_t*)(((phcsBflI3P3B_t*)(attrib_param->self))->mp_Lower))->mp_CallingObject = this;
    status = m_Interface.Attrib(attrib_param);
    attrib_param->self = p_self;

    return status;
}


phcsBfl_Status_t phcsBflI3P3B_Wrapper::HaltB(phcsBflI3P3B_HaltParam_t *halt_b_param)
{
    phcsBfl_Status_t   status;  
    void       *p_self; 
    
    p_self = halt_b_param->self;
    halt_b_param->self = &m_Interface;
    ((phcsBflIo_t*)(((phcsBflI3P3B_t*)(halt_b_param->self))->mp_Lower))->mp_CallingObject = this;
    status = m_Interface.HaltB(halt_b_param);
    halt_b_param->self = p_self;

    return status;
}

phcsBflI3P4_ProtParam_t *phcsBflI3P3B_Wrapper::ProtocolParameters(void)
{
    return &m_ProtocolParams;
}

// EOF

