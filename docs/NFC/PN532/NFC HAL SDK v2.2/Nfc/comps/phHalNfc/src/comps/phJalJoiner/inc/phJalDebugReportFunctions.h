/*
 *         Copyright (c), NXP Semiconductors
 *
 *                       (C)NXP Electronics N.V.2004
 *         All rights are reserved. Reproduction in whole or in part is
 *        prohibited without the written consent of the copyright owner.
 *    NXP reserves the right to make changes without notice at any time.
 *   NXP makes no warranty, expressed, implied or statutory, including but
 *   not limited to any implied warranty of merchantability or fitness for any
 *  particular purpose, or that the use will not infringe any third party patent,
 *   copyright or trademark. NXP must not be liable for any loss or damage
 *                            arising from its use.
 *
 */

/*!
 * \file phJalDebugReportFunctions.h
 * \brief JAL Debug Report Functions
 *
 *
 * Project: NFC MW / HAL
 *
 * $Date: Thu Sep 27 09:38:42 2007 $ 
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Aliases: SDK_HAL_2.2,SDK_HAL_2.2_071008,NFC_FRI_SDK_1.0_071031 $
 *
 */

#ifndef PHJALDEBUGREPORTFUNCTIONS_H
#define PHJALDEBUGREPORTFUNCTIONS_H


/*! \ingroup grp_file_attributes
 *  \name JAL Joiner
 *  File: \ref phJalDebugReportFunctions.h
 */
/*@{*/
#define PHJALDEBUGREPORTFUNCTIONS_FILEREVISION "$Revision: 1.1 $"
#define PHJALDEBUGREPORTFUNCTIONS_FILEALIASES  "$Aliases: SDK_HAL_2.2,SDK_HAL_2.2_071008,NFC_FRI_SDK_1.0_071031 $"
/*@}*/

/* TRACE is used for viewing the function call sequence. For debugging purpose only. */
// #define PHJALJOINER_TRACE

#if ((defined PHJALJOINER_TRACE) || (!defined NDEBUG))
    #include <stdio.h>
#endif /* */

#ifndef NDEBUG
    /* Debug is switched on */
    #define reportDbg0(msg)                     fprintf(stderr, msg)                   /* */
    #define reportDbg1(msg, arg1)               fprintf(stderr, msg, arg1)             /* */
    #define reportDbg2(msg, arg1, arg2)         fprintf(stderr, msg, arg1, arg2)       /* */
    #define reportDbg3(msg, arg1, arg2, arg3)   fprintf(stderr, msg, arg1, arg2, arg3) /* */
#else
    /* debug is switched off */
    #define reportDbg0(msg) ((void)0)                   /* */
    #define reportDbg1(msg, arg1) ((void)0)             /* */
    #define reportDbg2(msg, arg1, arg2) ((void)0)       /* */
    #define reportDbg3(msg, arg1, arg2, arg3) ((void)0) /* */
#endif /* NDEBUG */

#endif /* GUARD */
