/* /////////////////////////////////////////////////////////////////////////////////////////////////
//                     Copyright (c) Philips Semiconductors
//
//                       (C)PHILIPS Electronics N.V.2004
//         All rights are reserved. Reproduction in whole or in part is
//        prohibited without the written consent of the copyright owner.
//    Philips reserves the right to make changes without notice at any time.
//   Philips makes no warranty, expressed, implied or statutory, including but
//   not limited to any implied warranty of merchantability or fitness for any
//  particular purpose, or that the use will not infringe any third party patent,
//   copyright or trademark. Philips must not be liable for any loss or damage
//                            arising from its use.
///////////////////////////////////////////////////////////////////////////////////////////////// */

/*! \file ExampleProject.h
 *
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Date: Thu Sep 27 09:38:47 2007 $
 *
 * Comment:
 *  Example code for handling of BFL.
 *
 */

#ifndef PHCSBFL_EX_EXAMPLE_PROJECT_H
#define PHCSBFL_EX_EXAMPLE_PROJECT_H

/* 
 * Used for debugging reasons 
 */
#include <stdio.h>		// e.g. printf
#include <string.h>		// e.g. atoi
#include <stdlib.h>		// e.g. strtol

/* 
 * Platform dependent definitions
 * Needed in order to aquire the serial interface
 */
#ifdef WIN32
	/* Windows: CreateFile(), ...*/
	#include <windows.h>
#else
	/* Linux: fopen(), ... */
	
#endif

/*
 * The startup procedure (before selecting the operating mode) just needs access
 * to the registers. That's why the bal and the rcregctl is enough
 */
#include <phcsBflBalWrapper.hpp>
#include <phcsBflRegCtlWrapper.hpp>

/*
 * Main entry point. 
 */
int main(int, char *argv[]);

#endif /* PHCSBFL_EX_EXAMPLE_PROJECT_H */
