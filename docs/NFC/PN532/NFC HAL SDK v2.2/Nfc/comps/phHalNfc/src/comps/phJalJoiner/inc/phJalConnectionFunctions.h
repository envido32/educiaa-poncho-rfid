/*
 *         Copyright (c), NXP Semiconductors
 *
 *                       (C)NXP Electronics N.V.2004
 *         All rights are reserved. Reproduction in whole or in part is
 *        prohibited without the written consent of the copyright owner.
 *    NXP reserves the right to make changes without notice at any time.
 *   NXP makes no warranty, expressed, implied or statutory, including but
 *   not limited to any implied warranty of merchantability or fitness for any
 *  particular purpose, or that the use will not infringe any third party patent,
 *   copyright or trademark. NXP must not be liable for any loss or damage
 *                            arising from its use.
 *
 */

/*!
 * \file phJalConnectionFunctions.h
 * \brief JAL Connect/Disconnect Functions
 *
 *
 * Project: NFC MW / HAL
 *
 * $Date: Thu Sep 27 09:38:41 2007 $ 
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Aliases: SDK_HAL_2.2,SDK_HAL_2.2_071008,NFC_FRI_SDK_1.0_071031 $
 *
 */

#ifndef PHJALCONNECTIONFUNCTIONS_H
#define PHJALCONNECTIONFUNCTIONS_H


/*! \ingroup grp_file_attributes
 *  \name JAL Joiner
 *  File: \ref phJalConnectionFunctions.h
 */
/*@{*/
#define PHJALCONNECTIONFUNCTIONS_FILEREVISION "$Revision: 1.1 $"
#define PHJALCONNECTIONFUNCTIONS_FILEALIASES  "$Aliases: SDK_HAL_2.2,SDK_HAL_2.2_071008,NFC_FRI_SDK_1.0_071031 $"
/*@}*/

#include <phNfcTypes.h>
#include <phNfcHalTypes.h>
#include <phJalJoinerAux.h>

/**
 * \ingroup grp_jal_nfct_aux_functions
 * \brief Function connecting to a NFC device.
 *
 * NFC devices configured for any of the Active OpModes will connected using
 * WUP Requests. For the other ones configured for any of the Passive OpModes
 * the connection call is passed to \ref phJal_ActivateNfcProtocol. 
 */
NFCSTATUS phJal_ConnectToNfcDevice(phHal_sHwReference_t              *psHwReference,
                                   phHal_sRemoteDevInformation_t     *psRemoteDevInfo,
                                   phHal_sDevInputParam_t            *psDevInputParam,
                                   NfcModes                          DeviceMode);

/**
 * \ingroup grp_jal_nfct_aux_functions
 * \brief Function connecting to NFC devices configured for Passive 106k Meta Mode.
 *
 * It first executes all configurations for this Meta Mode and then
 * dispatches to \ref phJal_ConnectToNfcDevice to actually connect to the device. 
 */
NFCSTATUS phJal_ConnectToNfcPassive106Device(phHal_sHwReference_t              *psHwReference,
                                             phHal_sRemoteDevInformation_t     *psRemoteDevInfo,
                                             phHal_sDevInputParam_t            *psDevInputParam);

/**
 * \ingroup grp_jal_nfct_aux_functions
 * \brief Function connecting to NFC devices configured for Active Meta Mode.
 */
NFCSTATUS phJal_ConnectToNfcActiveDevice(phHal_sHwReference_t              *psHwReference,
                                         phHal_sRemoteDevInformation_t     *psRemoteDevInfo,
                                         phHal_sDevInputParam_t            *psDevInputParam);

/**
 * \ingroup grp_jal_nfct_aux_functions
 * \brief Function connecting to ISO 14443-4 devices.
 */
NFCSTATUS phJal_ConnectToIso144434Device(phHal_sHwReference_t              *psHwReference,
                                         phHal_sRemoteDevInformation_t     *psRemoteDevInfo,
                                         phHal_sDevInputParam_t            *psDevInputParam);

/**
 * \ingroup grp_jal_nfct_aux_functions
 * \brief Function connecting to NFC devices configured for Passive 212k/424k Meta Mode.
 */
NFCSTATUS phJal_ConnectToNfcPassive212424Device(phHal_sHwReference_t             *psHwReference,
                                                phHal_sRemoteDevInformation_t    *psRemoteDevInfo,
                                                phHal_sDevInputParam_t           *psDevInputParam);

/**
 * \ingroup grp_jal_nfct_aux_functions
 * \brief Function disconnecting a NFC device.
 *
 * This function executes the Deselect command to disconnect a NFC device.
 */
NFCSTATUS phJal_DisconnectNfcDevice(phHal_sHwReference_t              *psHwReference,
                                    phHal_sRemoteDevInformation_t     *psRemoteDevInfo);

/**
 * \ingroup grp_jal_nfct_aux_functions
 * \brief Function disconnecting a ISO 14443-4 device.
 *
 * This function executes the Deselect command to disconnect a NFC device.
 */
NFCSTATUS phJal_DisconnectIso144434Device(phHal_sHwReference_t              *psHwReference,
                                          phHal_sRemoteDevInformation_t     *psRemoteDevInfo);

/**
 * \ingroup grp_jal_nfct_aux_functions
 * \brief Function activating a previously disconnected device which has an
 * ISO 14443-3 based activation.
 *
 */
NFCSTATUS phJal_ReActivateIso144433BasedDevice(phHal_sHwReference_t             *psHwReference,
                                               phHal_sRemoteDevInformation_t    *psRemoteDevInfo);



#endif