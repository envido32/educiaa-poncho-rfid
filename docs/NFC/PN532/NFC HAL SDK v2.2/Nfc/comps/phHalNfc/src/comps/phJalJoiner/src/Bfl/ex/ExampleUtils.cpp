/* /////////////////////////////////////////////////////////////////////////////////////////////////
//                     Copyright (c) Philips Semiconductors
//
//                       (C)PHILIPS Electronics N.V.2004
//         All rights are reserved. Reproduction in whole or in part is
//        prohibited without the written consent of the copyright owner.
//    Philips reserves the right to make changes without notice at any time.
//   Philips makes no warranty, expressed, implied or statutory, including but
//   not limited to any implied warranty of merchantability or fitness for any
//  particular purpose, or that the use will not infringe any third party patent,
//   copyright or trademark. Philips must not be liable for any loss or damage
//                            arising from its use.
///////////////////////////////////////////////////////////////////////////////////////////////// */


/*! \file ExampleUtils.cpp
 *
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Date: Thu Sep 27 09:38:46 2007 $
 *
 */

/* RcRegCtl is always needed */
#include <phcsBflRegCtlWrapper.hpp>
#ifdef WIN32
	/* BAL is only needed for the PC environment since this uses the UART */
	#include "phcsBflBalWrapper.hpp"
#endif

/* Names for the PN51x registers */
#include <phcsBflHw1Reg.h>

/* Access to the OpCtl component */
#include <phcsBflOpCtlWrapper.hpp>
#include <phcsBflOpCtl_Hw1Ordinals.h>

#include <stdio.h>

/* Header definitions */
#include "ExampleUtils.h"

/* 
 * Checks if the platform is appropriate.
 * If the conditions are not met, the application returns a value unequal RCLSTATUS_SUCCESS
 * #pragma disables a non-relevant warning
 */
#pragma warning (disable: 4127) 
#pragma warning (disable: 4505) 


/* specifying used namespace for BFL */
using namespace phcs_BFL;
    
/*
 * Checks if the platform is appropriate.
 * If the conditions are not met, the application returns a value unequal PH_ERR_BFL_SUCCESS
 */
phcsBfl_Status_t CheckPlatform()
{
	if (sizeof(int32_t) != 4)
		return PH_ERR_BFL_OTHER_ERROR;
	
	if (sizeof(int32_t) != sizeof(void*))
		return PH_ERR_BFL_OTHER_ERROR;

	return PH_ERR_BFL_SUCCESS;
}

/*
 * Checks if the version register of the PN51x fits to the specified register value
 */
phcsBfl_Status_t CheckSiliconVersion(	phcsBflRegCtl_Wrapper<phcsBflRegCtl_SerHw1Params_t> *rc_reg_ctl, 
								    uint8_t expectedVersion )
{
	phcsBfl_Status_t                  status;
    phcsBflRegCtl_GetRegParam_t     grp;

	grp.address = PHCS_BFL_JREG_VERSION;
	status = rc_reg_ctl->GetRegister(&grp);
	if (status)
		return status;

	if (grp.reg_data == expectedVersion)
		return PH_ERR_BFL_SUCCESS;
	else
		return PH_ERR_BFL_INVALID_DEVICE_STATE;
}

/*
 * Changes the serial speed of the PN51x. 
 * Note that the speed of the host interface (UART on PC) has to be also set to the
 * appropriate one.
 */
phcsBfl_Status_t ChangeJoinerBaudRate(phcsBflRegCtl_Wrapper<phcsBflRegCtl_SerHw1Params_t> *rc_reg_ctl, 
							        uint32_t baudrate)
{
    phcsBfl_Status_t                  status = PH_ERR_BFL_SUCCESS;
    phcsBflRegCtl_SetRegParam_t     srp;

    srp.address = PHCS_BFL_JREG_SERIALSPEED; // register to set to change speed

    switch(baudrate)
    {
        case 9600:
            srp.reg_data = 0xEB;
            break;

        case 14400:
            srp.reg_data = 0xDA;
            break;

        case 19200:
            srp.reg_data = 0xCB;
            break;

        case 38400:
            srp.reg_data = 0xAB;
            break;

        case 57600:
            srp.reg_data = 0x9A;
            break;

        case 115200:
            srp.reg_data = 0x7A;
            break;

        case 128000:
            srp.reg_data = 0x74;
            break;

        case 230400:
            srp.reg_data = 0x5A;
            break;

        case 460800:
            srp.reg_data = 0x3A;
            break;

        case 921600:
            srp.reg_data = 0x1C;
            break;

        case 1288000:
            srp.reg_data = 0x14;
            break;

        default:
            status = PH_ERR_BFL_INVALID_PARAMETER;
            break;
    }

	/* Set the appropriate value */
	if (!status)
		status = rc_reg_ctl->SetRegister(&srp);

	/* Now the PN51x is set to the new speed*/
    return status;
}

	
/*
 * This example shows how to set the PN51x into soft power down mode.
 * Note: This implementation just works for Windows in the C++ version.
 */
phcsBfl_Status_t PowerDownExample(phcsBflRegCtl_Wrapper<phcsBflRegCtl_SerHw1Params_t> *rc_reg_ctl)
{
    phcsBflRegCtl_GetRegParam_t   grp;
    phcsBflRegCtl_ModRegParam_t   mrp;
	phcsBfl_Status_t				  status;

	/* Set AutoWakeUp so that the chip wakes up automatically */
	mrp.address = PHCS_BFL_JREG_TXAUTO;
	mrp.mask = 0x20;
	mrp.set = 1;
	status = rc_reg_ctl->ModifyRegister(&mrp);
	if (status)
		return status;

	/* Set the arguments for Power Down */
	mrp.address  = PHCS_BFL_JREG_COMMAND;
	mrp.set = PHCS_BFL_JBIT_POWERDOWN;
	mrp.mask = PHCS_BFL_JBIT_POWERDOWN;
	status = rc_reg_ctl->ModifyRegister(&mrp); 
	if (status)
		return status;

	/* Now the chip should be in soft power down mode */

	/* Show that the PN51x is in Power Down (a time-out should be generated) */
	grp.address = PHCS_BFL_JREG_VERSION;
	status = rc_reg_ctl->GetRegister(&grp);
	if ((status & ~PH_ERR_BFL_COMP_MASK) != PH_ERR_BFL_INTERFACE_ERROR) 
	{
		printf("[E] Chip seems to be not in soft power down mode.\n");
		status = PH_ERR_BFL_SUCCESS;
	}
	else
		printf("[I] PN51x is now in soft power down mode.\n");

	return status;
}

phcsBfl_Status_t WakeUpRs232Example(	phcsBflRegCtl_Wrapper<phcsBflRegCtl_SerHw1Params_t>    *rc_reg_ctl, 
								    phcsBflBal_Wrapper<phcsBflBal_Hw1SerWinInitParams_t> *bal)
{
	phcsBflBal_WriteBusParam_t      wbp;
    phcsBflRegCtl_GetRegParam_t     grp;
	phcsBfl_Status_t				    status;

	/* Send a 0x55 according to the data sheet */
	wbp.bus_data = 0x55;
	bal->WriteBus(&wbp);

	/* Read from register 0x00, until no time-out is generated any more */
	grp.address = 0x00;
	do {
		printf("[I] Trying to wake up PN51x ...\n");
		status = rc_reg_ctl->GetRegister(&grp);
	} while(status);

	return status;
}


/*
 * Wrapper for the WaitForPN51xInterruptRI() function
 * This is needed for the BFL callbacks.
 */
phcsBfl_Status_t WaitForPN51xInterruptCb(phcsBflAux_WaitEventCbParam_t* aParms)
{
	return WaitForPN51xInterruptRI( (aParms->user_ref), 0);
}

/*
 * Waits for an interrupt on the Ring Indicator (used with the EvalBoard, DemoBoard)
 */
phcsBfl_Status_t WaitForPN51xInterruptRI(void *aPort, int32_t aTimeOut)
#ifndef WIN32
{
	return PH_ERR_BFL_SUCCESS;
} 
#else
{
    DWORD           mask;
	uint8_t   abort = FALSE;
	OVERLAPPED		o;

	int32_t			    counter = 0;
	phcsBfl_Status_t  status = PH_ERR_BFL_SUCCESS;

	o.hEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

	// Enable RI interrupt
	// Note: Never call SetCommMask twice! Seems to be a problem within Windows!
	SetCommMask((HANDLE)aPort, EV_RING);

	WaitCommEvent((HANDLE) aPort, &mask, &o);
	do 
	{
		if (mask & EV_RING)
		{
			// RI indicator is set --> interrupt is set by the PN51x
			// printf("[I] WaitCommEvent has been passed.\n");
			abort = TRUE;
			status = PH_ERR_BFL_SUCCESS;
		}

		if (aTimeOut)
		{
			++counter;
		}

		if ( aTimeOut && (counter >= aTimeOut) )
		{
			abort = TRUE;
			status = PH_ERR_BFL_IO_TIMEOUT;
		}

		// Give the scheduler a chance to dispatch
		Sleep(10);

	} while(!abort);

	// Disable interrupts
	SetCommMask((HANDLE)aPort, 0);

	// Not needed any more
	CloseHandle(o.hEvent);

	return status;
}
#endif

/*
 * This function shows how to use the interrupt line.
 * NOTES:
 * - This code is highly specific to the platform!
 * - This code is also written for the UART interface. In case of SPI, I2C or Parallel
 *   interface the mechanism is more straightforward.
 * - Furthermore some serial interfaces are known not to work because they do not
 *   propagate the interrupt line!
 */
phcsBfl_Status_t ExampleUtils(void *comHandle,
						    phcsBflOpCtl_Wrapper<phcsBflOpCtl_Hw1Params_t>           *rc_op_ctl, 
						    phcsBflRegCtl_Wrapper<phcsBflRegCtl_SerHw1Params_t>  *rc_reg_ctl, 
						    phcsBflBal_Wrapper<phcsBflBal_Hw1SerWinInitParams_t> *bal,
						    uint8_t aSettings)
{
    phcsBflRegCtl_SetRegParam_t     srp;
    phcsBflRegCtl_GetRegParam_t     grp;
	phcsBfl_Status_t				    status;
    phcsBflOpCtl_AttribParam_t      opp;

	printf("[I] Demonstrates how to (re-)set the PN51x into soft-power down mode.\n");
	printf("    Please terminate the application with Control+C while the RF field is on.\n");
	printf("    Otherwise the PN51x is in soft power down and it has to be HW-resetted.\n");

#ifdef WIN32
	// Active the Ring Indicator for Interrupts
	SetCommMask((HANDLE) comHandle, EV_RING);
#endif

	// Check the last command line argument
	if (aSettings & 0x02)
	{
		/* Set registers for passive mode */
		opp.group_ordinal    = PHCS_BFLOPCTL_GROUP_MODE;
		opp.attrtype_ordinal = PHCS_BFLOPCTL_ATTR_PASSIVE;
		opp.param_a          = PHCS_BFLOPCTL_VAL_RF106K;
		opp.param_b          = PHCS_BFLOPCTL_VAL_RF106K;
		status = rc_op_ctl->SetAttrib(&opp);
		if (status != PH_ERR_BFL_SUCCESS)
            return status;

		/* Only the SIGIN/SIGOUT should be used */
		opp.group_ordinal = PHCS_BFLOPCTL_GROUP_SIGNAL;
		opp.attrtype_ordinal = PHCS_BFLOPCTL_ATTR_SIGINOUT;
		opp.param_a = PHCS_BFLOPCTL_VAL_CARD;
		opp.param_b = PHCS_BFLOPCTL_VAL_ONLYSIGINOUT;
		status = rc_op_ctl->SetAttrib(&opp);
		if (status != PH_ERR_BFL_SUCCESS)
			return status;

		/* Activate the IDLE command */
		srp.address  = PHCS_BFL_JREG_COMMAND;
		srp.reg_data = PHCS_BFL_JCMD_IDLE;
		status = rc_reg_ctl->SetRegister(&srp);
		if (status != PH_ERR_BFL_SUCCESS)
			return status;

	}

	/* Disable all COMM related interrupts */
	srp.address = PHCS_BFL_JREG_COMMIEN;
	srp.reg_data = 0x00;
	status = rc_reg_ctl->SetRegister(&srp);
	if (status)
		return status;

	/* 
	 * Generate an interrupt when an RF field has been detected.
	 * MSB is set because IRQPushPull shall be used for the RS232 connection.
	 */
	srp.address = PHCS_BFL_JREG_DIVIEN;
	srp.reg_data = 0x82;
	status = rc_reg_ctl->SetRegister(&srp);
	if (status)
		return status;

	while (!status)
	{
		printf("[I] -----------------------------------------------------------\n");

		/* Clear the interrupts */
		srp.address = PHCS_BFL_JREG_DIVIRQ;
		srp.reg_data = 0x7F;
		status = rc_reg_ctl->SetRegister(&srp);
		if (status)
			printf("[E] Timeout while clearing the interrupts.\n");

		/* Set the chip to power down */
		status = PowerDownExample(rc_reg_ctl);

		printf("[I] Waiting for an interrupt (triggered by an external RF)...\n");

		/* Wait for an interrupt (on the interrupt line) 
		 * There are two different implementations available:
		 * 1) This function senses the CTS line on the RS232 interface
		 * 2) This function senses the RI line (which is the default)
		 * Thus if the interrupt line from the PN51x is routed to the CTS, comment
		 * the function WaitForPN51xInterruptRI and uncomment the function below.
		 */
		status = WaitForPN51xInterruptRI(comHandle, 0);
        if(status != PH_ERR_BFL_SUCCESS)
        {
            printf("[E] Error during Interrupt operation. Status = 0x%04X\n", status);
        }

		printf("[I] Interrupt has been detected.\n");

		/* Synchronize the serial interface. 
		 * This is only needed for the UART interface.
		 */
		status = WakeUpRs232Example(rc_reg_ctl, bal);
		if (status != PH_ERR_BFL_SUCCESS)
		{
            printf("[E] Error during wake up procedure. Status = 0x%04X", status);
        }


		/* 
		 * Now check which event occurred
		 * (For sake of simplicity only DIVIRQ is checked)
		 */
		grp.address = PHCS_BFL_JREG_DIVIRQ;
		grp.reg_data = 0x7F;
		status = rc_reg_ctl->GetRegister(&grp);
		if (status == PH_ERR_BFL_SUCCESS)
		{
			printf("[I] PN51x is now active. Now polling until external RF is switched off...\n");
		} else
			return status;

		grp.address = PHCS_BFL_JREG_STATUS1;
		for(;;)
		{
			status = rc_reg_ctl->GetRegister(&grp);
			if (status != PH_ERR_BFL_SUCCESS) 
				return status;
			if ( (grp.reg_data & PHCS_BFL_JBIT_RFON) == 0 )
				break;
		} 

		printf("[I] External field is disappeared. Falling back to soft power down mode.\n");

	}

	return status;
}


/*
 * Sets the timeout value (via PN51x timer).
 */
phcsBfl_Status_t SetTimeOut(phcsBflRegCtl_Wrapper<phcsBflRegCtl_SerHw1Params_t> *rc_reg_ctl,
  					      phcsBflOpCtl_Wrapper<phcsBflOpCtl_Hw1Params_t>          *rc_op_ctl, 
					      uint32_t  aMicroSeconds, 
					      uint8_t   aFlags)
{
	phcsBfl_Status_t				    status;
    phcsBflOpCtl_AttribParam_t      opp;
	phcsBflRegCtl_ModRegParam_t	    mrp;
	phcsBflRegCtl_GetRegParam_t	    grp;
	phcsBflRegCtl_SetRegParam_t	    srp;
	uint32_t					    regValues;

	if (aFlags == PN51X_STEPS_10US)
    {
		regValues = aMicroSeconds / 10;
    } else if ( (aFlags == PN51X_STEPS_100US)  || (aFlags == PN51X_TMR_DEFAULT) )
	{
		aFlags = PN51X_STEPS_100US;
		regValues = aMicroSeconds / 100;
	} else if (aFlags == PN51X_STEPS_500US)
    {
		regValues = aMicroSeconds / 500;
    }else
	{
		printf("[E] SetTimeOut: Invalid flags.\n");
		return PH_ERR_BFL_INVALID_PARAMETER;
	}

	 
	 /* Check if value is within the supported range 
	  * NOTE: The supported hardware range is bigger, since the prescaler here is always set
	  *       to 100us.
	  */
	 if (regValues >= 0xFFFF)
	 {
		 printf("[E] SetTimeOut: Overflow.\n");
		 return PH_ERR_BFL_INVALID_PARAMETER;
	 }

    /* 
	 * Activate / deactivate timer
	 */
    opp.group_ordinal    = PHCS_BFLOPCTL_GROUP_TMR;        
    opp.attrtype_ordinal = PHCS_BFLOPCTL_ATTR_TMR_AUTO;    
	if (aMicroSeconds == PN51X_TMR_OFF)
    {
		opp.param_a = PHCS_BFLOPCTL_VAL_OFF;
    } else
    {
		opp.param_a = PHCS_BFLOPCTL_VAL_ON;
    }
    status = rc_op_ctl->SetAttrib(&opp);
    if(status != PH_ERR_BFL_SUCCESS)
	{
        printf("[E] Error setting timer! Status = %04x \n", status);
		return status;
	}

	 if (aMicroSeconds == PN51X_TMR_OFF)
	 {
		 /* We would just like to switch off the timer --> we are done */
		 return status;
	 }

    /* 
	 * Set prescaler steps. A granularity of 100us is defined.
	 */
    opp.group_ordinal    = PHCS_BFLOPCTL_GROUP_TMR;         
    opp.attrtype_ordinal = PHCS_BFLOPCTL_ATTR_TMR_PRESCALER;    
	if (aMicroSeconds == PN51X_TMR_MAX)
	{
		opp.param_a			= 0xFF;
		opp.param_b			= 0x0F;
	} else
	{
		if (aFlags == PN51X_STEPS_10US)
		{
			opp.param_a			= 0x44;
			opp.param_b			= 0x00;
		} else if (aFlags == PN51X_STEPS_100US)
		{
			/* Calculation of the values: 100us = 6.78MHz/(677+1) = 6.78MHz/(0x2A5+1) */
			opp.param_a          = 0xA5;                        /* Low value  */
			opp.param_b          = 0x02;                        /* High value */
		} else if (aFlags == PN51X_STEPS_500US)
		{
			opp.param_a 		 = 0x3D;
			opp.param_b			 = 0x0D;
		} else
		{
			printf("[E] Timer: Invalid step size.\n");
			return PH_ERR_BFL_OTHER_ERROR;
		}
	}
    status = rc_op_ctl->SetAttrib(&opp);
    /* Check status and display error if something went wrong */
    if(status != PH_ERR_BFL_SUCCESS)
	{
        printf("[E] Error setting timer! Status = %04x \n", status);
		return status;
	}

    /* 
	 * Set the reload value 
	 */
	if ( (aMicroSeconds == PN51X_TMR_MAX) || (aFlags == PN51X_TMR_MAX) )
    {
		regValues = 0xFFFF;
    }

    opp.group_ordinal    = PHCS_BFLOPCTL_GROUP_TMR;         /* set operation control group parameter to timer */
    opp.attrtype_ordinal = PHCS_BFLOPCTL_ATTR_TMR_RELOAD;   /* set to 50 for 5 ms */
    opp.param_a          = (uint8_t) ( regValues & 0xFF );        /* Low value */
    opp.param_b          = (uint8_t) ( (regValues >> 8) & 0xFF ); /* High value (max 0xFF) */
    status = rc_op_ctl->SetAttrib(&opp);
    if(status != PH_ERR_BFL_SUCCESS)
	{
        printf("[E] Error setting timer! Status = %04x \n", status);
	}

	// Usually there has to be an event to be occured to start the timer.
	// However, if one wants to start the timer right now, this flag can
	// be set. 
	if ((aFlags & PN51X_START_NOW) == PN51X_START_NOW)
	{
		mrp.set = 1;
	} else
	{
		mrp.set = 0;
	}

	mrp.address = PHCS_BFL_JREG_CONTROL;
	mrp.mask = PHCS_BFL_JBIT_TSTARTNOW;
	status = rc_reg_ctl->ModifyRegister(&mrp);
	if ((aFlags & PN51X_START_NOW) == PN51X_START_NOW)
	{
	    grp.address  = PHCS_BFL_JREG_COMMIRQ;
	    grp.reg_data = 0x00;
	    while(status == PH_ERR_BFL_SUCCESS && (grp.reg_data & PHCS_BFL_JBIT_TIMERI) != PHCS_BFL_JBIT_TIMERI) 
	    {
	        status = rc_reg_ctl->GetRegister(&grp);
	    }
	} 
    if(status != PH_ERR_BFL_SUCCESS)
	{
        printf("[E] Error accessing register! Status = %04x \n", status);
	}

	/* stop timer if running */
	mrp.set = 1;
	mrp.address = PHCS_BFL_JREG_CONTROL;
	mrp.mask = PHCS_BFL_JBIT_TSTOPNOW;
	status = rc_reg_ctl->ModifyRegister(&mrp);
    if(status != PH_ERR_BFL_SUCCESS)
	{
        printf("[E] Error modifying register 0x%02X! Status = %04x \n", mrp.address, status);
	}
	
	/* clear all IRQs if occured */
	srp.address = PHCS_BFL_JREG_COMMIRQ;
	srp.reg_data = 0x7F;
	status = rc_reg_ctl->SetRegister(&srp);
    if(status != PH_ERR_BFL_SUCCESS)
	{
        printf("[E] Error setting register 0x%02X! Status = %04x \n", srp.address, status);
	}

	return status;
}

phcsBfl_Status_t DebugViaSigOut(phcsBflRegCtl_Wrapper<phcsBflRegCtl_SerHw1Params_t> *rc_reg_ctl, 
                              uint8_t aSigOutSel)
{
	phcsBfl_Status_t					status;
    phcsBflRegCtl_GetRegParam_t			grp;
    phcsBflRegCtl_SetRegParam_t			srp;

	/*
	 * Specifies the signal of the SIGOUT pin. Most relevant signals:
	 * 03h - TestBus
	 * 04h - Modulation Signal (Envelope) from the internal coder (no carrier)
	 * 05h - Serial data stream to be transmitted (1=HIGH, 0=LOW)
	 * 06h - Receiver Signal (no carrier)
	 */

	grp.address = PHCS_BFL_JREG_TXSEL;
	status = rc_reg_ctl->GetRegister(&grp);
	if (status != PH_ERR_BFL_SUCCESS)
		return status;

	srp.address = PHCS_BFL_JREG_TXSEL;
	srp.reg_data = ( grp.reg_data & 0xF0 ) | aSigOutSel;
	status = rc_reg_ctl->SetRegister(&srp);
	if (status != PH_ERR_BFL_SUCCESS)
		return status;

	if (aSigOutSel == 0x03)
	{
		// TestBus is used --> specify the TestBus settings

		// Set TestBusBitSel (specify which line will be routed to SIGOUT)
		// Only needed if SigOut is used instead of the TestBus
		srp.address = 0x31;
		srp.reg_data = 0x05;
		status = rc_reg_ctl->SetRegister(&srp);
		if (status != PH_ERR_BFL_SUCCESS)
			return status;

		// Select the testbus
		srp.address = 0x32;
		srp.reg_data = 0x19;
		status = rc_reg_ctl->SetRegister(&srp);
		if (status != PH_ERR_BFL_SUCCESS)
			return status;
		
		// Enable / disable test data
		// Only needed if TestBus is used (but not SigOut)
		srp.address = 0x33;
		srp.reg_data = 0x20;
		status = rc_reg_ctl->SetRegister(&srp);
	}	
	
	return status;
}

phcsBfl_Status_t ResetInterrupts(phcsBflRegCtl_Wrapper<phcsBflRegCtl_SerHw1Params_t> *rc_reg_ctl, 
                               uint8_t aCommIen, uint8_t aDivIen, uint8_t aSettings)
{
	phcsBfl_Status_t  				status = PH_ERR_BFL_OTHER_ERROR;
    phcsBflRegCtl_SetRegParam_t		srp;

	if ( (aSettings & SET_COMM_IEN) == SET_COMM_IEN )
	{
		srp.address = PHCS_BFL_JREG_COMMIEN;
		srp.reg_data = aCommIen;
		status = rc_reg_ctl->SetRegister(&srp);
	}

	if ( (aSettings & SET_DIV_IEN) == SET_DIV_IEN )
	{
		srp.address = PHCS_BFL_JREG_DIVIEN;
		srp.reg_data = aDivIen;
		status = rc_reg_ctl->SetRegister(&srp);
	}
	
	if ( (aSettings & FLUSH_COMM_IRQ) == FLUSH_COMM_IRQ )
	{
		srp.address = PHCS_BFL_JREG_COMMIRQ;
		srp.reg_data = 0x7F;
		status = rc_reg_ctl->SetRegister(&srp);
	}

	if ( (aSettings & FLUSH_DIV_IRQ) == FLUSH_DIV_IRQ )
	{
		srp.address = PHCS_BFL_JREG_DIVIRQ;
		srp.reg_data = 0x7F;
		status = rc_reg_ctl->SetRegister(&srp);
	}

	return status;
}

/*
 * Example to show possible polling implementation in an user defined callback. It shows the same 
 * functionality as implemented in the library (RcAux component). 
 * All before sending the data and sending the data is done. IEn's and Irq's default to use are in
 * variables handed over. After this CB the registers CommIrq and DivIrq are read and the the error
 * processing and the reading of the data is done.
 */
phcsBfl_Status_t WaitEventCB(phcsBflAux_WaitEventCbParam_t* wait_event_cb_param)
{
    /* Macro definition for error handling of underlaying layer and exit. */
    #define CHECK_SUCCESS(result) { status = (result); if(status != PH_ERR_BFL_SUCCESS) return status; }
    phcsBfl_Status_t                status = PH_ERR_BFL_SUCCESS;
    phcsBflRegCtl_ModRegParam_t     mrp;
    phcsBflRegCtl_SetRegParam_t		srp;
    phcsBflRegCtl_GetRegParam_t		grp;
    phcsBflRegCtl_GetRegParam_t		grp_1;
    
    /* do seperate action if command to be executed is transceive */
    if (wait_event_cb_param->command == PHCS_BFL_JCMD_TRANSCEIVE || 
        wait_event_cb_param->command == PHCS_BFL_CMD_TRANSCEIVE_TO)
    {
        /* check if Initiator mode or Target Transmit mode */
        if (wait_event_cb_param->target_send || wait_event_cb_param->initiator_not_target)
        {
            /* TRx is always an endless loop, Initiator and Target must set STARTSEND. */
            mrp.address = PHCS_BFL_JREG_BITFRAMING;
            mrp.set = 1;
            mrp.mask = PHCS_BFL_JBIT_STARTSEND;
            CHECK_SUCCESS(wait_event_cb_param->lower->ModifyRegister(&mrp));
        }
    } else
    {
        grp.address = PHCS_BFL_JREG_COMMAND;
        CHECK_SUCCESS(wait_event_cb_param->lower->GetRegister(&grp));

        srp.address = PHCS_BFL_JREG_COMMAND;
        srp.reg_data = (uint8_t)((grp.reg_data & ~PHCS_BFL_JMASK_COMMAND) | wait_event_cb_param->command);
        CHECK_SUCCESS(wait_event_cb_param->lower->SetRegister(&srp));
    }

    /* Poll for IRQ: */
    grp_1.address = PHCS_BFL_JREG_COMMIRQ;    /* setreg_param used to save RAM */
    grp.address = PHCS_BFL_JREG_DIVIRQ;
    grp_1.reg_data = 0x00;           /* setreg_param used to save RAM */
    grp.reg_data = 0x00;

    while(!(*wait_event_cb_param->waitForComm & grp_1.reg_data) && 
          !(*wait_event_cb_param->waitForDiv  & grp.reg_data)  )
    {
        CHECK_SUCCESS(wait_event_cb_param->lower->GetRegister(&grp_1));
        CHECK_SUCCESS(wait_event_cb_param->lower->GetRegister(&grp));
    }
    
    return status;
    #undef CHECK_SUCCESS
}

#ifdef __DEBUG__
phcsBfl_Status_t DumpRegisters(phcsBflRegCtl_t *rc_reg_ctl)
{
    phcsBflRegCtl_GetRegParam_t getreg_param;
    uint8_t               i;
    int8_t                        log[100];
    phcsBfl_Status_t              status;

	getreg_param.self = rc_reg_ctl;

    // Access Joiner directly:
    getreg_param.address = 0;
    getreg_param.reg_data = 0;
    if((status = rc_reg_ctl->GetRegister(&getreg_param)) != RCLSTATUS_SUCCESS)
    {
        sprintf(log, "[E] Writing Error on register 0x%02X: RCLSTATUS 0x%04X!\n", getreg_param.address, status);
        DebugClient(1, log);
		return PH_ERR_BFL_OTHER_ERROR;
    }

    sprintf(log,"Registersetting : \n");
    DebugClient(1, log);
    sprintf(log,"    0  1  2  3  4  5  6  7   8  9  A  B  C  D  E  F");
    // Loop for all register except FIFO data, display xx instead:
    for(i=0;i<0x41;i++)
    {
        if((i&0xf) == 0)
        {
            DebugClient(1, log);
            sprintf(log, "P0 ");
            log[1] = (int8_t)(log[1] + (i>>4));
        }
        if(i != 0x09)
        {
            getreg_param.address = i;
            status = rc_reg_ctl->GetRegister(&getreg_param);
            sprintf(&log[3+3*(i&0xf)+(i>>3&1)], "%02x  ", getreg_param.reg_data);
        } else
        {
            sprintf(&log[3+3*(i&0xf)+(i>>3&1)], "xx  ");
        }
		if ((i%15) == 0)
			DebugClient(1, "\n");

        if (status) i=0x50; // STOP
    }

	DebugClient(1, "\n");

	return PH_ERR_BFL_SUCCESS;
}
#endif
