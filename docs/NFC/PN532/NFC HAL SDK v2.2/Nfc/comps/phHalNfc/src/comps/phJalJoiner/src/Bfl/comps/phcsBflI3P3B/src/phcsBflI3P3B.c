/* /////////////////////////////////////////////////////////////////////////////////////////////////
//                     Copyright (c) Philips Semiconductors
//
//                       (C)PHILIPS Electronics N.V.2004
//         All rights are reserved. Reproduction in whole or in part is
//        prohibited without the written consent of the copyright owner.
//    Philips reserves the right to make changes without notice at any time.
//   Philips makes no warranty, expressed, implied or statutory, including but
//   not limited to any implied warranty of merchantability or fitness for any
//  particular purpose, or that the use will not infringe any third party patent,
//   copyright or trademark. Philips must not be liable for any loss or damage
//                            arising from its use.
///////////////////////////////////////////////////////////////////////////////////////////////// */

/*! \file phcsBflI3P3B.c
 *
 * Project: Object Oriented Library Framework ISO14443_3B component.
 *
 *  Source: phcsBflI3P3B.c
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Date: Thu Sep 27 09:39:31 2007 $
 *
 * Comment:
 *  Joiner-specific variant of phcsBfl_I3P3A Type B.
 *
 * History:
 *  MHa:    Generated 19. Jannuary 2005
 *  MHa: Migrated to MoReUse September 2005
 *
 */

#include <phcsBflI3P3B.h>
#include <phcsBflHw1Reg.h>

static uint16_t g_Max_Frame_Size_Table[9] = {16, 24, 32, 40, 48, 64, 96, 128, 256};


void      phcsBflI3P3B_Init(phcsBflI3P3B_t              *cif,
                          void                      *rp,
                          phcsBflI3P4_ProtParam_t   *p_td,
                          phcsBflIo_t                 *p_lower,
                          uint8_t                    initiator__not_target,
                          uint8_t                   *p_trxbuffer,
                          uint16_t                   trxbuffersize)
{
    /* Glue together and init the operation parameters: */
    ((phcsBflI3P3B_Params_t*)rp)->m_InitiatorNotTarget       = initiator__not_target;
    ((phcsBflI3P3B_Params_t*)rp)->mp_prot_p                  = p_td;
    ((phcsBflI3P3B_Params_t*)rp)->mp_prot_p->trx_buffer      = p_trxbuffer;
    ((phcsBflI3P3B_Params_t*)rp)->mp_prot_p->trx_buffer_size = trxbuffersize;
    ((phcsBflI3P3B_Params_t*)rp)->m_trx_buffer_size          = trxbuffersize;

    cif->mp_Members                                                     = rp;
    cif->mp_Lower                                                       = p_lower;
    cif->mp_WaitEventCB                                                 = NULL;
    cif->mp_UserRef                                                     = NULL;

    /* Initialize the function pointers: */
    cif->RequestB                                                       = phcsBflI3P3B_Request;
    cif->SlotMarker                                                     = phcsBflI3P3B_SlotMarker;
    cif->Attrib                                                         = phcsBflI3P3B_Attrib;
    cif->HaltB                                                          = phcsBflI3P3B_HaltB;
    
}


phcsBfl_Status_t phcsBflI3P3B_Request(phcsBflI3P3B_RequestParam_t *request_b_param)
{   
    /* Recover the pointer to the protocol parameter structure: */
    phcsBflI3P4_ProtParam_t PHCS_BFL_MEMLOC_REM *p_td = 
        (phcsBflI3P4_ProtParam_t*)((phcsBflI3P3B_Params_t*)(((phcsBflI3P3B_t*)(request_b_param->self))->mp_Members))->mp_prot_p;
    
    phcsBflIo_TransceiveParam_t  PHCS_BFL_MEMLOC_REM  transceive_param;
    phcsBfl_Status_t             PHCS_BFL_MEMLOC_REM  status  = PH_ERR_BFL_SUCCESS;
    uint8_t                    PHCS_BFL_MEMLOC_COUNT   i;
    uint8_t                    PHCS_BFL_MEMLOC_REM  fsci;

    /* Apply default values for exchange protocol (phcsBfl_I3P4) initialisation struct: */
    p_td->nad_supported = 0;
    p_td->cid_supported = 0;
    p_td->cid           = 0;
    p_td->tx_baud_int   = PHCS_BFLI3P3B_BAUD_106_INT;
    p_td->rx_baud_int   = PHCS_BFLI3P3B_BAUD_106_INT;
    p_td->fwi           = 4;
    p_td->fsci          = 0;
    
    if (((phcsBflI3P3B_Params_t*)(((phcsBflI3P3B_t*)(request_b_param->self))->mp_Members))->m_InitiatorNotTarget)
    {
        /* 
         * Device is Initiator: We do a "True" Request cycle: 
         */
        /* check if input parameter are correct */
        if(request_b_param->n > 0x04 || request_b_param->req_code > 0x01) 
            return PH_ERR_BFL_INVALID_PARAMETER;

        /* prepare buffer with right data */
        p_td->trx_buffer[0] = PHCS_BFLI3P3B_APF_BYTE; /* Anticollision Prefix byte */
        p_td->trx_buffer[1] = request_b_param->afi; /* AFI code */
        /* Param: b0-b2:n, b3:REQB/WUPB */
        p_td->trx_buffer[2] = (uint8_t)(request_b_param->n | (request_b_param->req_code << 3));


        /* Set necessary parameters for transmission */
     
        transceive_param.tx_buffer      = p_td->trx_buffer;
        transceive_param.tx_buffer_size = PHCS_BFLI3P3B_REQB_LEN;
        transceive_param.rx_buffer      = p_td->trx_buffer;
        transceive_param.rx_buffer_size = PHCS_BFLI3P3B_ATQB_LEN;
        transceive_param.self           = ((phcsBflI3P3B_t*)request_b_param->self)->mp_Lower;

        status = ((phcsBflI3P3B_t*)request_b_param->self)->mp_Lower->Transceive(&transceive_param);

        if(status == PH_ERR_BFL_SUCCESS && transceive_param.rx_buffer_size == 12)
        {
            if (transceive_param.rx_buffer[0]!= PHCS_BFLI3P3B_ATQB_BYTE1)
            {
                status = PH_ERR_BFL_PROTOCOL_ERROR;
            
            } else
            {
                /* ATQB Response */
                if (transceive_param.rx_buffer_size == PHCS_BFLI3P3B_ATQB_LEN)
                {
                    /* Copy PUPI bytes into PUPI buffer */
                    for(i = 0; i < PHCS_BFLI3P3B_PUPI_LEN; i++)
                    {
                        request_b_param->pupi[i] = p_td->trx_buffer[i+1];
                    }

                    /* Copy the application data into user buffer */
                    for(i = 0; i < PHCS_BFLI3P3B_APP_DATA_LEN; i++)
                    {
                        request_b_param->application_data[i] = p_td->trx_buffer[i+5];
                    }

                    /* Copy the protocol info into user buffer */
                    for(i = 0; i < PHCS_BFLI3P3B_PROTOCOL_INF_LEN; i++)
                    {
                        request_b_param->protocol_info[i] = p_td->trx_buffer[i+9];
                    }
                    
                    /* Frame Option */
                    /* NAD supported */
                    if (request_b_param->protocol_info[PHCS_BFLI3P3B_FWI_ADC_FO_BYTE] & PHCS_BFLI3P3B_NAD_POS)
                    {
                        p_td->nad_supported = 1;
                    }
                    /* CID supported */
                    if (request_b_param->protocol_info[PHCS_BFLI3P3B_FWI_ADC_FO_BYTE] & PHCS_BFLI3P3B_CID_POS)
                    {
                        p_td->cid_supported = 1;
                    }

                    /* FWI */
                    p_td->fwi = ((request_b_param->protocol_info[PHCS_BFLI3P3B_FWI_ADC_FO_BYTE] >> PHCS_BFLI3P3B_SHR4_MASK) 
                                  & PHCS_BFLI3P3B_FWI_MASK);

                    /* Maximum frame size that can received by the PICC */
                    fsci = (request_b_param->protocol_info[PHCS_BFLI3P3B_FR_SIZE_PR_TYPE] >> PHCS_BFLI3P3B_SHR4_MASK) 
                                        & PHCS_BFLI3P3B_FR_SIZE_MASK;

                    if (fsci > PHCS_BFLI3P3B_MAX_FSCI)
                    {
                        /* Interpret FSCI greater than PHCS_BFLI3P3B_MAX_FSCI as PHCS_BFLI3P3B_MAX_FSCI */
                        fsci = PHCS_BFLI3P3B_MAX_FSCI;
                    }
                    
                    /* Max. frame size into protocol parameters. */
                    p_td->fsci = fsci;

                    /* Protocl type */
                    request_b_param->protocol_type = request_b_param->protocol_info[PHCS_BFLI3P3B_FR_SIZE_PR_TYPE] 
                                                      & PHCS_BFLI3P3B_PROTOCOL_TYPE_MASK; 

                     
                } else
                {
                    status = PH_ERR_BFL_INVALID_FORMAT;
                }
            }
        } else
        {
            status = PH_ERR_BFL_INVALID_FORMAT;
        }
    } else
    {
        /*
         * Device is Target: The request is invalid if Target operation.
         */
        status = PH_ERR_BFL_INVALID_DEVICE_STATE;
    }

    return status;
}

phcsBfl_Status_t phcsBflI3P3B_SlotMarker(phcsBflI3P3B_SlotmarkerParam_t *slotmarker_param)
{
    /* Recover the pointer to the protocol parameter structure: */
    phcsBflI3P4_ProtParam_t PHCS_BFL_MEMLOC_REM *p_td = 
        (phcsBflI3P4_ProtParam_t*)((phcsBflI3P3B_Params_t*)(((phcsBflI3P3B_t*)(slotmarker_param->self))->mp_Members))->mp_prot_p;
    
    phcsBfl_Status_t                       PHCS_BFL_MEMLOC_REM  status = PH_ERR_BFL_SUCCESS;
    phcsBflIo_TransceiveParam_t           PHCS_BFL_MEMLOC_REM  transceive_param;
    uint8_t                   PHCS_BFL_MEMLOC_COUNT   i;
    uint8_t                   PHCS_BFL_MEMLOC_REM  fsci;
    
    
    /* Apply default values for exchange protocol (phcsBfl_I3P4) initialisation struct: */
    p_td->nad_supported = 0;
    p_td->cid_supported = 0;
    p_td->cid           = 0;
    p_td->tx_baud_int   = PHCS_BFLI3P3B_BAUD_106_INT;
    p_td->rx_baud_int   = PHCS_BFLI3P3B_BAUD_106_INT;
    p_td->fwi           = 4;
    p_td->fsci          = 0;

    if (((phcsBflI3P3B_Params_t*)(((phcsBflI3P3B_t*)(slotmarker_param->self))->mp_Members))->m_InitiatorNotTarget)
    {
        /* 
         * Device is Initiator: We do a "True" SlotMarker cycle: 
         */
        /* check if input parameter are correct */
        if(slotmarker_param->apn > 0x0F)
            return PH_ERR_BFL_INVALID_PARAMETER;

        /* prepare buffer with right data */
        p_td->trx_buffer[0] = PHCS_BFLI3P3B_APN | (slotmarker_param->apn << 4);   /* Slotmarker byte */

        /* set necessary parameters for transmission */
        transceive_param.tx_buffer = p_td->trx_buffer;
        transceive_param.tx_buffer_size  = PHCS_BFLI3P3B_SLOTMARKER_LEN;
        transceive_param.rx_buffer = p_td->trx_buffer;
        transceive_param.rx_buffer_size  = PHCS_BFLI3P3B_ATQB_LEN;
        transceive_param.self = ((phcsBflI3P3B_t*)slotmarker_param->self)->mp_Lower;
        
        status = ((phcsBflI3P3B_t*)slotmarker_param->self)->mp_Lower->Transceive(&transceive_param);

        if(status == PH_ERR_BFL_SUCCESS)
        {
            if(transceive_param.rx_buffer[0] != PHCS_BFLI3P3B_ATQB_BYTE1)
            {
                status = PH_ERR_BFL_PROTOCOL_ERROR;
            
            } else
            {
                /* ATQB Response */
                if (transceive_param.rx_buffer_size == PHCS_BFLI3P3B_ATQB_LEN)
                {
                    /* Copy PUPI bytes into PUPI buffer */
                    for(i = 0; i < PHCS_BFLI3P3B_PUPI_LEN; i++)
                    {
                        slotmarker_param->pupi[i] = p_td->trx_buffer[i+1];
                    }

                    /* Copy the application data into user buffer */
                    for(i = 0; i < PHCS_BFLI3P3B_APP_DATA_LEN; i++)
                    {
                        slotmarker_param->application_data[i] = p_td->trx_buffer[i+5];
                    }

                    /* Copy the protocol info into user buffer */
                    for(i = 0; i < PHCS_BFLI3P3B_PROTOCOL_INF_LEN; i++)
                    {
                        slotmarker_param->protocol_info[i] = p_td->trx_buffer[i+9];
                    }
                    
                    /* Frame Option */
                    /* NAD supported */
                    if (slotmarker_param->protocol_info[PHCS_BFLI3P3B_FWI_ADC_FO_BYTE] & PHCS_BFLI3P3B_NAD_POS)
                    {
                        p_td->nad_supported =1;
                    }

                    /* CID supported */
                    if (slotmarker_param->protocol_info[PHCS_BFLI3P3B_FWI_ADC_FO_BYTE] & PHCS_BFLI3P3B_CID_POS)
                    {
                        p_td->cid_supported = 1;
                    }

                    /* FWI */
                    p_td->fwi = ((slotmarker_param->protocol_info[PHCS_BFLI3P3B_FWI_ADC_FO_BYTE] >> PHCS_BFLI3P3B_SHR4_MASK) 
                                  & PHCS_BFLI3P3B_FWI_MASK);

                    /* Maximum frame size that can received by the PICC */
                    fsci = (slotmarker_param->protocol_info[PHCS_BFLI3P3B_FR_SIZE_PR_TYPE] >> PHCS_BFLI3P3B_SHR4_MASK) 
                            & PHCS_BFLI3P3B_FR_SIZE_MASK;

                    if (fsci > PHCS_BFLI3P3B_MAX_FSCI)
                    {
                        /* interpret FSCI greater than PHCS_BFLI3P3B_MAX_FSCI as PHCS_BFLI3P3B_MAX_FSCI */
                        fsci = PHCS_BFLI3P3B_MAX_FSCI;
                    }
                    
                    /* Max. frame size into protocol parameters. */
                    p_td->fsci = fsci;

                    /* Protocl type */
                    slotmarker_param->protocol_type = (slotmarker_param->protocol_info[PHCS_BFLI3P3B_FR_SIZE_PR_TYPE] 
                                                       & PHCS_BFLI3P3B_PROTOCOL_TYPE_MASK); 

                    
                } else
                {
                    status = PH_ERR_BFL_INVALID_FORMAT;
                }
            }
        } 
    
    } else
    {
        /*
         * Device is Target: The Slotmarker command is invalid.
         */
        status = PH_ERR_BFL_INVALID_DEVICE_STATE;
    }

    return status;
}



phcsBfl_Status_t phcsBflI3P3B_Attrib(phcsBflI3P3B_AttribParam_t *attrib_param)
{
    /* Recover the pointer to the protocol parameter structure: */
    phcsBflI3P4_ProtParam_t PHCS_BFL_MEMLOC_REM *p_td = 
        (phcsBflI3P4_ProtParam_t*)((phcsBflI3P3B_Params_t*)(((phcsBflI3P3B_t*)(attrib_param->self))->mp_Members))->mp_prot_p;
    
    phcsBfl_Status_t               PHCS_BFL_MEMLOC_REM  status = PH_ERR_BFL_SUCCESS;
    phcsBflIo_TransceiveParam_t   PHCS_BFL_MEMLOC_REM  transceive_param;
    uint8_t           PHCS_BFL_MEMLOC_COUNT  i;
    uint8_t           PHCS_BFL_MEMLOC_COUNT  k;
    uint8_t           PHCS_BFL_MEMLOC_COUNT  index = 0;

    p_td->fsdi = 0;

    if (((phcsBflI3P3B_Params_t*)(((phcsBflI3P3B_t*)(attrib_param->self))->mp_Members))->m_InitiatorNotTarget)
    {
        /* 
         * Device is Initiator: We do a "True" Attrib cycle: 
         */

        /* Input Parameter check. */
        /* Param 1 */
        if (((attrib_param->param_byte_1 & 0xC0) == 0xC0) 
            || ((attrib_param->param_byte_1 & 0x30)  == 0x30)
            || ((attrib_param->param_byte_1 & 0x03) > 0))
            return PH_ERR_BFL_INVALID_PARAMETER;

        /* Param 3, 4 */
        if (((attrib_param->protocol_type & PHCS_BFLI3P3B_RFU_MASK) > 0) 
            || ((attrib_param->protocol_type & PHCS_BFLI3P3B_PROTOCOL_TYPE_MASK)> 1)
            || ((attrib_param->cid & PHCS_BFLI3P3B_CID_MASK) > PHCS_BFLI3P3B_MAX_CID)
            || ((attrib_param->cid & PHCS_BFLI3P3B_RFU_MASK) > 0))
            return PH_ERR_BFL_INVALID_PARAMETER;

        
        /* Maximum Frame size that can received by the PCD */
        p_td->fsdi = attrib_param->param_byte_2 & PHCS_BFLI3P3B_FWI_MASK;

        if (p_td->fsdi <= PHCS_BFLI3P3B_MAX_FSDI)
        {
            /* initialise data buffer */
            p_td->trx_buffer[index++] = PHCS_BFLI3P3B_ATTRIB_CMD;     /* command byte */
            for (i=0; i<4; i++)
                p_td->trx_buffer[index++] = attrib_param->identifier[i];  /* PUPI */
             /* Parameter bytes */
            p_td->trx_buffer[index++] = attrib_param->param_byte_1;   
            p_td->trx_buffer[index++] = attrib_param->param_byte_2;   
            p_td->trx_buffer[index++] = attrib_param->protocol_type;   
            p_td->trx_buffer[index++] = attrib_param->cid;   
                
            for (i=0; i<attrib_param->higher_inf_bytes_length; i++)
                p_td->trx_buffer[index++] = attrib_param->higher_inf_bytes_buffer[i];      /* Higher layer INF bytes */
            
            /* Set single command parameter */
            transceive_param.tx_buffer_size  = index;
            transceive_param.tx_buffer = p_td->trx_buffer;
            transceive_param.rx_buffer = p_td->trx_buffer;
            transceive_param.rx_buffer_size  = (((phcsBflI3P3B_Params_t*)
                                                (((phcsBflI3P3B_t*)(attrib_param->self))->mp_Members))->m_trx_buffer_size);
            
            transceive_param.self = ((phcsBflI3P3B_t*)attrib_param->self)->mp_Lower;
            


            status = ((phcsBflI3P3B_t*)attrib_param->self)->mp_Lower->Transceive(&transceive_param);

            if (status == PH_ERR_BFL_SUCCESS)
            {
                
                /* Copy Higher Layer Inf Bytes */
                if (transceive_param.rx_buffer_size > 1)
                {
                    for (k = 0; k < transceive_param.rx_buffer_size; k++)
                    {
                        attrib_param->higher_inf_bytes_buffer[k] = p_td->trx_buffer[k+1]; 
                    }

                    attrib_param->higher_inf_bytes_length = (uint8_t)(transceive_param.rx_buffer_size -1);
                
                } else
                {
                    attrib_param->higher_inf_bytes_length = 0;
                }

                /* CID */
                p_td->cid = (p_td->trx_buffer[PHCS_BFLI3P3B_MBLI_CID_POS] & PHCS_BFLI3P3B_CID_MASK);
                
                /* MBLI */
                attrib_param->mbli = ((p_td->trx_buffer[PHCS_BFLI3P3B_MBLI_CID_POS] >> PHCS_BFLI3P3B_SHR4_MASK) 
                                        & PHCS_BFLI3P3B_MBLI_MASK);

                
                /* Bit Rate */
                p_td->tx_baud_int = ((attrib_param->param_byte_2 >> PHCS_BFLI3P3B_SHR4_MASK) & PHCS_BFLI3P3B_FR_SIZE_BIT_RATE_MASK);
                p_td->rx_baud_int = ((attrib_param->param_byte_2 >> PHCS_BFLI3P3B_SHR6_MASK) & PHCS_BFLI3P3B_FR_SIZE_BIT_RATE_MASK);

            }
        } else
        {
            status = PH_ERR_BFL_INVALID_PARAMETER;
        }

    } else
    {
        /*
         * Device is Target: The Attrib is invalid.
         */
        status = PH_ERR_BFL_INVALID_DEVICE_STATE;
    }

    return status;
}


phcsBfl_Status_t phcsBflI3P3B_HaltB(phcsBflI3P3B_HaltParam_t *halt_b_param)
{
    /* Recover the pointer to the protocol parameter structure: */
    phcsBflI3P4_ProtParam_t PHCS_BFL_MEMLOC_REM *p_td = 
        (phcsBflI3P4_ProtParam_t*)((phcsBflI3P3B_Params_t*)(((phcsBflI3P3B_t*)(halt_b_param->self))->mp_Members))->mp_prot_p;
    
    phcsBfl_Status_t               PHCS_BFL_MEMLOC_REM  status = PH_ERR_BFL_SUCCESS;
    phcsBflIo_TransceiveParam_t  PHCS_BFL_MEMLOC_REM  transceive_param;
    uint8_t                      PHCS_BFL_MEMLOC_COUNT  i;
    
    if (((phcsBflI3P3B_Params_t*)(((phcsBflI3P3B_t*)(halt_b_param->self))->mp_Members))->m_InitiatorNotTarget)
    {
        /* 
         * Device is Initiator: We do a "True" Halt cycle: 
         */
        

        /* initialise data buffer */
        p_td->trx_buffer[0] = PHCS_BFLI3P3B_HALTB_CMD;      /* command byte */
        for (i=0; i<4; i++)
            p_td->trx_buffer[i+1] = *(halt_b_param->identifier);     /* PUPI as received in ATQB */

        /* set single command parameter */
        transceive_param.tx_buffer_size  = PHCS_BFLI3P3B_HALTB_CMD_LEN;
        transceive_param.tx_buffer = p_td->trx_buffer;
        transceive_param.rx_buffer = p_td->trx_buffer;
        transceive_param.rx_buffer_size  = PHCS_BFLI3P3B_HALTB_RESP_LEN;
        transceive_param.self = ((phcsBflI3P3B_t*)halt_b_param->self)->mp_Lower;
        
        
        status = ((phcsBflI3P3B_t*)halt_b_param->self)->mp_Lower->Transceive(&transceive_param);
        
        if (status == PH_ERR_BFL_SUCCESS)
        {   
            /* check if response is according to specification */
            if (transceive_param.rx_buffer[0] != PHCS_BFLI3P3B_HALTB_RESP)
                status = PH_ERR_BFL_PROTOCOL_ERROR;
        }
    
    } else
    {
        /*
         * Device is Target: The HaltB is invalid.
         */
        status = PH_ERR_BFL_INVALID_DEVICE_STATE;
    }

    return status;
}

/* E.O.F. */
