/* /////////////////////////////////////////////////////////////////////////////////////////////////
//                     Copyright (c) Philips Semiconductors
//
//                       (C)PHILIPS Electronics N.V.2004
//         All rights are reserved. Reproduction in whole or in part is
//        prohibited without the written consent of the copyright owner.
//    Philips reserves the right to make changes without notice at any time.
//   Philips makes no warranty, expressed, implied or statutory, including but
//   not limited to any implied warranty of merchantability or fitness for any
//  particular purpose, or that the use will not infringe any third party patent,
//   copyright or trademark. Philips must not be liable for any loss or damage
//                            arising from its use.
///////////////////////////////////////////////////////////////////////////////////////////////// */

/*! 
 * \file phcsBflI3P3B.h
 *
 * Project: Object Oriented Library Framework ISO14443_3B component for Joiner.
 *
 *  Source: phcsBflI3P3B.h
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Date: Thu Sep 27 09:39:31 2007 $
 *
 * Comment:
 *  None.
 *
 * History:
 *  MHa: Generated 17. Jannuary 2005
 *  MHa: Migrated to MoReUse September 2005
 *
 */


#ifndef PHCSBFLI3P3B_H
#define PHCSBFLI3P3B_H

#include <phcsBflStatus.h>
#include <phcsBflI3P4_Common.h>
#include <phcsBflIo.h>
#include <phcsBflI3P4.h>


#define PHCS_BFLI3P3B_MBLI_CID_POS            (uint8_t)(0)                  /* MBLI/CID byte position */
#define PHCS_BFLI3P3B_RFU_MASK                (uint8_t)(0xF0)
#define PHCS_BFLI3P3B_MAX_CID                 (uint8_t)(14)
#define PHCS_BFLI3P3B_FR_SIZE_BIT_RATE_MASK   (uint8_t)(3)                  /* Frame size and bit rate mask. */ 
#define PHCS_BFLI3P3B_FWI_ADC_FO_BYTE         (uint8_t)(2)                  /* 3rd Protocol Info byte (ATQB) position */
#define PHCS_BFLI3P3B_FR_SIZE_PR_TYPE         (uint8_t)(1)                  /* 2nd Protocol Info byte (ATQB) position */
#define PHCS_BFLI3P3B_NAD_POS                 (uint8_t)(0x02)               /* Position of the NAD supported bit */
#define PHCS_BFLI3P3B_CID_POS                 (uint8_t)(0x01)               /* Position of the NAD supported bit */
#define PHCS_BFLI3P3B_SHR4_MASK               (uint8_t)(0x04)               /* Shift right (4 bits) */
#define PHCS_BFLI3P3B_SHR6_MASK               (uint8_t)(0x06)               /* Shift right (6 bits) */
#define PHCS_BFLI3P3B_FWI_MASK                (uint8_t)(0x0F)               /* FWI mask */
#define PHCS_BFLI3P3B_FR_SIZE_MASK            (uint8_t)(PHCS_BFLI3P3B_FWI_MASK)   /* Frame size mask (used to get */
                                                                                    /* the Frame size value) */
#define PHCS_BFLI3P3B_CID_MASK                (uint8_t)(PHCS_BFLI3P3B_FWI_MASK)   /* CID mask (used to get the CID) */
#define PHCS_BFLI3P3B_MBLI_MASK               (uint8_t)(PHCS_BFLI3P3B_FWI_MASK)   /* MBLI mask ( used to get the MBLI */
#define PHCS_BFLI3P3B_MAX_FSCI                (uint8_t)(0x08)                   /* Maximum Fsci value              */
#define PHCS_BFLI3P3B_MAX_FSDI                (uint8_t)(PHCS_BFLI3P3B_MAX_FSCI)   /* Maximum Fsdi value              */
#define PHCS_BFLI3P3B_MAX_FRAME_POS           (uint8_t)(0x01)                   /* Position of Max Frame 
                                                                                       Size in Param 2 in ATTRIB Command*/
                                                                                       
#define PHCS_BFLI3P3B_MAX_FRAME_MASK          (uint8_t)(PHCS_BFLI3P3B_FWI_MASK)   /* Mask for Max Frame 
                                                                                       Size in Param 2 in ATTRIB Command*/
#define PHCS_BFLI3P3B_PROTOCOL_TYPE_MASK      (uint8_t)(PHCS_BFLI3P3B_FWI_MASK) 
#define PHCS_BFLI3P3B_PUPI_LEN                (uint8_t)(0x04)
#define PHCS_BFLI3P3B_APP_DATA_LEN            (uint8_t)(0x04)
#define PHCS_BFLI3P3B_PROTOCOL_INF_LEN        (uint8_t)(0x03)

#define PHCS_BFLI3P3B_BAUD_106_INT            (uint8_t)(0x00)       /* Baudrate */

/* definitions for Request B functionality */
#define PHCS_BFLI3P3B_REQB_LEN     (uint8_t)(0x03)
#define PHCS_BFLI3P3B_APF_BYTE        (uint8_t)(0x05)
#define PHCS_BFLI3P3B_ATQB_BYTE1      (uint8_t)(0x50)
#define PHCS_BFLI3P3B_ATQB_LEN     (uint8_t)(12)

/* definitions for Slotmarker functionality */
#define PHCS_BFLI3P3B_APN                 (uint8_t)(0x05)
#define PHCS_BFLI3P3B_SLOTMARKER_LEN      (uint8_t)(0x01)

/* definitions for Attrib functionality */
#define PHCS_BFLI3P3B_ATTRIB_CMD  (uint8_t)(0x1D)

/* Command and Parameter byte definitions for HaltA function. */
#define PHCS_BFLI3P3B_HALTB_CMD           0x50
#define PHCS_BFLI3P3B_HALTB_CMD_LEN       0x05
#define PHCS_BFLI3P3B_HALTB_RESP          0x00
#define PHCS_BFLI3P3B_HALTB_RESP_LEN      0x01

/* Operation mode to be specified for phcsBfl_I3P3A: */
#define PHCS_BFLI3P3B_INITIATOR  0x01 /*!< \ingroup iso14443_3 
                                        Support NFC-Initiator and Reader functionality. */
#define PHCS_BFLI3P3B_TARGET     0x00 /*!< \ingroup iso14443_3 
                                        Target and Card functionality only supported.   */

/* Request codes and definitions */
#define PHCS_BFLI3P3B_REQB		   0x00 /*!< \ingroup iso14443_3 
                                        Request code marker for all devices. */
#define PHCS_BFLI3P3B_WUPB        0x01 /*!< \ingroup iso14443_3 
                                        Wakeup code code marker. */
#define PHCS_BFLI3P3B_AFI_00	   0x00 /*< \ingroup iso14443_3 
                                         AFI definition for all families and subfamilies. */



/*! \ingroup iso14443_3
 *  \brief Parameter structure for Request functionality. The RequestB function issues a REQB or WUPB request,
 *  depending on the req_code parameter.
 */
typedef struct 
{
    uint8_t  req_code;            /*!< \brief [in] Request code according to ISO 14443-3. Possible
                                                  values are PHCS_BFLI3P3B_REQB and PHCS_BFLI3P3B_WUPB. */
    uint8_t  afi;			      /*!< \brief [in] AFI code according to ISO 14443-3. Used to code application
												  families. PHCS_BFLI3P3B_AFI_00 codes a family value of 0x00. */
	uint8_t  n;					  /*!< \brief [in] Codes number of slots parameter in Param. RFU values are not 
												  checked insed the function! */
    uint8_t *pupi;                /*!< \brief [out] Buffer (4 bytes) for the Pseudo-Unique PICC Identifier (PUPI). */
    uint8_t *application_data;    /*!< \brief [out] Buffer (4 bytes) for the application data. */
    uint8_t *protocol_info;       /*!< \brief [out] Buffer (3 bytes) for the protocol Info */
    uint8_t  protocol_type;       /*!< \brief [out] Protocol Type; 1: compliant to ISO14443-4 */
    void    *self;                /*   [in]  Pointer to the C-interface of this component in order 
                                                to let this member function find its "object". 
                                                Remark: Should be left dangling when calling the 
                                                C++ wrapper. */
} phcsBflI3P3B_RequestParam_t;


/*! \ingroup iso14443_3
 *  \brief Parameter structure for SlotMarker functionality. The SlotMarker function which
 *  takes this parameter performs the timeslot anticollision according to ISO 14443-3.
 */
typedef struct 
{
    uint8_t  apn;				  /*!< \brief [in] Anticollision Prefix byte as defined in ISO 14443 Part 3. */
    uint8_t *pupi;                /*!< \brief [out] Buffer for the Pseudo-Unique PICC Identifier (PUPI). */
    uint8_t *application_data;    /*!< \brief [out] Buffer for the application data. */
    uint8_t *protocol_info;       /*!< \brief [out] Buffer for the protocol Info */
    uint8_t  protocol_type;       /*!< \brief [out] Protocol Type */
    void    *self;                /*   [in]  Pointer to the C-interface of this component in order 
                                                to let this member function find its "object". 
                                                Remark: Should be left dangling when calling the C++ wrapper.  */
} phcsBflI3P3B_SlotmarkerParam_t;

/*! \ingroup iso14443_3
 *  Parameter structure for Attrib functionality. The Attrib function which selects a PICC with a known 
 *  identifier. 
 */
typedef struct 
{
    uint8_t *identifier;                  /*!< \brief [in] Pointer to buffer for identifier of preallocated Card of 
												            a lenght of 4 bytes. */
    uint8_t  param_byte_1;                /*!< \brief [in] Param byte 1 of ATTRIB command (codes SOF, EOF, min TR0, min TR1) */
    uint8_t  param_byte_2;                /*!< \brief [in] Param byte 2 of ATTRIB command (codes max Frame Size)*/
    uint8_t  protocol_type;               /*!< \brief [in] Param byte 3 of ATTRIB command (protocol byte as received during REQB) */
    uint8_t  cid;                         /*!< \brief [in] Param byte 4 of ATTRIB command */
    uint8_t *higher_inf_bytes_buffer;     /*!< \brief [in] Already allocated buffer which contains the higher layer 
												           information bytes. The length is coded in the param length_in.\n 
											         [out] This buffer is also used to handle the output higher layer 
											               information bytes. Ther the maximum buffer size is coded in length_out. */
    uint8_t  higher_inf_bytes_length;	  /*!< \brief [in] Number of higher layer information bytes to be sent. \n
                                                     [out] Number of higher layer information bytes received. */
	uint8_t  mbli;				          /*!< \brief [out] Information on the maximum buffer size of the PICC. */
    void    *self;                        /*   [in] Pointer to the C-interface of this component in order 
                                                          to let this member function find its "object". 
                                                          Remark: Should be left dangling when calling the C++ wrapper.  */
} phcsBflI3P3B_AttribParam_t;

/*! \ingroup iso14443_3
 *  Parameter structure for Halt functionality for ISO14443 Type B.
 *  The HaltB function moves an ISO 14443-3 Type B PICC out of protocol  and checks if the response is ok.
 */
typedef struct 
{
    uint8_t  *identifier;         /*!< \brief [in] Pointer to buffer for identifier of preallocated Card of 
												  a lenght of 4 bytes. */
    void     *self;               /*   [in] Pointer to the C-interface of this component in order 
                                                  to let this member function find its "object". 
                                                  Remark: Should be left dangling when calling the C++ wrapper.  */
} phcsBflI3P3B_HaltParam_t;

/*! \name Typedefs 
 *  \ingroup iso14443_3 
 */
/*@{*/
/* C-interface member function pointer types: */
typedef phcsBfl_Status_t (*pphcsBflI3P3B_Request_t)      (phcsBflI3P3B_RequestParam_t*);
typedef phcsBfl_Status_t (*pphcsBflI3P3B_Slotmarker_t)	(phcsBflI3P3B_SlotmarkerParam_t*);
typedef phcsBfl_Status_t (*pphcsBflI3P3B_Attrib_t)       (phcsBflI3P3B_AttribParam_t*);
typedef phcsBfl_Status_t (*pphcsBflI3P3B_Halt_t)         (phcsBflI3P3B_HaltParam_t*);
/*@}*/

/*! \ingroup iso14443_3
 *  \brief C - Component interface: Structure to call ISO 14443 Part 3 Type B functionality.
 *  ISO 14443.3 functionality represents PICC Initialisation and Anticollision as well as Halt.
 */
typedef struct 
{
    /* Methods: */
    phcsBflIo_SetWecParam_t     SetWaitEventCb;         /* Event detection CB address submission. */

    pphcsBflI3P3B_Request_t       RequestB;               /* RequestB member function pointer. */
    pphcsBflI3P3B_Slotmarker_t    SlotMarker;				/* SlotMarker member function pointer. */
    pphcsBflI3P3B_Attrib_t        Attrib;                 /* Attrib member function pointer. */
    pphcsBflI3P3B_Halt_t          HaltB;                  /* HaltB member function pointer. */

    
    void *mp_Members;   /* Internal variables of the C-interface. Usually a structure is behind
                           this pointer. The type of the structure depends on the implementation
                           requirements. */
    #ifdef PHFL_BFL_CPP
        void *mp_CallingObject;   /* Used by the "Glue-Class" to reference the wrapping
                                     C++ object, calling into the C-interface. */
    #endif

    /* User reference. RCL_WAIT_EVENT_CB receives this value with the parameter
       structure RCL_WAIT_EVENT_CB_PARAM. */
    void *mp_UserRef;
    /* Pointer to the event-detecting callback function, to be implemented by the embedding SW. */
    pphcsBflAux_WaitEventCb_t mp_WaitEventCB;
    /* Point to the lower I/O device: */
    phcsBflIo_t *mp_Lower;
} phcsBflI3P3B_t;



/*! \if use_html 
 *  \ingroup iso14443_3B
 *   Internal control variable structure 
 *  \endif
 */
typedef struct 
{
    uint8_t                    m_InitiatorNotTarget;   /* [in] Initiator access */
    phcsBflI3P4_ProtParam_t   *mp_prot_p;              /* [in] Parameter structure for Iso 14443 Part 4 handling */
    uint16_t                   m_trx_buffer_size;      /* [in] Buffer size for frames composed by the
                                                                activation. Typical values are 64, 128
                                                                or 256[Bytes] */
} phcsBflI3P3B_Params_t;



/* //////////////////////////////////////////////////////////////////////////////////////////////
// ISO14443_3B Initialise for Joiner:
*/
/*!
* \ingroup iso14443_3B
* \param[in,out]  *cif              C object interface structure
* \param[in,out]  *rp               Pointer to the internal control variables structure.
* \param[in]      *p_td             Pointer to the Protocol Parameters structure. This structure is filled
*                                   by REQB/SLOTMARKER and ATRRIB and to be handed over to the exchange protocol (phcsBfl_I3P4)
*                                   object. 
* \param[in]      *p_lower          Pointer to the underlying layer's TRx function.
* \param[in]       initiator__not_target    Specifier for mode whether to support Initiator or Target.                                  Pointer to the underlying layer's function.
* \param[in]      *p_trxbuffer      Pointers to TX and RX buffer, used by the protocol for intermediate storage.
* \param[in]       trxbuffersize    Size of TX and RX buffer, used by the protocol for intermediate storage.
* 
* This function shall be called first to initialise the ISO14443-3 type B component. There the C-Layer, 
* the internal variables, the underlaying layer and the device mode are initialised.
* An own function pointer is typedef'ed for this function to enable the call within a generic C++ 
* ISO14443_3B wrapper. For the C++ wrapper see the file INCISO14443_3B.H.
*
*/
void      phcsBflI3P3B_Init(phcsBflI3P3B_t      *cif,
                          void                      *rp,
                          phcsBflI3P4_ProtParam_t   *p_td,
                          phcsBflIo_t                 *p_lower,
                          uint8_t                    initiator__not_target,
                          uint8_t                   *p_trxbuffer,
                          uint16_t                   trxbuffersize);

/* //////////////////////////////////////////////////////////////////////////////////////////////////
// ISO14443_3B Set Wait Event Cb for Joiner:
*/
/*!
*  \ingroup iso14443_3B
*
*  \param[in,out]   set_wec_param   Pointer to the I/O parameter structure
*
*  This function initializes the user defined callback (interrupt handler) which might be used
*  in the functions of this component.
*/
void phcsBflI3P3B_SetWaitEventCbB(phcsBflIo_SetWecParam_t *set_wec_param);


/* //////////////////////////////////////////////////////////////////////////////////////////////////
// ISO14443_3B Request for Joiner:
*/
/*!
* \ingroup iso14443_3B
* \param[in,out] request_b_param  Pointer to the I/O parameter structure
*
* \retval       PH_ERR_BFL_SUCCESS
* \retval       PH_ERR_BFL_INVALID_PARAMETER
* \retval       PH_ERR_BFL_PROTOCOL_ERROR
* \retval       PH_ERR_BFL_INVALID_FORMAT
* \retval       PH_ERR_BFL_INVALID_DEVICE_STATE
* \retval       Other phcsBfl_Status_t values depending on the underlying components.
* 
* This command handles the Request procedure of ISO14443-3 type B. Depending on the Request Code (PHCS_BFLI3P3B_REQB or 
* PHCS_BFLI3P3B_WUPB) and the state of the cards in the field all cards reply. 
*
* \note In case of an error, the appropriate error code is set. Nevertheless all received data 
*       during the RF-Communication is returned for debugging reasons.
*
*/
phcsBfl_Status_t phcsBflI3P3B_Request(phcsBflI3P3B_RequestParam_t *request_b_param);


/* //////////////////////////////////////////////////////////////////////////////////////////////////
// ISO14443_3B SlotMarker for Joiner:
*/
/*!
* \ingroup iso14443_3B
* \param[in,out] *slotmarker_param		Pointer to the I/O parameter structure
*
* \retval       PH_ERR_BFL_SUCCESS
* \retval       PH_ERR_BFL_INVALID_PARAMETER
* \retval       PH_ERR_BFL_PROTOCOL_ERROR
* \retval       PH_ERR_BFL_INVALID_FORMAT
* \retval       PH_ERR_BFL_INVALID_DEVICE_STATE
* \retval       Other phcsBfl_Status_t values depending on the underlying components.
* 
* \note In case of an error, the appropriate error code is set. Nevertheless all received data 
*       during the RF-Communication is returned for debugging reasons.
*
*/
phcsBfl_Status_t phcsBflI3P3B_SlotMarker(phcsBflI3P3B_SlotmarkerParam_t *slotmarker_param);


/* //////////////////////////////////////////////////////////////////////////////////////////////////
// ISO14443_3B Attrib for Joiner:
*/
/*!
* \ingroup iso14443_3B
* \param[in,out] *attrib_param      Pointer to the I/O parameter structure
*
* \retval       PH_ERR_BFL_SUCCESS
* \retval       PH_ERR_BFL_INVALID_DEVICE_STATE
* \retval       Other phcsBfl_Status_t values depending on the underlying components.
* 
* \note In case of an error, the appropriate error code is set. Nevertheless all received data 
*       during the RF-Communication is returned. This is done for debugging reasons.
*
*/
phcsBfl_Status_t phcsBflI3P3B_Attrib(phcsBflI3P3B_AttribParam_t *attrib_param);


/* //////////////////////////////////////////////////////////////////////////////////////////////////
// ISO14443_3B HaltB for Joiner:
*/
/*!
* \ingroup iso14443_3B
* \param[in,out] *halt_b_param      Pointer to the I/O parameter structure
*
* \retval       PH_ERR_BFL_SUCCESS
* \retval       PH_ERR_BFL_PROTOCOL_ERROR
* \retval       PH_ERR_BFL_INVALID_DEVICE_STATE 
* \retval       Other phcsBfl_Status_t values depending on the underlying components.
* 
*/
phcsBfl_Status_t phcsBflI3P3B_HaltB(phcsBflI3P3B_HaltParam_t *halt_b_param);


#endif /* PHCSBFLI3P3B_H */

/* E.O.F. */


