/* /////////////////////////////////////////////////////////////////////////////////////////////////
//                     Copyright (c) Philips Semiconductors
//
//                       (C)PHILIPS Electronics N.V.2004
//         All rights are reserved. Reproduction in whole or in part is
//        prohibited without the written consent of the copyright owner.
//    Philips reserves the right to make changes without notice at any time.
//   Philips makes no warranty, expressed, implied or statutory, including but
//   not limited to any implied warranty of merchantability or fitness for any
//  particular purpose, or that the use will not infringe any third party patent,
//   copyright or trademark. Philips must not be liable for any loss or damage
//                            arising from its use.
///////////////////////////////////////////////////////////////////////////////////////////////// */

/*! \file phcsBflIo_Wrapper.cpp
 *
 * Project: Object Oriented Library Framework I/O Component.
 *
 *  Source: phcsBflIo_Wrapper.cpp
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Date: Thu Sep 27 09:39:13 2007 $
 *
 * Comment:
 *  C++ wrapper for I/O.
 *
 * History:
 *  GK:  Generated 12. March 2003
 *  MHa: Migrated to MoReUse September 2005
 *
 */

#include <phcsBflIoWrapper.hpp>

using namespace phcs_BFL;

////////////////////////////////////////////////////////////////////////////////////////////////////
// Glue:

phcsBfl_IoGlue::phcsBfl_IoGlue(void)
{
    // Initialise RcRegCtl struct members with static class members:
    m_GlueStruct.SetRegister         = &phcsBfl_IoGlue::SetRegister;
    m_GlueStruct.GetRegister         = &phcsBfl_IoGlue::GetRegister;
    m_GlueStruct.ModifyRegister      = &phcsBfl_IoGlue::ModifyRegister;
    m_GlueStruct.SetRegisterMultiple = &phcsBfl_IoGlue::SetRegisterMultiple;
    m_GlueStruct.GetRegisterMultiple = &phcsBfl_IoGlue::GetRegisterMultiple;
    m_GlueStruct.mp_Members          =  NULL;
    m_GlueStruct.mp_Lower            =  NULL;
}


phcsBfl_IoGlue::~phcsBfl_IoGlue(void)
{
    // For this implementation we don't have anything to clean up.
}


// Static class members, able to call into the lower device's C++ code again:

phcsBfl_Status_t phcsBfl_IoGlue::SetRegister(phcsBflRegCtl_SetRegParam_t *setreg_param)
{
    class phcsBfl_Io *rcio_wrapping_object = (class phcsBfl_Io*)
        (((phcsBflRegCtl_t*)(setreg_param->self))->mp_CallingObject);
    return rcio_wrapping_object->mp_Lower->SetRegister(setreg_param);
}


phcsBfl_Status_t phcsBfl_IoGlue::GetRegister(phcsBflRegCtl_GetRegParam_t *getreg_param)
{
    class phcsBfl_Io *rcio_wrapping_object = (class phcsBfl_Io*)
        (((phcsBflRegCtl_t*)(getreg_param->self))->mp_CallingObject);
    return rcio_wrapping_object->mp_Lower->GetRegister(getreg_param);
}


phcsBfl_Status_t phcsBfl_IoGlue::ModifyRegister(phcsBflRegCtl_ModRegParam_t *modify_param)
{
    class phcsBfl_Io *rcio_wrapping_object = (class phcsBfl_Io*)
        (((phcsBflRegCtl_t*)(modify_param->self))->mp_CallingObject);
    return rcio_wrapping_object->mp_Lower->ModifyRegister(modify_param);
}


phcsBfl_Status_t phcsBfl_IoGlue::SetRegisterMultiple(phcsBflRegCtl_SetMultiRegParam_t *setmultireg_param)
{
    class phcsBfl_Io *rcio_wrapping_object = (class phcsBfl_Io*)
        (((phcsBflRegCtl_t*)(setmultireg_param->self))->mp_CallingObject);
    return rcio_wrapping_object->mp_Lower->SetRegisterMultiple(setmultireg_param);
}


phcsBfl_Status_t phcsBfl_IoGlue::GetRegisterMultiple(phcsBflRegCtl_GetMultiRegParam_t *getmultireg_param)
{
    class phcsBfl_Io *rcio_wrapping_object = (class phcsBfl_Io*)
        (((phcsBflRegCtl_t*)(getmultireg_param->self))->mp_CallingObject);
    return rcio_wrapping_object->mp_Lower->GetRegisterMultiple(getmultireg_param);
}

/* E.O.F. */
