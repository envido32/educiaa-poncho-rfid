/* /////////////////////////////////////////////////////////////////////////////////////////////////
//                     Copyright (c) NXP Semiconductors
//
//                       (C)NXP Electronics N.V.2004
//         All rights are reserved. Reproduction in whole or in part is
//        prohibited without the written consent of the copyright owner.
//    NXP reserves the right to make changes without notice at any time.
//   NXP makes no warranty, expressed, implied or statutory, including but
//   not limited to any implied warranty of merchantability or fitness for any
//  particular purpose, or that the use will not infringe any third party patent,
//   copyright or trademark. NXP must not be liable for any loss or damage
//                            arising from its use.
///////////////////////////////////////////////////////////////////////////////////////////////// */

/*! \file phcsBflBal_Hw1SerWin.c
 *
 * Project: Object Oriented Library Framework phcsBfl_Bal Component.
 *
 *  Source: phcsBflBal_Hw1SerWin.c
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Date: Thu Sep 27 09:39:11 2007 $
 *
 * Comment:
 *  None.
 *
 * History:
 *  GK:  Generated 12. March 2003
 *  MHa: Migrated to MoReUse September 2005
 *
 */

#include <windows.h>
#include <stdio.h>

#include <phcsBflBal.h>
#include "phcsBflBal_Hw1SerWinInt.h"


// Some PCs seem to have problems conveying the RI line to the OS
// Using CTS is probably better, but this yields touching the PCB.
// Hence by default use the RI (with the risk that you might have
// problems...
// #define CTS_HACK

#ifdef CTS_HACK
	#define EV_INTERRUPT_LINE EV_CTS
#else
	#define EV_INTERRUPT_LINE EV_RING
#endif

/* Timeout value used for serial configuration and to guarantee no deadlocks during 
   usage of the comport (ReadBus & WriteBus). */
#define RX_TIMEOUT_VAL      500

/* Internal functions: */
/* Creates file and configures new created interface */
void*   phcsBflBal_Hw1SerWinIniPort(const int8_t* aComPort, uint32_t aBaudRate);
/* Changes host data rate */
void    phcsBflBal_Hw1SerWinCHB(HANDLE Port, uint32_t Baudrate);
/* Sets the timeouts for the com-port */
void    phcsBflBal_Hw1SerWinSetCPT(HANDLE aHandle);


/* Externally accessible functions: */

void phcsBflBal_Hw1SerWinInit(phcsBflBal_t *cif,
                              void         *comm_params)
{
    phcsBflBal_Hw1SerWinInitParams_t *bp = comm_params;

    cif->mp_Members     = comm_params;
    cif->ReadBus        = phcsBflBal_Hw1SerWinReadBus;
    cif->WriteBus       = phcsBflBal_Hw1SerWinWriteBus;
    cif->ChangeHostBaud = phcsBflBal_Hw1SerWinHostBaud;
    cif->ClosePort      = phcsBflBal_Hw1SerWinClosePort;
    
    /* check com_handle and open serial interface if 0 */
    if(bp->com_handle == 0)
    {
        bp->com_handle = phcsBflBal_Hw1SerWinIniPort(bp->com_port, 9600);
    }
}


phcsBfl_Status_t phcsBflBal_Hw1SerWinWriteBus(phcsBflBal_WriteBusParam_t *writebus_param)
{
    #define             BUFFER_LENGTH                     1 /* */
    const uint8_t 	    bytes_to_send                   = BUFFER_LENGTH;
    uint8_t             buffer[BUFFER_LENGTH];
    uint32_t            bytes_sent                      = 0;
    OVERLAPPED          ovr_wr                          = {0};
    phcsBfl_Status_t      status                          = PH_ERR_BFL_SUCCESS;
    
    phcsBflBal_Hw1SerWinInitParams_t *bp = ((phcsBflBal_t*)(writebus_param->self))->mp_Members;

    if ((ovr_wr.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL)) != NULL)
    {
        buffer[0] = writebus_param->bus_data;
        WriteFile(bp->com_handle,
                  buffer,
                  bytes_to_send,
                  &bytes_sent,
                  &ovr_wr);
        if (GetLastError() == ERROR_IO_PENDING)
        {
            /* OK, operation pending: */
            if (WaitForSingleObject(ovr_wr.hEvent, RX_TIMEOUT_VAL*2) == WAIT_OBJECT_0)
            {
                /* OK: The state of the specified object is signaled. */
                GetOverlappedResult(bp->com_handle, &ovr_wr, &bytes_sent, FALSE);
            } else
            {
                /* Wait function exhibits error: */
                status = PH_ERR_BFL_INVALID_DEVICE_STATE;
            }
        } else
        {
            /* Operation returned immediately: Did it return data? */
            if (bytes_sent == 0)
            {
                /* Error: */
                status = PH_ERR_BFL_INTERFACE_ERROR;
            }
        }
        CloseHandle(ovr_wr.hEvent);
    } else
    {
        /* Event creation failure: */
        status = PH_ERR_BFL_INSUFFICIENT_RESOURCES;
    }
    PHCS_BFL_ADD_COMPCODE(status, PH_ERR_BFL_BAL);
    return status;// | PH_ERR_BFL_BAL;
    #undef BUFFER_LENGTH /* */
}


phcsBfl_Status_t phcsBflBal_Hw1SerWinReadBus(phcsBflBal_ReadBusParam_t *readbus_param)
{
    #define             BUFFER_LENGTH             1 /* */
    const uint8_t       bytes_to_read           = BUFFER_LENGTH;
    phcsBfl_Status_t    status                  = PH_ERR_BFL_SUCCESS;
    uint8_t             buffer[BUFFER_LENGTH];
    uint32_t            bytes_read              = 0;
    OVERLAPPED          ovr_rd                  = {0};
    phcsBflBal_Hw1SerWinInitParams_t *bp = ((phcsBflBal_t*)(readbus_param->self))->mp_Members;

    if ((ovr_rd.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL)) != NULL)
    {
        ReadFile(bp->com_handle,
                 buffer,
                 bytes_to_read,
                 &bytes_read,
                 &ovr_rd);
        if (GetLastError() == ERROR_IO_PENDING)
        {
            /* Overlapped operation in progress: */
            if (WaitForSingleObject(ovr_rd.hEvent, RX_TIMEOUT_VAL*2) == WAIT_OBJECT_0)
            {
                /* OK: The state of the specified object is signaled. */
                DWORD error = GetLastError();
                GetOverlappedResult(bp->com_handle, &ovr_rd, &bytes_read, FALSE);
                error = GetLastError();
                //printf("OVR OK: Bytes read = %d, error = 0x%08X\n", bytes_read, error);
            } else
            {
                /* Wait function exhibits error: */
                DWORD error = GetLastError();
                status = PH_ERR_BFL_INVALID_DEVICE_STATE;
                //printf("OVR ERR: Bytes read = %d, error = 0x%08X\n", bytes_read, error);
            }

        } else
        {
            /*
             * Returned immediately:
             * Could be OK: Success is tested at the end upon returned byte count:
             */
        }
        CloseHandle(ovr_rd.hEvent);
    } else
    {
        /* Could not create event: */
        status = PH_ERR_BFL_INSUFFICIENT_RESOURCES;
    }
    
    if (status == PH_ERR_BFL_SUCCESS)
    {
        readbus_param->bus_data = buffer[0];
        if (bytes_read != 0)
        {
            /* OK: No further action req'd. */
        } else
        {
            status = PH_ERR_BFL_INTERFACE_ERROR;
        } 
    }
    PHCS_BFL_ADD_COMPCODE(status, PH_ERR_BFL_BAL);
    return status; // | PH_ERR_BFL_BAL;
    #undef BUFFER_LENGTH /* */
}


void phcsBflBal_Hw1SerWinHostBaud(phcsBflBal_HostBaudParam_t *hostbaud_param)
{
    phcsBflBal_Hw1SerWinInitParams_t *bp = ((phcsBflBal_t*)(hostbaud_param->self))->mp_Members;

    phcsBflBal_Hw1SerWinCHB( bp->com_handle, hostbaud_param->baudrate);
    phcsBflBal_Hw1SerWinSetCPT(bp->com_handle);
}


/* Internal functions */

HANDLE phcsBflBal_Hw1SerWinIniPort(const int8_t* aComPort, uint32_t aBaudRate)
{
    HANDLE handle;
    TCHAR  Port[10];
    size_t i;

    /* check if errornous input (both handle and comport 0) */
    if (aComPort == NULL) 
    {
        handle = NULL;
        return handle;
    }

    for (i=0;i<strlen(aComPort);i++)
    {
        if(i == 10)
        {   /* exit criteria for too long com name */
            handle = NULL;
            return handle;
        }
        Port[i] = aComPort[i];
    }
    Port[i] = 0x00;     /* add 0 terminator at end of string */

    /* Aquire the handle */
	handle = CreateFile(Port,
                        GENERIC_READ | GENERIC_WRITE,      // Access (read-write) mode
                        0,                                 // Share mode
                        NULL,                              // Pointer to the security attribute
                        OPEN_EXISTING,                     // How to open the serial port
                        FILE_FLAG_OVERLAPPED,              // Port attributes
                        NULL);                             // Handle to port with attribute to copy


    if (handle != (HANDLE)INVALID_HANDLE_VALUE)
    {
        /* Open OK - apply default settings to the port */
        phcsBflBal_Hw1SerWinCHB( handle, aBaudRate);
        phcsBflBal_Hw1SerWinSetCPT(handle);
    } else
    {
		/* Not OK -> invalidate handle and print error message */
        handle = NULL;
    }

	if (!SetCommMask(handle, EV_INTERRUPT_LINE))
	{
	    if(GetLastError() != ERROR_SUCCESS);
	    {
		    handle = NULL;
		}
	}

	return handle;
}

/* 
 * Changes host baudrate and defines other important values (parity and
 * the like.
 */
void phcsBflBal_Hw1SerWinCHB(HANDLE Port, uint32_t Baudrate)
{
    DCB             dcb;
    
    SetLastError(ERROR_SUCCESS);
    
    dcb.BaudRate = Baudrate;
    dcb.DCBlength = sizeof (dcb);
    dcb.fBinary = TRUE;
    dcb.fParity = FALSE;
    dcb.fOutxCtsFlow = FALSE;
    dcb.fOutxDsrFlow = FALSE;
    dcb.fDtrControl = DTR_CONTROL_DISABLE;
    dcb.fDsrSensitivity = FALSE;
    dcb.fTXContinueOnXoff = FALSE;
    dcb.fOutX = FALSE;
    dcb.fInX = FALSE;
    dcb.fErrorChar = FALSE;
    dcb.fNull = FALSE; 
    dcb.fRtsControl = RTS_CONTROL_DISABLE; 
    dcb.fAbortOnError = FALSE;
    dcb.XonLim = 0; 
    dcb.XoffLim = 0;  
    dcb.ByteSize = 8; 
    dcb.Parity = NOPARITY; 
    dcb.StopBits = ONESTOPBIT;
    
	/* Apply the settings */
	SetCommState(Port, &dcb);

	// Set the interrupt mask  (currently either CTS or RI, depends on the setting in SerialWindows.h )
	SetCommMask((HANDLE) Port, EV_INTERRUPT_LINE);
}

/*
 * Specify the timeout values
 */
void phcsBflBal_Hw1SerWinSetCPT(HANDLE aHandle)
{
    COMMTIMEOUTS    cto;

    SetLastError(ERROR_SUCCESS);
    cto.ReadIntervalTimeout = MAXDWORD;
    cto.ReadTotalTimeoutMultiplier = MAXDWORD;
    cto.ReadTotalTimeoutConstant = RX_TIMEOUT_VAL;
    cto.WriteTotalTimeoutConstant = 0;
    cto.WriteTotalTimeoutMultiplier = 0;
    SetCommTimeouts(aHandle, &cto);    
}

/*
 * Closes the port via aquired handle
 */
void phcsBflBal_Hw1SerWinClosePort(phcsBflBal_ClosePortParam_t *closeport_param)
{
    phcsBflBal_Hw1SerWinInitParams_t *bp = ((phcsBflBal_t*)(closeport_param->self))->mp_Members;

    phcsBflBal_Hw1SerWinCHB(bp->com_handle, 9600);

    if (bp->com_handle != 0)
    {
        CloseHandle(bp->com_handle);
    }
}

/* E.O.F. */
