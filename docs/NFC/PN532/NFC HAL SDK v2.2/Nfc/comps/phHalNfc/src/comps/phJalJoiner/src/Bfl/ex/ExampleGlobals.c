/* /////////////////////////////////////////////////////////////////////////////////////////////////
//                     Copyright (c) Philips Semiconductors
//
//                       (C)PHILIPS Electronics N.V.2005
//         All rights are reserved. Reproduction in whole or in part is
//        prohibited without the written consent of the copyright owner.
//    Philips reserves the right to make changes without notice at any time.
//   Philips makes no warranty, expressed, implied or statutory, including but
//   not limited to any implied warranty of merchantability or fitness for any
//  particular purpose, or that the use will not infringe any third party patent,
//   copyright or trademark. Philips must not be liable for any loss or damage
//                            arising from its use.
///////////////////////////////////////////////////////////////////////////////////////////////// */

/*! \file ExampleGlobals.c
 *
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Date: Thu Sep 27 09:38:47 2007 $
 *
 */

#include <phcsTypeDefs.h>


#ifndef __EXAMPLE_GLOBALS_C__
#define __EXAMPLE_GLOBALS_C__

const int8_t gHelpString[] = 
"\nExampleProject/BFL Source Code Examples, (c) Philips Semiconductors 2004, 2005\n\n\
Windows : ExampleProject[C].exe [-i ComIfc] [-o OpMode] [-m p|a] [-s 0-2] [-h]\n\
Linux   : C[pp]Example [-i ComIfc] [-o OpMode] [-m p|a] [-s 0-2] [-h] \n\
\n\
    -h           Show this help \n\
    -i ComIfc    RS232 Interface (e.g. -i COM1 or -i /dev/ttyS0) \n\
    -d ComBr     RS232 Baudrate (default: 9600; e.g. -d 115200) \n\
       Following baudrates are supported: \n\
           9600, 115200, 128000, 230400, 460800, 921600, 1288000 \n\
    -o [1-6] Operating Mode (e.g. -o 3 for NFC Initiator) \n\
       Following Operating Modes are supported: \n\
           1 ... Mifare Reader \n\
           2 ... FeliCa Reader (just Polling) \n\
           3 ... NFC Initiator \n\
           4 ... NFC Target \n\
           5 ... ISO14443-4 Reader \n\
           6 ... Power Down Example\n\
    -m [p|a] Passive/Active Mode (only relevant for NFC-I)\n\
           p ... Passive Mode \n\
           a ... Active Mode \n\
    -s [0-2] Bit Rate (only relevant for NFC-I) \n\
           0 ... 106kBit/s \n\
           1 ... 212kBit/s \n\
           2 ... 424kBit/s \n\
";

#endif /* __EXAMPLE_GLOBALS_C__ */
