/* /////////////////////////////////////////////////////////////////////////////////////////////////
//                     Copyright (c) Philips Semiconductors
//
//                       (C)PHILIPS Electronics N.V.2004
//         All rights are reserved. Reproduction in whole or in part is
//        prohibited without the written consent of the copyright owner.
//    Philips reserves the right to make changes without notice at any time.
//   Philips makes no warranty, expressed, implied or statutory, including but
//   not limited to any implied warranty of merchantability or fitness for any
//  particular purpose, or that the use will not infringe any third party patent,
//   copyright or trademark. Philips must not be liable for any loss or damage
//                            arising from its use.
///////////////////////////////////////////////////////////////////////////////////////////////// */

/*! \file phcsBflIdMan_Wrapper.cpp
 *
 * Project: ID Manager.
 *
 * Workfile: phcsBflIdMan_Wrapper.cpp
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Date: Thu Sep 27 09:39:28 2007 $
 * $KeysEnd$
 *
 * Comment:
 *  Id Manger class framework wrapper to enable multi-instance operation.
 *
 * History:
 *  GK:  Generated 8. Sept. 2003
 *  MHa: Migrated to MoReUse September 2005
 *
 */


#include <phcsBflIdManWrapper.hpp>

using namespace phcs_BFL;

phcsBflIdMan_Wrapper::phcsBflIdMan_Wrapper(void)
{
    m_ConstructorStatus = phcsBflIdMan_Init(&m_Interface, &m_IdmanParam);
}


phcsBflIdMan_Wrapper::~phcsBflIdMan_Wrapper(void)
{
    // No action.
}



phcsBfl_Status_t phcsBflIdMan_Wrapper::GetFreeID(phcsBflIdMan_Param_t *idman_param)
{
    phcsBfl_Status_t status;
    void       *p_self; 
    
    p_self = idman_param->self;
    idman_param->self = &m_Interface;
    status = m_Interface.GetFreeID(idman_param);
    idman_param->self = p_self;

    return status;
}


phcsBfl_Status_t phcsBflIdMan_Wrapper::IsFreeID(phcsBflIdMan_Param_t *idman_param)
{
    phcsBfl_Status_t status;
    void       *p_self; 
    
    p_self = idman_param->self;
    idman_param->self = &m_Interface;
    status = m_Interface.IsFreeID(idman_param);
    idman_param->self = p_self;

    return status;
}


phcsBfl_Status_t phcsBflIdMan_Wrapper::AssignID(phcsBflIdMan_Param_t *idman_param)
{
    phcsBfl_Status_t status;
    void       *p_self; 
    
    p_self = idman_param->self;
    idman_param->self = &m_Interface;
    status = m_Interface.AssignID(idman_param);
    idman_param->self = p_self;

    return status;
}


phcsBfl_Status_t phcsBflIdMan_Wrapper::FreeIDByNumber(phcsBflIdMan_Param_t *idman_param)
{
    phcsBfl_Status_t status;
    void       *p_self; 
    
    p_self = idman_param->self;
    idman_param->self = &m_Interface;
    status = m_Interface.FreeIDByNumber(idman_param);
    idman_param->self = p_self;

    return status;
}


phcsBfl_Status_t phcsBflIdMan_Wrapper::FreeIDByInstance(phcsBflIdMan_Param_t *idman_param)
{
    phcsBfl_Status_t status;
    void       *p_self; 
    
    p_self = idman_param->self;
    idman_param->self = &m_Interface;
    status = m_Interface.FreeIDByInstance(idman_param);
    idman_param->self = p_self;

    return status;
}

//EOF
