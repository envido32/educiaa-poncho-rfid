/*
 *         Copyright (c), NXP Semiconductors
 *
 *                       (C)NXP Electronics N.V.2004
 *         All rights are reserved. Reproduction in whole or in part is
 *        prohibited without the written consent of the copyright owner.
 *    NXP reserves the right to make changes without notice at any time.
 *   NXP makes no warranty, expressed, implied or statutory, including but
 *   not limited to any implied warranty of merchantability or fitness for any
 *  particular purpose, or that the use will not infringe any third party patent,
 *   copyright or trademark. NXP must not be liable for any loss or damage
 *                            arising from its use.
 *
 */

/*!
 * \file phJalJoinerBflFrame.h
 * \brief JAL BFL Frame structure and functions and Meta Mode declarations
 *
 *
 * Project: NFC MW / HAL
 *
 * $Date: Thu Sep 27 09:38:41 2007 $ 
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Aliases: SDK_HAL_2.2,SDK_HAL_2.2_071008,NFC_FRI_SDK_1.0_071031 $
 *
 */

#ifndef PHJALJOINERBFLFRAME_H
#define PHJALJOINERBFLFRAME_H

/*! \ingroup grp_file_attributes
 *  \name JAL BFL Frame
 *  File: \ref phJalJoinerBflFrame.h
 */
/*@{*/
#define PHJALJOINERBFLFRAME_FILEREVISION "$Revision: 1.1 $"
#define PHJALJOINERBFLFRAME_FILEALIASES  "$Aliases: SDK_HAL_2.2,SDK_HAL_2.2_071008,NFC_FRI_SDK_1.0_071031 $"
/*@}*/

#include <phcsBflOpCtl.h>
#include <phcsBflI3P3A.h>
#include <phcsBflIo.h>
#include <phcsBflNfc.h>
#include <phcsBflI3P4AAct.h>
#include <phcsBflI3P4.h>
#include <phcsBflMfRd.h>
#include <phcsBflFelRd.h>
#include <phcsBflRegCtl.h>
#include <phcsBflPolAct.h>
#include <phcsBflNfcCommon.h>

#include <phNfcHalTypes.h>
#include <phJalDebugReportFunctions.h>


#if (defined PHJALJOINER_TRACE)
    /* We have the trace switched on */
    #include <time.h>
    #define phJal_Trace(hwref, msg)          phJal_BFLFrameTrace(hwref, msg)    /* */
#else
    /* Trace == OFF */
    #define phJal_Trace(hwref, msg)          ((void)0)                          /* */
#endif /*PHJALJOINER_TRACE */


#define PHJAL_J_VD_NAME     "NXP SEMICONDUCTORS" /* PN 511 chip vendor name. */

#define PHJAL_TRXBL                     64  /* TRx buffer length supported by current BFL. */
#define PHJAL_TARGET_BUFFER_SIZE        58  /* Target buffer length */
#define PHJAL_ENDPOINT_BUFFER_SIZE      58  /* Target buffer length */
#define PHJAL_MAX_RDEV                  4   /* Maximum number of Remote Devices. */
#define PHJAL_MAX_UID_LENGTH            10  /* Maximum number of UID bytes for a cache entry  */
#define PHJAL_MAX_UID_CACHE_ENTRIES     10  /* Maximum number of UID cache entries  */
#define PHJAL_MAX_HANDLE_ENTRIES        10  /* Maximum number of Handle Entries available for remote devices */
#define PHJAL_MAX_GENERIC_BYTES         48  /* Maximum number of Generic Bytes for ATR Request/Response */

#define OP_SUCCEEDED        0x00
#define UID_CACHE_FULL      0x01
#define INVALID_CACHE       0x02
#define NO_HANDLE_AVAILABLE (-1)
#define WAIT_INFINITE       0xFFFFFFFF

#define OP_MODE_IN_LIST         0x01
#define OP_MODE_COMPATIBLE      0x00
#define OP_MODE_NOT_COMPATIBLE  0x01

// reception handling states
#define NO_STATE                    0x00
#define START_RECEPTION             0x01
#define CONFIGURE_TARGET            0x02
#define RECEPTION_TIMEOUT           0x04
#define DATA_RECEIVED               0x08
#define TERMINATE_RECEPTION         0x10
#define AUTOMATIC_ATR_RES_SENT      0x20
#define USER_ATR_SENT               0x80
#define USER_DATA_SENT              0x100
#define TRANSMISSION_ERROR          0x200
#define TARGET_NOT_AVAILABLE        0x400
#define MORE_DATA_AVAILABLE         0x800
#define TIMEOUT_OCCURED             0x1000
#define USER_READY                  0x2000
#define USER_META_CHAINING          0x4000
#define USER_BUFFER_TOO_SMALL       0x8000
#define TARGET_RELEASED_NOW         0x10000
#define RECEPTION_ABORTED           0x20000
#define INTERFACE_ERROR             0x40000



// target configuration states
/** \anchor TargetStates */
#define TARGET_NOT_CONFIGURED               0x00
#define TARGET_CONFIGURATION_IN_PROGRESS    0x01
#define TARGET_READY                        0x02
#define TARGET_DESELECT_IN_PROGRESS         0x03
#define TARGET_DESELECT_CONFIGURATION       0x04
#define TARGET_DESELECTED                   0x05
#define TARGET_WAKED_UP                     0x06
#define TARGET_RELEASED                     0x07


#define MIFARE_KEY_LENGTH               6

#define INITATOR_MODE                   0x01
#define TARGET_MODE                     0x02


// DEP states in target endpoint
#define DEP_START                       0x01
#define DEP_CONTINUE                    0x02
#define DEP_MI_CONTINUE                 0x03
#define DEP_DONE                        0x04

// DEP Modes of target endpoint
#define DEP_RECEPTION                   0x01
#define DEP_TRANSMISSION                0x02

/* New BFL-Status values : From user-defined space! */
#define PH_ERR_BFL_ABORTED              0x0080   /* Command has been aborted in the driver/DAL ... */

/* Interrupt Event to sense. */
#define JAL_SENSE_RI                    0       /* */
#define JAL_SENSE_CTS                   1       /* */
#define JAL_SENSE_DSR                   2       /* */
#define JAL_SENSE_OTHER                 0xFF    /* */


typedef enum {RF106K = 0, RF212K, RF424K} NfcSpeeds;
typedef enum {PASSIVE_MODE = 0, ACTIVE_MODE, NO_MODE} NfcModes;
typedef enum {TX_SPEED = 0, RX_SPEED, BOTH_SPEEDS} SpeedConfiguration;



/* Op-Mode Matrix (yes, a global ... but ro :-) ):
 * The modes, defined in the HAL Types file are tied together
 * to Meta-Modes. These meta-modes are again tied together into an
 * array. A Meta-mode is a set of modes compatible to each other.
 *
 * The Polling function can, depending on the first Remote Device
 * found, determine which other modes can be polled safely from
 * that moment on.
 * 
 * Please notice that each Meta-Mode must be terminated accordingly.
 * Also, the array of Meta-modes is terminated by NULL.
 */
extern const phHal_eOpModes_t PHJAL_P106[];
extern const phHal_eOpModes_t PHJAL_P106B[];
extern const phHal_eOpModes_t PHJAL_P212_424[];
extern const phHal_eOpModes_t PHJAL_A[];
extern const phHal_eOpModes_t *PHJAL_OPMODE_MATRIX[];

/**
 * \ingroup grp_jal_nfct_aux_functions
 * \brief Structure containing all information stored in a UID Cache Entry
 *
 */
typedef struct phJal_sUIDCacheEntry
{
    uint8_t             UIDLength;      
    uint8_t             UID[PHJAL_MAX_UID_LENGTH];
    uint8_t             EntryAvailable;
} phJal_sUIDCacheEntry_t;

/**
 * \ingroup grp_jal_nfct_aux_functions
 * \brief UID Cache structure containing an array of Cache Entries
 *
 */
typedef struct phJal_sUIDCache
{
    uint8_t                 NumUIDs;      
    phJal_sUIDCacheEntry_t  CacheEntries[PHJAL_MAX_UID_CACHE_ENTRIES];
} phJal_sUIDCache_t;

/**
 * \ingroup grp_jal_nfct_aux_functions
 * \brief Logical Handle structure for assigning handles to \ref phHal_sRemoteDevInformation_t structures
 */
typedef struct phJal_HandleEntry
{
    uint8_t HandleAvailable;
    uint8_t Handle;
} phJal_HandleEntry_t;

/**
 * \ingroup grp_jal_nfct_aux_functions
 * \brief Structure mapping protocol pointers to logical handles.
 *
 * ProtocolEntry stores the dependency between a pointer to a protocol structure and
 * a logical handle. Furthermore a flag indicates the state of availabilty for
 * this entry.
 */
typedef struct phJal_ProtocolEntry
{
    uint8_t ProtocolAvailable;
    void    *Protocol;
    uint8_t LogicalHandle;
    uint8_t DeviceId;
} phJal_ProtocolEntry_t;

/**
 * \ingroup grp_jal_nfci_aux_functions
 * \brief Wrapper structure for NFC Initiator Protocol
 */
typedef struct phJal_NfcIProtocol
{
    phcsBflNfc_Initiator_t  *Protocol;           /**< Pointer to the NFC Initiator protocol structure. */
    uint32_t                ResponseWaitingTime; /**< Waiting Time Value received by the taget. */
} phJal_NfcIProtocol_t;

/**
 * \ingroup grp_jal_nfct_aux_functions
 * \brief Strucuture containing all variables and structures needed for reception handling in Target Mode.
 *
 * \note This structure is only used in Target Mode.
 */
typedef struct phJal_ReceptionHandlingParams
{
    uint32_t                        ReceptionInfoState;                     /**< Variable containing the reception state during ATR. Synchronised with phJal_sBFLUpperFrame_t::ReceptionInfoHandle. */ 
    uint32_t                        UserTimeoutState;                       /**< Variable handling the user timeout states during DEP. Synchronised with phJal_sBFLUpperFrame_t::UserTimeoutHandle. */ 
    uint32_t                        TerminationState;                       /**< Variable containing the termination state (used to stop the reception thread). Synchronised with phJal_sBFLUpperFrame_t::TerminateReceptionHandle. */
    uint32_t                        ReceptionEndpointToUserState;           /**< Variable containing the reception state for events sent by the Endpoint to the user thread. Synchronised with phJal_sBFLUpperFrame_t::ReceptionEndpointToUserHandle. */
    uint32_t                        ReceptionUserToEndpointState;           /**< Variable containing the reception state for events sent by the user thread to the Endpoint. Synchronised with phJal_sBFLUpperFrame_t::ReceptionUserToEndpointHandle. */
    uint32_t                        TransmissionEndpointToUserState;        /**< Variable containing the transmission state for events sent by the Endpoint to the user thread. Synchronised with phJal_sBFLUpperFrame_t::TransmissionEndpointToUserHandle. */
    uint32_t                        TransmissionUserToEndpointState;        /**< Variable containing the transmission state for events sent by the user thread to the Endpoint. Synchronised with phJal_sBFLUpperFrame_t::TransmissionUserToEndpointHandle. */
    uint32_t                        ReceptionUserReadyForMetaChaining;      /**< Variable containing the Meta Chaining state of the user thread (sent by user thread to the Endpoint). Synchronised with phJal_sBFLUpperFrame_t::ReceptionUserReadyForMetaChaining. */
    phHal_sHwReference_t            *psHwReference;                         /**< Hardware Reference currently in use. */
    phHal_sTargetInfo_t             *pTgInfo;                               /**< Pointer to the \ref phHal_sTargetInfo_t structure only available during the call of \ref phHalNfc_StartTargetMode. */
    phHal_eOpModes_t                *OpModes;                               /**< Pointer to the array of OpModes passed to the \ref phHalNfc_StartTargetMode function. */
    uint8_t                         *pConnectionReq;                        /**< Pointer to the array containing the ATR Response passed by the user to \ref phHalNfc_StartTargetMode.*/
    uint8_t                         *pConnectionReqBufLength;               /**< Length of the Connection Request buffer. */
    uint8_t                         *pUserData;                             /**< Pointer to buffer for exchanging data between user and Endpoint. */
    uint16_t                        UserDataLength;                         /**< Current length of the User Data buffer. */
    uint16_t                        NumBytesReceivedByLastReceive;          /**< Number of bytes received by the last call of the RcIO receive function. */
    uint8_t                         TargetConfiguration;                    /**< Variable containing the state of the current Target. (see \ref TargetStates) */
    uint8_t                         TargetRxSpeed;                          /**< Rx Speed of the Target. */
    uint8_t                         TargetTxSpeed;                          /**< Tx Speed of the Target. */
    uint8_t                         InitialiseTarget;                       /**< Flag indicating wether to initialise the Target from scratch or not. */
    uint8_t                         ConsumeReceptionEvent;                  /**< Flag indicating wether to consume the \ref phJal_sBFLUpperFrame_t::ReceptionInfoHandle or not. */
    uint8_t                         WaitingTimeMultiplier;                  /**< Multiplier for increasing the Waiting Time in case of WTX. */
    uint8_t                         WTXRequested;                           /**< Flag indicating wether a WTX was requested or not. */
    uint8_t                         WaitingForMoreData;
    uint8_t                         EndpointDEPState;
    uint8_t                         EndpointDEPMode;
    uint8_t                         NadValue;
    uint8_t                         NadPresent;
    uint8_t                         MetaChainingActive;
    uint8_t                         EnableCRCAfterNextReception;
    uint8_t                         UserNotifiedAboutDeselectedState;
} phJal_ReceptionHandlingParams_t;


/*
 * \ingroup grp_jal_nfct_aux_functions
 * \brief Parameter structure for Event Callback located in DAL
 *
 * All members contained in the structure are used by the Event Callback
 * in the BFL.
 */
typedef struct phJal_WaitForReceptionParams
{
    phHal_sHwReference_t        *psHwReference;     /**< Pointer to Hardware Reference currently in use. */
} phJal_WaitForReceptionParams_t;


/**
 * \ingroup grp_jal_nfct_aux_functions
 * \brief BFL Upper Frame structure containing all BFL structures and further variables for data handling.
 *
 * The BFL Frame is subdivided into two so called frames, the Upper and the Lower frame. Hardware dependent
 * functions are located in the lower frame and thus located in the DAL. High-level functionality is contained
 * in the upper frame and is located in the JAL. Both frames are instantiated and connected together in the DAL.
 */
typedef struct phJal_sBFLUpperFrame
{
    uint8_t             TRxBuffer[PHJAL_TRXBL];                         /**< TX/RX buffer for I/O.*/
    uint16_t            TRxBufferSize;                                  /**< Size of the TX/RX buffer. */
    uint8_t             TargetBuffer[PHJAL_TRXBL];                      /**< Buffer used in Target Endpoint for saving data not fetched in time by the user. */ 
    uint16_t            CurrentTargetBufferLength;                      /**< Current length of the Endpoint buffer. */
    
    uint8_t             AtrResponseBuffer[PHJAL_TRXBL];                 /**< Buffer containing all bytes for ATR_RES sent by Target to Initiator */
    uint16_t            AtrResponseLength;                              /**< Number of bytes the \ref phJal_sBFLUpperFrame_t::AtrResponseBuffer contains. */ 
    phJal_sUIDCache_t   UIDCache;                                       /**< Cache for UIDs (only used with \ref PHJAL_P106 Meta Mode. */    
    phcsBflRegCtl_t     *RcRegCtl;                                      /**< Pointer to the RcRegCtl structure connecting both frames together. */

    const phHal_eOpModes_t    *CurrentMetaMode;                         /**< Current Meta Mode used by the Poll function. */
    uint8_t                   InitialPollExecuted;                      /**< Flag indicating wether the Poll function was already executed or not. */ 
    uint8_t                   CIDlessModeActivated;                     /**< Flag representing wether remote devices with no support for CID are used or not. */

    phJal_HandleEntry_t LogHandleEntries[PHJAL_MAX_HANDLE_ENTRIES];     /**< Handle entries for remote devices. */
    phJal_HandleEntry_t CidEntries[PHJAL_MAX_RDEV];                     /**< Available CID entries for ISO14443-4 devices. */
    phJal_HandleEntry_t DidEntries[PHJAL_MAX_RDEV];                     /**< Available DID entries for NFC devices. */   
    
    phcsBflOpCtl_t  RcOpCtl;                                            /**< RcOpCtl structure. */   
    phcsBflOpCtl_Hw1Params_t    RcOpCtlParams;                          /**< RcOpCtl members structure. */

    phcsBflI3P3A_t              Iso14443_3A;                            /**< The ISO 14443-3 structure, as fixed part of the container. */
    phcsBflI3P3A_Hw1Params_t    Iso14443_3P;                            /**< Internals of ISO 14443-3. */

    phcsBflPolAct_t FeliCaPolling;                                      /**< The Fellica-Polling structure. */
    phcsBflPolAct_PollingParam_t FeliCaPollingParams;                   /**< Internals of Fellica-Polling. */

    phcsBflIo_t          RcIo;                                          /**< The RC-I/O structure. */
    phcsBflIo_Hw1Params_t RcIoP;                                        /**< Internals of RC-I/O. */

    phcsBflNfc_Initiator_t  NfcI[PHJAL_MAX_RDEV];                       /**< The NFC-I structure. */
    phJal_NfcIProtocol_t     ExtendedNfcIProtocol[PHJAL_MAX_RDEV];      /**< Structure containing a pointer to a NFC-I structure and a Waiting Time Value. */
    phcsBflNfc_InitiatorCommParams_t NfcICp[PHJAL_MAX_RDEV];            /**< Internals of NFC-I. */
    phJal_ProtocolEntry_t  NfcInitiator_Entries[PHJAL_MAX_RDEV];        /**< Protocol Entries storing handles to protocol instances. */
    uint8_t         INfcIdT[PHCS_BFLNFCCOMMON_ID_LEN];                  /**< NFC-ID required by Initiator. */

    phcsBflNfc_Target_t     NfcT;                                       /**< The NFC-T structure, as fixed part of the container. */
    phcsBflNfc_TargetCommParam_t NfcTCp;                                /**< Internals of NFC-T. */
    phcsBflNfc_TargetEndpoint_t NfcTargetEndpoint;                      /**< The NFC-T-Endpoint structure. */
    uint8_t TargetTimeout;                                              /**< Timeout value received during ATR from target */
    uint8_t STMDoesSoftReset;                                           /**< Flag that shall tell STM do do SR: Default: ON. */
    
    uint8_t     TNfcIdI[PHCS_BFLNFCCOMMON_ID_LEN];                      /**< NFC-IDs req'd by Target. */
    uint8_t     TNfcIdT[PHCS_BFLNFCCOMMON_ID_LEN];                      /**< NFC-IDs req'd by Target. */

    phcsBflNfc_TargetDispatchParam_t NfcTargetDispatchParam;            /**< Dispatch parameter for Target Endpoint-Callback. */

    phcsBflI3P4AAct_t ISO14443_4A_Act;                                  /**< The T=CL Activation structure, as fixed part of the container. */
    phcsBflI3P4_ProtParam_t ISO14443_4A_Pp;                             /**< The protocol parameters, as filled in by the T=CL activation. */
    phcsBflI3P4_t    ISO14443_4[PHJAL_MAX_RDEV];                        /**< The T=CL structure, as fixed part of the container. */
    phcsBflI3P4_CommParam_t ISO14443_4Cp[PHJAL_MAX_RDEV];               /**< Internals of T=CL. */
    phJal_ProtocolEntry_t  ISO14443_4Entries[PHJAL_MAX_RDEV];           /**< Protocol Entries storing handles to protocol instances. */
    phcsBflMfRd_t  MfRd;                                                /**< The MF RD structure. */
    phcsBflMfRd_InternalParam_t   MfRdP;                                /**< Internals of MF-RD. */
    phcsBflFelRd_t  FelRd;                                              /**< The FELICA RD structure. */
    phcsBflFelRd_InternalParam_t   FelRdP;                              /**< Internals of FEL-RD. */

    uint8_t CurrentMifareKey[MIFARE_KEY_LENGTH];                        /**< Mifare Key used for ongoing operations (only available after authentication) */
    uint8_t AuthenticatedBlockAddress;                                  /**< Address of block authenticated. */
    uint8_t MifareKeyAvailable;                                         /**< If true a Mifare Key is available otherwise not. */
    uint8_t MifareAuthentCommand;                                       /**< Type of authentication (Authent_A or Authent_B). */

    uint8_t BrsValue;                                                   /**< BRS value of PSL Request. */
    uint8_t FslValue;                                                   /**< FSL value of PSL Request. */
    NfcModes CurrentInitiatorMode;                                      /**< Current Mode Initiator is configured for. */

    phcsBflOpCtl_AttribParam_t  RcOpCtlAttribParam;                     /**< Parameters to be used with the BFL functions. */
    phcsBflRegCtl_GetRegParam_t RcRegCtlGetRegParam;
    phcsBflRegCtl_SetRegParam_t RcRegCtlSetRegParam;
    phcsBflRegCtl_ModRegParam_t RcRegCtlModifyParam;

    /* ATR, User, and WTX related waiting time. */
    uint8_t  CurrentWT;
    uint32_t JalWaitingTime;

    /* Target LOOP - related: */

    /* 
     *    Change of WaitForSemaphoreProtectedEvent() function in StartTargetMode:
     *
     *          Since the same handle (ReceptionInfoState) is used in two threads - 
     *          the current thread (phJalJoiner_StartTargetMode) and the other thread 
     *          namely, phJal_ReceptionHandling thread, this leads to some deadlock 
     *          condition. So, two handles namely ReceptionInfoState_STM for the former
     *          thread and ReceptionInfoState_RHT for the latter are introduced.
     *
     */

    void *ReceptionInfoHandle_STM;                                      /**< Handle to the semaphore protecting the Reception Loop for Target Mode - used for StartTargetMode. */ 
    void *ReceptionInfoHandle_RHT;                                      /**< Handle to the semaphore protecting the Reception Loop for Target Mode - used for ReceptionHandling. */ 
    void *TargetInfoHandle;                                             /**< Handle to the semaphore protecting user data handling for Target Mode. */
    void *TerminateReceptionHandle;                                     /**< Semaphore indicating that the reception loop shall be closed now. */
    void *UserTimeoutHandle;                                            /**< Handle to the semaphore used to handle WTX timeouts for Target Mode. */
    void *TargetModeRunningHandle;                                      /**< Semaphore used to protecting the Target Mode thread to ensure that only thread is running at a time. */
    void *ReceptionThreadHandle;                                        /**< Handle to the Reception Thread needed for Target Mode functionality. */                 
    //void *TargetModeFunctionReturnHandle;                             /**< Semaphore used to prevent StartTargetMode to return before all activities in the thread have ended. */ // Peer to Peer BugFix: Semaphore removed
    void *TargetModeRestartCleanupSync;                                 /**< Semaphore to sync Target Mode restart with exit of reception thread. */
    uint8_t TargetLoopItemsCreated;                                     /**< Tells the open/close functions whether to (re-)create the events or not. */
    uint8_t WaitingForReception;                                        /**< Flag that indicates that Tg. loop is active and waiting for data. */
    uint8_t IsStartTargetMode;                                          /**< Flag that indicates whether the StartTargetMode thread is still active. */

    /* User-to-Endpoint - related: */
    void *ReceptionUserToEndpointHandle;                                /**< Semaphore used to propagate reception information from the user thread to the Target Endpoint. */
    void *ReceptionEndpointToUserHandle;                                /**< Semaphore used to propagate reception information from the Target Endpoint to the user thread. */
    void *TransmissionUserToEndpointHandle;                             /**< Semaphore used to propagate transmission information from the user thread to the Target Endpoint. */
    /* Change Of WFSPE function - TransmissionEndpointToUserHandle
     *      This semaphore is always consumed in the phJal_SendDataToEndpoint function 
     *      which used the WFSPE function. The semaphore blocks there and will be produced
     *      in the phJal_DispatchTargetCommand function. So, all other places where this
     *      Semaphore is consumed are commented out.
     *
     *      When creating the semaphore, the initial value is also set to O in order to
     *      block the first time.
     */
    void *TransmissionEndpointToUserHandle;                             /**< Semaphore used to propagate transmission information from the Target Endpoint to the user thread. */
    void *ReceptionUserReadyForMetaChaining;                            /**< Semaphore used to propagate meta chanining information from the user thread to the Target Endpoint. */

    void *IoOverlappedStructure;                                        /**< Pointer to the Overlapped structure for detecting ring indicator events. */                    
    phJal_WaitForReceptionParams_t  ReceptionTimeoutParams;             /**< Structure containing all variables needed by the Event Callback.*/
    phJal_ReceptionHandlingParams_t ReceptionHandlingParams;            /**< Structure containing all data and states needed for Target Mode.*/

    /* Interrupt hardware interface configuration */
    uint8_t InterruptLine;                                              /**< The serial line that carries the INTR signal (e.g. RI). */

    /* Interrupt sensing function for the BFL: */
    pphcsBflAux_WaitEventCb_t WaitEventCb;
    uint8_t                   EventCallbackUsesInterrupt;

    /* *** Time measurement only: *** */
    #if (defined PHJALJOINER_TRACE)
        clock_t           StartTime;
    #endif /* PHJALJOINER_TRACE */
} phJal_sBFLUpperFrame_t;

/**
 * \ingroup grp_jal_common
 * \brief Function initialising the BFL Upper Frame.
 */
void phJal_BuildBflUpperFrame(phJal_sBFLUpperFrame_t     *bfl_frame,
                              uint8_t                    initiator__not_target,
                              phcsBflRegCtl_t            *rc_reg_ctl,
                              uint8_t                     do_initialise_chip);

/**
 * \ingroup grp_jal_common
 * \brief Function returning a pointer to the Meta Mode, the OpMode mode belongs to.
 */
const phHal_eOpModes_t *phJal_GetMetaMode(phHal_eOpModes_t mode);

/**
 * \ingroup grp_jal_common
 * \brief Function checking wether each OpMode contained in the array belongs to a Meta Mode.
 */
NFCSTATUS phJal_AreOpModesValid(phHal_eOpModes_t OpModes[]);

/**
 * \ingroup grp_jal_common
 * \brief Function checking wether an OpMode is contained in an array of OpModes or not.
 */
uint8_t phJal_IsOpModeInOpModesList(phHal_eOpModes_t OpModesList[], phHal_eOpModes_t OpModeToSearch);

/**
 * \ingroup grp_jal_common
 * \brief Function checking wether an OpMode is compatible with the Meta Mode currently in use. 
 */
uint8_t phJal_IsOpModeCompatibleWithCurrentMetaMode(phHal_sHwReference_t *psHwReference,
                                                    phHal_eOpModes_t     OpMode);

/**
 * \ingroup grp_jal_common
 * \brief Function checking wether the passed array of remote devices contain open sessions or not.
 */
uint8_t phJal_AreSessionsOpened(phHal_sRemoteDevInformation_t   psRemoteDevInfoList[],
                                uint8_t                         NbrOfRemoteDev);

/**
 * \ingroup grp_jal_common
 * \brief Function releasing a logical handle previously acquired.
 */
void phJal_ReleaseHandle(phJal_HandleEntry_t psEntries[], uint8_t Handle);

/**
 * \ingroup grp_jal_common
 * \brief Function acquiring a logical handle.
 * \retval Positive  All positive values returned by the function have to be casted to type uint8_t.
 * \retval Negative No handle is available. 
 */
int16_t phJal_AquireHandle(phJal_HandleEntry_t psEntries[]);

/**
 * \ingroup grp_jal_common
 * \brief Function acquiring a protocol from a list protocol entries.
 */
void* phJal_AquireProtocol(phJal_ProtocolEntry_t psEntries[]);

/**
 * \ingroup grp_jal_common
 * \brief Function releasing a protocol handle previously acquired.
 */
void phJal_ReleaseProtocol(phJal_ProtocolEntry_t psEntries[], void *psProtocol);

/**
 * \ingroup grp_jal_common
 * \brief Function registering a logical handle with a protocol pointer, so the
 * protocol can later on be accessed with the handle.
 */
void phJal_RegisterLogicalHandleForProtocol(phJal_ProtocolEntry_t   psEntries[], 
                                            void                    *psProtocol, 
                                            uint8_t                 handle);

/**
 * \ingroup grp_jal_common
 * \brief Function returning the protocol pointer registered with the logical handle.
 */
phJal_ProtocolEntry_t* phJal_GetProtocolEntryForLogicalHandle(phJal_ProtocolEntry_t  psEntries[], 
                                                              uint8_t                handle);

/**
 * \ingroup grp_jal_common
 * \brief Function registering a device ID with a protocol pointer.
 */
void phJal_RegisterDeviceIdForProtocol(phJal_ProtocolEntry_t psEntries[], 
                                       void                  *psProtocol,
                                       uint8_t               DeviceId);

/**
 * \ingroup grp_jal_common
 * \brief Function adding a Cache entry to an UID Cache.
 */
uint8_t phJal_AddUIDCacheEntry(phJal_sUIDCache_t *Cache,
                               uint8_t *Uid, 
                               uint8_t UidLength);

/**
 * \ingroup grp_jal_common
 * \brief Function retrieving Cache Entries from an UID Cache.
 * \note The second parameter contains the current position within the cache and must
 * not be changed during subsequent calls.
 */
phJal_sUIDCacheEntry_t* phJal_GetNextCacheEntry(phJal_sUIDCache_t  *Cache,
                                                uint8_t            *pCurrentPosition);

#if (defined PHJALJOINER_TRACE)
    double phJal_GetClockdiff(clock_t start);
    void   phJal_BFLFrameTrace(phHal_sHwReference_t *psHwReference, int8_t *msg);
#endif /* PHJALJOINER_TRACE */

#endif /* GUARD */
