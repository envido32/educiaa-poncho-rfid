/*
 *         Copyright (c), NXP Semiconductors
 *
 *                       (C)NXP Electronics N.V.2004
 *         All rights are reserved. Reproduction in whole or in part is
 *        prohibited without the written consent of the copyright owner.
 *    NXP reserves the right to make changes without notice at any time.
 *   NXP makes no warranty, expressed, implied or statutory, including but
 *   not limited to any implied warranty of merchantability or fitness for any
 *  particular purpose, or that the use will not infringe any third party patent,
 *   copyright or trademark. NXP must not be liable for any loss or damage
 *                            arising from its use.
 *
 */


/*!
 * \file phJalTargetCommands.c
 * \brief For function declarations see \ref phJalTargetCommands.h
 *
 *
 * Project: NFC MW / HAL
 *
 * $Date: Thu Sep 27 09:38:43 2007 $ 
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Aliases: SDK_HAL_2.2,SDK_HAL_2.2_071008,NFC_FRI_SDK_1.0_071031 $
 *
 */

#ifdef PHHAL_VERSION_ACQUISITION
    #define PHJALTARGETCOMMANDS_SOURCE_FILEREVISION "$Revision: 1.1 $"
    #define PHJALTARGETCOMMANDS_SOURCE_FILEALIASES  "$Aliases: SDK_HAL_2.2,SDK_HAL_2.2_071008,NFC_FRI_SDK_1.0_071031 $"
#else /* PHHAL_VERSION_ACQUISITION */

#include <memory.h>

#include <phcsBflHw1Reg.h>
#include <phcsBflOpCtl_Hw1Ordinals.h>
#include <phcsBflI3P3A.h>
#include <phcsBflNfc_TargetInt.h>
#include <phNfcStatus.h>
#include <phOsalNfc.h>

#include <phJalTargetCommands.h>
#include <phJalTargetModeFunctions.h>
#include <phJalDebugReportFunctions.h>
#include <phJalTransceiveFunctions.h>


/* Internal: Calc WTX time for user handling. */
uint32_t CalcUserCommHandlingWaitingTime(phJal_sBFLUpperFrame_t *bfl_frame, uint8_t multiplier);


phcsBfl_Status_t HandleAtrRequest(phJal_ReceptionHandlingParams_t   *psReceptionHandlingParams,
                                  phcsBflNfc_TargetEndpointParam_t  *psEndpointParam)
{
    phJal_sBFLUpperFrame_t *bfl_frame               = NULL;
    phcsBfl_Status_t        status                  = PH_ERR_BFL_SUCCESS;
    uint8_t                 use_auto_response       = 0;
    uint8_t                 passive_mode_enabled    = 0;
    
    /* The BFL returns everything after the DID in ATR req. We must subtract this
       offset from the buffer in order to get the entire ATR_REQ !! */
    const uint8_t           ATR_REQ_BFL_CONREQ_OFFSET   = 14;
    
    #ifndef NDEBUG
        uint8_t                 buffer_index;
    #endif

    if ((psReceptionHandlingParams != NULL) && (psEndpointParam != NULL))
    {
        phJal_Trace(psReceptionHandlingParams->psHwReference, "HandleAtrRequest");
        bfl_frame = (phJal_sBFLUpperFrame_t*)psReceptionHandlingParams->psHwReference->pBoardDriver;
      
        if(!(psEndpointParam->flags & PHCS_BFLNFC_TARGET_EP_FLAGS_ACTIVE))
        {
            /* passive mode */
            bfl_frame->RcRegCtlGetRegParam.address = PHCS_BFL_JREG_RXMODE;
            status = bfl_frame->RcRegCtl->GetRegister(&bfl_frame->RcRegCtlGetRegParam);
            passive_mode_enabled = 1;
            reportDbg0("endpoint ATR:\t Passive Mode detected\n");
            /* In passive mode 212 & 424 kbps check if the NFCID3 is equal to NFCID2. If not do not respond. */
            
            if ((status == PH_ERR_BFL_SUCCESS) && (psReceptionHandlingParams->pTgInfo != 0))
            {
                if(((bfl_frame->RcRegCtlGetRegParam.reg_data & PHCS_BFL_JMASK_SPEED) == PHCS_BFL_JBIT_212KBPS) || 
                    ((bfl_frame->RcRegCtlGetRegParam.reg_data & PHCS_BFL_JMASK_SPEED) == PHCS_BFL_JBIT_424KBPS))
                {
                    #ifndef NDEBUG
                        reportDbg0("pTgInfo->NFCID2t: ");
                        for (buffer_index = 0; buffer_index < 8; ++buffer_index) 
                        {
                            reportDbg1(" %02X", psReceptionHandlingParams->pTgInfo->NFCID2t[buffer_index]);
                        }
                        reportDbg0("\n");
                        reportDbg0("NfcTCp.mp_NfcIDi: ");
                        for (buffer_index = 0; buffer_index < 8; ++buffer_index) 
                        {
                            reportDbg1(" %02X", bfl_frame->NfcTCp.mp_NfcIDi[buffer_index]);
                        }
                        reportDbg0("\n");
                    #endif /*NDEBUG*/

                    if (memcmp(psReceptionHandlingParams->pTgInfo->NFCID2t, bfl_frame->NfcTCp.mp_NfcIDi, 8) != 0)
                    {
                        status = PH_ERR_BFL_TARGET_NFCID_MISMATCH;
                    }
                }
            }
        }

        if (status == PH_ERR_BFL_SUCCESS)
        {
            /* switch off Collision Avoidance */
            bfl_frame->RcRegCtlModifyParam.address = PHCS_BFL_JREG_TXAUTO;
            bfl_frame->RcRegCtlModifyParam.mask    = PHCS_BFL_JBIT_CAON;
            bfl_frame->RcRegCtlModifyParam.set     = 0;
            status = bfl_frame->RcRegCtl->ModifyRegister(&bfl_frame->RcRegCtlModifyParam);

            if (psReceptionHandlingParams->pTgInfo != NULL)
            {
                use_auto_response = 
                    (psReceptionHandlingParams->pTgInfo->Options.AutoConnectionResponse == 1)? 1:0;
            }
            else
            {
                use_auto_response = 0;
            }

            reportDbg1("endpoint ATR:\t Auto Response Mode used: %s\n", 
                (use_auto_response == 1)? "yes" : "no");

            if (status == PH_ERR_BFL_SUCCESS)
            {              
                if ((psReceptionHandlingParams->TargetConfiguration == TARGET_DESELECTED) &&
                    (passive_mode_enabled == 1))
                {
                    status = 
                        HandleAtrResponseForPassiveConnect(psReceptionHandlingParams, psEndpointParam);
                }
                else
                {
                    /* copy ATR_REQ into buffer provided by the user */
                    if (psReceptionHandlingParams->pConnectionReqBufLength != NULL)
                    {
                        if (*psReceptionHandlingParams->pConnectionReqBufLength >= psEndpointParam->req_data_length + ATR_REQ_BFL_CONREQ_OFFSET)
                        {
                            memcpy(psReceptionHandlingParams->pConnectionReq, psEndpointParam->p_req_data - ATR_REQ_BFL_CONREQ_OFFSET, 
                                psEndpointParam->req_data_length + ATR_REQ_BFL_CONREQ_OFFSET);
                            *psReceptionHandlingParams->pConnectionReqBufLength = 
                                (uint8_t)psEndpointParam->req_data_length + ATR_REQ_BFL_CONREQ_OFFSET;
                        }
                        else
                        {
                            status = PH_ERR_BFL_OTHER_ERROR;
                        }
                    }
                    else
                    {
                        status = PH_ERR_BFL_OTHER_ERROR;
                    }

                    if (use_auto_response == 1)
                    {
                        status = 
                            HandleAtrAutomaticResponse(psReceptionHandlingParams, psEndpointParam);            
                    }
                    else
                    {
                        status = 
                            HandleAtrManualResponse(psReceptionHandlingParams, psEndpointParam);
                    }

                    /* store complete buffer to be used later on by the Connect Function */
                    if ((status == PH_ERR_BFL_SUCCESS) && (passive_mode_enabled == 1))
                    {
                        reportDbg0("endpoint ATR:\t store complete ATR_RES\n");
                        memcpy(bfl_frame->AtrResponseBuffer, 
                            psEndpointParam->p_res_data, 
                            psEndpointParam->res_data_length);
                        bfl_frame->AtrResponseLength = psEndpointParam->res_data_length;
                    }
                }                               
            }
        }
    }
    else
    {
        status = PH_ERR_BFL_INVALID_PARAMETER;
    }
    
    return status;
}


phcsBfl_Status_t HandleAtrManualResponse(phJal_ReceptionHandlingParams_t  *psReceptionHandlingParams,
                                         phcsBflNfc_TargetEndpointParam_t *psEndpointParam)
{
    uint32_t                user_state = NO_STATE;
    NFCSTATUS               event_status = NFCSTATUS_SUCCESS;
    phcsBfl_Status_t        status = PH_ERR_BFL_SUCCESS;
    phJal_sBFLUpperFrame_t  *bfl_frame = 
        (phJal_sBFLUpperFrame_t*)psReceptionHandlingParams->psHwReference->pBoardDriver;

    phJal_Trace(psReceptionHandlingParams->psHwReference, "HandleAtrRequest");

    event_status = phOsalNfc_ConsumeSemaphore(bfl_frame->TransmissionUserToEndpointHandle);
    if (event_status == NFCSTATUS_SUCCESS)
    {
        /* signal StartTargetMode Function that data has been received */
        psReceptionHandlingParams->ReceptionInfoState = DATA_RECEIVED;
        
        /* Change of WFSPE() function in StartTargetMode - Addition of Handle: */
        event_status = phOsalNfc_ProduceSemaphore(bfl_frame->ReceptionInfoHandle_STM);
        //event_status = phOsalNfc_ProduceSemaphore(bfl_frame->ReceptionInfoHandle_RHT);
        if (event_status == NFCSTATUS_SUCCESS)
        {
            /* Wait for ATR_RES provided by user */
            /* Change Of WFSPE function - TransmissionUserToEndpointHandle */
            //event_status = 
            //    WaitForSemaphoreProtectedEvent(bfl_frame->TransmissionUserToEndpointHandle, 
            //        &psReceptionHandlingParams->TransmissionUserToEndpointState, 
            //        USER_READY | TERMINATE_RECEPTION, 
            //        &user_state,
            //        (bfl_frame->JalWaitingTime * psReceptionHandlingParams->WaitingTimeMultiplier)/2);

            event_status = phOsalNfc_ConsumeSemaphore(bfl_frame->TransmissionUserToEndpointHandle);
            user_state = psReceptionHandlingParams->TransmissionUserToEndpointState;
            psReceptionHandlingParams->TransmissionUserToEndpointState = NO_STATE;

            if (event_status == NFCSTATUS_SUCCESS)
            {
                /* Change Of WFSPE function - TransmissionEndpointToUserHandle */
                //event_status = 
                //    phOsalNfc_ConsumeSemaphore(bfl_frame->TransmissionEndpointToUserHandle);

                if (event_status == NFCSTATUS_SUCCESS)
                {
                    if(user_state == USER_READY)
                    {
                        psReceptionHandlingParams->TransmissionEndpointToUserState = USER_ATR_SENT;
                        if (psReceptionHandlingParams->UserDataLength <= PHJAL_TARGET_BUFFER_SIZE)
                        {
                            memcpy(psEndpointParam->p_res_data, psReceptionHandlingParams->pUserData, 
                                psReceptionHandlingParams->UserDataLength);
                            psEndpointParam->res_data_length = psReceptionHandlingParams->UserDataLength;                                      
                        }
                        else
                        {
                            status = PH_ERR_BFL_OTHER_ERROR;
                        }
                    }
                    else
                    {
                        status = PH_ERR_BFL_OTHER_ERROR;
                    }
                }
                else
                {
                    status = PH_ERR_BFL_OTHER_ERROR;
                }
            }
            else
            {
                status = PH_ERR_BFL_OTHER_ERROR;
            }
        }
        else
        {
            status = PH_ERR_BFL_OTHER_ERROR;
        }
    }
    else
    {
        status = PH_ERR_BFL_OTHER_ERROR;
    }
    return status;
}

phcsBfl_Status_t HandleAtrAutomaticResponse(phJal_ReceptionHandlingParams_t  *psReceptionHandlingParams,
                                            phcsBflNfc_TargetEndpointParam_t *psEndpointParam)
{
    uint8_t response_index = 0;

    phJal_Trace(psReceptionHandlingParams->psHwReference, "HandleAtrAutomaticResponse");

    psEndpointParam->p_res_data[response_index++] = 
        PHCS_BFLNFCCOMMON_ATR_BS_DS_8 | PHCS_BFLNFCCOMMON_ATR_BS_DS_16 | 
        PHCS_BFLNFCCOMMON_ATR_BS_DS_32;
    psEndpointParam->p_res_data[response_index++] = 
        PHCS_BFLNFCCOMMON_ATR_BR_DR_8 | PHCS_BFLNFCCOMMON_ATR_BR_DR_16 | 
        PHCS_BFLNFCCOMMON_ATR_BR_DR_32;
    psEndpointParam->p_res_data[response_index++] = ((phJal_sBFLUpperFrame_t*)(psReceptionHandlingParams->psHwReference->pBoardDriver))->CurrentWT;
    psEndpointParam->p_res_data[response_index++] = PHCS_BFLNFCCOMMON_ATR_PP_LR_128; 
                    
    if (psReceptionHandlingParams->pTgInfo->GeneralBytesLength > 0)
    {
        /* Set GT bit */
        psEndpointParam->p_res_data[response_index - 1] |= PHCS_BFLNFCCOMMON_ATR_PP_GI_PRES; 
        /* Apply GT */
        memcpy(&psEndpointParam->p_res_data[response_index],
            psReceptionHandlingParams->pTgInfo->GeneralBytes, 
            psReceptionHandlingParams->pTgInfo->GeneralBytesLength);
        response_index = response_index + psReceptionHandlingParams->pTgInfo->GeneralBytesLength;
    }
    psReceptionHandlingParams->ReceptionInfoState = AUTOMATIC_ATR_RES_SENT;                    
    psEndpointParam->res_data_length = response_index;
    return PH_ERR_BFL_SUCCESS;
}

phcsBfl_Status_t HandleAtrResponseForPassiveConnect(phJal_ReceptionHandlingParams_t  *psReceptionHandlingParams,
                                                    phcsBflNfc_TargetEndpointParam_t *psEndpointParam)
{
    phJal_sBFLUpperFrame_t  *bfl_frame = 
        (phJal_sBFLUpperFrame_t*)psReceptionHandlingParams->psHwReference->pBoardDriver;

    phJal_Trace(psReceptionHandlingParams->psHwReference, "HandleAtrResponseForPassiveConnect");

    /* In this state an initiator connects to the Target in Passive Mode */
    reportDbg0("endpoint ATR:\t sending stored ATR_RES\n");
    memcpy(&psEndpointParam->p_res_data, bfl_frame->AtrResponseBuffer, bfl_frame->AtrResponseLength);
    psEndpointParam->res_data_length = bfl_frame->AtrResponseLength;
    psReceptionHandlingParams->ReceptionInfoState = AUTOMATIC_ATR_RES_SENT; 
    return PH_ERR_BFL_SUCCESS; 
}

phcsBfl_Status_t HandleDslRequest(phJal_ReceptionHandlingParams_t   *psReceptionHandlingParams,
                                  phcsBflNfc_TargetEndpointParam_t  *psEndpointParam)
{
    phcsBfl_Status_t            command_status = PH_ERR_BFL_SUCCESS;
    phJal_sBFLUpperFrame_t      *bfl_frame = NULL;
    phcsBflI3P3A_HaltAParam_t   halt_param = {0};
    uint8_t                     halt_command_buffer[2] = {0};

    if ((psReceptionHandlingParams != NULL) && (psEndpointParam!= NULL))
    {
        phJal_Trace(psReceptionHandlingParams->psHwReference, "HandleDslRequest");

        bfl_frame = (phJal_sBFLUpperFrame_t*)psReceptionHandlingParams->psHwReference->pBoardDriver;

        bfl_frame->RcRegCtlModifyParam.address = PHCS_BFL_JREG_MODE;
        bfl_frame->RcRegCtlModifyParam.mask    = PHCS_BFL_JBIT_DETECTSYNC;
        bfl_frame->RcRegCtlModifyParam.set     = 0;
        command_status = bfl_frame->RcRegCtl->ModifyRegister(&bfl_frame->RcRegCtlModifyParam);
        
        if (command_status == PH_ERR_BFL_SUCCESS)
        {
            bfl_frame->RcRegCtlModifyParam.address = PHCS_BFL_JREG_TXMODE;
            bfl_frame->RcRegCtlModifyParam.mask    = PHCS_BFL_JMASK_SPEED | PHCS_BFL_JMASK_FRAMING;
            bfl_frame->RcRegCtlModifyParam.set     = 0;
            command_status = bfl_frame->RcRegCtl->ModifyRegister(&bfl_frame->RcRegCtlModifyParam);

            if (command_status == PH_ERR_BFL_SUCCESS)
            {           
                bfl_frame->RcRegCtlModifyParam.address = PHCS_BFL_JREG_RXMODE;
                bfl_frame->RcRegCtlModifyParam.mask    = PHCS_BFL_JMASK_SPEED | PHCS_BFL_JMASK_FRAMING | PHCS_BFL_JBIT_CRCEN;
                bfl_frame->RcRegCtlModifyParam.set     = 0;
                command_status = bfl_frame->RcRegCtl->ModifyRegister(&bfl_frame->RcRegCtlModifyParam);  
                psReceptionHandlingParams->EnableCRCAfterNextReception = 1;

                if (command_status == PH_ERR_BFL_SUCCESS)
                {
                    if(!(psEndpointParam->flags & PHCS_BFLNFC_TARGET_EP_FLAGS_ACTIVE))
                    {
                        halt_param.self = &bfl_frame->Iso14443_3A;
                        halt_param.buffer = halt_command_buffer;
                        bfl_frame->Iso14443_3A.HaltA(&halt_param);
                    } 
                    else
                    {                    
                        bfl_frame->RcRegCtlSetRegParam.address  = PHCS_BFL_JREG_COMMAND;
                        bfl_frame->RcRegCtlSetRegParam.reg_data = PHCS_BFL_JCMD_AUTOCOLL;
                        command_status = 
                            bfl_frame->RcRegCtl->SetRegister(&bfl_frame->RcRegCtlSetRegParam);
                        psReceptionHandlingParams->UserNotifiedAboutDeselectedState = 0;
                    }
                }
            }
        }
    }
    else
    {
        command_status = PH_ERR_BFL_INVALID_PARAMETER;
    }
    return command_status;

}

phcsBfl_Status_t HandlePslRequest(phJal_ReceptionHandlingParams_t *psReceptionHandlingParams,
                                  phcsBflNfc_TargetEndpointParam_t *psEndpointParam)
{
    phcsBfl_Status_t        command_status = PH_ERR_BFL_SUCCESS;
    phJal_sBFLUpperFrame_t  *bfl_frame = NULL;
    
    if ((psReceptionHandlingParams != NULL) && (psEndpointParam!= NULL))
    {
        phJal_Trace(psReceptionHandlingParams->psHwReference, "HandlePslRequest");

        bfl_frame = (phJal_sBFLUpperFrame_t*)psReceptionHandlingParams->psHwReference->pBoardDriver;

        bfl_frame->BrsValue = (psEndpointParam->p_req_data)[0];
        bfl_frame->FslValue = (psEndpointParam->p_req_data)[1];
        reportDbg2("endpoint PSLREQ: BRS=0x%02X, FSL=0x%02X\n", 
            bfl_frame->BrsValue, bfl_frame->FslValue);

        if(psReceptionHandlingParams->TargetRxSpeed != (bfl_frame->BrsValue >> 3))
        {
            reportDbg0("endpoint PSLREQ: changing RX speed\n");
            bfl_frame->RcOpCtlAttribParam.param_a = psReceptionHandlingParams->TargetTxSpeed;
            bfl_frame->RcOpCtlAttribParam.param_b = 
                (uint8_t)((bfl_frame->BrsValue & PHCS_BFLNFCCOMMON_PSL_BRS_DS_MASK)>>3);
            bfl_frame->RcOpCtlAttribParam.group_ordinal = PHCS_BFLOPCTL_GROUP_MODE;

            if(psEndpointParam->flags & PHCS_BFLNFC_TARGET_EP_FLAGS_ACTIVE)
            {
                bfl_frame->RcOpCtlAttribParam.attrtype_ordinal = PHCS_BFLOPCTL_ATTR_ACTIVE;
            }
            else
            {
                bfl_frame->RcOpCtlAttribParam.attrtype_ordinal = PHCS_BFLOPCTL_ATTR_PASSIVE;
            }

            command_status = bfl_frame->RcOpCtl.SetAttrib(&bfl_frame->RcOpCtlAttribParam);
        
            if (command_status == PH_ERR_BFL_SUCCESS)
            {
                psReceptionHandlingParams->TargetRxSpeed =
                    (uint8_t)((bfl_frame->BrsValue & PHCS_BFLNFCCOMMON_PSL_BRS_DS_MASK) >> 3);
                reportDbg1("endpoint PSLREQ: new RX speed 0x%02X\n", 
                    psReceptionHandlingParams->TargetRxSpeed);
            }
        }
    }
    else
    {
        command_status = PH_ERR_BFL_INVALID_PARAMETER;
    }

    return command_status;
}

phcsBfl_Status_t HandlePslResponse(phJal_ReceptionHandlingParams_t *psReceptionHandlingParams,
                                   phcsBflNfc_TargetEndpointParam_t *psEndpointParam)
{
    phcsBfl_Status_t        command_status = PH_ERR_BFL_SUCCESS;
    phJal_sBFLUpperFrame_t  *bfl_frame = NULL;
    
    if ((psReceptionHandlingParams != NULL) && (psEndpointParam!= NULL))
    {
        phJal_Trace(psReceptionHandlingParams->psHwReference, "HandlePslRequest");

        bfl_frame = (phJal_sBFLUpperFrame_t*)psReceptionHandlingParams->psHwReference->pBoardDriver;

        if(psReceptionHandlingParams->TargetTxSpeed != 
            (bfl_frame->BrsValue & PHCS_BFLNFCCOMMON_PSL_BRS_DR_MASK))
        {
            reportDbg0("endpoint PSLRES: changing TX speed\n");
            bfl_frame->RcOpCtlAttribParam.param_a = 
                (uint8_t)(bfl_frame->BrsValue & PHCS_BFLNFCCOMMON_PSL_BRS_DR_MASK);
            bfl_frame->RcOpCtlAttribParam.param_b = psReceptionHandlingParams->TargetRxSpeed;
            bfl_frame->RcOpCtlAttribParam.group_ordinal = PHCS_BFLOPCTL_GROUP_MODE;

            if(psEndpointParam->flags & PHCS_BFLNFC_TARGET_EP_FLAGS_ACTIVE)
            {
                bfl_frame->RcOpCtlAttribParam.attrtype_ordinal = PHCS_BFLOPCTL_ATTR_ACTIVE;
            }
            else
            {
                bfl_frame->RcOpCtlAttribParam.attrtype_ordinal = PHCS_BFLOPCTL_ATTR_PASSIVE;
            }

            command_status = bfl_frame->RcOpCtl.SetAttrib(&bfl_frame->RcOpCtlAttribParam);
        
            if (command_status == PH_ERR_BFL_SUCCESS)
            {
                psReceptionHandlingParams->TargetTxSpeed = 
                    (uint8_t)(bfl_frame->BrsValue & PHCS_BFLNFCCOMMON_PSL_BRS_DR_MASK);
                reportDbg1("endpoint PSLRES: new TX speed 0x%02X\n", psReceptionHandlingParams->TargetTxSpeed);
            }
        }
    }
    else
    {
        command_status = PH_ERR_BFL_INVALID_PARAMETER;
    }

    return command_status;
}

phcsBfl_Status_t HandleDepRequest(phJal_ReceptionHandlingParams_t   *psReceptionHandlingParams,
                                  phcsBflNfc_TargetEndpointParam_t  *psEndpointParam)
{
    phcsBfl_Status_t                command_status = PH_ERR_BFL_SUCCESS;
    phJal_sBFLUpperFrame_t          *bfl_frame = NULL;
    void                            *timeout_thread_handle = NULL;

    psEndpointParam->res_data_length = 0;

    if ((psReceptionHandlingParams != NULL) && (psEndpointParam!= NULL))
    {
        phJal_Trace(psReceptionHandlingParams->psHwReference, "HandleDepRequest");

        bfl_frame = (phJal_sBFLUpperFrame_t*)psReceptionHandlingParams->psHwReference->pBoardDriver;
        reportDbg1("endpoint DEP:\t endpoint state: %02X\n", psReceptionHandlingParams->EndpointDEPState);
        
        if (psReceptionHandlingParams->EndpointDEPMode == DEP_RECEPTION)
        {
            command_status = phJal_HandleDataTransferToUser(psReceptionHandlingParams, psEndpointParam);
        }
        
        if ((psReceptionHandlingParams->EndpointDEPMode == DEP_TRANSMISSION) &&
            (command_status == PH_ERR_BFL_SUCCESS))
        {
            /* no more data from initiator available - wait for user data */
            reportDbg0("endpoint DEP:\t start data transfer from user\n");
            command_status = 
                phJal_HandleDataTransferFromUser(psReceptionHandlingParams, psEndpointParam);
        }

        if (command_status == PHCS_BFLNFC_TARGET_RES_TOX)
        {
            command_status = PH_ERR_BFL_SUCCESS;
        }
    }
    else
    {
        command_status = PH_ERR_BFL_INVALID_PARAMETER;
    }

    if (timeout_thread_handle != NULL)
    {
        phOsalNfc_CloseHandle(timeout_thread_handle);
    }

    return command_status;
}

NFCSTATUS RetrieveUserEvents(phJal_WaitForUserEventParams_t  *user_params)
{
    phJal_ReceptionHandlingParams_t *reception_handling_params = NULL;
    phJal_sBFLUpperFrame_t          *bfl_frame = NULL;
    NFCSTATUS                       event_status = NFCSTATUS_SUCCESS;

    reception_handling_params = user_params->psReceptionHandlingParams;
    bfl_frame = (phJal_sBFLUpperFrame_t*)reception_handling_params->psHwReference->pBoardDriver;

    phJal_Trace(reception_handling_params->psHwReference, "RetrieveUserEvents");

    event_status = WaitForSemaphoreProtectedEvent(user_params->pEventHandle, 
        user_params->pEventState, 
        user_params->EventStateToWait,
        user_params->pDataState,
        user_params->WaitingTime);                                    
    return event_status;
}

void SetWTXResponse(phJal_ReceptionHandlingParams_t  *psReceptionHandlingParams, 
                       phcsBflNfc_TargetEndpointParam_t *psEndpointParam)
{
    phJal_Trace(psReceptionHandlingParams->psHwReference, "SetWTXResponse");

    /* GK:
     * The old implementation was:
     * psReceptionHandlingParams->WaitingTimeMultiplier += TOX_MULTIPLIER;
     * WTXM is now always 1!
     */
    reportDbg1("endpoint DEP:\t requesting WTX with multiplier %d\n", 
        psReceptionHandlingParams->WaitingTimeMultiplier);
    psEndpointParam->p_res_data[0] = psReceptionHandlingParams->WaitingTimeMultiplier;
    psEndpointParam->res_data_length = 1;
    psEndpointParam->action_type = PHCS_BFLNFC_TARGET_RES_TOX;
    psReceptionHandlingParams->WTXRequested = 1;
}



phcsBfl_Status_t HandleWTXForUserData(phJal_WaitForUserEventParams_t   *psUserParams,
                                      phcsBflNfc_TargetEndpointParam_t *psEndpointParam,
                                      uint32_t                          StatesToRequestWTX)
{
    phcsBfl_Status_t    bfl_status = PH_ERR_BFL_SUCCESS;
    NFCSTATUS           event_status = NFCSTATUS_SUCCESS;
    
    phJal_Trace(psUserParams->psReceptionHandlingParams->psHwReference, "HandleWTXForUserData");
    reportDbg0("endpoint DEP:\t WTX handling started\n");

    event_status = RetrieveUserEvents(psUserParams);    
    if (event_status == NFCSTATUS_SUCCESS)
    {
        if ((StatesToRequestWTX & (*(psUserParams->pDataState))) != 0)
        {
            SetWTXResponse(psUserParams->psReceptionHandlingParams, psEndpointParam);
            bfl_status = PHCS_BFLNFC_TARGET_RES_TOX;
        }
        else
        {
            bfl_status = PH_ERR_BFL_SUCCESS;
        }
    }
    else
    {
        bfl_status = PH_ERR_BFL_OTHER_ERROR;
    }
    return bfl_status;
}

void phJal_WaitForUserEvent(void *Parameters)
{
    phJal_ReceptionHandlingParams_t *reception_handling_params = NULL;
    phJal_sBFLUpperFrame_t          *bfl_frame = NULL;
    NFCSTATUS                       event_status = NFCSTATUS_SUCCESS;
    phJal_WaitForUserEventParams_t  *user_params = NULL;
    
    if (Parameters != NULL)
    {   
        user_params = (phJal_WaitForUserEventParams_t*) Parameters;
        reception_handling_params = user_params->psReceptionHandlingParams;
        bfl_frame = (phJal_sBFLUpperFrame_t*)
            (user_params->psReceptionHandlingParams->psHwReference->pBoardDriver);

        event_status = phOsalNfc_ConsumeSemaphore(bfl_frame->UserTimeoutHandle);

        if (event_status == NFCSTATUS_SUCCESS)
        {
            phJal_Trace(reception_handling_params->psHwReference, "phJal_WaitForUserEvent");
            reportDbg0("event thread:\t waiting for user event\n");

            event_status = 
                RetrieveUserEvents(user_params);
            reportDbg1("event thread:\t got event with status %02X\n", event_status);

            if (event_status == PHNFCSTVAL(CID_NFC_NONE, NFCSTATUS_SEMAPHORE_CONSUME_ERROR))
            {
                reportDbg0("event thread:\t timeout occurred\n");
                reception_handling_params->UserTimeoutState = TIMEOUT_OCCURED;
            }
            else if (event_status == NFCSTATUS_SUCCESS)
            {
                reportDbg1("event thread:\t got user event %02X\n", *(user_params->pDataState));
                reception_handling_params->UserTimeoutState = *(user_params->pDataState);
            }
            else
            {
                reportDbg1("event thread:\t error detected\n", *(user_params->pDataState));
                reception_handling_params->UserTimeoutState = TERMINATE_RECEPTION;
            }
            phOsalNfc_ProduceSemaphore(bfl_frame->UserTimeoutHandle);        
        }
    }
}


uint32_t CalcUserCommHandlingWaitingTime(phJal_sBFLUpperFrame_t *bfl_frame, uint8_t multiplier)
{
    /* Make time very short to do WTX also if user is very delayed due to high load. */
    const uint8_t wt_divisor        = 4;  /*WTX = timeout/4 !!*/
    const uint8_t min_wt            = 10;
    uint32_t wt = 
        (bfl_frame->JalWaitingTime * multiplier) / wt_divisor;
    return (wt < min_wt ? min_wt : wt);
}


phcsBfl_Status_t phJal_HandleDataTransferToUser(phJal_ReceptionHandlingParams_t   *psReceptionHandlingParams,
                                                phcsBflNfc_TargetEndpointParam_t  *psEndpointParam)
{
    NFCSTATUS                       synch_status = NFCSTATUS_SUCCESS;
    phcsBfl_Status_t                bfl_status = PH_ERR_BFL_SUCCESS;
    phJal_sBFLUpperFrame_t          *bfl_frame = NULL;
    void                            *timeout_thread_handle = NULL;
    phJal_WaitForUserEventParams_t  user_event_params = {0};
    phJal_WaitForUserEventParams_t  user_timeout_params = {0};
    phJal_WaitForUserEventParams_t  *current_user_params = NULL;
    uint32_t                        event_data_state = NO_STATE;
    uint32_t                        timeout_data_state = NO_STATE;
    uint8_t                         confirm_last_dep = 0;
    uint16_t                        received_data_length = 0;
    uint8_t                         *received_data = NULL;
    uint32_t                        state_to_request_wtx = TIMEOUT_OCCURED | USER_META_CHAINING;
    const uint8_t                   wtx_divisor_j2u = 8;
    const uint8_t                   min_waiting_time = 10;
    
    bfl_frame = (phJal_sBFLUpperFrame_t*)psReceptionHandlingParams->psHwReference->pBoardDriver;
    user_event_params.pEventState = &psReceptionHandlingParams->ReceptionUserToEndpointState;
    user_event_params.EventStateToWait = USER_READY | USER_BUFFER_TOO_SMALL | USER_META_CHAINING; 
    user_event_params.pEventHandle = bfl_frame->ReceptionUserToEndpointHandle;
    user_event_params.pDataState = &event_data_state;
    user_event_params.psReceptionHandlingParams = psReceptionHandlingParams;
    user_event_params.WaitingTime = CalcUserCommHandlingWaitingTime(bfl_frame, psReceptionHandlingParams->WaitingTimeMultiplier);

    user_timeout_params.pEventState = &bfl_frame->ReceptionHandlingParams.UserTimeoutState;
    user_timeout_params.EventStateToWait = USER_READY | MORE_DATA_AVAILABLE | 
        USER_META_CHAINING | TIMEOUT_OCCURED | USER_BUFFER_TOO_SMALL;
    user_timeout_params.pDataState = &timeout_data_state;
    user_timeout_params.pEventHandle = bfl_frame->UserTimeoutHandle;
    user_timeout_params.psReceptionHandlingParams = psReceptionHandlingParams;
    user_timeout_params.WaitingTime = WAIT_INFINITE;

    phJal_Trace(psReceptionHandlingParams->psHwReference, "phJal_HandleDataTransferToUser");

    if (psReceptionHandlingParams->MetaChainingActive == 1)
    {
        user_event_params.EventStateToWait = USER_READY;
        user_event_params.pEventHandle = bfl_frame->ReceptionUserReadyForMetaChaining;
        user_event_params.pEventState = &psReceptionHandlingParams->ReceptionUserReadyForMetaChaining;
    }
    
    if (psReceptionHandlingParams->WTXRequested == 1)
    {                           
        reportDbg0("endpoint DEP:\t start WTX handling\n");
        synch_status = phOsalNfc_CreateThread(&timeout_thread_handle, 
            phJal_WaitForUserEvent, &user_event_params);

        if (synch_status == NFCSTATUS_SUCCESS)
        {            
            bfl_status = 
                HandleWTXForUserData(&user_timeout_params, psEndpointParam, state_to_request_wtx);
            if (bfl_status == PH_ERR_BFL_SUCCESS)
            {                
                psReceptionHandlingParams->MetaChainingActive = 0;
                psReceptionHandlingParams->WaitingTimeMultiplier = 1;
                psReceptionHandlingParams->WTXRequested = 0;
                if (psReceptionHandlingParams->WaitingForMoreData == 1)
                {
                    reportDbg0("endpoint DEP:\t confirm DEP before WTX\n");
                    psEndpointParam->res_data_length = 0;
                    psEndpointParam->action_type = PHCS_BFLNFC_TARGET_RES_ACK;
                    confirm_last_dep = 1;
                }
            }
        }
        else
        {
            bfl_status = PH_ERR_BFL_OTHER_ERROR;
        }
    }

    if ((bfl_status == PH_ERR_BFL_SUCCESS) && (confirm_last_dep == 0))
    {
        if (psReceptionHandlingParams->EndpointDEPState == DEP_START)
        {
            synch_status = phOsalNfc_ConsumeSemaphore(bfl_frame->ReceptionUserToEndpointHandle);
            if (synch_status == NFCSTATUS_SUCCESS)
            {
                synch_status = phOsalNfc_CreateThread(&timeout_thread_handle, 
                    phJal_WaitForUserEvent, &user_event_params);
                bfl_status = (synch_status == NFCSTATUS_SUCCESS)? 
                    PH_ERR_BFL_SUCCESS : PH_ERR_BFL_OTHER_ERROR;
                current_user_params = &user_timeout_params;

                /* store data in TargetBuffer to ensure that no data is lost */
                memcpy(bfl_frame->TargetBuffer, psEndpointParam->p_req_data, psEndpointParam->req_data_length);
                bfl_frame->CurrentTargetBufferLength = psEndpointParam->req_data_length;
                received_data_length = psEndpointParam->req_data_length;
                received_data = bfl_frame->TargetBuffer;
            }

            if (psEndpointParam->nad == NULL)
            {
                psReceptionHandlingParams->NadPresent = 0;
            }
            else
            {
                psReceptionHandlingParams->NadPresent = 1;
                psReceptionHandlingParams->NadValue = *psEndpointParam->nad;
            }
            psReceptionHandlingParams->EndpointDEPState = 
                (psReceptionHandlingParams->WaitingForMoreData == 0)? DEP_DONE : DEP_CONTINUE;
        }
        else if (psReceptionHandlingParams->EndpointDEPState == DEP_MI_CONTINUE)
        {
            /* confirm last DEP to receive further data */
            reportDbg0("endpoint DEP:\t confirm last DEP\n");
            psEndpointParam->res_data_length = 0;
            psEndpointParam->action_type = PHCS_BFLNFC_TARGET_RES_ACK;
            confirm_last_dep = 1;
            psReceptionHandlingParams->EndpointDEPState = DEP_CONTINUE;
            current_user_params = &user_event_params;
            received_data_length = psEndpointParam->req_data_length;
            received_data = psEndpointParam->p_req_data;
            /* in this state no timeout is used */
            user_event_params.WaitingTime = WAIT_INFINITE;
            
        }
        else if (psReceptionHandlingParams->EndpointDEPState == DEP_DONE)
        {
            /* switch to user data transmission */
            psReceptionHandlingParams->EndpointDEPMode = DEP_TRANSMISSION;
        }
        else if (psReceptionHandlingParams->EndpointDEPState == DEP_CONTINUE)
        {
            psReceptionHandlingParams->MetaChainingActive = 0;
            reportDbg0("endpoint DEP:\t consume rUtoE event\n");
            synch_status = phOsalNfc_ConsumeSemaphore(bfl_frame->ReceptionUserToEndpointHandle);
            bfl_status = (synch_status == NFCSTATUS_SUCCESS)? 
                PH_ERR_BFL_SUCCESS : PH_ERR_BFL_OTHER_ERROR;
            current_user_params = &user_event_params;
            received_data_length = psEndpointParam->req_data_length;
            received_data = psEndpointParam->p_req_data;
            /* in this state no timeout is used */
            user_event_params.WaitingTime = WAIT_INFINITE;
        }
        else
        {
            bfl_status = PH_ERR_BFL_OTHER_ERROR;
        }
    }

    if ((bfl_status == PH_ERR_BFL_SUCCESS) && 
        (confirm_last_dep == 0) &&
        (psReceptionHandlingParams->EndpointDEPMode == DEP_RECEPTION))
    {   
        /* WaitForSemaphoreProtectedEvent Function Change -  ReceptionEndpointToUserHandle*/
        // synch_status = phOsalNfc_ConsumeSemaphore(bfl_frame->ReceptionEndpointToUserHandle);
        synch_status = NFCSTATUS_SUCCESS;

        if (synch_status == NFCSTATUS_SUCCESS)
        {        
            if (psReceptionHandlingParams->WaitingForMoreData == 1)
            { 
                psReceptionHandlingParams->ReceptionEndpointToUserState = MORE_DATA_AVAILABLE;
            }
            else
            {
                psReceptionHandlingParams->ReceptionEndpointToUserState = DATA_RECEIVED;            
                /* no additional data is to be received thus consume transmission event to */
                /* be synchronised with send function */
                psReceptionHandlingParams->EndpointDEPMode = DEP_TRANSMISSION;
                reportDbg0("endpoint DEP:\t consume UtoE event\n");
                synch_status = phOsalNfc_ConsumeSemaphore(bfl_frame->TransmissionUserToEndpointHandle);
            }
                
            psReceptionHandlingParams->pUserData = received_data;
            psReceptionHandlingParams->UserDataLength = received_data_length;
         
            reportDbg1("endpoint DEP:\t %d data bytes passing to user\n", 
                psEndpointParam->req_data_length);
            reportDbg0("endpoint DEP:\t producing EtoU event\n");
            phOsalNfc_ProduceSemaphore(bfl_frame->ReceptionEndpointToUserHandle);                       
        }
        else
        {
            bfl_status = PH_ERR_BFL_OTHER_ERROR;
        }
        
        if (bfl_status == PH_ERR_BFL_SUCCESS)
        {         
            bfl_status = 
                HandleWTXForUserData(current_user_params, psEndpointParam, state_to_request_wtx);

            if (bfl_status == PH_ERR_BFL_SUCCESS)
            {
                if (*current_user_params->pDataState == USER_BUFFER_TOO_SMALL)
                {
                    reportDbg0("endpoint DEP:\t Error -> User buffer too small\n");
                    bfl_status = PH_ERR_BFL_OTHER_ERROR; 
                }
                else
                {
                    if (psReceptionHandlingParams->WaitingForMoreData == 0)
                    {
                        /* no more data to be received thus setting flag to start transmission */
                        /* to initiator */
                        reportDbg0("endpoint DEP:\t starting transmission of user data\n");
                        psReceptionHandlingParams->EndpointDEPMode = DEP_TRANSMISSION;
                        psReceptionHandlingParams->EndpointDEPState = DEP_START;                 
                    }
                    else
                    {
                        psReceptionHandlingParams->EndpointDEPState = DEP_CONTINUE;
                        psEndpointParam->action_type = PHCS_BFLNFC_TARGET_RES_ACK;
                    }
                }
            }
            else if (bfl_status == PHCS_BFLNFC_TARGET_RES_TOX)
            {
                if (*(current_user_params->pDataState) == USER_META_CHAINING)
                {
                    reportDbg0("endpoint DEP:\t reception meta chaining active\n");
                    psReceptionHandlingParams->EndpointDEPState = DEP_CONTINUE;
                    /* wait for signal from Receive function to continue chaining */
                }
                else
                {
                    reportDbg0("endpoint DEP:\t requesting WTX\n");
                }
            }
        }
    }
    if (timeout_thread_handle != NULL)
    {
        phOsalNfc_CloseHandle(timeout_thread_handle);
    }

    if ((bfl_status != PH_ERR_BFL_SUCCESS) && (bfl_status != PHCS_BFLNFC_TARGET_RES_TOX))
    {
        reportDbg0("endpoint DEP:\t producing UtoE event\n");
        phOsalNfc_ProduceSemaphore(bfl_frame->ReceptionUserToEndpointHandle);
    }

    reportDbg1("endpoint DEP:\t returning with action type %02X\n", psEndpointParam->action_type);
    return bfl_status;
}


phcsBfl_Status_t phJal_HandleDataTransferFromUser(phJal_ReceptionHandlingParams_t   *psReceptionHandlingParams,
                                                  phcsBflNfc_TargetEndpointParam_t  *psEndpointParam)
{
    NFCSTATUS                           synch_status = NFCSTATUS_SUCCESS;
    uint32_t                            event_data_state = NO_STATE;
    uint32_t                            timeout_data_state = NO_STATE;
    uint32_t                            data_state_to_evaluate = NO_STATE;
    phJal_sBFLUpperFrame_t              *bfl_frame = NULL;
    phJal_WaitForUserEventParams_t      user_event_params = {0};
    phJal_WaitForUserEventParams_t      user_timeout_params = {0};
    phcsBfl_Status_t                    bfl_status = PH_ERR_BFL_SUCCESS;
    void                                *timeout_thread_handle = NULL;
    uint32_t                            state_to_request_wtx = TIMEOUT_OCCURED;

    phJal_Trace(psReceptionHandlingParams->psHwReference, "phJal_HandleDataTransferFromUser");
    
    bfl_frame = (phJal_sBFLUpperFrame_t*)psReceptionHandlingParams->psHwReference->pBoardDriver;
    
    user_event_params.pEventState = &psReceptionHandlingParams->TransmissionUserToEndpointState;
    user_event_params.pEventHandle = bfl_frame->TransmissionUserToEndpointHandle;
    user_event_params.psReceptionHandlingParams = psReceptionHandlingParams;
    user_event_params.EventStateToWait = USER_READY | MORE_DATA_AVAILABLE | USER_META_CHAINING;
    user_event_params.WaitingTime = CalcUserCommHandlingWaitingTime(bfl_frame, psReceptionHandlingParams->WaitingTimeMultiplier);
    user_event_params.pDataState = &event_data_state;

    user_timeout_params.pEventState = &bfl_frame->ReceptionHandlingParams.UserTimeoutState;
    user_timeout_params.pEventHandle = bfl_frame->UserTimeoutHandle;
    user_timeout_params.psReceptionHandlingParams = psReceptionHandlingParams;
    user_timeout_params.EventStateToWait = USER_READY | MORE_DATA_AVAILABLE | 
        USER_META_CHAINING | TIMEOUT_OCCURED;
    user_timeout_params.WaitingTime = WAIT_INFINITE;
    user_timeout_params.pDataState = &timeout_data_state;

    bfl_frame = (phJal_sBFLUpperFrame_t*)psReceptionHandlingParams->psHwReference->pBoardDriver;    
    
    if (psReceptionHandlingParams->WTXRequested == 1)            
    {
        reportDbg0("endpoint DEP:\t start WTX handling\n");
        synch_status = phOsalNfc_CreateThread(&timeout_thread_handle, 
            phJal_WaitForUserEvent, &user_event_params);
        
        if (synch_status == NFCSTATUS_SUCCESS)
        {            
            bfl_status = 
                HandleWTXForUserData(&user_timeout_params, psEndpointParam, state_to_request_wtx);
            if (bfl_status == PH_ERR_BFL_SUCCESS)
            {
                psReceptionHandlingParams->EndpointDEPState = DEP_CONTINUE;
			    /* reset WTX multiplier to standard value */
                psReceptionHandlingParams->WaitingTimeMultiplier = 1;
                psReceptionHandlingParams->WTXRequested = 0;
                data_state_to_evaluate = *(user_timeout_params.pDataState);
            }
        }
        else
        {
            bfl_status = PH_ERR_BFL_OTHER_ERROR;
        }
    }
    else
    {
        if (bfl_status == PH_ERR_BFL_SUCCESS)
        {
            if ((psReceptionHandlingParams->EndpointDEPState == DEP_START) ||
                (psReceptionHandlingParams->MetaChainingActive == 1))
            {
                synch_status = phOsalNfc_CreateThread(&timeout_thread_handle,
                    phJal_WaitForUserEvent, &user_event_params);
                bfl_status = (synch_status == NFCSTATUS_SUCCESS)? 
                    PH_ERR_BFL_SUCCESS : PH_ERR_BFL_OTHER_ERROR;
                
                if (bfl_status == PH_ERR_BFL_SUCCESS)
                {
                    bfl_status = 
                        HandleWTXForUserData(&user_timeout_params, psEndpointParam, state_to_request_wtx);
                    data_state_to_evaluate = *(user_timeout_params.pDataState);
                    psReceptionHandlingParams->MetaChainingActive = 0;
                }
            }
            else if (psReceptionHandlingParams->EndpointDEPState == DEP_CONTINUE)
            {
                user_event_params.WaitingTime = WAIT_INFINITE;
                HandleWTXForUserData(&user_event_params, psEndpointParam, state_to_request_wtx);
                data_state_to_evaluate = *(user_event_params.pDataState);
                bfl_status = (synch_status == NFCSTATUS_SUCCESS)? 
                    PH_ERR_BFL_SUCCESS : PH_ERR_BFL_OTHER_ERROR;
            }
            else
            {
                bfl_status = PH_ERR_BFL_OTHER_ERROR;
            }
        }
    }

    if (bfl_status == PH_ERR_BFL_SUCCESS)
    {                
		if (data_state_to_evaluate == MORE_DATA_AVAILABLE)            
		{
             reportDbg0("endpoint DEP:\t more user data available thus activate chaining\n");
			psEndpointParam->flags |= PHCS_BFLNFC_TARGET_EP_FLAGS_MI_PRES;
            psReceptionHandlingParams->EndpointDEPState = DEP_CONTINUE;
		}
        else if (data_state_to_evaluate == USER_META_CHAINING)
        {
            reportDbg0("endpoint DEP:\t user meta chaining active\n");
            psEndpointParam->flags |= PHCS_BFLNFC_TARGET_EP_FLAGS_MI_PRES;
            psReceptionHandlingParams->EndpointDEPState = DEP_CONTINUE;
            psReceptionHandlingParams->MetaChainingActive = 1;
        }
        else
        {
            reportDbg0("endpoint DEP:\t DEP done\n");
            psEndpointParam->flags &= ~PHCS_BFLNFC_TARGET_EP_FLAGS_MI_PRES;
            psReceptionHandlingParams->EndpointDEPState = DEP_DONE;
        }          
        
        /* Change Of WFSPE function - TransmissionEndpointToUserHandle */
        //synch_status = 
        //    phOsalNfc_ConsumeSemaphore(bfl_frame->TransmissionEndpointToUserHandle);
        if (synch_status == NFCSTATUS_SUCCESS)
        {
            reportDbg1("endpoint DEP:\t copy %d user data bytes to target buffer\n", 
			    psReceptionHandlingParams->UserDataLength);
            memcpy(psEndpointParam->p_res_data, 
                psReceptionHandlingParams->pUserData, 
                psReceptionHandlingParams->UserDataLength);
            psEndpointParam->res_data_length = psReceptionHandlingParams->UserDataLength;
            
            psEndpointParam->action_type = PHCS_BFLNFC_TARGET_RES_INF;
            psReceptionHandlingParams->TransmissionEndpointToUserState = USER_DATA_SENT;
        }
        else
        {
            bfl_status = PH_ERR_BFL_OTHER_ERROR;
        }
    }

    if (timeout_thread_handle != NULL)
    {
        phOsalNfc_CloseHandle(timeout_thread_handle);
    }

    return bfl_status;
}

phcsBfl_Status_t HandleRlsRequest(phJal_ReceptionHandlingParams_t   *psReceptionHandlingParams,
                                  phcsBflNfc_TargetEndpointParam_t  *psEndpointParam)
{   
    NFCSTATUS                   synch_status = NFCSTATUS_SUCCESS;
    phJal_sBFLUpperFrame_t      *bfl_frame = NULL;

    phJal_Trace(psReceptionHandlingParams->psHwReference, "HandleRlsRequest");

    psEndpointParam = psEndpointParam; /* RM unref. param. */

    bfl_frame = (phJal_sBFLUpperFrame_t*)psReceptionHandlingParams->psHwReference->pBoardDriver;

    synch_status = phOsalNfc_ConsumeSemaphore(bfl_frame->ReceptionUserToEndpointHandle);

    if (synch_status == NFCSTATUS_SUCCESS)
    {
        synch_status = phOsalNfc_ConsumeSemaphore(bfl_frame->TerminateReceptionHandle);
        if (synch_status == NFCSTATUS_SUCCESS)
        {
            bfl_frame->ReceptionHandlingParams.TerminationState = TERMINATE_RECEPTION;
		    reportDbg0("endpoint RLS:\t signal reception termination\n");
            synch_status = phOsalNfc_ProduceSemaphore(bfl_frame->TerminateReceptionHandle);

            if (synch_status == NFCSTATUS_SUCCESS)
            {    
                psReceptionHandlingParams->ReceptionUserToEndpointState = TARGET_RELEASED_NOW;
                synch_status = phOsalNfc_ProduceSemaphore(bfl_frame->ReceptionUserToEndpointHandle);
            }
            else
            {
                psReceptionHandlingParams->ReceptionUserToEndpointState = TERMINATE_RECEPTION;
                synch_status = phOsalNfc_ProduceSemaphore(bfl_frame->ReceptionUserToEndpointHandle);
            }
        }
    }
    
    return((synch_status == NFCSTATUS_SUCCESS)? PH_ERR_BFL_SUCCESS : PH_ERR_BFL_OTHER_ERROR);
}



#endif