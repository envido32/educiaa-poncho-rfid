/* /////////////////////////////////////////////////////////////////////////////////////////////////
//                     Copyright (c) Philips Semiconductors
//
//                       (C)PHILIPS Electronics N.V.2004
//         All rights are reserved. Reproduction in whole or in part is
//        prohibited without the written consent of the copyright owner.
//    Philips reserves the right to make changes without notice at any time.
//   Philips makes no warranty, expressed, implied or statutory, including but
//   not limited to any implied warranty of merchantability or fitness for any
//  particular purpose, or that the use will not infringe any third party patent,
//   copyright or trademark. Philips must not be liable for any loss or damage
//                            arising from its use.
///////////////////////////////////////////////////////////////////////////////////////////////// */


/*! \file ExampleProject.cpp
 *
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Date: Thu Sep 27 09:38:46 2007 $
 *
 * Comment:
 *  Example code for handling of BFL.
 *
 */

/* Header for this file */
#include "ExampleProject.h"
#include "ExampleGlobals.h"

/* Names for the PN51x registers    */
#include <phcsBflHw1Reg.h>

#include <phcsBflBalWrapper.hpp>
#include <phcsBflRegCtlWrapper.hpp>

///* Access to the OpCtl component */
#include <phcsBflOpCtlWrapper.hpp>
#include <phcsBflOpCtl_Hw1Ordinals.h>

///* Access to the RcIo component */
#include <phcsBflIoWrapper.hpp>

/* Provides utility functions       */
#include "ExampleUtils.h"

/* Headers for the Operating Modes  */
/* OpMode = 1 */
#include "MifareReader.h"
/* OpMode = 2 */
#include "FelicaReader.h"
/* OpMode = 3 */
#include "NfcInitiator.h"
/* OpMode = 4 */
#include "NfcTarget.h"
/* OpMode = 5 */
#include "Iso14443_4_Reader.h"




int main(int argc, char *argv[])
{
    using namespace phcs_BFL;
    
    phcsBfl_Status_t    status;
	int32_t 			    opMode = 0;	
	void		       *comHandle;
	int32_t				    res = 0;
	uint8_t	    settings = 0;
    uint8_t	    isPassive = 0x00, 
					    opSpeed = 0x01;
	int32_t				    i, state = 0;
	int8_t			    character = '\0';
	
	/* Default values for the interfaces */
#ifdef WIN32
    int8_t 			mifc[] = "COM3:";
#else
    int8_t 			mifc[] = "/dev/ttyS0";
#endif
    int8_t *ifc = 	mifc;	

	/* 
	 * Default baudrate (on the interface between host and PN51x)
	 * In Windows these values are the same as the values with the prefix CBR_ 
	 */
	uint32_t   mBaudRate = 9600;
	
    /*
     * Build up the BFL stack (C++ option)
	 * The lower layer is different between Windows and Linux
	 */
#ifdef WIN32    
	phcsBflBal_Wrapper<phcsBflBal_Hw1SerWinInitParams_t>   bal;
#else    
	phcsBflBal_Wrapper<phcsBflBal_Hw1SerLinInitParams_t>     bal;
#endif
	
    /* RegCtl */
    phcsBflRegCtl_Wrapper<phcsBflRegCtl_SerHw1Params_t>  rc_reg_ctl;
    /* OpCtl */
    phcsBflOpCtl_Wrapper<phcsBflOpCtl_Hw1Params_t>       rc_op_ctl;

#ifdef WIN32
    phcsBflBal_Hw1SerWinInitParams_t   bal_params;
#else
    phcsBflBal_Hw1SerLinInitParams_t   bal_params;
#endif

    /* Parameter structure definitions for bal */
    phcsBflBal_HostBaudParam_t         hostbaud;
    phcsBflBal_ClosePortParam_t        closeport;

    /* parameter structure definitions for register control */
    phcsBflRegCtl_SetRegParam_t     srp;
    phcsBflRegCtl_GetRegParam_t     grp;

	/*
	 * Some checks if the platform fulfils the requirements (very conservative)
	 */
	status = CheckPlatform();
	if (status != PH_ERR_BFL_SUCCESS)
	{
		printf("Not all platform assumptions are fulfilled. You might experience problems.\n");
	}


	for (i = 1; (i < argc) || (state == 1); i++)
	{
		if (state == 1)
		{
			state = 0;
			switch (character)
			{
			case 'o':
				opMode = (uint8_t) strtol(argv[i], NULL, 10);
				printf("[I] Using Operating Mode %d\n", opMode);
				break;
			case 'i':
				ifc = argv[i];
				printf("[I] Using Interface %s.\n", ifc);
				break;
			case 'z':
				settings = (uint8_t) strtol(argv[i], NULL, 16);
				printf("[I] Using generic hex setting %02Xh.\n", settings);
				break;
			case 'm':
				if (argv[i][0] == 'p')
				{
					// Use passive mode, only relevant for NFC. Default is active mode
					isPassive = 0x01;
				} 
				break;
			case 's':
				opSpeed = (uint8_t) strtol(argv[i], NULL, 10);
				break;
			case 'h':
			case '?':
				printf("%s", gHelpString);
				return PH_ERR_BFL_SUCCESS;
				break;
            case 'd':
                mBaudRate = strtol(argv[i], NULL, 10);
				break;
			default:
				printf("[E] The switch \"%c\" is not supported.\n", character);
				break;
			}
			continue;
		}

		// Check if this is a switch. If yes, next string will be the value for the corresponding switch
		if ( (argv[i][0] == '-') || (argv[i][0] == '/') )
		{
			state = 1;
			character = argv[i][1];
		}
	}

    /* In order to use the components they have to be initialized.
	 * This is done by calling the Initialise functions 
	 */
    bal_params.com_handle = 0;
    bal_params.com_port   = ifc;
#ifdef WIN32
    bal.Initialise(phcsBflBal_Hw1SerWinInit, (void*)(&bal_params)); 
#else
	bal.Initialise(phcsBflBal_Hw1SerLinInit, (void*)(&bal_params)); 
#endif
    comHandle = bal_params.com_handle;
    
    rc_reg_ctl.Initialise(phcsBflRegCtl_SerHw1Init, &bal);
    rc_op_ctl.Initialise(phcsBflOpCtl_Hw1Init, &rc_reg_ctl); 


	/* Now it's possible to communicate on register level */
    /* 
     * Get the Version Number - just to verify if the connection is okay
	 */
    /* Get the Version Number */
    grp.address  = PHCS_BFL_JREG_VERSION;
    status = rc_reg_ctl.GetRegister(&grp); 
	if (status != PH_ERR_BFL_SUCCESS)
    {
        printf("[E] Unable to connect to PN51x. Terminating application.\n");
		return -1;
    }
	printf("[I] Hello, this is PN51x - hardware version %x.%x\n", 
					(grp.reg_data >> 4) & 0x0F, grp.reg_data & 0x0F);

    srp.address  = PHCS_BFL_JREG_COMMAND;
    srp.reg_data = PHCS_BFL_JCMD_SOFTRESET;
	status = rc_reg_ctl.SetRegister(&srp); 
	if (status != PH_ERR_BFL_SUCCESS)
		printf("[E] Not able to reset the PN51x.\n");

	/*
	 * Check if the silicon version fits to this software
	 * Uncheck it if is needed.
	 */
	/*
	if (CheckSiliconVersion(&rc_reg_ctl, 0x28))
	{
		printf("[E] Invalid version of the silicon. Aborting.\n");
		return -1;
	}
	*/

	#ifdef WIN32
	/*
     * Adjust the baudrate
	 * For debug purposes it may be better to switch off these routines, since
	 * otherwise the PN51x has to be always reset, when the baudrate is not 
	 * set back to the default value at the end.
	 *
     * (1) Set the appropriate register */
    ChangeJoinerBaudRate(&rc_reg_ctl, mBaudRate);
    /* (2) Set the UART on the host */
    hostbaud.baudrate = mBaudRate;
    bal.ChangeHostBaud(&hostbaud);
    #endif
	
    /*
     * Switch to the default/specified operating mode
	 */
	switch (opMode)
	{
	case 1:
    	/* ISO14443_3A activation and Mifare reader commands */
		printf("[I] Acting as Mifare Reader.\n");
    	res = ActivateMifareReader(comHandle, settings); 
		break;
	case 2:
		/* FeliCa polling */
		printf("[I] Acting as FeliCa Reader.\n");
		res = ActivateFelicaReader(comHandle); 
		break;
	case 3:
		printf("[I] Acting as NFC Initiator.\n");
		res = ActivateNfcInitiator(comHandle, isPassive, opSpeed);
		break;
	case 4:
		/* Activate SDD of PN51x and automatic send receive */
		printf("[I] Acting as NFC Target.\n");
		res = ActivateNfcTarget(comHandle, settings);
		break;
	case 5:
    	/* Act as ISO14443-4 reader */
		printf("[I] Acting as ISO14443-4 Reader.\n");
    	res = ActivateIso14443_4_Reader(comHandle, settings);
		break;
	case 6:
		/* Some other examples which are valid for any operating mode */
		printf("[I] Operating Mode independent examples.\n");
		#ifdef WIN32
			res = ExampleUtils(comHandle, &rc_op_ctl, &rc_reg_ctl, &bal, settings);
		#else
			printf("[E] Sorry, this mode is not yet supported in Linux.\n");
			res = -1;
		#endif
		break;
	default:
		/* Invalid */
		printf("[E] Invalid operating mode\n");
		res = -1;
        break;
	}
    
    /*
     * Reset chip (Basically in order to reset the baudrate to 9600 Baud and to
	 * reject the current register values
	 */
    srp.address  = PHCS_BFL_JREG_COMMAND;
    srp.reg_data = PHCS_BFL_JCMD_SOFTRESET;
	rc_reg_ctl.SetRegister(&srp); 

	/* 
	 * Release the COM port handle
	 */
#ifdef WIN32
	bal.ClosePort(&closeport);
#endif

    /*
	 * Returns zero if the procedure was successful. Otherwise a value
	 * unequal zero is returned 
	 */
	return res;
}
