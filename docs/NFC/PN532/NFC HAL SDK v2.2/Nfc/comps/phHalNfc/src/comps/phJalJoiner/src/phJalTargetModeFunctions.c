/*
 *         Copyright (c), NXP Semiconductors
 *
 *                       (C)NXP Electronics N.V.2004
 *         All rights are reserved. Reproduction in whole or in part is
 *        prohibited without the written consent of the copyright owner.
 *    NXP reserves the right to make changes without notice at any time.
 *   NXP makes no warranty, expressed, implied or statutory, including but
 *   not limited to any implied warranty of merchantability or fitness for any
 *  particular purpose, or that the use will not infringe any third party patent,
 *   copyright or trademark. NXP must not be liable for any loss or damage
 *                            arising from its use.
 *
 */


/*!
 * \file phJalTargetModeFunctions.c
 * \brief JAL functions for Target Mode handling
 *
 *
 * Project: NFC MW / HAL
 *
 * $Date: Thu Sep 27 09:38:43 2007 $
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Aliases: SDK_HAL_2.2,SDK_HAL_2.2_071008,NFC_FRI_SDK_1.0_071031 $
 *
 */

#ifdef PHHAL_VERSION_ACQUISITION
    #define PHJALTARGETMODEFUNCTIONS_SOURCE_FILEREVISION "$Revision: 1.1 $"
    #define PHJALTARGETMODEFUNCTIONS_SOURCE_FILEALIASES  "$Aliases: SDK_HAL_2.2,SDK_HAL_2.2_071008,NFC_FRI_SDK_1.0_071031 $"
#else /* PHHAL_VERSION_ACQUISITION */

/* Uncomment for viewing the actual incoming data on FIFO level: */
/* #define PHCS_NFC_T_BUFFER_DEBUG */

#ifdef PHCS_NFC_T_BUFFER_DEBUG
    #include <stdio.h>
#endif



#include <string.h>

#include <phcsBflIo.h>
#include <phcsBflHw1Reg.h>
#include <phcsBflI3P3A.h>
#include <phcsBflOpCtl_Hw1Ordinals.h>
#include <phcsBflNfcCommon.h>

#include <phNfcStatus.h>
#include <phOsalNfc.h>
#include <phNfcHalTypes.h>
#include <phDalNfc.h>

#include <phJalTargetModeFunctions.h>
#include <phJalTargetCommands.h>
#include <phJalJoiner.h>
#include <phJalJoinerBflFrame.h>
#include <phJalDebugReportFunctions.h>

#define NO_TARGET           0x00
#define NFC_TARGET          0x01
#define FELICA_TARGET       0x02
#define ISO144434_TARGET    0x03


void phJal_ReceptionHandling(void *Parameters)
{
    phcsBfl_Status_t                bfl_status = PH_ERR_BFL_SUCCESS;
    NFCSTATUS                       event_status = NFCSTATUS_SUCCESS;
    NFCSTATUS                       dispatch_status = NFCSTATUS_SUCCESS;
    phJal_ReceptionHandlingParams_t *reception_handling_params = NULL;
    phJal_sBFLUpperFrame_t          *bfl_frame = NULL;
    phcsBflIo_ReceiveParam_t        receive_param = {0};
    uint8_t                         target_error_occurred = 0;
    uint8_t                         configure_target = 0;
    phcsBflI3P3A_HaltAParam_t       halt_param = {0};

    #ifndef NDEBUG
        uint16_t                    buffer_index       = 0;
    #endif


    if (Parameters != NULL)
    {
        reception_handling_params = (phJal_ReceptionHandlingParams_t*) Parameters;

        bfl_frame = (phJal_sBFLUpperFrame_t*)reception_handling_params->psHwReference->pBoardDriver;

        if (bfl_frame != NULL)
        {
            phJal_Trace(reception_handling_params->psHwReference, "phJal_ReceptionHandling: ENTERING");

            halt_param.self = &bfl_frame->Iso14443_3A;
            if ((bfl_status == PH_ERR_BFL_SUCCESS) && (event_status == NFCSTATUS_SUCCESS))
            {
                bfl_status = ConfigureTargetCommunicationUnit(bfl_frame);

                receive_param.self = &bfl_frame->RcIo;
                receive_param.rx_buffer = bfl_frame->TRxBuffer;
                receive_param.rx_buffer_size =  bfl_frame->TRxBufferSize;
                reception_handling_params->TargetConfiguration = TARGET_NOT_CONFIGURED;
                reception_handling_params->InitialiseTarget = 1;
                reception_handling_params->ConsumeReceptionEvent = 1;
                reception_handling_params->EnableCRCAfterNextReception = 0;

                /* Use semaphore to sync with STM restart use case: When STM is called again we will
                   want to wait for the loop to be exited (race condition avoiding): */
                phOsalNfc_ConsumeSemaphore(bfl_frame->TargetModeRestartCleanupSync);

                do
                {
                    /* Trace total LOOP time */
                    phJal_Trace(reception_handling_params->psHwReference, "\n\nphJal_ReceptionHandling: LOOP TOP");

                    if ((dispatch_status == NFCSTATUS_SUCCESS) &&
                        (reception_handling_params->ConsumeReceptionEvent == 1))
                    {
                        event_status = phOsalNfc_ConsumeSemaphore(bfl_frame->ReceptionInfoHandle_RHT);

                        reportDbg0("Locking caller: Not allowed to EXIT:\n");
                        //phOsalNfc_ConsumeSemaphore(bfl_frame->TargetModeFunctionReturnHandle); // Peer to Peer BugFix: Semaphore removed
                    }

                    if (reception_handling_params->TerminationState != TERMINATE_RECEPTION)
                    {
                        reportDbg0("loop:\t\t receiving\n");
                        receive_param.rx_buffer = bfl_frame->TRxBuffer;
                        receive_param.rx_buffer_size = bfl_frame->TRxBufferSize;
                        /* Remember that we are now waiting for data: */
                        bfl_frame->WaitingForReception = 1;
                        bfl_status = bfl_frame->RcIo.Receive(&receive_param);
                        /* Waiting finished: */
                        bfl_frame->WaitingForReception = 0;
                        bfl_frame->NfcTargetDispatchParam.buffer = receive_param.rx_buffer;
                        bfl_frame->NfcTargetDispatchParam.length = receive_param.rx_buffer_size;

                        reportDbg2("loop:\t\t received %d data bytes with (status %02X):\n",
                            receive_param.rx_buffer_size, bfl_status);

                        #ifndef NDEBUG
                        for (buffer_index = 0; buffer_index < receive_param.rx_buffer_size; ++buffer_index)
                        {
                            reportDbg1(" %02X", receive_param.rx_buffer[buffer_index]);
                        }
                        reportDbg0("\n");
                        bfl_frame->RcRegCtlGetRegParam.address = PHCS_BFL_JREG_STATUS2;
                        bfl_frame->RcRegCtl->GetRegister(&bfl_frame->RcRegCtlGetRegParam);
                        reportDbg1("Register Status2 %02X\n", bfl_frame->RcRegCtlGetRegParam.reg_data);
                        #endif
                    } else
                    {
                        /* In the meantime (between last RX, and now, while processing previous request)
                           STM has been restarted: We must abort current run: */
                        bfl_status = PH_ERR_BFL_ABORTED;
                    }

                    if (bfl_status == PH_ERR_BFL_SUCCESS)
                    {
                        if (bfl_frame->NfcTargetDispatchParam.length > 0)
                        {
                            reception_handling_params->NumBytesReceivedByLastReceive =
                                bfl_frame->NfcTargetDispatchParam.length;

                            if ((reception_handling_params->TargetConfiguration == TARGET_NOT_CONFIGURED) ||
                                (reception_handling_params->TargetConfiguration == TARGET_DESELECTED) ||
                                (configure_target == 1))
                            {
                                reportDbg0("loop:\t\t configuring target\n");
                                bfl_status = ConfigureTarget(reception_handling_params);
                                reportDbg1("loop:\t\t configuration status: 0x%02X\n", bfl_status);
                                target_error_occurred = (bfl_status == PH_ERR_BFL_SUCCESS)? 0 : 1;
                                if (target_error_occurred == 1)
                                {
                                    reception_handling_params->ReceptionInfoState = TERMINATE_RECEPTION;
                                } else
                                {
                                    /* No action! */
                                }
                            }

                            if (target_error_occurred == 0)
                            {
                                if (reception_handling_params->TargetConfiguration == TARGET_DESELECT_CONFIGURATION)
                                {
                                    reception_handling_params->TargetConfiguration = TARGET_DESELECTED;
                                    reportDbg1("loop:\t\t target configuration %02X\n",
                                        reception_handling_params->TargetConfiguration);
                                }

                                if (bfl_status == PH_ERR_BFL_SUCCESS)
                                {
                                    if(reception_handling_params->TargetRxSpeed == PHCS_BFLOPCTL_VAL_RF106K)
                                    {
                                        reportDbg0("loop:\t\t call Halt function\n");
                                        halt_param.buffer        = bfl_frame->NfcTargetDispatchParam.buffer;
                                        halt_param.buffer_length = bfl_frame->NfcTargetDispatchParam.length;
                                        bfl_status = bfl_frame->Iso14443_3A.HaltA(&halt_param);
                                        if(bfl_status == PH_ERR_BFL_SUCCESS)
                                        {
                                            reception_handling_params->ConsumeReceptionEvent = 0;
                                            reportDbg0("loop:\t\t Halt command received.\n");
                                        }
                                        else
                                        {
                                            bfl_status = PH_ERR_BFL_SUCCESS;
                                            reportDbg0("loop:\t\t starting dispatcher\n");

                                            dispatch_status = phJal_DispatchTargetCommand(reception_handling_params);
                                            if ((dispatch_status == NFCSTATUS_SUCCESS) &&
                                                (reception_handling_params->TargetConfiguration == TARGET_DESELECT_CONFIGURATION))
                                            {
                                                bfl_status = ConfigureTargetCommunicationUnit(bfl_frame);
                                                reportDbg1("loop:\t\t reconfigured target with status 0x%02X\n", bfl_status);
                                                reception_handling_params->TargetConfiguration = TARGET_DESELECTED;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        reportDbg0("loop:\t\t starting dispatcher\n");
                                        dispatch_status = phJal_DispatchTargetCommand(reception_handling_params);
                                        if ((dispatch_status == NFCSTATUS_SUCCESS) &&
                                            (reception_handling_params->TargetConfiguration == TARGET_DESELECT_CONFIGURATION))
                                        {
                                            reportDbg0("loop:\t\t reconfigure target\n");
                                            ConfigureTargetCommunicationUnit(bfl_frame);
                                            reception_handling_params->TargetConfiguration = TARGET_DESELECTED;
                                        }
                                    }

                                    if (dispatch_status != NFCSTATUS_SUCCESS)
                                    {
                                        if (reception_handling_params->TargetConfiguration == TARGET_READY)
                                        {
                                            /* If an error during communication (DEP) occurs, the
                                               reception loop will be closed. */
                                            target_error_occurred = 1;
                                        }
                                        reportDbg0("loop:\t\t reconfigure target because of error during dispatching\n");
                                        /* Config Tg: */
                                        reception_handling_params->NumBytesReceivedByLastReceive = 0;
                                        reception_handling_params->ReceptionInfoState = RECEPTION_TIMEOUT;
                                        reception_handling_params->InitialiseTarget = 0;
                                        bfl_status = ConfigureTargetCommunicationUnit(bfl_frame);
                                        /* Switch OFF RF: */
                                        phJal_SwitchOffRf(bfl_frame->ReceptionHandlingParams.psHwReference);
                                        if (bfl_status != PH_ERR_BFL_SUCCESS)
                                        {
                                            target_error_occurred = 1;
                                        }
                                        configure_target = 1;

                                        phJal_RegDump(bfl_frame);
                                    }
                                    else
                                    {
                                        reception_handling_params->InitialiseTarget = 0;
                                        configure_target = 0;
                                        target_error_occurred = 0;
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (reception_handling_params->ConsumeReceptionEvent == 1)
                            {
                               reception_handling_params->ConsumeReceptionEvent = 0;
                            }
                        }
                    }

                    if (bfl_status != PH_ERR_BFL_SUCCESS)
                    {
                        reportDbg1("loop:\t\t error (bfl_status == 0x%4X) occurred!\n", bfl_status);

                        /* MANTIS #622
                         * Switching RF OFF on error, ignoring the status:
                         */
                        phJal_SwitchOffRf(reception_handling_params->psHwReference);

                        switch (bfl_status & ~PH_ERR_BFL_COMP_MASK)
                        {
                            case PH_ERR_BFL_IO_TIMEOUT:
                                reportDbg0("loop:\t\t TIMEOUT recognised.\n");
                                reception_handling_params->ReceptionInfoState = RECEPTION_TIMEOUT;
                                // Change of WFSPE() function in StartTargetMode - Addition of Handle
                                // phOsalNfc_ProduceSemaphore(bfl_frame->ReceptionInfoHandle_STM); // Need not be produced here since produced later
                                target_error_occurred = 1;
                                break;

                            case PH_ERR_BFL_ABORTED:
                                reportDbg0("loop:\t\t ABORT EVENT recognised.\n");
                                reception_handling_params->ReceptionInfoState = RECEPTION_ABORTED;
                                // Change of WFSPE() function in StartTargetMode - Addition of Handle
                                // phOsalNfc_ProduceSemaphore(bfl_frame->ReceptionInfoHandle_STM); // Need not be produced here since produced later
                                target_error_occurred = 1;
                                break;

                            case PH_ERR_BFL_INTERFACE_ERROR:
                                reportDbg0("loop:\t\t INTERFACE_ERROR recognised.\n");
                                reception_handling_params->ReceptionInfoState = TERMINATE_RECEPTION;
                                // Change of WFSPE() function in StartTargetMode - Addition of Handle
                                // phOsalNfc_ProduceSemaphore(bfl_frame->ReceptionInfoHandle_STM); // Need not be produced here since produced later
                                target_error_occurred = 1;
                                break;

                            default:
                                if (reception_handling_params->TargetConfiguration == TARGET_NOT_CONFIGURED)
                                {
                                    reportDbg0("loop:\t\t reconfiguring target\n");
                                    reception_handling_params->NumBytesReceivedByLastReceive = 0;
                                    reception_handling_params->ReceptionInfoState = RECEPTION_TIMEOUT;
                                    reception_handling_params->InitialiseTarget = 0;
                                    bfl_status = ConfigureTargetCommunicationUnit(bfl_frame);
                                    if (bfl_status == PH_ERR_BFL_SUCCESS)
                                    {
                                        /* wait for further requests */
                                        reception_handling_params->ConsumeReceptionEvent = 0;
                                        target_error_occurred = 0;
                                        configure_target = 1;
                                    } else
                                    {
                                        target_error_occurred = 1;
                                    }
                                } else
                                {
                                    target_error_occurred = 1;
                                }
                                break;
                        }
                    }

                    if (target_error_occurred == 1)
                    {
                        /* signal termination */
                        reportDbg0("\nloop:\t\t terminating reception events\n");
                        phJal_SetTargetConfiguration(bfl_frame, TARGET_NOT_CONFIGURED);
                        /* -- by GK: status overwritten: wanted in some cases?? -- reception_handling_params->ReceptionInfoState = TERMINATE_RECEPTION; */
                        reception_handling_params->ReceptionEndpointToUserState = TERMINATE_RECEPTION;
                        reception_handling_params->TransmissionEndpointToUserState = TERMINATE_RECEPTION;
                        phOsalNfc_ProduceSemaphore(bfl_frame->ReceptionEndpointToUserHandle);
                        phOsalNfc_ProduceSemaphore(bfl_frame->TransmissionEndpointToUserHandle);
                        /* Change of WFSPE() function in StartTargetMode - Addition of Handle */
                        phOsalNfc_ProduceSemaphore(bfl_frame->ReceptionInfoHandle_STM);
                        phOsalNfc_ProduceSemaphore(bfl_frame->ReceptionInfoHandle_RHT);

                    }
                    event_status = phOsalNfc_ConsumeSemaphore(bfl_frame->TerminateReceptionHandle);
                    phOsalNfc_ProduceSemaphore(bfl_frame->TerminateReceptionHandle);
                    /* Semaphore for Target mode function return! */
                    //phOsalNfc_ProduceSemaphore(bfl_frame->TargetModeFunctionReturnHandle); // Peer to Peer BugFix: Semaphore removed

                    /* Trace total LOOP time */
                    phJal_Trace(reception_handling_params->psHwReference, "phJal_ReceptionHandling: LOOP BOTTOM");

                } while(((reception_handling_params->TerminationState) != TERMINATE_RECEPTION) && (target_error_occurred == 0));

                reportDbg0("\nloop:\t\t exiting reception loop\n");
            }
            /* signal termination of the reception loop */
            phOsalNfc_ProduceSemaphore(bfl_frame->TargetModeRunningHandle);
            /* STM-Restart sync handling: */
            phOsalNfc_ProduceSemaphore(bfl_frame->TargetModeRestartCleanupSync);

            phJal_Trace(bfl_frame->ReceptionHandlingParams.psHwReference, "phJal_ReceptionHandling: EXITING");
        } else
        {
            /* BFL frame == NULL */
        }
    } else
    {
        /* Parameters == NULL */
    }
}

NFCSTATUS phJal_DispatchTargetCommand(phJal_ReceptionHandlingParams_t *psReceptionHandlingParams)
{
	phcsBfl_Status_t        bfl_status = PH_ERR_BFL_SUCCESS;
    NFCSTATUS               event_status = NFCSTATUS_SUCCESS;
    NFCSTATUS               command_status = NFCSTATUS_SUCCESS;
    phJal_sBFLUpperFrame_t	*bfl_frame = NULL;

    #ifdef PHCS_NFC_T_BUFFER_DEBUG
        int i;
    #endif

	phJal_Trace(psReceptionHandlingParams->psHwReference, "phJal_DispatchTargetCommand");

    bfl_frame = (phJal_sBFLUpperFrame_t*)psReceptionHandlingParams->psHwReference->pBoardDriver;
    reportDbg1("dispatcher:\t started with target configuration: %02X\n",
        psReceptionHandlingParams->TargetConfiguration);
	if (event_status == NFCSTATUS_SUCCESS)
	{
		if (event_status == NFCSTATUS_SUCCESS)
		{
            psReceptionHandlingParams->ConsumeReceptionEvent = 1;
            reportDbg0("dispatcher:\t dispatching to endpoint\n");

            #ifdef PHCS_NFC_T_BUFFER_DEBUG
            printf("\n\nFIFO-Content: Length = %2d\n", bfl_frame->NfcTargetDispatchParam.length);
                for (i = 0; i < bfl_frame->NfcTargetDispatchParam.length; i++)
                {
                    printf("%02X ", bfl_frame->NfcTargetDispatchParam.buffer[i]);
                    if ((i > 0) && ((i + 1) % 16 == 0))
                    {
                        printf("\n");
                    }
                }
                printf("\n---\n");
            #endif
            
            bfl_status = bfl_frame->NfcT.Dispatch(&bfl_frame->NfcTargetDispatchParam);
            reportDbg1("dispatcher:\t bfl status: %02X\n", bfl_status);
            /* RegDumpFile("target_dump.txt", "a","after dispatch", 0, 1, bfl_frame->RcRegCtl); // Debug Only */
			if (bfl_status == PH_ERR_BFL_SUCCESS)
			{
                reportDbg0("dispatcher:\t successfully returned from dispatching\n");
                reportDbg1("dispatcher:\t target configuration: %02X\n",
                    psReceptionHandlingParams->TargetConfiguration);

                if (psReceptionHandlingParams->TargetConfiguration == TARGET_READY)
                {
                    reportDbg0("dispatcher:\t release info handle\n");
                    /* Change of WFSPE() function in StartTargetMode - Addition of Handle */
                    event_status = phOsalNfc_ProduceSemaphore(bfl_frame->ReceptionInfoHandle_RHT);

                    if (psReceptionHandlingParams->EndpointDEPMode == DEP_TRANSMISSION)
                    {
                        if (psReceptionHandlingParams->EndpointDEPState == DEP_CONTINUE)
                        {
                            reportDbg0("dispatcher:\t DEP data transmission successfully\n");

                            event_status =
                                phOsalNfc_ConsumeSemaphore(bfl_frame->TransmissionUserToEndpointHandle);
                            if (event_status == NFCSTATUS_SUCCESS)
                            {
                                /* signal user that data was successfully sent */
                                event_status =
                                    phOsalNfc_ProduceSemaphore(bfl_frame->TransmissionEndpointToUserHandle);
                            }
                        }
                        else if (psReceptionHandlingParams->EndpointDEPState == DEP_DONE)
                        {
                            /* transmission of user data is now finished thus preparing target
                               for another DEP request */
                            reportDbg0("dispatcher:\t DEP target data transmission finished\n");
                            psReceptionHandlingParams->EndpointDEPState = DEP_START;
                            psReceptionHandlingParams->EndpointDEPMode = DEP_RECEPTION;
                            phOsalNfc_ProduceSemaphore(bfl_frame->TransmissionEndpointToUserHandle);
                        }
                    }
                }
				else if (psReceptionHandlingParams->TargetConfiguration == TARGET_CONFIGURATION_IN_PROGRESS)
				{
                    reportDbg0("dispatcher:\t Target configured successfully\n");
                    reportDbg1("dispatcher:\t Reception State %d\n", psReceptionHandlingParams->ReceptionInfoState);
                    event_status = phJal_SetTargetConfiguration(bfl_frame, TARGET_READY);
				}
				else if (psReceptionHandlingParams->TargetConfiguration == TARGET_DESELECT_IN_PROGRESS)
				{

                    event_status = phJal_SetTargetConfiguration(bfl_frame, TARGET_DESELECT_CONFIGURATION);
                    psReceptionHandlingParams->ReceptionInfoState = TARGET_DESELECTED;
                    if (event_status == NFCSTATUS_SUCCESS)
                    {
                        /* Change of WFSPE() function in StartTargetMode - Addition of Handle */
                        event_status = phOsalNfc_ProduceSemaphore(bfl_frame->ReceptionInfoHandle_RHT);
                    }
				}
                else if (psReceptionHandlingParams->TargetConfiguration == TARGET_WAKED_UP)
				{
                    event_status = phJal_SetTargetConfiguration(bfl_frame, TARGET_READY);
                    psReceptionHandlingParams->ReceptionInfoState = NO_STATE;
                    psReceptionHandlingParams->ConsumeReceptionEvent = 0;
				}

				if (psReceptionHandlingParams->ReceptionInfoState == AUTOMATIC_ATR_RES_SENT)
				{
                    reportDbg1("dispatcher:\t event status: %02X\n", event_status);
                    if (event_status == NFCSTATUS_SUCCESS)
                    {
                        /* TgInfo isn't available anymore because StartTargetMode will return now */
                        psReceptionHandlingParams->pTgInfo = NULL;
                        psReceptionHandlingParams->pConnectionReq = NULL;
                        psReceptionHandlingParams->pConnectionReqBufLength = NULL;
                        reportDbg0("dispatcher:\t release reception info handle\n");
                        /* Change of WFSPE() function in StartTargetMode - Addition of Handle */
					    if(bfl_frame->IsStartTargetMode == 1)
                        {
                            phOsalNfc_ProduceSemaphore(bfl_frame->ReceptionInfoHandle_STM);
                        }
                        event_status = phOsalNfc_ProduceSemaphore(bfl_frame->ReceptionInfoHandle_RHT);
                    }
				}

                if (psReceptionHandlingParams->TransmissionEndpointToUserState == USER_ATR_SENT)
				{
					if (event_status == NFCSTATUS_SUCCESS)
                    {
                        reportDbg0("dispatcher:\t release tEtoU handle\n");
                        event_status =
                            phOsalNfc_ProduceSemaphore(bfl_frame->TransmissionEndpointToUserHandle);
                    }
				}
                command_status = NFCSTATUS_SUCCESS;
			} else
            {
                /* Switch OFF RF, as there is the need to RX again: */
                phJal_SwitchOffRf(bfl_frame->ReceptionHandlingParams.psHwReference);

                /* Handle error cases: */
                if ((bfl_status & ~PH_ERR_BFL_COMP_MASK) == PH_ERR_BFL_UNSUPPORTED_COMMAND)
                {
                    command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_DEVICE_REQUEST);
                }
                else if ((bfl_status & ~PH_ERR_BFL_COMP_MASK) == PH_ERR_BFL_INVALID_DEVICE_STATE)
                {
                    /* Ignore all requests invalid in the current state */
                    psReceptionHandlingParams->ConsumeReceptionEvent = 0;
                    command_status = NFCSTATUS_SUCCESS;
                }
                else if ((bfl_status & ~PH_ERR_BFL_COMP_MASK) == PH_ERR_BFL_INVALID_FORMAT)
                {
                    psReceptionHandlingParams->ConsumeReceptionEvent = 0;
                    command_status = NFCSTATUS_SUCCESS;
                }
			    else
			    {
				    if (psReceptionHandlingParams->TargetConfiguration == TARGET_CONFIGURATION_IN_PROGRESS)
				    {
                        event_status = phJal_SetTargetConfiguration(bfl_frame, TARGET_NOT_CONFIGURED);
				    }

                    if (psReceptionHandlingParams->TargetConfiguration == TARGET_DESELECT_IN_PROGRESS)
				    {
					    event_status = phJal_SetTargetConfiguration(bfl_frame, TARGET_READY);
				    }

                    command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_DEVICE_REQUEST);
                    psReceptionHandlingParams->ConsumeReceptionEvent = 0;
                    /* add else stmt to release receptioninfohandle */
			    }
            }
		}
        else
        {
            reportDbg0("dispatcher:\t consume error\n");
        }
	}

    if (event_status == NFCSTATUS_SUCCESS)
    {
        reportDbg1("dispatcher:\t ended with status %02X\n", command_status);
        return command_status;
    }
    else
    {
        reportDbg1("dispatcher:\t ended with status %02X\n", command_status);
	    return event_status;
    }
}

NFCSTATUS phJal_SetTargetConfiguration(phJal_sBFLUpperFrame_t *psBflFrame,
                                       uint8_t TargetConfiguration)
{
    NFCSTATUS   event_status = NFCSTATUS_SUCCESS;

    phJal_Trace(NULL, "phJal_SetTargetConfiguration");

    event_status = phOsalNfc_ConsumeSemaphore(psBflFrame->TargetInfoHandle);
    if (event_status == NFCSTATUS_SUCCESS)
    {
		psBflFrame->ReceptionHandlingParams.TargetConfiguration = TargetConfiguration;
        event_status = phOsalNfc_ProduceSemaphore(psBflFrame->TargetInfoHandle);
    }
    return event_status;
}


phcsBfl_Status_t ConfigureTarget(phJal_ReceptionHandlingParams_t *psReceptionHandlingParams)
{
    phcsBfl_Status_t        bfl_status = PH_ERR_BFL_SUCCESS;
    phJal_sBFLUpperFrame_t  *bfl_frame = NULL;
    uint8_t                 reception_speed = 0;

    if (psReceptionHandlingParams != NULL)
    {
        phJal_Trace(psReceptionHandlingParams->psHwReference, "ConfigureTarget");

        bfl_frame = (phJal_sBFLUpperFrame_t*)psReceptionHandlingParams->psHwReference->pBoardDriver;

        if (bfl_status == PH_ERR_BFL_SUCCESS)
        {
            bfl_frame->RcRegCtlGetRegParam.address = PHCS_BFL_JREG_RXMODE;
            bfl_status = bfl_frame->RcRegCtl->GetRegister(&bfl_frame->RcRegCtlGetRegParam);

            if (bfl_status == PH_ERR_BFL_SUCCESS)
            {
                reception_speed =
                    (uint8_t)((bfl_frame->RcRegCtlGetRegParam.reg_data & PHCS_BFL_JMASK_SPEED)>>4);
                psReceptionHandlingParams->TargetRxSpeed =
                    (uint8_t)((bfl_frame->RcRegCtlGetRegParam.reg_data & PHCS_BFL_JMASK_SPEED)>>4);

                bfl_frame->RcRegCtlGetRegParam.address = PHCS_BFL_JREG_TXMODE;
                bfl_status = bfl_frame->RcRegCtl->GetRegister(&bfl_frame->RcRegCtlGetRegParam);

                if (bfl_status == PH_ERR_BFL_SUCCESS)
                {
                    psReceptionHandlingParams->TargetTxSpeed =
                        (uint8_t)((bfl_frame->RcRegCtlGetRegParam.reg_data & PHCS_BFL_JMASK_SPEED)>>4);

                    bfl_frame->RcRegCtlGetRegParam.address = PHCS_BFL_JREG_STATUS2;
                    bfl_status = bfl_frame->RcRegCtl->GetRegister(&bfl_frame->RcRegCtlGetRegParam);
                    reportDbg1("config target:\t target rx speed: %02X\n",
                        psReceptionHandlingParams->TargetTxSpeed);
                    reportDbg1("config target:\t target tx speed: %02X\n",
                        psReceptionHandlingParams->TargetRxSpeed);

                    if (bfl_frame->ReceptionHandlingParams.EnableCRCAfterNextReception == 1)
                    {
                        reportDbg0("config target:\t enabling CRC\n");
                        bfl_frame->RcRegCtlModifyParam.address = PHCS_BFL_JREG_RXMODE;
                        bfl_frame->RcRegCtlModifyParam.mask    = PHCS_BFL_JBIT_CRCEN;
                        bfl_frame->RcRegCtlModifyParam.set     = 1;
                        bfl_status = bfl_frame->RcRegCtl->ModifyRegister(&bfl_frame->RcRegCtlModifyParam);
                    }

                    if (bfl_status == PH_ERR_BFL_SUCCESS)
                    {
                        if (bfl_frame->RcRegCtlGetRegParam.reg_data & PHCS_BFL_JBIT_MFSELECTED)
                        {
                            bfl_status =
                                ConfigureTargetForPassiveMode(psReceptionHandlingParams);
                        }
                        else
                        {
                            bfl_status =
                                ConfigureTargetForActiveMode(psReceptionHandlingParams);
                        }
                    }
                }
            }
        }
    }
    return bfl_status;
}

phHal_eOpModes_t phJal_GetRequestedOpMode(uint8_t Mode, uint8_t TargetType, uint8_t Speed)
{
    phHal_eOpModes_t requested_op_mode = phHal_eOpModesArrayTerminator;

    phJal_Trace(NULL, "phJal_GetRequestedOpMode");

    if (Mode == CONFIGURE_PASSIVE_MODE)
    {
        switch(Speed)
        {
        case PHCS_BFLOPCTL_VAL_RF106K:
            requested_op_mode = (TargetType == ISO144434_TARGET)?
                phHal_eOpModesISO14443_4A : phHal_eOpModesNfcPassive106;
            break;
        case PHCS_BFLOPCTL_VAL_RF212K:
            requested_op_mode = (TargetType == NFC_TARGET)?
                phHal_eOpModesNfcPassive212 : phHal_eOpModesFelica212;
            break;
        case PHCS_BFLOPCTL_VAL_RF424K:
            requested_op_mode = (TargetType == NFC_TARGET)?
                phHal_eOpModesNfcPassive424 : phHal_eOpModesFelica424;
            break;
        default:
            requested_op_mode = phHal_eOpModesArrayTerminator;
            break;
        }
    }
    else if (Mode == CONFIGURE_ACTIVE_MODE)
    {
        switch(Speed)
        {
        case PHCS_BFLOPCTL_VAL_RF106K:
            requested_op_mode = phHal_eOpModesNfcActive106;
            break;
        case PHCS_BFLOPCTL_VAL_RF212K:
            requested_op_mode = phHal_eOpModesNfcActive212;
            break;
        case PHCS_BFLOPCTL_VAL_RF424K:
            requested_op_mode = phHal_eOpModesNfcActive424;
            break;
        default:
            requested_op_mode = phHal_eOpModesArrayTerminator;
            break;
        }
    }
    else
    {
        requested_op_mode = phHal_eOpModesArrayTerminator;
    }
    return requested_op_mode;
}

phcsBfl_Status_t ConfigureTargetForPassiveMode(phJal_ReceptionHandlingParams_t *psReceptionHandlingParams)
{
    phcsBfl_Status_t        bfl_status = PH_ERR_BFL_SUCCESS;
    phJal_sBFLUpperFrame_t  *bfl_frame = NULL;
    uint8_t                 target_type_to_configure = NO_TARGET;
    phHal_eOpModes_t        requested_op_mode = phHal_eOpModesArrayTerminator;

    if (psReceptionHandlingParams != NULL)
    {
        phJal_Trace(psReceptionHandlingParams->psHwReference, "ConfigureTargetForPassiveMode");

        bfl_frame = (phJal_sBFLUpperFrame_t*)psReceptionHandlingParams->psHwReference->pBoardDriver;

        bfl_frame->RcRegCtlModifyParam.address = PHCS_BFL_JREG_TXAUTO;
        bfl_frame->RcRegCtlModifyParam.mask    = PHCS_BFL_JBIT_AUTORFOFF | PHCS_BFL_JBIT_CAON |
            PHCS_BFL_JBIT_TX2RFAUTOEN | PHCS_BFL_JBIT_TX1RFAUTOEN;
        bfl_frame->RcRegCtlModifyParam.set = 0;
        bfl_status = bfl_frame->RcRegCtl->ModifyRegister(&bfl_frame->RcRegCtlModifyParam);

        if (bfl_status == PH_ERR_BFL_SUCCESS)
        {
            if(psReceptionHandlingParams->TargetRxSpeed == PHCS_BFLOPCTL_VAL_RF106K)
            {
                if(bfl_status == PH_ERR_BFL_SUCCESS)
                {
                    if((bfl_frame->TRxBuffer[0] == PHCS_BFLNFCCOMMON_PREAM_1BYTE)
                        && ((bfl_frame->TRxBuffer[1] + 1) ==
                            psReceptionHandlingParams->NumBytesReceivedByLastReceive))
                    {
                        target_type_to_configure = NFC_TARGET;
                        bfl_frame->NfcTargetDispatchParam.buffer = &bfl_frame->TRxBuffer[1];
                        bfl_frame->NfcTargetDispatchParam.length =
                            psReceptionHandlingParams->NumBytesReceivedByLastReceive - 1;

                        bfl_frame->RcOpCtlAttribParam.group_ordinal = PHCS_BFLOPCTL_GROUP_MODE;
                        bfl_frame->RcOpCtlAttribParam.attrtype_ordinal = PHCS_BFLOPCTL_ATTR_PASSIVE;
                        bfl_frame->RcOpCtlAttribParam.param_a = PHCS_BFLOPCTL_VAL_RF106K;
                        bfl_frame->RcOpCtlAttribParam.param_b = PHCS_BFLOPCTL_VAL_RF106K;
                        bfl_status = bfl_frame->RcOpCtl.SetAttrib(&bfl_frame->RcOpCtlAttribParam);
                    }
                    else
                    {
                        target_type_to_configure = ISO144434_TARGET;
                        bfl_frame->NfcTargetDispatchParam.buffer = bfl_frame->TRxBuffer;
                        bfl_frame->NfcTargetDispatchParam.length =
                            psReceptionHandlingParams->NumBytesReceivedByLastReceive;
                        bfl_status = PH_ERR_BFL_SUCCESS;
                    }

                }
            }
            else
            {
                if ((psReceptionHandlingParams->TargetRxSpeed == PHCS_BFLOPCTL_VAL_RF212K) ||
                    (psReceptionHandlingParams->TargetRxSpeed == PHCS_BFLOPCTL_VAL_RF424K))
                {
                    if((bfl_frame->TRxBuffer[0] == psReceptionHandlingParams->NumBytesReceivedByLastReceive) &&
                        (bfl_frame->TRxBuffer[1] == PHCS_BFLNFCCOMMON_REQ))
                    {
                        target_type_to_configure = NFC_TARGET;
                    }
                    else
                    {
                        target_type_to_configure = FELICA_TARGET;
                    }

                    if (bfl_frame->ReceptionHandlingParams.EnableCRCAfterNextReception == 1)
                    {
                        bfl_frame->NfcTargetDispatchParam.length -= 2;
                        reportDbg1("config target:\t reduce data length to %d bytes\n",
                            bfl_frame->NfcTargetDispatchParam.length);
                        bfl_frame->ReceptionHandlingParams.NumBytesReceivedByLastReceive =
                            bfl_frame->NfcTargetDispatchParam.length;
                        bfl_frame->ReceptionHandlingParams.EnableCRCAfterNextReception = 0;
                    }
                    bfl_frame->RcOpCtlAttribParam.group_ordinal = PHCS_BFLOPCTL_GROUP_MODE;
                    bfl_frame->RcOpCtlAttribParam.attrtype_ordinal = PHCS_BFLOPCTL_ATTR_PASSIVE;
                    bfl_frame->RcOpCtlAttribParam.param_a = psReceptionHandlingParams->TargetRxSpeed;
                    bfl_frame->RcOpCtlAttribParam.param_b = psReceptionHandlingParams->TargetRxSpeed;
                    bfl_status = bfl_frame->RcOpCtl.SetAttrib(&bfl_frame->RcOpCtlAttribParam);
                }
                else
                {
                    bfl_status = PH_ERR_BFL_ERROR_NY_IMPLEMENTED;
                }
            }
        }

        if (bfl_status == PH_ERR_BFL_SUCCESS)
        {
            if (psReceptionHandlingParams->InitialiseTarget == 1)
            {
                bfl_status = InitialiseNfcTarget(psReceptionHandlingParams, CONFIGURE_PASSIVE_MODE);

                /* set OpMode to be used by target */
                if (psReceptionHandlingParams->OpModes != NULL)
                {
                    requested_op_mode = phJal_GetRequestedOpMode(CONFIGURE_PASSIVE_MODE,
                        target_type_to_configure, psReceptionHandlingParams->TargetRxSpeed);

                    psReceptionHandlingParams->OpModes[0] = (bfl_status == PH_ERR_BFL_SUCCESS)?
                        requested_op_mode : phHal_eOpModesArrayTerminator;
                    psReceptionHandlingParams->OpModes[1] = phHal_eOpModesArrayTerminator;
                }
            }
            else
            {
                bfl_frame->RcRegCtlModifyParam.address = PHCS_BFL_JREG_MODE;
                bfl_frame->RcRegCtlModifyParam.mask    = PHCS_BFL_JBIT_DETECTSYNC;
                bfl_frame->RcRegCtlModifyParam.set     = 1;
                bfl_status =
                    bfl_frame->RcRegCtl->ModifyRegister(&bfl_frame->RcRegCtlModifyParam);
            }

        }
    }
    else
    {
        bfl_status = PH_ERR_BFL_INVALID_PARAMETER;
    }

    return bfl_status;
}

phcsBfl_Status_t ConfigureTargetForActiveMode(phJal_ReceptionHandlingParams_t *psReceptionHandlingParams)
{
    phcsBfl_Status_t        bfl_status = PH_ERR_BFL_SUCCESS;
    phJal_sBFLUpperFrame_t  *bfl_frame = NULL;
    uint8_t                 target_type_to_configure = NO_TARGET;
    uint8_t                 crc_result = 0;
    phHal_eOpModes_t        requested_op_mode = phHal_eOpModesArrayTerminator;

    if (psReceptionHandlingParams != NULL)
    {
        phJal_Trace(psReceptionHandlingParams->psHwReference, "ConfigureTargetForActiveMode");

        bfl_frame =
            (phJal_sBFLUpperFrame_t*)psReceptionHandlingParams->psHwReference->pBoardDriver;

        if(psReceptionHandlingParams->TargetRxSpeed == PHCS_BFLOPCTL_VAL_RF106K)
        {
            if (bfl_frame->ReceptionHandlingParams.EnableCRCAfterNextReception == 1)
            {
                bfl_frame->ReceptionHandlingParams.EnableCRCAfterNextReception = 0;
            }

            reportDbg0("config target:\t active 106k mode detected.\n");
            if((bfl_frame->TRxBuffer[0] == PHCS_BFLNFCCOMMON_PREAM_1BYTE) &&
               ((bfl_frame->TRxBuffer[1] + 3) == psReceptionHandlingParams->NumBytesReceivedByLastReceive))
            {
                bfl_frame->RcRegCtlGetRegParam.address = PHCS_BFL_JREG_CRCRESULT1;
                bfl_status = bfl_frame->RcRegCtl->GetRegister(&bfl_frame->RcRegCtlGetRegParam);
                crc_result = bfl_frame->RcRegCtlGetRegParam.reg_data;

                bfl_frame->RcRegCtlGetRegParam.address = PHCS_BFL_JREG_CRCRESULT2;
                bfl_status = bfl_frame->RcRegCtl->GetRegister(&bfl_frame->RcRegCtlGetRegParam);

                if(bfl_status == PH_ERR_BFL_SUCCESS)
                {
                    if ((crc_result | bfl_frame->RcRegCtlGetRegParam.reg_data) == 0x00)
                    {
                        bfl_frame->RcOpCtlAttribParam.group_ordinal    = PHCS_BFLOPCTL_GROUP_MODE;
                        bfl_frame->RcOpCtlAttribParam.attrtype_ordinal = PHCS_BFLOPCTL_ATTR_ACTIVE;
                        bfl_frame->RcOpCtlAttribParam.param_a          = PHCS_BFLOPCTL_VAL_RF106K;
                        bfl_frame->RcOpCtlAttribParam.param_b          = PHCS_BFLOPCTL_VAL_RF106K;
                        bfl_status = bfl_frame->RcOpCtl.SetAttrib(&bfl_frame->RcOpCtlAttribParam);

                        if(bfl_status == PH_ERR_BFL_SUCCESS)
                        {
                            target_type_to_configure = NFC_TARGET;
                            bfl_frame->NfcTargetDispatchParam.buffer =
                                &bfl_frame->TRxBuffer[1];
                            bfl_frame->NfcTargetDispatchParam.length =
                                psReceptionHandlingParams->NumBytesReceivedByLastReceive - 3;

                            bfl_frame->RcRegCtlModifyParam.address = PHCS_BFL_JREG_MODE;
                            bfl_frame->RcRegCtlModifyParam.mask    = PHCS_BFL_JBIT_DETECTSYNC;
                            bfl_frame->RcRegCtlModifyParam.set     = 1;
                            bfl_frame->RcRegCtl->ModifyRegister(&bfl_frame->RcRegCtlModifyParam);
                        }
                    }
                }
            }
            else
            {
                bfl_status = PH_ERR_BFL_INVALID_FORMAT;
            }
        }
        else if (psReceptionHandlingParams->TargetRxSpeed == PHCS_BFLOPCTL_VAL_RF212K)
        {
            if (bfl_frame->ReceptionHandlingParams.EnableCRCAfterNextReception == 1)
            {
                bfl_frame->NfcTargetDispatchParam.length -= 2;
                bfl_frame->ReceptionHandlingParams.NumBytesReceivedByLastReceive =
                    bfl_frame->NfcTargetDispatchParam.length;
                bfl_frame->ReceptionHandlingParams.EnableCRCAfterNextReception = 0;
            }

            reportDbg0("config target:\t active 212k mode.\n");
            bfl_frame->RcOpCtlAttribParam.group_ordinal    = PHCS_BFLOPCTL_GROUP_MODE;
            bfl_frame->RcOpCtlAttribParam.attrtype_ordinal = PHCS_BFLOPCTL_ATTR_ACTIVE;
            bfl_frame->RcOpCtlAttribParam.param_a = PHCS_BFLOPCTL_VAL_RF212K;
            bfl_frame->RcOpCtlAttribParam.param_b = PHCS_BFLOPCTL_VAL_RF212K;

            bfl_status = bfl_frame->RcOpCtl.SetAttrib(&bfl_frame->RcOpCtlAttribParam);

            bfl_frame->NfcTargetDispatchParam.buffer =
                bfl_frame->TRxBuffer;
            bfl_frame->NfcTargetDispatchParam.length =
                psReceptionHandlingParams->NumBytesReceivedByLastReceive;
            target_type_to_configure = NFC_TARGET;

            bfl_frame->RcRegCtlModifyParam.address = PHCS_BFL_JREG_MODE;
            bfl_frame->RcRegCtlModifyParam.mask    = PHCS_BFL_JBIT_DETECTSYNC;
            bfl_frame->RcRegCtlModifyParam.set     = 1;
            bfl_frame->RcRegCtl->ModifyRegister(&bfl_frame->RcRegCtlModifyParam);

        }
        else if(psReceptionHandlingParams->TargetRxSpeed == PHCS_BFLOPCTL_VAL_RF424K)
        {
            if (bfl_frame->ReceptionHandlingParams.EnableCRCAfterNextReception == 1)
            {
                bfl_frame->NfcTargetDispatchParam.length -= 2;
                bfl_frame->ReceptionHandlingParams.NumBytesReceivedByLastReceive =
                    bfl_frame->NfcTargetDispatchParam.length;
                bfl_frame->ReceptionHandlingParams.EnableCRCAfterNextReception = 0;
            }

            reportDbg0("config target:\t active 424k mode.\n");
            bfl_frame->RcOpCtlAttribParam.group_ordinal    = PHCS_BFLOPCTL_GROUP_MODE;
            bfl_frame->RcOpCtlAttribParam.attrtype_ordinal = PHCS_BFLOPCTL_ATTR_ACTIVE;
            bfl_frame->RcOpCtlAttribParam.param_a = PHCS_BFLOPCTL_VAL_RF424K;
            bfl_frame->RcOpCtlAttribParam.param_b = PHCS_BFLOPCTL_VAL_RF424K;

            bfl_status = bfl_frame->RcOpCtl.SetAttrib(&bfl_frame->RcOpCtlAttribParam);

            bfl_frame->NfcTargetDispatchParam.buffer =
                bfl_frame->TRxBuffer;
            bfl_frame->NfcTargetDispatchParam.length =
                psReceptionHandlingParams->NumBytesReceivedByLastReceive;
            target_type_to_configure = NFC_TARGET;
        }
        else
        {
            bfl_status = PH_ERR_BFL_ERROR_NY_IMPLEMENTED;
        }

        if (bfl_status == PH_ERR_BFL_SUCCESS)
        {
            if (psReceptionHandlingParams->InitialiseTarget == 1)
            {
                /* set OpMode to be used by target */
                if (psReceptionHandlingParams->OpModes != NULL)
                {
                    requested_op_mode = phJal_GetRequestedOpMode(CONFIGURE_ACTIVE_MODE,
                        target_type_to_configure, psReceptionHandlingParams->TargetRxSpeed);
                    psReceptionHandlingParams->OpModes[0] = (bfl_status == PH_ERR_BFL_SUCCESS)?
                        requested_op_mode : phHal_eOpModesArrayTerminator;
                    psReceptionHandlingParams->OpModes[1] = phHal_eOpModesArrayTerminator;
                }

                switch(target_type_to_configure)
                {
                    case NFC_TARGET:
                        bfl_status =
                            InitialiseNfcTarget(psReceptionHandlingParams, CONFIGURE_ACTIVE_MODE);
                        break;
                    default:
                        break;
                }
            }
        }
    }
    else
    {
        bfl_status = PH_ERR_BFL_INVALID_PARAMETER;
    }

    return bfl_status;
}

phcsBfl_Status_t TargetEndpointCallback(phcsBflNfc_TargetEndpointParam_t *endpoint_param)
{
    phJal_ReceptionHandlingParams_t *reception_handling_params = NULL;
    phcsBfl_Status_t                callback_status = PH_ERR_BFL_SUCCESS;
    NFCSTATUS                       event_status = NFCSTATUS_SUCCESS;
    phJal_sBFLUpperFrame_t          *bfl_frame = NULL;

    reception_handling_params = (phJal_ReceptionHandlingParams_t*)endpoint_param->p_user;
    bfl_frame = (phJal_sBFLUpperFrame_t*)reception_handling_params->psHwReference->pBoardDriver;

    phJal_Trace(reception_handling_params->psHwReference, "TargetEndpointCallback");
    reportDbg1("endpoint:\t started (action type: %02X)\n", endpoint_param->action_type);
    switch (endpoint_param->action_type)
    {
        reportDbg0("endpoint:\t ATR detected\n");
        case PHCS_BFLNFCCOMMON_ATR_REQ:
            callback_status = HandleAtrRequest(reception_handling_params, endpoint_param);
            if (callback_status == PH_ERR_BFL_SUCCESS)
            {
                if (reception_handling_params->TargetConfiguration == TARGET_DESELECTED)
                {
                    /* target was waked up in passive mode */
                    event_status = phJal_SetTargetConfiguration(bfl_frame, TARGET_WAKED_UP);
                }
                else
                {
                    /* initial ATR */
                        event_status =
                            phJal_SetTargetConfiguration(bfl_frame, TARGET_CONFIGURATION_IN_PROGRESS);
                }
            }
            if (event_status != NFCSTATUS_SUCCESS)
            {
                callback_status = PH_ERR_BFL_OTHER_ERROR;
            }

            if (callback_status == PH_ERR_BFL_SUCCESS)
            {
                reception_handling_params->MetaChainingActive = 0;
                reception_handling_params->EndpointDEPState = DEP_START;
                reception_handling_params->EndpointDEPMode = DEP_RECEPTION;
                reportDbg0("endpoint:\t DEP Mode ready\n");
            }
            break;
        case PHCS_BFLNFCCOMMON_DSL_REQ:
            event_status = phJal_SetTargetConfiguration(bfl_frame, TARGET_DESELECT_IN_PROGRESS);
            callback_status = (event_status == NFCSTATUS_SUCCESS)?
                PH_ERR_BFL_SUCCESS : PH_ERR_BFL_OTHER_ERROR;
            break;
        case PHCS_BFLNFCCOMMON_DSL_RES:
            callback_status = HandleDslRequest(reception_handling_params, endpoint_param);
            break;
        case PHCS_BFLNFCCOMMON_PSL_REQ:
            reportDbg0("endpoint:\t PSL request detected\n");
            callback_status = HandlePslRequest(reception_handling_params, endpoint_param);
            break;
        case PHCS_BFLNFCCOMMON_PSL_RES:
            reportDbg0("endpoint:\t PSL RES detected\n");
            callback_status = HandlePslResponse(reception_handling_params, endpoint_param);
            break;
        case PHCS_BFLNFCCOMMON_WUP_REQ:
            event_status = phJal_SetTargetConfiguration(bfl_frame, TARGET_WAKED_UP);
            callback_status = (event_status == NFCSTATUS_SUCCESS)?
                PH_ERR_BFL_SUCCESS : PH_ERR_BFL_OTHER_ERROR;
            if (callback_status == PH_ERR_BFL_SUCCESS)
            {
                reception_handling_params->MetaChainingActive = 0;
                reception_handling_params->ReceptionInfoState = DATA_RECEIVED;
                reception_handling_params->EndpointDEPState = DEP_START;
                reception_handling_params->EndpointDEPMode = DEP_RECEPTION;
                reportDbg0("endpoint:\t WUP request detected\n");
            }
            break;
        case PHCS_BFLNFCCOMMON_DEP_REQ:
            reportDbg0("endpoint:\t DEP request detected\n");
            callback_status = HandleDepRequest(reception_handling_params, endpoint_param);
            break;
        case PHCS_BFLNFC_TARGET_REQ_ACK:
            reportDbg0("endpoint:\t REQ ACK detected\n");
            callback_status = HandleDepRequest(reception_handling_params, endpoint_param);
            break;
        case PHCS_BFLNFC_TARGET_REQ_INF:
            reportDbg2("endpoint:\t REQ INF detected (request length: %02X, flags: %02X)\n",
                endpoint_param->req_data_length, endpoint_param->flags);
            if (reception_handling_params->WTXRequested == 0)
            {
                /* MI-flag is only valid if DEP request with user data is received */
                if ((endpoint_param->flags & PHCS_BFLNFC_TARGET_EP_FLAGS_MI_PRES) ==
                    PHCS_BFLNFC_TARGET_EP_FLAGS_MI_PRES)
                {
                    reception_handling_params->WaitingForMoreData = 1;
                    reportDbg1("endpoint:\t %d bytes rec. and more data bytes available\n",
                        endpoint_param->req_data_length);
                }
                else
                {
                    reportDbg1("endpoint:\t %d bytes rec. and no further data bytes available\n",
                        endpoint_param->req_data_length);
                    reception_handling_params->WaitingForMoreData = 0;
                }
            }
            callback_status = HandleDepRequest(reception_handling_params, endpoint_param);
            break;
        case PHCS_BFLNFC_TARGET_REQ_TOX:
            endpoint_param->res_data_length = 0;
            reportDbg0("endpoint:\t REQ TOX detected\n");
            callback_status = PHCS_BFLNFC_TARGET_RES_INF;
            break;
        case PHCS_BFLNFCCOMMON_RLS_REQ:
            reportDbg0("endpoint:\t RLS detected\n");
            break;
        default:
            callback_status = PH_ERR_BFL_UNSUPPORTED_COMMAND;
            break;

    }
    return callback_status;
}

phcsBfl_Status_t InitialiseNfcTarget(phJal_ReceptionHandlingParams_t   *psReceptionHandlingParams,
                                     uint8_t                           Mode)
{
    phcsBfl_Status_t                bfl_status = PH_ERR_BFL_SUCCESS;
    phJal_sBFLUpperFrame_t          *bfl_frame = NULL;
    phcsBflNfc_TargetSetPUserParam_t user_endpoint_param = {0};

    if (psReceptionHandlingParams != NULL)
    {
        phJal_Trace(psReceptionHandlingParams->psHwReference, "InitialiseNfcTarget");

        bfl_frame =
            (phJal_sBFLUpperFrame_t*)psReceptionHandlingParams->psHwReference->pBoardDriver;

        bfl_frame->RcRegCtlModifyParam.address = PHCS_BFL_JREG_MODE;
        bfl_frame->RcRegCtlModifyParam.mask    = PHCS_BFL_JBIT_DETECTSYNC;
        bfl_frame->RcRegCtlModifyParam.set     = 1;
        bfl_status = bfl_frame->RcRegCtl->ModifyRegister(&bfl_frame->RcRegCtlModifyParam);

        if (bfl_status == PH_ERR_BFL_SUCCESS)
        {
            reportDbg0("config target:\t initialise NFC target\n");

            phcsBflNfc_TargetInit(&bfl_frame->NfcT,
                &bfl_frame->NfcTCp,
                PHCS_BFLNFCCOMMON_PREAM_OMIT,
                PHCS_BFLNFCCOMMON_PREAM_OMIT,
                NULL,
                PHJAL_TARGET_BUFFER_SIZE,
                bfl_frame->TNfcIdI,
                bfl_frame->TNfcIdT,
                (Mode == CONFIGURE_PASSIVE_MODE)? 1 : 0,
                &bfl_frame->NfcTargetEndpoint,
                &bfl_frame->RcIo);

            user_endpoint_param.p_user = psReceptionHandlingParams;
            user_endpoint_param.self = &bfl_frame->NfcT;
            bfl_frame->NfcT.SetPUser(&user_endpoint_param);
        }
    }
    else
    {
        bfl_status = PH_ERR_BFL_INVALID_PARAMETER;
    }
    return bfl_status;
}

phcsBfl_Status_t ConfigureTargetIds(phJal_ReceptionHandlingParams_t *psReceptionHandlingParams)
{
    phcsBfl_Status_t        bfl_status = PH_ERR_BFL_SUCCESS;
    phJal_sBFLUpperFrame_t  *bfl_frame = NULL;
    uint8_t                 buffer_index = 0;

    if (psReceptionHandlingParams != NULL)
    {
        phJal_Trace(psReceptionHandlingParams->psHwReference, "ConfigureTargetIds");

        bfl_frame =
            (phJal_sBFLUpperFrame_t*)psReceptionHandlingParams->psHwReference->pBoardDriver;

        bfl_frame->TRxBuffer[buffer_index++] = psReceptionHandlingParams->pTgInfo->SensRes[0];
        bfl_frame->TRxBuffer[buffer_index++] = psReceptionHandlingParams->pTgInfo->SensRes[1];

        memcpy(&bfl_frame->TRxBuffer[buffer_index], &psReceptionHandlingParams->pTgInfo->NFCID1t[0], 3);
        buffer_index += 3;
        bfl_frame->TRxBuffer[buffer_index++] = psReceptionHandlingParams->pTgInfo->SelRes;

        bfl_frame->RcOpCtlAttribParam.group_ordinal    = PHCS_BFLOPCTL_GROUP_PROTOCOL;
        bfl_frame->RcOpCtlAttribParam.attrtype_ordinal = PHCS_BFLOPCTL_ATTR_NFCID;
        bfl_frame->RcOpCtlAttribParam.param_a          = PHCS_BFLOPCTL_VAL_NFCID1;
        bfl_frame->RcOpCtlAttribParam.param_b          = buffer_index;
        bfl_frame->RcOpCtlAttribParam.buffer           = bfl_frame->TRxBuffer;

        bfl_status = bfl_frame->RcOpCtl.SetAttrib(&bfl_frame->RcOpCtlAttribParam);
        if( bfl_status == PH_ERR_BFL_SUCCESS)
        {
            buffer_index = 0;
            memcpy(&bfl_frame->TRxBuffer[buffer_index],
                psReceptionHandlingParams->pTgInfo->NFCID2t, 8);
            buffer_index += 8;
            memcpy(&bfl_frame->TRxBuffer[buffer_index], psReceptionHandlingParams->pTgInfo->PAD, 8);
            buffer_index += 8;

            bfl_frame->TRxBuffer[buffer_index++] = psReceptionHandlingParams->pTgInfo->SystemCode[0];
            bfl_frame->TRxBuffer[buffer_index++] = psReceptionHandlingParams->pTgInfo->SystemCode[1];

            bfl_frame->RcOpCtlAttribParam.param_a = PHCS_BFLOPCTL_VAL_NFCID2;
            bfl_frame->RcOpCtlAttribParam.param_b = buffer_index;
            bfl_frame->RcOpCtlAttribParam.buffer  = bfl_frame->TRxBuffer;

            bfl_status = bfl_frame->RcOpCtl.SetAttrib(&bfl_frame->RcOpCtlAttribParam);
        }
    }
    else
    {
        bfl_status = PH_ERR_BFL_INVALID_PARAMETER;
    }

    return bfl_status;
}

NFCSTATUS WaitForSemaphoreProtectedEvent(void       *hSemaphore,
                                         uint32_t   *pHandleToEventState,
                                         uint32_t   EventStateToWait,
                                         uint32_t   *pCurrentEventState,
                                         uint32_t   TimeoutValue)
{
    uint8_t event_signaled = 0;
    NFCSTATUS command_status = NFCSTATUS_SUCCESS;
    NFCSTATUS event_status = NFCSTATUS_SUCCESS;

    if ((hSemaphore != NULL) &&
        (pCurrentEventState != NULL) &&
        (pHandleToEventState != NULL))
    {
        phJal_Trace(NULL, "WaitForSemaphoreProtectedEvent");

        do
        {
            event_status = phOsalNfc_ConsumeSemaphoreEx(hSemaphore, TimeoutValue);

            if (event_status == NFCSTATUS_SUCCESS)
            {
                if (((*pHandleToEventState) & EventStateToWait) != 0)
                {
                    *pCurrentEventState = *pHandleToEventState;
                    *pHandleToEventState = NO_STATE;
                    event_signaled = 1;
                    phOsalNfc_ProduceSemaphore(hSemaphore);
                } else
                {
                    phOsalNfc_ProduceSemaphore(hSemaphore);
                    event_signaled = 0;
                    phOsalNfc_Suspend(5);
                }
            } else
            {
                command_status = event_status;
                break;
            }

        } while (event_signaled == 0);
    } else
    {
        command_status = (PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER));
    }
    return command_status;
}

phcsBfl_Status_t ConfigureTargetCommunicationUnit(phJal_sBFLUpperFrame_t *psBflFrame)
{
    phcsBfl_Status_t bfl_status = PH_ERR_BFL_SUCCESS;

    phJal_Trace(NULL, "ConfigureTargetCommunicationUnit");

    psBflFrame->RcRegCtlModifyParam.address = PHCS_BFL_JREG_TXAUTO;
    psBflFrame->RcRegCtlModifyParam.mask    =
        PHCS_BFL_JBIT_AUTORFOFF | PHCS_BFL_JBIT_CAON |
        PHCS_BFL_JBIT_TX2RFAUTOEN | PHCS_BFL_JBIT_TX1RFAUTOEN;
    psBflFrame->RcRegCtlModifyParam.set     = 0;
    bfl_status = psBflFrame->RcRegCtl->ModifyRegister(&psBflFrame->RcRegCtlModifyParam);

    /* Mantis ID:503 - Fixed: Go to idle mode in order to abort any active command. */
    psBflFrame->RcRegCtlSetRegParam.address  = PHCS_BFL_JREG_COMMAND;
    psBflFrame->RcRegCtlSetRegParam.reg_data = PHCS_BFL_JCMD_IDLE;
    bfl_status = psBflFrame->RcRegCtl->SetRegister(&psBflFrame->RcRegCtlSetRegParam);

    if (bfl_status == PH_ERR_BFL_SUCCESS)
    {
        psBflFrame->RcRegCtlModifyParam.address = PHCS_BFL_JREG_TXCONTROL;
        psBflFrame->RcRegCtlModifyParam.mask    = PHCS_BFL_JBIT_TX2RFEN | PHCS_BFL_JBIT_TX1RFEN;
        psBflFrame->RcRegCtlModifyParam.set     = 0;
        bfl_status = psBflFrame->RcRegCtl->ModifyRegister(&psBflFrame->RcRegCtlModifyParam);

        if (bfl_status == PH_ERR_BFL_SUCCESS)
        {
            psBflFrame->RcRegCtlModifyParam.address = PHCS_BFL_JREG_MODE;
            psBflFrame->RcRegCtlModifyParam.mask    = PHCS_BFL_JBIT_DETECTSYNC;
            psBflFrame->RcRegCtlModifyParam.set     = 0;
            bfl_status = psBflFrame->RcRegCtl->ModifyRegister(&psBflFrame->RcRegCtlModifyParam);

            /* Mantis ID:503 - Fixed: Reset FIFO Level: */
            psBflFrame->RcRegCtlSetRegParam.address  = PHCS_BFL_JREG_FIFOLEVEL;
            psBflFrame->RcRegCtlSetRegParam.reg_data = PHCS_BFL_JBIT_FLUSHBUFFER;
            psBflFrame->RcRegCtl->SetRegister(&psBflFrame->RcRegCtlSetRegParam);

            /* Mantis ID:503 - Fixed: Reset all pending interrupts to clear event history: */
            psBflFrame->RcRegCtlSetRegParam.address  = PHCS_BFL_JREG_COMMIRQ;
            psBflFrame->RcRegCtlSetRegParam.reg_data = 0x7F;
            psBflFrame->RcRegCtl->SetRegister(&psBflFrame->RcRegCtlSetRegParam);

            if (bfl_status == PH_ERR_BFL_SUCCESS)
            {
                psBflFrame->RcRegCtlSetRegParam.address  = PHCS_BFL_JREG_COMMAND;
                psBflFrame->RcRegCtlSetRegParam.reg_data = PHCS_BFL_JCMD_AUTOCOLL;
                bfl_status = psBflFrame->RcRegCtl->SetRegister(&psBflFrame->RcRegCtlSetRegParam);

                if (bfl_status == PH_ERR_BFL_SUCCESS)
                {
                    psBflFrame->RcRegCtlModifyParam.address = PHCS_BFL_JREG_TXAUTO;
                    psBflFrame->RcRegCtlModifyParam.mask =
                        PHCS_BFL_JBIT_AUTORFOFF | PHCS_BFL_JBIT_CAON |
                        PHCS_BFL_JBIT_TX2RFAUTOEN | PHCS_BFL_JBIT_TX1RFAUTOEN;
                    psBflFrame->RcRegCtlModifyParam.set = 1;
                    bfl_status =
                        psBflFrame->RcRegCtl->ModifyRegister(&psBflFrame->RcRegCtlModifyParam);
                }
            }
        }
    }
    /* RegDumpFile("Dmp_TargetStdConfig.txt", "a","target configuration", 0, 1, psBflFrame->RcRegCtl); */
    return bfl_status;
}

NFCSTATUS phJal_SetupTargetReceptionHandling(phJal_sBFLUpperFrame_t *psBflFrame)
{
    NFCSTATUS  command_status = NFCSTATUS_SUCCESS;

    phJal_Trace(psBflFrame->ReceptionHandlingParams.psHwReference, "phJal_SetupTargetReceptionHandling");

    if (psBflFrame->TargetLoopItemsCreated == 0)
    {
        /* Change of WFSPE() function in StartTargetMode - Addition of Handle */
        command_status = phOsalNfc_CreateSemaphore(&psBflFrame->ReceptionInfoHandle_STM, 0, 1);
        if (command_status == NFCSTATUS_SUCCESS)
        {
            command_status = phOsalNfc_CreateSemaphore(&psBflFrame->ReceptionInfoHandle_RHT, 1, 1);
            if (command_status == NFCSTATUS_SUCCESS)
            {
                command_status = phOsalNfc_CreateSemaphore(&psBflFrame->TargetInfoHandle, 1, 1);

                if (command_status == NFCSTATUS_SUCCESS)
                {
                    psBflFrame->ReceptionHandlingParams.TerminationState = NO_STATE;
                    command_status = phOsalNfc_CreateSemaphore(&psBflFrame->TerminateReceptionHandle, 1, 1);

                    if (command_status == NFCSTATUS_SUCCESS)
                    {
                        psBflFrame->ReceptionHandlingParams.UserTimeoutState = NO_STATE;
                        command_status = phOsalNfc_CreateSemaphore(&psBflFrame->UserTimeoutHandle, 1, 1);

                        if (command_status == NFCSTATUS_SUCCESS)
                        {
                            command_status = phOsalNfc_CreateSemaphore(&psBflFrame->TargetModeRunningHandle, 1, 1);

                            if (command_status == NFCSTATUS_SUCCESS)
                            {
                                command_status =
                                    phOsalNfc_CreateThread(&psBflFrame->ReceptionThreadHandle,
                                        phJal_ReceptionHandling, &psBflFrame->ReceptionHandlingParams);

                                if (command_status == NFCSTATUS_SUCCESS)
                                {
                                    /*Mantis #586: Target sync for exit. */
                                    //command_status = phOsalNfc_CreateSemaphore(&psBflFrame->TargetModeFunctionReturnHandle, 1, 1); // Peer to Peer BugFix: Semaphore removed

                                    if (command_status == NFCSTATUS_SUCCESS)
                                    {
                                        command_status = phOsalNfc_CreateSemaphore(&psBflFrame->TargetModeRestartCleanupSync, 1, 1);
                                        if (command_status == NFCSTATUS_SUCCESS)
                                        {
                                            /* OK, done with all. */
                                        } else
                                        {
                                            psBflFrame->TargetModeRestartCleanupSync = NULL;
                                        }
                                    } else
                                    {
                                        // psBflFrame->TargetModeFunctionReturnHandle = NULL; // Peer to Peer BugFix: Semaphore removed
                                    }
                                } else
                                {
                                    psBflFrame->ReceptionThreadHandle = NULL;
                                }
                            } else
                            {
                                psBflFrame->TargetModeRunningHandle = NULL;
                            }
                        } else
                        {
                            psBflFrame->UserTimeoutHandle = NULL;
                        }
                    } else
                    {
                        psBflFrame->TerminateReceptionHandle = NULL;
                    }
                } else
                {
                    psBflFrame->TargetInfoHandle = NULL;
                }
            } else
            {
                psBflFrame->ReceptionInfoHandle_RHT = NULL;
            }
        } else
        {
            psBflFrame->ReceptionInfoHandle_STM = NULL;
        }
        /* Events created, or alt least attempted to .... */
        psBflFrame->TargetLoopItemsCreated = 1;
    } else
    {
        /* Items are already created! */
    }

    /* Close all if one+ error(s) occurred: */
    if (command_status == NFCSTATUS_SUCCESS)
    {

        /* OK, no action. */
    } else
    {
        /* Not successful: Removing items: */

        phJal_CloseTargetReceptionHandling(psBflFrame);
    }

    return command_status;
}

void phJal_CloseTargetReceptionHandling(phJal_sBFLUpperFrame_t *psBflFrame)
{
    void    *aux_handle = NULL;
    uint8_t  handle_nr  = 0;
    uint8_t  finished   = 0;


    phJal_Trace(psBflFrame->ReceptionHandlingParams.psHwReference, "phJal_CloseTargetReceptionHandling");

    while (finished == 0)
    {
        switch (handle_nr)
        {
            case 0: aux_handle = psBflFrame->ReceptionInfoHandle_STM; break; // Change of WFSPE() function in StartTargetMode - Addition of Handle
            case 1: aux_handle = psBflFrame->ReceptionInfoHandle_RHT; break; // Change of WFSPE() function in StartTargetMode - Addition of Handle
            case 2: aux_handle = psBflFrame->TargetInfoHandle; break;
            case 3: aux_handle = psBflFrame->TerminateReceptionHandle; break;
            case 4: aux_handle = psBflFrame->UserTimeoutHandle; break;
            case 5: aux_handle = psBflFrame->TargetModeRunningHandle; break;
            case 6: aux_handle = psBflFrame->ReceptionThreadHandle; break;
            case 7: break;//aux_handle = psBflFrame->TargetModeFunctionReturnHandle; break; // Peer to Peer BugFix: Semaphore removed
            case 8: aux_handle = psBflFrame->TargetModeRestartCleanupSync; break;
                
            default:
                finished = 1;
                break;
        }
        if ((aux_handle != NULL) && (finished == 0))
        {
            phOsalNfc_CloseHandle(aux_handle);
            aux_handle = NULL;
        }
        handle_nr++;
    }
    psBflFrame->TargetLoopItemsCreated = 0;
}


NFCSTATUS phJal_SetupTargetDepHandlingEvents(phJal_sBFLUpperFrame_t *psBflFrame)
{
    NFCSTATUS command_status = NFCSTATUS_SUCCESS;

    phJal_Trace(NULL, "phJal_SetupTargetDepHandlingEvents");

    psBflFrame->ReceptionHandlingParams.ReceptionEndpointToUserState = NO_STATE;
    psBflFrame->ReceptionHandlingParams.ReceptionUserToEndpointState = NO_STATE;
    psBflFrame->ReceptionHandlingParams.TransmissionEndpointToUserState = NO_STATE;
    psBflFrame->ReceptionHandlingParams.TransmissionUserToEndpointState = NO_STATE;
    psBflFrame->ReceptionHandlingParams.ReceptionUserReadyForMetaChaining = NO_STATE;

    if (command_status == NFCSTATUS_SUCCESS)
    {
        /* Change Of WFSPE function - TransmissionEndpointToUserHandle */
        command_status =
            phOsalNfc_CreateSemaphore(&psBflFrame->TransmissionEndpointToUserHandle, 0, 1);
        if (command_status == NFCSTATUS_SUCCESS)
        {
            command_status =
                phOsalNfc_CreateSemaphore(&psBflFrame->TransmissionUserToEndpointHandle, 1, 1);
            if (command_status == NFCSTATUS_SUCCESS)
            {
                command_status =
                    phOsalNfc_CreateSemaphore(&psBflFrame->ReceptionEndpointToUserHandle, 0, 1); // WFSPE Function Change
                if (command_status == NFCSTATUS_SUCCESS)
                {
                    command_status =
                        phOsalNfc_CreateSemaphore(&psBflFrame->ReceptionUserToEndpointHandle, 1, 1);

                    if (command_status == NFCSTATUS_SUCCESS)
                    {
                        command_status =
                            phOsalNfc_CreateSemaphore(&psBflFrame->ReceptionUserReadyForMetaChaining, 1, 1);
                    }
                    else
                    {
                        phOsalNfc_CloseHandle(psBflFrame->TransmissionEndpointToUserHandle);
                        phOsalNfc_CloseHandle(psBflFrame->TransmissionUserToEndpointHandle);
                        phOsalNfc_CloseHandle(psBflFrame->ReceptionEndpointToUserHandle);
                    }
                }
                else
                {
                    phOsalNfc_CloseHandle(psBflFrame->TransmissionEndpointToUserHandle);
                    phOsalNfc_CloseHandle(psBflFrame->TransmissionUserToEndpointHandle);
                }
            }
            else
            {
                phOsalNfc_CloseHandle(psBflFrame->TransmissionEndpointToUserHandle);
            }
        }

    }
    return command_status;
}

void phJal_CloseTargetDepHandlingEvents(phJal_sBFLUpperFrame_t *psBflFrame)
{
    phJal_Trace(NULL, "phJal_CloseTargetDepHandlingEvents");

    if (psBflFrame->ReceptionEndpointToUserHandle != NULL)
    {
        phOsalNfc_CloseHandle(psBflFrame->ReceptionEndpointToUserHandle);
        psBflFrame->ReceptionEndpointToUserHandle = NULL;
    }

    if (psBflFrame->ReceptionUserToEndpointHandle != NULL)
    {
        phOsalNfc_CloseHandle(psBflFrame->ReceptionUserToEndpointHandle);
        psBflFrame->ReceptionUserToEndpointHandle = NULL;
    }

    if (psBflFrame->ReceptionUserReadyForMetaChaining != NULL)
    {
        phOsalNfc_CloseHandle(psBflFrame->ReceptionUserReadyForMetaChaining);
        psBflFrame->ReceptionUserReadyForMetaChaining = NULL;
    }

    if (psBflFrame->TransmissionEndpointToUserHandle != NULL)
    {
        phOsalNfc_CloseHandle(psBflFrame->TransmissionEndpointToUserHandle);
        psBflFrame->TransmissionEndpointToUserHandle = NULL;
    }

    if (psBflFrame->TransmissionUserToEndpointHandle != NULL)
    {
        phOsalNfc_CloseHandle(psBflFrame->TransmissionUserToEndpointHandle);
        psBflFrame->TransmissionUserToEndpointHandle = NULL;
    }
}

#endif
