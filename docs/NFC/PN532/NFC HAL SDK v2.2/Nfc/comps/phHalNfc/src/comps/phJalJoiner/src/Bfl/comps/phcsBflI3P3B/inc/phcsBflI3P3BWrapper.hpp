/* /////////////////////////////////////////////////////////////////////////////////////////////////
//                     Copyright (c) Philips Semiconductors
//
//                       (C)PHILIPS Electronics N.V.2004
//         All rights are reserved. Reproduction in whole or in part is
//        prohibited without the written consent of the copyright owner.
//    Philips reserves the right to make changes without notice at any time.
//   Philips makes no warranty, expressed, implied or statutory, including but
//   not limited to any implied warranty of merchantability or fitness for any
//  particular purpose, or that the use will not infringe any third party patent,
//   copyright or trademark. Philips must not be liable for any loss or damage
//                            arising from its use.
///////////////////////////////////////////////////////////////////////////////////////////////// */

/*! 
 * \file phcsBflI3P3BWrapper.hpp
 *
 * Project: Object Oriented Library Framework ISO14443_3B Component.
 *
 *  Source: phcsBflI3P3BWrapper.hpp
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Date: Thu Sep 27 09:39:31 2007 $
 *
 * Comment:
 *  C++ wrapper for ISO14443_3B.
 *
 * History:
 *  MHa: Generated 17. Jannuary 2005
 *  MHa: Migrated to MoReUse September 2005
 *
 */

#ifndef PHCSBFLI3P3BWRAPPER_HPP
#define PHCSBFLI3P3BWRAPPER_HPP

#include <phcsBflStatus.h>
#include <phcsBflIoWrapper.hpp>


// Include the meta-header file for all hardware variants supported by the C++ interface:
extern "C"
{
    #include <phcsBflI3P3B.h>
}

namespace phcs_BFL
{
    
/*! \ingroup iso14443_3
 *  Functionality (activation/deactivation) according to ISO 14443-3 Type B specification
 *  (abstract class, used for the C++ interface).
 */
class phcsBfl_I3P3B
{
    public:

        virtual void Initialise(class phcsBfl_Io      *p_lower,
                                uint8_t              initiator__not_target,
                                uint8_t             *p_trxbuffer,
                                uint16_t             trxbuffersize)                         = 0;
        virtual phcsBfl_Status_t RequestB(phcsBflI3P3B_RequestParam_t *request_b_param)         = 0;
        virtual phcsBfl_Status_t SlotMarker(phcsBflI3P3B_SlotmarkerParam_t *slotmarker_param)   = 0;
        virtual phcsBfl_Status_t Attrib(phcsBflI3P3B_AttribParam_t *attrib_param)               = 0;
        virtual phcsBfl_Status_t HaltB(phcsBflI3P3B_HaltParam_t *halt_b_param)                  = 0;
                      
        /*! Lower edge interface: Not to be redefined in the derived class. */
        class phcsBfl_Io *mp_Lower;
};


/*! \ingroup iso14443_3
 *  This is the GLUE class the C-kernel phcsBfl_I3P3B "sees" when calling down the stack. The glue class
 *  represents the C-API of the lower layer (RC Reg. Ctl.). 
 *
 */
class phcsBfl_I3P3BGlue
{
    public:
        phcsBfl_I3P3BGlue(void);
        ~phcsBfl_I3P3BGlue(void);
        
        //! Static functions able to call into C++ again.
        //@{
        static phcsBfl_Status_t Transceive(phcsBflIo_TransceiveParam_t *transceive_param);
        //@}

        //! Structure accessible from the upper object's pure C-kernel.
        phcsBflIo_t m_GlueStruct;
};


/*! \ingroup iso14443_3 
 * This is the main interface of phcsBfl_I3P3B. The class is a template because it cannot be foreseen
 * which type of C-kernel to wrap around. Depending on the implementation of this kernel, the
 * type of the internal C-member structure changes (avoid multiple definitions of the same structure
 * name for different structures when more than one type of functionality is used). The type of the
 * MEMBERS (in kernel's .h file) structure is the one to specify when creating an instance
 * of this class. */
class phcsBflI3P3B_Wrapper : public phcsBfl_I3P3B
{
    public:
        phcsBflI3P3B_Wrapper(void);
        ~phcsBflI3P3B_Wrapper(void);

        /*!
        * \ingroup iso14443_3
        * \param  c_iso_14443_3B_initialise \n [in] 
        *                  Pointer to the Initialise function of the type of C-kernel (for
        *                  each hardware variant an own kernel must exist) to use with the
        *                  current wrapper instance.
        * \param *p_lower \n [in] 
        *                  Pointer to the C++ object of the underlying layer.
        * \param *initiator__not_target \n [in] 
        *                  Specifier for operation mode (1 = Initiator, 0 = Target).
        *
        * \note The alternative Constructor does setup required by the Component but not done
        *       by the default constructor.
        *  
        */
        phcsBflI3P3B_Wrapper(class phcsBfl_Io     *p_lower,
                                   uint8_t               initiator__not_target,
                                   uint8_t              *p_trxbuffer,
                                   uint16_t              trxbuffersize);

       /*!
        * \ingroup iso14443_3
        * \param  c_iso_14443_3B_initialise \n [in] 
        *                  Pointer to the Initialise function of the type of C-kernel (for
        *                  each hardware variant an own kernel must exist) to use with the
        *                  current wrapper instance.
        * \param *p_lower \n [in] 
        *                  Pointer to the C++ object of the underlying layer.
        * \param *initiator__not_target \n [in] 
        *                  Specifier for operation mode (1 = Initiator, 0 = Target).
        *
        * \note The Initialise method does setup required by the Component but not done
        *       by the default constructor.
        *  
        */
        void Initialise(class phcsBfl_Io     *p_lower,
                        uint8_t             initiator__not_target,
                        uint8_t            *p_trxbuffer,
                        uint16_t            trxbuffersize);
                        
                                       
        /*! \name Component Interfaces
         * C++ wrapper of the C-kernel.
         */
        //@{
        phcsBfl_Status_t RequestB(phcsBflI3P3B_RequestParam_t *request_b_param);
        phcsBfl_Status_t SlotMarker(phcsBflI3P3B_SlotmarkerParam_t  *slotmarker_param);
        phcsBfl_Status_t Attrib(phcsBflI3P3B_AttribParam_t *attrib_param);
        phcsBfl_Status_t HaltB(phcsBflI3P3B_HaltParam_t *halt_b_param);
        //@}

        /*! Protocol parameters retrieval function. */
        phcsBflI3P4_ProtParam_t *ProtocolParameters(void);

        /*! Constructor status: This status gives information about whether the object has been
         *  allocated/configured correctly or not. It is a good idea to check this status (verify
         *  that it is set to PH_ERR_BFL_SUCCESS) before proceeding.
         */
        phcsBfl_Status_t                   m_ConstructorStatus;

        /*! Protocol parameters structure filled by the initialisation of a PICC. Hand this
         *  structure over to the phcsBfl_I3P4 protocol communication class object.
         */
        phcsBflI3P4_ProtParam_t     m_ProtocolParams;

        phcsBflI3P3B_Params_t       m_CommunicationParams;

        phcsBflI3P3B_t              m_Interface;            /*!< C-kernel instance. */
        

    protected:
        class phcsBfl_I3P3BGlue m_I3_3BGlue;
};

}   /* phcs_BFL */

#endif /* PHCSBFLI3P3BWRAPPER_HPP */

/* E.O.F. */
