/*
 *         Copyright (c), NXP Semiconductors
 *
 *                       (C)NXP Electronics N.V.2004
 *         All rights are reserved. Reproduction in whole or in part is
 *        prohibited without the written consent of the copyright owner.
 *    NXP reserves the right to make changes without notice at any time.
 *   NXP makes no warranty, expressed, implied or statutory, including but
 *   not limited to any implied warranty of merchantability or fitness for any
 *  particular purpose, or that the use will not infringe any third party patent,
 *   copyright or trademark. NXP must not be liable for any loss or damage
 *                            arising from its use.
 *
 */


/*!
 * \file phJalTransceiveFunctions.c
 * \brief JAL Transceive Functions
 *
 *
 * Project: NFC MW / HAL
 *
 * $Date: Thu Sep 27 09:38:43 2007 $ 
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Aliases: SDK_HAL_2.2,SDK_HAL_2.2_071008,NFC_FRI_SDK_1.0_071031 $
 *
 */

#ifdef PHHAL_VERSION_ACQUISITION
    #define PHJALTRANSCEIVEFUNCTIONS_SOURCE_FILEREVISION "$Revision: 1.1 $"
    #define PHJALTRANSCEIVEFUNCTIONS_SOURCE_FILEALIASES  "$Aliases: SDK_HAL_2.2,SDK_HAL_2.2_071008,NFC_FRI_SDK_1.0_071031 $"
#else /* PHHAL_VERSION_ACQUISITION */

#include <memory.h>

#include <phcsBflNfc.h>
#include <phcsBflMfRd.h>
#include <phcsBflI3P4.h>

#include <phNfcHalTypes.h>
#include <phNfcStatus.h>

#include <phJalTransceiveFunctions.h>
#include <phJalConnectionFunctions.h>
#include <phJalJoinerBflFrame.h>
#include <phJalJoinerAux.h>
#include <phJalDebugReportFunctions.h>
#include <phcsBflOpCtl_Hw1Ordinals.h>

#define MIFARE_SNR_LENGTH       4

#include <stdio.h>


NFCSTATUS phJal_DataExchangeProtocolRequest(phHal_sHwReference_t            *psHwReference,  
                                            phHal_sRemoteDevInformation_t   *psRemoteDevInfo,
                                            phHal_sDepAdditionalInfo_t      *psDepAdditionalInfo,  
                                            uint8_t                         *pSendBuf,  
                                            uint16_t                        SendLength,  
                                            uint8_t                         *pRecvBuf,  
                                            uint16_t                        *pRecvLength)
{
    NFCSTATUS                           command_status = NFCSTATUS_SUCCESS;
    phcsBfl_Status_t                    bfl_status = PH_ERR_BFL_SUCCESS;
    phJal_ProtocolEntry_t               *protocol_entry = NULL;
    phJal_sBFLUpperFrame_t              *bfl_frame = NULL;
    phcsBflNfc_InitiatorDepReqParam_t   dep_request_param = {0};
    phJal_NfcIProtocol_t                *protocol = NULL;
    
    if (psHwReference != NULL || 
        psRemoteDevInfo != NULL ||
        psDepAdditionalInfo != NULL ||
        pSendBuf != NULL ||
        pRecvBuf != NULL ||
        pRecvLength != NULL)
    {
        phJal_Trace(psHwReference, "phJal_DataExchangeProtocolRequest");
        
        bfl_frame = (phJal_sBFLUpperFrame_t*)psHwReference->pBoardDriver;

        if (bfl_frame != NULL)
        {                    
            protocol_entry =
                phJal_GetProtocolEntryForLogicalHandle(bfl_frame->NfcInitiator_Entries,
                    psRemoteDevInfo->hLogHandle);

            if (protocol_entry != NULL)
            {
                protocol = (phJal_NfcIProtocol_t*)(protocol_entry->Protocol);
                
                if (psDepAdditionalInfo->DepFlags.NADPresent == 1)
                {
                    dep_request_param.nad = &psDepAdditionalInfo->NAD; 
                }
                else
                {
                    dep_request_param.nad = NULL;
                }

                dep_request_param.self = protocol->Protocol;
                dep_request_param.input_buffer = pSendBuf;
                dep_request_param.input_buffer_size = SendLength;
                dep_request_param.output_buffer = pRecvBuf;
                dep_request_param.output_buffer_size = *pRecvLength;
                dep_request_param.flags = 
                    (psDepAdditionalInfo->DepFlags.MetaChaining == 1)? 
                        PHCS_BFLNFC_INITIATOR_CONT_CHAIN : 0;
              
                reportDbg1("transceive DEP:\t sending %d bytes\n", SendLength);    
                bfl_status = HandleDEPAndWTX(psHwReference, protocol, &dep_request_param, pRecvLength);
                
                if (bfl_status == PH_ERR_BFL_SUCCESS)
                {
                    *pRecvLength = dep_request_param.output_buffer_size;
                    psDepAdditionalInfo->DepFlags.MetaChaining = 0;
                    command_status = NFCSTATUS_SUCCESS;
                } else if ((bfl_status & 0x00FF) == PH_ERR_BFL_USERBUFFER_FULL)
                {
                    psDepAdditionalInfo->DepFlags.MetaChaining = 1;
                    *pRecvLength = dep_request_param.output_buffer_size;
                    command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_MORE_INFORMATION);
                } else
                {
                    command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_RF_TIMEOUT);
                }
            }
            else
            {
                command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_DEVICE);
            }
        }
        else
        {
            command_status = (PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER));
        }
    }
    else
    {
        command_status = (PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER));
    }
    return command_status;
}

phcsBfl_Status_t HandleDEPAndWTX(phHal_sHwReference_t                *psHwReference,
                                 phJal_NfcIProtocol_t                *psProtocol,
                                 phcsBflNfc_InitiatorDepReqParam_t   *psDepRequestParam,
                                 uint16_t                            *pRecvLength)
{
    phcsBfl_Status_t                    bfl_status = PH_ERR_BFL_SUCCESS;
    phJal_sBFLUpperFrame_t              *bfl_frame = NULL;
    phcsBflNfc_InitiatorToxReqParam_t   tox_param = {0};
    uint32_t                            requested_time_extension = 0;
    phcsBflNfc_Initiator_t              *nfc_protocol = psProtocol->Protocol;

    phJal_Trace(psHwReference, "HandleDEPAndWTX");

    bfl_frame = (phJal_sBFLUpperFrame_t*)psHwReference->pBoardDriver;
    tox_param.self = bfl_frame->NfcI;
    reportDbg1("transceive:\t Current RWT: %dus\n", psProtocol->ResponseWaitingTime);
    bfl_status = SetTimeOut(bfl_frame->RcRegCtl, &bfl_frame->RcOpCtl,
            psProtocol->ResponseWaitingTime, USED_PRESCALER_VALUE);

    if (bfl_status == PH_ERR_BFL_SUCCESS)
    {
        bfl_status = nfc_protocol->DepRequest(psDepRequestParam);
        reportDbg1("transceive:\t DEP Return Status: 0x%02X\n", bfl_status);

        while (((bfl_status & ~PH_ERR_BFL_COMP_MASK) == PH_ERR_BFL_TARGET_RESET_TOX) || 
               ((bfl_status & ~PH_ERR_BFL_COMP_MASK) == PH_ERR_BFL_TARGET_SET_TOX))
        {
            if((bfl_status & ~PH_ERR_BFL_COMP_MASK) == PH_ERR_BFL_TARGET_SET_TOX)
            {
                reportDbg1("transceive:\t requested WTX Multiplier: %d\n", 
                    nfc_protocol->RequestedToxValue(&tox_param));
                requested_time_extension = psProtocol->ResponseWaitingTime *
                    nfc_protocol->RequestedToxValue(&tox_param);            
                reportDbg1("transceive:\t WTX: %dus\n", requested_time_extension);

                bfl_status = SetTimeOut(bfl_frame->RcRegCtl, &bfl_frame->RcOpCtl, 
                        requested_time_extension, 
                        USED_PRESCALER_VALUE);
                reportDbg1("transceive:\t WTX: new timeout set with status 0x%02X\n", bfl_status);

                if (bfl_status == PH_ERR_BFL_SUCCESS)
                {
                    psDepRequestParam->output_buffer_size = *pRecvLength;
                    bfl_status = nfc_protocol->DepRequest(psDepRequestParam);
                }
            }  
    		
            else if ((bfl_status & ~PH_ERR_BFL_COMP_MASK) == PH_ERR_BFL_TARGET_RESET_TOX)
            {
                reportDbg0("transceive:\t DEP: Resetting TOX\n");
			    bfl_status = SetTimeOut(bfl_frame->RcRegCtl, &bfl_frame->RcOpCtl,
                        psProtocol->ResponseWaitingTime, USED_PRESCALER_VALUE);

			    psDepRequestParam->output_buffer_size = *pRecvLength;
                if (bfl_status == PH_ERR_BFL_SUCCESS)
                {
                    psDepRequestParam->output_buffer_size = *pRecvLength;
                    bfl_status = nfc_protocol->DepRequest(psDepRequestParam);
                }
            } 
        }
    }
    return bfl_status;
}

NFCSTATUS phJal_ParameterSelectionRequest(phHal_sHwReference_t          *psHwReference,  
                                        phHal_sRemoteDevInformation_t   *psRemoteDevInfo,  
                                        uint8_t                         *pSendBuf,  
                                        uint16_t                        SendLength)
{
    NFCSTATUS               command_status = NFCSTATUS_SUCCESS;
    phcsBfl_Status_t        bfl_status = PH_ERR_BFL_SUCCESS;
    phJal_ProtocolEntry_t   *protocol_entry = NULL;
    phJal_sBFLUpperFrame_t  *bfl_frame = NULL;
    uint8_t                 brs_value = 0;
    uint8_t                 tx_speed = PHCS_BFLOPCTL_VAL_RF106K;
    uint8_t                 rx_speed = PHCS_BFLOPCTL_VAL_RF106K;
    NfcSpeeds               speed_for_new_opmode = RF106K;
    
    phcsBflNfc_InitiatorPslReqParam_t      psl_request_param = {0};
    phcsBflNfc_Initiator_t                 *protocol = NULL;
    
    if ((psHwReference != NULL) || 
        (psRemoteDevInfo != NULL) ||
        (pSendBuf != NULL) ||
        (SendLength == PSL_REQUIRED_BUFFER_LENGTH))
    {
        phJal_Trace(psHwReference, "phJal_ParameterSelectionRequest");

        bfl_frame = (phJal_sBFLUpperFrame_t*)psHwReference->pBoardDriver;

        if (bfl_frame != NULL)
        {
             protocol_entry =
                phJal_GetProtocolEntryForLogicalHandle(bfl_frame->NfcInitiator_Entries,
                psRemoteDevInfo->hLogHandle);

            if (protocol_entry != NULL)
            {
                protocol = ((phJal_NfcIProtocol_t*)(protocol_entry->Protocol))->Protocol;
                                
                command_status = NFCSTATUS_SUCCESS;
                switch(pSendBuf[BRIT_VALUE_POS])
                {
                    case 0x0:
                        brs_value = PHCS_BFLNFCCOMMON_PSL_BRS_DS_1;
                        rx_speed = PHCS_BFLOPCTL_VAL_RF106K;
                        speed_for_new_opmode = RF106K;
                        break;
                    case 0x01:
                        brs_value = PHCS_BFLNFCCOMMON_PSL_BRS_DS_2;
                        rx_speed = PHCS_BFLOPCTL_VAL_RF212K;
                        speed_for_new_opmode = RF212K;
                        break;
                    case 0x02:
                        brs_value = PHCS_BFLNFCCOMMON_PSL_BRS_DS_4;
                        rx_speed = PHCS_BFLOPCTL_VAL_RF424K;
                        speed_for_new_opmode = RF424K;
                        break;
                    default:
                        command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER);
                        break;
                }

                if (command_status == NFCSTATUS_SUCCESS)
                {
                    switch(pSendBuf[BRTI_VALUE_POS])
                    {
                        case 0x0:
                            brs_value |= PHCS_BFLNFCCOMMON_PSL_BRS_DR_1;
                            tx_speed = PHCS_BFLOPCTL_VAL_RF106K;                            
                            break;
                        case 0x01:
                            brs_value |= PHCS_BFLNFCCOMMON_PSL_BRS_DR_2;
                            tx_speed = PHCS_BFLOPCTL_VAL_RF212K;
                            break;
                        case 0x02:
                            brs_value |= PHCS_BFLNFCCOMMON_PSL_BRS_DR_4;
                            tx_speed = PHCS_BFLOPCTL_VAL_RF424K;
                            break;
                        default:
                            command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER);
                            break;
                    }
                    reportDbg1("transceive:\t new tx speed 0x%02X\n", tx_speed);

                    if (command_status == NFCSTATUS_SUCCESS)
                    {
                        psl_request_param.self = protocol;
                        psl_request_param.BRS = brs_value;
                        psl_request_param.FSL = FSL_VALUE;

                        bfl_status = protocol->PslRequest(&psl_request_param);
                        if (bfl_status == PH_ERR_BFL_SUCCESS)
                        {                                      
                            switch(bfl_frame->CurrentInitiatorMode)
                            {
                                case PASSIVE_MODE:
                                    reportDbg0("transceive:\t Configure Passive Mode after PSL\n");
                                    if (tx_speed == PHCS_BFLOPCTL_VAL_RF106K)
                                    {
                                        bfl_frame->RcOpCtlAttribParam.attrtype_ordinal = 
                                            PHCS_BFLOPCTL_ATTR_MFRD_A;
                                    }
                                    else
                                    {
                                        bfl_frame->RcOpCtlAttribParam.attrtype_ordinal = 
                                            PHCS_BFLOPCTL_ATTR_PAS_POL;                           
                                    }
                                    break;
                                case ACTIVE_MODE:
                                    reportDbg0("transceive:\t Configure Active Mode after PSL\n");
                                    bfl_frame->RcOpCtlAttribParam.attrtype_ordinal = PHCS_BFLOPCTL_ATTR_ACTIVE;
                                    break;
                                default:
                                    bfl_status = PH_ERR_BFL_OTHER_ERROR;
                                    break;
                            }
                         
                            if (bfl_status == PH_ERR_BFL_SUCCESS)
                            {
                                bfl_frame->RcOpCtlAttribParam.param_a = tx_speed;
                                bfl_frame->RcOpCtlAttribParam.param_b = rx_speed;
                                bfl_status = bfl_frame->RcOpCtl.SetAttrib(&bfl_frame->RcOpCtlAttribParam);
                                if (bfl_status == PH_ERR_BFL_SUCCESS)
                                {
                                    command_status = 
                                        phJal_ConfigureHardwareForNfc106kProtocol(psHwReference);
                                }                              
                            }
                            else
                            {
                                command_status = (PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER));
                            }
                        }
                        else
                        {
                            command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_RF_TIMEOUT); 
                        }
                    }                       
                }               
            }
            else
            {
                command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_DEVICE);
            }
        }
        else
        {
            command_status = (PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER));
        }
    }
    else
    {
        command_status = (PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER));
    }
    
    if (command_status == NFCSTATUS_SUCCESS)
    {
        command_status = 
            phHalNfc_SetNewNfcRemoteDeviceOpMode(psRemoteDevInfo, bfl_frame->CurrentInitiatorMode, speed_for_new_opmode); 
        reportDbg1("transceive:\t Opmode of Remote Device changed with status 0x%02X\n", command_status );
    }
    return command_status;
}

NFCSTATUS phHalNfc_SetNewNfcRemoteDeviceOpMode(phHal_sRemoteDevInformation_t *psRemoteDevInfo,
                                               NfcModes                      CurrentMode,
                                               NfcSpeeds                     NewSpeed)
{
    NFCSTATUS   command_status = NFCSTATUS_SUCCESS;

    phJal_Trace(NULL, "phHalNfc_SetNewNfcRemoteDeviceOpMode");
    
    if (psRemoteDevInfo != NULL)
    {
        reportDbg2("transceive:\t Opmode of Remote Device changed to mode 0x%02X and speed 0x%02X\n", CurrentMode, NewSpeed);

        switch(CurrentMode)
        {
            case PASSIVE_MODE:
                switch(NewSpeed)
                {
                    case RF106K:
                        psRemoteDevInfo->OpMode = phHal_eOpModesNfcPassive106;
                        break;
                    case RF212K:
                        psRemoteDevInfo->OpMode = phHal_eOpModesNfcPassive212;
                        break;
                    case RF424K:
                        psRemoteDevInfo->OpMode = phHal_eOpModesNfcPassive424;
                        break;
                }
                break;
            case ACTIVE_MODE:
                switch(NewSpeed)
                {
                    case RF106K:
                        psRemoteDevInfo->OpMode = phHal_eOpModesNfcActive106;
                        break;
                    case RF212K:
                        psRemoteDevInfo->OpMode = phHal_eOpModesNfcActive212;
                        break;
                    case RF424K:
                        psRemoteDevInfo->OpMode = phHal_eOpModesNfcActive424;
                        break;
                }
                break;
            default:
                command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER);
                break;
        }
    }
    else
    {
        command_status =  PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER);
    }
    return command_status;
}

NFCSTATUS phJal_TransceiveRawCommand(phHal_sHwReference_t           *psHwReference, 
                                    uint8_t                         *pSendBuf,  
                                    uint16_t                        SendLength,  
                                    uint8_t                         *pRecvBuf,  
                                    uint16_t                        *pRecvLength)
{
    NFCSTATUS                   command_status = NFCSTATUS_SUCCESS;
    phcsBfl_Status_t            bfl_status = PH_ERR_BFL_SUCCESS;
    phJal_sBFLUpperFrame_t      *bfl_frame = NULL;
    phcsBflIo_TransceiveParam_t transceive_param = {0};
    
    if ((psHwReference == NULL) ||
        (pSendBuf == NULL) ||
        (pRecvBuf == NULL) ||
        (pRecvLength == NULL))
    {
        command_status = (PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER));
    } else
    {      
        phJal_Trace(psHwReference, "phJal_TransceiveRawCommand");

        bfl_frame = (phJal_sBFLUpperFrame_t*)psHwReference->pBoardDriver;

        if (bfl_frame == NULL)
        {
            transceive_param.rx_buffer = pRecvBuf;
            transceive_param.rx_buffer_size = *pRecvLength;
            transceive_param.tx_buffer = pSendBuf;
            transceive_param.tx_buffer_size = SendLength;
            transceive_param.self = &bfl_frame->RcIo;

            bfl_status = bfl_frame->RcIo.Transceive(&transceive_param);

            if (bfl_status == PH_ERR_BFL_SUCCESS)
            {
                command_status = NFCSTATUS_SUCCESS;
            }
            else
            {
                command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_RF_TIMEOUT); 
            }
        }
        else
        {
            command_status = (PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER));
        }
    }

    return command_status;
}

//
//  Assuming sequence in pSendBuf to be <len> <cmd> <data>
//  <data> = <num_serv> <serv_list> <num_blocks> <block_list>
//
NFCSTATUS phHalNfc_TransceiveFelicaCommand(phHal_sHwReference_t           *psHwReference,
                                           phHal_sRemoteDevInformation_t  *psRemoteDevInfo,
                                           phHal_uCmdList_t                Cmd,
                                           uint8_t                        *pSendBuf,
                                           uint16_t                        SendLength,
                                           uint8_t                        *pRecvBuf,
                                           uint16_t                       *pRecvLength)
{
    NFCSTATUS                     command_status = NFCSTATUS_SUCCESS;
    phcsBfl_Status_t              bfl_status = PH_ERR_BFL_SUCCESS;
    phJal_sBFLUpperFrame_t       *bfl_frame = NULL;
    phcsBflFelRd_CommandParam_t   command_param = {0};
    uint8_t                       i = 0;
    uint8_t                       poll_resp[32];

    #define                       _update_cmd   0x08 //
    #define                       _check_cmd    0x06 //

    if (psHwReference == NULL || 
        psRemoteDevInfo == NULL ||
        pSendBuf == NULL ||
        pRecvBuf == NULL ||
        pRecvLength == NULL ||
        SendLength == 0)
    {
        *pRecvLength = 0;
        return (PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER));
    }

    phJal_Trace(psHwReference, "phHalNfc_TransceiveFelicaCommand");

    bfl_frame = (phJal_sBFLUpperFrame_t*)psHwReference->pBoardDriver;

    if (bfl_frame == NULL)
    {
        *pRecvLength = 0;
        return (PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER));
    }
    
    // Pre-load params with what info is common and available:
    i = 1;

    // Resp Code:
    poll_resp[i++] = psRemoteDevInfo->RemoteDevInfo.CardInfo212_424.Startup212_424.RespCode;
    // NFCID2t:
    memcpy(&poll_resp[i],
           psRemoteDevInfo->RemoteDevInfo.CardInfo212_424.Startup212_424.NFCID2t,
           FELICA_IDMM_LENGTH);
    i+= FELICA_IDMM_LENGTH;

    memcpy(&poll_resp[i],
           psRemoteDevInfo->RemoteDevInfo.CardInfo212_424.Startup212_424.PMm,
           FELICA_PMM_LENGTH);
    i+= FELICA_PMM_LENGTH;

    if (psRemoteDevInfo->RemoteDevInfo.CardInfo212_424.Startup212_424.SystemCodeAvailable != 0)
    {
        memcpy(&poll_resp[i],
               psRemoteDevInfo->RemoteDevInfo.CardInfo212_424.Startup212_424.SystemCode,
               FELICA_SYSTEM_CODE_LENGTH);
        i += FELICA_SYSTEM_CODE_LENGTH;
    }
    // Length:
    poll_resp[0] = i;

    /* Apply Timeout: */
    bfl_status = SetTimeOut(bfl_frame->RcRegCtl, &bfl_frame->RcOpCtl, FELICA_READ_WRITE_TIMEOUT_DELAY, USED_PRESCALER_VALUE);
    
    // Generic
    command_param.self      = &bfl_frame->FelRd;
    command_param.poll_resp = poll_resp;

    // transaction specific, for all commands    
    command_param.txLength = pSendBuf[0];
    
    switch (pSendBuf[1])
    {
        case _update_cmd:
            command_param.txBuffer = pSendBuf;
            command_param.rxBuffer = pRecvBuf;
            bfl_status = bfl_frame->FelRd.Update(&command_param);
            if ((bfl_status == PH_ERR_BFL_SUCCESS) && (*pRecvLength >= 2))
            {
                pRecvBuf[0] = command_param.flags[0];
                pRecvBuf[1] = command_param.flags[1];
                *pRecvLength = 2;
            }
            break;

        case _check_cmd:
            command_param.txBuffer = pSendBuf;
            command_param.rxBuffer = pRecvBuf;
            bfl_status = bfl_frame->FelRd.Check(&command_param);
            if (bfl_status == PH_ERR_BFL_SUCCESS)
            {
                memcpy(pRecvBuf, command_param.rxBuffer, (uint8_t)(16 * command_param.rxLength));
                *pRecvLength = command_param.rxLength;
            }
            break;

        default:
            *pRecvLength = 0;
            return (PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER));
            break;
    }

    if (bfl_status != PH_ERR_BFL_SUCCESS)
    {
        *pRecvLength = 0;
        switch (bfl_status & ~PH_ERR_BFL_COMP_MASK)
        {
            case PH_ERR_BFL_IO_TIMEOUT:
                command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_RF_TIMEOUT);
                break;
            case PH_ERR_BFL_BUF_2_SMALL:
                command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_BUFFER_TOO_SMALL);
                break;
            case PH_ERR_BFL_INVALID_PARAMETER:
                command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER);
                break;
            case PH_ERR_BFL_CRC_ERROR:
                command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_FORMAT);
                break;
            default:
                command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_CMD_ABORTED);
                break;
        }
    }
    
    return command_status;
}


NFCSTATUS phHalNfc_TransceiveMifareCommand(phHal_sHwReference_t            *psHwReference,
                                            phHal_sRemoteDevInformation_t  *psRemoteDevInfo,
                                            phHal_uCmdList_t                Cmd,
                                            uint8_t                        *pSendBuf,
                                            uint16_t                        SendLength,
                                            uint8_t                        *pRecvBuf,
                                            uint16_t                       *pRecvLength)
{
    NFCSTATUS                   command_status = NFCSTATUS_SUCCESS;
    phcsBfl_Status_t            bfl_status = PH_ERR_BFL_SUCCESS;
    phJal_sBFLUpperFrame_t      *bfl_frame = NULL;
    phcsBflMfRd_CommandParam_t  command_param = {0};
    phcsBflI3P3A_HaltAParam_t   halt_param;
    const uint8_t               MF_BLOCK_SIZE = 16;

    if (psHwReference == NULL || 
        psRemoteDevInfo == NULL ||
        pSendBuf == NULL ||
        pRecvBuf == NULL ||
        pRecvLength == NULL)
    {
        return (PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER));
    }

    phJal_Trace(psHwReference, "phHalNfc_TransceiveMifareCommand");

    bfl_frame = (phJal_sBFLUpperFrame_t*)psHwReference->pBoardDriver;

    if (bfl_frame == NULL)
    {
        return (PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER));
    }

    command_status = phJal_ReActivateIso144433BasedDevice(psHwReference, psRemoteDevInfo);
    if (command_status != NFCSTATUS_SUCCESS)
    {
        command_status = phJal_ReActivateIso144433BasedDevice(psHwReference, psRemoteDevInfo);
    }
    if (command_status == NFCSTATUS_SUCCESS)
    { 
        bfl_status = SetTimeOut(bfl_frame->RcRegCtl, &bfl_frame->RcOpCtl, 
            MIFARE_COMMAND_TIMEOUT, TIMER_DEFAULT_SETTINGS);
        
        if (bfl_status == PH_ERR_BFL_SUCCESS)
        {
            if (((psRemoteDevInfo->RemoteDevInfo.CardInfo106.Startup106.SelRes & 0x08) != 0) &&
                (Cmd.MfCmd != phHal_eMifareCmdListMifareAuthentA) &&
                (Cmd.MfCmd != phHal_eMifareCmdListMifareAuthentB))
            {
                if (bfl_frame->MifareKeyAvailable == 0x01)
                {
                    bfl_status = 
                        phJal_AuthenticateMifare(psHwReference, psRemoteDevInfo); 
                }
            }
            if (bfl_status == PH_ERR_BFL_SUCCESS)
            {
                command_param.self = &bfl_frame->MfRd;
                command_param.addr = pSendBuf[0];
                command_param.buffer = &pSendBuf[1];
                command_param.buffer_length = (uint8_t)(SendLength - 1);
                //*pRecvLength = 0;
                
                switch (Cmd.MfCmd)
                {
                case phHal_eMifareCmdListMifareAuthentA:    
                    bfl_frame->MifareAuthentCommand = PHCS_BFLMFRD_AUTHENT_A;
                    bfl_frame->AuthenticatedBlockAddress = pSendBuf[0];
                    memcpy(bfl_frame->CurrentMifareKey, &pSendBuf[1], MIFARE_KEY_LENGTH);
                    bfl_status = phJal_AuthenticateMifare(psHwReference, psRemoteDevInfo);
                    break;
                case phHal_eMifareCmdListMifareAuthentB:
                    bfl_frame->MifareAuthentCommand = PHCS_BFLMFRD_AUTHENT_B;
                    bfl_frame->AuthenticatedBlockAddress = pSendBuf[0];
                    memcpy(bfl_frame->CurrentMifareKey, &pSendBuf[1], MIFARE_KEY_LENGTH);
                    bfl_status = phJal_AuthenticateMifare(psHwReference, psRemoteDevInfo);
                    break;
                case phHal_eMifareCmdListMifareRead:
                    if (*pRecvLength < MF_BLOCK_SIZE)
                    {
                        command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_BUFFER_TOO_SMALL);    
                    } else
                    {
                        command_param.addr = pSendBuf[0];
                        command_param.buffer = pRecvBuf;
                        command_param.buffer_length = 0;
                        command_param.cmd = PHCS_BFLMFRD_READ;
                        bfl_status = bfl_frame->MfRd.Transaction(&command_param);
                        *pRecvLength = command_param.buffer_length;
                    }
                    break;
                case phHal_eMifareCmdListMifareWrite16:
                    command_param.cmd = PHCS_BFLMFRD_WRITE;
                    bfl_status = bfl_frame->MfRd.Transaction(&command_param);
                    break;
                case phHal_eMifareCmdListMifareWrite4:
                    command_param.cmd = PHCS_BFLMFRD_WRITE4;
                    bfl_status = bfl_frame->MfRd.Transaction(&command_param);
                    break;
                case phHal_eMifareCmdListMifareInc:
                    command_param.cmd = PHCS_BFLMFRD_INCREMENT;
                    bfl_status = bfl_frame->MfRd.Transaction(&command_param);
                    break;
                case phHal_eMifareCmdListMifareDec:
                    command_param.cmd = PHCS_BFLMFRD_DECREMENT;
                    bfl_status = bfl_frame->MfRd.Transaction(&command_param);
                    break;
                case phHal_eMifareCmdListMifareTransfer:
                    command_param.cmd = PHCS_BFLMFRD_TRANSFER;
                    bfl_status = bfl_frame->MfRd.Transaction(&command_param);
                    break;
                case phHal_eMifareCmdListMifareRestore:
                    command_param.cmd = PHCS_BFLMFRD_RESTORE;
                    bfl_status = bfl_frame->MfRd.Transaction(&command_param);
                    break;
                default:
                    command_status = (PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER));
                break;
                }

                if (command_status == NFCSTATUS_SUCCESS)
                {
                    if (bfl_status == PH_ERR_BFL_SUCCESS)
                    {
                        halt_param.self = &bfl_frame->Iso14443_3A;
                        bfl_status = bfl_frame->Iso14443_3A.HaltA(&halt_param);
                        if (bfl_status == PH_ERR_BFL_SUCCESS)
                        {
                            command_status = NFCSTATUS_SUCCESS;
                        }
                        else
                        {
                            command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_RF_TIMEOUT); 
                        }                        
                    }
                    else
                    {
                        command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_RF_TIMEOUT); 
                    }
                }
            }
            else
            {
                command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_RF_TIMEOUT);
            }
        }
        else
        {
            command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_BOARD_COMMUNICATION_ERROR);
        }
    }
    return command_status;
}

NFCSTATUS phJal_AttentionRequest(phHal_sHwReference_t           *psHwReference,  
                                phHal_sRemoteDevInformation_t   *psRemoteDevInfo)
{
    NFCSTATUS                           command_status = NFCSTATUS_SUCCESS;
    phcsBfl_Status_t                    bfl_status = PH_ERR_BFL_SUCCESS;
    phJal_ProtocolEntry_t               *protocol_entry = NULL;
    phJal_sBFLUpperFrame_t              *bfl_frame = NULL;
    phcsBflNfc_InitiatorAttReqParam_t   attention_request_param = {0};
    phcsBflNfc_Initiator_t              *protocol = NULL;
    
    if ((psHwReference == NULL) || 
        (psRemoteDevInfo == NULL))
    {
        return (PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER));
    }

    phJal_Trace(psHwReference, "phJal_AttentionRequest");

    bfl_frame = (phJal_sBFLUpperFrame_t*)psHwReference->pBoardDriver;

    if (bfl_frame == NULL)
    {
        return (PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER));
    }
    
    if (protocol_entry != NULL)
    {
        protocol = (phcsBflNfc_Initiator_t*)protocol_entry->Protocol;

        attention_request_param.self = protocol;
        

        bfl_status = protocol->AttRequest(&attention_request_param);
        if (bfl_status == PH_ERR_BFL_SUCCESS)
        {
            command_status = NFCSTATUS_SUCCESS;
        }
        else
        {
           command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_RF_TIMEOUT); 
        }
    }
    else
    {
        command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_DEVICE);
    }
    return command_status;
}

NFCSTATUS phHalNfc_TransceiveIso144434Command(phHal_sHwReference_t         *psHwReference,
                                            phHal_sRemoteDevInformation_t  *psRemoteDevInfo,
                                            phHal_sDepAdditionalInfo_t     *psDepAdditionalInfo,
                                            phHal_uCmdList_t                Cmd,
                                            uint8_t                        *pSendBuf,
                                            uint16_t                        SendLength,
                                            uint8_t                        *pRecvBuf,
                                            uint16_t                       *pRecvLength)
{
    NFCSTATUS                       command_status = NFCSTATUS_SUCCESS;
    phcsBfl_Status_t                bfl_status = PH_ERR_BFL_SUCCESS;
    phJal_sBFLUpperFrame_t          *bfl_frame = NULL;
    phcsBflI3P4_ExchangeParam_t     exchange_param = {0};
    phcsBflI3P4_PresCheckParam_t    presence_check_param = {0};
    phJal_ProtocolEntry_t           *protocol_entry = NULL;
    phcsBflI3P4_t                   *protocol = NULL;
    
    if (psHwReference == NULL || 
        psRemoteDevInfo == NULL ||
        pSendBuf == NULL ||
        pRecvBuf == NULL ||
        pRecvLength == NULL)
    {
        return (PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER));
    }

    phJal_Trace(psHwReference, "phHalNfc_TransceiveIso144434Command");

    bfl_frame = (phJal_sBFLUpperFrame_t*)psHwReference->pBoardDriver;

    if (bfl_frame == NULL)
    {
        return (PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER));
    }

    if (command_status == NFCSTATUS_SUCCESS)
    {
        if (bfl_status == PH_ERR_BFL_SUCCESS)
        {
            protocol_entry =
                phJal_GetProtocolEntryForLogicalHandle(bfl_frame->ISO14443_4Entries,
                psRemoteDevInfo->hLogHandle);

            if (protocol_entry != NULL)
            {
                protocol = (phcsBflI3P4_t*)protocol_entry->Protocol;

                switch (Cmd.Iso144434Cmd)
                {
                    case phHal_eIso14443_4_CmdListTClCmd:
                        if (psDepAdditionalInfo->DepFlags.NADPresent == 1)
                        {
                            exchange_param.nad_send = &psDepAdditionalInfo->NAD; 
                        }
                        else
                        {
                            exchange_param.nad_send = NULL;
                        }
                        exchange_param.self = protocol;
                        exchange_param.snd_buf = pSendBuf;
                        exchange_param.snd_buf_len = SendLength;
                        exchange_param.rec_buf = pRecvBuf;
                        exchange_param.rec_buf_len = *pRecvLength;
                        exchange_param.flags = 
                            (psDepAdditionalInfo->DepFlags.MetaChaining == 1)? 
                            PHCS_BFLI3P4_CONT_CHAINING : 0;

                        *pRecvLength = (uint16_t)exchange_param.rec_buf_len;
                        bfl_status = protocol->Exchange(&exchange_param);
                        
                        if (bfl_status == PH_ERR_BFL_SUCCESS)
                        {
                            command_status = NFCSTATUS_SUCCESS;
                        }
                        else
                        {
                            command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_RF_TIMEOUT); 
                        }
                        break;
                    case phHal_eIso14443_4_CmdListPresenceCheckCmd:
                        presence_check_param.self = protocol;
                        
                        bfl_status = protocol->PresenceCheck(&presence_check_param);
                        
                        if (bfl_status == PH_ERR_BFL_SUCCESS)
                        {
                            command_status = NFCSTATUS_SUCCESS;
                        }
                        else
                        {
                            command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_RF_TIMEOUT); 
                        }
                        break;
                    case phHal_eIso14443_4_CmdListPPSCmd:
                        /*
                         * Done during ACTIVATION (POLL function) it is not generally avaliable
                         * for the states after that, which means after a successful CONNECTion.
                         * Therefore it is better and safer to omit it. However, the command
                         * exists in the T=Cl commands in the HAL Types header, as a placeholder.
                         */
                        command_status = PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_DEVICE_REQUEST);
                        break;
                    default:
                        command_status = (PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_INVALID_PARAMETER));                    
                        break;
                }
            }      
        }
    }
    *pRecvLength = (uint16_t)exchange_param.rec_buf_len;
    return command_status;
}

phcsBfl_Status_t phJal_AuthenticateMifare(phHal_sHwReference_t      *psHwReference,
                                   phHal_sRemoteDevInformation_t    *psRemoteDevInfo)
{
    phJal_sBFLUpperFrame_t      *bfl_frame = (phJal_sBFLUpperFrame_t*)psHwReference->pBoardDriver;
    phcsBfl_Status_t            bfl_status = PH_ERR_BFL_SUCCESS;
    phcsBflMfRd_CommandParam_t  command_param = {0};
    uint8_t                     command_buffer[MIFARE_KEY_LENGTH + PHCS_BFLNFCCOMMON_ID_LEN]; 

    phJal_Trace(psHwReference, "phJal_AuthenticateMifare");

    memcpy(command_buffer, bfl_frame->CurrentMifareKey, MIFARE_KEY_LENGTH);
    memcpy(&command_buffer[MIFARE_KEY_LENGTH], 
        psRemoteDevInfo->RemoteDevInfo.CardInfo106.Startup106.NFCID1t, 
        psRemoteDevInfo->RemoteDevInfo.CardInfo106.Startup106.NFCID1tLength);
    
    command_param.cmd = bfl_frame->MifareAuthentCommand;
    command_param.self = &bfl_frame->MfRd;
    command_param.addr = bfl_frame->AuthenticatedBlockAddress;
    command_param.buffer = command_buffer;
    command_param.buffer_length = psRemoteDevInfo->RemoteDevInfo.CardInfo106.Startup106.NFCID1tLength + MIFARE_KEY_LENGTH;

    bfl_status = bfl_frame->MfRd.Transaction(&command_param);

    if (bfl_status == PH_ERR_BFL_SUCCESS)
    {
        bfl_frame->MifareKeyAvailable = 1;
    }
    else
    {
        memset(bfl_frame->CurrentMifareKey, 0, MIFARE_KEY_LENGTH);
        bfl_frame->MifareKeyAvailable = 0;
    }

    return bfl_status;
}

#endif
