/* /////////////////////////////////////////////////////////////////////////////////////////////////
//                     Copyright (c) Philips Semiconductors
//
//                       (C)PHILIPS Electronics N.V.2004
//         All rights are reserved. Reproduction in whole or in part is
//        prohibited without the written consent of the copyright owner.
//    Philips reserves the right to make changes without notice at any time.
//   Philips makes no warranty, expressed, implied or statutory, including but
//   not limited to any implied warranty of merchantability or fitness for any
//  particular purpose, or that the use will not infringe any third party patent,
//   copyright or trademark. Philips must not be liable for any loss or damage
//                            arising from its use.
///////////////////////////////////////////////////////////////////////////////////////////////// */


/*! \file Iso14443_4_Reader.cpp
 *
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Date: Thu Sep 27 09:38:47 2007 $
 *
 * Comment:
 *  Example code for using the ISO14443-4 Reader protocol.
 *
 */


/* For debugging purposes (printf) */
#include <stdio.h>

/* Common headers */
#include <phcsBflRefDefs.h>
#include <phcsBflHw1Reg.h>

/* Needed for any Operating Mode */
#include <phcsBflBalWrapper.hpp>
#include <phcsBflRegCtlWrapper.hpp>
#include <phcsBflOpCtlWrapper.hpp>
#include <phcsBflOpCtl_Hw1Ordinals.h>
#include <phcsBflIoWrapper.hpp>

/* Needed for ISO14443-4 */
#include <phcsBflI3P3AWrapper.hpp>
#include <phcsBflI3P4AActWrapper.hpp>
#include <phcsBflI3P4Wrapper.hpp>

#include "ExampleGlobals.h"
#include "ExampleUtils.h"

/* Speccifying namespace for BFL */
using namespace phcs_BFL;
    
/* 
   Callback definition for WTX timeout handling.
   The user interactions is done separated from the main routine which performs only data operations.
 */
void WTXCallback(phcsBflI3P4_CbSetWtxParam_t* wtx_param)
{
    #define             PRESCALER_100_US    0x02A5
    phcsBfl_Status_t    status            = PH_ERR_BFL_SUCCESS;
    uint32_t       timeout           = 302;
    uint8_t       buffer[4];

    /* Operation control parameters */
    phcsBflOpCtl_AttribParam_t            opp;

    /* change timeout value according to received TO value */
    timeout   = (timeout << wtx_param->fwi) / 100;

    /* The value of wtxm is only valid if this value is set to 1, otherwise reset to the default */
    if(wtx_param->set)
    {   /* multiply the timeout value with the wtxm value */
        timeout = timeout * wtx_param->wtxm;
    }
    /* prepare buffer for Timer settings (Prescaler and Reload value) in any case */
    buffer[0] = (uint8_t) ( PRESCALER_100_US & 0xFF );        /* Low value */
    buffer[1] = (uint8_t) ( (PRESCALER_100_US >> 8) & 0x0F ); /* High value (max 0x0F) */
    buffer[2] = (uint8_t) ( timeout & 0xFF );          /* Low value */
    buffer[3] = (uint8_t) ( (timeout >> 8) & 0xFF );   /* High value (max 0xFF) */
    
    /* do adjustments of timer with preset values */
    opp.group_ordinal    = PHCS_BFLOPCTL_GROUP_TMR;    
    opp.attrtype_ordinal = PHCS_BFLOPCTL_ATTR_TMR;
    opp.param_a          = 1;                   /* Timer Auto mode ON */
    opp.param_b          = 0;                   /* Timer auto reload OFF */
    opp.buffer           = buffer;              /* Buffer used for Prescaler and Reload value */
    status = ((phcsBflOpCtl_Wrapper<phcsBflOpCtl_Hw1Params_t>*)(((phcsBflI3P4_Wrapper*)(wtx_param->object))->mp_UserRef))->SetAttrib(&opp);
    /* Check status and display error if something went wrong */
    if(status != PH_ERR_BFL_SUCCESS)
	{
        printf("[E] Error setting timer! Status = %04X \n", status);
	}

    #undef PRESCALER_100_US
}

/*
 * This example shows how to act as ISO14443-4 reader 
 */
uint16_t ActivateIso14443_4_Reader(void *comHandle, uint32_t aSettings)
{
	/* BAL either for Windows xor for Linux */
#ifdef WIN32    
	phcsBflBal_Wrapper<phcsBflBal_Hw1SerWinInitParams_t>   bal;
#else    
	phcsBflBal_Wrapper<phcsBflBal_Hw1SerLinInitParams_t>     bal;
#endif
    phcsBflRegCtl_Wrapper<phcsBflRegCtl_SerHw1Params_t>    rc_reg_ctl;
    phcsBflOpCtl_Wrapper<phcsBflOpCtl_Hw1Params_t>             rc_op_ctl;
    phcsBflIo_Wrapper<phcsBflIo_Hw1Params_t>                   rcio;
    phcsBflI3P3A_Wrapper<phcsBflI3P3A_Hw1Params_t> iso14443_3;
    phcsBflI3P4AAct_Wrapper                                 iso14443_4A;
    phcsBflI3P4_Wrapper                                     iso14443_4;

    /* Register control parameters */
    phcsBflRegCtl_ModRegParam_t         mrp;
    phcsBflRegCtl_SetRegParam_t         srp;
    phcsBflRegCtl_GetRegParam_t         grp;

    /* Operation control parameters */
    phcsBflOpCtl_AttribParam_t          opp;

    /* ISO14443-3A parameters       */
    phcsBflI3P3A_ReqAParam_t          req_p;
    phcsBflI3P3A_AnticollSelectParam_t    sel_p;

    /* ISO14443-4A activation parameters */
    phcsBflI3P4AAct_RatsParam_t           rat_p;
    phcsBflI3P4AAct_PpsParam_t            pps_p;

    /* Transparent protocol operations   */
    phcsBflI3P4_SetCbParam_t          scb_p;
    phcsBflI3P4_ExchangeParam_t       exc_p;
    phcsBflI3P4_PresCheckParam_t  prs_p;
    phcsBflI3P4_DeselectParam_t       dsl_p;
    phcsBflI3P4_SetProtParam_t    spr_p;
    phcsBflI3P4_ResetProtParam_t  rst_p;
    
    /* Declaration of variables, buffer, ... */
	phcsBfl_Status_t		status = PH_ERR_BFL_SUCCESS;
    /* Buffer for data transfer (send/receive)  */
    uint8_t buffer[EXAMPLE_USER_BUFFER_SIZE];
    uint8_t rcvBuf[EXAMPLE_TRX_BUFFER_SIZE];
    /* Buffer serial number						*/
    uint8_t serial[12];
    /* Counter variable							*/
    uint8_t i;
    /* Buffer for ATS */
	uint8_t  ats[64];

    /* Mifare EvalOS command to read data from Mifare EEProm area using PasswordRead function */
    uint8_t  readMFblock [14] = { 0xB8, 0x00, 0x3F, 0x00, 0x08, 0x0b, 0x54, 0x57, 0x07, 0x45, 0xfe, 
                                        0x3a, 0xe7, 0x10 };	

    /* Initialise Bus Abstraction Layer component (RS232) */
#ifdef WIN32
    /* Either for Windows... */
    phcsBflBal_Hw1SerWinInitParams_t init_params;
    init_params.com_handle = (void*)(comHandle);
    init_params.com_port   = 0;
    bal.Initialise(phcsBflBal_Hw1SerWinInit, (void*)(&init_params)); 
#else
    /* ... or for Linux */
    phcsBflBal_Hw1SerLinInitParams_t init_params;
    init_params.com_handle = (void*)(comHandle);
    init_params.com_port   = 0;
	bal.Initialise(phcsBflBal_Hw1SerLinInit, (void*)(&init_params)); 
#endif
    /* Initialise Register Control Layer component (same for Initiator and Target) */
    rc_reg_ctl.Initialise(phcsBflRegCtl_SerHw1Init, &bal);
    /* Initialise Operation Control component (same for Initiator and Target) */
    rc_op_ctl.Initialise(phcsBflOpCtl_Hw1Init, &rc_reg_ctl); 
    /* Initialise RcIo component (choose Initiator behaviour) */
    rcio.Initialise(phcsBflIo_Hw1Init, &rc_reg_ctl, PHCS_BFLIO_INITIATOR); 
    /* Initialise ISO14443-3 component (choose Initiator == PCD behaviour) */
    iso14443_3.Initialise(phcsBflI3P3A_Hw1Initialise, &rc_reg_ctl, PHCS_BFLI3P3A_INITIATOR);
    /* Initialise ISO14443-4A Activation component (only reader side available)  */
    status = iso14443_4A.Initialise(buffer, EXAMPLE_USER_BUFFER_SIZE, &rcio);
	if (status != PH_ERR_BFL_SUCCESS)
	{
        printf("[E] Error activation of phcsBflI3P4AAct component. Status = 0x%04X", status);
    }
    /* Initialise ISO14443-4 transport protocol */
    status = iso14443_4.Initialise(&rcio);
	if (status != PH_ERR_BFL_SUCCESS)
	{
        printf("[E] Error activation of phcsBflI3P4 component. Status = 0x%04X", status);
    }

    /* 
	 * Configure PN51x to act as a Mifare Reader. All register which are set in other modes 
     * are configured to the right values. 
     * Remark: Operation control function do not perform a softreset and does not know anything 
     *         about the antanne configuration, so these things have to be handled here!          
	 */
    opp.group_ordinal    = PHCS_BFLOPCTL_GROUP_MODE;
    opp.attrtype_ordinal = PHCS_BFLOPCTL_ATTR_MFRD_A;
    opp.param_a          = PHCS_BFLOPCTL_VAL_RF106K;
    opp.param_b          = PHCS_BFLOPCTL_VAL_RF106K;
    /* Start set attribute command */
    status = rc_op_ctl.SetAttrib(&opp);
    /* Check status and display error if something went wrong */
    if(status != PH_ERR_BFL_SUCCESS)
        printf("*** ERROR performing Mifare Reader configuration! Status = %04X \n", status);

    /* Set Initiator bit of Joiner to act as a reader using register control functions. */
    srp.address = PHCS_BFL_JREG_CONTROL;
    srp.reg_data = PHCS_BFL_JBIT_INITIATOR;
    status = rc_reg_ctl.SetRegister(&srp);
    /* Check status and display error if something went wrong */
    if(status != PH_ERR_BFL_SUCCESS)
        printf("*** ERROR! SetRegister: Address=%02X, Data=%02X, Status = %04X \n", 
                srp.address, srp.reg_data, status);

    /* 
	 * Set the timer to auto mode, 5ms using operation control commands before HF is 
	 * switched on to guarantee Iso14443-3 compliance of the polling procedure 
	 */
    opp.group_ordinal    = PHCS_BFLOPCTL_GROUP_TMR;         /* Set operation control group parameter to Timer */
    opp.attrtype_ordinal = PHCS_BFLOPCTL_ATTR_TMR_AUTO;     /* Set function for Timer automatic mode          */
    opp.param_a          = PHCS_BFLOPCTL_VAL_ON;              /* Use parameter a to switch it on                */
    status = rc_op_ctl.SetAttrib(&opp);
    /* Check status and display error if something went wrong */
    if(status != PH_ERR_BFL_SUCCESS)
        printf("*** ERROR! OperationControl settings Timer auto mode. Status = %04X \n", status);

    /* 
	 * Set prescaler steps to 100us
	 */
    opp.group_ordinal    = PHCS_BFLOPCTL_GROUP_TMR;         
    opp.attrtype_ordinal = PHCS_BFLOPCTL_ATTR_TMR_PRESCALER;    
	/* Calculation of the values: 100us = 6.78MHz/678 = 6.78MHz/0x2A6 */
    opp.param_a          = 0xA5;                        /* Low value  */
    opp.param_b          = 0x02;                        /* High value */
    status = rc_op_ctl.SetAttrib(&opp);
    /* Check status and display error if something went wrong */
    if(status != PH_ERR_BFL_SUCCESS)
        printf("*** ERROR! OperationControl settings Timer Prescaler. Status = %04X \n", status);

    /* set reload value 
       (group parameter is set from above and not modified during operation) */
    opp.attrtype_ordinal = PHCS_BFLOPCTL_ATTR_TMR_RELOAD;   /* set to 50 for 5 ms    */
    opp.param_a          = 50;                      /* Low value             */
    opp.param_b          = 0;                       /* High value (max 0xFF) */
    status = rc_op_ctl.SetAttrib(&opp);
    if(status != PH_ERR_BFL_SUCCESS)
        printf("*** ERROR! OperationControl settings Timer Reload. Status = %04x \n", status);


    /* Activate the field (automatically if there is no external field detected) */
    mrp.address = PHCS_BFL_JREG_TXAUTO;
    mrp.mask    = PHCS_BFL_JBIT_INITIALRFON | PHCS_BFL_JBIT_TX2RFAUTOEN | PHCS_BFL_JBIT_TX1RFAUTOEN;
    mrp.set     = 1;
    status = rc_reg_ctl.ModifyRegister(&mrp);
    if(status != PH_ERR_BFL_SUCCESS)
        printf("*** ERROR! ModifyRegister: Set=%1d, Address=%02X, Mask=%02X, Status = %04X \n", 
                mrp.set, mrp.address, mrp.mask, status);


    /* 
	 * After switching on the drivers wait until the timer interrupt occures, so that 
     * the field is on and the 5ms delay have been passed. 
	 */
    grp.address = PHCS_BFL_JREG_COMMIRQ;
    do {
        status = rc_reg_ctl.GetRegister(&grp);
    } 
    while(!(grp.reg_data & PHCS_BFL_JBIT_TIMERI) && status == PH_ERR_BFL_SUCCESS);
    /* Check status and diplay error if something went wrong */
    if(status != PH_ERR_BFL_SUCCESS)
        printf("*** ERROR! GetRegister: Address=%02X, Data=%02X, Status = %04X \n", 
                grp.address, grp.reg_data, status);

    /* Clear the status flag afterwards (use again register commands) */
    srp.address  = PHCS_BFL_JREG_COMMIRQ;
    srp.reg_data = PHCS_BFL_JBIT_TIMERI;
    status = rc_reg_ctl.SetRegister(&srp);
    /* Check status and display error if something went wrong */
    if(status != PH_ERR_BFL_SUCCESS)
        printf("*** ERROR! SetRegister: Address=%02X, Data=%02X, Status = %04X \n", 
                srp.address, srp.reg_data, status);

	/* Note that this is only needed if the SIGIN/SIGOUT feature is used! */
	if (aSettings & 0x03)
	{
		if ( aSettings == 1 )
			opp.param_b = PHCS_BFLOPCTL_VAL_NOSIGINOUT;
		else if ( aSettings == 2 )
			opp.param_b = PHCS_BFLOPCTL_VAL_ONLYSIGINOUT;
		else
			opp.param_b = PHCS_BFLOPCTL_VAL_SIGINOUT;

		opp.group_ordinal = PHCS_BFLOPCTL_GROUP_SIGNAL;
		opp.attrtype_ordinal = PHCS_BFLOPCTL_ATTR_SIGINOUT;
		opp.param_a = PHCS_BFLOPCTL_VAL_READER; 
		status = rc_op_ctl.SetAttrib(&opp);
		if(status != PH_ERR_BFL_SUCCESS)
			printf("[E] Failed to set SIGINOUT features. Status = %04X \n", status);
	}

    /* 
	 * Reset timer 1 ms using operation control commands (AutoMode and Prescaler are the same) 
	 * set reload value
	 */
    opp.group_ordinal    = PHCS_BFLOPCTL_GROUP_TMR;         /* set operation control group parameter to Timer */
    opp.attrtype_ordinal = PHCS_BFLOPCTL_ATTR_TMR_RELOAD;   /* set to 10 for 1 ms */
    opp.param_a          = 10;                      /* Low value */
    opp.param_b          = 0;                       /* High value (max 0xFF) */
    status = rc_op_ctl.SetAttrib(&opp);
    // check status and diplay error if something went wrong
    if(status != PH_ERR_BFL_SUCCESS)
        printf("*** ERROR! OperationControl settings Timer Reload. Status = %04X \n", status);


    /* Activate receiver for communication
       The RcvOff bit and the PowerDown bit are cleared, the command is not changed. */
    srp.address  = PHCS_BFL_JREG_COMMAND;
    srp.reg_data = PHCS_BFL_JCMD_NOCMDCHANGE;
    status = rc_reg_ctl.SetRegister(&srp);
    if(status != PH_ERR_BFL_SUCCESS)
        printf("*** ERROR! SetRegister: Address=%02X, Data=%02X, Status = %04X \n", 
                srp.address, srp.reg_data, status);

    /* 
	 * Do activation according to ISO14443A Part3 
     * Prepare the Request command 
	 */
    buffer[0] = 0;              
    buffer[1] = 0;
    req_p.req_code = PHCS_BFLI3P3A_REQIDL;    /* Set Request command code (or Wakeup: ISO14443_3_REQALL) */
    req_p.atq      = buffer;                        /* Let Request use the defined return buffer */
    status = iso14443_3.RequestA(&req_p);           /* Start Request command */
    /* Check return code: This code could be either 
     *    PH_ERR_BFL_SUCCESS (only one card in field) or
     *   PH_ERR_BFL_COLLISION_ERROR (if more than one card is in the field). 
	 */
    if(status == PH_ERR_BFL_SUCCESS || (status & ~PH_ERR_BFL_COMP_MASK) == PH_ERR_BFL_COLLISION_ERROR)
    {
        /* There was at least one response */
        if((status & ~PH_ERR_BFL_COMP_MASK) == PH_ERR_BFL_COLLISION_ERROR)
            printf("Collision occurred during Request! Multiple Cards detected!\n");
        printf("ATQA = %02X %02X \n", req_p.atq[0],req_p.atq[1]);
    } else
    {
		/* No response at all */
        printf("*** ERROR! RequestA: RequestCode=%02X, ATQA=%02X %02X, Status = %04X \n", 
                req_p.req_code, req_p.atq[0],req_p.atq[1], status);
    }

    /* Prepare data for combined anticollision/select command to get one complete ID */
    for(i=0;i<12;i++)
        serial[i]      = 0x00;      /* Clear all unused bytes of the I/O buffer */

    sel_p.uid          = serial;                 /* Put serial buffer for ID */
    sel_p.uid_length   = 0;                      /* No bits are known from ID, so set to 0 */
    /* Start Anticollision/Select command */
    status = iso14443_3.AnticollSelect(&sel_p);
    /* Check return code: If the command was ok, is always returns PH_ERR_BFL_SUCCESSS.
     * If nothing was received, parameter uid_length is 0. */
    if(status == PH_ERR_BFL_SUCCESS)
    {
        /* Display UID and SAK if anything was ok */
        printf("Anticollision/Select: \t UID: ");
        for(i=0;i<(sel_p.uid_length/8);i++)
            printf("%02X", sel_p.uid[i]);
        printf("\tSAK: %02X \n",sel_p.sak);
    } else
    {
        printf("*** ERROR! Anticollision/Select: Status = %04X \n", status);
    }

	status = SetTimeOut(&rc_reg_ctl, &rc_op_ctl, 5000, 0);
	if (status != PH_ERR_BFL_SUCCESS)
    {
		printf("[E] Could not set the timer of the PN51x.\n");
    } else
    {
		printf("[I] Timer of the PN51x reconfigured.\n");
    }

    /* ---------------------------------------------------------------------------- */
    /* Example how to use the Iso14443 Part4 (T=CL) specific command set of the BFL */
        
    /* Prepare for RATS command */
    rat_p.cid         = 0;
    rat_p.fsi         = 4;	/* Take care that the real buffer size of the PCD is big enough */
    rat_p.ats         = ats;
    /* Issue the RATS command */
    status = iso14443_4A.Rats(&rat_p);
    if(status == PH_ERR_BFL_SUCCESS)
    {
        /* Display all received parameters if anything was ok */
        printf("RATS: \t ATS Length: %04X, ", rat_p.ats_len);
		/* Display (negotiated) FSI 
		 * FSI is determined by calculating fsi=min(fsci, fsdi) */
		printf("negotiated FSI=%02X, ", rat_p.fsi);
        /* Display TA(1) */
        if(rat_p.ta1)
            printf("TA(1)=%02X, ", *rat_p.ta1);
        else
            printf("no TA(1), ");
        /* display TB(1) */
        if(rat_p.tb1)
            printf("TB(1)=%02X, ", *rat_p.tb1);
        else
            printf("no TB(1), ");
        /* display TC(1) if applicable */
        if(rat_p.tc1)
            printf("TC(1)=%02X, ", *rat_p.tc1);
        else
            printf("no TC(1), ");
        if(rat_p.app_inf_len)
        {
            printf("\n    Historical characters (%d bytes):  ", rat_p.app_inf_len);
            for(i=0;i<(rat_p.app_inf_len);i++)
                printf("%02X ", rat_p.app_inf[i]);
        } else
        {
            printf("\nNo Historical characters. ");
        }
        printf("\n");
    } else 
    {
        printf("*** ERROR! ISO14443 Part4 -> RATS: Status = %04X \n", status);
    }


    /* Check speed capability of the remote card (stored in TA(1) byte) */
	/* Only relevant if the last command was successful and TA(1) is supported */
	if ( ( !status ) && (rat_p.ta1 != 0) )
    {
        if ((*rat_p.ta1 & 0x80) == 0) 
        {
            printf("Asymmetric datarates supported! \n  Possible data rates (besides 106kBit/s) for PCD -> PICC are: \n\t");
            if(*rat_p.ta1 & 0x01)
                printf("212kbps, ");
            if(*rat_p.ta1 & 0x02)
                printf("424kbps, ");
            if(*rat_p.ta1 & 0x04)
                printf("848kbps");
            printf("\n  Possible data rates (besides 106kBit/s) for PICC -> PCD are: \n\t");
            if(*rat_p.ta1 & 0x10)
                printf("212kbps, ");
            if(*rat_p.ta1 & 0x20)
                printf("424kbps, ");
            if(*rat_p.ta1 & 0x40)
                printf("848kbps");
        } else 
        {
            printf("Only symmetrical datarates supported! \n  Possible data rates (besides 106kBit/s) are: \n\t");
            if(*rat_p.ta1 & 0x01 && *rat_p.ta1 & 0x10)
                printf("212kbps, ");
            if(*rat_p.ta1 & 0x02 && *rat_p.ta1 & 0x20)
                printf("424kbps, ");
            if(*rat_p.ta1 & 0x04 && *rat_p.ta1 & 0x40)
                printf("848kbps");
        }
        printf("\n");
    }

	if (rat_p.ta1)
	{
		/* 
		 * Change data rate if TA(1) was valid in ATS 
		 * PPS can only be done right after the RATS/ATS chain
		 */
        pps_p.cid = 0x00;               /* no card identifier used */
        pps_p.dri = PHCS_BFLOPCTL_VAL_RF106K;     /* data rate PCD -> PICC   */
        pps_p.dsi = PHCS_BFLOPCTL_VAL_RF106K;     /* data rate PICC -> PCD   */

        status = iso14443_4A.Pps(&pps_p);
        if(status == PH_ERR_BFL_SUCCESS)
        {
			    printf("[I] PPS handled without errors, reconfiguration is possible.\n");
			    /* call Operation control function with new data rate */
			    opp.group_ordinal    = PHCS_BFLOPCTL_GROUP_MODE;
			    opp.attrtype_ordinal = PHCS_BFLOPCTL_ATTR_MFRD_A;
			    opp.param_a          = PHCS_BFLOPCTL_VAL_RF106K;
			    opp.param_b          = PHCS_BFLOPCTL_VAL_RF106K;
			    status = rc_op_ctl.SetAttrib(&opp);
			    if(status != PH_ERR_BFL_SUCCESS)
				    printf("[E] ERROR performing Mifare Reader configuration for higher data rates! Status = %04x \n", status);

		} else
		{
			printf("[E] ERROR! ISO14443 Part4 -> PPS: Status = %04x \n", status);
		}
    	
		/* Hand over parameters to transparent protocol */
	} else
	{
		printf("[I] No TA(1) has been specified -> no speed adjustment possible.\n");
	}

	/* Initalize Callbacks for user interaction during protocol operation */
    iso14443_4.mp_UserRef = &rc_op_ctl;         /* Init user pointer to reach operation control component from callback */
    
    scb_p.set_reader_params_cb = NULL;          /* no interactive change of bitrate parameters, done in the main function */
    scb_p.set_wtx_cb           = WTXCallback;   /* Callback function to process WTX requests from card side */
    iso14443_4.SetCallbacks(&scb_p);

    /* Hand over parameters generated in activatin part to transparent protocol part of ISO 14443-4 */
    spr_p.max_retry        = 1;
    spr_p.p_protocol_param = iso14443_4A.ProtocolParameters();
    iso14443_4.SetProtocol(&spr_p);
    
    /* call WTX callback to set timeout value */
    phcsBflI3P4_CbSetWtxParam_t  wtx_param;
    wtx_param.set = 0;
    wtx_param.object = iso14443_4.m_Interface.mp_CppWrapper;
    WTXCallback(&wtx_param);


	/*
	 * This is a typical data exchange pair 
	 */
	exc_p.nad_receive = 0;
    exc_p.nad_send    = 0;
    exc_p.snd_buf     = readMFblock;
    exc_p.snd_buf_len = 14;
    exc_p.rec_buf     = rcvBuf;
    exc_p.rec_buf_len = 64;
    status = iso14443_4.Exchange(&exc_p);
    if(status == PH_ERR_BFL_SUCCESS)
    {
        printf("[I] Exchange handled without errors, Mifare Read OK. \nData: ");
        for (i=0; i<exc_p.rec_buf_len; i++)
            printf("%02X ", exc_p.rec_buf[i]);
        printf("\n");
    } else
    {
        printf("[E] ERROR! ISO14443 Part4 -> Exchange: MifareRead: Status = %04x \n", status);
    }

    /* Check if card is still in field */
    status = iso14443_4.PresenceCheck(&prs_p);
    if(status == PH_ERR_BFL_SUCCESS)
    {
        printf("Presence check OK, Card available. \n");
    } else
    {
        printf("Presence check FAIL, No card available any more! \n");
    }


    /* Deselect device at the end of communication */
    status = iso14443_4.Deselect(&dsl_p);
    if(status == PH_ERR_BFL_SUCCESS)
    {
        printf("Deselect OK. \n");
    } else
    {
        printf("Deselect FAIL, No card available any more! Try error handling. \n");
    }

    /* To start a communication with a new device all internal status variables have to be reset.  */
    iso14443_4.ResetProt(&rst_p);     /* This function has no return value. All variables are reset! */

	return status;
}
