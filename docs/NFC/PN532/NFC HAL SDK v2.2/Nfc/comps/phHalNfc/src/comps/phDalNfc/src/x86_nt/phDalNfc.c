/*
 *         Copyright (c), NXP Semiconductors
 *
 *                       (C)NXP Electronics N.V.2004
 *         All rights are reserved. Reproduction in whole or in part is
 *        prohibited without the written consent of the copyright owner.
 *    NXP reserves the right to make changes without notice at any time.
 *   NXP makes no warranty, expressed, implied or statutory, including but
 *   not limited to any implied warranty of merchantability or fitness for any
 *  particular purpose, or that the use will not infringe any third party patent,
 *   copyright or trademark. NXP must not be liable for any loss or damage
 *                            arising from its use.
 *
 */

/*
 * \file phDalNFC.c
 *
 * Project: NFC MW / HAL
 *
 * $Date: Thu Sep 27 09:38:40 2007 $
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Aliases: SDK_HAL_2.2,SDK_HAL_2.2_071008 $
 *
 */

#ifdef  PHHAL_VERSION_ACQUISITION /**/

    #define PHDALNFC_SOURCE_FILEREVISION "$Revision: 1.1 $"
    #define PHDALNFC_SOURCE_FILEALIASES  "$Aliases: SDK_HAL_2.2,SDK_HAL_2.2_071008 $"

#else /* PHHAL_VERSION_ACQUISITION */

#if ((! defined PHJALJOINER) && (! defined PHTALTAMA))
    #define PHJALJOINER
    #define PHTALTAMA
#endif

#include <phDalNfc.h>
#include <phNfcIoCtlCode.h>
#include <phOsalNfc.h>

#ifdef PHTALTAMA
#include <phHalNfc.h>
#endif

#ifdef PHJALJOINER
    #include <phJalJoinerBflFrame.h>     /* For BFL usage in this file only */
    #include <phcsBflHw1Reg.h>
    #include <phcsBflRegCtl.h>
#include <phJalDebugReportFunctions.h>
#endif

#include <assert.h>
#include <stdio.h>
#include <windows.h>
#include <winioctl.h>
#include <time.h>


#define USB_TEST_IOCTL_VERSION_NUMBER           CTL_CODE(FILE_DEVICE_UNKNOWN, 0x807, METHOD_BUFFERED, FILE_ANY_ACCESS)

/* Internally used, ensure that the total number of devices does not exceed 255 ! */
#define MAX_USB_DEVICES                         9 /**/
#define MAX_J_RS232_DEVICES                     9 /**/

/*  Internally used for SERIAL Communication with TAMA  */
#define T_RS232_RETRY                       1
#define T_RS232_TIMEOUT_MIN                 15  /*  msec    */
#define T_RS232_TIMEOUT_DEFAULT             150 /*  msec    */
#define T_RS232_USEOVERLAPPED               1
#define PHDAL_DNL                           10

#ifdef _UNICODE
    #define CREATEFILE CreateFileW /* WIDE CHAR */
#else
    #define CREATEFILE CreateFileA /* ANSI */
#endif


#ifdef PHJALJOINER

typedef struct phDal_sBFLLowerFrame
{
    int8_t           DeviceName[PHDAL_DNL];         /*!< The name of the device (required to open it). */

    phcsBflBal_t                     Bal;           /*!< The BAL structure, as fixed part of the container. */
    phcsBflBal_Hw1SerWinInitParams_t BalP;          /*!< The internal variables/params the BAL is using. */

    phcsBflRegCtl_t                  RcRegCtl;      /*!< The RcRegCtl structure, as fixed part of the container. */
    phcsBflRegCtl_SerHw1Params_t     RrcP;          /*!< Internals of RcRegCtl. */

}phDal_sBFLLowerFrame_t;

void phDal_BuildBflLowerFrame(phDal_sBFLLowerFrame_t *bfl_frame);

phcsBfl_Status_t WaitForPeripheralEvent(phJal_WaitForReceptionParams_t *psReceptionParams,     /* Reception Parameters provided by the upper layer */
                                        uint8_t                        Command,                /* Command to be executed by the BFL */
                                        uint8_t                        JoinerOperationMode,    /* Differentiates between Initiator or Target operation mode of Joiner. */
                                        uint8_t                        TargetSend,             /* Shows in target mode the intention to send data in transceive mode not data to be received */
                                        uint8_t                        CommIrqMask);           /* CommIrqs to be waited for */

#define PHDAL_JBAUD_115K                    0x7A  /* Register setting for serial speed of Joiner . */





phcsBfl_Status_t phcsBflDal_RecoverPN51x(phcsBflRegCtl_t *regctl)
{
    phcsBfl_Status_t             rclstatus;
    phcsBflRegCtl_GetRegParam_t  getreg_param;
    phcsBflRegCtl_SetRegParam_t  setreg_param;

    getreg_param.self    = regctl;
    setreg_param.self    = getreg_param.self;

    /* Read reg 0 2x to get back into sync: */
    getreg_param.address = PHCS_BFL_JREG_PAGE0;
    rclstatus = regctl->GetRegister(&getreg_param);
    rclstatus = regctl->GetRegister(&getreg_param);

    /* Write 0 to reg 0 to disable PAGING in case it is there accidentially: */
    setreg_param.address = PHCS_BFL_JREG_PAGE0;
    setreg_param.reg_data = 0;
    rclstatus = regctl->SetRegister(&setreg_param);

    /* Do a readout of reg 0 again to compare: */
    rclstatus = regctl->GetRegister(&getreg_param);
    
    if (PH_ERR_BFL_SUCCESS == rclstatus)
    {
        if (getreg_param.reg_data == setreg_param.reg_data)
        {
            /* OK! */
            rclstatus = PH_ERR_BFL_SUCCESS;
        } else
        {
            /* Data read back are not the data that have been written. */
            rclstatus = PH_ERR_BFL_INTERFACE_ERROR;
        }
    } else
    {
        /* BFL function returned error: */
        rclstatus = PH_ERR_BFL_INTERFACE_ERROR;
    }

    return rclstatus;
}

#endif /* JOINER */

/**
 * Function use to protect USB buffer write.
 * When the time out occured, the function flush the USB
 * Buffer.
 */
DWORD WINAPI phDalNfc_BufferUSBFlush(LPVOID iValue)
{ 
    /* MAX_SIZE_DAL_BUFFER */
    phHal_sHwReference_t* psHwReference;
    /* Flush buffer */
    uint8_t  Buffer[300];
    uint32_t NumberOfBytesRead;
    OVERLAPPED  osRead;
    DWORD status;

    psHwReference = (phHal_sHwReference_t*) iValue;

    while ( psHwReference->DalContext.startThread == 1)
    {
        status = WaitForSingleObject(psHwReference->DalContext.evTimeoutStarted, INFINITE);

        if (WAIT_OBJECT_0 == status)
        {
            /* Protect the WriteFile*/
            status = WaitForSingleObject(psHwReference->DalContext.evAbortTimeout, 500);      
            if (WAIT_TIMEOUT == status) 
            {
                /* The USB writeFile is locked: flush */
                osRead.Offset = 0;
                osRead.OffsetHigh = 0;
                osRead.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
                ReadFile ((HANDLE)(psHwReference->pBoardDriver), &Buffer, 300, &NumberOfBytesRead, &osRead);
            }

            SetEvent(psHwReference->DalContext.evReady);

        } 
    }  
    return TRUE;
}


void phDalNfc_FlushUSBRequest(phHal_sHwReference_t* psHwReference)
{
    if (     NULL           !=  psHwReference 
        &&  1               ==  psHwReference->DalContext.startThread 
        && PHHAL_USB_DEVICE ==  psHwReference->LinkType)
    {
        ResetEvent(psHwReference->DalContext.evAbortTimeout);
        SetEvent(psHwReference->DalContext.evTimeoutStarted);
    }
}

void phDalNfc_FlushUSBCancel(phHal_sHwReference_t* psHwReference)
{
    if (     NULL           !=  psHwReference 
        &&  1               ==  psHwReference->DalContext.startThread 
        && PHHAL_USB_DEVICE ==  psHwReference->LinkType)
    {
        SetEvent(psHwReference->DalContext.evAbortTimeout);
        WaitForSingleObject(psHwReference->DalContext.evReady, INFINITE);      
    }
}


/* Internal prototypes: */
DWORD   ChangeHostBaudrate          (   void* Port, unsigned long Baudrate);
DWORD   SetComPortTimeouts          (   void* port, DWORD timeout);
BOOL    TAMABaudrateChange          (   HANDLE handle, unsigned long baudrate, uint8_t* can_communicate);
BOOL    TAMAWakeUp                  (   HANDLE handle, uint8_t *can_communicate);
BOOL    SendSerialCommand           (   HANDLE handle, uint8_t *FrameInOut, uint16_t uiBufferInSize, uint8_t *can_communicate);
BOOL    TAMABaudrateMatchWithHost   (   HANDLE handle, unsigned long baudrate, uint8_t Asleep);
BOOL    TAMAWaitForIo               (   phHal_sHwReference_t *psHwReference, OVERLAPPED *pOsRead, uint32_t nTimeout, uint8_t allowAbort);
void    TAMAAbortDriverIo           (   phHal_sHwReference_t *psHwReference, HANDLE Handle,                       OVERLAPPED *pOsRead, uint32_t *pNumberOfBytesRead);
BOOL    Serial232IO                 (   phHal_sHwReference_t *psHwReference, BOOL readOrWrite, HANDLE handle, uint8_t * lpBuf, uint16_t dwToDo, DWORD* dwDone, uint8_t nbRetry, uint32_t nTimeout, uint8_t allowAbort);
BOOL    Serial232Read               (   phHal_sHwReference_t *psHwReference, HANDLE handle, uint8_t * lpBuf, uint16_t dwToDo, DWORD*   dwDone, uint8_t nbRetry, uint32_t nTimeout, uint8_t allowAbort);
BOOL    Serial232Write              (   phHal_sHwReference_t *psHwReference, HANDLE handle, uint8_t * lpBuf, uint16_t dwToDo, DWORD*   dwDone, uint8_t nbRetry, uint32_t nTimeout);



BOOL Serial232IO(phHal_sHwReference_t *psHwReference, BOOL readOrWrite, HANDLE handle, uint8_t * lpBuf, uint16_t dwToDo, DWORD*    dwDone, uint8_t nbRetry, uint32_t nTimeout, uint8_t allowAbort)
{

    DWORD           dwRes       =   WAIT_FAILED;
    BOOL            dwResFunc   =   FALSE;
    DWORD           dwDoneTmp;
    *dwDone     =   0;

//    if(nbRetry<=0)
        nbRetry =   1;

    nTimeout    =   nTimeout    /   nbRetry;

    if(T_RS232_TIMEOUT_MIN >  nTimeout)
        nTimeout    =   T_RS232_TIMEOUT_MIN;

    SetComPortTimeouts(handle, nTimeout);

    while(nbRetry>0)
    {

        OVERLAPPED  over        = {0};
        BOOL        dwTmp;
        HANDLE      events[2];
        DWORD       waitForObjRes   =   0;
        BOOL        bResult;

        /*  create this writes overlapped structure hEvent */
        over.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
        if (over.hEvent == NULL)
            return dwResFunc;

        if(0    !=  *dwDone &&  T_RS232_TIMEOUT_MIN >  nTimeout)
        {
            nTimeout    =   T_RS232_TIMEOUT_MIN;
            SetComPortTimeouts(handle, nTimeout);
        }

        /*  issue operation     */
        if(readOrWrite)
            dwTmp   =   ReadFile(handle, lpBuf+(*dwDone), dwToDo, &dwDoneTmp, &over);
        else
            dwTmp   =   WriteFile(handle, lpBuf+(*dwDone), dwToDo, &dwDoneTmp, &over);

        if (!dwTmp)
        {
            if (GetLastError() != ERROR_IO_PENDING)
            {
                /*  Error in communication */
            }
            else
            {
                /*  operation is delayed */
                if(!readOrWrite)
                {

                    /*  Wating for Write operation completion. We should wait for at least nTimeout+1 */
                    events[0]   =   over.hEvent;
                    dwRes = WaitForMultipleObjects(1, events, FALSE, nTimeout+1);

                    bResult =       (GetLastError() == ERROR_IO_PENDING)
                                &&  (WAIT_OBJECT_0  ==  waitForObjRes);
                }
                else
                {
                    /*  Waiting for Read operation completion. We should wait for at least nTimeout+1 */
                    bResult =   TAMAWaitForIo(psHwReference, &over, nTimeout+1, allowAbort );
                }

                if( bResult )
                {
                    if (!GetOverlappedResult(handle, &over, &dwDoneTmp, FALSE))
                    {
                        /*  Error in communication
                            This is possible to get here if the buffer
                            provided by the caller within Read/WriteFile
                            function was not big enough to read all bytes from USB
                            */
                    }
                    else
                    {
                        *dwDone     +=  dwDoneTmp;
                        dwToDo      -=  (uint16_t)dwDoneTmp;
                        if (0   == dwToDo)
                        {
                            /*  successfull completion of the operation */
                            dwResFunc   =   TRUE;
                        }
                    }
                }
                else
                {
                    TAMAAbortDriverIo( psHwReference, handle, &over, &dwDoneTmp);
                }

            }
        }
        else
        {
            *dwDone     +=  dwDoneTmp;
            dwToDo      -=  (uint16_t)dwDoneTmp;
            if (0 == dwToDo)
            {
                /*  Operation successfully completed */
                dwResFunc   =   TRUE;
            }
        }

        CloseHandle(over.hEvent);

        if(0    ==  dwDoneTmp)
        {
            CancelIo(handle);
            nbRetry--;
        }

        if(dwResFunc)
            break;

    }   /*  while   */

    return          dwResFunc;


}

BOOL Serial232Read(phHal_sHwReference_t *psHwReference, HANDLE handle, uint8_t * lpBuf, uint16_t dwToDo, DWORD*    dwDone, uint8_t nbRetry, uint32_t nTimeout, uint8_t allowAbort)
{
    return Serial232IO(psHwReference, TRUE, handle, lpBuf, dwToDo, dwDone, nbRetry, nTimeout, allowAbort);
}
BOOL Serial232Write(phHal_sHwReference_t *psHwReference, HANDLE handle, uint8_t * lpBuf, uint16_t dwToDo, DWORD*   dwDone, uint8_t nbRetry, uint32_t nTimeout)
{
    PurgeComm(handle,0x08);
    return Serial232IO(psHwReference, FALSE, handle, lpBuf, dwToDo, dwDone, nbRetry, nTimeout, 0);
}

/*! Attempts to change and match baud rate of both Host and TAMA, according to baudrate.
 *  Returns TRUE if baudrate was successfully set
 */

BOOL TAMABaudrateMatchWithHost(HANDLE handle, unsigned long    baudrate, uint8_t Asleep)
{
    unsigned long   baudrate_tmp;
    int k;
    uint8_t         can_communicate;
    int can_change;
    DWORD       dwDone;
    uint8_t     FrameAsleep[]   =   {0x00,0x00,0xFF,0x05,0xFB,0xD4,0x14,0x02,0x00,0x00,0x16,0x00};/* Asllep the PN532 1.5*/

    int result  =   -1;

    DCB dcbInitial;

    if(!GetCommState(handle, &dcbInitial))
    {
        return FALSE;
    }
    else
    {
        k   =   0;

        while(result<0 &&  k < 5)
        {
            switch(k)
            {
            case 0:

                baudrate_tmp =   CBR_115200;
                break;

            case 1:
                baudrate_tmp =   CBR_57600;
                break;

            case 2:
                baudrate_tmp =   CBR_38400;
                break;

            case 3:
                baudrate_tmp =   CBR_19200;
                break;

            case 4:
            default:
                baudrate_tmp =   CBR_9600;
                break;
            }   /* switch */

            if(ERROR_SUCCESS    !=  ChangeHostBaudrate(handle, baudrate_tmp))
            {
                if(baudrate_tmp<=CBR_115200)
                {
                    result  =   0;
                }
            }
            else
            {

                can_change  =   TAMABaudrateChange(handle, baudrate, &can_communicate);

                if(!can_change)
                {
                    if(can_communicate)
                    {
                        baudrate    =   baudrate_tmp;
                        result      =   1;
                    }
                }
                else
                {
                    Sleep(20);
                    if(ERROR_SUCCESS    ==  ChangeHostBaudrate(handle, baudrate))
                    {
                        result      =   1;
                    }
                    else
                    {
                        result      =   0;
                    }
                }

                if(Asleep)
                {
                    if(!Serial232Write(NULL, handle, &FrameAsleep[0], sizeof(FrameAsleep), &dwDone, T_RS232_RETRY, T_RS232_TIMEOUT_DEFAULT))
                    {
                        return FALSE;
                    }
                }
            }
        k++;
        }
    }

    if(result<1)
    {
        /*  reset intial baudrate   */
        ChangeHostBaudrate(handle, dcbInitial.BaudRate);
        SetCommState(handle, &dcbInitial);
        return FALSE;
    }
    else
        return TRUE;

}
/* PN532 v1.5: send a Wakeup frame to deactivate the low VBAT mode of PN532 v1.5
 * This command has no effect on other boards
 * As the PN532 v1.5 is in Low VBat mode by default, we send a long preamble 0x55,0x55
 * following by a defined delay (0x00 0x00 0x00 0x00)
 * before changing the virtual mode into normal mode */
BOOL SendSerialCommand(HANDLE handle, uint8_t *FrameInOut, uint16_t uiBufferInSize, uint8_t *can_communicate)
{
    uint8_t     FrameAck[]      =   {0x00, 0x00,0xFF,0x00,0xFF,0x00};
    uint8_t     FrameError[]    =   {0x00,0x00,0xFF,0x01,0xFF,0x7F,0x81,0x00};
    DWORD       dwDone;

    *can_communicate =   0;

    /* Write the Command to TAMA */
    if(!Serial232Write( NULL, handle, &FrameInOut[0], uiBufferInSize, &dwDone, T_RS232_RETRY, T_RS232_TIMEOUT_DEFAULT))
    {
        return FALSE;
    }

    /* Read back the acknowledgement */
    if(!Serial232Read(NULL, handle, &FrameInOut[0], sizeof(FrameAck), &dwDone, T_RS232_RETRY, T_RS232_TIMEOUT_DEFAULT, 0))
    {
        return FALSE;
    }

    /* verify if we have an ACK */
    if (memcmp(FrameInOut,FrameAck,6)!=0)
    {
        return FALSE;
    }

    /*  verify we don't have an error frame */
    if(!Serial232Read(NULL, handle, &FrameInOut[0], sizeof(FrameError), &dwDone, T_RS232_RETRY, T_RS232_TIMEOUT_DEFAULT, 0))
    {
        return FALSE;
    }

    *can_communicate =   1;

    if (memcmp(FrameInOut,FrameError,sizeof(FrameError))==0)
    {
        return FALSE;
    }

    /*  Good. Now, let's read the response */
    if(!Serial232Read(NULL, handle, FrameInOut+sizeof(FrameError), FrameInOut[3]-1, &dwDone, T_RS232_RETRY, T_RS232_TIMEOUT_DEFAULT, 0))
    {
        return FALSE;
    }

    return TRUE;
}

/* PN532 v1.5: send a Wakeup frame to deactivate the low VBAT mode of PN532 v1.5
 * This command has no effect on other boards
 * As the PN532 v1.5 is in Low VBat mode by default, we send a long preamble 0x55,0x55
 * following by a specified delay (0x00 0x00 0x00 0x00) & then the command */
BOOL TAMAWakeUp(HANDLE handle, uint8_t *can_communicate)
{
    uint8_t     FrameWakeup[]   =   {0xAA,0xAA,0x00,0x00,0x00,0x00,0x00,0x00,0xFF,0x02,0xFE,0xD4,0x02,0x2A,0x00};

    if(!SendSerialCommand(handle, &FrameWakeup[0], sizeof(FrameWakeup), can_communicate))
    {
        return FALSE;
    }

    if(     0xD5    !=  FrameWakeup[5]
        ||  0x03    !=  FrameWakeup[6])
    {
        return FALSE;
    }

    /* Set the PN532 v1.5 into Virtual Mode with IRQ (exit LowVBat mode)*/
    if(     0x32    ==  FrameWakeup[7]
            ||  0x01    !=  FrameWakeup[8]
            ||  0x05    !=  FrameWakeup[9])
    {
        uint8_t     FrameSwitchToNormal[]   =   {0xAA,0xAA,0x00,0x00,0x00,0x00,0x00,0x00,0xFF,0x03,0xFD,0xD4,0x14,0x02,0x00,0x01,0x15,0x00};

        if(!SendSerialCommand(handle, &FrameSwitchToNormal[0], sizeof(FrameSwitchToNormal), can_communicate))
        {
            return FALSE;
        }
    }

    return TRUE;
}

/*! Changes baud rate of TAMA, according to baudrate.
 *  can_communicate: informs the caller whether the communication is possible.
 *  Returns TRUE if baudrate was successfully set
 */

BOOL TAMABaudrateChange      (   HANDLE handle, unsigned long baudrate, uint8_t* can_communicate)
{

    uint8_t     FrameInOut          [0xFF];
    uint8_t     FrameAck[]      =   {0x00, 0x00,0xFF,0x00,0xFF,0x00};
    uint8_t     FrameError[]    =   {0x00,0x00,0xFF,0x01,0xFF,0x7F,0x81,0x00};
    uint8_t     FrameWakeup[]   =   {0x55,0x55,0x00,0x00,0x00,0x00,0x00,0xFF,0x03,0xFD,0xD4,0x14,0x01,0x17,0x00};/* 5 rising edge necessary to wake up PN532 1.5*/
    uint16_t    uiBufferInSize  =   0;
    uint8_t     pBufferIn           [20];
    DWORD       dwDone;
    uint8_t     uiCmd;
    uint16_t    sum =   0;

    int i;

    *can_communicate =   0;

    /*  We send a wakeup frame followed by a wake up delay
        so that the PN532 is waken up if connected onto this serial port
        This command has no effect onto PN511, PN512, PN531 4.2 & PN532 1.4
    */
    if(!Serial232Write(NULL, handle, &FrameWakeup[0], sizeof(FrameWakeup), &dwDone, T_RS232_RETRY, T_RS232_TIMEOUT_DEFAULT))
    {
        return FALSE;
    }

    Sleep(50);

    if(CBR_9600     ==  baudrate)
        pBufferIn[0]    =   0x00;
    else
    if(CBR_19200    ==  baudrate)
        pBufferIn[0]    =   0x01;
    else
    if(CBR_38400    ==  baudrate)
        pBufferIn[0]    =   0x02;
    else
    if(CBR_57600    ==  baudrate)
        pBufferIn[0]    =   0x03;
    else
    if(CBR_115200   ==  baudrate)
        pBufferIn[0]    =   0x04;
    else
    if(230400   ==  baudrate)
        pBufferIn[0]    =   0x05;
    else
    if(460800   ==  baudrate)
        pBufferIn[0]    =   0x06;
    else
    if(921600   ==  baudrate)
        pBufferIn[0]    =   0x07;
    else
    {
        /*  For the moment, we don't allow higher baudrate
            since we assume this function will be used for TAMA communication via RS232 */
        return FALSE;
    }

    uiCmd   =   0x10;
    uiBufferInSize  =   1;

    /* Preambule */
    FrameInOut[0] = 0x00;
    /* SYNC bytes */
    FrameInOut[1] = 0x00;
    FrameInOut[2] = 0xFF;
    /* LEN */
    FrameInOut[3] = (uint8_t)(uiBufferInSize + 2);
    /* LCS */
    FrameInOut[4] = (uint8_t)(0x00 - FrameInOut[3]);
    /* TFI */
    FrameInOut[5] = 0xD4;
    /* Command Tama */
    FrameInOut[6] = uiCmd;
    /* Data */
    memcpy( FrameInOut+7, pBufferIn, uiBufferInSize );
    /* DCS */
    for( i=5; i<uiBufferInSize+7; i++ )
        sum = (uint16_t)(sum + FrameInOut[i]);
    FrameInOut[uiBufferInSize+7] = (uint8_t)(0x00 - sum);
    /* Postambule */
    FrameInOut[uiBufferInSize+8] = 0x00;


    if(!SendSerialCommand(handle, &FrameInOut[0], (uint16_t)(uiBufferInSize + 9), can_communicate))
    {
        return FALSE;
    }

    if(     0xD5    !=  FrameInOut[5]
        ||  0x11    !=  FrameInOut[6])
    {
        return FALSE;
    }

    /*  We have a SetbaudRate response.
        Now, send our ACK, so that TAMA really does the baudrate change.
    */
    if(!Serial232Write(NULL, handle, &FrameAck[0], sizeof(FrameAck), &dwDone, T_RS232_RETRY, T_RS232_TIMEOUT_DEFAULT))
    {
        return FALSE;
    }

    return TRUE;
}

/*! Aborts pending IO
 */

void TAMAAbortDriverIo(phHal_sHwReference_t    *psHwReference, HANDLE Handle, OVERLAPPED*  pOsRead, uint32_t*    pNumberOfBytesRead)
{
    //Abort the Driver IO
    if (Handle != INVALID_HANDLE_VALUE)
    {
        uint8_t pBuffer[] = { 0x00, 0x00, 0xFF, 0x00, 0xFF, 0x00 };
        uint32_t nNumberOfBytesToWrite = 6;
        uint32_t NumberOfBytesWritten = 0;
        BOOL MyResult;
        BOOL resultOfWrite;
        OVERLAPPED  osWrite;
        osWrite.Offset = 0;
        osWrite.OffsetHigh = 0;
        osWrite.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);

        //We should cancel the pending I/O first
        MyResult = GetOverlappedResult(Handle, pOsRead, pNumberOfBytesRead, FALSE);
        if( !MyResult )
        {
            CancelIo( Handle );
        }

        if (osWrite.hEvent == INVALID_HANDLE_VALUE)
        {
            resultOfWrite = FALSE;
        }
        else
        {
                if( Handle != INVALID_HANDLE_VALUE )
                {
                    //We send the 'ACK frame to abort the current TAMA process
                    phDalNfc_FlushUSBRequest(psHwReference);
                    resultOfWrite = WriteFile( Handle, pBuffer, (uint32_t)nNumberOfBytesToWrite, &NumberOfBytesWritten, &osWrite);
                    phDalNfc_FlushUSBCancel(psHwReference);
                    if( !resultOfWrite )
                    {
                        resultOfWrite =     (GetLastError() == ERROR_IO_PENDING)
                                        &&  (WaitForSingleObject (osWrite.hEvent, 200) == WAIT_OBJECT_0);
                        if( resultOfWrite )
                        {
                            GetOverlappedResult(Handle, &osWrite, &NumberOfBytesWritten, FALSE);
                        }
                    }

                }
                else
                {
                    resultOfWrite = FALSE;
                }
                CloseHandle(osWrite.hEvent);
        }

    }
}
/*! Waits for either pending IO or Abort event (if allowAbort!=0).
 *  pOsRead: Overlapped data referring to IO
 *  nTimemout: maximum timeout to wait
 *  allowAbort: Allows waiting on Abort Event
 *  Returns TRUE if exits on IO completion
 *  Returns FALSE if exits on either Abort event, or Timeout
 */

BOOL TAMAWaitForIo(    phHal_sHwReference_t    *psHwReference,
                       OVERLAPPED              *pOsRead,
                       uint32_t                 nTimeout,
                       uint8_t                  allowAbort)
{
    HANDLE  events[2];
    DWORD   waitForObjRes   =   0;
    BOOL        bResult     =   (GetLastError() == ERROR_IO_PENDING);

    if(!bResult)
        return bResult;

    events[0] = pOsRead->hEvent;
    if(NULL ==  psHwReference)
        events[1] = INVALID_HANDLE_VALUE;
    else
        events[1] = (HANDLE)psHwReference->DalContext.AbortEvent;

    if(     !allowAbort
        ||  !psHwReference->Flags.AbortAllowed
        ||  INVALID_HANDLE_VALUE    == events[1])
    {
        /*  We don't allow Abort Event when reading an ACK, since TAMA's behavior is not guaranteed
            to be safe it we send 2 consecutive commands before receiving the ACK */
        waitForObjRes  =   WaitForMultipleObjects (1, events, FALSE, nTimeout);
    }
    else
    {
        /*  Check whether the abort has been allowed by the HAL. */
        if(1    ==  psHwReference->Flags.AbortAllowed)
        {
            /* This is the first time we get within the DAL, for this HAL function.
               Then, Reset the Event that allows the abort, so that any previous call to the Abort() function
               that had not been taken into account is not considered within this HAL function call
            */

            ResetEvent((HANDLE)psHwReference->DalContext.AbortEvent);

            /* Set the AbortAllowed flag to another value than 1, so that we dont pass in this branch again
            */

            psHwReference->Flags.AbortAllowed   =   2;
        }

        /*  We only allow Abort Event when *not* reading an ACK */
        if(INVALID_HANDLE_VALUE ==  events[1])
        {
            waitForObjRes  =   WaitForMultipleObjects (1, events, FALSE, nTimeout);
        }
        else
        {
            waitForObjRes  =   WaitForMultipleObjects (2, events, FALSE, nTimeout);
        }

    }
    switch(waitForObjRes)
    {
    case WAIT_OBJECT_0:
        break;
    case WAIT_OBJECT_0+1:
        break;
    case WAIT_TIMEOUT:
        break;
    default:
        assert(0);
        break;

    }

    bResult =       bResult &&  (WAIT_OBJECT_0  ==  waitForObjRes);

    return bResult;
}

NFCSTATUS phDalNfc_Abort(phHal_sHwReference_t *psHwReference)
{
    HANDLE    AbortEvent = (HANDLE)psHwReference->DalContext.AbortEvent;
    NFCSTATUS status     = NFCSTATUS_SUCCESS;

    if (NULL == psHwReference)
    {
        status = PHNFCSTVAL(CID_NFC_DAL, NFCSTATUS_INVALID_PARAMETER);
    } else
    {
        /* Check whether Abort is allowed: */
        if (!psHwReference->Flags.AbortAllowed)
        {
            status = PHNFCSTVAL(CID_NFC_DAL, NFCSTATUS_INVALID_DEVICE_REQUEST);
        } else
        {
            if (SetEvent(AbortEvent) != 0)
            {
                /* OK */
            } else
            {
                /* Set Event failed: */
                status = PHNFCSTVAL(CID_NFC_DAL, NFCSTATUS_INVALID_DEVICE);
            }
        }
    }

    return status;
}


/*! For documentation, see header file
 *
 */
extern NFCSTATUS phDalNfc_Enumerate(phHal_sHwReference_t  sHwReference[],
                                    uint8_t              *pNbrOfDevDetected )
{
    NFCSTATUS   result          = NFCSTATUS_SUCCESS;
    uint8_t     t = 0, j = 0, k = 0;
    uint8_t     num_devs        = 0;
    int8_t      sLinkNameUsb[]  = "\\\\.\\PN531 NFC Device0";
    int8_t      sLinkNameSer[]  = "COMx:";
    void*       hLink           = NULL;
    #ifdef _UNICODE
    wchar_t     sLinkName[MAX_PATH];
    #else
    int8_t      sLinkName[MAX_PATH];
    #endif

    #ifdef PHJALJOINER
        phcsBflRegCtl_GetRegParam_t  getreg_param;
        phcsBflRegCtl_SetRegParam_t  setreg_param;
        phDal_sBFLLowerFrame_t      *temp_bfl_frame   = NULL;
        HANDLE                       h_serial_port;
        const uint16_t               timeout              = 200;
        uint8_t                      version              = 0,
                                     version_ok           = 0,
                                     version_loop         = 0;
        uint8_t                      version_for_dal_ctxt;
        uint8_t                      version_array[]      = PHCS_BFLOPCTL_HW_VERSIONS;
        phcsBfl_Status_t             rclstatus            = PH_ERR_BFL_SUCCESS;
    #endif /* PHJALJOINER */

    if ((sHwReference == NULL) || (*pNbrOfDevDetected == 0))
    {
        return PHNFCSTVAL(CID_NFC_DAL, NFCSTATUS_INVALID_PARAMETER);
    }

    memset(sHwReference, 0x0, sizeof(sHwReference[0])*(*pNbrOfDevDetected));

    #ifdef PHTALTAMA

    /* FIRST: Search for USB PN531 devices: */
    /* Create a handle to the driver */
    for( t = 0; t < MAX_USB_DEVICES; t++)
    {
        sLinkNameUsb[strlen(sLinkNameUsb)-1] = (uint8_t) ('0' + t);

        #ifdef _UNICODE
        MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, sLinkNameUsb, -1,sLinkName, sizeof(sLinkName)/sizeof(sLinkName[0]));
        #else
        strcpy(sLinkName, sLinkNameUsb);
        #endif

        hLink =  (void*)CREATEFILE( sLinkName,
                                    GENERIC_READ | GENERIC_WRITE,
                                    FILE_SHARE_READ,
                                    NULL,
                                    OPEN_EXISTING,
                                    FILE_FLAG_OVERLAPPED,
                                    NULL);

        if ( hLink ==  INVALID_HANDLE_VALUE )
        {
            continue;
        }
        else
        {
            CloseHandle(hLink);

            if( *pNbrOfDevDetected <(num_devs+1))
                continue;
            else
            {

                phHal_sHwReference_t *pHwRef = &sHwReference[num_devs];
                phDal_Context_t * pDalContext =  &(sHwReference[num_devs].DalContext);

                pHwRef->pBoardDriver  = NULL;
                pHwRef->Flags.LinkValid = 0;
                memset( pDalContext->sLinkName, 0, sizeof(pDalContext->sLinkName));
                strcpy( pDalContext->sLinkName, sLinkNameUsb);
                pDalContext->AbortEvent     =   NULL;
                sHwReference[num_devs].ChipRevision =   0;  /*  Will be filled later on within the open function    */
                sHwReference[num_devs].ChipName     =   phHal_eChipName531;
                pHwRef->CompType = CID_NFC_TAL;
                pHwRef->DeviceNbr = num_devs;
                num_devs++;
                pHwRef->LinkType = PHHAL_USB_DEVICE;
            }
        }
    }

    /* SECOND: Search for SERIAL PN531 or PN532 devices: */

    for (j = 0; j < MAX_J_RS232_DEVICES; j++)
    {
        sLinkNameSer[strlen(sLinkNameSer) - 2] = (int8_t)('1' + j);

        #ifdef _UNICODE
        MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, sLinkNameSer, -1,sLinkName, sizeof(sLinkName)/sizeof(sLinkName[0]));
        #else
        strcpy(sLinkName, sLinkNameSer);
        #endif

        hLink =  (void*)CREATEFILE( sLinkName,
                                    GENERIC_READ | GENERIC_WRITE,
                                    0,
                                    0,
                                    OPEN_EXISTING,
#if T_RS232_USEOVERLAPPED
                                    FILE_FLAG_OVERLAPPED,
#else
                                    0,
#endif
                                    0);

        if (hLink != INVALID_HANDLE_VALUE)
        {

            BOOL                targetFound =   FALSE;

            targetFound     =   TAMABaudrateMatchWithHost( hLink, CBR_115200, 1);

            if(targetFound)
            {
                /*  finally, we succeeded to find the PN531 target , since
                    we've succedded to set its baudrate to 115200.
                    Let's also change the host baudrate to 115200 */
                if( *pNbrOfDevDetected >=(num_devs+1))
                {

                    phHal_sHwReference_t *pHwRef = &sHwReference[num_devs];
                    phDal_Context_t * pDalContext =  &(sHwReference[num_devs].DalContext);

                    pHwRef->pBoardDriver  = NULL;
                    pHwRef->Flags.LinkValid = 0;
                    memset( pDalContext->sLinkName, 0, sizeof(pDalContext->sLinkName));
                    strcpy(pDalContext->sLinkName, sLinkNameSer);
                    pDalContext->AbortEvent     =   NULL;
                    sHwReference[num_devs].Sleeping =   1;
                    sHwReference[num_devs].ChipRevision =   0;  /*  Will be filled later on within the open function    */
                    sHwReference[num_devs].ChipName     =   phHal_eChipName53x;
                    pHwRef->CompType = CID_NFC_TAL;
                    pHwRef->DeviceNbr = num_devs;
                    num_devs++;
                    pHwRef->LinkType = PHHAL_SERIAL_DEVICE;
                }
            }

            CloseHandle(hLink);

        }
        else
        {
            /* Port in use or does not exist. Do nothing.*/
        }
    }

    #endif


    /* THIRD: Search for PN511 Boards on RS 232:
     * This is SAMPLE code only: It is not reliably possible to detect PN511 boards (no PnP).
     * Therefore we try the following for each serial port found to be free:
     *
     * 1. Open the port.
     * 2. Read Version register 0x37 @ 9600bps and check for upper nibble to be 2.
     * 3. If no success try the same @ 115200 bps.
     * 4a. If still no success step back to 9600 bps, bail out and go to the next port...
     * 4b. Issue a SoftReset @ current bit rate.
     * 5. Step back to 9600 bps.
     * 6. Read register 0x01 and check for 0x20 (initial value).
     * 7. Read register 0x0B and check for 0x08 (initial value).
     * 8. Close the port.
     * 9. In case of an error or invalid data consider the board not present.
     *
     * Remarks: 1. If the HW Reference array is already full do the same thing but discard
     *             the data.
     *          2. The chip baudrate is not changed by this function using the explicit command.
     *             As a side-effect of a SOFTRESET it is brought down to 9600 bps.
     *          3. It is assumed to find the chip responsive @ 9600bps. If not another try is
     *             gives @ 115200bps (see algorithm).
     */
    #ifdef PHJALJOINER
    temp_bfl_frame = malloc(sizeof(phDal_sBFLLowerFrame_t));
    if ((result == NFCSTATUS_SUCCESS) &&
        (temp_bfl_frame != NULL))
    {
        phDal_BuildBflLowerFrame(temp_bfl_frame);
        for (j = 0; j < MAX_J_RS232_DEVICES; j++)
        {
            sLinkNameSer[strlen(sLinkNameSer) - 2] = (uint8_t)('1' + j);

            #ifdef _UNICODE
            MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, sLinkNameSer, -1,sLinkName, sizeof(sLinkName)/sizeof(sLinkName[0]));
            #else
            strcpy(sLinkName, sLinkNameSer);
            #endif

            h_serial_port =
                CREATEFILE(sLinkName,
                            GENERIC_READ | GENERIC_WRITE,
                            0,                   /* Share mode */
                            NULL,                /* Pointer to the security attribute */
                            OPEN_EXISTING,
                            FILE_FLAG_OVERLAPPED,
                            NULL);
            if (h_serial_port != INVALID_HANDLE_VALUE)
            {
                /* Free Port found: Init port and BFL instance. */
                /* Init Port (timing) - Fire&Forget: No return value check. */
                SetComPortTimeouts(h_serial_port, timeout);

                /* Tell BAL about port: */
                ((phcsBflBal_Hw1SerWinInitParams_t*)
                    temp_bfl_frame->Bal.mp_Members)->com_handle = h_serial_port;

                /* Prepare: */
                getreg_param.self    = &(temp_bfl_frame->RcRegCtl);
                setreg_param.self    = getreg_param.self;

                /* Probe chip: */
                /* Read version @ 9600 & 115200 bps. */
                for (k = 0; k < 1; k++)
                {
                    /* 115k currently disabled!! */
                    switch (k)
                    {
                        case 0:
                            /* Config PORT ONLY to 9600 bps : */
                            ChangeHostBaudrate(h_serial_port, CBR_9600);
                            /* Try to recover PN51x in case it has gone out of sync due to previous actions (port, PN53x enum, etc.): */
                            rclstatus = phcsBflDal_RecoverPN51x(&(temp_bfl_frame->RcRegCtl));
                            break;

                        case 1:
                        default:
                            /*! Config PORT ONLY to 115200 bps: */
                            ChangeHostBaudrate(h_serial_port, CBR_115200);
                            break;
                    }
                    /* Double-read version and see whether it matches: */

                    /* First attempt: */
                    getreg_param.address = PHCS_BFL_JREG_VERSION;
                    rclstatus = temp_bfl_frame->RcRegCtl.GetRegister(&getreg_param);
                    version = getreg_param.reg_data;

                    if (rclstatus == PH_ERR_BFL_SUCCESS)
                    {
                        /* OK, read again: */
                        getreg_param.address = PHCS_BFL_JREG_VERSION;
                        rclstatus = temp_bfl_frame->RcRegCtl.GetRegister(&getreg_param);
                        if ((rclstatus == PH_ERR_BFL_SUCCESS) && (version == getreg_param.reg_data))
                        {
                            // Double-read OK and has given a match!
                            version_ok = 1;
                            /* Record the version for later usage (DAL-Ctxt. and Dev. Caps.): */
                            version_for_dal_ctxt = getreg_param.reg_data;
                            /* Loop could read and got back matching data: */
                            break;
                        } else
                        {
                            // Error: Whatever we got here, it appears not to work (properly).
                            version_ok = 0;
                        }
                    } else
                    {
                        /* No success: Perhaps next time... */
                        version_ok = 0;
                    }
                } /* for.. */

                /*
                 * Check whether the version we have got is actually supported by the BFL.
                 * We go through the BFL SUPPORTED version array until its last element:
                 */

                if (version_ok)
                {
                    version_ok   = 0;
                    version_loop = 0;
                    do
                    {
                        if (version_ok != 0)
                        {
                            break;
                        }
                        if (version == version_array[version_loop])
                        {
                            // YES, previously retrieved version is valid.
                            version_ok = 1;
                        }

                        version_loop++;
                    } while (version_array[version_loop] != PHCS_BFLOPCTL_HW1VAL_UNDEF_HW);
                }

                /* Finally, assign values, if we have the OK from above. */
                if (version_ok)
                {
                    /* YES, it's the chip (very likely): Enter values if possible
                       (parameter value still unmodified): */
                    if (*pNbrOfDevDetected > num_devs)
                    {
                        /* Insert still missing HW Reference Information: */
                        sHwReference[num_devs].CompType                 = CID_NFC_JAL;
                        sHwReference[num_devs].DeviceNbr                = j;
                        sHwReference[num_devs].Flags.LinkValid          = 0; /* false */
                        sHwReference[num_devs].LinkType                 = PHHAL_SERIAL_DEVICE;
                        strcpy(sHwReference[num_devs].DalContext.sLinkName, sLinkNameSer);
                        sHwReference[num_devs].ChipRevision             = version_for_dal_ctxt;
                        switch (version_for_dal_ctxt)
                        {
                            case PHCS_BFLOPCTL_HW1VAL_VERS1:
                            case PHCS_BFLOPCTL_HW1VAL_VERS2:
                            case PHCS_BFLOPCTL_HW1VAL_VERS3:
                            case PHCS_BFLOPCTL_HW1VAL_VERS4:
                            case PHCS_BFLOPCTL_HW1VAL_VERS7:
                                sHwReference[num_devs].ChipName = phHal_eChipName511;
                                break;

                            case PHCS_BFLOPCTL_HW1VAL_VERS5:
                            case PHCS_BFLOPCTL_HW1VAL_VERS6:
                                sHwReference[num_devs].ChipName = phHal_eChipName512;
                                break;

                            case PHCS_BFLOPCTL_HW1VAL_VERS8:
                            case PHCS_BFLOPCTL_HW1VAL_VERS9:
                                sHwReference[num_devs].ChipName = phHal_eChipName522;
                                break;

                            case PHCS_BFLOPCTL_HW1VAL_VERS10:
                                sHwReference[num_devs].ChipName = phHal_eChipName523;
                                break;

                            default:
                                sHwReference[num_devs].ChipName = phHal_eChipNameUndefined;
                                break;
                        }
                        sHwReference[num_devs].FirmwareVersion  = 0;
                        sHwReference[num_devs].FirmwareRevision = 0;
                        /* Enum makes board not immediately usable (refer to Open): */
                        sHwReference[num_devs].pBoardDriver             = NULL;
                        sHwReference[num_devs].DalContext.BflLowerFrame = NULL;
                    }
                    else
                    {
                        /* HW Refs. full: Cannot assign more - do nothing. */
                    }
                    num_devs++;
                } else
                {
                    /* Nothing to do because the retrieved version is unsupported. */
                }
                /* Close the handle: */
                CloseHandle(h_serial_port);
            } else
            {
                /* Port in use or does not exist. Do nothing.*/
            }
        } /* for (..) */
        if (temp_bfl_frame != NULL)
        {
            free(temp_bfl_frame);
        }
    } else
    {
        /* Previous error (from TAMA detection) or alloc failure. */
        if (temp_bfl_frame == NULL)
        {
            rclstatus = PH_ERR_BFL_INSUFFICIENT_RESOURCES;
        }
    }
    #endif /* PHJALJOINER */

    /* Fill unused HW References to indicate that they are unused: */
    for (j = num_devs; j < *pNbrOfDevDetected; j++)
    {
        sHwReference[j].CompType = CID_NFC_NONE;
    }

    *pNbrOfDevDetected = num_devs;
    return result;
}



/*! For documentation, see header file
 *
 */
extern NFCSTATUS phDalNfc_Open(phHal_sHwReference_t *psHwReference)
{
    NFCSTATUS result                = NFCSTATUS_SUCCESS;
    HANDLE Handle, AbortHandle;
    phDal_Context_t * pDalContext;
#ifdef _UNICODE
    wchar_t sLinkName[MAX_PATH];
#else
    int8_t  sLinkName[MAX_PATH];
#endif

    #ifdef PHJALJOINER
        HANDLE                      h_serial_port;
        phDal_sBFLLowerFrame_t      *temp_bfl_lower_frame  = NULL;
        phJal_sBFLUpperFrame_t      *temp_bfl_upper_frame  = NULL;
        phcsBfl_Status_t             rclstatus = PH_ERR_BFL_SUCCESS;
        OVERLAPPED                  *overlapped_structure  = NULL;
        const uint8_t                jbaud_115200 = PHDAL_JBAUD_115K;
        const uint8_t                initiator = 1;
        const uint8_t                do_not_init_chip = 0;   /* Prevent attempt to init chip! */
    #endif /* PHJALJOINER */
    

    uint8_t   version[5] = {0x00, 0x00, 0x00, 0x00, 0x00};
    uint32_t  lenght;

    if (psHwReference == NULL)
    {
        result = PHNFCSTVAL(CID_NFC_DAL, NFCSTATUS_INVALID_PARAMETER);
    } else
    {
        if (psHwReference->Flags.LinkValid)
        {
            result = PHNFCSTVAL(CID_NFC_DAL, NFCSTATUS_INVALID_DEVICE);
        }
    }

    /* Open a new link to the device (driver) */
    if (result == NFCSTATUS_SUCCESS)
    {
        pDalContext             = &psHwReference->DalContext;

        switch (psHwReference->CompType)
        {
            case CID_NFC_JAL:
                #ifdef PHJALJOINER
                /* Set to FALSE by default: */
                psHwReference->Flags.LinkValid   = 0; /* false */
                temp_bfl_lower_frame = malloc(sizeof(phDal_sBFLLowerFrame_t));
                temp_bfl_upper_frame = malloc(sizeof(phJal_sBFLUpperFrame_t));
                overlapped_structure = malloc(sizeof(OVERLAPPED));

                if ((temp_bfl_lower_frame != NULL) &&
                    (temp_bfl_upper_frame != NULL) &&
                    (overlapped_structure != NULL))
                {
                    /* Zero out structures to avoid problems with OVR operations or
                       other initialisations: */
                    memset(temp_bfl_lower_frame, 0, sizeof (phDal_sBFLLowerFrame_t));
                    memset(temp_bfl_upper_frame, 0, sizeof (phJal_sBFLUpperFrame_t));
                    memset(overlapped_structure, 0, sizeof (OVERLAPPED));

                    /* Assign BFL Container to HW Ref,#
                       but chip cannot be init'ed since no connection yet... */
                    phDal_BuildBflLowerFrame(temp_bfl_lower_frame);
                    phJal_BuildBflUpperFrame(temp_bfl_upper_frame,
                                                    initiator,
                                             &(temp_bfl_lower_frame->RcRegCtl),
                                             do_not_init_chip);

                    overlapped_structure->hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
                    temp_bfl_upper_frame->IoOverlappedStructure = overlapped_structure;

                    psHwReference->pBoardDriver = temp_bfl_upper_frame;
                    pDalContext->BflLowerFrame = temp_bfl_lower_frame;
                    strcpy(temp_bfl_lower_frame->DeviceName, psHwReference->DalContext.sLinkName);
                    /* This opens the prepared connection in HW Ref! */

                    #ifdef _UNICODE
                    MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, pDalContext->sLinkName, -1,sLinkName, sizeof(sLinkName)/sizeof(sLinkName[0]));
                    #else
                    strcpy(sLinkName, pDalContext->sLinkName);
                    #endif

                    h_serial_port =
                        CREATEFILE(sLinkName,
                                    GENERIC_READ | GENERIC_WRITE,
                                    0,                   /* Share mode */
                                    NULL,                /* Pointer to the security attribute */
                                    OPEN_EXISTING,
                                    FILE_FLAG_OVERLAPPED,
                                    NULL);

                    if (h_serial_port != INVALID_HANDLE_VALUE)
                    {
                        psHwReference->pBoardDriver = temp_bfl_upper_frame;
                        /* Tell BFL about port: */
                        ((phcsBflBal_Hw1SerWinInitParams_t*)
                            (temp_bfl_lower_frame->Bal.mp_Members))->com_handle = h_serial_port;

                        /* Try to recover PN51x in case it has gone out of sync due to previous actions (port, PN53x enum, etc.): */
                        rclstatus = phcsBflDal_RecoverPN51x(&(temp_bfl_lower_frame->RcRegCtl));

                        if (phDalNfc_Ioctl(psHwReference, PHHALNFC_IOCTL_PN511_SOFTRESET, NULL, 0, NULL, NULL) == NFCSTATUS_SUCCESS)
                        {
                            /* OK */
                            psHwReference->Flags.LinkValid   = 1;
                        } else
                        {
                            free(temp_bfl_lower_frame);
                            free(temp_bfl_upper_frame);
                            result = PHNFCSTVAL(CID_NFC_DAL, NFCSTATUS_INVALID_DEVICE);
                        }
                    }
                    else
                    {
                        free(temp_bfl_lower_frame);
                        free(temp_bfl_upper_frame);
                        result = PHNFCSTVAL(CID_NFC_DAL, NFCSTATUS_INVALID_DEVICE);
                    }
                }
                else
                {
                    /* BFL Container alloc failure! */
                    result = PHNFCSTVAL(CID_NFC_DAL, NFCSTATUS_INSUFFICIENT_RESOURCES);
                }
                #else
                    result = PHNFCSTVAL(CID_NFC_DAL, NFCSTATUS_INVALID_DEVICE);
                #endif /* PHJALJOINER */

                break;

            case CID_NFC_TAL:
                {

                    #ifdef _UNICODE
                    MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, pDalContext->sLinkName, -1,sLinkName, sizeof(sLinkName)/sizeof(sLinkName[0]));
                    #else
                    strcpy(sLinkName, pDalContext->sLinkName);
                    #endif

                    switch(psHwReference->LinkType)
                    {

                    case PHHAL_SERIAL_DEVICE:

                         Handle =     CREATEFILE(    sLinkName,
                                                    GENERIC_READ | GENERIC_WRITE,
                                                    0,
                                                    0,
                                                    OPEN_EXISTING,
    #if T_RS232_USEOVERLAPPED
                                                    FILE_FLAG_OVERLAPPED,
    #else
                                                    0,
    #endif
                                                    0);

                        if (INVALID_HANDLE_VALUE == Handle)
                        {
                            return PHNFCSTVAL(CID_NFC_DAL, NFCSTATUS_INVALID_DEVICE);
                        }
                        else
                        {
                            /*  make sure the baudrate is set to 115200.    */
                            if(!TAMABaudrateMatchWithHost(Handle, CBR_115200, 0))
                            {
                                return PHNFCSTVAL(CID_NFC_DAL, NFCSTATUS_INVALID_DEVICE);
                            }
                            else
                            {
                                psHwReference->pBoardDriver =   Handle;
                                psHwReference->Flags.LinkValid = 0x01;
                                psHwReference->Sleeping =   1;
                            }
                        }
                        /* In case of PN 532 v1.5 or newer, we have to put the chip in Off mode after the Open is successfull */
                        break;

                    case PHHAL_USB_DEVICE:
                    default:

                        Handle =     CREATEFILE(    sLinkName,
                                                    GENERIC_READ | GENERIC_WRITE,
                                                    FILE_SHARE_READ,
                                                    NULL,
                                                    OPEN_EXISTING,
                                                    FILE_FLAG_OVERLAPPED,
                                                    NULL);

                        if (INVALID_HANDLE_VALUE == Handle)
                        {
                            return PHNFCSTVAL(CID_NFC_DAL, NFCSTATUS_INVALID_DEVICE);
                        }
                        else
                        {

                            //  Call device IO Control interface (USB_TEST_IOCTL_VERSION_NUMBER) in driver
                            if ( !DeviceIoControl(Handle,
                                                USB_TEST_IOCTL_VERSION_NUMBER,
                                                NULL,
                                                0,
                                                version,
                                                sizeof(version),
                                                &lenght,
                                                NULL)
                            )
                            {
                                return PHNFCSTVAL(CID_NFC_DAL, NFCSTATUS_INVALID_DEVICE);
                            }
                            psHwReference->pBoardDriver     = Handle;
                            psHwReference->Flags.LinkValid  = 0x01;
                                
                            /* Thread creation for USB lock problem */
                            psHwReference->DalContext.startThread = 1;
                            psHwReference->DalContext.evTimeoutStarted = CreateEvent(NULL, FALSE, FALSE, NULL);
                            psHwReference->DalContext.evAbortTimeout = CreateEvent(NULL, FALSE, FALSE, NULL);
                            psHwReference->DalContext.evReady = CreateEvent(NULL, FALSE, FALSE, NULL);
                            pDalContext->hThread = CreateThread(NULL, 0, phDalNfc_BufferUSBFlush, psHwReference, 0, NULL);

                        }

                        break;

                    }   /*  switch  */
                }

                break;

            default:
                result = PHNFCSTVAL(CID_NFC_DAL, NFCSTATUS_INVALID_DEVICE);
                break;
        }

        /* ABORT Event Creation: */
        AbortHandle = CreateEvent(NULL, FALSE, FALSE, NULL);
        if (INVALID_HANDLE_VALUE == AbortHandle)
        {
            pDalContext->AbortEvent = NULL;
            result = PHNFCSTVAL(CID_NFC_DAL, NFCSTATUS_INVALID_DEVICE);
        } else
        {
            pDalContext->AbortEvent = (void*)AbortHandle;
        }

    } else
    {
        /* Initial check failure: Do not change the state! */
    }

     return result;
}


/*! For documentation, see header file
 *
 */
extern NFCSTATUS phDalNfc_Close(phHal_sHwReference_t *psHwReference)
{
    NFCSTATUS result                = NFCSTATUS_SUCCESS;
    HANDLE    Handle             = (HANDLE)(psHwReference->pBoardDriver);
    phDal_Context_t * pDalContext   =  &psHwReference->DalContext;

    #ifdef PHJALJOINER
        phcsBflRegCtl_t                     *rc_reg_control = NULL;
        phcsBflRegCtl_SetRegParam_t         setreg_param;
        phcsBfl_Status_t                    rclstatus = PH_ERR_BFL_SUCCESS;
        const uint8_t                       jbaud_9600 = 0xEB;
        phJal_sBFLUpperFrame_t              *upper_bfl_frame =
            (phJal_sBFLUpperFrame_t*)(psHwReference->pBoardDriver);
    #endif /* PHJALJOINER */

    if (psHwReference == NULL)
    {
        result = PHNFCSTVAL(CID_NFC_DAL, NFCSTATUS_INVALID_PARAMETER);
    } else
    {
        if (psHwReference->Flags.LinkValid == 0)
        {
            result = PHNFCSTVAL(CID_NFC_DAL, NFCSTATUS_INVALID_DEVICE);
        }
    }

    if (result == NFCSTATUS_SUCCESS)
    {
        switch (psHwReference->CompType)
        {
            case CID_NFC_JAL:
                #ifdef PHJALJOINER

                    rc_reg_control =
                        ((phJal_sBFLUpperFrame_t*)psHwReference->pBoardDriver)->RcRegCtl;
                    /* Reset chip and host to low baud. */
                    setreg_param.self = rc_reg_control;
                    setreg_param.address = PHCS_BFL_JREG_SERIALSPEED;
                    setreg_param.reg_data = jbaud_9600;
                    rclstatus = rc_reg_control->SetRegister(&setreg_param);
                    Sleep(5);
                    ChangeHostBaudrate(((phcsBflBal_Hw1SerWinInitParams_t*)
                                            (((phJal_sBFLUpperFrame_t*)
                                            (psHwReference->pBoardDriver))->RcRegCtl->mp_Lower->
                                            mp_Members))->com_handle,
                                            CBR_9600);

                    /* Don't need the serial link any more:
                     * We're talking about: RegCtl->BAL->COM_HANDLE. :-)
                     */
                    if(CloseHandle(((phcsBflBal_Hw1SerWinInitParams_t*)
                                   (((phJal_sBFLUpperFrame_t*)
                                   (psHwReference->pBoardDriver))->
                                   RcRegCtl->mp_Lower->mp_Members))->com_handle))
                    {
                        result  =   NFCSTATUS_SUCCESS;
                    } else
                    {
                        result  =   PHNFCSTVAL(CID_NFC_DAL, NFCSTATUS_INVALID_DEVICE);
                    }

                    if (CloseHandle(((OVERLAPPED*)(((phJal_sBFLUpperFrame_t*)
                                    (psHwReference->pBoardDriver))->IoOverlappedStructure))->hEvent))
                    {
                        result  =   NFCSTATUS_SUCCESS;
                    } else
                    {
                        result  =   PHNFCSTVAL(CID_NFC_DAL, NFCSTATUS_INVALID_DEVICE);
                    }

                    psHwReference->Flags.LinkValid = 0; /* false */

                    if (upper_bfl_frame != NULL)
                    {
                        if (upper_bfl_frame->IoOverlappedStructure != NULL)
                        {
                            free((OVERLAPPED*)(upper_bfl_frame->IoOverlappedStructure));
                            upper_bfl_frame->IoOverlappedStructure = NULL;
                        }
                        free(upper_bfl_frame);
                        psHwReference->pBoardDriver = NULL;
                    }

                    if (pDalContext->BflLowerFrame != NULL)
                    {
                        free((phDal_sBFLLowerFrame_t*)(pDalContext->BflLowerFrame));
                        pDalContext->BflLowerFrame = NULL;
                    }

                    if (rclstatus != PH_ERR_BFL_SUCCESS)
                    {
                        /* Device is not responsive (disconnected meanwhile?). */
                        result = PHNFCSTVAL(CID_NFC_DAL, NFCSTATUS_INVALID_DEVICE);
                    }
                #else
                    result = PHNFCSTVAL(CID_NFC_DAL, NFCSTATUS_INVALID_DEVICE);
                #endif /* PHJALJOINER */

                break;

            case CID_NFC_TAL:
                psHwReference->Flags.LinkValid = 0x00;

                if(     NULL                    ==  psHwReference->pBoardDriver
                    ||  INVALID_HANDLE_VALUE    ==  psHwReference->pBoardDriver)
                {
                    result  =   PHNFCSTVAL(CID_NFC_DAL, NFCSTATUS_INVALID_PARAMETER);
                }
                else
                {

                    Handle  =   (HANDLE)psHwReference->pBoardDriver;

                    if(PHHAL_SERIAL_DEVICE  ==  psHwReference->LinkType)
                    {
                        /*  reset baudrates to 9600 on both HOST and TAMA*/
                        ChangeHostBaudrate( Handle, CBR_9600);
                    }

                    if(     PHHAL_USB_DEVICE == psHwReference->LinkType
                        &&  psHwReference->DalContext.startThread ==  1)
                    {
                        /* Close thread */
                        psHwReference->DalContext.startThread = 0;
                        SetEvent(psHwReference->DalContext.evTimeoutStarted);
                        SetEvent(psHwReference->DalContext.evAbortTimeout);
                        CloseHandle(pDalContext->hThread);
                    }

                    /*  close & reset the handle    */
                    psHwReference->pBoardDriver =   NULL;
                    if(CloseHandle(Handle))
                    {
                        result  =   NFCSTATUS_SUCCESS;
                    }
                    else
                    {
                        result  =   PHNFCSTVAL(CID_NFC_DAL, NFCSTATUS_INVALID_DEVICE);
                    }

                }

                break;

            default:
                result = PHNFCSTVAL(CID_NFC_DAL, NFCSTATUS_INVALID_DEVICE);
                break;
        }

        if( NULL    ==  pDalContext->AbortEvent)
        {
            result  =   PHNFCSTVAL(CID_NFC_DAL, NFCSTATUS_INVALID_PARAMETER);
        }
        else
        {
            if(CloseHandle((HANDLE)pDalContext->AbortEvent))
            {
                result  =   NFCSTATUS_SUCCESS;
            }
            else
            {
                result  =   PHNFCSTVAL(CID_NFC_DAL, NFCSTATUS_INVALID_DEVICE);
            }
        }


    }

    return result;
}

/*! For documentation, see header file
 *
 */
extern NFCSTATUS phDalNfc_Write(phHal_sHwReference_t *psHwReference,
                                uint8_t              *pBuffer,
                                uint16_t              nNumberOfBytesToWrite,
                                uint16_t             *pNumberOfBytesWritten)
{
    NFCSTATUS   result = NFCSTATUS_SUCCESS;
    HANDLE      Handle = (HANDLE)(psHwReference->pBoardDriver);
    BOOL        resultOfWrite = FALSE;
    

    switch (psHwReference->CompType)
    {
        case CID_NFC_JAL:
            /*! \todo Add the JAL code for WRITE here! */
            result = PHNFCSTVAL(CID_NFC_DAL, NFCSTATUS_INVALID_DEVICE);
            break;

        case CID_NFC_TAL:
        {
            uint32_t NumberOfBytesWritten;

            if(PHHAL_SERIAL_DEVICE  ==  psHwReference->LinkType)
            {
                /*  SERIAL Link */

                resultOfWrite = Serial232Write (psHwReference, Handle, pBuffer, nNumberOfBytesToWrite, &NumberOfBytesWritten, T_RS232_RETRY, T_RS232_TIMEOUT_DEFAULT);
            }
            else
            {
                /*  USB Link */
    
                OVERLAPPED  osWrite = {0};
                osWrite.Offset = 0;
                osWrite.OffsetHigh = 0;
                osWrite.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
                if (osWrite.hEvent == INVALID_HANDLE_VALUE)
                {
                    resultOfWrite = FALSE;
                }
                else
                {
                        if( Handle != INVALID_HANDLE_VALUE )
                        {
                            phDalNfc_FlushUSBRequest(psHwReference);
                            resultOfWrite = WriteFile( Handle, pBuffer, (uint32_t)nNumberOfBytesToWrite, &NumberOfBytesWritten, &osWrite); 
                            phDalNfc_FlushUSBCancel(psHwReference);       
                            if( !resultOfWrite )
                            {
                                resultOfWrite = (GetLastError() == ERROR_IO_PENDING) &&
                                        (WaitForSingleObject (osWrite.hEvent, 200) == WAIT_OBJECT_0);
                                if( resultOfWrite )
                                {
                                    GetOverlappedResult(Handle, &osWrite, &NumberOfBytesWritten, FALSE);
                                }
                            }

                            *pNumberOfBytesWritten = (uint16_t)NumberOfBytesWritten;
                        }
                        else
                        {
                            resultOfWrite = FALSE;
                        }
                }
            }

            if(resultOfWrite)
            {
                /*  If the function succeeds, the return value is nonzero */
                result = NFCSTATUS_SUCCESS;
            }
            else
            {
                /*  If the function fails, the return value is zero.
                    To get extended error information, call GetLastError.*/
                /*  But, here, the return values of the GetLatError function
                    clash(same as) with the existing error values.
                    Hence NFCSTATUS_INVALID_DEVICE is being returned */
                result = PHNFCSTVAL(CID_NFC_DAL, NFCSTATUS_INVALID_DEVICE);
            }
            break;
        }
        default:
            result = PHNFCSTVAL(CID_NFC_DAL, NFCSTATUS_INVALID_DEVICE);
            break;
    }

    return result;
}



/*! For documentation, see header file
 *
 */
extern NFCSTATUS phDalNfc_Read(phHal_sHwReference_t *psHwReference,
                               uint8_t              *pBuffer,
                               uint16_t              nNumberOfBytesToRead,
                               uint16_t             *pNumberOfBytesRead,
                               uint32_t              nTimeout)
{
    NFCSTATUS result = NFCSTATUS_SUCCESS;
    HANDLE Handle = (HANDLE)(psHwReference->pBoardDriver);

    *pNumberOfBytesRead =   0;

    switch (psHwReference->CompType)
    {
        case CID_NFC_JAL:
            /*! \todo Add the JAL code for READ here! */
            result = PHNFCSTVAL(CID_NFC_DAL, NFCSTATUS_INVALID_DEVICE);
            break;

        case CID_NFC_TAL:
        {
            uint32_t    NumberOfBytesRead   =   0;
            BOOL        bResult;
            if(PHHAL_SERIAL_DEVICE  ==  psHwReference->LinkType)
            {
                /* SERIAL Link */

                /*  With USB driver, no problem, we are sure that the number of bytes read
                    matches the number of bytes sent by TAMA. But for serial link,
                    we don't know in advance how many bytes we want to read.
                    Thus, we'll have to prefetch the 6 first bytes in order to analyze the packet type
                    sent by TAMA.   */

                DWORD           NumberOfBytesReadTmp;
                uint8_t         FrameError[]    =   {0x00,0x00,0xFF,0x01,0xFF,0x7F,0x81,0x00};
                uint8_t         FrameNAck[]     =   {0x00,0x00,0xFF,0xFF,0x00,0x00};
                uint8_t         FrameAck[]      =   {0x00,0x00,0xFF,0x00,0xFF,0x00};
                uint8_t         FrameExt[]      =   {0x00,0x00,0xFF,0xFF,0xFF};

                uint8_t         Buffer[9];
                uint32_t        Buffer_sz       =   0;

                size_t          PREFETCH_SZ     =   6;
                DWORD           toRead;

                bResult                     =   FALSE;

                if( nNumberOfBytesToRead == 6 ) /*  15 mSec is to short for USB link. 100mSec is the typical value to used. */
                    nTimeout = T_RS232_TIMEOUT_DEFAULT;
                if( nTimeout == 0 ) /*  For TAL '0' means infinite, but 10 Sec is used to avoid blocking situation  */
                    nTimeout = 10000;

                // First, let's check whether the caller provided enough
                //  buffer size for intelligent processing
                if(nNumberOfBytesToRead <6)
                {
                    bResult =   Serial232Read       (   psHwReference, Handle, pBuffer, nNumberOfBytesToRead, &NumberOfBytesRead, T_RS232_RETRY, nTimeout, 1);
                }
                else
                {
                    //  This is the intelligent processing (with prefetch mechanism)

                    /* first, read the PREFETCH_SZ first bytes  */
                    Serial232Read       (   psHwReference,
                                            Handle,
                                            &Buffer[0],
                                            (uint16_t)PREFETCH_SZ,
                                            &NumberOfBytesReadTmp,
                                            T_RS232_RETRY,
                                            nTimeout,
                                            nNumberOfBytesToRead != 6 ?1:0  /*  Does not allow abort if reading an ACK */
                                            );
                    NumberOfBytesRead   +=  NumberOfBytesReadTmp;
                    Buffer_sz            =  NumberOfBytesRead;

                    //  Standart infromation Frame : initialize toread with this value
                    toRead              =   Buffer[3] + 1;

                    if(PREFETCH_SZ  ==  NumberOfBytesReadTmp)
                    {
                        if(     0   ==  memcmp(FrameAck, Buffer, sizeof(FrameAck))
                            ||  0   ==  memcmp(FrameNAck, Buffer, sizeof(FrameNAck)))
                        {
                            /*  we've read a full ACK or NACK   */
                            toRead  =   0;
                        }
                        else
                        if(     0   ==  memcmp(FrameError, Buffer, sizeof(FrameError)))
                        {
                            /*  The begining of an Frame error  */
                            if(Buffer[4] != (uint8_t)(0x00 - Buffer[3]))
                            {
                                toRead  =   2;
                            }
                        }
                        else
                        if(     0   ==  memcmp(FrameExt, Buffer, sizeof(FrameExt)))
                        {
                            //  In the case of an Extended infromation Frame
                            //  toread is not defined in the same way as in the standart information frame
                            Serial232Read       (   psHwReference, Handle, &Buffer[Buffer_sz], 3, &NumberOfBytesReadTmp, T_RS232_RETRY, nTimeout, 1);
                            NumberOfBytesRead   +=   NumberOfBytesReadTmp;
                            Buffer_sz            =  NumberOfBytesRead;
                            PREFETCH_SZ+=3;
                            toRead              =   ((uint16_t)Buffer[5]) * 256+ Buffer[6] + 1;
                        }

                        memcpy(pBuffer, Buffer, Buffer_sz);


                        if(toRead <=  nNumberOfBytesToRead - NumberOfBytesRead)
                        {
                            if(0    ==  toRead)
                            {
                                bResult =   TRUE;
                            }
                            else
                            {
                                /*  Let's read the remaining bytes  */
                                Serial232Read (psHwReference, Handle, &pBuffer[NumberOfBytesRead], (uint16_t)(min(nNumberOfBytesToRead, toRead)), &NumberOfBytesReadTmp, T_RS232_RETRY, T_RS232_TIMEOUT_DEFAULT, 1);
                                NumberOfBytesRead   +=  NumberOfBytesReadTmp;

                                if(NumberOfBytesRead    ==  toRead  +   PREFETCH_SZ)
                                {
                                    /*  the operation completed if we've read at the expected number of bytes */
                                    bResult =   TRUE;
                                }
                            }

                        }
                    }
                }
            }
            else
            {
                /* USB Link */

                OVERLAPPED  osRead;

                osRead.Offset = 0;
                osRead.OffsetHigh = 0;
                osRead.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
                if( osRead.hEvent == INVALID_HANDLE_VALUE )
                {
                    bResult = FALSE;
                }
                else
                {
                    if( Handle != INVALID_HANDLE_VALUE )
                    {
                        if( nNumberOfBytesToRead == 6 ) // 15 mSec is to short for USB link. 100mSec is the typical value to used.
                            nTimeout = 100;
                        if( nTimeout == 0 ) //For TAL '0' means infinite, but 10 Sec is used to avoid blocking situation
                            nTimeout = 10000;

                        bResult = ReadFile (Handle, pBuffer, (uint32_t)nNumberOfBytesToRead, &NumberOfBytesRead, &osRead);
                        if( !bResult )
                        {
                            bResult =   TAMAWaitForIo(    psHwReference,
                                                                &osRead,
                                                                nTimeout,
                                                                nNumberOfBytesToRead != 6 ?1:0   /*  Does not allow abort if reading an ACK */
                                                                );
                            if( bResult )
                            {
                                if(!GetOverlappedResult(Handle, &osRead, &NumberOfBytesRead, FALSE))
                                {
                                    /*  Error in communication
                                        This is possible to get here if the buffer
                                        provided by the caller within Read/WriteFile
                                        function was not big enough to read all bytes from USB
                                    */
                                    bResult =   FALSE;
                                }
                            }
                            else
                            {
                                TAMAAbortDriverIo( psHwReference, Handle, &osRead, &NumberOfBytesRead);
                            }

                            if ( osRead.hEvent != INVALID_HANDLE_VALUE)
                            {
                                CloseHandle (osRead.hEvent);
                            }
                        }
                    }
                    else
                    {
                        bResult = FALSE;
                    }
                }
            }

            *pNumberOfBytesRead = (uint16_t)NumberOfBytesRead;

            if(bResult == FALSE)
            {
                result = PHNFCSTVAL(CID_NFC_DAL, NFCSTATUS_CMD_ABORTED);
            }

            #ifdef PRINTF_DEBUG
                printf("DAL : Read result = %04X\n",result);
            #endif

            break;
        }

        default:
            result = PHNFCSTVAL(CID_NFC_DAL, NFCSTATUS_INVALID_DEVICE);
            break;
    }

    return result;
}


/*! For documentation, see header file
 *
 */
extern NFCSTATUS phDalNfc_Ioctl(phHal_sHwReference_t    *psHwReference,
                                uint16_t                 IoctlCode,
                                uint8_t                 *pInBuf,
                                uint16_t                 InLength,
                                uint8_t                 *pOutBuf,
                                uint16_t                *pOutLength)
{
    NFCSTATUS result = NFCSTATUS_SUCCESS;

    #ifdef PHJALJOINER
        phJal_sBFLUpperFrame_t      *bfl_frame  = ((phJal_sBFLUpperFrame_t*)(psHwReference->pBoardDriver));
        void                        *com_handle = ((phcsBflBal_Hw1SerWinInitParams_t*)(((phDal_sBFLLowerFrame_t*)(psHwReference->DalContext.BflLowerFrame))->Bal.mp_Members))->com_handle;
    #endif

    switch (psHwReference->CompType)
    {
        #ifdef PHJALJOINER
        case CID_NFC_JAL:
            switch (IoctlCode)
            {
                case PHHALNFC_IOCTL_PN511_SOFTRESET:
                    /* Perform softreset (chip is now @ 9k6 bps! */
                    reportDbg0("DAL IOCTL:\t SR!\n");
                    bfl_frame->RcRegCtlSetRegParam.address  = PHCS_BFL_JREG_COMMAND;
                    bfl_frame->RcRegCtlSetRegParam.reg_data = PHCS_BFL_JCMD_SOFTRESET;
                    bfl_frame->RcRegCtlSetRegParam.self     = bfl_frame->RcRegCtl;
                    if (bfl_frame->RcRegCtl->SetRegister(&bfl_frame->RcRegCtlSetRegParam) == PH_ERR_BFL_SUCCESS)
                    {
                        /* Workaround: Give the device some time :-) */
                        Sleep(5);
                        /*Switch to 9600 bps for PN51X */
                        ChangeHostBaudrate(com_handle, CBR_9600);
                        /* Try to recover PN51x in case it has gone out of sync due to previous actions (port, PN53x enum, etc.): */
                        result = phcsBflDal_RecoverPN51x(bfl_frame->RcRegCtl);

                        /* Switch chip to 115 kbps again: */
                        bfl_frame->RcRegCtlSetRegParam.address = PHCS_BFL_JREG_SERIALSPEED;
                        bfl_frame->RcRegCtlSetRegParam.reg_data = PHDAL_JBAUD_115K;
                        if (bfl_frame->RcRegCtl->SetRegister(&bfl_frame->RcRegCtlSetRegParam) == PH_ERR_BFL_SUCCESS)
                        {
                            Sleep(5);
                            ChangeHostBaudrate(com_handle, CBR_115200);
                            /* Try to recover PN51x in case it has gone out of sync due to previous actions (port, PN53x enum, etc.): */
                            result = phcsBflDal_RecoverPN51x(bfl_frame->RcRegCtl);
                            result = PHNFCSTVAL(CID_NFC_TAL, NFCSTATUS_SUCCESS);
                        } else
                        {
                            result = PHNFCSTVAL(CID_NFC_TAL, NFCSTATUS_BOARD_COMMUNICATION_ERROR);
                        }
                    } else
                    {
                        result = PHNFCSTVAL(CID_NFC_TAL, NFCSTATUS_BOARD_COMMUNICATION_ERROR);
                    }
                    break;

                case PHHALNFC_IOCTL_PN511_ENINTR:
                    if (InLength > 0)
                    {
                        switch (pInBuf[0])
                        {
                            case 1:
                                bfl_frame->WaitEventCb = &phDal_WaitForReceptionCallback;
                                bfl_frame->EventCallbackUsesInterrupt = 0;
                                break;

                            case 2:
                                bfl_frame->WaitEventCb = &phDal_WaitForReceptionCallback;
                                bfl_frame->EventCallbackUsesInterrupt = 1;
                                break;

                            default:
                                bfl_frame->WaitEventCb = NULL;
                                break;
                        }
                    } else
                    {
                        result = PHNFCSTVAL(CID_NFC_TAL, NFCSTATUS_INVALID_PARAMETER);
                    }
                    break;

                default:
                    result = PHNFCSTVAL(CID_NFC_DAL, NFCSTATUS_INVALID_DEVICE);
                    break;
            }
            break;
        #endif /* Joiner */

        case CID_NFC_TAL:
            /*! \todo Add the TAL code for IOCTL here! */
            result = PHNFCSTVAL(CID_NFC_DAL, NFCSTATUS_INVALID_DEVICE);
            break;

        default:
            result = PHNFCSTVAL(CID_NFC_DAL, NFCSTATUS_INVALID_DEVICE);
                break;
    }
    return result;
}


#ifdef PHJALJOINER

/* ********************************************************************************************** */
/* Internally used functions (see prototypes:) */
/* ********************************************************************************************** */

void phDal_BuildBflLowerFrame(phDal_sBFLLowerFrame_t *bfl_frame)
{
    bfl_frame->DeviceName[0] = 0;

    /* BAL: */
    phcsBflBal_Hw1SerWinInit(&(bfl_frame->Bal), &(bfl_frame->BalP));
    bfl_frame->BalP.com_handle = NULL;

    /* RcRegCtl: */
    phcsBflRegCtl_SerHw1Init(&(bfl_frame->RcRegCtl), &(bfl_frame->RrcP), &(bfl_frame->Bal));
}

#endif

DWORD ChangeHostBaudrate(void* Port, unsigned long Baudrate)
{
    DCB             dcb;

    SetLastError(ERROR_SUCCESS);

    dcb.BaudRate = Baudrate;
    dcb.DCBlength = sizeof (dcb);
    dcb.fBinary = TRUE;
    dcb.fParity = FALSE;
    dcb.fOutxCtsFlow = FALSE;
    dcb.fOutxDsrFlow = FALSE;
    dcb.fDtrControl = DTR_CONTROL_DISABLE;
    dcb.fDsrSensitivity = FALSE;
    dcb.fTXContinueOnXoff = FALSE;
    dcb.fOutX = FALSE;
    dcb.fInX = FALSE;
    dcb.fErrorChar = FALSE;
    dcb.fNull = FALSE;
    dcb.fRtsControl = RTS_CONTROL_DISABLE;
    dcb.fAbortOnError = FALSE;
    dcb.XonLim = 0;
    dcb.XoffLim = 0;
    dcb.ByteSize = 8;
    dcb.Parity = NOPARITY;
    dcb.StopBits = ONESTOPBIT;

    /* Apply the settings */
    SetCommState((void*) Port, &dcb);

    return GetLastError();
}

DWORD SetComPortTimeouts(void* port, DWORD timeout)
{
    COMMTIMEOUTS    cto;

    SetLastError(ERROR_SUCCESS);
    cto.ReadIntervalTimeout = MAXDWORD;
    cto.ReadTotalTimeoutMultiplier = MAXDWORD;
    cto.ReadTotalTimeoutConstant = timeout;
    cto.WriteTotalTimeoutConstant = 0;
    cto.WriteTotalTimeoutMultiplier = 0;
    SetCommTimeouts(port, &cto);
    return GetLastError();
}

#ifdef PHJALJOINER

phcsBfl_Status_t WaitForPeripheralEvent(phJal_WaitForReceptionParams_t   *psReceptionParams,
                                        uint8_t                          Command,
                                        uint8_t                          JoinerOperationMode,
                                        uint8_t                          TargetSend,
                                        uint8_t                          CommIrqMask)
{
    DWORD                   event_mask = 0;
    DWORD                   ri_event_status = 0;
    BOOL                    communication_status = 0;
    BOOL                    start_communication = FALSE;
    phcsBfl_Status_t        timeout_status = PH_ERR_BFL_SUCCESS;
    OVERLAPPED              *overlapped_operation = NULL;
    HANDLE                  comport_handle = NULL;
    phJal_sBFLUpperFrame_t  *bfl_frame = NULL;
    DWORD                   error_code = 0;
    const DWORD             multi_obj_count = 2;
    HANDLE                  multi_obj[2];
    DWORD                   event_type_to_sense = EV_RING;
    clock_t                 polling_start, elapsed;

    if (psReceptionParams->psHwReference != NULL)
    {
        bfl_frame = (phJal_sBFLUpperFrame_t*)psReceptionParams->psHwReference->pBoardDriver;
        comport_handle = (((phcsBflBal_Hw1SerWinInitParams_t*)
        bfl_frame->RcRegCtl->mp_Lower->mp_Members))->com_handle;
        overlapped_operation = (OVERLAPPED*)(bfl_frame->IoOverlappedStructure);

        /* Select line to sense: */
        switch (bfl_frame->InterruptLine)
        {
            case JAL_SENSE_RI:
                event_type_to_sense = EV_RING;
                break;

            case JAL_SENSE_CTS:
                event_type_to_sense = EV_CTS;
                break;

            case JAL_SENSE_DSR:
                event_type_to_sense = EV_DSR;
                break;

            default:
                event_type_to_sense = EV_RING;
                break;
        }

        SetCommMask(comport_handle, 0);
        SetCommMask(comport_handle, event_type_to_sense);

        #ifndef NDEBUG
            bfl_frame->RcRegCtlGetRegParam.address = PHCS_BFL_JREG_COMMIRQ;
            bfl_frame->RcRegCtl->GetRegister(&bfl_frame->RcRegCtlGetRegParam);
            reportDbg1("event callback:\t COMMIRQ (begin of cb): %02X\n", bfl_frame->RcRegCtlGetRegParam.reg_data);

            bfl_frame->RcRegCtlGetRegParam.address = PHCS_BFL_JREG_COMMIEN;
            bfl_frame->RcRegCtl->GetRegister(&bfl_frame->RcRegCtlGetRegParam);
            reportDbg1("event callback:\t COMMIEN (begin of cb): %02X\n", bfl_frame->RcRegCtlGetRegParam.reg_data);
        #endif

        /* do seperate action if command to be executed is transceive */
        if ((Command == PHCS_BFL_JCMD_TRANSCEIVE) ||
            (Command == PHCS_BFL_CMD_TRANSCEIVE_TO))
        {
            /* check if Initiator mode or Target Transmit mode */
            if ((TargetSend) || (JoinerOperationMode))
            {
                /* TRx is always an endless loop, Initiator and Target must set STARTSEND. */
                bfl_frame->RcRegCtlModifyParam.address = PHCS_BFL_JREG_BITFRAMING;
                bfl_frame->RcRegCtlModifyParam.mask    = PHCS_BFL_JBIT_STARTSEND;
                bfl_frame->RcRegCtlModifyParam.set     = 1;
                bfl_frame->RcRegCtl->ModifyRegister(&bfl_frame->RcRegCtlModifyParam);
            }
        } else
        {
                /* MF AUTHENT command handling */
                bfl_frame->RcRegCtlGetRegParam.address = PHCS_BFL_JREG_COMMAND;
                bfl_frame->RcRegCtl->GetRegister(&bfl_frame->RcRegCtlGetRegParam);

                bfl_frame->RcRegCtlSetRegParam.address = PHCS_BFL_JREG_COMMAND;
                bfl_frame->RcRegCtlSetRegParam.reg_data = (uint8_t)((bfl_frame->RcRegCtlGetRegParam.reg_data & ~PHCS_BFL_JMASK_COMMAND) | Command);
                bfl_frame->RcRegCtl->SetRegister(&bfl_frame->RcRegCtlSetRegParam);
        }

        bfl_frame->RcRegCtlGetRegParam.address = PHCS_BFL_JREG_COMMIRQ;
        if (!bfl_frame->EventCallbackUsesInterrupt)
        {
            /* POLLING */
            reportDbg0("event callback:\t waiting for IRQ in POLLING mode:\n");
            polling_start = clock();
            do
            {
                /* POLLING LOOP: */
                bfl_frame->RcRegCtl->GetRegister(&bfl_frame->RcRegCtlGetRegParam);

                /* Sense ABORT OS Event:
                 * ri_event_status is reused.
                 * WaitForSingleObject can check the event status and return
                 * immediately, if the TIMEOUT is set to 0. As we are in POLLING
                 * operation mode, this is exactly what we want.
                 */
                ri_event_status = WaitForSingleObject((HANDLE)(psReceptionParams->psHwReference->DalContext.AbortEvent), 0 /*!! ms */);
                if (WAIT_OBJECT_0 == ri_event_status)
                {
                    timeout_status = PH_ERR_BFL_ABORTED;
                    ResetEvent(psReceptionParams->psHwReference->DalContext.AbortEvent);
                    reportDbg0("event callback:\t POLLING ABORTED!\n");
                    break;
                }

                /* Sense TIMEOUT: */
                elapsed = clock() - polling_start;
                if (((double)elapsed / CLOCKS_PER_SEC) * 1000 > psReceptionParams->psHwReference->UserTimeout)
                {
                    timeout_status = PH_ERR_BFL_IO_TIMEOUT;
                    reportDbg0("event callback:\t POLLING TIMEOUT!\n");
                    break;
                }
            } while (((bfl_frame->RcRegCtlGetRegParam.reg_data) & CommIrqMask) == 0);
            reportDbg1("event callback:\t COMMIRQ (end of POLLING): %02X\n", bfl_frame->RcRegCtlGetRegParam.reg_data);
        } else
        {
            /* INTR: Initially check status. */
            bfl_frame->RcRegCtl->GetRegister(&bfl_frame->RcRegCtlGetRegParam);
            reportDbg1("event callback:\t COMMIRQ (INTR mode): %02X\n", bfl_frame->RcRegCtlGetRegParam.reg_data);
            if ((bfl_frame->RcRegCtlGetRegParam.reg_data & CommIrqMask) == 0)
            {
                reportDbg0("event callback:\t waiting for IRQ in INTERRUPT mode:\n");
                communication_status = WaitCommEvent(comport_handle, &event_mask,
                    overlapped_operation);

                if (communication_status == 0)
                {
                    error_code = GetLastError();
                    if (error_code == ERROR_IO_PENDING)
                    {
                        start_communication = TRUE;
                    }
                    else
                    {
                        start_communication = FALSE;
                    }
                }
                else
                {
                    start_communication = TRUE;
                }

                if (start_communication == TRUE)
                {
                    do
                    {
                        multi_obj[0] = (HANDLE)(overlapped_operation->hEvent);
                        multi_obj[1] = (HANDLE)(psReceptionParams->psHwReference->DalContext.AbortEvent);

                        ri_event_status = WaitForMultipleObjects(multi_obj_count,
                                                                multi_obj,
                                                                FALSE,  /* ONE event active is enough. */
                                                                psReceptionParams->psHwReference->UserTimeout);

                        bfl_frame->RcRegCtl->GetRegister(&bfl_frame->RcRegCtlGetRegParam);
                        reportDbg1("event callback:\t COMMIRQ (After INTR): %02X\n", bfl_frame->RcRegCtlGetRegParam.reg_data);

                        switch (ri_event_status)
                        {
                            case WAIT_OBJECT_0:
                                /* Event of index 0 has been triggered: */
                                if (event_mask & event_type_to_sense)
                                {
                                    timeout_status = PH_ERR_BFL_SUCCESS;
                                    reportDbg0("event callback:\t EVENT!\n");
                                }
                                else
                                {
                                    timeout_status = PH_ERR_BFL_INVALID_PARAMETER;
                                    reportDbg0("event callback:\t INVALID event mask ERROR!\n");
                                }
                                ResetEvent(multi_obj[1]);
                                break;
                            case WAIT_OBJECT_0 + 1:
                                /* Event of index 1 (ABORT) has been triggered: */
                                timeout_status = PH_ERR_BFL_ABORTED;
                                ResetEvent(multi_obj[1]);
                                reportDbg0("event callback:\t ABORTED!\n");
                                break;
                            case WAIT_TIMEOUT:
                                    timeout_status = PH_ERR_BFL_IO_TIMEOUT;
                                    reportDbg0("event callback:\t TIMEOUT!\n");
                                break;
                            default:
                                /* Should never get here... */
                                reportDbg0("event callback:\t Interface Error!\n");
                                timeout_status = PH_ERR_BFL_INTERFACE_ERROR;
                                break;
                        }
                    } while ((timeout_status != PH_ERR_BFL_SUCCESS) &&
                            (timeout_status  != PH_ERR_BFL_IO_TIMEOUT) &&
                            (timeout_status  != PH_ERR_BFL_INTERFACE_ERROR) &&
                            (timeout_status  != PH_ERR_BFL_ABORTED) &&
                            (timeout_status  != PH_ERR_BFL_INVALID_PARAMETER));
                }
                else
                {
                    timeout_status = PH_ERR_BFL_INTERFACE_ERROR;
                }
            } else
            {
                reportDbg0("event callback:\t intermediate IRQ detected\n");
                timeout_status = PH_ERR_BFL_SUCCESS;
            }
        }
    }
    else
    {
        timeout_status = PH_ERR_BFL_INVALID_PARAMETER;
    }

    reportDbg1("event callback:\t returning with timeout_status == 0x%04X\n", timeout_status);

    return timeout_status;
}

phcsBfl_Status_t phDal_WaitForReceptionCallback(phcsBflAux_WaitEventCbParam_t *psParams)
{
    phcsBfl_Status_t callback_status = PH_ERR_BFL_SUCCESS;
    phJal_WaitForReceptionParams_t *callback_params = NULL;

    if (psParams != NULL)
    {
        callback_params = (phJal_WaitForReceptionParams_t*)psParams->user_ref;

        callback_status =
            WaitForPeripheralEvent(callback_params,
                                   psParams->command,
                                   psParams->initiator_not_target,
                                   psParams->target_send,
                                   *psParams->waitForComm);
    }
    else
    {
        callback_status = PHNFCSTVAL(CID_NFC_DAL, NFCSTATUS_INVALID_PARAMETER);
    }
    return callback_status;
}




#endif  /* PHJALJOINER */

#endif /* PHHAL_VERSION_ACQUISITION */
