/* /////////////////////////////////////////////////////////////////////////////////////////////////
//                     Copyright (c) Philips Semiconductors
//
//                       (C)PHILIPS Electronics N.V.2004
//         All rights are reserved. Reproduction in whole or in part is
//        prohibited without the written consent of the copyright owner.
//    Philips reserves the right to make changes without notice at any time.
//   Philips makes no warranty, expressed, implied or statutory, including but
//   not limited to any implied warranty of merchantability or fitness for any
//  particular purpose, or that the use will not infringe any third party patent,
//   copyright or trademark. Philips must not be liable for any loss or damage
//                            arising from its use.
///////////////////////////////////////////////////////////////////////////////////////////////// */

/*! 
 * \if cond_generate_filedocu 
 * \file phcsBflMfRdWrapper.hpp
 *
 * Project: Object Oriented Library Framework Mifare Component.
 *
 *  Source: phcsBflMfRdWrapper.hpp
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Date: Thu Sep 27 09:38:55 2007 $
 *
 * Comment:
 *  C++ wrapper for Mifare.
 *
 * History:
 *  GK:  Generated 29. April 2003
 *  MHa: Migrated to MoReUse September 2005
 * \endif
 *
 */

#ifndef PHCSBFLMFRDWRAPPER_HPP
#define PHCSBFLMFRDWRAPPER_HPP

#include <phcsBflIoWrapper.hpp>

extern "C"
{
    #include <phcsBflMfRd.h>
}

/* \ingroup mfrd
 * \brief Namespace used for complete library to avoid namspace clashes.
 */
namespace phcs_BFL
{

/*! \cond cond_Cpp_complete */
/*  \ingroup mfrd
 *  \brief Reader, according to the MIFARE (R) specification
 *  (abstract class, used for the C++ interface).
 */
class phcsBfl_MfRd
{
    public:
        virtual phcsBfl_Status_t Transaction(phcsBflMfRd_CommandParam_t *cmd_param)      = 0;

        /*! Lower edge interface: Not to be redefined in the derived class: */
        class phcsBfl_Io *mp_Lower;
};


/* \ingroup mfrd
 *  This is the GLUE class the C-kernel phcsBflMfRd_t "sees" when calling down the stack. The glue class
 *  represents the C-API of the lower layer (RC I/O.).
 */
class phcsBfl_MfRdGlue
{
    public:
        phcsBfl_MfRdGlue(void);
        ~phcsBfl_MfRdGlue(void);
        
        //! \name Static functions, able to call into C++ again.
        //@{
        static phcsBfl_Status_t Transceive(phcsBflIo_TransceiveParam_t *transceive_param);
        static phcsBfl_Status_t MfAuthent(phcsBflIo_MfAuthentParam_t *mfauthent_param);
        //@}

        //! Structure accessible from the upper object's pure C-kernel.
        phcsBflIo_t m_GlueStruct;
};
/*! \endcond */


/*! \ingroup mfrd
 *  This is the main C++ interface of MIFARE (C++ wrapper).
 */
class phcsBflMfRd_Wrapper : public phcsBfl_MfRd
{
    public:
        phcsBflMfRd_Wrapper(void);
        ~phcsBflMfRd_Wrapper(void);

        /*
        * \ingroup mfrd
        * \param[in] *p_lower       Pointer to the C++ object of the underlying layer I/O.
        * \param[in] *p_trxbuffer   Pointer to the system-wide TRx buffer, allocated and managed by the
        *                           embedding software. The buffer serves as the source/destination
        *                           buffer for the underlying I/O Transceive functionality.
        *
        * \note The alternative Constructor does setup required by the Component but not done
        *       by the default constructor.
        *  
        */
        phcsBflMfRd_Wrapper(phcsBfl_Io    *p_lower,
                          uint8_t     *p_trxbuffer);

        /*!
        * \ingroup mfrd
        * \param[in] *p_lower       Pointer to the C++ object of the underlying layer I/O.
        * \param[in] *p_trxbuffer   Pointer to the system-wide TRx buffer, allocated and managed by the
        *                           embedding software. The buffer serves as the source/destination
        *                           buffer for the underlying I/O Transceive functionality.
        *
        * \note The Initialise function does setup required by the Component but not done
        *       by the default constructor.
        *  
        */
        void Initialise(phcsBfl_Io    *p_lower,
                        uint8_t     *p_trxbuffer);
        
        phcsBfl_Status_t Transaction(phcsBflMfRd_CommandParam_t *cmd_param);

        /*! \brief Constructor status: This status gives information about whether the object has been
         *  allocated/configured correctly or not. It is a good idea to check this status (verify
         *  that it is set to PH_ERR_BFL_SUCCESS) before proceeding.
         */
        phcsBfl_Status_t                m_ConstructorStatus;

        phcsBflMfRd_t                   m_Interface;           /*!< \brief C-part instance. */
        phcsBflMfRd_InternalParam_t     m_CommunicationParams; /*!< \brief C-part internal variables. */
        
    protected:
        class phcsBfl_MfRdGlue  m_MifareGlue;          /*!< \brief Glue class for connecting the C-part
                                                          *   with the C++ wrapper. */
};

}

#endif /* PHCSBFLMFRDWRAPPER_HPP */

/* E.O.F. */
