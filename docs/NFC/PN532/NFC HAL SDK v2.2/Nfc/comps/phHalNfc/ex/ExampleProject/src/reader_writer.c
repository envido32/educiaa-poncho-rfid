/*
 *               Copyright (c), NXP Semiconductors
 *
 *                       (C)NXP B.V.2006
 *         All rights are reserved. Reproduction in whole or in part is
 *        prohibited without the written consent of the copyright owner.
 *       NXP reserves the right to make changes without notice at any time.
 *      NXP makes no warranty, expressed, implied or statutory, including but
 *   not limited to any implied warranty of merchantability or fitness for any
 *  particular purpose, or that the use will not infringe any third party patent,
 *      copyright or trademark. NXP must not be liable for any loss or damage
 *                     arising from its use.
 *
 */

/*!
 * \file reader_writer.c
 * \brief For function declarations see \ref main.h
 *
 *
 * Project: CAS Example Project for NFC MW / HAL
 *
 * $Date: Thu Sep 27 09:38:01 2007 $
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Aliases: SDK_HAL_2.2,SDK_HAL_2.2_071008,NFC_FRI_SDK_1.0_071031 $
 *
*/

#include "main.h"

void Reader_Writer_Example(phHal_sHwReference_t *RW_HW, phcsExampleInitParam_t *InitParams)
{
    // Instantiate Reader/Writer parameter struct
    ExampleRWParam RWParams;

    // Feed the information of opened initiator hardware into initiator parameter struct
    RWParams.HwRef            =  RW_HW;
    RWParams.MTU              =  (uint16_t) InitParams->MTU;
    RWParams.MultiPollSession =  InitParams->MultiPoll;

    // Set default values in parameter struct
    Init_RW_Parameters(&RWParams);

    // Set remote device activation attributes to activate the different tags
    SetRWCommunicationParams(&RWParams);

    // Set Secure Element (SAM) configuation, S2C interface
    printf("\nNotice: Virtual Card configuration only applicable in target configuration!\n");
    RWParams.status = SAMConfiguration(RWParams.HwRef);
    
    // MultiPollSession restriction due to design e.g. PN531, PN532 - see the table on priorities during Poll
    if(RWParams.MultiPollSession == 0x00)       
    {
        // Which tags shall be polled for? See the documentation to find out about priorisation of Poll()

        RWParams.PollModes[0] = phHal_eOpModesMifare;
        RWParams.PollModes[1] = phHal_eOpModesISO14443_4A;
        RWParams.PollModes[2] = phHal_eOpModesFelica212;         // only recognition possible
        // RWParams.PollModes[3] = phHal_eOpModesFelica424;         // only recognition possible
        RWParams.PollModes[3] = phHal_eOpModesArrayTerminator;
        
        // Current restriction is to have a max number of 1 tag in Type B and Jewel poll mode
        // RWParams.nbRemoteDevMifare = 1;
        // RWParams.PollModes[0] = phHal_eOpModesJewel;
        // RWParams.PollModes[0] = phHal_eOpModesISO14443_4B;
        // RWParams.PollModes[1] = phHal_eOpModesArrayTerminator;

        // Poll for remote tags - keep in mind the Poll priorisation list!
        RWParams.status = phHalNfc_Poll(  RWParams.HwRef,
                                          RWParams.PollModes,
                                          RWParams.remoteDevInfoListMifare,
                                        &(RWParams.nbRemoteDevMifare),
                                        &(RWParams.communicationparams) );
        
        RWParams.nbRemoteDev = 0;
    }
    
    // MultiPollSession supported e.g. PN511, PN512 - See the example how to integrate it
    else if(RWParams.MultiPollSession == 0x01)      
    {
        // in case MultiPollSession is used but only one Poll is performed (please use the second one)
        // the variable 
        // RWParams.nbRemoteDevMifare has to be set to 0x00 otherwise ProcessRWPollData()
        // gets confused (it merges the Poll results into one struct)
        RWParams.nbRemoteDevMifare = NB_TAG_MAX;    // 0x00;
        // allow multi poll to add remote devices
        RWParams.communicationparams.ControlFlags.PollingAddsDevs = 1;

        // Which tags shall be polled for? See the documentation to find out about priorisation of Poll()

        RWParams.PollModes[0] = phHal_eOpModesMifare;
        // RWParams.PollModes[1] = phHal_eOpModesISO14443_4A;
        // RWParams.PollModes[2] = phHal_eOpModesFelica212;        // only recognition possible
        // RWParams.PollModes[3] = phHal_eOpModesFelica424;        // only recognition possible
        RWParams.PollModes[1] = phHal_eOpModesArrayTerminator;

        // Poll for Mifare remote tags which are potential targets
        // As Mifare has lowest priority it would not be detected if done together with Type 4 tag search
        RWParams.status = phHalNfc_Poll(  RWParams.HwRef,
                                          RWParams.PollModes,
                                          RWParams.remoteDevInfoListMifare,
                                        &(RWParams.nbRemoteDevMifare),
                                        &(RWParams.communicationparams) );

        // Which other tags shall be polled for?
        // RWParams.PollModes[0] = phHal_eOpModesMifare;
        RWParams.PollModes[0] = phHal_eOpModesISO14443_4A;
        RWParams.PollModes[1] = phHal_eOpModesFelica212;
        // RWParams.PollModes[3] = phHal_eOpModesFelica424;
        RWParams.PollModes[2] = phHal_eOpModesArrayTerminator;

        // Poll for remote tags which are potential targets
        RWParams.status = phHalNfc_Poll(  RWParams.HwRef,
                                          RWParams.PollModes,
                                          RWParams.remoteDevInfoList,
                                        &(RWParams.nbRemoteDev),
                                        &(RWParams.communicationparams) );
    }
    // Merge the polling results and identify the mode to connect to
    ProcessRWPollData (&RWParams);

    // Connect to the remote device
    RWParams.status = phHalNfc_Connect (   RWParams.HwRef,
                                          (RWParams.pollindex),
                                         &(RWParams.remoteDevInfoList[(RWParams.remoteDevChosen)]),
                                         &(RWParams.communicationparams) );

    // Transceive Data to the remote device - go into tag specific examples
    if (RWParams.status == NFCSTATUS_SUCCESS)
        if(RWParams.pollindex == phHal_eOpModesMifare)                // jump into Mifare(R) implementation
        {
            // e.g. jump into file reader_writer_mifare_example.c
            Reader_Writer_Mifare_Example(&RWParams);
        }
        else if(RWParams.pollindex == phHal_eOpModesISO14443_4A)      // jump into Type A implementation
        {
            // e.g. jump into file reader_writer_ISO14443_4A_example.c
            Reader_Writer_ISO14443_4A_Example(&RWParams);             
        }
        else if(RWParams.pollindex == phHal_eOpModesISO14443_4B)      // jump into Type B implementation
        {
            // e.g. jump into file reader_writer_ISO14443_4B_example.c
            Reader_Writer_ISO14443_4B_Example(&RWParams);
        }
        else if(RWParams.pollindex == phHal_eOpModesJewel)            // jump into Jewel(R) implementation
        {
            // e.g. jump into file reader_writer_Jewel_example.c
            Reader_Writer_Jewel_Example(&RWParams);
        }
        else if( (RWParams.pollindex == phHal_eOpModesFelica212)||    // jump into FeliCa(TM) implementation
                 (RWParams.pollindex == phHal_eOpModesFelica424)  )
        {
            // e.g. jump into file reader_writer_FeliCa_example.c
            Reader_Writer_FeliCa_Example(&RWParams);
        }
        else
        {
            // no corresponding tag communication example found...
            printf("\nExample for this type of tag is not available - Sorry\n");
        }
    else
    {
        printf("\nError: Could not connect to remote tag\n");
        displayNfcError(RWParams.status);
        return;
    }

    // Close the channel to the remote device
    RWParams.status = phHalNfc_Disconnect(RWParams.HwRef, &(RWParams.remoteDevInfoList[RWParams.remoteDevChosen]));

    // Give feedback if disconnect from remote device was successful
    if (RWParams.status == NFCSTATUS_SUCCESS)
        printf("\nDisconnect from remote device successful\n");
    else
        displayNfcError (RWParams.status);
    return;
}