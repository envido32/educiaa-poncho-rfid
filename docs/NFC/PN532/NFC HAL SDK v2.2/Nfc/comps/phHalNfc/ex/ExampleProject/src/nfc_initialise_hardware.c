/*
 *               Copyright (c), NXP Semiconductors
 *
 *                       (C)NXP B.V.2006
 *         All rights are reserved. Reproduction in whole or in part is
 *        prohibited without the written consent of the copyright owner.
 *       NXP reserves the right to make changes without notice at any time.
 *      NXP makes no warranty, expressed, implied or statutory, including but
 *   not limited to any implied warranty of merchantability or fitness for any
 *  particular purpose, or that the use will not infringe any third party patent,
 *      copyright or trademark. NXP must not be liable for any loss or damage
 *                     arising from its use.
 *
 */

/*!
 * \file nfc_initialise_hardware.c
 * \brief For function declarations see \ref main.h
 *
 *
 * Project: CAS Example Project for NFC MW / HAL
 *
 * $Date: Thu Sep 27 09:38:01 2007 $
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Aliases: SDK_HAL_2.2,SDK_HAL_2.2_071008,NFC_FRI_SDK_1.0_071031 $
 *
*/

#include "main.h"
#include "phOsalNfc.h"

void Initialise_NFC_Hardware(phcsExampleInitParam_t *InitParams)
{
    // Instantiate struct to get more HW information
    phHal_sDeviceCapabilities_t sDevCapabilities;

    // Initialise Parameters
    Init_Init_Parameters(InitParams);

    // Enumerate: Find out which hardware is connected to the PC
    (InitParams->status) = phHalNfc_Enumerate(InitParams->HwRef, &(InitParams->NbrOfDevDetected));

    // User interaction to decide which hardware shall be opened
    ProcessEnumerateResult(InitParams);

    // Open Board according to user input
    (InitParams->status) = phHalNfc_Open( &(InitParams->HwRef [InitParams->index]));
    
    // Error handling
    if (InitParams->status == NFCSTATUS_SUCCESS )
        printf("\nOpen SUCCESSFUL \n");
    else
    {
        printf("\nOpen NOT SUCCESSFUL - Error code: %d \n\n !!! Please ENUMERATE AGAIN !!!\n\n", InitParams->status);
        phOsalNfc_Suspend(3500);
        return;
    }

    // Get the maximum transfer unit size (MTU) to control data exchange
    InitParams->status    = phHalNfc_GetDeviceCapabilities(&(InitParams->HwRef[InitParams->index]), &sDevCapabilities);
    InitParams->MTU       = (sDevCapabilities.MTU);
    InitParams->MultiPoll = (sDevCapabilities.MultiPollSessionSup);

    // Error handling
    if (InitParams->status == NFCSTATUS_SUCCESS )
        printf("Get Device Capabilities SUCCESSFUL \n");
    else
    {
        printf("\nGet Device Capabilities NOT SUCCESSFUL - Error code: %d \n", InitParams->status);
        return;
    }
}
