
/*
 *               Copyright (c), NXP Semiconductors
 *
 *                       (C)NXP B.V.2006
 *         All rights are reserved. Reproduction in whole or in part is
 *        prohibited without the written consent of the copyright owner.
 *       NXP reserves the right to make changes without notice at any time.
 *      NXP makes no warranty, expressed, implied or statutory, including but
 *   not limited to any implied warranty of merchantability or fitness for any
 *  particular purpose, or that the use will not infringe any third party patent,
 *      copyright or trademark. NXP must not be liable for any loss or damage
 *                     arising from its use.
 *
 */


/*!
 * \file reader_writer_jewel_example.c
 * \brief For function declarations see \ref main.h
 *
 *
 * Project: CAS Example Project for NFC MW / HAL
 *
 * $Date: Thu Sep 27 09:38:01 2007 $
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Aliases: SDK_HAL_2.2,SDK_HAL_2.2_071008,NFC_FRI_SDK_1.0_071031 $
 *
*/

#include "main.h"

void DisplayReadAllMemResponse(ExampleRWParam *RWParams)
{
    uint16_t i = 0;

    if(RWParams->status == NFCSTATUS_SUCCESS)
    {
        printf("Response APDU length is: %5d\n\nHeader Byte: %02x %02x\n", RWParams->RcvLength, RWParams->buffer[0], RWParams->buffer[1]);
        printf("Tag ID: %02x %02x %02x %02x\n\nData: \n", RWParams->buffer[2], RWParams->buffer[3], RWParams->buffer[4], RWParams->buffer[5]);
        
        for(i=6; i < RWParams->RcvLength; i++)
        {
            printf("%02X ", RWParams->buffer[i]);
        }
        printf("\n");
    }
    else
        displayNfcError(RWParams->status);
    printf("\n");
}



void Reader_Writer_Jewel_Example(ExampleRWParam *RWParams)
{
    uint8_t i = 0;

    printf("\nJewel Example reached\n\n");
    // please enter code here...

    printf("\n--> Read All Memory\n");
    RWParams->Cmd.Iso144434Cmd = phHal_eIso14443_4_CmdListTClCmd;
    RWParams->SendLength = 3;
    RWParams->RcvLength  = RWParams->MTU;

    // fill data buffer
    RWParams->buffer[0] = 0x00;
    RWParams->buffer[1] = 0x00;
    RWParams->buffer[2] = 0x00;
    
    printf("Send data: ");
    for(i=0; i < RWParams->SendLength; i++)
    {
        printf("%02X ", RWParams->buffer[i]);
    }
    printf("\n");

    RWParams->status = phHalNfc_Transceive(  RWParams->HwRef,
                                            &(RWParams->remoteDevInfoList[(RWParams->remoteDevChosen)]),
                                              RWParams->Cmd,
                                            &(RWParams->DepMoreInfo),
                                              RWParams->buffer,
                                              RWParams->SendLength,
                                              RWParams->buffer,
                                            &(RWParams->RcvLength));

    DisplayReadAllMemResponse(RWParams);
    //printf("\n");


    //printf("\n--> Read 1byte\n");
    //RWParams->Cmd.Iso144434Cmd = phHal_eIso14443_4_CmdListTClCmd;
    //RWParams->SendLength = 3;
    //RWParams->RcvLength  = RWParams->MTU;

    //// fill data buffer
    //RWParams->buffer[0] = 0x01; // Read
    //RWParams->buffer[1] = 0x10; // Addr
    //RWParams->buffer[2] = 0x00;

    //RWParams->buffer[3] = 0x0B;  // ID of Jewel - added automatically
    //RWParams->buffer[4] = 0x6E;
    //RWParams->buffer[5] = 0x21;
    //RWParams->buffer[6] = 0x00;

    //printf("Send data: ");
    //for(i=0; i < RWParams->SendLength; i++)
    //{
    //    printf("%02X ", RWParams->buffer[i]);
    //}
    //printf("\n");

    //RWParams->status = phHalNfc_Transceive(  RWParams->HwRef,
    //                                        &(RWParams->remoteDevInfoList[(RWParams->remoteDevChosen)]),
    //                                          RWParams->Cmd,
    //                                        &(RWParams->DepMoreInfo),
    //                                          RWParams->buffer,
    //                                          RWParams->SendLength,
    //                                          RWParams->buffer,
    //                                        &(RWParams->RcvLength));

    //DisplayReadAllMemResponse(RWParams);
    //printf("\n");
}