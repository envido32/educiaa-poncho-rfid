/*
 *               Copyright (c), NXP Semiconductors
 *
 *                       (C)NXP B.V.2006
 *         All rights are reserved. Reproduction in whole or in part is
 *        prohibited without the written consent of the copyright owner.
 *       NXP reserves the right to make changes without notice at any time.
 *      NXP makes no warranty, expressed, implied or statutory, including but
 *   not limited to any implied warranty of merchantability or fitness for any
 *  particular purpose, or that the use will not infringe any third party patent,
 *      copyright or trademark. NXP must not be liable for any loss or damage
 *                     arising from its use.
 *
 */

/*!
 * \file mifare_example.h
 *
 *
 * Project: CAS Example Project for NFC MW / HAL
 *
 * $Date: Thu Sep 27 09:38:02 2007 $
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Aliases: SDK_HAL_2.2,SDK_HAL_2.2_071008,NFC_FRI_SDK_1.0_071031 $
 *
*/


#ifndef __MIFARE_EXAMPLE_H__
#define __MIFARE_EXAMPLE_H__

#define MIFARE_UL_BLOCKS 16         // Mifare UL has 16 block with 4 bytes each memory

#define MIFARE_1K_SECTORS 16        // Mifare 1k has 16 sectors with 4 blocks of 16bytes each
#define MIFARE_1K_BLOCKS 4

#define MIFARE_4K_SECTORS 32        // Mifare 4k has 32 sectors with 4 blocks of 16bytes each +
#define MIFARE_4K_BLOCKS 4
#define MIFARE_4K_UPPER_SECTORS 8   // Mifare 4k has additional 8 sectors with 16 blocks of 16bytes each
#define MIFARE_4K_UPPER_BLOCKS 16


#define CHAR2DIGIT(c) ((unsigned int)((c) <= '9' ? (c)-'0' : (c)-'A'+10))


void PrepareMifareData(ExampleRWParam *RWParams);

void ProcessMifareData(ExampleRWParam *RWParams);


void ReadWholeMifareUL(ExampleRWParam *RWParams);

void WriteWholeMifareUL(ExampleRWParam *RWParams);


void ReadWholeMifare1k(ExampleRWParam *RWParams);

void WriteWholeMifare1k(ExampleRWParam *RWParams);


void ReadWholeMifare4k(ExampleRWParam *RWParams);

void WriteWholeMifare4k(ExampleRWParam *RWParams);

#endif