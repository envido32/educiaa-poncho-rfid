/*
 *               Copyright (c), NXP Semiconductors
 *
 *                       (C)NXP B.V.2006
 *         All rights are reserved. Reproduction in whole or in part is
 *        prohibited without the written consent of the copyright owner.
 *       NXP reserves the right to make changes without notice at any time.
 *      NXP makes no warranty, expressed, implied or statutory, including but
 *   not limited to any implied warranty of merchantability or fitness for any
 *  particular purpose, or that the use will not infringe any third party patent,
 *      copyright or trademark. NXP must not be liable for any loss or damage
 *                     arising from its use.
 *
 */

/*!
 * \file nfc_initiator.c
 * \brief For function declarations see \ref main.h
 *
 *
 * Project: CAS Example Project for NFC MW / HAL
 *
 * $Date: Thu Sep 27 09:38:01 2007 $
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Aliases: SDK_HAL_2.2,SDK_HAL_2.2_071008,NFC_FRI_SDK_1.0_071031 $
 *
*/

#include "main.h"

void Peer2Peer_Initiator_Example(phHal_sHwReference_t *InitiatorHW, p2pDataFlowParams_t *P2PDataFlowParams, uint32_t mtu)
{

    //Instantiate initiator parameter struct
    phcsExampleInitiatorParam_t IParam;

    // Feed the information of opened initiator hardware into initiator parameter struct
    IParam.HwRef =  InitiatorHW;
    IParam.MTU   =  (uint16_t) mtu;

    // Set default values in parameter struct
    Init_Initiator_Parameters(&IParam);
    // Poll for the remote device, choose:
    // speeds: 106/212/424kbps and types active/passive which shall be polled for
    SelectPollingMode(&IParam);

    // Set remote device activation attributes like NFCIDs, DiD, NAD...
    SetCommunicationParams(&IParam, P2PDataFlowParams);

    //Poll for available remote devices
    IParam.status = phHalNfc_Poll(  IParam.HwRef,
                                    IParam.PollModes,
                                    IParam.remoteDevInfoList,
                                    &IParam.nbRemoteDev,
                                    IParam.communicationparams);

    // Give feedback to the user what was going on during Poll
    ProcessRemotePollData(&IParam);

    // Connect to the remote Device which was found in first place
    if(IParam.status == NFCSTATUS_SUCCESS)
    {
        IParam.status = phHalNfc_Connect(   IParam.HwRef,
                                            IParam.PollModes[0],
                                          &(IParam.remoteDevInfoList[0]),
                                          &(IParam.communicationparams[0]) );
    }

    // Feedback to the user after Connect operation
    if(IParam.status == NFCSTATUS_SUCCESS)
    {
        printf("> Connect Successful! OK\n");
    }
    else
    {
        printf("> Connect NOT Successful! OK\n");
        displayNfcError(IParam.status);
        return;
    }

    // Prepare data exchange
    IParam.DEPcmd.NfcInCmd = phHal_eNfcCmdListNfcDEP;
    
    // Take over user input made related to P2PDataTransfer
    IParam.iterations = P2PDataFlowParams->iterations;
    IParam.SendLength = P2PDataFlowParams->NbrOfBytesTransceived;
    IParam.OpenChainingData = P2PDataFlowParams->NbrOfBytesTransceived;
    IParam.DepMoreInfo.DepFlags.MetaChaining = 0;
    IParam.MetaChainingCounter = 0;

    do
    {
        //Prepare data for DEP exchange, setup meta chaining if needed
        PrepareMetaChainingData(&IParam);
        
        // debug purpose
        // printf("a priori SendLength: %4d Index: %4d ReceiveLength: %4d ReceiveIndex: %4d\n", IParam.SendLength, IParam.index, IParam.RcvLength, IParam.receiveindex);
        
        //Perform the data exchange
        IParam.status = phHalNfc_Transceive(  IParam.HwRef,
                                              IParam.remoteDevInfoList,
                                              IParam.DEPcmd,
                                            &(IParam.DepMoreInfo),
                                             (IParam.buffer + IParam.index),
                                              IParam.SendLength,
                                             (IParam.receivebuffer + IParam.receiveindex),
                                            &(IParam.RcvLength) );
        
        // debug purpose
        // printf("a posteriori SendLength: %4d Index: %4d ReceiveLength: %4d ReceiveIndex: %4d\n", IParam.SendLength, IParam.index, IParam.RcvLength, IParam.receiveindex);

        // display information after DEP operation, prepare new if necessary
        ProcessDEPTransceiveData(&IParam, P2PDataFlowParams);

        // watch the iteration counter, break while loop if data exchange comes to an end
        if (IParam.iterations <= 0)
            break;
    }while(IParam.status == NFCSTATUS_SUCCESS);

    //---------------
    // Prepare data exchange to signal last data packet sent to target
    //---------------
    printf("Send last data packet 'end'\n");
    IParam.buffer[0] = 'e';
    IParam.buffer[1] = 'n';
    IParam.buffer[2] = 'd';
    IParam.DEPcmd.NfcInCmd = phHal_eNfcCmdListNfcDEP;
    //IParam.iterations = 1;
    IParam.SendLength = 3;
    IParam.OpenChainingData = 3;
    IParam.DepMoreInfo.DepFlags.MetaChaining = 0;
    IParam.MetaChainingCounter = 0;
    PrepareMetaChainingData(&IParam);

    IParam.status = phHalNfc_Transceive(  IParam.HwRef,
                                          IParam.remoteDevInfoList,
                                          IParam.DEPcmd,
                                        &(IParam.DepMoreInfo),
                                          IParam.buffer,
                                          IParam.SendLength,
                                          IParam.buffer,
                                        &(IParam.RcvLength) );
    // Check success
    if(IParam.status == NFCSTATUS_SUCCESS)
        printf("> 'end' OK\n");
    else
    {
        printf("> 'end' error !\n");
        displayNfcError(IParam.status);
    }
    // After data exchange last data packet transferred disconnect from the remote device
    IParam.status = phHalNfc_Disconnect(IParam.HwRef, IParam.remoteDevInfoList);

    // Check success
    if(IParam.status == NFCSTATUS_SUCCESS)
        printf("> disconnect OK\n");
    else
    {
        printf("> disconnect error !\n");
        displayNfcError(IParam.status);
    }
    // End of NFC Initiator Example
    return;
}
