/*
 *               Copyright (c), NXP Semiconductors
 *
 *                       (C)NXP B.V.2006
 *         All rights are reserved. Reproduction in whole or in part is
 *        prohibited without the written consent of the copyright owner.
 *       NXP reserves the right to make changes without notice at any time.
 *      NXP makes no warranty, expressed, implied or statutory, including but
 *   not limited to any implied warranty of merchantability or fitness for any
 *  particular purpose, or that the use will not infringe any third party patent,
 *      copyright or trademark. NXP must not be liable for any loss or damage
 *                     arising from its use.
 *
 */


/*!
 * \file reader_writer_felica_example.c
 * \brief For function declarations see \ref main.h
 *
 *
 * Project: CAS Example Project for NFC MW / HAL
 *
 * $Date: Thu Sep 27 09:38:01 2007 $
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Aliases: SDK_HAL_2.2,SDK_HAL_2.2_071008 $
 *
*/

#include "main.h"
#define MAX_BUFLEN      100
#define FEL_IDM_LEN     8


void Reader_Writer_FeliCa_Example(ExampleRWParam *RWParams)
{
    // this is a sample transceive frame

    uint8_t		fel_buf[128];
	uint16_t	fel_buf_len	=	0;

	uint8_t*	txbuf	=	NULL;
    uint16_t	txlen	=	0;

	uint8_t		rxbuf[MAX_BUFLEN];
    uint16_t	rxlen	=	0;
    
    NFCSTATUS status = NFCSTATUS_SUCCESS;

	uint16_t i,j;
    
    printf("\nFeliCa Example reached\n\n");

    //Read:

    printf("\nRead Card\n");

    fel_buf_len         =   0;
	fel_buf[fel_buf_len++]    =   0x00; //  LEN : will be updated later on
    fel_buf[fel_buf_len++]    =   0x06;
    //  IDm is the NFCID2t (8 bytes) (FEL_IDM_LEN)
	memcpy(&fel_buf[fel_buf_len], &RWParams->remoteDevInfoList[0].RemoteDevInfo.CardInfo212_424.Startup212_424.NFCID2t[0], FEL_IDM_LEN);
    fel_buf_len+=FEL_IDM_LEN;

    fel_buf[fel_buf_len++]    =   0x01;   //  Number of Services (n=1 ==> 0x80)
    fel_buf[fel_buf_len++]    =   0x0B;  //  Service Code List
    fel_buf[fel_buf_len++]    =   0x00;  //  Service Code List
    fel_buf[fel_buf_len++]    =   0x04;  //  Number of Blocks    (to Nbr max in case of read)

    fel_buf[fel_buf_len++]    =   0x80;  //  1st Block Element : byte 1
    fel_buf[fel_buf_len++]    =   0x00;  //  1st Block Element : byte 2, block 1

    fel_buf[fel_buf_len++]    =   0x80;  //  2nd Block Element : byte 1
    fel_buf[fel_buf_len++]    =   0x01;  //  2nd Block Element : byte 2, block 1

    fel_buf[fel_buf_len++]    =   0x80;  //  3rd Block Element : byte 1
    fel_buf[fel_buf_len++]    =   0x02;  //  3rd Block Element : byte 2, block 1

    fel_buf[fel_buf_len++]    =   0x80;  //  4th Block Element : byte 1
    fel_buf[fel_buf_len++]    =   0x03;  //  4th Block Element : byte 2, block 1

    fel_buf[0]                =   (uint8_t)fel_buf_len; //  LEN
	
	txbuf		=	fel_buf;
	txlen		=	fel_buf_len;
	rxlen		= sizeof(rxbuf);
    
    RWParams->Cmd.FelCmd = phHal_eFelicaCmdListFelicaCmd;

    status = phHalNfc_Transceive(RWParams->HwRef,
                                 &RWParams->remoteDevInfoList[0],
                                 RWParams->Cmd,
                                 &(RWParams->DepMoreInfo),
                                 txbuf,
                                 txlen,
                                 rxbuf,
                                 &rxlen);

    printf("CHECK: Status = 0x%04X\n", status);
    printf("CHECK Response length = %d, Data = \n", rxlen);
    for (i = 0; i < rxlen; i++)
    {
        if (i == 13)
        {
            printf("\n");
            break;
        }
        printf ("%02X ", rxbuf[i]);
        if (i % 16 == 0) {printf("\n");}
    }

    printf("\n");
    for (j=1; i < rxlen; i++,j++)
    {
        printf ("%02X ", rxbuf[i]);
        if ( j % 16 == 0) {printf("\n");}
    }
    for (i = 0; i < rxlen; i++)
    {
        //printf ("%c ", rxbuf[i]);
        if (i % 16 == 0) {printf("\n");}
    }

    //Update:

    printf("\nWrite Card\n");

    fel_buf_len         =   0;
	fel_buf[fel_buf_len++]    =   0x00; //  LEN : will be updated later on
    fel_buf[fel_buf_len++]    =   0x08;
    //  IDm is the NFCID2t (8 bytes) (FEL_IDM_LEN)
	memcpy(&fel_buf[fel_buf_len], &RWParams->remoteDevInfoList[0].RemoteDevInfo.CardInfo212_424.Startup212_424.NFCID2t[0], FEL_IDM_LEN);
    fel_buf_len+=FEL_IDM_LEN;

    fel_buf[fel_buf_len++]    =   0x01;   //  Number of Services (n=1 ==> 0x80)
    fel_buf[fel_buf_len++]    =   0x09;  //  Service Code List
    fel_buf[fel_buf_len++]    =   0x00;  //  Service Code List
    fel_buf[fel_buf_len++]    =   0x04;  //  Number of Blocks    (to Nbr max in case of read)

    fel_buf[fel_buf_len++]    =   0x80;  //  1st Block Element : byte 1
    fel_buf[fel_buf_len++]    =   0x00;  //  1st Block Element : byte 2, block 1
    
    fel_buf[fel_buf_len++]    =   0x80;  //  2nd Block Element : byte 1
    fel_buf[fel_buf_len++]    =   0x01;  //  2nd Block Element : byte 2, block 1

    fel_buf[fel_buf_len++]    =   0x80;  //  3rd Block Element : byte 1
    fel_buf[fel_buf_len++]    =   0x02;  //  3rd Block Element : byte 2, block 1

    fel_buf[fel_buf_len++]    =   0x80;  //  4th Block Element : byte 1
    fel_buf[fel_buf_len++]    =   0x03;  //  4th Block Element : byte 2, block 1

    fel_buf[fel_buf_len++]    =   0x10;  //  1st Block Data1
    fel_buf[fel_buf_len++]    =   0x0C;  //  1st Block Data2
    fel_buf[fel_buf_len++]    =   0x08;  //  1st Block Data3
    fel_buf[fel_buf_len++]    =   0x00;  //  1st Block Data4

    fel_buf[fel_buf_len++]    =   0x93;  //  1st Block Data5
    fel_buf[fel_buf_len++]    =   0x00;  //  1st Block Data6
    fel_buf[fel_buf_len++]    =   0x00;  //  1st Block Data7
    fel_buf[fel_buf_len++]    =   0x00;  //  1st Block Data8

    fel_buf[fel_buf_len++]    =   0x00;  //  1st Block Data9
    fel_buf[fel_buf_len++]    =   0x00;  //  1st Block Data10
    fel_buf[fel_buf_len++]    =   0x01;  //  1st Block Data11
    fel_buf[fel_buf_len++]    =   0x00;  //  1st Block Data12

    fel_buf[fel_buf_len++]    =   0x00;  //  1st Block Data13
    fel_buf[fel_buf_len++]    =   0x3F;  //  1st Block Data14
    fel_buf[fel_buf_len++]    =   0x00;  //  1st Block Data15
    fel_buf[fel_buf_len++]    =   0xF7;  //  1st Block Data16

    fel_buf[fel_buf_len++]    =   0x60;  //  2nd Block Data1
    fel_buf[fel_buf_len++]    =   0x61;  //  2nd Block Data2
    fel_buf[fel_buf_len++]    =   0x62;  //  2nd Block Data3
    fel_buf[fel_buf_len++]    =   0x63;  //  2nd Block Data4

    fel_buf[fel_buf_len++]    =   0x64;  //  2nd Block Data5
    fel_buf[fel_buf_len++]    =   0x65;  //  2nd Block Data6
    fel_buf[fel_buf_len++]    =   0x66;  //  2nd Block Data7
    fel_buf[fel_buf_len++]    =   0x67;  //  2nd Block Data8

    fel_buf[fel_buf_len++]    =   0x64;  //  2nd Block Data9
    fel_buf[fel_buf_len++]    =   0x65;  //  2nd Block Data10
    fel_buf[fel_buf_len++]    =   0x66;  //  2nd Block Data11
    fel_buf[fel_buf_len++]    =   0x67;  //  2nd Block Data12

    fel_buf[fel_buf_len++]    =   0x60;  //  2nd Block Data13
    fel_buf[fel_buf_len++]    =   0x61;  //  2nd Block Data14
    fel_buf[fel_buf_len++]    =   0x62;  //  2nd Block Data15
    fel_buf[fel_buf_len++]    =   0x63;  //  2nd Block Data16

    fel_buf[fel_buf_len++]    =   0x60;  //  3rd Block Data1
    fel_buf[fel_buf_len++]    =   0x61;  //  3rd Block Data2
    fel_buf[fel_buf_len++]    =   0x62;  //  3rd Block Data3
    fel_buf[fel_buf_len++]    =   0x63;  //  3rd Block Data4

    fel_buf[fel_buf_len++]    =   0x64;  //  3rd Block Data5
    fel_buf[fel_buf_len++]    =   0x65;  //  3rd Block Data6
    fel_buf[fel_buf_len++]    =   0x66;  //  3rd Block Data7
    fel_buf[fel_buf_len++]    =   0x67;  //  3rd Block Data8

    fel_buf[fel_buf_len++]    =   0x64;  //  3rd Block Data9
    fel_buf[fel_buf_len++]    =   0x65;  //  3rd Block Data10
    fel_buf[fel_buf_len++]    =   0x66;  //  3rd Block Data11
    fel_buf[fel_buf_len++]    =   0x67;  //  3rd Block Data12

    fel_buf[fel_buf_len++]    =   0x60;  //  3rd Block Data13
    fel_buf[fel_buf_len++]    =   0x61;  //  3rd Block Data14
    fel_buf[fel_buf_len++]    =   0x62;  //  3rd Block Data15
    fel_buf[fel_buf_len++]    =   0x63;  //  3rd Block Data16

    fel_buf[fel_buf_len++]    =   0x60;  //  4th Block Data1
    fel_buf[fel_buf_len++]    =   0x61;  //  4th Block Data2
    fel_buf[fel_buf_len++]    =   0x62;  //  4th Block Data3
    fel_buf[fel_buf_len++]    =   0x63;  //  4th Block Data4

    fel_buf[fel_buf_len++]    =   0x64;  //  4th Block Data5
    fel_buf[fel_buf_len++]    =   0x65;  //  4th Block Data6
    fel_buf[fel_buf_len++]    =   0x66;  //  4th Block Data7
    fel_buf[fel_buf_len++]    =   0x67;  //  4th Block Data8

    fel_buf[fel_buf_len++]    =   0x64;  //  4th Block Data9
    fel_buf[fel_buf_len++]    =   0x65;  //  4th Block Data10
    fel_buf[fel_buf_len++]    =   0x66;  //  4th Block Data11
    fel_buf[fel_buf_len++]    =   0x67;  //  4th Block Data12

    fel_buf[fel_buf_len++]    =   0x60;  //  4th Block Data13
    fel_buf[fel_buf_len++]    =   0x61;  //  4th Block Data14
    fel_buf[fel_buf_len++]    =   0x62;  //  4th Block Data15
    fel_buf[fel_buf_len++]    =   0x63;  //  4th Block Data16

    fel_buf[0]                =   (uint8_t)fel_buf_len; //  LEN
	
	txbuf		=	fel_buf;
	txlen		=	fel_buf_len;
	rxlen		= sizeof(rxbuf);

    status = phHalNfc_Transceive(RWParams->HwRef,
                                 &RWParams->remoteDevInfoList[0],
                                 RWParams->Cmd,
                                 &(RWParams->DepMoreInfo),
                                 txbuf,
                                 txlen,
                                 rxbuf,
                                 &rxlen);

    printf("UPDATE: Status = 0x%04X\n", status);
    //printf("UPDATE Response length = %d\nFlag 1 = 0x%02X\nFlag 2 0x%02X\n", rxlen, rxbuf[0], rxbuf[1]);
    for (i = 0; i < rxlen; i++)
    {
        printf ("%02X ", rxbuf[i]);
        if (i % 16 == 0) {printf("\n");}
    }
    for (i = 0; i < rxlen; i++)
    {
        //printf ("%02X ", rxbuf[i]);
        if (i % 16 == 0) {printf("\n");}
    }
	fel_buf_len         =   0;
	fel_buf[fel_buf_len++]    =   0x00; //  LEN : will be updated later on
    fel_buf[fel_buf_len++]    =   0x06;
    //  IDm is the NFCID2t (8 bytes) (FEL_IDM_LEN)
	memcpy(&fel_buf[fel_buf_len], &RWParams->remoteDevInfoList[0].RemoteDevInfo.CardInfo212_424.Startup212_424.NFCID2t[0], FEL_IDM_LEN);
    fel_buf_len+=FEL_IDM_LEN;

    fel_buf[fel_buf_len++]    =   0x01;   //  Number of Services (n=1 ==> 0x80)
    fel_buf[fel_buf_len++]    =   0x0B;  //  Service Code List
    fel_buf[fel_buf_len++]    =   0x00;  //  Service Code List
    fel_buf[fel_buf_len++]    =   0x04;  //  Number of Blocks    (to Nbr max in case of read)

    fel_buf[fel_buf_len++]    =   0x80;  //  1st Block Element : byte 1
    fel_buf[fel_buf_len++]    =   0x00;  //  1st Block Element : byte 2, block 1

    fel_buf[fel_buf_len++]    =   0x80;  //  2nd Block Element : byte 1
    fel_buf[fel_buf_len++]    =   0x01;  //  2nd Block Element : byte 2, block 1

    fel_buf[fel_buf_len++]    =   0x80;  //  3rd Block Element : byte 1
    fel_buf[fel_buf_len++]    =   0x02;  //  3rd Block Element : byte 2, block 1

    fel_buf[fel_buf_len++]    =   0x80;  //  4th Block Element : byte 1
    fel_buf[fel_buf_len++]    =   0x03;  //  4th Block Element : byte 2, block 1

    fel_buf[0]                =   (uint8_t)fel_buf_len; //  LEN
	
    //Read:

    printf("\n\nRead Card\n");

	txbuf		=	fel_buf;
	txlen		=	fel_buf_len;
	rxlen		= sizeof(rxbuf);
    
    RWParams->Cmd.FelCmd = phHal_eFelicaCmdListFelicaCmd;

    status = phHalNfc_Transceive(RWParams->HwRef,
                                 &RWParams->remoteDevInfoList[0],
                                 RWParams->Cmd,
                                 &(RWParams->DepMoreInfo),
                                 txbuf,
                                 txlen,
                                 rxbuf,
                                 &rxlen);

    printf("CHECK: Status = 0x%04X\n", status);
    printf("CHECK Response length = %d, Data = \n", rxlen);
    for (i = 0; i < rxlen; i++)
    {
        if (i == 13)
        {
            printf("\n");
            break;
        }
        printf ("%02X ", rxbuf[i]);
        if (i % 16 == 0) {printf("\n");}
    }

    printf("\n");
    for (j=1; i < rxlen; i++,j++)
    {
        printf ("%02X ", rxbuf[i]);
        if ( j % 16 == 0) {printf("\n");}
    }
    for (i = 0; i < rxlen; i++)
    {
        //printf ("%c ", rxbuf[i]);
        if (i % 16 == 0) {printf("\n");}
    }

}