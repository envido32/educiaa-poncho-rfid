/*
 *               Copyright (c), NXP Semiconductors
 *
 *                       (C)NXP B.V.2006
 *         All rights are reserved. Reproduction in whole or in part is
 *        prohibited without the written consent of the copyright owner.
 *       NXP reserves the right to make changes without notice at any time.
 *      NXP makes no warranty, expressed, implied or statutory, including but
 *   not limited to any implied warranty of merchantability or fitness for any
 *  particular purpose, or that the use will not infringe any third party patent,
 *      copyright or trademark. NXP must not be liable for any loss or damage
 *                     arising from its use.
 *
 */


/*!
 * \file nfc_target.c
 * \brief For function declarations see \ref main.h
 *
 *
 * Project: CAS Example Project for NFC MW / HAL
 *
 * $Date: Thu Sep 27 09:38:01 2007 $
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Aliases: SDK_HAL_2.2,SDK_HAL_2.2_071008,NFC_FRI_SDK_1.0_071031 $
 *
*/

#include "main.h"
#include "phOsalNfc.h"

void Peer2Peer_Target_Example(phHal_sHwReference_t *TargetHW, p2pDataFlowParams_t *P2PDataFlowParams, uint32_t mtu)
{
    //Instantiate target parameters struct
    phcsExampleTargetParam_t TParams;
    // Take over target hardware information
    TParams.HwRef =  TargetHW;
    // Take over number of maximum transfer unit
    TParams.MTU = mtu;
    // Init target parameters struct
    Init_Target_Parameters(&TParams);

    // Enter NFC Target Parameters
    SetTargetCommunicationParams(&TParams);

    // Enter valid NFC Target Modes for P2P communication
    SelectTargetPollingMode(&TParams);

    if(TParams.status == NFCSTATUS_SUCCESS)
    {
        if(TParams.PollModes[0] == phHal_eOpModesArrayTerminator)   // check if taget mode(s) selected are valid
        {
            // No communication mode selected - no target operation possible
            printf("Target Mode(s) array is found empty - exiting\n");
            return;
        }
        printf("Target Mode(s) set successfully \n");
    }
    else
    {
        printf("Target Modes NOT set\n");
        displayNfcError(TParams.status);
        return;
    }

    // Start the target mode
    TParams.status = phHalNfc_StartTargetMode(  TParams.HwRef,
                                              &(TParams.communicationparams),
                                                TParams.PollModes,
                                                TParams.ConnectionReq,
                                              &(TParams.ConnectionReqBufLength) );

    if(TParams.status == NFCSTATUS_SUCCESS)
    {
        // Display initiator activation information here
        ProcessTargetModeData(TParams.ConnectionReq, TParams.ConnectionReqBufLength);
        printf("\nNADi: %02x", TParams.DepMoreInfo.DepFlags.NADPresent);
        printf("\n---------------------------------\n");
    }
    else
    {
        displayNfcError(TParams.status);
        return;
    }

    // Set the amount of bytes which are send as response from P2P flow struct
    TParams.SendLength = P2PDataFlowParams->NbrOfBytesResponse;
    TParams.OpenChainingData = P2PDataFlowParams->NbrOfBytesResponse;
    TParams.MetaChainingCounter = 0;
    TParams.iterations = 0;

    // Receive data from the initiator
    do{
        // loop running as long as the initator wants to, hint: 'end'
        TParams.index = 0;
        do
        {
            // while loop needed if MI is used inside receive
            //Receive data from initiator
            TParams.RcvLength = DATABUFFERSIZE - (TParams.index);
            //RX
            TParams.status = phHalNfc_Receive(   TParams.HwRef,
                                               &(TParams.DepMoreInfo),
                                                (TParams.buffer+(TParams.index)),
                                               &(TParams.RcvLength) );
        
            ProcessReceivedData(&TParams);
        }while(TParams.status == NFCSTATUS_SUCCESS && TParams.QuitDEP == 0 && TParams.DepMoreInfo.DepFlags.MetaChaining != 0x00);

        // To check WTX handling a delay might be introduced here
        phOsalNfc_Suspend(P2PDataFlowParams->LatencyOfResponseToRequest);

        do{
            // How many bytes are send back as response? Meta chaining needed?
            PrepareResponseData(&TParams, P2PDataFlowParams);
            // TX
            TParams.status = phHalNfc_Send(  TParams.HwRef,
                                           &(TParams.DepMoreInfo),
                                            (TParams.buffer+(TParams.index)),
                                             TParams.SendLength);

            ProcessResponseData(&TParams, P2PDataFlowParams);
        }while(TParams.MetaChaining != 0 && TParams.status == NFCSTATUS_SUCCESS && TParams.QuitDEP == 0);

    }while(TParams.status == NFCSTATUS_SUCCESS && TParams.QuitDEP == 0); // until release or deselect was detected
    return;
}
