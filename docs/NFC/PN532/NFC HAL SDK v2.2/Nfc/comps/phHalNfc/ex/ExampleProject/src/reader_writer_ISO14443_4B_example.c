/*
 *               Copyright (c), NXP Semiconductors
 *
 *                       (C)NXP B.V.2006
 *         All rights are reserved. Reproduction in whole or in part is
 *        prohibited without the written consent of the copyright owner.
 *       NXP reserves the right to make changes without notice at any time.
 *      NXP makes no warranty, expressed, implied or statutory, including but
 *   not limited to any implied warranty of merchantability or fitness for any
 *  particular purpose, or that the use will not infringe any third party patent,
 *      copyright or trademark. NXP must not be liable for any loss or damage
 *                     arising from its use.
 *
 */


/*!
 * \file reader_writer_ISO14443_4B_example.c
 * \brief For function declarations see \ref main.h
 *
 *
 * Project: CAS Example Project for NFC MW / HAL
 *
 * $Date: Thu Sep 27 09:38:01 2007 $
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Aliases: SDK_HAL_2.2,SDK_HAL_2.2_071008,NFC_FRI_SDK_1.0_071031 $
 *
*/

#include "main.h"

void Reader_Writer_ISO14443_4B_Example(ExampleRWParam *RWParams)
{
    uint8_t i = 0;

    printf("\nISO14443-4B Example reached\n");
    // have a try and send a GetChallenge APDU    printf("\n--> Get Challenge APDU\n");
    RWParams->Cmd.Iso144434Cmd = phHal_eIso14443_4_CmdListTClCmd;
    RWParams->SendLength = 5;
    RWParams->RcvLength  = RWParams->MTU;

    RWParams->buffer[0] = 0x00;
    RWParams->buffer[1] = 0x84;
    RWParams->buffer[2] = 0x00;
    RWParams->buffer[3] = 0x00;
    RWParams->buffer[4] = 0x08;

    printf("Send data: ");
    for(i=0; i < RWParams->SendLength; i++)
    {
        printf("%02X ", RWParams->buffer[i]);
    }
    printf("\n");

    RWParams->status = phHalNfc_Transceive(  RWParams->HwRef,
                                            &(RWParams->remoteDevInfoList[(RWParams->remoteDevChosen)]),
                                              RWParams->Cmd,
                                            &(RWParams->DepMoreInfo),
                                              RWParams->buffer,
                                              RWParams->SendLength,
                                              RWParams->buffer,
                                            &(RWParams->RcvLength));

    DisplayAPDUResponse(RWParams);
    printf("\n");
    printf("\n--> Just some bytes APDU\n");
    RWParams->Cmd.Iso144434Cmd = phHal_eIso14443_4_CmdListTClCmd;
    RWParams->SendLength = 5;
    RWParams->RcvLength  = RWParams->MTU;

    RWParams->buffer[0] = 0x01;
    RWParams->buffer[1] = 0x02;
    RWParams->buffer[2] = 0x03;
    RWParams->buffer[3] = 0x04;
    RWParams->buffer[4] = 0x05;

    printf("Send data: ");
    for(i=0; i < RWParams->SendLength; i++)
    {
        printf("%02X ", RWParams->buffer[i]);
    }
    printf("\n");

    RWParams->status = phHalNfc_Transceive(  RWParams->HwRef,
                                            &(RWParams->remoteDevInfoList[(RWParams->remoteDevChosen)]),
                                              RWParams->Cmd,
                                            &(RWParams->DepMoreInfo),
                                              RWParams->buffer,
                                              RWParams->SendLength,
                                              RWParams->buffer,
                                            &(RWParams->RcvLength));

    DisplayAPDUResponse(RWParams);
    printf("\n");    // please enter code here...

}