These files are produced to work with Code::Blocks IDE

To get more information please go to:
http://www.codeblocks.org/


-------------------------------------------------------------------------

This package is known to work (as it was done with it) with this package:

Code::Blocks IDE, with MINGW compiler
Filesize: 13,597,181 Bytes | Version: 1.0rc2
MD5 Hash: 7638bdd4fd087b3168be4f0f968d1f3c 

To find it have a look at:
http://www.codeblocks.org/downloads.shtml


--------------------------------------------------------------------------

Windows 2000, Windows XP

To start using the HAL solution file with CB just open "SDK_HAL_PN5xx_v2.2.workspace"
That easy it is.


--------------------------------------------------------------------------


Windows VISTA 32bit Enterprise Edition

If you target VISTA and want to run Code::Blocks IDE some things with respect to MinGW have changed.
The package described above will work and install correctly, the workspace link given in the START menu
will work as well but if you try to compile the project the debugger will tell you something about
"... could not open and run some file of MinGW..."

This seems to be a known issue - therefore a link in the Start Menu "MinGW Installation under Win Vista"
was introduced. Please open it and proceed as explained.

You could also try to google "MinGW Vista" and should find some entries to this topic as well.

The verification on NXP side was not further proceeded.


--------------------------------------------------------------------------

