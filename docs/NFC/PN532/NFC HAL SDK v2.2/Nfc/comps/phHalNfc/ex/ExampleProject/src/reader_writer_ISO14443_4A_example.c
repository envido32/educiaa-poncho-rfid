/*
 *               Copyright (c), NXP Semiconductors
 *
 *                       (C)NXP B.V.2006
 *         All rights are reserved. Reproduction in whole or in part is
 *        prohibited without the written consent of the copyright owner.
 *       NXP reserves the right to make changes without notice at any time.
 *      NXP makes no warranty, expressed, implied or statutory, including but
 *   not limited to any implied warranty of merchantability or fitness for any
 *  particular purpose, or that the use will not infringe any third party patent,
 *      copyright or trademark. NXP must not be liable for any loss or damage
 *                     arising from its use.
 *
 */


/*!
 * \file reader_writer_ISO14443_4A_example.c
 * \brief For function declarations see \ref main.h
 *
 *
 * Project: CAS Example Project for NFC MW / HAL
 *
 * $Date: Thu Sep 27 09:38:01 2007 $
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Aliases: SDK_HAL_2.2,SDK_HAL_2.2_071008,NFC_FRI_SDK_1.0_071031 $
 *
*/

#include "main.h"

void Reader_Writer_ISO14443_4A_Example(ExampleRWParam *RWParams)
{
    uint16_t i = 0;

    printf("\nISO14443-4A Example reached\n");

    if ( (RWParams->remoteDevInfoList[(RWParams->remoteDevChosen)].RemoteDevInfo.CardInfo106.Startup106.SelRes == 0x00) ||
         (RWParams->remoteDevInfoList[(RWParams->remoteDevChosen)].RemoteDevInfo.CardInfo106.Startup106.SelRes == 0x08) ||
         (RWParams->remoteDevInfoList[(RWParams->remoteDevChosen)].RemoteDevInfo.CardInfo106.Startup106.SelRes == 0x18) ||
         (RWParams->remoteDevInfoList[(RWParams->remoteDevChosen)].RemoteDevInfo.CardInfo106.Startup106.SelRes == 0x28) ||
         (RWParams->remoteDevInfoList[(RWParams->remoteDevChosen)].RemoteDevInfo.CardInfo106.Startup106.SelRes == 0x38) )   // most probably NXP SMX device
    {
        printf("\n  SmartMX detected\n");

        printf("\n--> Select Card Manager\n");
        RWParams->Cmd.Iso144434Cmd = phHal_eIso14443_4_CmdListTClCmd;
        RWParams->SendLength = 13;
        RWParams->RcvLength  = RWParams->MTU;

        //Send: 00 A4 04 00 07 A0 00
        //      00 00 03 00 00 00

        RWParams->buffer[0] = 0x00;
        RWParams->buffer[1] = 0xA4;
        RWParams->buffer[2] = 0x04;
        RWParams->buffer[3] = 0x00;
        RWParams->buffer[4] = 0x07;
        RWParams->buffer[5] = 0xA0;
        RWParams->buffer[6] = 0x00;

        RWParams->buffer[7]  = 0x00;
        RWParams->buffer[8]  = 0x00;
        RWParams->buffer[9]  = 0x03;
        RWParams->buffer[10] = 0x00;
        RWParams->buffer[11] = 0x00;
        RWParams->buffer[12] = 0x00;

        printf("Send data: ");
        for(i=0; i < RWParams->SendLength; i++)
        {
            printf("%02X ", RWParams->buffer[i]);
        }
        printf("\n");

        RWParams->status = phHalNfc_Transceive(  RWParams->HwRef,
                                               &(RWParams->remoteDevInfoList[(RWParams->remoteDevChosen)]),
                                                 RWParams->Cmd,
                                               &(RWParams->DepMoreInfo),
                                                 RWParams->buffer,
                                                 RWParams->SendLength,
                                                 RWParams->buffer,
                                               &(RWParams->RcvLength));

        DisplayAPDUResponse(RWParams);

        //-------------------------------------
        printf("\n--> Get CPLC\n");
        RWParams->Cmd.Iso144434Cmd = phHal_eIso14443_4_CmdListTClCmd;
        RWParams->SendLength = 05;
        RWParams->RcvLength  = RWParams->MTU;

        // send 80 CA 9F 7F 00

        RWParams->buffer[0] = 0x80;
        RWParams->buffer[1] = 0xca;
        RWParams->buffer[2] = 0x9f;
        RWParams->buffer[3] = 0x7f;
        RWParams->buffer[4] = 0x00;

        printf("Send data: ");
        for(i=0; i < RWParams->SendLength; i++)
        {
            printf("%02X ", RWParams->buffer[i]);
        }
        printf("\n");

        RWParams->status = phHalNfc_Transceive(  RWParams->HwRef,
                                               &(RWParams->remoteDevInfoList[(RWParams->remoteDevChosen)]),
                                                 RWParams->Cmd,
                                               &(RWParams->DepMoreInfo),
                                                 RWParams->buffer,
                                                 RWParams->SendLength,
                                                 RWParams->buffer,
                                               &(RWParams->RcvLength));

        DisplayAPDUResponse(RWParams);

        //-------------------------------------
        printf("\n--> Identify\n");
        RWParams->Cmd.Iso144434Cmd = phHal_eIso14443_4_CmdListTClCmd;
        RWParams->SendLength = 15;
        RWParams->RcvLength  = RWParams->MTU;

        // send 00 A4 04 00 09 A0 00 00 01 67 41 30 00 FF 00

        RWParams->buffer[0] = 0x00;
        RWParams->buffer[1] = 0xa4;
        RWParams->buffer[2] = 0x04;
        RWParams->buffer[3] = 0x00;
        RWParams->buffer[4] = 0x09;

        RWParams->buffer[5] = 0xa0;
        RWParams->buffer[6] = 0x00;
        RWParams->buffer[7] = 0x00;
        RWParams->buffer[8] = 0x01;
        RWParams->buffer[9] = 0x67;

        RWParams->buffer[10] = 0x41;
        RWParams->buffer[11] = 0x30;
        RWParams->buffer[12] = 0x00;
        RWParams->buffer[13] = 0xff;
        RWParams->buffer[14] = 0x00;

        printf("Send data: ");
        for(i=0; i < RWParams->SendLength; i++)
        {
            printf("%02X ", RWParams->buffer[i]);
        }
        printf("\n");

        RWParams->status = phHalNfc_Transceive(  RWParams->HwRef,
                                               &(RWParams->remoteDevInfoList[(RWParams->remoteDevChosen)]),
                                                 RWParams->Cmd,
                                               &(RWParams->DepMoreInfo),
                                                 RWParams->buffer,
                                                 RWParams->SendLength,
                                                 RWParams->buffer,
                                               &(RWParams->RcvLength));

        DisplayAPDUResponse(RWParams);

    }
    else if ( (RWParams->remoteDevInfoList[(RWParams->remoteDevChosen)].RemoteDevInfo.CardInfo106.Startup106.SelRes == 0x20) ||
              (RWParams->remoteDevInfoList[(RWParams->remoteDevChosen)].RemoteDevInfo.CardInfo106.Startup106.SelRes == 0x24) )
    {
        printf("\n  Mifare ->DesFire<- detected\n");

        printf("\n--> Get version number\n");
        RWParams->Cmd.Iso144434Cmd = phHal_eIso14443_4_CmdListTClCmd;
        RWParams->SendLength = 1;
        RWParams->RcvLength  = RWParams->MTU;

        RWParams->buffer[0] = 0x60;

        printf("Send data: ");
        for(i=0; i < RWParams->SendLength; i++)
        {
            printf("%02X ", RWParams->buffer[i]);
        }
        printf("\n");

        RWParams->status = phHalNfc_Transceive(  RWParams->HwRef,
                                                &RWParams->remoteDevInfoList[(RWParams->remoteDevChosen)],
                                                 RWParams->Cmd,
                                                &RWParams->DepMoreInfo,
                                                 RWParams->buffer,
                                                 RWParams->SendLength,
                                                 RWParams->buffer,
                                                &RWParams->RcvLength );

        DisplayAPDUResponse(RWParams);

        //-------------------------------------
    }
    else
    {
        printf("\nCould not resolve SAK byte\n");
    }

    printf("\n--> Perform Presence Check\n");
    RWParams->Cmd.Iso144434Cmd = phHal_eIso14443_4_CmdListPresenceCheckCmd;
    RWParams->SendLength = 0;
    RWParams->RcvLength  = RWParams->MTU;

    RWParams->status = phHalNfc_Transceive(  RWParams->HwRef,
                                            &RWParams->remoteDevInfoList[(RWParams->remoteDevChosen)],
                                             RWParams->Cmd,
                                            &RWParams->DepMoreInfo,
                                             RWParams->buffer,
                                             RWParams->SendLength,
                                             RWParams->buffer,
                                            &RWParams->RcvLength );

    if(RWParams->status == NFCSTATUS_SUCCESS)
    {
        printf("Successful\n");
    }
    else
        displayNfcError(RWParams->status);


    // send some bytes - some error code shall come back
    printf("\n--> Send wrong APDU\n");
    RWParams->Cmd.Iso144434Cmd = phHal_eIso14443_4_CmdListTClCmd;
    RWParams->SendLength = 6;
    RWParams->RcvLength  = RWParams->MTU;

    RWParams->buffer[0] = 0x01;
    RWParams->buffer[1] = 0x02;
    RWParams->buffer[2] = 0x03;
    RWParams->buffer[3] = 0x04;
    RWParams->buffer[4] = 0x05;
    RWParams->buffer[5] = 0x06;

    printf("Send APDU: ");
    for(i=0; i < RWParams->SendLength; i++)
    {
        printf("%02X ", RWParams->buffer[i]);
    }
    printf("\n");

    RWParams->status = phHalNfc_Transceive(  RWParams->HwRef,
                                            &RWParams->remoteDevInfoList[(RWParams->remoteDevChosen)],
                                             RWParams->Cmd,
                                            &RWParams->DepMoreInfo,
                                             RWParams->buffer,
                                             RWParams->SendLength,
                                             RWParams->buffer,
                                            &RWParams->RcvLength );

    DisplayAPDUResponse(RWParams);


    // Deselect Tag
    printf("\n--> Deselect target\n");
    RWParams->Cmd.Iso144434Cmd = phHal_eIso14443_4_CmdListTClCmd;
    RWParams->SendLength = 1;
    RWParams->RcvLength  = RWParams->MTU;

    RWParams->buffer[0] = 0xc2;

    printf("Send APDU: ");
    for(i=0; i < RWParams->SendLength; i++)
    {
        printf("%02X ", RWParams->buffer[i]);
    }
    printf("\n");

    RWParams->status = phHalNfc_Transceive(  RWParams->HwRef,
                                            &RWParams->remoteDevInfoList[(RWParams->remoteDevChosen)],
                                             RWParams->Cmd,
                                            &RWParams->DepMoreInfo,
                                             RWParams->buffer,
                                             RWParams->SendLength,
                                             RWParams->buffer,
                                            &RWParams->RcvLength );

    DisplayAPDUResponse(RWParams);

    return;
}


void DisplayAPDUResponse(ExampleRWParam *RWParams)
{
    uint16_t i = 0;

    if(RWParams->status == NFCSTATUS_SUCCESS)
    {
        printf("Response APDU length is: %5d\nResponse APDU  :", RWParams->RcvLength);
        for(i=0; i < RWParams->RcvLength; i++)
        {
            printf("%02X ", RWParams->buffer[i]);
        }
        printf("\n");
    }
    else
        displayNfcError(RWParams->status);
    printf("\n");
}