/*
 *               Copyright (c), NXP Semiconductors
 *
 *                       (C)NXP B.V.2006
 *         All rights are reserved. Reproduction in whole or in part is
 *        prohibited without the written consent of the copyright owner.
 *       NXP reserves the right to make changes without notice at any time.
 *      NXP makes no warranty, expressed, implied or statutory, including but
 *   not limited to any implied warranty of merchantability or fitness for any
 *  particular purpose, or that the use will not infringe any third party patent,
 *      copyright or trademark. NXP must not be liable for any loss or damage
 *                     arising from its use.
 *
 */

/*!
 * \file helpers.c
 * \brief For function declarations see \ref main.h
 *
 *
 * Project: CAS Example Project for NFC MW / HAL
 *
 * $Date: Mon Oct  8 12:10:35 2007 $
 * $Author: beq03097 $
 * $Revision: 1.2 $
 * $Aliases: SDK_HAL_2.2,SDK_HAL_2.2_071008,NFC_FRI_SDK_1.0_071031 $
 *
*/

#include <stdio.h>
#include "main.h"

#define FLUSH_STDIN() \
{                                                 \
  int8_t ch=0;                                    \
  while(((ch = getchar())!= '\n') && (ch != EOF));\
}

void Init_Initiator_Parameters(phcsExampleInitiatorParam_t *IParam)
{
    uint16_t i = 0;

    // set the major structs to zero
    memset(&IParam->remoteDevInfoList,      0, sizeof(IParam->remoteDevInfoList));
    memset(&IParam->communicationparams,    0, sizeof(IParam->communicationparams));
    memset(&IParam->DepMoreInfo,            0, sizeof(IParam->DepMoreInfo));
    memset(&IParam->DEPcmd,                 0, sizeof(IParam->DEPcmd));
    memset(&IParam->PollModes,              0, sizeof(IParam->PollModes));

    IParam->status              = NFCSTATUS_SUCCESS;
    IParam->menu_choice         = 0;
    IParam->nbRemoteDev         = 2;

    IParam->DEPcmd.NfcInCmd         = 0;
    IParam->SendLength              = 0;
    IParam->RcvLength               = 0;    //DATABUFFERSIZE;
    IParam->MetaChainingCounter               = 0;
    IParam->OpenChainingData                  = 0;
    IParam->MetaChaining                      = 0;
    IParam->index                             = 0;
    IParam->receiveindex                      = 0;
    IParam->receivecomplete                   = 0;
    IParam->communicationparams->DIDiUsed     = 0;
    IParam->communicationparams->CIDiUsed     = 0;
    IParam->communicationparams->NfcNADiUsed  = 0;

    IParam->PollModes[0] = phHal_eOpModesArrayTerminator;

    for(i=0; i < NUMBER_GENERAL_BYTES; i++)
    {
        IParam->communicationparams->GeneralByte[i] = (0xB0 + (uint8_t) i);
    }

    //init the transceive buffer with some data
    for(i=0; i< DATABUFFERSIZE; i++)
    {
        IParam->buffer[i] = i%255;                      //fill with hex numbers 0x00 -> 0xFE
        IParam->receivebuffer[i] = i%26 + 97;
        //IParam->buffer[i] = i%26 + 97;                    //for the alphabeth 'a' till 'z'
        //IParam->buffer[i] = i%26 + 97 - ('a' -'A');       //for the alphabeth 'A' till 'Z'
    }
}

void Init_Init_Parameters(phcsExampleInitParam_t *InitParams)
{
    // set the major structs to zero
    memset(&InitParams->HwRef[0], 0, sizeof(InitParams->HwRef));

    InitParams->index               = 0;
    InitParams->NbrOfDevDetected    = MAX_BOARD_NUMBER;
    InitParams->status              = NFCSTATUS_SUCCESS;
    InitParams->MTU                 = 0;
    InitParams->MultiPoll           = 1;
}

void Init_Target_Parameters(phcsExampleTargetParam_t *TParams)
{
    uint16_t i = 0;

    // set the major structs to zero
    memset(&TParams->communicationparams, 0, sizeof(TParams->communicationparams));
    memset(&TParams->DepMoreInfo,         0, sizeof(TParams->DepMoreInfo));
    memset(&TParams->DEPcmd,              0, sizeof(TParams->DEPcmd));
    memset(&TParams->GeneralBytes,        0, sizeof(TParams->GeneralBytes));
    memset(&TParams->PollModes,           0, sizeof(TParams->PollModes));
    memset(&TParams->ConnectionReq,       0, sizeof(TParams->ConnectionReq));
    memset(&TParams->buffer,              0, sizeof(TParams->buffer));

    TParams->status = NFCSTATUS_SUCCESS;
    TParams->ConnectionReqBufLength = 48;

    // DEP Request and Response steering data
    TParams->SendLength             = 0;
    TParams->OpenChainingData       = 0;
    TParams->RcvLength              = 0;        //DATABUFFERSIZE;
    TParams->MetaChaining           = 0;
    TParams->MetaChainingCounter    = 0;
    TParams->iterations             = 0;
    TParams->QuitDEP                = 0;
    TParams->index                  = 0;
    TParams->DepMoreInfo.DepFlags.NADPresent = 0;

    TParams->PollModes[0] = phHal_eOpModesArrayTerminator;

    for(i=0; i < NUMBER_GENERAL_BYTES; i++)
    {
        TParams->communicationparams.GeneralBytes[i] = (0xB0 + (uint8_t) i);
    }

    //init the response / send buffer with some data until end of buffer size
    for(i=0; i < DATABUFFERSIZE; i++)
    {
        TParams->buffer[i] = i%255;                     //fill with hex numbers 0x00 -> 0xFE
        //TParams->buffer[i] = ((i%26) + 97);                   //for the alphabeth 'a' till 'z'
        //TParams->buffer[i] = i%26 + 97 - ('a' -'A');          //for the alphabeth 'A' till 'Z'
    }
}

void Init_RW_Parameters(ExampleRWParam *RWParams)
{

    // set the major structs to zero
    memset(&RWParams->communicationparams,      0, sizeof(RWParams->communicationparams));
    memset(&RWParams->remoteDevInfoList,        0, sizeof(RWParams->remoteDevInfoList));
    memset(&RWParams->remoteDevInfoListMifare,  0, sizeof(RWParams->remoteDevInfoListMifare));
    memset(&RWParams->PollModes,                0, sizeof(RWParams->PollModes));
    memset(&RWParams->buffer,                   0, sizeof(RWParams->buffer));

    RWParams->status            = NFCSTATUS_SUCCESS;
    RWParams->nbRemoteDev       = NB_TAG_MAX*2;
    RWParams->nbRemoteDevMifare = NB_TAG_MAX;
    RWParams->pollindex         = 0;
    RWParams->remoteDevChosen   = 0;
    RWParams->pollindex         = 0;
    RWParams->write_zeros       = 0;
    RWParams->RcvLength         = RWParams->MTU;
    RWParams->SendLength        = 0;
    RWParams->S2CMarker         = 1;     // reset value
    RWParams->communicationparams.CIDiUsed = 0;
}

void ProcessEnumerateResult(phcsExampleInitParam_t *InitParams)
{
    uint8_t n, c;

    printf("\nAmount of boards detected : %d\n\n", InitParams->NbrOfDevDetected);
    if(InitParams->status == NFCSTATUS_SUCCESS)
    {
        for(n=0; n<(InitParams->NbrOfDevDetected); n++)
        {
            printf("\n* %d ", n+1);
            if(((InitParams->HwRef)[n].LinkType) == 1)          // COM Port Type
            {
                printf(" Link: %.4s", (InitParams->HwRef)[n].DalContext.sLinkName);
                //printf("      Type: %s \n", ((InitParams->HwRef) [n].ChipName));
            }
            else if (((InitParams->HwRef)[n].LinkType) == 2)    // USB Type
            {
                printf(" Link: USB");
                //printf("      Type: %s \n", (IParam->HwRef) [n].DalContext.sLinkName);
            }
            else
                printf(" Link: Unknown\n");
            //printf("      Type: %s \n", (InitParams->HwRef) [n].ChipName);
        }
        printf("   --> default choice\n");
    }
    else
    {
        if( PHNFCSTVAL(CID_NFC_HAL,(InitParams->status)) == NFCSTATUS_MORE_INFORMATION )
        {
            printf("Amount of boards detected : %d \n", InitParams->NbrOfDevDetected);
            printf("Maximum Hw References allocated: %d\n", MAX_BOARD_NUMBER);
            InitParams->status = NFCSTATUS_SUCCESS;
        }
        else
            printf("phDalNfc_Enumerate error !\n");
            InitParams->status = NFCSTATUS_NO_DEVICE_FOUND;
    }

    if(((InitParams->status) ==  NFCSTATUS_SUCCESS) && (InitParams->NbrOfDevDetected) > 1)
    {
        do{
            printf("\nSelect the board (1 to %d ) : ", InitParams->NbrOfDevDetected);
            fflush(stdin);
            c = (int8_t )getchar();
#if defined(__linux__)
            FLUSH_STDIN();
#endif
            if( c == '\n')
            {
                printf("Open hardware number %2d\n", InitParams->NbrOfDevDetected);
                c = (InitParams->NbrOfDevDetected + (int8_t )'0');
            }
        }while (c < '1' || c > (InitParams->NbrOfDevDetected + (int8_t )'0') );
        InitParams->index = (uint8_t )(c - (int8_t )'1');
    }
    else if (((InitParams->status) ==  NFCSTATUS_SUCCESS) && (InitParams->NbrOfDevDetected) == 0)
    {
        printf("\nNo local NFC device found - please try to reset your hardware \nand restart the Enumeration!\n");
        InitParams->index = 0;
    }
    else
    {
        printf("As only one local NFC device found - trying to open it...\n");
        InitParams->index = 0;
    }
}

void ShowDeviceCapabilities(phHal_sDeviceCapabilities_t *sDevCapabilities)
{
    printf("\nVendor Name : %s\n",sDevCapabilities->VendorName.Name);

    if ( (((sDevCapabilities->psHwReference)->ChipName)== phHal_eChipName531) ||
         (((sDevCapabilities->psHwReference)->ChipName)== phHal_eChipName532) ||
         (((sDevCapabilities->psHwReference)->ChipName)== phHal_eChipName53x)   )
    {
        printf("Chip family : PN53x family\n");
    }
    else if ( (((sDevCapabilities->psHwReference)->ChipName)== phHal_eChipName51x) ||
              (((sDevCapabilities->psHwReference)->ChipName)== phHal_eChipName511) ||
              (((sDevCapabilities->psHwReference)->ChipName)== phHal_eChipName512)   )
    {
        printf("Chip family : PN51x family\n");
    }

    if (((sDevCapabilities->psHwReference)->ChipName) == phHal_eChipName531)
        printf("Product name: PN531\n");
    else if(((sDevCapabilities->psHwReference)->ChipName) == phHal_eChipName532)
        printf("Product name: PN532\n");
    else if(((sDevCapabilities->psHwReference)->ChipName) == phHal_eChipName511)
        printf("Product name: PN511\n");
    else if(((sDevCapabilities->psHwReference)->ChipName) == phHal_eChipName512)
        printf("Product name: PN512\n");
    else
        printf("Product name: unknown\n");

    if ((sDevCapabilities->psHwReference)->ChipRevision != 0)
        printf("Chip Version : %02x\n", (sDevCapabilities->psHwReference)->ChipRevision);

    if ((sDevCapabilities->psHwReference)->FirmwareVersion != 0)
        printf("Firmware Version.Revision : %d.%d\n", (sDevCapabilities->psHwReference)->FirmwareVersion, (sDevCapabilities->psHwReference)->FirmwareRevision);

    printf("\nSupported protocols as Initiator:\n MifareUL = %d\n MifareStd = %d\n ISO14443-4A = %d\n ISO14443-4B = %d\n FeliCa = %d\n NFC = %d\n Jewel = %d\n\n",
        sDevCapabilities->InitiatorSupProtocol.MifareUL, sDevCapabilities->InitiatorSupProtocol.MifareStd,
        sDevCapabilities->InitiatorSupProtocol.ISO14443_4A, sDevCapabilities->InitiatorSupProtocol.ISO14443_4B,
        sDevCapabilities->InitiatorSupProtocol.Felica, sDevCapabilities->InitiatorSupProtocol.NFC,
        sDevCapabilities->InitiatorSupProtocol.Jewel);

    printf("Supported protocols as Target:\n MifareUL = %d\n MifareStd = %d\n ISO14443-4A = %d\n ISO14443-4B = %d\n FeliCa = %d\n NFC = %d\n Jewel = %d\n\n",
        sDevCapabilities->TargetSupProtocol.MifareUL, sDevCapabilities->TargetSupProtocol.MifareStd,
        sDevCapabilities->TargetSupProtocol.ISO14443_4A, sDevCapabilities->TargetSupProtocol.ISO14443_4B,
        sDevCapabilities->TargetSupProtocol.Felica, sDevCapabilities->TargetSupProtocol.NFC,
        sDevCapabilities->TargetSupProtocol.Jewel);

    printf("MTU (Maximum Transfer Unit) size: %8d\n", (sDevCapabilities->MTU));

    if(sDevCapabilities->psHwReference->LinkType == 1)          // COM Port Type
    {
        printf("Host Link Used: %.4s\n", sDevCapabilities->psHwReference->DalContext.sLinkName);
    }
    else if (sDevCapabilities->psHwReference->LinkType == 2)    // USB Type
    {
        printf("Host Link Used: USB\n");
        //printf("Host Link Used: %.17s \n", sDevCapabilities->psHwReference->DalContext.sLinkName+4);
    }
    else
    {
        printf(" Link: Unknown\n");
    }
}

void SelectPollingMode(phcsExampleInitiatorParam_t *IParam)
{
    uint16_t i   = 0;
    uint16_t out = 0;
    uint8_t  c   = 0;

    printf("\nPlease key in the valid modes:\n\n");
    printf("* 1 - NFC passive mode @106kbps --> default choice\n");
    printf("* 2 - NFC passive mode @212kbps\n");
    printf("* 3 - NFC passive mode @424kbps\n\n");
    printf("* 4 - NFC active  mode @106kbps\n");
    printf("* 5 - NFC active  mode @212kbps\n");
    printf("* 6 - NFC active  mode @424kbps\n\n");
#if defined(__linux__)
    printf("* s - Start the transaction\n\n");
#endif
    printf("* x - E(x)it or if invalid input stream\n\n");
    printf("More than one mode is possible: ");

    fflush(stdin);
    while((out == 0) && (i < (NB_OP_MODE_MAX-1)))
    {
        c = (uint8_t) getchar();
#if defined(__linux__)
        FLUSH_STDIN();
#endif
        switch(c)
        {
            case '1':
                IParam->PollModes[i] = phHal_eOpModesNfcPassive106;
                i++;
                break;
            case '2':
                IParam->PollModes[i] = phHal_eOpModesNfcPassive212;
                i++;
                break;
            case '3':
                IParam->PollModes[i] = phHal_eOpModesNfcPassive424;
                i++;
                break;
            case '4':
                IParam->PollModes[i] = phHal_eOpModesNfcActive106 ;
                i++;
                break;
            case '5':
                IParam->PollModes[i] = phHal_eOpModesNfcActive212 ;
                i++;
                break;
            case '6':
                IParam->PollModes[i] = phHal_eOpModesNfcActive424 ;
                i++;
                break;
#if defined(__linux__)
            case 's':
#else
            case '\n':
#endif
                if(i == 0)
                {
                    // no dedicated mode set by user
                    IParam->PollModes[i] = phHal_eOpModesNfcPassive106;
                    // inform user about default mode
                    printf("Set default 106kbps passive mode\n");
                    i++;
                }
                out = 1;
                break;
            case 'x':
                i = 0;
                out = 1;
                printf("Polling mode(s) array is set empty\n");
                break;
            default:
                printf("unknown command\n");
                break;
        }
   }
    IParam->PollModes[i] = phHal_eOpModesArrayTerminator;
   return;
}

void SelectTargetPollingMode(phcsExampleTargetParam_t *IParam)
{
    //uint16_t i   = 0;
    //uint16_t out = 0;
    //uint8_t  c   = 0;

   // printf("\nPlease key in the valid target modes:\n\n");
   // printf("* 1 - NFC passive mode @106kbps  --> default choice default\n");
   // printf("* 2 - NFC passive mode @212kbps\n");
   // printf("* 3 - NFC passive mode @424kbps\n\n");
   // printf("* 4 - NFC active  mode @106kbps\n");
   // printf("* 5 - NFC active  mode @212kbps\n");
   // printf("* 6 - NFC active  mode @424kbps\n\n");
   // printf("* x - E(x)it or if invalid input stream\n\n");
   // printf("More than one mode is possible: ");

   // fflush(stdin);
   // while((out == 0) && (i < (NB_OP_MODE_MAX-1)))
   // {
   //     c = (uint8_t) getchar();
   //     FLUSH_STDIN();
   //     switch(c)
   //     {
   //         case '1':
   //             IParam->PollModes[i] = phHal_eOpModesNfcPassive106;
   //             i++;
   //             break;
   //         case '2':
   //             IParam->PollModes[i] = phHal_eOpModesNfcPassive212;
   //             i++;
   //             break;
   //         case '3':
   //             IParam->PollModes[i] = phHal_eOpModesNfcPassive424;
   //             i++;
   //             break;
   //         case '4':
   //             IParam->PollModes[i] = phHal_eOpModesNfcActive106;
   //             i++;
   //             break;
   //         case '5':
   //             IParam->PollModes[i] = phHal_eOpModesNfcActive212;
   //             i++;
   //             break;
   //         case '6':
   //             IParam->PollModes[i] = phHal_eOpModesNfcActive424;
   //             i++;
   //             break;
   //         case '\n':
   //             if(i == 0)
   //             {
   //                 // no dedicated mode set by user
   //                 IParam->PollModes[i] = phHal_eOpModesNfcPassive106;
   //                 // inform user about default mode
   //                 printf("Set default 106kbps passive mode\n");
   //                 i++;
   //             }
   //             out = 1;
   //             break;
   //         case 'x':
   //             i = 0;
   //             out = 1;
   //             break;
   //         default:
   //             printf("unknown command\n");
   //             break;
   //     }
   //}

    // set all modes to be able to connect to them
    IParam->PollModes[0] = phHal_eOpModesNfcPassive106;
    IParam->PollModes[1] = phHal_eOpModesNfcPassive212;
    IParam->PollModes[2] = phHal_eOpModesNfcPassive424;
    IParam->PollModes[3] = phHal_eOpModesNfcActive106;
    IParam->PollModes[4] = phHal_eOpModesNfcActive212;
    IParam->PollModes[5] = phHal_eOpModesNfcActive424;
    IParam->PollModes[6] = phHal_eOpModesArrayTerminator;

   return;
}

void Initialise_P2P_Data_Flow(p2pDataFlowParams_t *P2PParams)
{
    // default configuration is to have 5 data transfers of size NbrOfBytesTransceived
    P2PParams->iterations = 5;
    P2PParams->NbrOfBytesTransceived = 102;
    P2PParams->NbrOfBytesResponse    =  52;

    P2PParams->status                       = NFCSTATUS_SUCCESS;
    P2PParams->DiDUsage                     = 0;
    P2PParams->NADUsage                     = 0;

    P2PParams->AddBytesPerIteration         = 150;
    P2PParams->SubtractBytesPerIteration    = 0;

    P2PParams->AddBytesPerRequest           = 200;
    P2PParams->SubtractBytesPerRequest      = 0;

    // to check WTX capabilities of the target this waiting time can be changed
    P2PParams->LatencyOfResponseToRequest   = 10;
}


void CustomiseTheP2PDataFlow(p2pDataFlowParams_t *P2PParams)
{
    uint32_t        number  = 0;
    uint16_t        c       = 0;
    P2PParams->status       = NFCSTATUS_SUCCESS;

    do
    {
        printf("\n      P2P Initiator related parameters\n");
        printf("\n      Send / Receive Buffersize: %5d\n", DATABUFFERSIZE);
        printf("* - p (p)ayload databytes per transfer:                 %5d \n", P2PParams->NbrOfBytesTransceived);
        printf("* - i (i)terations:                                     %5d \n", P2PParams->iterations);
        printf("* - a (a)dd number of bytes per iteration:              %5d \n", P2PParams->AddBytesPerIteration);
        printf("* - s (s)ubtract number of bytes per iteration:         %5d \n\n", P2PParams->SubtractBytesPerIteration);
        
        printf("* - e  DiD Used during data exchange                    %5d \n", P2PParams->DiDUsage);
        printf("* - n (N)AD Used during data exchange                   %5d \n\n", P2PParams->NADUsage);

        printf("\n      P2P Target related parameters\n");
        printf("* - r (r)esponse data bytes:                                     %5d \n", P2PParams->NbrOfBytesResponse);
        printf("* - d  a(d)d bytes each time initiator issues a new DEP_REQ:     %5d \n", P2PParams->AddBytesPerRequest);
        printf("* - u  s(u)btract bytes each time initiator issues a new DEP_REQ:%5d \n", P2PParams->SubtractBytesPerRequest);

        printf("* - w (w)aiting time (delay [ms]) to initiator request:          %5d \n\n", P2PParams->LatencyOfResponseToRequest);
        printf("* - x e(x)it\n");
        printf("______________\n");
        printf("Your choice: ");

        fflush(stdin);
        c = getchar();
        
        switch(c)
        {
        case 'e':
            {
                printf("Please enter the number: ");
                fflush(stdin);
                P2PParams->status = scanf("%5u", &number);
                if(number > 1)
                    printf("Number must be < 1 and > = 0\n");
                else
                    P2PParams->DiDUsage = (uint8_t) number;
            }
            break;              
        case 'n':
            {
                printf("Please enter the number: ");
                fflush(stdin);
                P2PParams->status = scanf("%5u", &number);
                if(number > 1)
                    printf("Number must be < 1 and > = 0\n");
                else
                    P2PParams->NADUsage = (uint8_t) number;
            }
            break;        
        case 'p':
            {
                printf("Please enter the number: ");
                fflush(stdin);
                P2PParams->status = scanf("%5u", &number);
                if(number > DATABUFFERSIZE || number < 1)
                    printf("Number must be < DATABUFFERSIZE and > = 1\n");
                else
                    P2PParams->NbrOfBytesTransceived = (uint16_t) number;
            }
            break;
        case 'i':
            {
                printf("Please enter the number: ");
                fflush(stdin);
                P2PParams->status = scanf("%5u", &number);
                if(number > 1000 || number < 1)
                    printf("Number must be < 1000 and >= 1\n");
                else
                    P2PParams->iterations = (uint16_t) number;
            }
            break;
        case 'a':
            {
                printf("Please enter the number: ");
                fflush(stdin);
                P2PParams->status = scanf("%5u", &number);
                if(number > 1000)
                    printf("Number must be < 1000 and >= 0\n");
                else
                {
                    P2PParams->AddBytesPerIteration = (uint16_t) number;
                    P2PParams->SubtractBytesPerIteration = 0;
                }
            }
            break;
        case 's':
            {
                printf("Please enter the number: ");
                fflush(stdin);
                P2PParams->status = scanf("%5u", &number);
                if(number > 1000)
                    printf("Number must be < 1000 and >= 0\n");
                else
                {
                    P2PParams->AddBytesPerIteration = 0;
                    P2PParams->SubtractBytesPerIteration = (uint16_t) number;
                }
                break;
            }
        case 'w':
            {
                printf("Please enter the number: ");
                fflush(stdin);
                P2PParams->status = scanf("%5u", &number);
                if(number > 11000)
                    printf("Number must be < 11000ms and >= 0ms\n");
                else
                {
                    P2PParams->LatencyOfResponseToRequest = (uint16_t) number;
                }
            }
            break;
        case 'r':
            {
                printf("Please enter the number: ");
                fflush(stdin);
                P2PParams->status = scanf("%5u", &number);
                if(number > DATABUFFERSIZE)
                    printf("Number must be < DATABUFFERSIZE and >= 0 \n");
                else
                {
                    P2PParams->NbrOfBytesResponse = (uint16_t) number;
                }
            }
            break;
        case 'd':
            {
                printf("Please enter the number: ");
                fflush(stdin);
                P2PParams->status = scanf("%5u", &number);
                if(number > DATABUFFERSIZE)
                    printf("Number must be < DATABUFFERSIZE and >= 0 \n");
                else
                {
                    P2PParams->AddBytesPerRequest = (uint16_t) number;
                    P2PParams->SubtractBytesPerRequest = 0;
                }
            }
            break;
        case 'u':
            {
                printf("Please enter the number: ");
                fflush(stdin);
                P2PParams->status = scanf("%5u", &number);
                if(number > DATABUFFERSIZE)
                    printf("Number must be < DATABUFFERSIZE and >= 0 \n");
                else
                {
                    P2PParams->SubtractBytesPerRequest = (uint16_t) number;
                    P2PParams->AddBytesPerRequest = 0;
                }
            }
            break;
        default:
            {
                // go back to main menu
                c = 'x';
            }
        }
        if (c != 'x')
        {
            printf("Your number was: %d \n", ((int16_t) number));
        }
        #if defined(__linux__)
            FLUSH_STDIN();
        #endif
    }while(c != 'x');
    P2PParams->status = NFCSTATUS_SUCCESS;
}


void SetCommunicationParams(phcsExampleInitiatorParam_t *IParams, p2pDataFlowParams_t *P2PDataFlowParams)
{
    // Set the DiD usage bit according to P2PDataFlow Params
    IParams->communicationparams[0].DIDiUsed = P2PDataFlowParams->DiDUsage;
    IParams->communicationparams[1].DIDiUsed = P2PDataFlowParams->DiDUsage;
    // Set the NAD usage bit according to P2PDataFlow Params
    IParams->communicationparams[0].NfcNADiUsed = P2PDataFlowParams->NADUsage;
    IParams->communicationparams[1].NfcNADiUsed = P2PDataFlowParams->NADUsage;

    // Generate NFCIDs automatically
    IParams->communicationparams[0].NFCIDAuto = 1;
    IParams->communicationparams[1].NFCIDAuto = 1;

    /*
    // NFCID3i will be generated by lower layers (or by HW)
    // no need to initialise since NFCIDAuto = 1
    IParams->communicationparams[0].NFCID3i[0] = 0xA0;
    IParams->communicationparams[0].NFCID3i[1] = 0xA1;
    IParams->communicationparams[0].NFCID3i[2] = 0xA2;
    IParams->communicationparams[0].NFCID3i[3] = 0xA3;
    IParams->communicationparams[0].NFCID3i[4] = 0xA4;
    IParams->communicationparams[0].NFCID3i[5] = 0xA5;
    IParams->communicationparams[0].NFCID3i[6] = 0xA6;
    IParams->communicationparams[0].NFCID3i[7] = 0xA7;
    IParams->communicationparams[0].NFCID3i[8] = 0xA8;
    IParams->communicationparams[0].NFCID3i[9] = 0xA9;
    */

    // remark : The maximum possible length is defined in ISO18092 standard
    IParams->communicationparams[0].GeneralByteLength = NUMBER_GENERAL_BYTES;

    IParams->communicationparams[0].GeneralByte[0] = 'W';
    IParams->communicationparams[0].GeneralByte[1] = 'h';
    IParams->communicationparams[0].GeneralByte[2] = 'o';

    IParams->communicationparams[0].GeneralByte[3] = ' ';
    IParams->communicationparams[0].GeneralByte[4] = 'a';
    IParams->communicationparams[0].GeneralByte[5] = 'r';
    IParams->communicationparams[0].GeneralByte[6] = 'e';

    IParams->communicationparams[0].GeneralByte[7]  = ' ';
    IParams->communicationparams[0].GeneralByte[8]  = 'y';
    IParams->communicationparams[0].GeneralByte[9]  = 'o';
    IParams->communicationparams[0].GeneralByte[10] = 'u';
    IParams->communicationparams[0].GeneralByte[11] = '?';

    // needed for 212 / 424kbps activation passive
    IParams->communicationparams[0].NfcPollPayload[0] = 0x00;
    IParams->communicationparams[0].NfcPollPayload[1] = 0xFF;
    IParams->communicationparams[0].NfcPollPayload[2] = 0xFF;
    IParams->communicationparams[0].NfcPollPayload[3] = 0x00;
    IParams->communicationparams[0].NfcPollPayload[4] = 0x01;
    
    IParams->communicationparams[1].GeneralByteLength = 0; 
    return;
}

void ProcessRemotePollData(phcsExampleInitiatorParam_t *IParams)
{
    uint16_t j;

    if ((IParams->status == NFCSTATUS_SUCCESS))
    {
        for(j=0 ; j< IParams->nbRemoteDev;j++)
        {
            phHal_eOpModes_t currentOpMode;

            printf("------------------------------------------------------------\n");
            printf("Poll Result :  \n");
            printf("------------------------------------------------------------\n");
            printf("hLogHandle : %d\n", IParams->remoteDevInfoList[j].hLogHandle);
            printf("Session Opened : %d\n", IParams->remoteDevInfoList[j].SessionOpened + 1);

            currentOpMode = IParams->remoteDevInfoList[j].OpMode;
            if (currentOpMode == phHal_eOpModesMifare )
            {
                printf("\n*** Mifare ***\n");
                displayStartup106(& IParams->remoteDevInfoList[j].RemoteDevInfo.CardInfo106.Startup106);
            }
            if (currentOpMode == phHal_eOpModesNfcPassive106 )
            {
                printf("\n*** Passive106 ***\n");
                displayStartup106 (& IParams->remoteDevInfoList[j].RemoteDevInfo.NfcInfo106.Startup106);
                displayNfcProtocol(& IParams->remoteDevInfoList[j].RemoteDevInfo.NfcInfo106.NfcProtocol);
            }
            if (currentOpMode == phHal_eOpModesNfcPassive212 )
            {
                printf("\n*** Passive212 ***\n");
                displayStartup212 (& IParams->remoteDevInfoList[j].RemoteDevInfo.NfcInfo212_424.Startup212_424);
                displayNfcProtocol(& IParams->remoteDevInfoList[j].RemoteDevInfo.NfcInfo212_424.NfcProtocol);
            }
            if (currentOpMode == phHal_eOpModesNfcPassive424 )
            {
                printf("\n*** Passive424 ***\n");
                displayStartup212 (& IParams->remoteDevInfoList[j].RemoteDevInfo.NfcInfo212_424.Startup212_424);
                displayNfcProtocol(& IParams->remoteDevInfoList[j].RemoteDevInfo.NfcInfo212_424.NfcProtocol);
            }
            if (currentOpMode == phHal_eOpModesFelica212 )
            {
                printf("\n*** Felica212 ***\n");
                displayStartup212(& IParams->remoteDevInfoList[j].RemoteDevInfo.NfcInfo212_424.Startup212_424);
            }
            if (currentOpMode == phHal_eOpModesFelica424 )
            {
                printf("\n*** Felica424 ***\n");
                displayStartup212(& IParams->remoteDevInfoList[j].RemoteDevInfo.NfcInfo212_424.Startup212_424);
            }
            if (currentOpMode == phHal_eOpModesNfcActive106 )
            {
                printf("\n*** 106 Active ***\n");
                displayNfcProtocol(& IParams->remoteDevInfoList[j].RemoteDevInfo.NfcActiveInfo.NfcProtocol);
            }
            if (currentOpMode == phHal_eOpModesNfcActive212 )
            {
                printf("\n*** 212 Active ***\n");
                displayNfcProtocol(& IParams->remoteDevInfoList[j].RemoteDevInfo.NfcActiveInfo.NfcProtocol);
            }
            if (currentOpMode == phHal_eOpModesNfcActive424 )
            {
                printf("\n*** 424 Active ***\n");
                displayNfcProtocol(& IParams->remoteDevInfoList[j].RemoteDevInfo.NfcActiveInfo.NfcProtocol);
            }
            if (currentOpMode == phHal_eOpModesISO14443_4A )
            {
                printf("\n*** T=CL ***\n");
                displayStartup106(& IParams->remoteDevInfoList[j].RemoteDevInfo.ISO14443_4A_Info.Startup106);
                display14443     (& IParams->remoteDevInfoList[j].RemoteDevInfo.ISO14443_4A_Info.ISO14443_4A_Protocol);
            }
            printf("------------------------------------------------------------\n");
        }
    }
    else
    {
        if( PHNFCSTVAL(CID_NFC_HAL,(IParams->status)) == NFCSTATUS_NO_DEVICE_FOUND )
        {
            printf("Poll : NO_DEVICE_FOUND\n");
        }
        else
        {
            printf("poll : NOT OK");
            displayNfcError(IParams->status);
        }
    }

    if( IParams->status!=NFCSTATUS_SUCCESS )
    {
        IParams->status = (NFCSTATUS) (IParams->status & 0x00FF);
        IParams->status = (NFCSTATUS) (IParams->status | 0x1000);
    }
    if( IParams->status!=NFCSTATUS_SUCCESS && IParams->status!=NFCSTATUS_NO_DEVICE_FOUND )
    {
        printf(">>>>> UNEXPECTED ERROR (%04X) <<<<<<\n",IParams->status);
        displayNfcError(IParams->status);
    }

    printf("\nConnecting to remote device...\n");
    return;
}

void ProcessRWPollData (ExampleRWParam *RWParam)
{
    int16_t j = 0;
    int16_t c = 0;

    if(RWParam->nbRemoteDevMifare > 0)
    {
        for(j = 0; j < (RWParam->nbRemoteDevMifare); j++)
        {
            (RWParam->remoteDevInfoList[(RWParam->nbRemoteDev)]) = *(&RWParam->remoteDevInfoListMifare[j]);
            RWParam->nbRemoteDev++;
        }
        RWParam->status = NFCSTATUS_SUCCESS;
    }

    if ((RWParam->status == NFCSTATUS_SUCCESS))
    {
        for(j = 0 ; j < (RWParam->nbRemoteDev); j++)
        {
            phHal_eOpModes_t currentOpMode;
            printf("---------------------------------\n");
            printf("Poll Result :  Tag number %3d\n", j+1);
            printf("hLogHandle : %d\n", RWParam->remoteDevInfoList[j].hLogHandle);
            printf("Session Opened : %d\n", RWParam->remoteDevInfoList[j].SessionOpened + 1);
            currentOpMode = RWParam->remoteDevInfoList[j].OpMode;

            if (currentOpMode == phHal_eOpModesMifare )
            {
                printf("\n                  *** Mifare ***\n");
                displayStartup106(& RWParam->remoteDevInfoList[j].RemoteDevInfo.CardInfo106.Startup106);
            }
            else if (currentOpMode == phHal_eOpModesISO14443_4A )
            {
                printf("\n                  *** ISO14443-4A ***\n");
                display14443(& RWParam->remoteDevInfoList[j].RemoteDevInfo.ISO14443_4A_Info.ISO14443_4A_Protocol);
            }
            else if (currentOpMode == phHal_eOpModesFelica212 )
            {
                printf("\n                  *** Felica ***\n");
                displayStartup212(& RWParam->remoteDevInfoList[j].RemoteDevInfo.CardInfo212_424.Startup212_424);
            }
            else if (currentOpMode == phHal_eOpModesISO14443_4B)
            {
                printf("\n                  *** ISO14443_4B ***\n");
                display14443_B(& RWParam->remoteDevInfoList[j].RemoteDevInfo.ISO14443_4B_Info.ISO14443_4B_Protocol);
            }
            else if (currentOpMode == phHal_eOpModesJewel)
            {
                printf("\n                  *** Juwel ***\n");
                displayJewel(& RWParam->remoteDevInfoList[j].RemoteDevInfo.JewelInfo);
            }
            //else if (currentOpMode == phHal_eOpModes xyz) // to be added in future
            //{
            //    printf("*** xyz ***\n");
            //    displayStartupxyz(& RWParam->remoteDevInfoList[j].RemoteDevInfo.CardInfo106.Startup106);
            //}
            else if (currentOpMode == phHal_eOpModesInvalid )
            {
                printf("\n                  *** OpModesInvalid ***\n");
            }
            else
            {
                printf("*** Operating mode (remote device) not expected. ***\n");
            }
            printf("------------------------------------------------------------\n");
        }

        if(RWParam->nbRemoteDev > 1)
        {
            printf("\nEnter the number of the device you want to connect to: ");
            fflush(stdin);
            c = (int16_t) getchar();
#if defined(__linux__)
            FLUSH_STDIN();
#endif
            c = c - '0';

            if ((c <= j) && (c >= 1))
                RWParam->remoteDevChosen = c - 1;
            else
            {
                printf("\nYour number chosen was out of the given range - resetting to default");
                RWParam->remoteDevChosen = 0;
            }
            printf("\nYour tag chosen is number: %4d\n", RWParam->remoteDevChosen + 1);
        }
        else
            RWParam->remoteDevChosen = 0;

        RWParam->pollindex = RWParam->remoteDevInfoList[RWParam->remoteDevChosen].OpMode;
    }
    else
    {
        printf("poll : NOT OK");
        displayNfcError((RWParam->status));
    }
}


void ProcessDEPTransceiveData(phcsExampleInitiatorParam_t *IParam, p2pDataFlowParams_t *P2PDataFlowParams)
{
    uint16_t    i = 0;
    
    // collect and process incoming target data
    if( (IParam->status == NFCSTATUS_SUCCESS) &&
        (IParam->MetaChaining == 0) &&
        (IParam->DepMoreInfo.DepFlags.MetaChaining == 0) )
    {
         // in case receive data chaining was ongoing
        // printf("Data fully received from target\n");
        IParam->RcvLength += IParam->receiveindex;
        printf("Iteration: %d\n", IParam->iterations+1);
        printf("Transceive done - Total Response Data Received from Target: %5d\n\n", IParam->RcvLength);
        for(i = 0; i < IParam->RcvLength; i++)
        {
            if ((i > 0) && ((i % 16) == 0)) {printf("\n");}
            printf("%02X ",IParam->receivebuffer[i]);
        }
        printf("\n\n");
        IParam->index = 0;
        IParam->receiveindex = 0;
        IParam->receivecomplete = 1;        // to reload transceive values
    }
    else if ( (IParam->status != NFCSTATUS_SUCCESS || (IParam->DepMoreInfo.DepFlags.MetaChaining == 1)) && IParam->MetaChaining == 0)
    {
        // printf("MI set from target\n");
        // MI needed to receive more data from target?
        if(IParam->DepMoreInfo.DepFlags.MetaChaining == 1)
        {
            // printf("\nReceiveIndex %d\n", IParam->receiveindex);
            if((IParam->receiveindex += IParam->RcvLength) > DATABUFFERSIZE)
            {
                // buffer overflow expected - return with error code
                printf("\n\n!!! Target response data set MI bit but receive buffer size not big enough -\n!!! Meta Chaining from Application side needed\n!!! or enlarge receive buffer!\n\n");   
            }
            else
            {
                // MI bit set - need to chain into receive buffer

                // next line not needed as it is done in the IF(...) already!
                //IParam->receiveindex += IParam->RcvLength;
                
                // printf("ReceiveIndex altered to %d\n", IParam->receiveindex);
                // keep iteration number
                IParam->iterations++;
                // go on with new transceive and append data from buffer[receiveindex] onwards
                return;
            }
        }
        printf("transceive problem! \n");
        displayNfcError(IParam->status);
        IParam->index = 0;
        IParam->receiveindex = 0;
        IParam->MetaChaining = 0;
    }
    
    // prepare and send outgoing data
    if( (((IParam->MetaChainingCounter) > 0) &&
        ((IParam->MetaChaining) == 0) &&
        ((IParam->OpenChainingData) <= (IParam->MTU))) ||
        (IParam->receivecomplete == 1) )
    {
        // printf(" reset data for new iteration\n\n");
        IParam->SendLength = P2PDataFlowParams->NbrOfBytesTransceived;  // reload value for next iteration
        IParam->OpenChainingData = P2PDataFlowParams->NbrOfBytesTransceived;
        IParam->MetaChainingCounter = 0;                                // reset counter for next iteration
        IParam->index = 0;
        IParam->receiveindex = 0;
        IParam->receivecomplete = 0;
    }
    else if( ((IParam->MetaChainingCounter) > 0) &&     // meta chaining ongoing
        ((IParam->MetaChaining) == 0) &&
        ((IParam->OpenChainingData) > (IParam->MTU)))
    {
        // printf(" go on with Meta Chaining...\n");
        IParam->SendLength = IParam->MTU;
        IParam->OpenChainingData -= IParam->MTU;
        IParam->receiveindex = 0;
    }

    // Check if Bytes shall be added or subtracted to/from the total payload within a new iteration
    if( ((P2PDataFlowParams->AddBytesPerIteration) > 0) &&
        ((IParam->MetaChaining) == 0) )
    {
        printf("\nAdd %6d bytes to SendBuffer for next iteration\n", P2PDataFlowParams->AddBytesPerIteration);
        IParam->SendLength = P2PDataFlowParams->NbrOfBytesTransceived;  // reload value for next iteration
        if ((IParam->SendLength + ((P2PDataFlowParams->iterations - IParam->iterations) * (P2PDataFlowParams->AddBytesPerIteration)) >= DATABUFFERSIZE))
        {
            // Set data amount within DATABUFFERSIZE by use of Modulo Operation
            // (IParam->SendLength) = (((IParam->SendLength) += ((P2PDataFlowParams->iterations - IParam->iterations) * (P2PDataFlowParams->AddBytesPerIteration)))) % DATABUFFERSIZE;
            IParam->SendLength = P2PDataFlowParams->NbrOfBytesTransceived;  // reload init value for next iteration
        }
        else
        {
            IParam->SendLength = (IParam->SendLength) + ((P2PDataFlowParams->iterations - IParam->iterations) * (P2PDataFlowParams->AddBytesPerIteration));
        }
        IParam->OpenChainingData = IParam->SendLength;
        IParam->index = 0;
        IParam->receiveindex = 0;
    }
    else if ( ((P2PDataFlowParams->SubtractBytesPerIteration) > 0) &&
              ((IParam->MetaChaining) == 0) )
    {
        printf("\nSubtract %6d bytes from SendBuffer for next iteration\n", P2PDataFlowParams->SubtractBytesPerIteration);
        IParam->SendLength = P2PDataFlowParams->NbrOfBytesTransceived;  // reload value for next iteration
        if ( ((IParam->SendLength - (P2PDataFlowParams->iterations - IParam->iterations) * (P2PDataFlowParams->SubtractBytesPerIteration)) <= 0) ||
             ((IParam->SendLength - (P2PDataFlowParams->iterations - IParam->iterations) * (P2PDataFlowParams->SubtractBytesPerIteration)) >= DATABUFFERSIZE) )
        {
             // Set data amount within DATABUFFERSIZE by use of Modulo Operation
             // IParam->SendLength = DATABUFFERSIZE - (((IParam->SendLength) - ((P2PDataFlowParams->iterations - IParam->iterations) * (P2PDataFlowParams->SubtractBytesPerIteration))) % DATABUFFERSIZE);
             IParam->SendLength = P2PDataFlowParams->NbrOfBytesTransceived;  // Easy way: reload init value for next iteration
        }
        else
        {
            IParam->SendLength = IParam->SendLength - ((P2PDataFlowParams->iterations - IParam->iterations) * P2PDataFlowParams->SubtractBytesPerIteration);
        }
        IParam->OpenChainingData = IParam->SendLength;
        IParam->index = 0;
        IParam->receiveindex = 0;
    }
    else
    {
        // nop
    }
}


void PrepareMetaChainingData(phcsExampleInitiatorParam_t *IParam)
{
    // printf("Iteration: %d\n", IParam->iterations);
    (IParam->iterations) -= 1;
   
    if(IParam->DepMoreInfo.DepFlags.MetaChaining == 0)
    {
        // incoming data stream finished, give info on outgoing stream
        // printf("IParam->SendLength: %d\n", IParam->SendLength);
        // printf("IParam->OpenChainingData: %d\n", IParam->OpenChainingData);
        IParam->receiveindex = 0;
    }
    else if (IParam->DepMoreInfo.DepFlags.MetaChaining == 0 && IParam->MetaChaining == 0)
    {
        // need to chain incoming data to buffer
        // printf("MI recognised, buffer start index: %d\nreceive buffer start index: %d\n", IParam->index, IParam->receiveindex);
        // no data needs to be send
        // IParam->SendLength = 0;
        IParam->index = 0;
        return;
    }
    else if ( (IParam->DepMoreInfo.DepFlags.MetaChaining == 1) && (IParam->MetaChaining == 0) )
    {
        // chaining of incoming data ongoing
        // printf("We go in a new receive loop to get missing target data\n");
        
        IParam->RcvLength = (DATABUFFERSIZE - (IParam->receiveindex));
        // printf("Index: %d   BufferLength: %d \n", IParam->receiveindex, IParam->RcvLength);

        // to keep the status
        IParam->SendLength = 0;
        // bufferindex incremented and receivebuffer re-initialised -> we are ready for a new receive
        return;
    }

    IParam->RcvLength = DATABUFFERSIZE;
    // now we handle the out datastream
    if  ( ((IParam->SendLength) > IParam->MTU) &&
          ((IParam->OpenChainingData) > IParam->MTU) &&
          (IParam->MetaChainingCounter == 0) )
    {
        // Meta-Chaining needed as amount of response data too big
        // printf("Use MI\n");
        IParam->DepMoreInfo.DepFlags.MetaChaining = 1;      // MI bit set
        IParam->OpenChainingData = (IParam->OpenChainingData) - ((uint16_t)(IParam->MTU));
        IParam->SendLength = ((uint16_t)(IParam->MTU));
        IParam->iterations += 1;                            // prevent change in iteration
        IParam->index = IParam->MetaChainingCounter * IParam->MTU;
        IParam->MetaChainingCounter += 1;                   // keep track on progress
        IParam->MetaChaining = 1;                           // indicate that initiator chaining is started
    }
    else if ( (IParam->SendLength == IParam->MTU) &&
              ((IParam->OpenChainingData) >= IParam->MTU) &&
              ((IParam->MetaChaining) == 1) &&
              (IParam->MetaChainingCounter > 0) )
    {
        // If Meta Chaining is in use and used for next transceive, too
        // printf("Further Use of MI\n");
        
        IParam->DepMoreInfo.DepFlags.MetaChaining = 1;      // keep MI bit set
        IParam->iterations += 1;                            // prevent change in iteration
        IParam->SendLength = ((uint16_t)(IParam->MTU));
        IParam->OpenChainingData = (IParam->OpenChainingData) - ((uint16_t)(IParam->MTU));
        IParam->MetaChaining = 1;
        IParam->index = IParam->MetaChainingCounter * (IParam->MTU);
        IParam->MetaChainingCounter += 1;
    }
    else if ( (IParam->SendLength <= IParam->MTU) &&
              ((IParam->OpenChainingData) <= IParam->MTU) &&
              ((IParam->MetaChaining) == 1) &&
              (IParam->MetaChainingCounter > 0) )
    {
        // If Meta Chaining was used and is not needed any more reset it
        // printf("Reset Use of MI\n");
        
        IParam->DepMoreInfo.DepFlags.MetaChaining = 0;      // MI bit reset
        IParam->SendLength = IParam->OpenChainingData;
        IParam->MetaChaining = 0;
        IParam->index = IParam->MetaChainingCounter * (IParam->MTU);
        IParam->MetaChainingCounter = 0;
        IParam->receiveindex = 0;
    }
    else
    {
        // no meta chaining needed - normal DEP_REQ send
        // printf("MI not set (not needed)\n");
        
        IParam->SendLength = IParam->OpenChainingData;
        IParam->receiveindex = 0;
    }
    // printf("buffer start index: %d  SendLength: %d  MI: %d\n", IParam->index, IParam->SendLength, IParam->DepMoreInfo.DepFlags.MetaChaining);
}

void ProcessReceivedData(phcsExampleTargetParam_t *TParams)
{
    uint16_t i = 0;
    // for debug purpose
    //printf("Incoming: RcvLength: %4d    BufferIndex: %4d    MI: %02x\n", TParams->RcvLength, TParams->index, TParams->DepMoreInfo.DepFlags.MetaChaining);
    
    // Check and display if data was received
    if(((TParams->status == NFCSTATUS_SUCCESS) || ((TParams->status) == (PHNFCSTVAL(CID_NFC_JAL, NFCSTATUS_DESELECTED))))&& TParams->DepMoreInfo.DepFlags.MetaChaining == 0x00)
    {
        // printf("Inside receive finished: RcvLength: %4d    BufferIndex: %4d\n\n", TParams->RcvLength, TParams->index);
        TParams->RcvLength += TParams->index;
        // printf("RcvLength: %4d    BufferIndex: %4d\n\n", TParams->RcvLength, TParams->index);
        printf("\nNumber of bytes received from initiator is: %5d\n\n", TParams->RcvLength);
        for(i=0; i < TParams->RcvLength; i++)
        {
            if ((i > 0) && ((i % 16) == 0)) {printf("\n");}
            printf("%02X ", TParams->buffer[i]);
        }
        printf("\n");
        if(TParams->buffer[0] == 'e' && TParams->buffer[1] == 'n' && TParams->buffer[2] == 'd')
        {
            TParams->QuitDEP = 1;
            printf("\n->Last Data packet 'end' from initiator detected<-\nPrepare 'end' to be send back as response, too.\n");
        }
        // All data collected - go on with response data
        TParams->index = 0;
        TParams->RcvLength = 0;
    }
    else if((TParams->DepMoreInfo.DepFlags.MetaChaining) == 0x01)     // MI bit set - more data waiting to be received
    {
        // try to add more data to the buffer given by looking at the MI bit
        // printf("\nMI bit set - Meta Chaining from Application side ongoing\n");
        // printf("RcvLength: %4d    BufferIndex: %4d\n\n", TParams->RcvLength, TParams->index);

        if((TParams->index += TParams->RcvLength) < DATABUFFERSIZE)
        {
            // seems no buffer overflow occures, we go on chaining
            // printf("Buffer seems big enough - we go on\n");
            TParams->RcvLength = 0;
            // printf("RcvLength: %4d    BufferIndex: %4d\n\n", TParams->RcvLength, TParams->index);
            // no iteration as data of same iteration needs to be chained
            TParams->iterations --;
        }
    
        else
        {
            // overflow of buffer detected - end user application needs to handle this
            printf("\n\n!!! MI bit set but buffer size not big enough -\n!!! Meta Chaining from Application side needed\n!!! or enlarge receive buffer!\n\n");
            printf("Receive problem! \nError code: %4x\n", TParams->status);
            // we exit the receive loop
            TParams->QuitDEP = 1;
            return;
        }
    }
    else
    {
        // go on
    }

    // Add up DEP_REQs send by initiator
    TParams->iterations ++;
    // printf("Outgoing: RcvLength: %4d    BufferIndex: %4d    MI: %02x\n", TParams->RcvLength, TParams->index, TParams->DepMoreInfo.DepFlags.MetaChaining);
}

void ProcessTargetModeData(uint8_t *ConnectionDataBuffer, uint8_t ConnectionDataBufferLength)
{
    uint8_t i = 0;
    printf("\n---------------------------------\n*** NFC Protocol ***\n");

    // Display the total length of ATR_REQ
    printf("Length of ATR_REQ: %02x\n", ConnectionDataBuffer[0]);
    // Index 1 and 2 of ATR_REQ indicate the CMD0 and CMD1
    // Index 3 till 13 shows the NFCID3
    printf("NFCID3i: ");
    for(i=3; i <= 13; i++)
    {
        printf("%02X ", ConnectionDataBuffer[i]);
    }
    // DiD used?
    printf("\nDiDi: %02x\n", ConnectionDataBuffer[14]);

    printf("BSi:  %02x\nBRi:  %02x\nPPi:  %02x\nInGeneralBytesLength : 0x%02x\nInGeneralBytes  (hex): ", ConnectionDataBuffer[15], ConnectionDataBuffer[16], ConnectionDataBuffer[17], ConnectionDataBufferLength-17);
    for(i=17; i < ConnectionDataBufferLength; i++)
    {
        printf("%02X ", ConnectionDataBuffer[i]);
    }
    printf("\nInGeneralBytes (char): ");

    for(i=17; i < ConnectionDataBufferLength; i++)
    {
        printf("%2c ", ConnectionDataBuffer[i]);
    }
}


void PrepareResponseData(phcsExampleTargetParam_t *TParams, p2pDataFlowParams_t *p2pDataFlowParams)
{
    if  ( ((TParams->SendLength) > (TParams->MTU)) &&
          ((TParams->OpenChainingData) > (TParams->MTU)) &&
           (TParams->QuitDEP != 0x01) )
    {
        // Meta-Chaining needed as amount of response data too big
        // printf("Use MI\n");
        
        if(TParams->MetaChaining = 0)
        {
            //first run in chaining sequence
            TParams->index = 0;  
        }
        TParams->DepMoreInfo.DepFlags.MetaChaining = 1;         // MI bit SET
        TParams->OpenChainingData = (TParams->OpenChainingData) - ((uint16_t)TParams->MTU);
        TParams->SendLength = ((uint16_t)TParams->MTU);
        TParams->index = TParams->MetaChainingCounter * ((uint16_t)TParams->MTU);
        TParams->MetaChainingCounter += 1;
        TParams->MetaChaining = 1;
    }
    else if ((TParams->SendLength == (TParams->MTU)) &&
             ((TParams->OpenChainingData) >= (TParams->MTU)) &&
             ((TParams->MetaChaining) == 1) &&
              (TParams->QuitDEP != 0x01) )
    {
        // If Meta Chaining is in use and used for next transceive, too
        // printf("Further Use of MI\n");
        TParams->DepMoreInfo.DepFlags.MetaChaining = 1;         // KEEP MI bit set
        TParams->SendLength = ((uint16_t)(TParams->MTU));
        TParams->OpenChainingData = (TParams->OpenChainingData) - ((uint16_t)(TParams->MTU));
        TParams->MetaChaining = 1;
        TParams->index = TParams->MetaChainingCounter * ((uint16_t)TParams->MTU);
        TParams->MetaChainingCounter += 1;
    }
    else if ( (TParams->SendLength <= (TParams->MTU)) &&
              ((TParams->OpenChainingData) <= (TParams->MTU) &&
              ((TParams->MetaChaining) == 1)) &&
              (TParams->QuitDEP != 0x01) )
    {
        // If Meta Chaining was used and is not needed any more: MI RESET
        // printf("Reset Use of MI\n");
        
        TParams->DepMoreInfo.DepFlags.MetaChaining = 0;
        TParams->SendLength = TParams->OpenChainingData;
        TParams->MetaChaining = 0;
        TParams->index = TParams->MetaChainingCounter * ((uint16_t)TParams->MTU);
        TParams->MetaChainingCounter = 0;
    }
    else if( (TParams->buffer[0] == 'e' && TParams->buffer[1] == 'n' && TParams->buffer[2] == 'd') || ((TParams->QuitDEP == 0x01)&& TParams->status != NFCSTATUS_SUCCESS) )
    {
        // In case 'end' was detected from initiator close the data transfer and send 'end' as response, too
        // printf("\nPrepare 'end' to be send back as DEP_RES, too.\n");
        TParams->buffer[0]  = 'e';
        TParams->buffer[1]  = 'n';
        TParams->buffer[2]  = 'd';
        TParams->QuitDEP    = 1;
        TParams->SendLength = 3;
        TParams->index      = 0;
        TParams->OpenChainingData = 3;
        TParams->MetaChaining     = 0;
        TParams->DepMoreInfo.DepFlags.MetaChaining = 0;
    }
    else
    {
        // no meta chaining needed - normal DEP_RES send
        TParams->index = 0;
        // printf("MI not set (not needed)\n");
    }
}

void ProcessResponseData(phcsExampleTargetParam_t *TParams, p2pDataFlowParams_t *P2PDataFlowParams)
{
    if( (TParams->MetaChaining == 0) &&
        (TParams->MetaChainingCounter == 0) )
    {
        // all data was reeived and send so prepare for next iteration
        // printf("Reset DEP_RES data for next DEP_REQ\n");
        TParams->SendLength = P2PDataFlowParams->NbrOfBytesResponse;  // reload value for next iteration
        TParams->OpenChainingData = P2PDataFlowParams->NbrOfBytesResponse;
        TParams->MetaChainingCounter = 0;                                // reset counter for next iteration
        TParams->index = 0;
    }

    // Check if Bytes shall be added or subtracted to/from the total payload within a new iteration
    if( ((P2PDataFlowParams->AddBytesPerRequest) > 0) &&
        ((TParams->MetaChaining) == 0) &&
        ((TParams->QuitDEP) != 1) )
    {
        printf("\nAdd %6d Bytes to response data for next iteration", P2PDataFlowParams->AddBytesPerRequest);
        TParams->SendLength = P2PDataFlowParams->NbrOfBytesResponse;  // reload value for next iteration
        if ((TParams->SendLength + ((TParams->iterations) * (P2PDataFlowParams->AddBytesPerRequest)) >= DATABUFFERSIZE))            // Set data amount within DATABUFFERSIZE by use of Modulo Operation
        {
            TParams->SendLength = P2PDataFlowParams->NbrOfBytesResponse;  // start from the beginning again
        }
        else
        {
            TParams->SendLength = TParams->SendLength + ((TParams->iterations) * (P2PDataFlowParams->AddBytesPerRequest));
        }
        TParams->OpenChainingData = TParams->SendLength;
        TParams->index = 0;
    }
    else if ( ((P2PDataFlowParams->SubtractBytesPerRequest) > 0) &&
              ((TParams->MetaChaining) == 0) &&
              ((TParams->QuitDEP) != 1) )
    {
        printf("\nSubtract %6d Bytes from response data for next iteration", P2PDataFlowParams->SubtractBytesPerRequest);
        TParams->SendLength = P2PDataFlowParams->NbrOfBytesResponse;  // reload value for next iteration
        if ( ((TParams->SendLength - (TParams->iterations) * (P2PDataFlowParams->SubtractBytesPerRequest)) <= 0) ||
             ((TParams->SendLength - (TParams->iterations) * (P2PDataFlowParams->SubtractBytesPerRequest)) >= DATABUFFERSIZE) )
             // Set data amount within DATABUFFERSIZE by use of Modulo Operation
             //TParams->SendLength = DATABUFFERSIZE - (((TParams->SendLength) - ((TParams->iterations) * (P2PDataFlowParams->SubtractBytesPerRequest))) % DATABUFFERSIZE);
        {
             TParams->SendLength = P2PDataFlowParams->NbrOfBytesResponse;  // start from the beginning again
        }
        else
        {
            TParams->SendLength = TParams->SendLength - ((TParams->iterations) * P2PDataFlowParams->SubtractBytesPerRequest);
        }
        TParams->OpenChainingData = TParams->SendLength;
        TParams->index = 0;
    }
    //else
    //{
    //    // just reload...
    //    TParams->SendLength = P2PDataFlowParams->NbrOfBytesResponse;
    //    TParams->OpenChainingData = P2PDataFlowParams->NbrOfBytesResponse;
    //    TParams->index = 0;
    //}

    if(TParams->status == NFCSTATUS_SUCCESS)
    {
        // go on...
    }
    else
    {
        displayNfcError(TParams->status);
    }
}

void SetTargetCommunicationParams(phcsExampleTargetParam_t *TParams)
{
    // initialize the communication parameters
    uint8_t i = 0;

    // set general target flow values
    TParams->communicationparams.RandomNFCIDt = 0;
    TParams->communicationparams.Options.AutoConnectionResponse = 1;
    TParams->communicationparams.Options.PassiveOnly = 0;

    // Enter Sense response data
    TParams->communicationparams.SensRes[0] = 0x04;
    TParams->communicationparams.SensRes[1] = 0x03;

    TParams->communicationparams.SelRes = 0x40; // bit 6 must be set to 1 (NFCIP1 Data exchange protocol compliance)
        
    // set NFCID 1
    for (i = 0;i<10;i++)
    {
        TParams->communicationparams.NFCID1t[i] = (uint8_t) (i+1);
    }

    //// set NFCID 2
    TParams->communicationparams.NFCID2t[0] = 0x01;
    TParams->communicationparams.NFCID2t[1] = 0xFE;
    for (i = 2;i<8;i++)
    {
        TParams->communicationparams.NFCID2t[i] = (uint8_t) (i+0xA0);
    }

    // set NFCID 3
    for (i = 0;i<10;i++)
    {
        TParams->communicationparams.NFCID3t[i] = (uint8_t) (i+1);
    }

    // set PAD Information
    for (i = 0;i<8;i++)
    {
        TParams->communicationparams.PAD[i] = (uint8_t) (0xAA-i);
    }

    TParams->communicationparams.SystemCode[0] = 0xFF;
    TParams->communicationparams.SystemCode[1] = 0xFF;

    // remark : The maximum possible length is defined in ISO18092 standard
    TParams->communicationparams.GeneralBytesLength = NUMBER_GENERAL_BYTES;

    TParams->communicationparams.GeneralBytes[0] = 'N';
    TParams->communicationparams.GeneralBytes[1] = 'F';
    TParams->communicationparams.GeneralBytes[2] = 'C';
    TParams->communicationparams.GeneralBytes[3] = ' ';

    TParams->communicationparams.GeneralBytes[4] = 'f';
    TParams->communicationparams.GeneralBytes[5] = 'r';
    TParams->communicationparams.GeneralBytes[6] = 'o';
    TParams->communicationparams.GeneralBytes[7] = 'm';

    TParams->communicationparams.GeneralBytes[8]  = ' ';
    TParams->communicationparams.GeneralBytes[9]  = 'N';
    TParams->communicationparams.GeneralBytes[10] = 'X';
    TParams->communicationparams.GeneralBytes[11] = 'P';

    // set the modes the target shall respond to
    TParams->PollModes[0] = phHal_eOpModesArrayTerminator;
}

void SetRWCommunicationParams(ExampleRWParam *RWParams)
{
    // Allow more than one T=CL (ISO14443-4A) tag polling
    RWParams->communicationparams.CIDiUsed = 1;

    // NFCIDs shall be chosen automatically
    RWParams->communicationparams.NFCIDAuto = 1;

    // Enter the activation data for FeliCa tag polling
    RWParams->communicationparams.FelicaPollPayload[0] = 0x00;
    RWParams->communicationparams.FelicaPollPayload[1] = 0xFF;
    RWParams->communicationparams.FelicaPollPayload[2] = 0xFF;
    RWParams->communicationparams.FelicaPollPayload[3] = 0x00;
    RWParams->communicationparams.FelicaPollPayload[4] = 0x01;

    // For TypeB polling
    RWParams->communicationparams.ISO14443_4B_AFI = 0x00;
}


uint16_t SAMConfiguration(phHal_sHwReference_t *RWParams)
{
    NFCSTATUS  status = NFCSTATUS_SUCCESS;
    uint16_t IoctlCode = PHHALNFC_IOCTL_PN5XX_SAM_CONFIG;
    uint16_t  length = 0x01;
    uint8_t   SAM_Param[2];
    int8_t    c = 0;
    SAM_Param[0] = 0x00;
    SAM_Param[1] = 0xFF;

    printf("\nConfigure the S2C Interface\n");

    printf("* 1 - Reset Mode   - Only NFC visible --> default action\n");
    printf("* 2 - Virtual Mode - SAM only seen from remote reader\n");
    printf("* 3 - Wired Mode   - SAM only seen from local NFC device\n");
    printf("* 4 - Dual Mode    - SAM and NFC device visible\n");
    printf("______________\n");
    printf("Your choice: ");

    fflush(stdin);
    c = (int8_t) getchar();
#if defined(__linux__)
    FLUSH_STDIN();
#endif
        switch(c)
        {
            default:
            case '1':
                SAM_Param[0] = PHHALNFC_IOCTL_PN5XX_SAM_CONFIG_RESET;  //0x01;
                break;
            case '2':
                {
                    SAM_Param[0] = PHHALNFC_IOCTL_PN5XX_SAM_CONFIG_VIRTUAL;  //0x02;
                    SAM_Param[1] = 0xFF;
                    length       = 0x02;
                }
                break;
            case '3':
                SAM_Param[0] = PHHALNFC_IOCTL_PN5XX_SAM_CONFIG_WIRED;  //0x03;
                break;
            case '4':
                SAM_Param[0] = PHHALNFC_IOCTL_PN5XX_SAM_CONFIG_BOTH;   //0x04;
                break;
        }

    status = phHalNfc_Ioctl( RWParams,
                             IoctlCode,
                             SAM_Param,
                             length,
                             SAM_Param,
                            &length );

    if (status == NFCSTATUS_SUCCESS)
    {
        printf("IOCtl command set succesfully\n");
    }
    else
    {
        displayNfcError(status);
    }
    return c;
}

void SAM_Reset_Configuration(phHal_sHwReference_t *RWParams)
{
    NFCSTATUS  status = NFCSTATUS_SUCCESS;
    uint16_t IoctlCode = PHHALNFC_IOCTL_PN5XX_SAM_CONFIG;
    uint16_t  length = 0x01;
    uint8_t   SAM_Param[2];
    SAM_Param[0] = 0x00;
    SAM_Param[1] = 0xFF;

    SAM_Param[0] = PHHALNFC_IOCTL_PN5XX_SAM_CONFIG_RESET;  //0x01;

    status = phHalNfc_Ioctl( RWParams,
                             IoctlCode,
                             SAM_Param,
                             length,
                             SAM_Param,
                            &length );

    if (status == NFCSTATUS_SUCCESS)
        printf("IOCtl command reset succesfully\n");
    else
        printf("Error number: %d \nMode NOT set\n", status);
    return;
}

void SAM_Target_Example(phHal_sHwReference_t *TargetHW, p2pDataFlowParams_t *P2PDataFlowParams, uint32_t mtu)
{
    //Instantiate target parameters struct
    phcsExampleTargetParam_t TParams;
    // Take over target hardware information
    TParams.HwRef =  TargetHW;
    // Take over number of maximum transfer unit
    TParams.MTU = mtu;
    // Init target parameters struct
    Init_Target_Parameters(&TParams);

    // Enter SAM Target Parameters
    SetTargetCommunicationParams(&TParams);

    // Enter valid NFC Target Modes for SAM Target Mode 106kbps
    TParams.PollModes[0] = phHal_eOpModesNfcPassive106;
    TParams.PollModes[1] = phHal_eOpModesNfcPassive212;
    TParams.PollModes[2] = phHal_eOpModesArrayTerminator;

    printf("\n-->Try to activate the target by use of ISO14443 reader/writer e.g.\n");
    printf("Use Mifare WND 'Show Cards' to explore your target configuration.\n");

    // Start the target mode
    TParams.status = phHalNfc_StartTargetMode(  TParams.HwRef,
                                              &(TParams.communicationparams),
                                                TParams.PollModes,
                                                TParams.ConnectionReq,
                                               &TParams.ConnectionReqBufLength );

    if(TParams.status == NFCSTATUS_SUCCESS)
    {
        printf("Target mode started \n");
    }
    else
    {
        displayNfcError(TParams.status);
        return;
    }

    return;
}

////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
// displaying functions
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////


void displayStartup106(phHal_sPassive106Startup_t* pp106ToDisplay)
{
    uint16_t i;
    printf("SensRes: %02X %02X \n",
           pp106ToDisplay->SensRes[0],
           pp106ToDisplay->SensRes[1]);
    printf("Select Response: %02X \n",pp106ToDisplay->SelRes);
    printf("NfcId1 target Length: %02X\n", pp106ToDisplay->NFCID1tLength);
    printf("NfcId1:");
    for (i = 0; i < pp106ToDisplay->NFCID1tLength ;i++)
    {
        printf(" %02X",pp106ToDisplay->NFCID1t[i]);
    }
    printf("\n");
}

void display14443(phHal_sISO_14443_4A_Protocol_t* p14443ToDisplay)
{
    uint16_t i;
    printf("Historical Bytes Length: %02X\n",p14443ToDisplay->HistoBytesLength);
    printf("Historical Bytes:");
    for (i = 0; i < p14443ToDisplay->HistoBytesLength ;i++)
    {
        printf(" %02X",p14443ToDisplay->HistoBytes[i]);
    }
    printf("\nCID used: %02X \n",p14443ToDisplay->CIDUsed);
    printf("NAD used: %02X \n\n",p14443ToDisplay->NADUsed);
}


void display14443_B(phHal_sISO_14443_4B_Protocol_t* p14443ToDisplay)
{
    uint16_t i;
    // printf("ATQB: %02X\n",p14443ToDisplay->ATQB[0]);

    printf("PUPI:");
    for (i = 0; i < 0x04 ;i++)
    {
        printf(" %02X",p14443ToDisplay->ATQB[i+1]);
    }

    printf("\nApplication Data:");
    for (i = 5; i <= 8 ;i++)
    {
        printf(" %02X",p14443ToDisplay->ATQB[i]);
    }    
   
    printf("\nProtocol Info:");
    for (i = 9; i <= 11 ;i++)
    {
        printf(" %02X",p14443ToDisplay->ATQB[i]);
    }
   
    printf("\nAttribute:");
    for (i = 0; i < p14443ToDisplay->AttribLength ;i++)
    {
        printf(" %02X",p14443ToDisplay->Attrib[i]);
    }

    printf("\nCiD Used: %02X\n",p14443ToDisplay->CIDUsed);
    printf("NAD Used: %02X\n",p14443ToDisplay->NADUsed);

}

void displayJewel(phHal_sPassive106_Jewel_CardInformation_t* pJewelToDisplay)
{

    printf("Sense Request: %02X % 02X\n",pJewelToDisplay->StartupJewel.SensRes[0], pJewelToDisplay->StartupJewel.SensRes[1]);
    printf("Jewel ID: %02X %02X %02X %02X\n",pJewelToDisplay->StartupJewel.JewelId[0], pJewelToDisplay->StartupJewel.JewelId[1], pJewelToDisplay->StartupJewel.JewelId[2], pJewelToDisplay->StartupJewel.JewelId[3]);
    printf("\n");
}


void displayStartup212(phHal_sPassive212_424Startup_t* pp212ToDisplay)
{
    uint8_t i;
    printf("RespCode: %02X\n",pp212ToDisplay->RespCode);
    printf("NFCID2t: ");
    for (i=0;i<8;i++)
    {
        printf("%02X ",pp212ToDisplay->NFCID2t[i]);
    }
    printf("\nPMm: ");
    for (i=0;i<8;i++)
    {
        printf("%02X ",pp212ToDisplay->PMm[i]);
    }
    printf("\nSystemCodeAvailable: %02x\n",pp212ToDisplay->SystemCodeAvailable);
    if(pp212ToDisplay->SystemCodeAvailable !=0)
    {
        printf("SystemCode: %02x %02x\n",pp212ToDisplay->SystemCode[0], pp212ToDisplay->SystemCode[1]);
    }
}

void displayNfcProtocol(phHal_NfcProtocol_t* pNfcProtToDisplay)
{
    uint16_t i;
    printf("*** NFC Protocol ***\n");
    printf("NFCID3t: ");
    for (i=0;i<10;i++)
    {
        printf("%02X ",pNfcProtToDisplay->NFCID3t[i]);
    }
    printf("\nDIDt: %02X\n",pNfcProtToDisplay->DIDtUsed );
    printf("NADt: %02X\n",pNfcProtToDisplay->NADtSup );
    printf("TgGeneralBytesLength : 0x%02X\n",pNfcProtToDisplay->TgGeneralByteLength);
    printf("TgGeneralBytes  (hex): ");
    for (i=0; i < pNfcProtToDisplay->TgGeneralByteLength; i++)
    {
        printf("%02X ",pNfcProtToDisplay->TgGeneralByte[i]);
    }
    printf("\nTgGeneralBytes (char): ");
    for (i=0; i < pNfcProtToDisplay->TgGeneralByteLength; i++)
    {
        printf("%2c ",(pNfcProtToDisplay->TgGeneralByte[i]));
    }
    printf("\n\n");
}


void displayNfcError(NFCSTATUS nfcStatus)
{
    nfcStatus = (NFCSTATUS) PHNFCSTVAL(CID_NFC_HAL, nfcStatus);
    switch (nfcStatus)
    {
        case NFCSTATUS_SUCCESS :
            printf(" nfcStatus =  NFCSTATUS_SUCCESS\n");
            break;
        case NFCSTATUS_INVALID_PARAMETER :
            printf(" nfcStatus =  NFCSTATUS_INVALID_PARAMETER\n");
            break;
        case NFCSTATUS_CMD_ABORTED :
            printf(" nfcStatus =  NFCSTATUS_CMD_ABORTED\n");
            break;
        case NFCSTATUS_BUFFER_TOO_SMALL :
            printf(" nfcStatus =  NFCSTATUS_BUFFER_TOO_SMALL\n");
            break;
        case NFCSTATUS_ALREADY_CONNECTED :
            printf(" nfcStatus =  NFCSTATUS_ALREADY_CONNECTED\n");
            break;
        case NFCSTATUS_MULTI_POLL_NOT_SUPPORTED :
            printf(" nfcStatus =  NFCSTATUS_MULTI_POLL_NOT_SUPPORTED\n");
            break;
        case NFCSTATUS_INVALID_DEVICE :
            printf(" nfcStatus =  NFCSTATUS_INVALID_DEVICE\n");
            break;
        case NFCSTATUS_INVALID_DEVICE_REQUEST :
            printf(" nfcStatus =  NFCSTATUS_INVALID_DEVICE_REQUEST\n");
            break;
        case NFCSTATUS_MORE_INFORMATION :
            printf(" nfcStatus =  NFCSTATUS_MORE_INFORMATION\n");
            break;
        case NFCSTATUS_RF_TIMEOUT :
            printf(" nfcStatus =  NFCSTATUS_RF_TIMEOUT\n");
            break;
        case NFCSTATUS_NO_DEVICE_FOUND :
            printf(" nfcStatus =  NFCSTATUS_NO_DEVICE_FOUND\n");
            break;
        case NFCSTATUS_NO_DEVICE_CONNECTED :
            printf("nfcStatus =  NFCSTATUS_NO_DEVICE_CONNECTED\n" ) ;
            break;
        case NFCSTATUS_COMMAND_NOT_SUPPORTED:
            printf("nfcStatus =  NFCSTATUS_COMMAND_NOT_SUPPORTED\n" ) ;
            break;
        case NFCSTATUS_BOARD_COMMUNICATION_ERROR:
            printf("nfcStatus =  NFCSTATUS_BOARD_COMMUNICATION_ERROR\n" ) ;
            break;
        default:
            printf("nfcStatus - error code is: %02x\n",nfcStatus);
            break;
    }
}




