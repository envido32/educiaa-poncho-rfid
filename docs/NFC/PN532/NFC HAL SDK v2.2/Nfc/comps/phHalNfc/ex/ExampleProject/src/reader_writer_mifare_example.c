/*
 *               Copyright (c), NXP Semiconductors
 *
 *                       (C)NXP B.V.2006
 *         All rights are reserved. Reproduction in whole or in part is
 *        prohibited without the written consent of the copyright owner.
 *       NXP reserves the right to make changes without notice at any time.
 *      NXP makes no warranty, expressed, implied or statutory, including but
 *   not limited to any implied warranty of merchantability or fitness for any
 *  particular purpose, or that the use will not infringe any third party patent,
 *      copyright or trademark. NXP must not be liable for any loss or damage
 *                     arising from its use.
 *
 */


/*!
 * \file reader_writer_mifare_example.c
 * \brief For function declarations see \ref main.h
 *
 *
 * Project: CAS Example Project for NFC MW / HAL
 *
 * $Date: Mon Oct  8 12:11:07 2007 $
 * $Author: beq03097 $
 * $Revision: 1.2 $
 * $Aliases: SDK_HAL_2.2,SDK_HAL_2.2_071008,NFC_FRI_SDK_1.0_071031 $
 *
*/

#include <stdio.h>
#include "main.h"
#include "mifare_example.h"

#define FLUSH_STDIN() \
{                                                 \
  int8_t ch=0;                                    \
  while(((ch = getchar())!= '\n') && (ch != EOF));\
}


void Reader_Writer_Mifare_Example(ExampleRWParam *RWParams)
{
    uint8_t  c = 0;
    printf("\n  Mifare Example reached...\n");

    if ( RWParams->remoteDevInfoList[(RWParams->remoteDevChosen)].RemoteDevInfo.CardInfo106.Startup106.SelRes == 0x00 )       // check SelRes additionally
    {
        printf("\n  Mifare ->UL<- detected\n");
        do{
            printf("--------------------------\n");
            printf("* r - (R)ead Mifare Ultralight (Read 16 bytes command)\n");
            printf("* w - (W)rite Mifare Ultralight (Write 4 bytes command)\n");
            printf("* s - (S)tandard Write Command Mifare (Write 16 bytes command)\n");
            printf("* c - (c)omplete read of UL memory --> default action\n");
            printf("* o - c(o)mplete write of UL memory\n");
            printf("* z - (z)eros written to whole Mifare UL memory\n");
            printf("* x - e(x)it to main menu\n");
            printf("Your Selection: ");
            fflush(stdin);
            c = (uint8_t) getchar();
#if defined(__linux__)
            FLUSH_STDIN();
#endif
            switch (c)
            {
                case 'w' :
                    RWParams->Cmd.MfCmd = phHal_eMifareCmdListMifareWrite4 ;
                    printf("Enter MifareUL address to write to e.g. 04: ");
                    break;
                case 's' :
                    RWParams->Cmd.MfCmd = phHal_eMifareCmdListMifareWrite16 ;
                    printf("Enter Mifare UL address to write to e.g. 04: ");
                    break;
                case 'r' :
                    RWParams->Cmd.MfCmd = phHal_eMifareCmdListMifareRead;
                    printf("Enter MifareUL address to read from e.g. 04: ");
                    break;
                default:
                case 'c' :
                    ReadWholeMifareUL(RWParams);
                    continue;
                case 'o' :
                    {
                        RWParams->write_zeros = 0;
                        WriteWholeMifareUL(RWParams);
                        continue;
                    }
                case 'z' :
                    {
                        RWParams->write_zeros = 1;
                        WriteWholeMifareUL(RWParams);
                        continue;
                    }
                case 'x':
                    return;
            }

            PrepareMifareData(RWParams);        // read data

            RWParams->status = phHalNfc_Transceive( RWParams->HwRef,
                                                   &RWParams->remoteDevInfoList[(RWParams->remoteDevChosen)],
                                                    RWParams->Cmd,
                                                   &RWParams->DepMoreInfo,
                                                    RWParams->buffer,
                                                    RWParams->SendLength,
                                                    RWParams->buffer,
                                                   &RWParams->RcvLength);
            ProcessMifareData(RWParams);

        }while(c != 'x');
    }
    else if ( (RWParams->remoteDevInfoList[(RWParams->remoteDevChosen)].RemoteDevInfo.CardInfo106.Startup106.SelRes == 0x08) || // Mifare 1k detected
              (RWParams->remoteDevInfoList[(RWParams->remoteDevChosen)].RemoteDevInfo.CardInfo106.Startup106.SelRes == 0x28) )  // Mifare 1k Emulation on SMX detected
    {
        printf("\n  Mifare ->1k<- detected\n");

        do{
            printf("--------------------------\n");
            printf("* a - (A)uthenticate to sector using Key_A\n");
            printf("* u - A(u)thenticate to sector using Key_B\n");
            printf("* r - (R)ead Mifare  (16 bytes command)\n");
            printf("* w - (W)rite Mifare (16 bytes command)\n");
            printf("* c - (c)omplete read of Mifare 1k memory --> default action\n");
            printf("* o - c(o)mplete write of Mifare 1k memory\n");
            printf("* z - (z)eros written to whole Mifare 1k memory\n");
            printf("* x - e(x)it to main menu\n");
            printf("Your Selection: ");
            fflush(stdin);
            c = (uint8_t) getchar();
#if defined(__linux__)
            FLUSH_STDIN();
#endif
            switch (c)
            {
                case 'a' :
                    RWParams->Cmd.MfCmd = phHal_eMifareCmdListMifareAuthentA ;
                    printf("Enter Mifare 1k block number to be authenticated e.g. 04: ");
                    break;
                case 'u' :
                    RWParams->Cmd.MfCmd = phHal_eMifareCmdListMifareAuthentB ;
                    printf("Enter Mifare 1k block number to be authenticated e.g. 04: ");
                    break;
                case 'w' :
                    RWParams->Cmd.MfCmd = phHal_eMifareCmdListMifareWrite16 ;
                    printf("Enter Mifare 1k block number to write to e.g. 04: ");
                    break;
                case 'r' :
                    RWParams->Cmd.MfCmd = phHal_eMifareCmdListMifareRead;
                    printf("Enter Mifare 1k block number to read from e.g. 04: ");
                    break;
                default:
                case 'c' :
                    ReadWholeMifare1k(RWParams);
                    continue;
                case 'z':
                    RWParams->write_zeros = 1;
                    WriteWholeMifare1k(RWParams);
                    continue;
                case 'o' :
                    RWParams->write_zeros = 0;
                    WriteWholeMifare1k(RWParams);
                    continue;
                case 'x':
                    return;
            }

            PrepareMifareData(RWParams);        // read data

            RWParams->status = phHalNfc_Transceive( RWParams->HwRef,
                                                   &RWParams->remoteDevInfoList[(RWParams->remoteDevChosen)],
                                                    RWParams->Cmd,
                                                   &RWParams->DepMoreInfo,
                                                    RWParams->buffer,
                                                    RWParams->SendLength,
                                                    RWParams->buffer,
                                                   &RWParams->RcvLength);
            ProcessMifareData(RWParams);
        }while(c != 'x');
    }
    else if ( (RWParams->remoteDevInfoList[(RWParams->remoteDevChosen)].RemoteDevInfo.CardInfo106.Startup106.SelRes == 0x18) ||    // Mifare 4k detected
              (RWParams->remoteDevInfoList[(RWParams->remoteDevChosen)].RemoteDevInfo.CardInfo106.Startup106.SelRes == 0x38) )     // Mifare 4k Emulation on SMX found
    {
        printf("\n  Mifare ->4k<- detected\n");

        do{
            printf("--------------------------\n");
            printf("* a - (A)uthenticate to sector using Key_A\n");
            printf("* u - A(u)thenticate to sector using Key_B\n");
            printf("* r - (R)ead Mifare  (16 bytes command)\n");
            printf("* w - (W)rite Mifare (16 bytes command)\n");
            printf("* c - (c)omplete read of Mifare 4k memory --> default action\n");
            printf("* o - c(o)mplete write of Mifare 4k memory\n");
            printf("* z - (z)eros written to whole Mifare 1k memory\n");
            printf("* x - e(x)it to main menu\n");
            printf("Your Selection: ");
            fflush(stdin);
            c = (uint8_t) getchar();
#if defined(__linux__)
            FLUSH_STDIN();
#endif
            switch (c)
            {
                case 'a' :
                    RWParams->Cmd.MfCmd = phHal_eMifareCmdListMifareAuthentA ;
                    printf("Enter Mifare 4k block number to be authenticated e.g. 04: ");
                    break;
                case 'u' :
                    RWParams->Cmd.MfCmd = phHal_eMifareCmdListMifareAuthentB ;
                    printf("Enter Mifare 4k block number to be authenticated e.g. 04: ");
                    break;
                case 'w' :
                    RWParams->Cmd.MfCmd = phHal_eMifareCmdListMifareWrite16 ;
                    printf("Enter Mifare 4k block number to write to e.g. 0x04: ");
                    break;
                case 'r' :
                    RWParams->Cmd.MfCmd = phHal_eMifareCmdListMifareRead;
                    printf("Enter Mifare 4k block number to read from e.g. 0x04: ");
                    break;
                default:
                case 'c' :
                    ReadWholeMifare4k(RWParams);
                    continue;
                case 'o' :
                    RWParams->write_zeros = 0;
                    WriteWholeMifare4k(RWParams);
                    continue;
                case 'z' :
                    RWParams->write_zeros = 1;
                    WriteWholeMifare4k(RWParams);
                    continue;
                case 'x':
                    return;
            }

            PrepareMifareData(RWParams);        // read data

            RWParams->status = phHalNfc_Transceive( RWParams->HwRef,
                                                   &RWParams->remoteDevInfoList[(RWParams->remoteDevChosen)],
                                                    RWParams->Cmd,
                                                   &RWParams->DepMoreInfo,
                                                    RWParams->buffer,
                                                    RWParams->SendLength,
                                                    RWParams->buffer,
                                                   &RWParams->RcvLength);
            ProcessMifareData(RWParams);

        }while(c != 'x');
    }
    else
    {
        printf("\n\nUnknown card type - stop execution\n");
        return;
    }
    return;
}

void PrepareMifareData(ExampleRWParam *RWParams)
{
    uint16_t n = 0;
    uint16_t i = 0;
    uint16_t v = 0;
    uint16_t w = 0;

    char default_key[12]    = "FFFFFFFFFFFF";
    char default_4data[4]   = "1abc";
    char default_16data[16] = "1abc2def3ghi4jkl";

    fflush(stdin);
    i = getchar();

    if(i == '\n')
    {
        //nop and use default key
        printf("\nCR detected - Set block number to default 0x04\n");
        RWParams->buffer[0] = 4;
        RWParams->SendLength = (uint16_t)strlen(RWParams->buffer);
    }
    else
    {
        // read input stream using scanf()
        ungetc(i, stdin);
        i = scanf("%02x", &(RWParams->buffer[0]));
        RWParams->SendLength = (uint16_t)strlen(RWParams->buffer);

        if (RWParams->buffer[0] == 0)
        {
            RWParams->SendLength = 1;
        }
    }

#if defined(__linux__)
    FLUSH_STDIN();
#endif

    RWParams->RcvLength  = (uint16_t) RWParams->MTU;

    if (RWParams->Cmd.MfCmd == phHal_eMifareCmdListMifareRead)
    {
        return;     // no further input needed
    }
    if (RWParams->Cmd.MfCmd == phHal_eMifareCmdListMifareAuthentA || RWParams->Cmd.MfCmd == phHal_eMifareCmdListMifareAuthentB)
    {
        printf("Please enter the 6 bytes of KEY A/B - Otherwise using default key\n->%c%c%c%c%c%c%c%c%c%c%c%c<-\n->",
                                                                                     default_key[0], default_key[1], default_key[2],  default_key[3],
                                                                                     default_key[4], default_key[5], default_key[6],  default_key[7],
                                                                                     default_key[8], default_key[9], default_key[10], default_key[11]);

        fflush(stdin);
        i = getchar();
        if(i == '\n')
        {
            //nop and use default key
            printf("\nCR detected - using default fab key: ");

            for(i = 0; i < 12; i++)
            {
                RWParams->buffer[i+1] = default_key[i];
                printf("%c", default_key[i]);
            }
            printf("\n");
            n = 13;
        }
        else
        {
            // read input stream using scanf()
            ungetc(i, stdin);
            i = scanf("%12s", &(RWParams->buffer[1]));
            n = (uint16_t) strlen((RWParams->buffer)+1);             // how many chars where read in?
        }

#if defined(__linux__)
        FLUSH_STDIN();
#endif

        if(n < 12)                                      // fill up with default key bytes to match Key A/B needs
        {
            printf("\nCompleting missing key bytes by filling up with default fab key: ");
            for(i = n; i <12; i++)
            {
                RWParams->buffer[i+1] = default_key[i];
                printf("%c", default_key[i]);
            }
            printf("\n");
        }

        RWParams->SendLength = 13;
        for(w=1, v=1; w <= RWParams->SendLength; w+=2, v++)
        {
            RWParams->buffer[v] = ((CHAR2DIGIT(RWParams->buffer[w]) << 4) | (CHAR2DIGIT(RWParams->buffer[w+1])));
        }
        RWParams->SendLength = 7;
    }

    if (RWParams->Cmd.MfCmd == phHal_eMifareCmdListMifareWrite4)
    {
        printf("\nPlease enter 4 ascii you want to write e.g. \n->%c%c%c%c<-\n->", default_4data[0], default_4data[1], default_4data[2], default_4data[3]);

        fflush(stdin);
        i = getchar();
        
        if(i == '\n')
        {
            //nop and use default data
            printf("\nCR detected - using default data bytes: ");

            for(i = 0; i<4; i++)
            {
                RWParams->buffer[i+1] = default_4data[i];
                printf("%c", default_4data[i]);
            }
            printf("\n");
        }
        else
        {
            // read input stream using scanf()
            ungetc(i, stdin);
            i = scanf("%4s", &(RWParams->buffer[1]));       // append data to hex coded address
        }

#if defined(__linux__)
        FLUSH_STDIN();
#endif

        n = (uint16_t) strlen(RWParams->buffer+1);          // how many chars where read in?
        if(n < 4)                                           // fill up with default data bytes to match Mifare needs
        {
            printf("\nCompleting missing bytes by filling up with bytes: ");
            for(i = n; i < 4; i++)
            {
                RWParams->buffer[i+1] = 0;
                printf("0");
            }
            printf("\n");
        }
        RWParams->SendLength = 5;
    }

    if (RWParams->Cmd.MfCmd == phHal_eMifareCmdListMifareWrite16)
    {
        printf("\nPlease enter 16 ascii you want to write e.g. \n->%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c<-\n->",
                                                               default_16data[0],  default_16data[1],  default_16data[2],  default_16data[3],
                                                               default_16data[4],  default_16data[5],  default_16data[6],  default_16data[7],
                                                               default_16data[8],  default_16data[9],  default_16data[10], default_16data[11],
                                                               default_16data[12], default_16data[13], default_16data[14], default_16data[15] );

        fflush(stdin);
        i = getchar();
        
        if(i == '\n')
        {
            //nop and use default data
            printf("\nCR detected - using default data bytes: ");
            for(i = 0; i<16; i++)
            {
                RWParams->buffer[i+1] = default_16data[i];
                printf("%c", default_16data[i]);
            }
            printf("\n");
        }
        else
        {
            // read input stream using scanf()
            // printf("Read input stream by use of scanf()\n");
            ungetc(i, stdin);
            i = scanf("%16s", &(RWParams->buffer[1]));       // append data to hex coded address
        }

#if defined(__linux__)
        FLUSH_STDIN();
#endif

        n = (uint16_t) strlen(RWParams->buffer+1);           // how many chars where read in?
        if(n < 16)                                  // fill up with default data bytes to match Mifare needs
        {
            printf("Completing missing bytes by filling up with bytes: ");
            for(i = n; i < 16; i++)
            {
                RWParams->buffer[i+1] = 0;
                printf("0");
            }
            printf("\n");
        }
        RWParams->SendLength = 17;    //(uint16_t)strlen(RWParams->buffer);
    }
}

void ProcessMifareData(ExampleRWParam *RWParams)
{
    uint16_t i = 0;

    if((RWParams->status == NFCSTATUS_SUCCESS) && (RWParams->Cmd.MfCmd == phHal_eMifareCmdListMifareRead))
    {
        //printf("\nData in char:   ");
        //for(i = 0; i < RWParams->RcvLength; i++)
        //{
        //    printf("%c ",RWParams->buffer[i]);
        //}
        // printf("\nData in hex:    ");

        for(i = 0; i < RWParams->RcvLength; i++)
        {
            printf("%02x ",RWParams->buffer[i]);
        }
        printf("\n");
    }
    else if ((RWParams->status == NFCSTATUS_SUCCESS) && (RWParams->Cmd.MfCmd == phHal_eMifareCmdListMifareWrite16))
    {
        // everything OK
    }
    else
    {
        displayNfcError(RWParams->status);
    }
    //printf("\n");
}

void ReadWholeMifareUL(ExampleRWParam *RWParams)
{
    uint8_t i = 0;
    printf("\n                        4 bytes of |4 bytes of |4 bytes of |4 bytes of |\n");
    printf("                      page selected| page + 1  | page + 2  | page + 3  |\n");
    printf("                                  <-          <-          <-          <-\n");

    RWParams->Cmd.MfCmd = phHal_eMifareCmdListMifareRead;
    for (i = 0; i <= MIFARE_UL_BLOCKS - 1; i++)
    {
        RWParams->buffer[0] = i;
        RWParams->SendLength = 1;
        RWParams->RcvLength  = RWParams->MTU;

        RWParams->status = phHalNfc_Transceive(  RWParams->HwRef,
                                                &RWParams->remoteDevInfoList[(RWParams->remoteDevChosen)],
                                                 RWParams->Cmd,
                                                &RWParams->DepMoreInfo,
                                                 RWParams->buffer,
                                                 RWParams->SendLength,
                                                 RWParams->buffer,
                                                &RWParams->RcvLength );

        if (RWParams->status == NFCSTATUS_SUCCESS)
        {
            printf("Read page %2d onwards:   ", i);
            ProcessMifareData(RWParams);
        }
        else
        {
            displayNfcError(RWParams->status);
            return;
        }
    }
    return;
}

void WriteWholeMifareUL(ExampleRWParam *RWParams)
{
    uint8_t i = 0;
    uint8_t j = 0;

    RWParams->Cmd.MfCmd = phHal_eMifareCmdListMifareWrite4;
    for (i = 4; i <= MIFARE_UL_BLOCKS - 1; i++)    // start with page 4 as page 3 is OTP area!
    {
        RWParams->buffer[0] = i;
        if (RWParams->write_zeros == 0)
        {
            RWParams->buffer[1] = i;
            RWParams->buffer[2] = 'A' + (i-4) *4;
            RWParams->buffer[3] = 'B' + (i-4) *4;
            RWParams->buffer[4] = 'C' + (i-4) *4;
        }
        else
        {
            RWParams->buffer[1] = 0;
            RWParams->buffer[2] = 0;
            RWParams->buffer[3] = 0;
            RWParams->buffer[4] = 0;
        }

        RWParams->SendLength = 5;
        RWParams->RcvLength  = RWParams->MTU;

        printf("\nWrite Block %3d:  ", i);
        for(j=1; j < RWParams->SendLength; j++)
        {
            printf("%02x ", RWParams->buffer[j]);
        }

        RWParams->status = phHalNfc_Transceive( RWParams->HwRef,
                                               &RWParams->remoteDevInfoList[(RWParams->remoteDevChosen)],
                                                RWParams->Cmd,
                                               &RWParams->DepMoreInfo,
                                                RWParams->buffer,
                                                RWParams->SendLength,
                                                RWParams->buffer,
                                               &RWParams->RcvLength);

        if (RWParams->status == NFCSTATUS_SUCCESS)
        {
            printf("   Successful");
        }
        else
        {
            displayNfcError(RWParams->status);
            return;
        }
    }
    printf("\n");
    return;
}

void ReadWholeMifare1k(ExampleRWParam *RWParams)
{
    uint8_t m = 0;

    RWParams->Cmd.MfCmd = phHal_eMifareCmdListMifareAuthentA ;

    for (m = 0; m <= MIFARE_1K_SECTORS * MIFARE_1K_BLOCKS - 1; m++)
    {
        if(m % 4 == 0)     // Auth needed for every sector
        {
            printf("\n------------------------\n");
            printf("Authenticate Sector %d\n", m / MIFARE_1K_BLOCKS);
            printf("------------------------\n");

            RWParams->buffer[0] = m;
            RWParams->buffer[1] = 0xFF;     // use default fab keys
            RWParams->buffer[2] = 0xFF;
            RWParams->buffer[3] = 0xFF;
            RWParams->buffer[4] = 0xFF;
            RWParams->buffer[5] = 0xFF;
            RWParams->buffer[6] = 0xFF;
            RWParams->SendLength = 7;
            RWParams->RcvLength  = RWParams->MTU;
            RWParams->Cmd.MfCmd  = phHal_eMifareCmdListMifareAuthentA ;

            RWParams->status = phHalNfc_Transceive( RWParams->HwRef,
                                                   &RWParams->remoteDevInfoList[(RWParams->remoteDevChosen)],
                                                    RWParams->Cmd,
                                                   &RWParams->DepMoreInfo,
                                                    RWParams->buffer,
                                                    RWParams->SendLength,
                                                    RWParams->buffer,
                                                   &RWParams->RcvLength);

        }
        if(RWParams->status == NFCSTATUS_SUCCESS)
        {
            RWParams->Cmd.MfCmd = phHal_eMifareCmdListMifareRead;
            RWParams->buffer[0]  = m;
            RWParams->SendLength = 1;
            RWParams->RcvLength  = RWParams->MTU;

            RWParams->status = phHalNfc_Transceive( RWParams->HwRef,
                                                   &RWParams->remoteDevInfoList[(RWParams->remoteDevChosen)],
                                                    RWParams->Cmd,
                                                   &RWParams->DepMoreInfo,
                                                    RWParams->buffer,
                                                    RWParams->SendLength,
                                                    RWParams->buffer,
                                                   &RWParams->RcvLength);

            if (RWParams->status == NFCSTATUS_SUCCESS)
            {
                printf("Read block %3d:   ", m);
                ProcessMifareData(RWParams);
            }
            else
            {
                displayNfcError(RWParams->status);
                return;
            }
        }
        else
        {
            displayNfcError(RWParams->status);
            return;
        }
    }
    return;
}

void WriteWholeMifare1k(ExampleRWParam *RWParams)
{
    uint16_t i = 0;
    uint16_t m = 0;

    RWParams->Cmd.MfCmd = phHal_eMifareCmdListMifareAuthentA ;

    for (m = 4; m <= MIFARE_1K_SECTORS * MIFARE_1K_BLOCKS - 1; m++)     // start with block 4!
    {
        if(m % 4 == 0)     // Auth needed for every sector (1 sector has 4 blocks)
        {
            printf("\n------------------------\n");
            printf("Authenticate Sector %d\n", m / MIFARE_1K_BLOCKS);
            printf("------------------------\n");

            RWParams->buffer[0] = (uint8_t) m;
            RWParams->buffer[1] = 0xFF;     // use fab keys
            RWParams->buffer[2] = 0xFF;
            RWParams->buffer[3] = 0xFF;
            RWParams->buffer[4] = 0xFF;
            RWParams->buffer[5] = 0xFF;
            RWParams->buffer[6] = 0xFF;
            RWParams->SendLength = 7;
            RWParams->RcvLength  = RWParams->MTU;
            RWParams->Cmd.MfCmd = phHal_eMifareCmdListMifareAuthentA ;

            RWParams->status = phHalNfc_Transceive( RWParams->HwRef,
                                                   &RWParams->remoteDevInfoList[(RWParams->remoteDevChosen)],
                                                    RWParams->Cmd,
                                                   &RWParams->DepMoreInfo,
                                                    RWParams->buffer,
                                                    RWParams->SendLength,
                                                    RWParams->buffer,
                                                   &RWParams->RcvLength);

        }
        if((RWParams->status == NFCSTATUS_SUCCESS) && (m%4 != 3))   // don�t write to sector trailer
        {
            RWParams->Cmd.MfCmd = phHal_eMifareCmdListMifareWrite16;
            RWParams->buffer[0]  = (uint8_t) m;

            printf("Write block %3d:   ", m);

            if (RWParams->write_zeros == 0)
                RWParams->buffer[1] = (uint8_t) m;
            else
                RWParams->buffer[1] = 0;

            printf("%02x ", RWParams->buffer[1]);
            for(i = 2; i <= MIFARE_1K_SECTORS; i++)
            {
                if (RWParams->write_zeros == 0)
                {
                    //RWParams->buffer[i] = (i%26 + 97);
                    RWParams->buffer[i] = (i+m)%256;
                    printf("%02x ", RWParams->buffer[i]);
                }
                else
                {
                    RWParams->buffer[i] = 0;
                    printf("%02x ", RWParams->buffer[i]);
                }
            }
            printf("\n");
            RWParams->SendLength = 17;
            RWParams->RcvLength  = RWParams->MTU;

            RWParams->status = phHalNfc_Transceive( RWParams->HwRef,
                                                   &RWParams->remoteDevInfoList[(RWParams->remoteDevChosen)],
                                                    RWParams->Cmd,
                                                   &RWParams->DepMoreInfo,
                                                    RWParams->buffer,
                                                    RWParams->SendLength,
                                                    RWParams->buffer,
                                                   &RWParams->RcvLength);

            if (RWParams->status == NFCSTATUS_SUCCESS)
            {
                ProcessMifareData(RWParams);
            }
            else
            {
                displayNfcError(RWParams->status);
                return;
            }
        }
        else if (m%4 == 3)
        {
            // sector trailer operation - currently nop
        }
        else
        {
            displayNfcError(RWParams->status);
            return;
        }
    }
    return;
}

void ReadWholeMifare4k(ExampleRWParam *RWParams)
{
    // uint8_t i = 0;
    uint16_t m = 0;

    RWParams->Cmd.MfCmd = phHal_eMifareCmdListMifareAuthentA;

    for (m = 0; m <= MIFARE_4K_SECTORS * MIFARE_4K_BLOCKS - 1; m++)     // more sectors than Mifare 1k
    {
        if(m % MIFARE_4K_BLOCKS == 0)     // Auth needed for every sector
        {
            printf("\n------------------------\n");
            printf("Authenticate Sector %d\n", m / MIFARE_4K_BLOCKS);
            printf("------------------------\n");

            RWParams->buffer[0] = (uint8_t) m;
            RWParams->buffer[1] = 0xFF;       // use fab keys
            RWParams->buffer[2] = 0xFF;
            RWParams->buffer[3] = 0xFF;
            RWParams->buffer[4] = 0xFF;
            RWParams->buffer[5] = 0xFF;
            RWParams->buffer[6] = 0xFF;
            RWParams->SendLength = 7;
            RWParams->RcvLength  = RWParams->MTU;
            RWParams->Cmd.MfCmd = phHal_eMifareCmdListMifareAuthentA ;

            RWParams->status = phHalNfc_Transceive( RWParams->HwRef,
                                                   &RWParams->remoteDevInfoList[(RWParams->remoteDevChosen)],
                                                    RWParams->Cmd,
                                                   &RWParams->DepMoreInfo,
                                                    RWParams->buffer,
                                                    RWParams->SendLength,
                                                    RWParams->buffer,
                                                   &RWParams->RcvLength);

        }
        if(RWParams->status == NFCSTATUS_SUCCESS)
        {
            RWParams->Cmd.MfCmd = phHal_eMifareCmdListMifareRead;
            RWParams->buffer[0]  = (uint8_t) m;
            RWParams->SendLength = 1;
            RWParams->RcvLength  = RWParams->MTU;

            RWParams->status = phHalNfc_Transceive( RWParams->HwRef,
                                                   &RWParams->remoteDevInfoList[(RWParams->remoteDevChosen)],
                                                    RWParams->Cmd,
                                                   &RWParams->DepMoreInfo,
                                                    RWParams->buffer,
                                                    RWParams->SendLength,
                                                    RWParams->buffer,
                                                   &RWParams->RcvLength);

            if (RWParams->status == NFCSTATUS_SUCCESS)
            {
                printf("Read block %3d:   ", m);
                ProcessMifareData(RWParams);
            }
            else
            {
                displayNfcError(RWParams->status);
                return;
            }
        }
        else
        {
            displayNfcError(RWParams->status);
            return;
        }
    }
    for (m = MIFARE_4K_SECTORS * MIFARE_4K_BLOCKS; m <= ((MIFARE_4K_SECTORS * MIFARE_4K_BLOCKS + MIFARE_4K_UPPER_SECTORS * MIFARE_4K_UPPER_BLOCKS)-1); m++)     // read the remaining sectors with bigger block sizes
    {
        if(m % MIFARE_4K_UPPER_BLOCKS == 0)     // Auth needed for every sector
        {
            printf("\n------------------------\n");
            printf("Authenticate Sector %d\n", (((m - MIFARE_4K_SECTORS * MIFARE_4K_BLOCKS)  / MIFARE_4K_UPPER_BLOCKS)) + MIFARE_4K_SECTORS);
            printf("------------------------\n");

            RWParams->buffer[0] = (uint8_t) m;
            RWParams->buffer[1] = 0xFF;     // use default fab keys
            RWParams->buffer[2] = 0xFF;
            RWParams->buffer[3] = 0xFF;
            RWParams->buffer[4] = 0xFF;
            RWParams->buffer[5] = 0xFF;
            RWParams->buffer[6] = 0xFF;
            RWParams->SendLength = 7;
            RWParams->RcvLength  = RWParams->MTU;
            RWParams->Cmd.MfCmd  = phHal_eMifareCmdListMifareAuthentA ;

            RWParams->status = phHalNfc_Transceive( RWParams->HwRef,
                                                   &RWParams->remoteDevInfoList[(RWParams->remoteDevChosen)],
                                                    RWParams->Cmd,
                                                   &RWParams->DepMoreInfo,
                                                    RWParams->buffer,
                                                    RWParams->SendLength,
                                                    RWParams->buffer,
                                                   &RWParams->RcvLength);

        }
        if(RWParams->status == NFCSTATUS_SUCCESS)
        {
            RWParams->Cmd.MfCmd = phHal_eMifareCmdListMifareRead;
            RWParams->buffer[0]  = (uint8_t) m;
            RWParams->SendLength = 1;
            RWParams->RcvLength  = RWParams->MTU;

            RWParams->status = phHalNfc_Transceive( RWParams->HwRef,
                                                   &RWParams->remoteDevInfoList[(RWParams->remoteDevChosen)],
                                                    RWParams->Cmd,
                                                   &RWParams->DepMoreInfo,
                                                    RWParams->buffer,
                                                    RWParams->SendLength,
                                                    RWParams->buffer,
                                                   &RWParams->RcvLength);

            if (RWParams->status == NFCSTATUS_SUCCESS)
            {
                printf("Read block %2d:   ", m);
                ProcessMifareData(RWParams);
            }
            else
            {
                displayNfcError(RWParams->status);
                return;
            }
        }
        else
        {
            displayNfcError(RWParams->status);
            return;
        }
    }
    return;
}

void WriteWholeMifare4k(ExampleRWParam *RWParams)
{
    uint16_t i = 0;
    uint16_t m = 0;

    RWParams->Cmd.MfCmd = phHal_eMifareCmdListMifareAuthentA ;

    for (m = MIFARE_4K_BLOCKS; m <= MIFARE_4K_SECTORS * MIFARE_4K_BLOCKS - 1; m++)     // start with block 4!
    {
        if(m % MIFARE_4K_BLOCKS == 0)     // Auth needed for every sector (1 sector has 4 blocks)
        {
            printf("\n------------------------\n");
            printf("Authenticate Sector %d\n", (m / MIFARE_4K_BLOCKS));
            printf("------------------------\n");

            RWParams->buffer[0] = (uint8_t) m;
            RWParams->buffer[1] = 0xFF;     // use fab keys
            RWParams->buffer[2] = 0xFF;
            RWParams->buffer[3] = 0xFF;
            RWParams->buffer[4] = 0xFF;
            RWParams->buffer[5] = 0xFF;
            RWParams->buffer[6] = 0xFF;
            RWParams->SendLength = 7;
            RWParams->RcvLength  = RWParams->MTU;
            RWParams->Cmd.MfCmd = phHal_eMifareCmdListMifareAuthentA ;

            RWParams->status = phHalNfc_Transceive( RWParams->HwRef,
                                                   &RWParams->remoteDevInfoList[(RWParams->remoteDevChosen)],
                                                    RWParams->Cmd,
                                                   &RWParams->DepMoreInfo,
                                                    RWParams->buffer,
                                                    RWParams->SendLength,
                                                    RWParams->buffer,
                                                   &RWParams->RcvLength);

        }
        if((RWParams->status == NFCSTATUS_SUCCESS) && (m % MIFARE_4K_BLOCKS != 3))   // don�t write to sector trailer
        {
            RWParams->Cmd.MfCmd = phHal_eMifareCmdListMifareWrite16;
            RWParams->buffer[0]  = (uint8_t) m;

            printf("Write block %3d:   ", m);

            if (RWParams->write_zeros == 0)
                RWParams->buffer[1] = (uint8_t) m;
            else
                RWParams->buffer[1] = 0;

            printf("%02x ", RWParams->buffer[1]);
            for(i = 2; i <= 16; i++)
            {
                if (RWParams->write_zeros == 0)
                {
                    //RWParams->buffer[i] = (i%26 + 97);
                    RWParams->buffer[i] = (i+m)%256;
                    printf("%02x ", RWParams->buffer[i]);
                }
                else
                {
                    RWParams->buffer[i] = 0;
                    printf("%02x ", RWParams->buffer[i]);
                }
            }
            printf("\n");
            RWParams->SendLength = 17;
            RWParams->RcvLength  = RWParams->MTU;

            RWParams->status = phHalNfc_Transceive( RWParams->HwRef,
                                                   &RWParams->remoteDevInfoList[(RWParams->remoteDevChosen)],
                                                    RWParams->Cmd,
                                                   &RWParams->DepMoreInfo,
                                                    RWParams->buffer,
                                                    RWParams->SendLength,
                                                    RWParams->buffer,
                                                   &RWParams->RcvLength);

            if (RWParams->status == NFCSTATUS_SUCCESS)
            {
                ProcessMifareData(RWParams);
            }
            else
            {
                displayNfcError(RWParams->status);
                return;
            }
        }
        else if (m % MIFARE_4K_BLOCKS == 3)
        {
            // sector trailer operation - currently nop
        }
        else
        {
            displayNfcError(RWParams->status);
            return;
        }
    }
    for (m = MIFARE_4K_SECTORS * MIFARE_4K_BLOCKS; m <= ((MIFARE_4K_SECTORS * MIFARE_4K_BLOCKS + MIFARE_4K_UPPER_SECTORS * MIFARE_4K_UPPER_BLOCKS)-1); m++)     // read the remaining sectors with bigger block sizes
    {
        if(m % MIFARE_4K_UPPER_BLOCKS == 0)     // Auth needed for every sector (1 sector has 16 blocks now)
        {
            printf("\n------------------------\n");
            printf("Authenticate Sector %d\n", (((m - MIFARE_4K_SECTORS * MIFARE_4K_BLOCKS)  / MIFARE_4K_UPPER_BLOCKS)) + MIFARE_4K_SECTORS);
            printf("------------------------\n");

            RWParams->buffer[0] = (uint8_t) m;
            RWParams->buffer[1] = 0xFF;     // use default fab keys
            RWParams->buffer[2] = 0xFF;
            RWParams->buffer[3] = 0xFF;
            RWParams->buffer[4] = 0xFF;
            RWParams->buffer[5] = 0xFF;
            RWParams->buffer[6] = 0xFF;
            RWParams->SendLength = 7;
            RWParams->RcvLength  = RWParams->MTU;
            RWParams->Cmd.MfCmd  = phHal_eMifareCmdListMifareAuthentA ;

            RWParams->status = phHalNfc_Transceive( RWParams->HwRef,
                                                   &RWParams->remoteDevInfoList[(RWParams->remoteDevChosen)],
                                                    RWParams->Cmd,
                                                   &RWParams->DepMoreInfo,
                                                    RWParams->buffer,
                                                    RWParams->SendLength,
                                                    RWParams->buffer,
                                                   &RWParams->RcvLength);

        }
        if((RWParams->status == NFCSTATUS_SUCCESS) && (m % MIFARE_4K_UPPER_BLOCKS != 15))   // don�t write to sector trailer
        {
            RWParams->Cmd.MfCmd = phHal_eMifareCmdListMifareWrite16;
            RWParams->buffer[0]  = (uint8_t) m;

            printf("Write block %3d:   ", m);
            RWParams->buffer[1] = (uint8_t) m;

            printf("%02x ", RWParams->buffer[1]);
            for(i = 2; i <= 16; i++)
            {
                if (RWParams->write_zeros == 0)
                {
                    //RWParams->buffer[i] = (i%26 + 97);
                    RWParams->buffer[i] = (i+m)%256;
                    printf("%02x ", RWParams->buffer[i]);
                }
                else
                {
                    RWParams->buffer[i] = 0;
                    printf("%02x ", RWParams->buffer[i]);
                }
            }
            printf("\n");
            RWParams->SendLength = 17;
            RWParams->RcvLength  = RWParams->MTU;

            RWParams->status = phHalNfc_Transceive( RWParams->HwRef,
                                                   &RWParams->remoteDevInfoList[(RWParams->remoteDevChosen)],
                                                    RWParams->Cmd,
                                                   &RWParams->DepMoreInfo,
                                                    RWParams->buffer,
                                                    RWParams->SendLength,
                                                    RWParams->buffer,
                                                   &RWParams->RcvLength);

            if (RWParams->status == NFCSTATUS_SUCCESS)
            {
                ProcessMifareData(RWParams);
            }
            else
            {
                displayNfcError(RWParams->status);
                return;
            }
        }
        else if (m % MIFARE_4K_UPPER_BLOCKS == 15)
        {
            // sector trailer operation - currently nop
        }
        else
        {
            displayNfcError(RWParams->status);
            return;
        }
    }
    return;
}

