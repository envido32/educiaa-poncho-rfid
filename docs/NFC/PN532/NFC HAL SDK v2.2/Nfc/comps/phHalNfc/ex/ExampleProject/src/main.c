/*
 *               Copyright (c), NXP Semiconductors
 *
 *                       (C)NXP B.V.2006
 *         All rights are reserved. Reproduction in whole or in part is
 *        prohibited without the written consent of the copyright owner.
 *       NXP reserves the right to make changes without notice at any time.
 *      NXP makes no warranty, expressed, implied or statutory, including but
 *   not limited to any implied warranty of merchantability or fitness for any
 *  particular purpose, or that the use will not infringe any third party patent,
 *      copyright or trademark. NXP must not be liable for any loss or damage
 *                     arising from its use.
 *
 */


/*!
 * \file main.c
 * \brief For function declarations see \ref main.h
 *
 * Project: CAS Example Project for NFC MW / HAL
 *
 * $Date: Thu Sep 27 09:38:01 2007 $
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Aliases: SDK_HAL_2.2,SDK_HAL_2.2_071008,NFC_FRI_SDK_1.0_071031 $
 *
*/

#include <stdio.h>
#include "main.h"

#define FLUSH_STDIN() \
{                                                 \
  int8_t ch=0;                                    \
  while(((ch = getchar())!= '\n') && (ch != EOF));\
}

int main(void)
{
    int8_t c = 'z';

    // Instantiate struct for first operations
    phcsExampleInitParam_t  InitParams;
    // Instantiate P2P data exchange steering struct
    p2pDataFlowParams_t     P2PParams;

    // Welcome Message...
    printf("Welcome to the NXP NFC HAL Example Project\n");
    printf("...Searching for NFC compliant Hardware...\n");

    // Find NFC compliant HW (Enumerate) and open one device
    Initialise_NFC_Hardware(&InitParams);
    // Set data transfer parameters for P2P mode
    Initialise_P2P_Data_Flow(&P2PParams);

    do
    {
        printf("\n");
        printf("\n*********************************************\n");
        printf(  "       Example Code - HAL NFC SDK v2.2       \n");
        printf(  "            NXP Semiconductors 2007          \n");
        printf(  "*********************************************\n\n");

        printf("     - NFC Peer to Peer Communication Examples -\n\n");

        printf("* i - NFC (I)nitiator Mode\n");
        printf("* t - NFC (T)arget Mode\n");
        printf("* f - Peer2Peer (F)low Control Setup - How (many) data bytes are exchanged\n\n");

        printf("     - Reader Writer Examples -\n\n");

        printf("* c - NFC Device as (C)ard Reader/ Writer Device  --> default choice\n");
        printf("* s - (S)2C Configuration - target example\n\n");

        printf("     - Other Utilities -\n\n");

        printf("* g - (G)et Device Capabilities\n");
        printf("* b - (B)audrate options\n");
        printf("* r - (R)estart Enumeration\n");
        printf("* x - E(x)it\n");
        printf("Your choice: ");

        fflush(stdin);
        c = (int8_t) getchar();
#if defined(__linux__)
        FLUSH_STDIN();
#endif
        switch(c)
        {
            case 'i':
                Peer2Peer_Initiator_Example( &(InitParams.HwRef [InitParams.index]), &P2PParams, InitParams.MTU);
                break;

            case 't':
                Peer2Peer_Target_Example( &(InitParams.HwRef)[InitParams.index], &P2PParams, InitParams.MTU);
                break;

            case 'f':
                CustomiseTheP2PDataFlow( &P2PParams);
                break;
            
            default:
            case 'c':
                Reader_Writer_Example( &(InitParams.HwRef)[InitParams.index], &InitParams);
                break;

            case 'r':
                {
                    //Close Board before looking for other hardware again
                    (InitParams.status) = phHalNfc_Close( &(InitParams.HwRef[InitParams.index]));

                    if(InitParams.status == NFCSTATUS_SUCCESS)
                    {
                        printf("\n>Board Closed properly - Enumerating again...\n");
                        InitParams.NbrOfDevDetected = MAX_BOARD_NUMBER;
                        Initialise_NFC_Hardware( &InitParams);       // Enumerate first if NFC compliant hardware is connected
                    }
                    else    // this is the work around if "hot switching" of HW occures
                    {
                        printf("\nphHalNfc_Close error ! - Error Code: %d\n", InitParams.status);
                        // Start from scratch...
                        InitParams.status = NFCSTATUS_SUCCESS;
                        InitParams.NbrOfDevDetected = MAX_BOARD_NUMBER;
                        Initialise_NFC_Hardware( &InitParams);       // Enumerate first if NFC compliant hardware is connected
                    }
                }
                break;

            case 's':
                {
                    printf("\nPlease be aware that in target mode option '3' does not make sense!\n");
                    // configure the S2C interface connection
                    InitParams.status = SAMConfiguration( &((InitParams.HwRef)[InitParams.index]));
                    // for some modes NFC information needs to be provided
                    if(InitParams.status == '1' || InitParams.status == '4')
                    {
                        SAM_Target_Example( &(InitParams.HwRef)[InitParams.index], &P2PParams, InitParams.MTU);
                    }
                    else if (InitParams.status == '3')
                    {
                        // theoretical possibility which will not be functional
                        printf("\nOption '3' Wired Mode does not make sense in target configuration!\n");
                    }
                    else
                    {
                        // target mode started... or error occured...
                    }
                }
                break;

            case 'g':   // find out the device capabilities (which modes the HW supports)
                {
                    phHal_sDeviceCapabilities_t sDevCapabilities;
                    InitParams.status = phHalNfc_GetDeviceCapabilities( &(InitParams.HwRef[InitParams.index]), &sDevCapabilities);

                    if(InitParams.status == NFCSTATUS_SUCCESS)
                        ShowDeviceCapabilities( &sDevCapabilities);
                    else
                        printf("Get Device Capabilities NOT SUCCESSFUL Error: %d\n", InitParams.status);
                }
                break;

            case 'b':   // find out the device capabilities (which modes the HW supports)
                {
                    if(InitParams.HwRef[(InitParams.index)].LinkType == 2)      // USB Type
                    {
                        printf("\nSorry, baudrate is fixed\n");
                    }
                    else                                                        // com port type detected
                    {
                        printf("\nSorry, not implemented yet\n");
                    }
                }
                break;

            case 'x':   //Close Board before leaving
                {
                    (InitParams.status) = phHalNfc_Close( &(InitParams.HwRef[InitParams.index]));

                    if(InitParams.status == NFCSTATUS_SUCCESS) printf("\n>Board Closed properly\n");
                    else printf("\nphHalNfc_Close error ! - Error Code: %d\n", InitParams.status);
                }
            break;
        }
    }while (c != 'x');
    return InitParams.status;
}



