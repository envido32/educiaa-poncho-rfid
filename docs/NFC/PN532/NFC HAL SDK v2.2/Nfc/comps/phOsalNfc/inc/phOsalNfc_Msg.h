/*
 *         Copyright (c), NXP Semiconductors
 *
 *                       (C)NXP Electronics N.V.2004
 *         All rights are reserved. Reproduction in whole or in part is
 *        prohibited without the written consent of the copyright owner.
 *    NXP reserves the right to make changes without notice at any time.
 *   NXP makes no warranty, expressed, implied or statutory, including but
 *   not limited to any implied warranty of merchantability or fitness for any
 *  particular purpose, or that the use will not infringe any third party patent,
 *   copyright or trademark. NXP must not be liable for any loss or damage
 *                            arising from its use.
 *
 */

/*!
 * \file  phOsalNfc_Msg.h
 *
 * Project: NFC FRI / OSAL
 *
 * $Workfile:: phOsalNfc.h                   $ 
 * $Modtime::                                    $ 
 * $Author: frq09147 $
 * $Revision: 1.1 $
 *
 */

#ifndef PHOSALNFC_MSG_H
#define PHOSALNFC_MSG_H
#ifdef  _WIN32
#include <phOsalNfc_MsgX86.h>
#else
#ifdef __linux__
#include <phOsalNfc.h>
#include <phOsalNfc_Common.h>
#else
#include <phOsalNfc_MsgRtk.h>
#endif
#endif

#endif  /*  PHOSALNFC_MSG_H  */