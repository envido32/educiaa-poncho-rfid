/*
 *         Copyright (c), NXP Semiconductors
 *
 *                       (C)NXP Electronics N.V.2006
 *         All rights are reserved. Reproduction in whole or in part is
 *        prohibited without the written consent of the copyright owner.
 *    NXP reserves the right to make changes without notice at any time.
 *   NXP makes no warranty, expressed, implied or statutory, including but
 *   not limited to any implied warranty of merchantability or fitness for any
 *  particular purpose, or that the use will not infringe any third party patent,
 *   copyright or trademark. NXP must not be liable for any loss or damage
 *                            arising from its use.
 *
 */

/**
 * \file  phOsalNfc_Timer.h
 * \brief Timer Implementation.
 *
 * Project: NFC-FRI
 *
 * $Date: Fri Sep 28 16:36:06 2007 $
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Aliases: SDK_HAL_2.2,SDK_HAL_2.2_071008,NFC_FRI_SDK_1.0_071031 $
 *
 */

#ifndef PHOSALNFC_TIMER_H 
#define PHOSALNFC_TIMER_H 


/**
 *  \name Phase Timer
 *
 * File: \ref phFriNfc_ADD_Timer.h
 *
 */
/*@{*/
#define PH_OSALNFC_TIMER_FILEREVISION "$Revision: 1.1 $" /**< \ingroup grp_file_attributes */
#define PH_OSALNFC_TIMER_FILEALIASES  "$Aliases: SDK_HAL_2.2,SDK_HAL_2.2_071008,NFC_FRI_SDK_1.0_071031 $"      /**< \ingroup grp_file_attributes */
/*@}*/


#include <phNfcTypes.h>


/**
 * \name phase timer-states Definitions
 *
 * These are the definitions of phase timer-states that can be assigned to a phase timer.
 *
 */
/*@{*/
#define PH_OSALNFC_TIMER_RESET      0 /**< \ingroup grp_fri_nfc_auto_dev_dis_seq
                                                           *   Reset State.
                                                           */
#define PH_OSALNFC_TIMER_COUNTING   1 /**< \ingroup grp_fri_nfc_auto_dev_dis_seq
                                                           *   Counting State.
                                                           */
/*@}*/


/*!
 *  TODO: updated this documentation
 *  \ingroup grp_osal_nfc_dev_dis_seq
 *
 *  \brief PhaseTimer Handler definition to Reset the count down.
 *
 *  The generic, (can be user-defined) handler function for the individual Phase Timer resetting count down.
 *  
 *  \par Parameter: Phase Timer
 *       Phase timer pointer has then to :
 *          - access his own IntegrationContext embeded into the \ref phOsalNfc_Timer_t structure
 *          - Reset the \ref CountDownState variable to Reset once the count down has been reset
 *          - has soon as the CountDown stops, reset to zero the \ref CountDownState variable 
 */

typedef NFCSTATUS (*pphOsalNfc_Timer_CbExpired_t)(void* context, NFCSTATUS status);


/*!
 *  \ingroup grp_fri_nfc_auto_dev_dis_seq
 *
 *  \brief PhaseTimer Handler definition to Start the count down.
 *
 *  The generic, (can be user-defined) handler function for the individual Phase Timer start count down.
 *
 *  \par Parameter: Phase Timer
 *       Phase timer pointer has then to :
 *          - Access user own IntegrationContext embeded into the \ref phOsalNfc_Timer_t structure
 *          - Set the \ref CountDownState variable to Counting once it the count down has started (before this function returns)
 *          - As soon as the CountDown stops, set the \ref CountDownState variable to Reset
 *  \par Parameter: CountDown 
 *       Value in msec of the Count Down 
 */

typedef NFCSTATUS (*pphOsalNfc_Timer_StartCountDown_t)(struct phOsalNfc_Timer* PhaseTimer, pphOsalNfc_Timer_CbExpired_t TimerExpiredFunc, void* TimerExpiredFuncCtx, uint32_t    CountDown);

/*!
 *  \ingroup grp_osal_nfc_dev_dis_seq
 *
 *  \brief PhaseTimer Handler definition to Reset the count down.
 *
 *  The generic, (can be user-defined) handler function for the individual Phase Timer resetting count down.
 *
 *  \par Parameter: Phase Timer
 *       Phase timer pointer has then to :
 *          - access his own IntegrationContext embeded into the \ref phOsalNfc_Timer_t structure
 *          - Reset the \ref CountDownState variable to Reset once the count down has been reset
 *          - has soon as the CountDown stops, reset to zero the \ref CountDownState variable 
 */

typedef NFCSTATUS (*pphOsalNfc_Timer_ResetCountDown_t)(struct phOsalNfc_Timer* PhaseTimer);


/**
 * \ingroup grp_osal_nfc_seq
 *
 * Generic Phase Timer. The integration has to ensure it is implemented, if used.
 *
 */
typedef struct phOsalNfc_Timer
{

    uint8_t                                      CountDownState;      /**< State of the phase timer (e.g.: Reset, Counting) */
    void                                         *PlatformContext;  /**< Pointer to Context used by the integration in \ref StartCountDown 
                                                                                  and \ref ResetCountDown functions.
                                                                                  It has to be provided by the user!*/
    pphOsalNfc_Timer_StartCountDown_t  StartCountDown;      /**< Pointer to Start Count Down function. 
                                                                                  It has to be provided by the user!*/
    pphOsalNfc_Timer_ResetCountDown_t  ResetCountDown;      /**<  Pointer to Reset Count Down function. 
                                                                                  It has to be provided by the user!*/

    pphOsalNfc_Timer_CbExpired_t       CbTimerExpired;
    void*                              CbTimerExpiredCtx;
} phOsalNfc_Timer_t;


/**
 * \ingroup grp_fri_nfc_auto_dev_dis_cr
 *
 * \brief This Function allows to easily initialize a timer.
 *  
 *  We must initialize here all needed members: function pointers, state, etc...
 *
 * \param[in,out] osalTimer   Pointer to an uninitialized timer.
 *
 * \param[in]     StartCountDown   Pointer to the Integration-defined function to be used for starting the timer's count down
 *
 * \param[in]     ResetCountDown   Pointer to the Integration-defined function to be used for resetting the timer's count down
 *
 * \param[in]     Context   (Optional) Pointer to the Integration-defined context to be used by the timer
 *
 * \retval NFCSTATUS_SUCCESS                Operation successful.
 * \retval NFCSTATUS_INSUFFICIENT_RESOURCES  Not enough resources available.
 *
 * \b MANDATORY: \n
 *  YES
 */
NFCSTATUS phOsalNfc_Timer_Initialize( phOsalNfc_Timer_t                    *osalTimer, 
                                      pphOsalNfc_Timer_StartCountDown_t    StartCountDown,
                                      pphOsalNfc_Timer_ResetCountDown_t    ResetCountDown,
                                      void                                 *IntegrationContext);

/**
 * \ingroup grp_osal_nfc_seq
 *
 *  \brief This function allows to start the timer.
 *
 *  When the timer expires, the TimerExpiredFunc will be called. 
 *
 * \param[in,out] osalTimer           Pointer to an uninitialized timer.
 *
 * \param[in]     TimerExpiredFunc    Pointer to the Integration-defined function to be used when the timer expires
 *
 * \param[in]     TimerExpiredFuncCtx Pointer to the Integration-defined context to be passed as a parameter of the TimerExpiredFunc function
 *
 * \param[in]     CountDown           Duration of the timer
 *
 * \retval NFCSTATUS_SUCCESS               Operation successful.
 * \retval NFCSTATUS_INVALID_PARAMETER     At least one parameter value is invalid.
 *
 * \b MANDATORY: \n
 *  YES
 */
NFCSTATUS phOsalNfc_Timer_StartCountDown (struct phOsalNfc_Timer*       osalTimer, 
                                          pphOsalNfc_Timer_CbExpired_t  TimerExpiredFunc, 
                                          void*                         TimerExpiredFuncCtx,
                                          uint32_t                      CountDown);
/**
 * \ingroup grp_osal_nfc_seq
 *
 *  \brief This function allows to stop and reset timer.
 *
 * \param[in,out] osalTimer  Pointer to an uninitialized Timer.
 *
 * \retval NFCSTATUS_SUCCESS               Operation successful.
 * \retval NFCSTATUS_INVALID_PARAMETER     At least one parameter value is invalid.
 *
 * \b MANDATORY: \n
 *  YES
 */
NFCSTATUS phOsalNfc_Timer_ResetCountDown (struct phOsalNfc_Timer* osalTimer);

/**
 * \ingroup grp_osal_nfc_seq
 *
 *  \brief This Function allocates the resources of a timer.
 *         This shall be the first one to be called for intializing this timer.
 *
 * \param[in,out] osalTimer  The pointer to the Osal timer
 *
 * \retval NFCSTATUS_SUCCESS               Operation successful.
 * \retval NFCSTATUS_INVALID_PARAMETER     At least one parameter value is invalid.
 *
 * \b MANDATORY: \n
 *  YES
 */
NFCSTATUS phOsalNfc_Timer_Create (struct phOsalNfc_Timer* osalTimer);

/*!
 *  \ingroup grp_osal_nfc_seq
 *
 *  \brief This function frees the resources used by the osal timer.
 *         It shall be called when the timer will not to be used anymore.
 *
 * \param[in,out] osalTimer  The pointer to the Osal timer
 *
 * \retval NFCSTATUS_SUCCESS               Operation successful.
 * \retval NFCSTATUS_INVALID_PARAMETER     At least one parameter value is invalid.
 *
 * \b MANDATORY: \n
 *  YES
 */
NFCSTATUS phOsalNfc_Timer_Release (struct phOsalNfc_Timer* osalTimer);

#endif /* PHOSALNFC_TIMER_H */