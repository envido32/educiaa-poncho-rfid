/*
 *         Copyright (c), NXP Semiconductors
 *
 *                       (C)NXP Electronics N.V.2004
 *         All rights are reserved. Reproduction in whole or in part is
 *        prohibited without the written consent of the copyright owner.
 *    NXP reserves the right to make changes without notice at any time.
 *   NXP makes no warranty, expressed, implied or statutory, including but
 *   not limited to any implied warranty of merchantability or fitness for any
 *  particular purpose, or that the use will not infringe any third party patent,
 *   copyright or trademark. NXP must not be liable for any loss or damage
 *                            arising from its use.
 *
 */

/*!
 * \file  phOsalNfc.h
 *
 * Project: NFC FRI / OSAL
 *
 * $Workfile:: phOsalNfc.h                   $ 
 * $Modtime::                                    $ 
 * $Author: frq09147 $
 * $Revision: 1.1 $
 *
 */



#ifndef PHOSALNFC_H
#define PHOSALNFC_H

#include <phNfcTypes.h>


typedef struct phOsalNfc_Message
{
    uint32_t eMsgType;
    void   * pMsgData;
    uint16_t Size;
} phOsalNfc_Message_t;


/*!
 * \ingroup grp_osal_nfc
 * \brief Thread Function Pointer Declaration.
 *
 * This points to the function ACTUALLY being the "workhorse" of the thread.
 * It is \b not the immediate thread procedure since the API of this function
 * depends on the OS. \n\n
 *
 * Rather, this function shall be called within the body of the thread procedure
 * defined by the underlying, OS-depending OSAL implementation.
 *
 * \b MANDATORY: \n
 *  YES
 */
typedef void(*pphOsalNfc_ThreadFunction_t)(void*);


/*!
 * \ingroup grp_osal_nfc
 * \brief Thread Creation.
 *
 * This function creates a thread in the underlying system. To delete the 
 * created thread use the phOsalNfc_CloseHandle function.
 *
 * In case of the platform is not able to create dynamically it own threads, 
 * the integrator can still modify phLibNfc_Int_Initialize function
 * to avoid the threads creation by handling with already created threads.
 * 
 *
 * \param[in,out] hThread    The handle: The caller has to prepare a void
 *                           pointer (that needs not to be initialised). The
 *                           value (content) of the pointer is set by the function.
 *
 * \param[in] ThreadFunction Pointer to a function within the
 *                           implementation that shall be called by the Thread
 *                           procedure. This represents the Thread main function.
 *                           When this function exits the thread exits.
 * \param[in] Param          A pointer to a user-defined location the thread
 *                            function receives.
 *
 * \retval NFCSTATUS_SUCCESS The operation was successful.
 * \retval NFCSTATUS_INSUFFICIENT_RESOURCES     At least one parameter value is invalid.
 *
 * \b MANDATORY: \n
 *  NO, The createThread function is not called directly by the stack. This function could be called
 *  by phLibNfc_Int_Initialize() in the integration file. It's up to the integrator to implement the 
 *  createThread function or to copy the source code in the integration file.
 */ 
NFCSTATUS phOsalNfc_CreateThread(void                        **hThread, 
                                 pphOsalNfc_ThreadFunction_t     ThreadFunction,
                                 void                         *Param);

/*!
 * \brief Semaphore Creation.
 * This function creates a semaphore in the underlying system. 
 * To delete the created thread use the phOsalNfc_CloseHandle function.
 * 
 * In case of the platform is not able to create dynamically it own semaphore,
 * the integrator can modify phLibNfc_Int_Initialize function to avoid the semaphores 
 * creation by handling with already created semaphores.
 *
 * \note: A wait function applied to the semaphore blocks when its value is zero.
 *
 * \param[in,out] hSemaphore The handle: The caller has to prepare a void
 *                           pointer (that needs not to be initialised). The
 *                           value (content) of the pointer is set by the function.
 *
 * \param[in] InitialValue   The initial value of the Semaphore.
 * \param[in] MaxValue       The maximum value of the Semaphore.
 *
 * \retval NFCSTATUS_SUCCESS The operation was successful.
 *
 * \b MANDATORY: \n
 *  YES
 */
NFCSTATUS phOsalNfc_CreateSemaphore(void     **hSemaphore,
                                    uint8_t    InitialValue,
                                    uint8_t    MaxValue);


/*!
 * \ingroup grp_osal_nfc
 * \brief Closes the handle to a system object.
 *
 * This function closes the handle and frees all resources of a system object (thread or semaphore).
 * 
 * In case of the platform is not able to create thread and semaphore 
 * dynamically, this function has no effect and must return NFCSTATUS_SUCCESS.
 *
 * \param[in] hItem The handle of the system object.
 *
 * \retval NFCSTATUS_SUCCESS            The operation was successful.
 * \retval NFCSTATUS_INVALID_PARAMETER  At least one parameter value is invalid.
 *
 *
 * \b MANDATORY: \n
 *  YES
 */
NFCSTATUS phOsalNfc_CloseHandle(void *hItem);

/*!
 * \ingroup grp_osal_nfc
 * \brief Semaphore-Produce (never waiting).
 *
 * Increment the value of the semaphore. This function never blocks.
 * The purpose is to enable a waiting thread ("consumer") to continue
 * if it has bee waiting because of the Semaphore value set to zero.
 *
 * \param[in] hSemaphore The handle of the Semaphore.
 *
 * \retval NFCSTATUS_SUCCESS                  The operation was successful.
 * \retval NFCSTATUS_INVALID_PARAMETER        At least one parameter value is invalid.
 * \retval NFCSTATUS_SEMAPHORE_PRODUCE_ERROR  The semaphore can not be produced due to a system error or invalid handle .
 *
 * \b MANDATORY: \n
 *  YES
 */
NFCSTATUS phOsalNfc_ProduceSemaphore(void *hSemaphore);


/*!
 * \ingroup grp_osal_nfc
 * \brief Semaphore Consumption (waiting if value is zero).
 *
 * Decrement the value of the semaphore. When the internal value is
 * non-zero, the function continues. If the value is zero however, the
 * function blocks for an infinite time.
 *
 * \param[in] hSemaphore The handle of the Semaphore.
 *
 * \retval NFCSTATUS_SUCCESS                  The operation was successful.
 * \retval NFCSTATUS_INVALID_PARAMETER        At least one parameter value is invalid.
 * \retval NFCSTATUS_SEMAPHORE_PRODUCE_ERROR  The semaphore can not be consumed due to a system error or invalid handle .
 *
 * \b MANDATORY: \n
 *  YES
 */
NFCSTATUS phOsalNfc_ConsumeSemaphore(void *hSemaphore);


/*!
 * \ingroup grp_osal_nfc
 * \brief Semaphore Consumption Extended (waiting if value is zero).
 *
 * Decrement the value of the semaphore. When the internal value is
 * non-zero, the function continues. If the value is zero however, the
 * function blocks.
 *
 * \param[in] hSemaphore The handle of the Semaphore.
 * \param[in] TimeOut    The timeout value in ms.
 *
 * \retval NFCSTATUS_SUCCESS                  The operation was successful.
 * \retval NFCSTATUS_INVALID_PARAMETER        At least one parameter value is invalid.
 * \retval NFCSTATUS_SEMAPHORE_CONSUME_ERROR  The semaphore can not be consumed due to a system error or invalid handle .
 *
 * \b MANDATORY: \n
 *  YES
 */
NFCSTATUS phOsalNfc_ConsumeSemaphoreEx(void *hSemaphore, uint32_t Timeout);


/*!
 * \ingroup grp_osal_nfc
 * \brief Wait/Sleep/Suspend function.
 *
 * Suspends the current thread for the specified time. Enables the
 * scheduler to switch the context meanwhile.
 *
 * \param[in] WaitingTime Time in \b ms to wait.
 *
 * \b MANDATORY: \n
 *  YES
 */
void phOsalNfc_Suspend(uint32_t WaitingTime);


/*!
 * \ingroup grp_osal_nfc
 * \brief Function use to post a message to a thread.
 *
 * \param[in] SourceID   The Thread identifier of the sending thread.
 *                       This value might be different from the caller Id thread.
 * \param[in] DestID     Thread identifier of the destination thread
 * \param[in] pMsg       The message to be sent itself
 *
 * \retval NFCSTATUS_SUCCESS                The operation was successful.
 * \retval NFCSTATUS_INVALID_PARAMETER      Some caller provided parameter was invalid.
 * \retval NFCSTATUS_INSUFFICIENT_RESOURCES The operation could not complete because of lack of resources.
 *
 * \b MANDATORY: \n
 *  YES
 */
NFCSTATUS phOsalNfc_PostMsg(uint32_t SourceID, uint32_t DestID, phOsalNfc_Message_t * pMsg);

/*!
 * \ingroup grp_osal_nfc
 * \brief Function use to receive a message.
 *
 * \param[out] pSourceID   The Thread identifier of the thread that had posted the message.
 * \param[in, out] pMsg    The message to be filled with the received data
 *
 * \retval NFCSTATUS_SUCCESS The operation was successful.
 * \retval NFCSTATUS_INVALID_PARAMETER Some caller provided parameter was invalid.
 * \retval NFCSTATUS_INSUFFICIENT_RESOURCES The operation could not complete because of lack of resources.
 *
 * \b MANDATORY: \n
 *  YES
 */
NFCSTATUS phOsalNfc_ReceiveMsg(uint32_t *pSourceID, phOsalNfc_Message_t* pMsg);


/*!
 * \ingroup grp_osal_nfc
 * \brief Get the task identifier of the caller thread.
 *
 * \retval takID of the caller thread
 *
 * \b MANDATORY: \n
 *  YES
 */
uint32_t phOsalNfc_GetTaskId(void);

/*!
 * \ingroup grp_osal_nfc
 * \brief Allocates some memory.
 *
 * \param[in] Size   Size, in uint8_t, to be allocated
 *
 * \retval NON-NULL value:  The memory was successfully allocated ; the return value points to the allocated memory location
 * \retval NULL:            The operation was not successfull, certainly because of insufficient resources.
 *
 * \b MANDATORY: \n
 *  YES
 */
void * phOsalNfc_GetMemory(uint32_t Size);

/*!
 * \ingroup grp_osal_nfc
 * \brief Free some previously allocated memory.
 *
 * \param[in] pMem  Pointer to the memory block to deallocated
 *
 * \retval None
 *
 * \b MANDATORY: \n
 *  YES
 */
void   phOsalNfc_FreeMemory(void * pMem);

/*!
 * \ingroup grp_osal_nfc
 * \brief Initializes Random Number Generator.
 *
 * \retval None
 *
 * \b MANDATORY: \n
 *  YES
 */
void   phOsalNfc_RandInit( void);

/*!
 * \ingroup grp_osal_nfc
 * \brief Gets Random number. \ref phOsalNfc_RandInit function shall have been called first.
 *
 * \retval Random number
 *
 * \b MANDATORY: \n
 *  YES
 */
uint16_t    phOsalNfc_RandGetNumber( void);

/*!
 * \ingroup grp_osal_nfc
 * \brief Gets system ticks. 
 * This function is used for debug only (tracing) and performance measurement.
 * Thus, it is not mandatory to implement it; moreover, the units that are used is not important, as the
 * difference between 2 calls is the only information that matters.
 *
 * \retval System Ticks
 *
 * \b MANDATORY: \n
 *  NO, this function is used only in debug mode.
 */
uint32_t    phOsalNfc_GetTicks( );

#endif /*  PHOSALNFC_H  */
