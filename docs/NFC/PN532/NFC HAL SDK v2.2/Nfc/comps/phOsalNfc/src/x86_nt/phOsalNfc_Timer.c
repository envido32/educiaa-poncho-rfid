
/*
 *                    Copyright (c), NXP Semiconductors
 *
 *                       (C)NXP Electronics N.V.2006
 *         All rights are reserved. Reproduction in whole or in part is
 *        prohibited without the written consent of the copyright owner.
 *    NXP reserves the right to make changes without notice at any time.
 *   NXP makes no warranty, expressed, implied or statutory, including but
 *   not limited to any implied warranty of merchantability or fitness for any
 *  particular purpose, or that the use will not infringe any third party patent,
 *   copyright or trademark. NXP must not be liable for any loss or damage
 *                            arising from its use.
 *
 */

/*!
 * \file  phOsalNfc_Timer.c
 * \brief Timer implementation.
 *
 * Project: NFC-FRI
 *
 * $Date: Fri Sep 28 16:36:13 2007 $
 * $Author: frq09147 $
 * $Revision: 1.1 $
 * $Aliases: SDK_HAL_2.2,SDK_HAL_2.2_071008,NFC_FRI_SDK_1.0_071031 $
 *
 */

#include <stdlib.h>
#include <Windows.h>
#include <assert.h>
#include <phNfcTypes.h>
#include <phOsalNfc.h>
#include <phOsalNfc_Timer.h>
#include <phNfcStatus.h>
#include <phNfcCompId.h>

/**
 * \ingroup grp_osal_nfc_seq
 *
 * Example of implementation of Phase Timer for x86 platform.
 *
 */

typedef struct phOsalNfc_TimerX86Nt
{
    void *                          hThread;
    void *                          hAbort;
    void *                          hStartCount;
    void *                          hResourcesFreed;
    void *                          semaphore;
    uint8_t                         cbCalled;
    uint8_t                         dontCallCB;
    uint32_t                        countDown;
    uint8_t                         freeResources;
} phOsalNfc_TimerX86Nt_t;

/*  This function will allow the user to easily initialize a Phase Timer.
    Here, we must initialize all needed members: function pointers, state, etc...
    Please note that the current API of this function has still some missing parameters (function pointers...)
    that should be added later on, during the implementation.
*/
NFCSTATUS phOsalNfc_Timer_Initialize(       phOsalNfc_Timer_t                     *OsalTimer, 
                                            pphOsalNfc_Timer_StartCountDown_t     StartCountDown,
                                            pphOsalNfc_Timer_ResetCountDown_t     ResetCountDown,
                                            void                                  *IntegrationContext)
{
    NFCSTATUS   status  =   PHNFCSTVAL(CID_NFC_NONE, NFCSTATUS_INVALID_PARAMETER);

    if(     (NULL == OsalTimer) 
        ||  (NULL == StartCountDown) 
        ||  (NULL == ResetCountDown)
        )
    {
        status = PHNFCSTVAL(CID_FRI_NFC_AUTO_DEV_DIS, NFCSTATUS_INVALID_PARAMETER);
    }
    else
    {
        OsalTimer->CountDownState     =   PH_OSALNFC_TIMER_RESET;
        OsalTimer->StartCountDown     =   StartCountDown;
        OsalTimer->ResetCountDown     =   ResetCountDown;
        OsalTimer->PlatformContext    =   IntegrationContext;
        OsalTimer->CbTimerExpired     =   NULL;
        OsalTimer->CbTimerExpiredCtx  =   NULL;
        status                        =   NFCSTATUS_SUCCESS;
    }    
    return status;
}

DWORD WINAPI phOsalNfc_Timer_Procedure(LPVOID iValue)
{
    DWORD       dwRes;
    phOsalNfc_Timer_t* osalTimer     =   (phOsalNfc_Timer_t*)       iValue;
    phOsalNfc_TimerX86Nt_t*  timer    =   (phOsalNfc_TimerX86Nt_t*)  osalTimer->PlatformContext;

    osalTimer->CountDownState   =   PH_OSALNFC_TIMER_RESET;

    while(!timer->freeResources)
    {
        osalTimer->CountDownState =   PH_OSALNFC_TIMER_RESET;

        /* Wait for Starting Event */
        dwRes = WaitForSingleObject(timer->hStartCount, INFINITE);

        phOsalNfc_ConsumeSemaphore( timer->semaphore );
        osalTimer->CountDownState   =   PH_OSALNFC_TIMER_COUNTING;
        timer->cbCalled   =   0;
        timer->dontCallCB    =   0;  
        phOsalNfc_ProduceSemaphore( timer->semaphore);

        /* Reseting Abort event */
        ResetEvent(timer->hAbort);

        /* Waiting for Abort event */
        dwRes = WaitForSingleObject(timer->hAbort, timer->countDown);

        while(NULL == timer->hThread)
        {
            phOsalNfc_Suspend(0);
        }

        phOsalNfc_ConsumeSemaphore( timer->semaphore );
        if(     WAIT_TIMEOUT == dwRes
            &&  NULL        !=  osalTimer->CbTimerExpired
            &&  0           ==  timer->dontCallCB
           )
        {
            /* The PhaseTimer has expired */
           osalTimer->CbTimerExpired(osalTimer->CbTimerExpiredCtx, NFCSTATUS_SUCCESS);
           timer->cbCalled       =   1;
        }
        else
        {
            /* The PhaseTimer has been cancelled */
        }
        phOsalNfc_ProduceSemaphore( timer->semaphore );
    }
    SetEvent(timer->hResourcesFreed);
    return TRUE;
}

NFCSTATUS phOsalNfc_Timer_StartCountDown(phOsalNfc_Timer_t* osalTimer, pphOsalNfc_Timer_CbExpired_t TimerExpiredFunc, void* TimerExpiredFuncCtx, uint32_t   CountDown)
{
    phOsalNfc_TimerX86Nt_t*  timer   =   (phOsalNfc_TimerX86Nt_t*)osalTimer->PlatformContext;
    NFCSTATUS   status =   PHNFCSTVAL(CID_NFC_NONE, NFCSTATUS_INVALID_PARAMETER);

    if(     (NULL == osalTimer) 
        ||  (NULL == timer) 
        ||  (NULL == TimerExpiredFunc) 
        ||  (NULL == TimerExpiredFuncCtx) 
        ||  (NULL == timer->hThread) 
        ||  (NULL == timer->hStartCount) 
        ||  (NULL == timer->hAbort) 
        ||  (NULL == timer->hResourcesFreed)
        ||  (NULL == timer->semaphore )
        )
    {
        status =   PHNFCSTVAL(CID_NFC_NONE, NFCSTATUS_INVALID_PARAMETER);
    }
    else
    {
        osalTimer->CbTimerExpired    = TimerExpiredFunc;
        osalTimer->CbTimerExpiredCtx = TimerExpiredFuncCtx;
        /*  Thread already started: just ask it to wait again!  */
        timer->countDown = CountDown;
        ResetEvent(timer->hAbort);
        if(!SetEvent(timer->hStartCount))
        {
            assert(0);
        }
        while(PH_OSALNFC_TIMER_COUNTING != osalTimer->CountDownState)
        {
            /* Wait for PT to be Reset */
            phOsalNfc_Suspend(0);
        }    
        status  =   NFCSTATUS_SUCCESS;
    }    
    return status;
}

NFCSTATUS phOsalNfc_Timer_ResetCountDown(phOsalNfc_Timer_t* osalTimer)
{
    phOsalNfc_TimerX86Nt_t*  timer   =   (phOsalNfc_TimerX86Nt_t*)osalTimer->PlatformContext;
    NFCSTATUS   status =   PHNFCSTVAL(CID_NFC_NONE, NFCSTATUS_INVALID_PARAMETER);

    if(NULL ==  timer->hAbort)
    {
        status = PHNFCSTVAL(CID_NFC_NONE, NFCSTATUS_INVALID_PARAMETER);
    }
    else if(PH_OSALNFC_TIMER_RESET   ==  osalTimer->CountDownState)
    {
        /* Already in reset state */
        status = NFCSTATUS_SUCCESS;
    }
    else if(!SetEvent(timer->hAbort))
    {
        /* Do the Reset */
        status = PHNFCSTVAL(CID_NFC_NONE, NFCSTATUS_INVALID_PARAMETER);
    }
    else
    {
        phOsalNfc_ConsumeSemaphore( timer->semaphore );
        if(     0   ==  timer->cbCalled )
        {
            timer->dontCallCB =   1;  
            status = NFCSTATUS_SUCCESS;
        }
        else
        {
            status = PHNFCSTVAL(CID_NFC_NONE, NFCSTATUS_CMD_ABORTED);
        }

        phOsalNfc_ProduceSemaphore( timer->semaphore );

        while(PH_OSALNFC_TIMER_RESET    !=  osalTimer->CountDownState)
        {
            /* Wait for PT to be Reset */
            phOsalNfc_Suspend(0);
        }
    }

    return status;
}



NFCSTATUS phOsalNfc_Timer_Create(phOsalNfc_Timer_t* osalTimer)
{
    NFCSTATUS   status  =   PHNFCSTVAL(CID_NFC_NONE, NFCSTATUS_INVALID_PARAMETER);
    phOsalNfc_TimerX86Nt_t*  timer;    

    if( (NULL != osalTimer))
    {
        timer                       =   phOsalNfc_GetMemory(sizeof(phOsalNfc_TimerX86Nt_t));
        osalTimer->PlatformContext  =   (void*) timer;
        memset (timer, 0x00, sizeof(phOsalNfc_TimerX86Nt_t));
        timer->hAbort               =   CreateEvent(NULL, FALSE, FALSE, NULL);
        timer->hStartCount          =   CreateEvent(NULL, FALSE, FALSE, NULL);
        timer->hResourcesFreed      =   CreateEvent(NULL, FALSE, FALSE, NULL);
        timer->freeResources        =   0;
        timer->countDown            =   0,
        timer->cbCalled             =   0;
        timer->dontCallCB           =   0;  
        timer->hThread              =   CreateThread(NULL, 0, phOsalNfc_Timer_Procedure, osalTimer, 0, NULL);
        osalTimer->CbTimerExpired   =   NULL;
        osalTimer->CbTimerExpiredCtx=   NULL;
        status  =   phOsalNfc_CreateSemaphore( &timer->semaphore, 1, 1);
    }
    return status;
}

NFCSTATUS phOsalNfc_Timer_Release(phOsalNfc_Timer_t* osalTimer)
{
    phOsalNfc_TimerX86Nt_t*  timer ;    
    HANDLE  event;
    DWORD   dwRes;

    NFCSTATUS   status  =   PHNFCSTVAL(CID_NFC_NONE, NFCSTATUS_INVALID_PARAMETER);

    if( NULL == osalTimer)
    {
        status  =   PHNFCSTVAL(CID_NFC_NONE, NFCSTATUS_INVALID_PARAMETER);        
    }
    else
    {
      timer   =   (phOsalNfc_TimerX86Nt_t*)osalTimer->PlatformContext;
      
      if( (NULL == timer) || (NULL == timer->hThread) || (NULL == timer->hStartCount) ||
          (NULL == timer->hAbort) || (NULL == timer->hResourcesFreed) || (NULL == timer->semaphore ))
      {
          status = PHNFCSTVAL(CID_NFC_NONE, NFCSTATUS_INVALID_PARAMETER);       
      }
      else
      {
          /*    Set CB to NULL, so that it is not called by the Timer */
          osalTimer->CbTimerExpired =   NULL;

          /*    Set Timeout to 0, so that wez don't wait useless for timer->countDown */
          timer->countDown      =   0;

          /*    Abort the timer, free resources, ... */
          event   =   timer->hResourcesFreed;
          timer->freeResources = 1;
          SetEvent(timer->hStartCount);
          event   =   timer->hResourcesFreed;
          dwRes = WaitForMultipleObjects(1, &event, FALSE, INFINITE);
          CloseHandle(timer->hThread);
          CloseHandle(timer->hStartCount);
          CloseHandle(timer->hAbort);
          CloseHandle(timer->hResourcesFreed);
          status  =   phOsalNfc_CloseHandle( timer->semaphore );
          timer->hThread          =   NULL;
          timer->hStartCount      =   NULL;
          timer->hAbort           =   NULL;
          timer->hResourcesFreed  =   NULL;
          timer->semaphore        =   NULL;
          phOsalNfc_FreeMemory(timer);
      } 
    }
    return status;
}
