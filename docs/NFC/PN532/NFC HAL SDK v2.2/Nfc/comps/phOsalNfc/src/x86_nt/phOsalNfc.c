/*
 *         Copyright (c), NXP Semiconductors
 *
 *                       (C)NXP Electronics N.V.2004
 *         All rights are reserved. Reproduction in whole or in part is
 *        prohibited without the written consent of the copyright owner.
 *    NXP reserves the right to make changes without notice at any time.
 *   NXP makes no warranty, expressed, implied or statutory, including but
 *   not limited to any implied warranty of merchantability or fitness for any
 *  particular purpose, or that the use will not infringe any third party patent,
 *   copyright or trademark. NXP must not be liable for any loss or damage
 *                            arising from its use.
 *
 */
     
     
/*!
 * \file phOsalNfc.c
 *
 * Project: NFC MW / HAL
 *
 * $Workfile:: phOsalNfc.c                                       $ 
 * $Modtime::                                      $ 
 * $Author: frq09147 $
 * $Revision: 1.1 $
 *
 */

#include <stdio.h>
#include <time.h>
#include <windows.h>

#include <phOsalNfc.h>
#include <phNfcStatus.h>
#include <phNfcCompId.h>

#define PHOSALNFC_MESSAGE_BASE WM_USER+0x3FF    /*  As an example */

typedef struct phOsalNfc_sOsalHandle
{
   pphOsalNfc_ThreadFunction_t   ThreadFunction;
   void                     * Params;
   HANDLE                     ObjectHandle;
   DWORD                      ThreadId;
} phOsalNfc_sOsalHandle_t;

uint32_t phOsalNfc_GetTaskId(void)
{
    uint32_t taskID = GetCurrentThreadId();
    return taskID;
}

DWORD WINAPI phOsalNfc_ThreadProcedure(LPVOID lpParameter)
{
    phOsalNfc_sOsalHandle_t* thread_handle = (phOsalNfc_sOsalHandle_t*) lpParameter;

    thread_handle->ThreadFunction(thread_handle->Params);
    return 0;
}

NFCSTATUS phOsalNfc_CreateThread(void                        **hThread, 
                                 pphOsalNfc_ThreadFunction_t     ThreadFunction,
                                 void                         *Param)
{
    NFCSTATUS creation_status = NFCSTATUS_SUCCESS;

    phOsalNfc_sOsalHandle_t *thread_handle = malloc(sizeof(phOsalNfc_sOsalHandle_t));

    if(thread_handle != NULL)
    {
        thread_handle->Params = Param;
        thread_handle->ThreadFunction = ThreadFunction;
        thread_handle->ObjectHandle = 
        CreateThread(NULL, 0, phOsalNfc_ThreadProcedure, thread_handle, 0, &(thread_handle->ThreadId) /*NULL*/);
        if (thread_handle->ObjectHandle != NULL)
        {
            *(phOsalNfc_sOsalHandle_t**) hThread = thread_handle;
        }
        else
        {
            creation_status = PHNFCSTVAL(CID_NFC_NONE, NFCSTATUS_INSUFFICIENT_RESOURCES);
            free(thread_handle);
        }
    }
    else
    {
        creation_status = PHNFCSTVAL(CID_NFC_NONE, NFCSTATUS_INSUFFICIENT_RESOURCES);
    }

    return creation_status;
}

NFCSTATUS phOsalNfc_CloseHandle(void *hItem)
{
    NFCSTATUS close_status = NFCSTATUS_SUCCESS;
    phOsalNfc_sOsalHandle_t *item_handle = (phOsalNfc_sOsalHandle_t*)hItem;

    if (item_handle != NULL)
    {
        CloseHandle(item_handle->ObjectHandle);
        free(item_handle);
        close_status = NFCSTATUS_SUCCESS;
    }
    else
    {
        close_status = PHNFCSTVAL(CID_NFC_NONE, NFCSTATUS_INVALID_PARAMETER);
    }
    return close_status;
}

void phOsalNfc_Suspend(uint32_t WaitingTime)
{
    Sleep(WaitingTime);
}

NFCSTATUS phOsalNfc_CreateSemaphore(void        **hSemaphore,
                                    uint8_t     InitialValue,
                                    uint8_t     MaxValue)
{
    NFCSTATUS creation_status = NFCSTATUS_SUCCESS;

    phOsalNfc_sOsalHandle_t *semaphore_handle = malloc(sizeof(phOsalNfc_sOsalHandle_t));

    if(semaphore_handle != NULL)
    {
        semaphore_handle->Params = NULL;
        semaphore_handle->ThreadFunction = NULL;

        semaphore_handle->ObjectHandle = 
            CreateSemaphore(NULL, InitialValue, MaxValue, NULL);

        if (semaphore_handle->ObjectHandle != NULL)
        {
            *(phOsalNfc_sOsalHandle_t**) hSemaphore = semaphore_handle;
        }
        else
        {
            creation_status = PHNFCSTVAL(CID_NFC_NONE, NFCSTATUS_SEMAPHORE_CREATION_ERROR);
            free(semaphore_handle);
        }
    }
    else
    {
        creation_status = PHNFCSTVAL(CID_NFC_NONE, NFCSTATUS_INSUFFICIENT_RESOURCES);
    }

    return creation_status;
}

NFCSTATUS phOsalNfc_ProduceSemaphore(void *hSemaphore)
{
    NFCSTATUS release_status;
    BOOL release_succeeded = FALSE;
    phOsalNfc_sOsalHandle_t *semaphore_handle = (phOsalNfc_sOsalHandle_t*) hSemaphore;
    
    if (semaphore_handle != NULL)
    {
        release_succeeded = ReleaseSemaphore(semaphore_handle->ObjectHandle, 1, NULL);

        if (release_succeeded == TRUE)
        {
            release_status = NFCSTATUS_SUCCESS;
        }
        else
        {
            release_status = PHNFCSTVAL(CID_NFC_NONE, NFCSTATUS_SEMAPHORE_PRODUCE_ERROR);
        }
    }
    else
    {
        release_status = PHNFCSTVAL(CID_NFC_NONE, NFCSTATUS_INVALID_PARAMETER);
    }
    
    return release_status;
}

NFCSTATUS phOsalNfc_ConsumeSemaphore(void *hSemaphore)
{
    return phOsalNfc_ConsumeSemaphoreEx(hSemaphore, INFINITE);
}

NFCSTATUS phOsalNfc_ConsumeSemaphoreEx(void *hSemaphore, uint32_t Timeout)
{
   
    NFCSTATUS consume_status;
    phOsalNfc_sOsalHandle_t *semaphore_handle = (phOsalNfc_sOsalHandle_t*) hSemaphore;
    DWORD semaphore_status;

    if (semaphore_handle != NULL)
    { 
        semaphore_status = WaitForSingleObject(semaphore_handle->ObjectHandle, Timeout);
        if (semaphore_status == WAIT_OBJECT_0)
        {
            consume_status = NFCSTATUS_SUCCESS;
        }
        else
        {
            consume_status = PHNFCSTVAL(CID_NFC_NONE, NFCSTATUS_SEMAPHORE_CONSUME_ERROR);
        }   
    }
    else
    {
        consume_status = PHNFCSTVAL(CID_NFC_NONE, NFCSTATUS_INVALID_PARAMETER);
    }    
    
    return consume_status;        
}


void * phOsalNfc_GetMemory(uint32_t size)
{
   void * pMem = (void *) malloc(size);
   return pMem;
}

void phOsalNfc_FreeMemory(void * pMem)
{
    if(NULL !=  pMem)
        free(pMem);
}



NFCSTATUS phOsalNfc_PostMsg(uint32_t SourceID, uint32_t DestID, phOsalNfc_Message_t * pMsg)
{

    phOsalNfc_Message_t*   pMsg_i      = NULL;
    uint16_t                  retvalue;
    
    if(NULL ==  pMsg)
        return PHNFCSTVAL(CID_NFC_NONE, NFCSTATUS_INVALID_PARAMETER);

    pMsg_i    =   (void*) phOsalNfc_GetMemory(sizeof(phOsalNfc_Message_t));
    
    if(NULL ==  pMsg_i)
        return PHNFCSTVAL(CID_NFC_NONE, NFCSTATUS_INSUFFICIENT_RESOURCES);
        
    *pMsg_i    =   *pMsg;

    retvalue = (uint16_t)PostThreadMessage((DWORD)DestID,
                                 (UINT)(PHOSALNFC_MESSAGE_BASE),
                                 (WPARAM)pMsg_i,
                                 (LPARAM)SourceID);

    if(retvalue == 0)
    {
        phOsalNfc_FreeMemory(pMsg_i);
        return PHNFCSTVAL(CID_NFC_NONE, NFCSTATUS_INSUFFICIENT_RESOURCES);
    }

    return NFCSTATUS_SUCCESS;
}

NFCSTATUS phOsalNfc_ReceiveMsg(uint32_t *pSourceID, phOsalNfc_Message_t* pMsg)
{
    MSG         msg;
    uint16_t     ret;

    if(NULL    ==  pMsg ||  NULL    ==  pSourceID)
        return PHNFCSTVAL(CID_NFC_NONE, NFCSTATUS_INVALID_PARAMETER);

    ret = (uint16_t)GetMessage(   &msg,
                        NULL,
                        PHOSALNFC_MESSAGE_BASE,
                        PHOSALNFC_MESSAGE_BASE);
  
    *pSourceID  =   0;
    memset(pMsg, 0x0, sizeof(*pMsg));

    if (ret == 0   ||  NULL    ==  (void*)msg.wParam)
    {
        return PHNFCSTVAL(CID_NFC_NONE, NFCSTATUS_INSUFFICIENT_RESOURCES);
    }

    *pSourceID  =   (uint32_t)(msg.lParam);
    *pMsg    =   *(phOsalNfc_Message_t *)msg.wParam;

    phOsalNfc_FreeMemory((void*)msg.wParam);

    return NFCSTATUS_SUCCESS;
}

void   phOsalNfc_RandInit( void)
{
    #ifndef WINCE	/*	This does not work under Windows CE */
		srand( (unsigned)time( NULL ) );
    #endif

}

uint16_t    phOsalNfc_RandGetNumber( void)
{
    #ifdef WINCE
		return (uint16_t)Random( );
    #else
		return rand( );
    #endif
    
}

uint32_t    phOsalNfc_GetTicks( )
{
  LARGE_INTEGER ticksPerSecond;
  LARGE_INTEGER tick;   

  uint32_t  nbMicro =   0;

  /* high resolution counter's accuracy */
  QueryPerformanceFrequency(&ticksPerSecond);

  /* Current Counter value */
  QueryPerformanceCounter(&tick);

  nbMicro   =   (uint32_t)(((double)tick.QuadPart / (double)ticksPerSecond.QuadPart) * 1E6);
  
  return nbMicro ;
}


