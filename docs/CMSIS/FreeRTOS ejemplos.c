#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"
#include "lpc17xx.h"
#include "stdio.h"

#define MS(t)		((portTickType) ( (t) / portTICK_RATE_MS))
#define Stack(s)	( configMINIMAL_STACK_SIZE + s )
#define Prority(p) 	( taskIDLE_PRIORITY + p )

static void vTask1( void *pvParameters );

void vApplicationIdleHook(void);
void vApplicationTickHook(void);

xSemaphoreHandle xSem1 = NULL;
xSemaphoreHandle xCount1 = NULL;
xSemaphoreHandle xMutex1 = NULL;
xQueueHandle xQueue1 = NULL;

int main(void){
	
	// Init vbles
	InitHardware();
	
	NVIC_EnableIRQ(UART_IRQn);
	NVIC_SetPriority(UART_IRQn, priority);	// ((0x01 << 3) | 0x01)

	vSemaphoreCreateBinary (xSem1);
	xSemaphoreTake(xSem1, MS(0)); // Init Rojo
	
	xCount1 = xSemaphoreCreateCounting(uxMaxCount, uxInitialCount);
	xMutex = xSemaphoreCreateMutex();	
	xQueue1 = xQueueCreate(NUM_QUEUE, sizeof(char));
	
	xTaskHandle xthTask1;
	
	xTaskCreate(vTask1,
				(const signed char *)"Task1",
				Stack(0),		// ( configMINIMAL_STACK_SIZE + s )
				NULL,
				Priority(0),	// ( taskIDLE_PRIORITY + p )
				&xthTask1);
				
	vTaskStartSchedules();
	
	while (TRUE){
		i++;		// ERROR!!
	}	
	return 0;
}
			
static void vTask1( void *pvParameters ){
	//Init vbles
	while (TRUE){
		// Cuerpo de la tarea
		
		esMutex = xSemaphoreTake(xMutex, portMAX_DELAY);  // MS(t); Sirve para Sem1 y Count1
		if (esMutex){
			// Usar Mutex
			xSemaphoreGive(xMutex);
		}
		else {
			// Mutex ocupado
		}
		taskYIELD();
	}
	vTaskDelete(NULL); // ERROR!!
}

void TIMER0_IRQHandler(void){
	TIM_Cmd(LPC_TIM0, DISABLE);
	TIM_ResetCounter(LPC_TIM0);

	portBASE_TYPE xHigherPriorityTaskWoken;
	xHigherPriorityTaskWoken = pdFALSE;

// Inicio de tarea del timer
	xSemaphoreGiveFromISR( xSem1, &xHigherPriorityTaskWoken);
	xQueueReceiveFromISR(xQueue1, &input, &xHigherPriorityTaskWoken);
	xQueueSendFromISR(xQueue1, &output, &xHigherPriorityTaskWoken);
	
// Fin de tarea del timer

	TIM_ClearIntPending(LPC_TIM0, TIM_MR3_INT);
	TIM_Cmd(LPC_TIM0, ENABLE);

	portEND_SWITCHING_ISR(xHighierProrityTaskWoken);
}

void vApplicationIdleHook( void ){
	ulIdleCycleCount++;
}

void vApplicationTickHook( void ){
	ulIdleCycleCount++;
}

// Funciones utiles
xQueueReceive(xQueue1, &input, MS(time));
xQueuePeek(xQueue1, &input, MS(time));
xQueueSend(xQueue1, &output, MS(time)); // xQueueSendToBack(xQueue, &output, MS(time));
xQueueSendToFront(xSalida, &output, MS(time));
datos = uxQueueMessagesWaiting(xQueue);
vPrintString("Hola Mundo");
vTaskDelay (MS(time));
void vTaskDelayUntil( portTickType *pxPreviousWakeTime, portTickType xTimeIncrement );
// ej:
	portTickType xLastWakeTime;
	xLastWakeTime = xTaskGetTickCount();
	while (TRUE){
		vTaskDelayUntil( &xLastWakeTime, MS(xFrequency)) ;	// ((portTickType) ( (t) / portTICK_RATE_MS))
	}
taskENTER_CRITICAL();
taskEXIT_CRITICAL();