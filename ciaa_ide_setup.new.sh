#!/bin/sh

# Delete conflict
sudo apt-get remove binutils-arm-none-eabi gcc-arm-none-eabi

# Add repositories
sudo add-apt-repository ppa:team-gcc-arm-embedded/ppa
sudo apt-get update

# Install common packs
sudo apt-get install -y -m -q gcc-arm-none-eabi git git-gui gtkterm openocd libftdi-dev libwebkitgtk-1.0-0 php7.2-cli
sudo snap install --classic eclipse

arch=$(arch)
# 64 bits packs
if [ $arch = x86_64 ]
then
sudo apt-get install -y -m -q gtk2-engines-murrine:i386 libglu1-mesa:i386 libgtk2.0-0:i386 libidn11:i386 libncurses5:i386 libnss3-1d:i386 libpangox-1.0-0:i386 libpangoxft-1.0-0:i386 libudev1:i386 libusb-0.1:i386 libusb-1.0-0-dev libusb-1.0:i386 libxtst6:i386

# 32 bits packs
else
sudo apt-get install -y -m -q gtk2-engines-murrine libglu1-mesa libgtk2.0-0 libidn11 libncurses5 libnss3-1d libpangox-1.0-0 libpangoxft-1.0-0 libudev1 libusb-0.1 libusb-1.0 libusb-1.0-0-dev libxtst6
fi

# Downloading firmware
git clone --recursive https://github.com/ciaa/firmware_v2.git

# Configurating system
openocd -f firmware_v2/etc/openocd/lpc4337.cfg
sudo adduser $USER dialout
sudo usermod -aG plugdev $USER
sudo udevadm control --reload-rules
sudo service udev restart

# Configurate Eclipse workspace
