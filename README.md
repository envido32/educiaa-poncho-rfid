# Poncho RFID para eduCIAA

Desarrollo de un [Poncho](http://www.proyecto-ciaa.com.ar/devwiki/doku.php?id=desarrollo:ciaa:ponchos) [RFID](https://dangerousthings.com/shop/pn532/) para [EduCIAA](http://www.proyecto-ciaa.com.ar/devwiki/doku.php?id=desarrollo:edu-ciaa:edu-ciaa-nxp).

Para estudiar, compartir, modificar y mejorar.

El Software tiene [Licencia GPL v3.0](https://www.gnu.org/licenses/gpl-3.0.en.html)

El Hardware tiene [Licencia CERN v1.2](https://www.ohwr.org/cernohl)

Proyecto CIAA tiene [Licencia BSD](http://www.proyecto-ciaa.com.ar/devwiki/doku.php?id=proyecto:licencia_ciaa)

- [Proyecto CIAA](http://www.proyecto-ciaa.com.ar/)
- [Latest firmware](https://github.com/ciaa/firmware_v2/releases)
- [Latest sAPI](https://github.com/epernia/sAPI/releases)

# Desarrollo

En la [Bitacora](docs/README.md) estoy haciendo un seguimento semana a semana para que quede registrado en un documento que se puede para mejorar en el proyecto y a los que empiezan pierdan miedo a la aventura de la electronica con el objetivo de desarrollar un Poncho para RFID desde cero.

En el directorio [code](code) encontraran el código de desarrollo el proyecto asi como algunos módulos de prueba que fui desarrollando.

En el directorio [docs](docs) encontraran documentación variada utilizada para el desarrollo del proyecto.

En el directorio [examples](examples) encontraran ejemplos de código que fui encontrando para las diferentes librerías. Los uso para estudiar su implementación y la mayoría funcionan pero no probe personalmente todos.


## Usage
* Install IDE runing ```ciaa_ide_setup.sh``` in Ubuntu based systems or manualy in other GNU/Linux distribution or Windows with [this manual](http://www.proyecto-ciaa.com.ar/devwiki/doku.php?id=docu:fw:bm:ide:install).
* Make sure you have an ```arm-none-eabi-*``` toolchain configured in your ```PATH```. If you don't have it, download [GCC ARM Embedded](https://developer.arm.com/open-source/gnu-toolchain/gnu-rm).
* ```git clone --recursive https://gitlab.com/envido32/educiaa-poncho-rfid.git```
* ```cp project.mk.template project.mk```
* Define ```PROJECT```, ```TARGET``` and ```BOARD``` (optional) variables in ```project.mk``` according to the project you want to compile.
* Compile with ```make```.
* Clean with ```make clean```. Clean for all targets with ```make clean_all```.
* Download to target via OpenOCD with ```make download```. If you don't have it, download [OpenOCD](http://openocd.org/)

## Supported targets
- [LPC4337](https://www.nxp.com/products/processors-and-microcontrollers/arm-based-processors-and-mcus/lpc-cortex-m-mcus/lpc4300-cortex-m4-m0/32-bit-arm-cortex-m4-m0-mcu-up-to-1-mb-flash-and-136-kb-sram-ethernet-two-high-speed-usb-lcd-emc:LPC4337JBD144)
 
Can be expanded to:
- LPC11U68
- LPC1769
- LPC54102

## Supported boards
- [Edu-CIAA-NXP](http://www.proyecto-ciaa.com.ar/devwiki/doku.php?id=desarrollo:edu-ciaa:edu-ciaa-nxp)

Can be expanded to:
- Other CIAA based boards
- LPCXpresso with LPC1769
- LPCXpresso with LPC54102
- LPCXpresso with LPC11U68

## Available libraries
- [sAPI](https://github.com/epernia/sapi)
- [FreeRTOS](http://www.freertos.org/)
- [LPCOpen](https://www.lpcware.com/lpcopen)
- [CMSIS](http://www.arm.com/products/processors/cortex-m/cortex-microcontroller-software-interface-standard.php)
- [CMSIS-DSPLIB](http://www.keil.com/pack/doc/CMSIS/DSP/html/index.html).
- [lwIP](http://lwip.wikia.com/wiki/LwIP_Wiki)

## Supported toolchains
- [GNU Embedded Toolchain for Arm](https://developer.arm.com/open-source/gnu-toolchain/gnu-rm/downloads)

## Supported debugger
- [Open On-Chip Debugger](http://openocd.org/)

## This environment is based on
- **Workspace** by Pablo Ridolfi: <https://github.com/pridolfi/workspace>
- **sAPI** by Eric Pernia: <https://github.com/epernia/sapi>